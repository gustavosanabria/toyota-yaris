﻿using UnityEngine;
using System.Collections;

public class ARHelpText : MonoBehaviour
{
	TweenPosition twS;
	[SerializeField] Texture helpTex;
	Rect helpRect;
	bool slideHelp;

	void Awake ()
	{
		helpRect.width = Screen.width;
		helpRect.height = helpRect.width / 15;

		twS = GetComponent<TweenPosition>();

		float t = twS.delay * .8f;

		Invoke ("AdjustTweenPosition", t);
	}

	void AdjustTweenPosition()
	{
		slideHelp = true;
		return;

		twS.from = transform.localPosition;
		//twS.to = twS.from + new Vector3 (0, 2000, 0);
		twS.to = twS.from + new Vector3 (0,GetComponent<UITexture>().height*1.5f,0);
	}

	void FixedUpdate()
	{
		if(slideHelp)
		{
			helpRect.y -= Time.fixedDeltaTime * 32;
			if(helpRect.y < helpRect.height*-1)
			{
				slideHelp = false;
				Destroy(this.gameObject);
			}
		}
	}

	void OnGUI()
	{
		GUI.DrawTexture(helpRect, helpTex);
	}
}
