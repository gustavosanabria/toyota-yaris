﻿using UnityEngine;
using System.Collections;

public class AppLibrary : MonoBehaviour
{
	public const byte AppVERSO = 0;
	public const byte AppRAV4 = 1;

    public static readonly byte AppTYPE = AppVERSO;
 
    //SCENES<
    public const byte SCENE_SPLASH = 1;

    public const byte SCENE_YARIS_MENU = 2;
    public const byte SCENE_YARIS_AR = 3;
    public const byte SCENE_YARIS_360 = 4;

    public const byte SCENE_AR_HELPVIDEO = 5;
    
	public const byte SCENE_GLOSSARY = 6;

    public const byte SCENE_EMPTY = 7;
    public const byte SCENE_LOADING = 8; 
    //>SCENES

    public const byte VERSO_MARKER_STEERINGWHEEL = 0;
	public const byte VERSO_MARKER_TACHOMETER = 1;
	public const byte VERSO_MARKER_RADIO = 2;
	public const byte RAV4_MARKER_AC = 3;
	public const byte RAV4_MARKER_SW = 4;
	public const byte RAV4_MARKER_TACHOMETER = 5;

	public static byte sceneLoad = 0;

	public static bool openTutAtomatically = false;
	public static bool openTFTTutAtomatically = false;

	public const int TUT_CATEGORY_PHONE = 1;
	public const int TUT_CATEGORY_GPS = 2;
	public const int TUT_CATEGORY_APP = 3;
	public const int TUT_CATEGORY_RADIO = 4;
	public static string TUT_TO_OPEN = "";	//MOD GLOSSARY V.2
	public static string LASTDEF = "";	//MOD GLOSSARY V.2

	public static int TFTSourceScene;

	public static string CURRENTVIDEONAME;
	public static string VIDEO_HELP_AR = "ARHelpVideo.mp4";
	public static string VIDEO_VR = "TireRepairkitFR.mp4";

	public const string AssistancePhone1 = "0800808935";
	public const string AssistancePhone1B = "+33149937226";
	public const string AssistancePhone2 = "0800869682";
	public const string AssistancePhone2B = "+33171041747";

	public static bool AR_HELPVIDEO_PLAYED = false;
    public static bool HYBRID_PLAYED = false;

	//GOOGLE PLAY APP LICENSE KEY
	public static string APPKEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqzJLjaxaIS5G4VTbgwyOfzuduqQbpjHEOuzLCuczXpmzrJKZGqurZHugUgYl6bSr0qmrz4j48XmjNFGRYyOQP5rpK31tvQMijBn2xvuK0NWZFQzzIv/gB75e3Ngwv4ME148Q4aTYZuiE0G9PLO68qAkFhlfcZyr53BdLQtYIqBngUM/upDs8eG07C+IEjzEdQwwjqP2rRsg+EKbIufTldsK9C9pe3Tbg5hWkyBMGm2MRFktAq1iWE2gD57q2nv+bSYtxqSVUpMzm8ei68pSvlPqZD3TXaBhH3E8B+5D9IcFcqF234ZfZPcUb4CPI6ITX0Gfe3sm2kC1fDEGXnjblfQIDAQAB";
}
