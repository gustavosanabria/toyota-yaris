﻿using UnityEngine;
using System.Collections;

public class DescriptionsStrings : MonoBehaviour
{

    public string
                    versoPrecollision,
                    versoLights,
                    versoStabilityControl,
                    versoParkingAssist,
                    versoMirror,
                    versoVoiceCommand,
                    versoMode,
                    versoPhone,
                    versoDispTrip,
                    versoSpeedLimit,
                    versoSpeedRegulator,
                    versoLeftTachometer,
                    versoRightTachometer,
                    versoCenterScreen,
                    versoGPSLeftWheel,
                    versoGPSRightWheel,
                    versoGPSLeftMenu,
                    versoGPSRightMenu,
                    versoACLeftMenu,
                    versoACCenterMenu,
                    versoACRightMenu,
                    versoAirbag,
                    versoWindow,
                    versoPower,
                    versoA_OFF,
        doorDescr,
        mirrorDescr,
        tires_preCollissionDescr,
        volume_ModeDescr,
        phone_LineCrossDescr,
        speedRegulatorDescr,
        warning_airbag_seatbeltDescr,
        ac_DualDescr,
        auto_OffDescr,
        front_rearDescr,
        usb_12VoltDescr,
        mode_stabilityCtrlDescr,
        enginePowerDescr,
        fuelTankLidDescr,
        hoodOpenBtnDescr;
}
