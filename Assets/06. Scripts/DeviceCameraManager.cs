﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class DeviceCameraManager : MonoBehaviour
{
	bool autofocusEnabled, flashEnabled;
	[SerializeField] UITexture flashBtnUITex;
	[SerializeField] Texture2D flashOnTex, flashOffTex;
	[SerializeField] TextMesh textMesh;
	
	void Awake()
	{
		Invoke("Focus", 1);
	}

	public void EnableDeviceFlash()
	{
		flashEnabled = !flashEnabled;
		CameraDevice.Instance.SetFlashTorchMode(flashEnabled);

		flashBtnUITex.mainTexture = flashEnabled ? flashOnTex : flashOffTex;
	}
	
	void OnApplicationFocus(bool focused)
	{
		if(focused) Invoke("Focus", 1);
	}

	void Focus()
	{
		autofocusEnabled = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

		//if(textMesh) textMesh.text = autofocusEnabled.ToString();
	}
}
