// ***********************************************************
// Written by Heyworks Unity Studio http://unity.heyworks.com/
// ***********************************************************
using UnityEngine;

/// <summary>
/// Gyroscope controller that works with any device orientation.
/// </summary>
public class GyroscopeController : MonoBehaviour
{
    private readonly Quaternion baseIdentity = Quaternion.Euler(90, 0, 0);
    private Quaternion referanceRotation = Quaternion.identity;

    private Quaternion cameraBase = Quaternion.identity;
    private Quaternion calibration = Quaternion.identity;
    private Quaternion baseOrientation = Quaternion.Euler(90, 0, 0);
    private Quaternion baseOrientationRotationFix = Quaternion.identity;
	
    public bool gyroEnble = true;
    public float minSwipeDistY;
    public float minSwipeDistX;

    public Camera cam360;
    public Camera camButtons;

    Vector2 startPos;
    float swipeDistVertical, swipeTime = 1;
    bool canCountUp = false;

    private Quaternion originalRotation;

#if UNITY_ANDROID
    private float sensitivityX = 0.75F;
    private float sensitivityY = 0.75F;
#else
	private float sensitivityX = 0.5F;
	private float sensitivityY = 0.5F;
#endif

    private float minimumX = -360F;
    private float maximumX = 360F;

    private float minimumY = -60F;
    private float maximumY = 60F;

    private float rotationY = 0F;
    private float rotationX = 0F;

    protected void Start()
    {
        AttachGyro();
    }

    protected void Update()
    {
#if UNITY_EDITOR
		if (Input.touchCount == 1) {
			Touch touch = Input.touches [0];
			if (touch.phase == TouchPhase.Moved) {
					rotationX += touch.deltaPosition.x * sensitivityX;
					rotationY += touch.deltaPosition.y * sensitivityY;
					
					rotationX = ClampAngle (rotationX, minimumX, maximumX);
					rotationY = ClampAngle (rotationY, minimumY, maximumY);
					
					Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
					Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, -Vector3.right);
					
				    cam360.transform.localRotation = ConvertRotation (referanceRotation * originalRotation * xQuaternion * yQuaternion);
                    camButtons.transform.localRotation = ConvertRotation (referanceRotation * originalRotation * xQuaternion * yQuaternion);
				}
			}

		if (gyroEnble) {
			transform.rotation = cameraBase * (ConvertRotation (referanceRotation * Input.gyro.attitude) * GetRotFix ());
            camButtons.transform.localRotation = ConvertRotation (referanceRotation * Input.gyro.attitude);
		}

        countSwipeTimeUp();
#endif
    }

    /// <summary>
    /// Attaches gyro controller to the transform.
    /// </summary>
    private void AttachGyro()
    {
        Input.gyro.enabled = true;
        gyroEnble = Input.gyro.enabled;

		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;

        if (gyroEnble)
        {
			CalibrateGyro();
        }

		originalRotation = transform.localRotation;
    }

    /// <summary>
    /// Detaches gyro controller from the transform
    /// </summary>
    private void DetachGyro()
    {
        Input.gyro.enabled = false;
    }

	private void CalibrateGyro()
	{
		ResetBaseOrientation();
		UpdateCalibration(true);
		UpdateCameraBaseRotation(true);
		RecalculateReferenceRotation();
	}

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    /// <summary>
    /// Update the gyro calibration.
    /// </summary>
    private void UpdateCalibration(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            var fw = (Input.gyro.attitude) * (-Vector3.forward);
            fw.z = 0;
            if (fw == Vector3.zero)
            {
                calibration = Quaternion.identity;
            }
            else
            {
                calibration = (Quaternion.FromToRotation(baseOrientationRotationFix * Vector3.up, fw));
            }
        }
        else
        {
            calibration = Input.gyro.attitude;
        }
    }

    /// <summary>
    /// Update the camera base rotation.
    /// </summary>
    /// <param name='onlyHorizontal'>
    /// Only y rotation.
    /// </param>
    private void UpdateCameraBaseRotation(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            var fw = transform.forward;
            fw.y = 0;
            if (fw == Vector3.zero)
            {
                cameraBase = Quaternion.identity;
            }
            else
            {
                cameraBase = Quaternion.FromToRotation(Vector3.forward, fw);
            }
        }
        else
        {
            cameraBase = transform.rotation;
        }
    }

    /// <summary>
    /// Converts the rotation from right handed to left handed.
    /// </summary>
    /// <returns>
    /// The result rotation.
    /// </returns>
    /// <param name='q'>
    /// The rotation to convert.
    /// </param>
    private Quaternion ConvertRotation(Quaternion q)
    {
		return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    /// <summary>
    /// Gets the rot fix for different orientations.
    /// </summary>
    /// <returns>
    /// The rot fix.
    /// </returns>
    private Quaternion GetRotFix()
    {
#if UNITY_3_5
		if (Screen.orientation == ScreenOrientation.Portrait)
			return Quaternion.identity;
		
		if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.Landscape)
			return landscapeLeft;
				
		if (Screen.orientation == ScreenOrientation.LandscapeRight)
			return landscapeRight;
				
		if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
			return upsideDown;
		return Quaternion.identity;
#else
        return Quaternion.identity;
#endif
    }

    /// <summary>
    /// Recalculates reference system.
    /// </summary>
    private void ResetBaseOrientation()
    {
        baseOrientationRotationFix = GetRotFix();
        baseOrientation = baseOrientationRotationFix * baseIdentity;
    }

    /// <summary>
    /// Recalculates reference rotation.
    /// </summary>
    private void RecalculateReferenceRotation()
    {
        referanceRotation = Quaternion.Inverse(baseOrientation) * Quaternion.Inverse(calibration);
    }


    void countSwipeTimeUp()
    {
        if (canCountUp)
        {
            swipeTime += Time.deltaTime;
        }
    }
}
