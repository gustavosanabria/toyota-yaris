﻿#if UNITY_EDITOR

using UnityEngine; 
using System.Collections;

public class GyroscopeEnable : MonoBehaviour
{    
	private Gyroscope gyro;
	[SerializeField] VRCameraClamper cameraClamper;

	void Awake()
	{

	}

	void Start ()
	{
		cameraClamper.enabled = true;

		if (SystemInfo.supportsGyroscope)
		{
			gyro = Input.gyro;
			gyro.enabled = true;
		}
	}
}

#endif
