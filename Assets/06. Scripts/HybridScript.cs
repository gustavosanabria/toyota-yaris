﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HybridScript : MonoBehaviour {

    void Awake()
    {
#if UNITY_ANDROID || UNITY_IOS
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            ChangeScene();
            return;
        }
    }

    public void ChangeScene()
    {
        AppLibrary.HYBRID_PLAYED = true;
        MySceneManager.GoToHomeScreen(true);
    }
}
