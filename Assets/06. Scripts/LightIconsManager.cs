﻿using UnityEngine;
using System.Collections;

public class LightIconsManager : MonoBehaviour
{
	[SerializeField] GameObject iconsContainer;
	[SerializeField] GameObject target28IconContainer;
	[SerializeField] GameObject targetSHIFTIconContainer;
    [SerializeField] GameObject target30IconContainer;
	[SerializeField] TweenScale[] lightIconTweens;
	GameObject currentIcon;
	GameObject currentDescription;
	byte iconsCount;
	bool showingDescription;

	void Awake()
	{
		GameObject[] icons = GameObject.FindGameObjectsWithTag("LightIcon");
		iconsCount = (byte) icons.Length;

		iconsContainer.SetActive(false);
		DisableIconContainers();

		lightIconTweens = new TweenScale[iconsCount];

		for (byte i = 0; i < iconsCount; i++) lightIconTweens[i] = icons[i].GetComponent<TweenScale>();
	}

	void DisableIconContainers()
	{
		target28IconContainer.SetActive(false);
        targetSHIFTIconContainer.SetActive(false);
        target30IconContainer.SetActive(false);
	}

	public void SetIconsGroup(GameObject group)
	{
		DisableIconContainers();
		group.SetActive(true);
	}

	public void ShowIcons()
	{
		CancelInvoke("DisableIconsContainer");
		iconsContainer.SetActive(true);

		iconsContainer.GetComponent<TweenScale>().PlayForward();
		iconsContainer.GetComponent<TweenScale>().ResetToBeginning();
		iconsContainer.GetComponent<TweenScale>().PlayForward();

		GetComponent<TargetManager>().EnableTargets(false);

		EnableIcons(true);
	}

	void HideIcons()
	{
		iconsContainer.GetComponent<TweenScale>().PlayReverse();
		Invoke("DisableIconsContainer", .35f);
		EnableIcons(false);
		GetComponent<TargetManager>().EnableTargets(true);
	}

	void DisableIconsContainer()
	{
		iconsContainer.SetActive(false);
	}

	public void CloseIconDescription()
	{
		for (byte i = 0; i < iconsCount; i++) lightIconTweens[i].ResetToBeginning();
	}

	void CloseDescription()
	{
		CancelInvoke();
		currentDescription.transform.localScale = Vector3.zero;
		currentDescription.SetActive(false);

		showingDescription = false;
	}

	public void SetCurrentIcon(GameObject icon)
	{
		currentIcon = icon;
		currentIcon.GetComponent<Collider>().enabled = true;
	}

	GameObject currDescr;
	public void SetCurrenDescription(GameObject description)
	{
		CancelInvoke();

		if(description == currDescr)
		{
			CloseCurrenDescription();
			return;
		}

		EnableIcons(false);

		description.SetActive(true);

		currentDescription = description;
		currDescr = description;

		currentDescription.GetComponent<TweenScale>().PlayForward();
		currentDescription.GetComponent<TweenPosition>().PlayForward();

		currentDescription.GetComponent<TweenScale>().ResetToBeginning();
		currentDescription.GetComponent<TweenPosition>().ResetToBeginning();

		currentDescription.GetComponent<TweenScale>().PlayForward();
		currentDescription.GetComponent<TweenPosition>().PlayForward();
		showingDescription = true;
	}

	public void CloseCurrenDescription()
	{
		if(!showingDescription)
		{
			HideIcons();
			return;
		}
		currDescr = null;

		StopIconAnimation();

		EnableIcons(true);

		currentDescription.GetComponent<TweenScale>().PlayReverse();
		currentDescription.GetComponent<TweenPosition>().PlayReverse();

		Invoke("CloseDescription", .35f);
	}

	void StopIconAnimation()
	{
		if(!currentIcon) return;
			
		currentIcon.GetComponent<TweenScale>().enabled = false;
		currentIcon.GetComponent<TweenScale>().ResetToBeginning();
		currentIcon.transform.localScale = Vector3.one;
	}
	
	void EnableIcons(bool b)
	{
		for (byte i = 0; i < iconsCount; i++) lightIconTweens[i].GetComponent<Collider>().enabled = b;
	}

	void ToIconSelect ()
	{
		for (byte i = 0; i < iconsCount; i++) lightIconTweens[i].ResetToBeginning();
	}
}
