﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
	AsyncOperation levelAsync;
	bool showARProgressBar;
	[SerializeField] GameObject loadingBar;
    [SerializeField] GameObject loadingText;
    [SerializeField] UITexture helpTex;
	[SerializeField] Texture2D vrHelpTex;
	static byte sceneID;
	
	void Start ()
	{
        if(sceneID == AppLibrary.SCENE_YARIS_MENU)
        {
            helpTex.gameObject.SetActive(false);
            loadingText.SetActive(true);
        }
        else
        {
            helpTex.gameObject.SetActive(true);
            loadingText.SetActive(false);
        }

		if(sceneID == AppLibrary.SCENE_YARIS_360) helpTex.mainTexture = vrHelpTex;
		LoadScene();
	}

	public static void SetLevelToLoad(byte pSceneID)
	{
		sceneID = pSceneID;
	}

	void LoadScene()
	{
		levelAsync = SceneManager.LoadSceneAsync(sceneID);
		showARProgressBar = true;
	}
	                                  
	void Update ()
	{
		if(!showARProgressBar) { return; }

		loadingBar.GetComponent<UITexture>().fillAmount = levelAsync.progress;
		
		if(levelAsync.progress > .85f)
		{
			loadingBar.GetComponent<UITexture>().fillAmount = 1;
			levelAsync.allowSceneActivation = true;
		}
	}
}
