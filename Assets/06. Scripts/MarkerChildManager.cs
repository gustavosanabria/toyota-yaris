﻿using UnityEngine;
using System.Collections;

public class MarkerChildManager : MonoBehaviour
{
	[SerializeField] TweenScale[] childTweens;
	[SerializeField] byte childTweensCount;
	Vector3 childInitialSize;

	void Awake()
	{
		childTweens = GetComponentsInChildren<TweenScale>();
		childTweensCount = (byte) childTweens.Length;

		for (byte i = 0; i < childTweensCount; i++)
		{
			childInitialSize = childTweens[i].transform.localScale;
			childTweens[i].from = childInitialSize;
			childTweens[i].to = childInitialSize * .7f;
		}
	}

	public void AnimateTargets()
	{
		Animator an;
		float time;
		string name = "name";

		time = 0f;

		for (byte i = 0; i < childTweensCount; i++)
		{
			an = childTweens[i].GetComponent<Animator>();

			time += 5;

			if (an.GetCurrentAnimatorStateInfo(0).IsName("CibleRectAnim")) name = "CibleRectAnim";
			else if (an.GetCurrentAnimatorStateInfo(0).IsName("CibleRondeAnim")) name = "CibleRondeAnim";
			else if (an.GetCurrentAnimatorStateInfo(0).IsName("CibleRectLongAnim 1")) name = "CibleRectLongAnim 1";

			//print (name);
			an.enabled = true;
			an.Play(name, 0, .045f * time);
		}
	}
}
