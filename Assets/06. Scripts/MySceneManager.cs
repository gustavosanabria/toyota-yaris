using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
	public static void GoToHomeScreenAndOpenTut()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		//Application.LoadLevel(AppLibrary.SCENE_VERSO_MENU);
		AppLibrary.openTutAtomatically = true;
        LoadingScript.SetLevelToLoad(AppLibrary.SCENE_YARIS_MENU);
        SceneManager.LoadScene(AppLibrary.SCENE_LOADING);
    }

	public static void GoToHomeScreenAndOpenTFTTut()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		//Application.LoadLevel(AppLibrary.SCENE_VERSO_MENU);
		AppLibrary.openTFTTutAtomatically = true;
        SceneManager.LoadScene(GetHomeScene());
	}

	public static void GoToHomeScreen(bool b)
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		//Application.LoadLevel(AppLibrary.SCENE_VERSO_MENU);
		AppLibrary.openTutAtomatically = false;
		AppLibrary.openTFTTutAtomatically = false;

        LoadingScript.SetLevelToLoad(AppLibrary.SCENE_YARIS_MENU);
        SceneManager.LoadScene(AppLibrary.SCENE_LOADING);
    }

	public void GoToHomeScreen()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		//Application.LoadLevel(AppLibrary.SCENE_VERSO_MENU);
		AppLibrary.openTutAtomatically = false;
		AppLibrary.openTFTTutAtomatically = false;

        LoadingScript.SetLevelToLoad(AppLibrary.SCENE_YARIS_MENU);
        SceneManager.LoadScene(AppLibrary.SCENE_LOADING);
    }

	public void ReturnToGlossary()	//MOD GLOSSARY V.2
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		AppLibrary.openTutAtomatically = false;
		AppLibrary.openTFTTutAtomatically = false;

		Application.LoadLevel (AppLibrary.SCENE_GLOSSARY);
	}

	static byte GetHomeScene()
	{
        //return AppLibrary.AppTYPE == AppLibrary.AppVERSO ? AppLibrary.SCENE_VERSO_MENU : AppLibrary.SCENE_RAV4_MENU;
        return AppLibrary.SCENE_YARIS_MENU;
	}

	public void GoToARScreen()
	{
		print("AR VIEW");
		//Application.LoadLevel(AppLibrary.SCENE_AR);
		Application.LoadLevel(GetARScene());
		//Application.LoadLevel(AppLibrary.SCENE_EMPTY);
	}

	public byte GetARScene()
	{
        //return AppLibrary.AppTYPE == AppLibrary.AppVERSO ? AppLibrary.SCENE_VERSO_AR : AppLibrary.SCENE_RAV4_AR;
        return AppLibrary.SCENE_YARIS_AR;
	}

	public static void JumpFromGlossaryTo360()
	{
		Application.LoadLevel(AppLibrary.SCENE_YARIS_360);
	}

	public static void JumpFromGlossaryToTutCategory()
	{
		Application.LoadLevel(AppLibrary.SCENE_YARIS_MENU);
	}

	public void GoTo360ViewScreen()
	{
		print("360 VIEW");
		//Application.LoadLevel(AppLibrary.SCENE_360);
		Application.LoadLevel(GetVRScene());
		//Application.LoadLevel(AppLibrary.SCENE_EMPTY);
	}

	public byte GetVRScene()
	{
        //return AppLibrary.AppTYPE == AppLibrary.AppVERSO ? AppLibrary.SCENE_VERSO_360 : AppLibrary.SCENE_RAV4_360;
        return AppLibrary.SCENE_YARIS_360;
	}
}
