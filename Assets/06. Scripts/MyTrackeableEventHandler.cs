﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class MyTrackeableEventHandler : MonoBehaviour, ITrackableEventHandler
{
		public GameObject ARController;		
		protected TrackableBehaviour _trackableBehaviour;
		
		void Start()
		{
			_trackableBehaviour = GetComponent<TrackableBehaviour>();
			if (_trackableBehaviour)
			{
				_trackableBehaviour.RegisterTrackableEventHandler(this);
                OnTrackingLost();
			}
            
		}
		
		/// <summary>
		/// Implementation of the ITrackableEventHandler function called when the
		/// tracking state changes.
		/// </summary>
		public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
		{
            Debug.Log("TRACKED STATE CHANGE");

			if (newStatus == TrackableBehaviour.Status.DETECTED ||
			    newStatus == TrackableBehaviour.Status.TRACKED ||
			    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
			{
				OnTrackingFound();
			}
			else
			{
				OnTrackingLost();
			}
		}

        #region PROTECTED_METHODS

        public void EnableComponents()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }
        }

        public void DisableComponents()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }
        }

        private void OnTrackingFound()
        {
            EnableComponents();
			ARController.SendMessage("OnTrackingFound", _trackableBehaviour, SendMessageOptions.DontRequireReceiver);
        }

        private void OnTrackingLost()
        {
            DisableComponents();
            ARController.SendMessage("OnTrackingLost", _trackableBehaviour, SendMessageOptions.DontRequireReceiver);
        }


        #endregion PROTECTED_METHODS
}
