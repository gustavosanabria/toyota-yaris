#define SPLIT_APK
using UnityEngine;
//using UnityEditor;
using System;
using System.Collections;

public class OBBDownloader : MonoBehaviour {
	[SerializeField]UILabel loadingText;
	string originalText;
    
	void Awake()
	{
#if UNITY_IOS
		Application.LoadLevel( AppLibrary.SCENE_SPLASH );
#endif
		//Application.LoadLevel( AppLibrary.SCENE_SPLASH );	//Enable if apk is NOT splitted
	}

	IEnumerator Start () 
    {
		originalText = loadingText.text;

        // perforn any initialization here
#if ( SPLIT_APK && UNITY_ANDROID  && !UNITY_EDITOR)

        yield return new WaitForSeconds(0.1f);

        Utils.Core.AndroidOBBHandler handler = Utils.Core.AndroidOBBHandler.Get();

		handler.SetAppKey(AppLibrary.APPKEY);
        handler.OnError = (e) => 
        {
            Debug.LogError("OBB ERROR: " + e.ToString() );
        };

        handler.OnProgress = (p) =>
        {
            Debug.Log( string.Format("Cargando {0:0.00}%", p*100f) );
			loadingText.text = originalText + string.Format("{0:0.00}%", p*100f);
        };

        handler.OnCompleted = () =>
        {
            StartCoroutine(LoadMainMenu());
        };
        
        handler.StartDownload();
#else
        yield return null;
		loadingText.text = originalText + "100%";
        StartCoroutine( LoadMainMenu() );
#endif
	}


    IEnumerator LoadMainMenu()
    {
        yield return new WaitForSeconds(1);
        Application.LoadLevel( AppLibrary.SCENE_SPLASH );
    }
}