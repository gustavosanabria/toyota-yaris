﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public partial class DescriptionsManager : MonoBehaviour
{
    //[SerializeField] Texture2D rav4WindowsDescriptionTex;

    //[SerializeField] Texture2D rav4MirrorDescriptionTex;
    //[SerializeField] Texture2D rav4LineCrossDescriptionTex;
    //[SerializeField] Texture2D rav4TrunkDescriptionTex;

    //[SerializeField] Texture2D rav4SWVolumeDescriptionTex;
    //[SerializeField] Texture2D rav4SWPhoneDescriptionTex;
    //[SerializeField] Texture2D rav4SWVoiceCommandDescriptionTex;
    //[SerializeField] Texture2D rav4SWTftNavDescriptionTex;
    //[SerializeField] Texture2D rav4SWReturnTripDescriptionTex;
    //[SerializeField] Texture2D rav4SWPreCollisionDescriptionTex;

    //[SerializeField] Texture2D rav4EmergencyStopDescriptionTex;
    //[SerializeField] Texture2D rav4ParkingAssistanceDescriptionTex;
    //[SerializeField] Texture2D rav4Airbag_DescriptionTex;
    //[SerializeField] Texture2D rav4StabilityControl_DescriptionTex;

    //[SerializeField] Texture2D rav4EnginePower_DescriptionTex;

    //[SerializeField] Texture2D rav4AcAUTO_OFFDescriptionTex;
    //[SerializeField] Texture2D rav4AcMONO_ACDescriptionTex2;
    //[SerializeField] Texture2D rav4AcDEMISTERDescriptionTex3;
    //[SerializeField] Texture2D rav4AcINFOSCREENDescriptionTex4;
    //[SerializeField] Texture2D rav4AcFANSPEEDDescriptionTex5;

    //[SerializeField] Texture2D rav4ModeDescriptionTex;
    //[SerializeField] Texture2D rav4SeatHeatDescriptionTex;
    //[SerializeField] Texture2D rav4UsbAudioDescriptionTex;
    //[SerializeField] Texture2D rav412VoltDescriptionTex;

	public Dictionary<string, float> texturesOffset = new Dictionary<string, float>();

	void SetDictionary()
    {
    #region Rav4
        texturesOffset.Add("Target01_ExplanationBouton01-02-03",1.35f);
		texturesOffset.Add("Target02_ExplanationBouton04-05-06",1.35f);
		texturesOffset.Add("Target03_ExplanationBouton7-8",1);
		texturesOffset.Add("Target04_ExplanationButton11-12",1);
		texturesOffset.Add("Target07_ExplanationButton14",2f);
		texturesOffset.Add("Target08_ExplanationButton15-16",1.3f);
		texturesOffset.Add("Target09_ExplanationButton17-18",1.3f);
		texturesOffset.Add("Target10_ExplanationBouton20",2);
		texturesOffset.Add("Target11_ExplanationBouton21-22",1.4f);
		texturesOffset.Add("Target12_ExplanationBouton23",2);
		texturesOffset.Add("Target14_ExplanationBouton27",2);
		texturesOffset.Add("Target15_ExplanationBouton28",2);
		texturesOffset.Add("Target17_ExplanationButton33",2);
		texturesOffset.Add("Target18_ExplanationButton34",1.3f);
		texturesOffset.Add("Target19_ExplanationButton35-36-37",1.25f);
		texturesOffset.Add("Target22_ExplanationButton44-45-46",1.24f);
		texturesOffset.Add("Target21_ExplanationButton41-42-43",1.23f);
		texturesOffset.Add("Target23_ExplanationButton47",2f);
		texturesOffset.Add("Target20_ExplanationButton38-39-40",1.25f);
		texturesOffset.Add("Target25_ExplanationButton51",2);
		texturesOffset.Add("Target26_ExplanationButton52",2);
		texturesOffset.Add("Target27_ExplanationButton53",1.45f);
    #endregion
    #region Verso
        texturesOffset.Add("ExplanationButton6", 1.25f);
    #endregion
    }

    public void Rav4WindowsDescription()
	{
        StartCoroutine(GetTexture("Target01_ExplanationBouton01-02-03"));
	}

    public void Rav4MirrorDescription()
	{
        StartCoroutine(GetTexture("Target02_ExplanationBouton04-05-06"));
	}

    public void Rav4LineCrossDescription()
	{
        StartCoroutine(GetTexture("Target03_ExplanationBouton7-8"));
	}

    public void Rav4SW_TrunkDescription()
	{
        StartCoroutine(GetTexture("Target04_ExplanationButton11-12"));
	}

    public void Rav4SW_VOLUMEDescription()
	{
        StartCoroutine(GetTexture("Target07_ExplanationButton14"));
	}

    public void Rav4SW_PHONEDescription()
	{
        StartCoroutine(GetTexture("Target08_ExplanationButton15-16"));
	}

    public void Rav4SW_VOICECOMMANDDescription()
	{
        StartCoroutine(GetTexture("Target09_ExplanationButton17-18"));
	}

    public void Rav4SW_TFTNAVDescription()
	{
        StartCoroutine(GetTexture("Target10_ExplanationBouton20"));
	}

    public void Rav4SW_RETURNTRIPDescription()
	{
        StartCoroutine(GetTexture("Target11_ExplanationBouton21-22"));
	}

    public void Rav4SW_PRECOLLISIONDescription()
	{
        StartCoroutine(GetTexture("Target12_ExplanationBouton23"));
	}

    public void Rav4EmergencyStopDescription()
    {
        StartCoroutine(GetTexture("Target14_ExplanationBouton27"));
    }

    public void Rav4ParkingAssistanceDescription()
	{
        StartCoroutine(GetTexture("Target15_ExplanationBouton28"));
	}

    public void Rav4StabilityControlDescription()
	{
        StartCoroutine(GetTexture("Target17_ExplanationButton33"));
	}

    public void Rav4EnginePowerDescription()
	{
        StartCoroutine(GetTexture("Target18_ExplanationButton34"));
	}

    public void Rav4AC_AUTO_OFFDescription()
	{
        StartCoroutine(GetTexture("Target19_ExplanationButton35-36-37"));
	}

    public void Rav4AC_MONO_ACDescription()
	{
        StartCoroutine(GetTexture("Target22_ExplanationButton44-45-46"));
	}

    public void Rav4AC_DEMISTERDescription()
	{
        StartCoroutine(GetTexture("Target21_ExplanationButton41-42-43"));
	}

    public void Rav4AC_INFOSCREENDescription()
	{
        StartCoroutine(GetTexture("Target23_ExplanationButton47"));
	}

    public void Rav4AC_FANSPEEDDescription()
	{
        StartCoroutine(GetTexture("Target20_ExplanationButton38-39-40"));
	}

    public void Rav4SEATHEATDescription()
	{
        StartCoroutine(GetTexture("Target25_ExplanationButton51"));
	}

    public void Rav4USBAUDIODescription()
	{
        StartCoroutine(GetTexture("Target26_ExplanationButton52"));
	}

    public void Rav412VOLTDescription()
	{
        StartCoroutine(GetTexture("Target27_ExplanationButton53"));
	}

	public void JumpToTutorial()
	{
		MySceneManager.GoToHomeScreenAndOpenTut();
	}

	public void JumpToTFTTutorial()
	{
		AppLibrary.TFTSourceScene = Application.loadedLevel;

		MySceneManager.GoToHomeScreenAndOpenTFTTut();
	}

    private IEnumerator GetTexture(string name, string pathStr = "")
    {
		Resources.UnloadAsset (descriptionTexture.mainTexture);
        Resources.UnloadUnusedAssets();

        string path = "";
        if (pathStr.Length < 1)
        {
            path = string.Format("{0}/{1}", "Augmented Reality/Button description Screen/Rav4/SeparateDescriptions", name);
        }
        else { path = string.Format("{0}/{1}", pathStr, name); }

        ResourceRequest resourceRequest = Resources.LoadAsync<Texture2D>(path);
        yield return resourceRequest;

        descriptionTexture.mainTexture = resourceRequest.asset as Texture2D;
		descriptionTexture.gameObject.SetActive(true);
        ShowDescription();

        yield return new WaitForSeconds(0.5f);
        descriptionsBackground.GetComponent<TweenScale>().PlayForward();
    }
}
