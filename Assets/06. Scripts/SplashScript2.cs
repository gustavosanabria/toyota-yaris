﻿using UnityEngine;
using System.Collections;

public class SplashScript2 : MonoBehaviour
{
    void Start()
    {
#if !UNITY_EDITOR
        
#else
        ChangeScene();
#endif
    }

    void ChangeScene()
    {
        MySceneManager.GoToHomeScreen(true);
    }
}
