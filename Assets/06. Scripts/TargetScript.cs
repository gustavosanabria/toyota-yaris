﻿using UnityEngine;
using System.Collections;

public class TargetScript : MonoBehaviour
{

	Transform target;

	void Awake ()
	{
		target = Camera.main.GetComponent<Transform>();
	}

	void Update ()
	{
		transform.LookAt(target.position);
		transform.forward *=-1;
	}
}
