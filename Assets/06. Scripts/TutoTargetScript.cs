﻿using UnityEngine;
using System.Collections;

public class TutoTargetScript : MonoBehaviour
{
	[SerializeField] Sprite targetHL;
	Animator an;
	TweenScale tS;
	Vector3 initSize;
	
	void Awake()
	{
		initSize = transform.localScale;

		an = GetComponent<Animator>();

		tS = GetComponent<TweenScale>();

		tS.from = initSize;
		tS.to = initSize * .7f;
	}

	public void ChangeTargetTexture()
	{
		an.enabled = false;
		GetComponent<SpriteRenderer>().sprite = targetHL;
	}

	public void Reset()
	{
		an.enabled = true;
	}
}
