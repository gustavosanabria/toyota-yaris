﻿using UnityEngine;
using System.Collections;

public class VRModeManager : MonoBehaviour
{
	[SerializeField] GameObject blackQuad;
	[SerializeField] GameObject vrContainer;

    [SerializeField] bool loadSkybox;

	[SerializeField] GameObject returnToGlossaryBtn;

	void Awake()
	{
		returnToGlossaryBtn.SetActive (GlossaryMgr.vrTargetsToShow.Count > 0);

        if (loadSkybox)
        {
            print("Loading Skybox...");
            SkyboxHelper.LoadFromResources();
        }
    }

	public void SetVROnSplitMode()
	{
		EnableVR(true);
	}

	public void SetVROnFullMode()
	{
		EnableVR(false);
	}

	void EnableVR(bool enableSplitMode)
	{
		vrContainer.GetComponent<Cardboard>().VRModeEnabled = enableSplitMode;
		vrContainer.SetActive(true);
		blackQuad.SetActive(false);
	}
}
