﻿using UnityEngine;
using System.Collections;

public class SkyboxHelper
{
    private static string _skybox_HD = "skyboxVR@4096";
    private static string _skybox_Default = "skyboxVR@2048";

    public static void LoadFromResources()
    {
        string skybox_name = (TextureHelper.GetMaxTextureSize() > 2048) ? _skybox_HD : _skybox_Default;
        Debug.Log("Loading skybox " + skybox_name);

        RenderSettings.skybox = Resources.Load<Material>(skybox_name);
    }
}
