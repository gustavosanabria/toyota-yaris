// ***********************************************************
// Written by Heyworks Unity Studio http://unity.heyworks.com/
// ***********************************************************
// 
// History:
// - Modified by IGS Mobile and Creative Space Lab
//
using UnityEngine;

/// <summary>
/// Gyroscope controller that works with landscape left orientation
/// </summary>
public class GyroscopeControllerFullFreedom : MonoBehaviour
{
    private readonly Quaternion baseIdentity = Quaternion.Euler(90, 0, 0);
    private Quaternion referenceRotation = Quaternion.identity;

    private Quaternion cameraBase = Quaternion.identity;
    private Quaternion calibration = Quaternion.identity;
    private Quaternion baseOrientation = Quaternion.Euler(90, 0, 0);
 	
    public bool m_gyroEnable = true; 


    private Quaternion originalRotation;

#if UNITY_ANDROID
    private float sensitivityX = 0.2F;	//original: .75f
	private float sensitivityY = 0.2F; //original: .75f
#else
	private float sensitivityX = 0.5F;
	private float sensitivityY = 0.5F;
#endif

    private float minimumX = -360F;
    private float maximumX = 360F;

    private float minimumY = -60F;
    private float maximumY = 60F;

    private float rotationY = 0F;
    private float rotationX = 0F;    

    protected void Start()
    {
        if(m_gyroEnable)
            AttachGyro();
    }

    public void UpdateRotationGyro()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Moved)
            {
                Quaternion oldRotation = transform.localRotation;

                Quaternion gyroRotation = GetGyroRotation();
                Quaternion modifiedRotation = Quaternion.Euler(gyroRotation.eulerAngles.x, transform.eulerAngles.y - touch.deltaPosition.x, gyroRotation.eulerAngles.z);
                Quaternion newRotation = Quaternion.Slerp(oldRotation, modifiedRotation, sensitivityX);
                transform.localRotation = newRotation;
                CalibrateGyro();
            }
        }
        else
        {
            transform.rotation = GetGyroRotation();
            CalibrateGyro();
        }
    }
    public void UpdateRotationUser()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Moved)
            {
                rotationX += touch.deltaPosition.x * sensitivityX;
                rotationY += touch.deltaPosition.y * sensitivityY;

                rotationX = ClampAngle(rotationX, minimumX, maximumX);
                rotationY = ClampAngle(rotationY, minimumY, maximumY);

                Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
                Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, -Vector3.right);

                transform.localRotation = ConvertRotation(referenceRotation * originalRotation * xQuaternion * yQuaternion);
            }
        }
    }

    protected void Update()
    {
        if (m_gyroEnable) UpdateRotationGyro();               
        else UpdateRotationUser();
        UpdateZoom();
    }

    public void UpdateZoom()
    {
        if (Input.touchCount == 2)
        {
            Touch t1 = Input.GetTouch(0);
            Touch t2 = Input.GetTouch(1);
            SmoothZoom(t1, t2);
        }
    }

    public Quaternion GetGyroRotation()
    {
        Quaternion GyroRotation = cameraBase * (ConvertRotation(referenceRotation * Input.gyro.attitude)); 
        return GyroRotation;
    }

    public float m_fovMin = 35f;
    public float m_fovMax = 59f;

    void SmoothZoom(Touch touchZero, Touch touchOne)
	{
		float perspectiveZoomSpeed= 0.1f;
		// The rate of change of the field of view in perspective mode.
		// Find the position in the previous frame of each touch.
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		
		// Find the magnitude of the vector (the distance) between the touches in each frame.
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		
		// Find the difference in the distances between each frame.
		float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
		
		// fov between 35 and 59
		float fov = Camera.main.fieldOfView;
		fov += deltaMagnitudeDiff * perspectiveZoomSpeed;
		//fov = Mathf.Clamp(fov, 35.0f, 75.0f);
		fov = Mathf.Clamp(fov, m_fovMin, m_fovMax);
		
		Camera.main.fieldOfView = fov;
	}
   
	/// <summary>
    /// Attaches gyro controller to the transform.
    /// </summary>
    private void AttachGyro()
    {
        Input.gyro.enabled = true;
        m_gyroEnable = Input.gyro.enabled;
 
        if (m_gyroEnable) CalibrateGyro();


		originalRotation = transform.localRotation;
    }

	private void CalibrateGyro()
	{
		UpdateCalibration();
		UpdateCameraBaseRotation();
		RecalculateReferenceRotation();
	}

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    /// <summary>
    /// Update the gyro calibration.
    /// </summary>
    private void UpdateCalibration()
    {
        var fw = (Input.gyro.attitude) * (-Vector3.forward);
        fw.z = 0;
        if (fw == Vector3.zero)
        {
            calibration = Quaternion.identity;
        }
        else
        {
            calibration = (Quaternion.FromToRotation(Quaternion.identity * Vector3.up, fw));
        }
    }

    /// <summary>
    /// Update the camera base rotation.
    /// </summary>
    /// <param name='onlyHorizontal'>
    /// Only y rotation.
    /// </param>
    private void UpdateCameraBaseRotation()
    {
        var fw = transform.forward;
        fw.y = 0;
        if (fw == Vector3.zero)
        {
            cameraBase = Quaternion.identity;
        }
        else
        {
            cameraBase = Quaternion.FromToRotation(Vector3.forward, fw);
        }
    }

    /// Converts the rotation from right handed to left handed.
    private Quaternion ConvertRotation(Quaternion q)
    {
		return new Quaternion(q.x, q.y, -q.z, -q.w);
    }


    private void RecalculateReferenceRotation()
    {
        referenceRotation = Quaternion.Inverse(baseOrientation) * Quaternion.Inverse(calibration);
    }
}
