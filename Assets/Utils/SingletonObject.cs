using UnityEngine;
using System.Collections;

namespace Utils
{
    public abstract class SingletonObject<_Ty> : MonoBehaviour where _Ty : SingletonObject<_Ty>
    {
        private static _Ty _instance = null;


        public static _Ty instance
        {
            get
            {
                if (null == _instance)
                {
                    // get the name
                    string name = typeof(_Ty).ToString();

                    // check is exists as a gameobject
                    _instance = FindObjectOfType<_Ty>();

                    if (null == _instance)
                    {
                        // create a gameObject
                        var _go = new GameObject(name);

                        _instance = _go.AddComponent<_Ty>();
                    }

                }

                return _instance;
            }
        }

        public static _Ty Get() { return instance; }

        public static void DestroyInstance()
        {
            // release reference count to object
            _instance = null;
        }

    }
}
