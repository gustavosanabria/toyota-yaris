﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// EventDelegate/Parameter
struct Parameter_t2566940569;
// EventDelegate
struct EventDelegate_t4004424223;
// TweenScale
struct TweenScale_t2936666559;
// UITweener
struct UITweener_t105489188;
// CardboardOnGUIWindow
struct CardboardOnGUIWindow_t3958675030;
// CardboardEye
struct CardboardEye_t2174202011;
// UIButton
struct UIButton_t179429094;
// UIButtonColor
struct UIButtonColor_t1546108957;
// UIWidgetContainer
struct UIWidgetContainer_t1520767337;
// UIDragDropItem
struct UIDragDropItem_t2087865514;
// UIKeyBinding
struct UIKeyBinding_t2313833690;
// UIKeyNavigation
struct UIKeyNavigation_t1837256607;
// UILabel
struct UILabel_t291504320;
// UIPlaySound
struct UIPlaySound_t532605447;
// UIWidget
struct UIWidget_t769069560;
// UIRect
struct UIRect_t2503437976;
// UIToggle
struct UIToggle_t688812808;
// UIScrollView
struct UIScrollView_t2113479878;
// UIPanel
struct UIPanel_t295209936;
// BMGlyph
struct BMGlyph_t719052705;
// UICamera
struct UICamera_t189364953;
// UIRoot
struct UIRoot_t2503447958;
// UIDrawCall
struct UIDrawCall_t913273974;
// UISpriteData
struct UISpriteData_t3578345923;
// UIAtlas/Sprite
struct Sprite_t2668923293;
// UISprite
struct UISprite_t661437049;
// UIBasicSprite
struct UIBasicSprite_t2501337439;
// UIFont
struct UIFont_t2503090435;
// UICamera/MouseOrTouch
struct MouseOrTouch_t2057376333;
// BMSymbol
struct BMSymbol_t1170982339;
// UITextList/Paragraph
struct Paragraph_t3894573406;
// BetterList`1<UITextList/Paragraph>
struct BetterList_1_t1096574122;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t433318935;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter2566940569.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_TweenScale2936666559.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"
#include "AssemblyU2DCSharp_CardboardOnGUIWindow3958675030.h"
#include "AssemblyU2DCSharp_CardboardEye2174202011.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry2858472101.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "AssemblyU2DCSharp_UIButtonColor1546108957.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337.h"
#include "AssemblyU2DCSharp_UIDragDropItem2087865514.h"
#include "AssemblyU2DCSharp_UIKeyBinding2313833690.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1837256607.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIPlaySound532605447.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_BMGlyph719052705.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "AssemblyU2DCSharp_UIAtlas_Sprite2668923293.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch2057376333.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph3894573406.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1096574122.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour433318935.h"

#pragma once
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t206664164  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Parameter_t2566940569 * m_Items[1];

public:
	inline Parameter_t2566940569 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Parameter_t2566940569 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Parameter_t2566940569 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EventDelegate[]
struct EventDelegateU5BU5D_t1029252742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventDelegate_t4004424223 * m_Items[1];

public:
	inline EventDelegate_t4004424223 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventDelegate_t4004424223 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventDelegate_t4004424223 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TweenScale[]
struct TweenScaleU5BU5D_t3591996518  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TweenScale_t2936666559 * m_Items[1];

public:
	inline TweenScale_t2936666559 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TweenScale_t2936666559 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TweenScale_t2936666559 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UITweener[]
struct UITweenerU5BU5D_t996366285  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UITweener_t105489188 * m_Items[1];

public:
	inline UITweener_t105489188 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UITweener_t105489188 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UITweener_t105489188 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Tutorial[]
struct TutorialU5BU5D_t2291760331  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Tutorial_t257920894  m_Items[1];

public:
	inline Tutorial_t257920894  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tutorial_t257920894 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tutorial_t257920894  value)
	{
		m_Items[index] = value;
	}
};
// CardboardOnGUIWindow[]
struct CardboardOnGUIWindowU5BU5D_t3646621587  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CardboardOnGUIWindow_t3958675030 * m_Items[1];

public:
	inline CardboardOnGUIWindow_t3958675030 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CardboardOnGUIWindow_t3958675030 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CardboardOnGUIWindow_t3958675030 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CardboardEye[]
struct CardboardEyeU5BU5D_t1172581018  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CardboardEye_t2174202011 * m_Items[1];

public:
	inline CardboardEye_t2174202011 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CardboardEye_t2174202011 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CardboardEye_t2174202011 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TypewriterEffect/FadeEntry[]
struct FadeEntryU5BU5D_t3343031592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FadeEntry_t2858472101  m_Items[1];

public:
	inline FadeEntry_t2858472101  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FadeEntry_t2858472101 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FadeEntry_t2858472101  value)
	{
		m_Items[index] = value;
	}
};
// UIButton[]
struct UIButtonU5BU5D_t2312321731  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIButton_t179429094 * m_Items[1];

public:
	inline UIButton_t179429094 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIButton_t179429094 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIButton_t179429094 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIButtonColor[]
struct UIButtonColorU5BU5D_t3320165072  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIButtonColor_t1546108957 * m_Items[1];

public:
	inline UIButtonColor_t1546108957 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIButtonColor_t1546108957 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIButtonColor_t1546108957 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIWidgetContainer[]
struct UIWidgetContainerU5BU5D_t2093242068  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIWidgetContainer_t1520767337 * m_Items[1];

public:
	inline UIWidgetContainer_t1520767337 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIWidgetContainer_t1520767337 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIWidgetContainer_t1520767337 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIDragDropItem[]
struct UIDragDropItemU5BU5D_t1078929775  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIDragDropItem_t2087865514 * m_Items[1];

public:
	inline UIDragDropItem_t2087865514 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIDragDropItem_t2087865514 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIDragDropItem_t2087865514 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIKeyBinding[]
struct UIKeyBindingU5BU5D_t620081791  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIKeyBinding_t2313833690 * m_Items[1];

public:
	inline UIKeyBinding_t2313833690 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIKeyBinding_t2313833690 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIKeyBinding_t2313833690 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIKeyNavigation[]
struct UIKeyNavigationU5BU5D_t1243790086  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIKeyNavigation_t1837256607 * m_Items[1];

public:
	inline UIKeyNavigation_t1837256607 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIKeyNavigation_t1837256607 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIKeyNavigation_t1837256607 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UILabel[]
struct UILabelU5BU5D_t1494885441  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UILabel_t291504320 * m_Items[1];

public:
	inline UILabel_t291504320 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UILabel_t291504320 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UILabel_t291504320 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIPlaySound[]
struct UIPlaySoundU5BU5D_t1195418110  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIPlaySound_t532605447 * m_Items[1];

public:
	inline UIPlaySound_t532605447 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIPlaySound_t532605447 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIPlaySound_t532605447 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIWidget[]
struct UIWidgetU5BU5D_t4236988201  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIWidget_t769069560 * m_Items[1];

public:
	inline UIWidget_t769069560 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIWidget_t769069560 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIWidget_t769069560 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIRect[]
struct UIRectU5BU5D_t1524091913  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIRect_t2503437976 * m_Items[1];

public:
	inline UIRect_t2503437976 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIRect_t2503437976 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIRect_t2503437976 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIToggle[]
struct UIToggleU5BU5D_t2046261209  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIToggle_t688812808 * m_Items[1];

public:
	inline UIToggle_t688812808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIToggle_t688812808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIToggle_t688812808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIScrollView[]
struct UIScrollViewU5BU5D_t4274241891  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIScrollView_t2113479878 * m_Items[1];

public:
	inline UIScrollView_t2113479878 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIScrollView_t2113479878 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIScrollView_t2113479878 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIPanel[]
struct UIPanelU5BU5D_t3742972657  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIPanel_t295209936 * m_Items[1];

public:
	inline UIPanel_t295209936 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIPanel_t295209936 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIPanel_t295209936 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BMGlyph[]
struct BMGlyphU5BU5D_t3436589244  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BMGlyph_t719052705 * m_Items[1];

public:
	inline BMGlyph_t719052705 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BMGlyph_t719052705 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BMGlyph_t719052705 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera[]
struct UICameraU5BU5D_t3682822564  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UICamera_t189364953 * m_Items[1];

public:
	inline UICamera_t189364953 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UICamera_t189364953 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UICamera_t189364953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIRoot[]
struct UIRootU5BU5D_t1337058131  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIRoot_t2503447958 * m_Items[1];

public:
	inline UIRoot_t2503447958 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIRoot_t2503447958 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIRoot_t2503447958 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIDrawCall[]
struct UIDrawCallU5BU5D_t3158789363  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIDrawCall_t913273974 * m_Items[1];

public:
	inline UIDrawCall_t913273974 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIDrawCall_t913273974 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIDrawCall_t913273974 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UISpriteData[]
struct UISpriteDataU5BU5D_t2292174802  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UISpriteData_t3578345923 * m_Items[1];

public:
	inline UISpriteData_t3578345923 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UISpriteData_t3578345923 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UISpriteData_t3578345923 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIAtlas/Sprite[]
struct SpriteU5BU5D_t2767786832  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Sprite_t2668923293 * m_Items[1];

public:
	inline Sprite_t2668923293 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Sprite_t2668923293 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Sprite_t2668923293 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UISprite[]
struct UISpriteU5BU5D_t3727562628  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UISprite_t661437049 * m_Items[1];

public:
	inline UISprite_t661437049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UISprite_t661437049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UISprite_t661437049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIBasicSprite[]
struct UIBasicSpriteU5BU5D_t3513018950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIBasicSprite_t2501337439 * m_Items[1];

public:
	inline UIBasicSprite_t2501337439 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIBasicSprite_t2501337439 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIBasicSprite_t2501337439 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIFont[]
struct UIFontU5BU5D_t3954451346  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIFont_t2503090435 * m_Items[1];

public:
	inline UIFont_t2503090435 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIFont_t2503090435 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIFont_t2503090435 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t546398688  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MouseOrTouch_t2057376333 * m_Items[1];

public:
	inline MouseOrTouch_t2057376333 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MouseOrTouch_t2057376333 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MouseOrTouch_t2057376333 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera/DepthEntry[]
struct DepthEntryU5BU5D_t1472539592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DepthEntry_t1145614469  m_Items[1];

public:
	inline DepthEntry_t1145614469  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DepthEntry_t1145614469 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DepthEntry_t1145614469  value)
	{
		m_Items[index] = value;
	}
};
// BMSymbol[]
struct BMSymbolU5BU5D_t484807634  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BMSymbol_t1170982339 * m_Items[1];

public:
	inline BMSymbol_t1170982339 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BMSymbol_t1170982339 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BMSymbol_t1170982339 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UITextList/Paragraph[]
struct ParagraphU5BU5D_t873942891  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Paragraph_t3894573406 * m_Items[1];

public:
	inline Paragraph_t3894573406 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Paragraph_t3894573406 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Paragraph_t3894573406 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BetterList`1<UITextList/Paragraph>[]
struct BetterList_1U5BU5D_t1203876207  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BetterList_1_t1096574122 * m_Items[1];

public:
	inline BetterList_1_t1096574122 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BetterList_1_t1096574122 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BetterList_1_t1096574122 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t1176995246  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t433318935 * m_Items[1];

public:
	inline WireframeBehaviour_t433318935 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WireframeBehaviour_t433318935 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t433318935 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
