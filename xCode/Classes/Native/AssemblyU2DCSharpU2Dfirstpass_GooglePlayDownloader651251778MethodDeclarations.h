﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayDownloader
struct GooglePlayDownloader_t651251778;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayDownloader::.ctor()
extern "C"  void GooglePlayDownloader__ctor_m2726240589 (GooglePlayDownloader_t651251778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
