﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkyboxHelper
struct  SkyboxHelper_t1246069112  : public Il2CppObject
{
public:

public:
};

struct SkyboxHelper_t1246069112_StaticFields
{
public:
	// System.String SkyboxHelper::_skybox_HD
	String_t* ____skybox_HD_0;
	// System.String SkyboxHelper::_skybox_Default
	String_t* ____skybox_Default_1;

public:
	inline static int32_t get_offset_of__skybox_HD_0() { return static_cast<int32_t>(offsetof(SkyboxHelper_t1246069112_StaticFields, ____skybox_HD_0)); }
	inline String_t* get__skybox_HD_0() const { return ____skybox_HD_0; }
	inline String_t** get_address_of__skybox_HD_0() { return &____skybox_HD_0; }
	inline void set__skybox_HD_0(String_t* value)
	{
		____skybox_HD_0 = value;
		Il2CppCodeGenWriteBarrier(&____skybox_HD_0, value);
	}

	inline static int32_t get_offset_of__skybox_Default_1() { return static_cast<int32_t>(offsetof(SkyboxHelper_t1246069112_StaticFields, ____skybox_Default_1)); }
	inline String_t* get__skybox_Default_1() const { return ____skybox_Default_1; }
	inline String_t** get_address_of__skybox_Default_1() { return &____skybox_Default_1; }
	inline void set__skybox_Default_1(String_t* value)
	{
		____skybox_Default_1 = value;
		Il2CppCodeGenWriteBarrier(&____skybox_Default_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
