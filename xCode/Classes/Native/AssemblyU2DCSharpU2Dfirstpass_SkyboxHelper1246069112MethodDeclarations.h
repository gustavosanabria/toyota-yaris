﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkyboxHelper
struct SkyboxHelper_t1246069112;

#include "codegen/il2cpp-codegen.h"

// System.Void SkyboxHelper::.ctor()
extern "C"  void SkyboxHelper__ctor_m2266069207 (SkyboxHelper_t1246069112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxHelper::.cctor()
extern "C"  void SkyboxHelper__cctor_m1046572470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxHelper::LoadFromResources()
extern "C"  void SkyboxHelper_LoadFromResources_m3614614794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
