﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextureHelper
struct TextureHelper_t764444809;

#include "codegen/il2cpp-codegen.h"

// System.Void TextureHelper::.ctor()
extern "C"  void TextureHelper__ctor_m1470275790 (TextureHelper_t764444809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TextureHelper::GetMaxTextureSize()
extern "C"  int32_t TextureHelper_GetMaxTextureSize_m760661776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
