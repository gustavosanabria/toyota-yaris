﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.DataSet
struct DataSet_t2095838082;
// Vuforia.ObjectTracker
struct ObjectTracker_t455954211;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARController
struct  ARController_t1110212269  : public MonoBehaviour_t667441552
{
public:
	// Vuforia.DataSet ARController::mDataset
	DataSet_t2095838082 * ___mDataset_2;
	// Vuforia.ObjectTracker ARController::tracker
	ObjectTracker_t455954211 * ___tracker_3;
	// System.Boolean ARController::_loaded
	bool ____loaded_4;

public:
	inline static int32_t get_offset_of_mDataset_2() { return static_cast<int32_t>(offsetof(ARController_t1110212269, ___mDataset_2)); }
	inline DataSet_t2095838082 * get_mDataset_2() const { return ___mDataset_2; }
	inline DataSet_t2095838082 ** get_address_of_mDataset_2() { return &___mDataset_2; }
	inline void set_mDataset_2(DataSet_t2095838082 * value)
	{
		___mDataset_2 = value;
		Il2CppCodeGenWriteBarrier(&___mDataset_2, value);
	}

	inline static int32_t get_offset_of_tracker_3() { return static_cast<int32_t>(offsetof(ARController_t1110212269, ___tracker_3)); }
	inline ObjectTracker_t455954211 * get_tracker_3() const { return ___tracker_3; }
	inline ObjectTracker_t455954211 ** get_address_of_tracker_3() { return &___tracker_3; }
	inline void set_tracker_3(ObjectTracker_t455954211 * value)
	{
		___tracker_3 = value;
		Il2CppCodeGenWriteBarrier(&___tracker_3, value);
	}

	inline static int32_t get_offset_of__loaded_4() { return static_cast<int32_t>(offsetof(ARController_t1110212269, ____loaded_4)); }
	inline bool get__loaded_4() const { return ____loaded_4; }
	inline bool* get_address_of__loaded_4() { return &____loaded_4; }
	inline void set__loaded_4(bool value)
	{
		____loaded_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
