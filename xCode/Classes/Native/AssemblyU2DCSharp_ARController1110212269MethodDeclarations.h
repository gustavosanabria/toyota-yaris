﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARController
struct ARController_t1110212269;

#include "codegen/il2cpp-codegen.h"

// System.Void ARController::.ctor()
extern "C"  void ARController__ctor_m845850254 (ARController_t1110212269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::Update()
extern "C"  void ARController_Update_m2178416191 (ARController_t1110212269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
