﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t3684358292;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARHelpText
struct  ARHelpText_t1498983263  : public MonoBehaviour_t667441552
{
public:
	// TweenPosition ARHelpText::twS
	TweenPosition_t3684358292 * ___twS_2;
	// UnityEngine.Texture ARHelpText::helpTex
	Texture_t2526458961 * ___helpTex_3;
	// UnityEngine.Rect ARHelpText::helpRect
	Rect_t4241904616  ___helpRect_4;
	// System.Boolean ARHelpText::slideHelp
	bool ___slideHelp_5;

public:
	inline static int32_t get_offset_of_twS_2() { return static_cast<int32_t>(offsetof(ARHelpText_t1498983263, ___twS_2)); }
	inline TweenPosition_t3684358292 * get_twS_2() const { return ___twS_2; }
	inline TweenPosition_t3684358292 ** get_address_of_twS_2() { return &___twS_2; }
	inline void set_twS_2(TweenPosition_t3684358292 * value)
	{
		___twS_2 = value;
		Il2CppCodeGenWriteBarrier(&___twS_2, value);
	}

	inline static int32_t get_offset_of_helpTex_3() { return static_cast<int32_t>(offsetof(ARHelpText_t1498983263, ___helpTex_3)); }
	inline Texture_t2526458961 * get_helpTex_3() const { return ___helpTex_3; }
	inline Texture_t2526458961 ** get_address_of_helpTex_3() { return &___helpTex_3; }
	inline void set_helpTex_3(Texture_t2526458961 * value)
	{
		___helpTex_3 = value;
		Il2CppCodeGenWriteBarrier(&___helpTex_3, value);
	}

	inline static int32_t get_offset_of_helpRect_4() { return static_cast<int32_t>(offsetof(ARHelpText_t1498983263, ___helpRect_4)); }
	inline Rect_t4241904616  get_helpRect_4() const { return ___helpRect_4; }
	inline Rect_t4241904616 * get_address_of_helpRect_4() { return &___helpRect_4; }
	inline void set_helpRect_4(Rect_t4241904616  value)
	{
		___helpRect_4 = value;
	}

	inline static int32_t get_offset_of_slideHelp_5() { return static_cast<int32_t>(offsetof(ARHelpText_t1498983263, ___slideHelp_5)); }
	inline bool get_slideHelp_5() const { return ___slideHelp_5; }
	inline bool* get_address_of_slideHelp_5() { return &___slideHelp_5; }
	inline void set_slideHelp_5(bool value)
	{
		___slideHelp_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
