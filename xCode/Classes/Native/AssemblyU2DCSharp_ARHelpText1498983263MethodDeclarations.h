﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARHelpText
struct ARHelpText_t1498983263;

#include "codegen/il2cpp-codegen.h"

// System.Void ARHelpText::.ctor()
extern "C"  void ARHelpText__ctor_m4215556124 (ARHelpText_t1498983263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARHelpText::Awake()
extern "C"  void ARHelpText_Awake_m158194047 (ARHelpText_t1498983263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARHelpText::AdjustTweenPosition()
extern "C"  void ARHelpText_AdjustTweenPosition_m738210207 (ARHelpText_t1498983263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARHelpText::FixedUpdate()
extern "C"  void ARHelpText_FixedUpdate_m3626410135 (ARHelpText_t1498983263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARHelpText::OnGUI()
extern "C"  void ARHelpText_OnGUI_m3710954774 (ARHelpText_t1498983263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
