﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppLibrary
struct  AppLibrary_t3292178298  : public MonoBehaviour_t667441552
{
public:

public:
};

struct AppLibrary_t3292178298_StaticFields
{
public:
	// System.Byte AppLibrary::AppTYPE
	uint8_t ___AppTYPE_26;
	// System.Byte AppLibrary::sceneLoad
	uint8_t ___sceneLoad_27;
	// System.Boolean AppLibrary::openTutAtomatically
	bool ___openTutAtomatically_28;
	// System.Boolean AppLibrary::openTFTTutAtomatically
	bool ___openTFTTutAtomatically_29;
	// System.String AppLibrary::TUT_TO_OPEN
	String_t* ___TUT_TO_OPEN_30;
	// System.String AppLibrary::LASTDEF
	String_t* ___LASTDEF_31;
	// System.Int32 AppLibrary::TFTSourceScene
	int32_t ___TFTSourceScene_32;
	// System.String AppLibrary::CURRENTVIDEONAME
	String_t* ___CURRENTVIDEONAME_33;
	// System.String AppLibrary::VIDEO_HELP_AR
	String_t* ___VIDEO_HELP_AR_34;
	// System.String AppLibrary::VIDEO_VR
	String_t* ___VIDEO_VR_35;
	// System.Boolean AppLibrary::AR_HELPVIDEO_PLAYED
	bool ___AR_HELPVIDEO_PLAYED_36;
	// System.String AppLibrary::APPKEY
	String_t* ___APPKEY_37;

public:
	inline static int32_t get_offset_of_AppTYPE_26() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___AppTYPE_26)); }
	inline uint8_t get_AppTYPE_26() const { return ___AppTYPE_26; }
	inline uint8_t* get_address_of_AppTYPE_26() { return &___AppTYPE_26; }
	inline void set_AppTYPE_26(uint8_t value)
	{
		___AppTYPE_26 = value;
	}

	inline static int32_t get_offset_of_sceneLoad_27() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___sceneLoad_27)); }
	inline uint8_t get_sceneLoad_27() const { return ___sceneLoad_27; }
	inline uint8_t* get_address_of_sceneLoad_27() { return &___sceneLoad_27; }
	inline void set_sceneLoad_27(uint8_t value)
	{
		___sceneLoad_27 = value;
	}

	inline static int32_t get_offset_of_openTutAtomatically_28() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___openTutAtomatically_28)); }
	inline bool get_openTutAtomatically_28() const { return ___openTutAtomatically_28; }
	inline bool* get_address_of_openTutAtomatically_28() { return &___openTutAtomatically_28; }
	inline void set_openTutAtomatically_28(bool value)
	{
		___openTutAtomatically_28 = value;
	}

	inline static int32_t get_offset_of_openTFTTutAtomatically_29() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___openTFTTutAtomatically_29)); }
	inline bool get_openTFTTutAtomatically_29() const { return ___openTFTTutAtomatically_29; }
	inline bool* get_address_of_openTFTTutAtomatically_29() { return &___openTFTTutAtomatically_29; }
	inline void set_openTFTTutAtomatically_29(bool value)
	{
		___openTFTTutAtomatically_29 = value;
	}

	inline static int32_t get_offset_of_TUT_TO_OPEN_30() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___TUT_TO_OPEN_30)); }
	inline String_t* get_TUT_TO_OPEN_30() const { return ___TUT_TO_OPEN_30; }
	inline String_t** get_address_of_TUT_TO_OPEN_30() { return &___TUT_TO_OPEN_30; }
	inline void set_TUT_TO_OPEN_30(String_t* value)
	{
		___TUT_TO_OPEN_30 = value;
		Il2CppCodeGenWriteBarrier(&___TUT_TO_OPEN_30, value);
	}

	inline static int32_t get_offset_of_LASTDEF_31() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___LASTDEF_31)); }
	inline String_t* get_LASTDEF_31() const { return ___LASTDEF_31; }
	inline String_t** get_address_of_LASTDEF_31() { return &___LASTDEF_31; }
	inline void set_LASTDEF_31(String_t* value)
	{
		___LASTDEF_31 = value;
		Il2CppCodeGenWriteBarrier(&___LASTDEF_31, value);
	}

	inline static int32_t get_offset_of_TFTSourceScene_32() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___TFTSourceScene_32)); }
	inline int32_t get_TFTSourceScene_32() const { return ___TFTSourceScene_32; }
	inline int32_t* get_address_of_TFTSourceScene_32() { return &___TFTSourceScene_32; }
	inline void set_TFTSourceScene_32(int32_t value)
	{
		___TFTSourceScene_32 = value;
	}

	inline static int32_t get_offset_of_CURRENTVIDEONAME_33() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___CURRENTVIDEONAME_33)); }
	inline String_t* get_CURRENTVIDEONAME_33() const { return ___CURRENTVIDEONAME_33; }
	inline String_t** get_address_of_CURRENTVIDEONAME_33() { return &___CURRENTVIDEONAME_33; }
	inline void set_CURRENTVIDEONAME_33(String_t* value)
	{
		___CURRENTVIDEONAME_33 = value;
		Il2CppCodeGenWriteBarrier(&___CURRENTVIDEONAME_33, value);
	}

	inline static int32_t get_offset_of_VIDEO_HELP_AR_34() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___VIDEO_HELP_AR_34)); }
	inline String_t* get_VIDEO_HELP_AR_34() const { return ___VIDEO_HELP_AR_34; }
	inline String_t** get_address_of_VIDEO_HELP_AR_34() { return &___VIDEO_HELP_AR_34; }
	inline void set_VIDEO_HELP_AR_34(String_t* value)
	{
		___VIDEO_HELP_AR_34 = value;
		Il2CppCodeGenWriteBarrier(&___VIDEO_HELP_AR_34, value);
	}

	inline static int32_t get_offset_of_VIDEO_VR_35() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___VIDEO_VR_35)); }
	inline String_t* get_VIDEO_VR_35() const { return ___VIDEO_VR_35; }
	inline String_t** get_address_of_VIDEO_VR_35() { return &___VIDEO_VR_35; }
	inline void set_VIDEO_VR_35(String_t* value)
	{
		___VIDEO_VR_35 = value;
		Il2CppCodeGenWriteBarrier(&___VIDEO_VR_35, value);
	}

	inline static int32_t get_offset_of_AR_HELPVIDEO_PLAYED_36() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___AR_HELPVIDEO_PLAYED_36)); }
	inline bool get_AR_HELPVIDEO_PLAYED_36() const { return ___AR_HELPVIDEO_PLAYED_36; }
	inline bool* get_address_of_AR_HELPVIDEO_PLAYED_36() { return &___AR_HELPVIDEO_PLAYED_36; }
	inline void set_AR_HELPVIDEO_PLAYED_36(bool value)
	{
		___AR_HELPVIDEO_PLAYED_36 = value;
	}

	inline static int32_t get_offset_of_APPKEY_37() { return static_cast<int32_t>(offsetof(AppLibrary_t3292178298_StaticFields, ___APPKEY_37)); }
	inline String_t* get_APPKEY_37() const { return ___APPKEY_37; }
	inline String_t** get_address_of_APPKEY_37() { return &___APPKEY_37; }
	inline void set_APPKEY_37(String_t* value)
	{
		___APPKEY_37 = value;
		Il2CppCodeGenWriteBarrier(&___APPKEY_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
