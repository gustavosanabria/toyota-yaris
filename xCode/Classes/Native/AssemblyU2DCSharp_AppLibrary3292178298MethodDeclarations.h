﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppLibrary
struct AppLibrary_t3292178298;

#include "codegen/il2cpp-codegen.h"

// System.Void AppLibrary::.ctor()
extern "C"  void AppLibrary__ctor_m1285339745 (AppLibrary_t3292178298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppLibrary::.cctor()
extern "C"  void AppLibrary__cctor_m708730220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
