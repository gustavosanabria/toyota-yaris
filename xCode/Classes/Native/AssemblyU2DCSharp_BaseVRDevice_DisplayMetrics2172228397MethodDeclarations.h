﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseVRDevice/DisplayMetrics
struct DisplayMetrics_t2172228397;
struct DisplayMetrics_t2172228397_marshaled_pinvoke;
struct DisplayMetrics_t2172228397_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct DisplayMetrics_t2172228397;
struct DisplayMetrics_t2172228397_marshaled_pinvoke;

extern "C" void DisplayMetrics_t2172228397_marshal_pinvoke(const DisplayMetrics_t2172228397& unmarshaled, DisplayMetrics_t2172228397_marshaled_pinvoke& marshaled);
extern "C" void DisplayMetrics_t2172228397_marshal_pinvoke_back(const DisplayMetrics_t2172228397_marshaled_pinvoke& marshaled, DisplayMetrics_t2172228397& unmarshaled);
extern "C" void DisplayMetrics_t2172228397_marshal_pinvoke_cleanup(DisplayMetrics_t2172228397_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DisplayMetrics_t2172228397;
struct DisplayMetrics_t2172228397_marshaled_com;

extern "C" void DisplayMetrics_t2172228397_marshal_com(const DisplayMetrics_t2172228397& unmarshaled, DisplayMetrics_t2172228397_marshaled_com& marshaled);
extern "C" void DisplayMetrics_t2172228397_marshal_com_back(const DisplayMetrics_t2172228397_marshaled_com& marshaled, DisplayMetrics_t2172228397& unmarshaled);
extern "C" void DisplayMetrics_t2172228397_marshal_com_cleanup(DisplayMetrics_t2172228397_marshaled_com& marshaled);
