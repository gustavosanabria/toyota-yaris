﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardHead
struct CardboardHead_t2975823030;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

// System.Void CardboardHead::.ctor()
extern "C"  void CardboardHead__ctor_m1135860085 (CardboardHead_t2975823030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray CardboardHead::get_Gaze()
extern "C"  Ray_t3134616544  CardboardHead_get_Gaze_m209370781 (CardboardHead_t2975823030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardHead::Update()
extern "C"  void CardboardHead_Update_m2578786360 (CardboardHead_t2975823030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardHead::LateUpdate()
extern "C"  void CardboardHead_LateUpdate_m2689077758 (CardboardHead_t2975823030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardHead::UpdateHead()
extern "C"  void CardboardHead_UpdateHead_m671905464 (CardboardHead_t2975823030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
