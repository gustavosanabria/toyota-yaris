﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// CardboardOnGUI/OnGUICallback
struct OnGUICallback_t3287452568;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardOnGUI
struct  CardboardOnGUI_t2062908102  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Color CardboardOnGUI::background
	Color_t4194546905  ___background_4;
	// UnityEngine.RenderTexture CardboardOnGUI::guiScreen
	RenderTexture_t1963041563 * ___guiScreen_5;

public:
	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(CardboardOnGUI_t2062908102, ___background_4)); }
	inline Color_t4194546905  get_background_4() const { return ___background_4; }
	inline Color_t4194546905 * get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(Color_t4194546905  value)
	{
		___background_4 = value;
	}

	inline static int32_t get_offset_of_guiScreen_5() { return static_cast<int32_t>(offsetof(CardboardOnGUI_t2062908102, ___guiScreen_5)); }
	inline RenderTexture_t1963041563 * get_guiScreen_5() const { return ___guiScreen_5; }
	inline RenderTexture_t1963041563 ** get_address_of_guiScreen_5() { return &___guiScreen_5; }
	inline void set_guiScreen_5(RenderTexture_t1963041563 * value)
	{
		___guiScreen_5 = value;
		Il2CppCodeGenWriteBarrier(&___guiScreen_5, value);
	}
};

struct CardboardOnGUI_t2062908102_StaticFields
{
public:
	// System.Boolean CardboardOnGUI::okToDraw
	bool ___okToDraw_2;
	// System.Boolean CardboardOnGUI::isGUIVisible
	bool ___isGUIVisible_3;
	// CardboardOnGUI/OnGUICallback CardboardOnGUI::onGUICallback
	OnGUICallback_t3287452568 * ___onGUICallback_6;

public:
	inline static int32_t get_offset_of_okToDraw_2() { return static_cast<int32_t>(offsetof(CardboardOnGUI_t2062908102_StaticFields, ___okToDraw_2)); }
	inline bool get_okToDraw_2() const { return ___okToDraw_2; }
	inline bool* get_address_of_okToDraw_2() { return &___okToDraw_2; }
	inline void set_okToDraw_2(bool value)
	{
		___okToDraw_2 = value;
	}

	inline static int32_t get_offset_of_isGUIVisible_3() { return static_cast<int32_t>(offsetof(CardboardOnGUI_t2062908102_StaticFields, ___isGUIVisible_3)); }
	inline bool get_isGUIVisible_3() const { return ___isGUIVisible_3; }
	inline bool* get_address_of_isGUIVisible_3() { return &___isGUIVisible_3; }
	inline void set_isGUIVisible_3(bool value)
	{
		___isGUIVisible_3 = value;
	}

	inline static int32_t get_offset_of_onGUICallback_6() { return static_cast<int32_t>(offsetof(CardboardOnGUI_t2062908102_StaticFields, ___onGUICallback_6)); }
	inline OnGUICallback_t3287452568 * get_onGUICallback_6() const { return ___onGUICallback_6; }
	inline OnGUICallback_t3287452568 ** get_address_of_onGUICallback_6() { return &___onGUICallback_6; }
	inline void set_onGUICallback_6(OnGUICallback_t3287452568 * value)
	{
		___onGUICallback_6 = value;
		Il2CppCodeGenWriteBarrier(&___onGUICallback_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
