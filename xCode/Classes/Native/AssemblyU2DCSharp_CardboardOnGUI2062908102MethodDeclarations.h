﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardOnGUI
struct CardboardOnGUI_t2062908102;
// CardboardOnGUI/OnGUICallback
struct OnGUICallback_t3287452568;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CardboardOnGUI_OnGUICallback3287452568.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

// System.Void CardboardOnGUI::.ctor()
extern "C"  void CardboardOnGUI__ctor_m1601638549 (CardboardOnGUI_t2062908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::add_onGUICallback(CardboardOnGUI/OnGUICallback)
extern "C"  void CardboardOnGUI_add_onGUICallback_m986154014 (Il2CppObject * __this /* static, unused */, OnGUICallback_t3287452568 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::remove_onGUICallback(CardboardOnGUI/OnGUICallback)
extern "C"  void CardboardOnGUI_remove_onGUICallback_m1828943005 (Il2CppObject * __this /* static, unused */, OnGUICallback_t3287452568 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardboardOnGUI::OKToDraw(UnityEngine.MonoBehaviour)
extern "C"  bool CardboardOnGUI_OKToDraw_m201202641 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t667441552 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardboardOnGUI::get_IsGUIVisible()
extern "C"  bool CardboardOnGUI_get_IsGUIVisible_m1228094989 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::set_IsGUIVisible(System.Boolean)
extern "C"  void CardboardOnGUI_set_IsGUIVisible_m752787588 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardboardOnGUI::get_Triggered()
extern "C"  bool CardboardOnGUI_get_Triggered_m3016263085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::Awake()
extern "C"  void CardboardOnGUI_Awake_m1839243768 (CardboardOnGUI_t2062908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::Start()
extern "C"  void CardboardOnGUI_Start_m548776341 (CardboardOnGUI_t2062908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::Create()
extern "C"  void CardboardOnGUI_Create_m335871115 (CardboardOnGUI_t2062908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::LateUpdate()
extern "C"  void CardboardOnGUI_LateUpdate_m2005552862 (CardboardOnGUI_t2062908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::OnGUI()
extern "C"  void CardboardOnGUI_OnGUI_m1097037199 (CardboardOnGUI_t2062908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
