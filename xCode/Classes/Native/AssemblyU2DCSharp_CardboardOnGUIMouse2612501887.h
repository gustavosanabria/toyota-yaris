﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2526458961;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardOnGUIMouse
struct  CardboardOnGUIMouse_t2612501887  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture CardboardOnGUIMouse::pointerImage
	Texture_t2526458961 * ___pointerImage_2;
	// UnityEngine.Vector2 CardboardOnGUIMouse::pointerSize
	Vector2_t4282066565  ___pointerSize_3;
	// UnityEngine.Vector2 CardboardOnGUIMouse::pointerSpot
	Vector2_t4282066565  ___pointerSpot_4;
	// System.Boolean CardboardOnGUIMouse::pointerVisible
	bool ___pointerVisible_5;
	// System.Int32 CardboardOnGUIMouse::pointerX
	int32_t ___pointerX_6;
	// System.Int32 CardboardOnGUIMouse::pointerY
	int32_t ___pointerY_7;

public:
	inline static int32_t get_offset_of_pointerImage_2() { return static_cast<int32_t>(offsetof(CardboardOnGUIMouse_t2612501887, ___pointerImage_2)); }
	inline Texture_t2526458961 * get_pointerImage_2() const { return ___pointerImage_2; }
	inline Texture_t2526458961 ** get_address_of_pointerImage_2() { return &___pointerImage_2; }
	inline void set_pointerImage_2(Texture_t2526458961 * value)
	{
		___pointerImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___pointerImage_2, value);
	}

	inline static int32_t get_offset_of_pointerSize_3() { return static_cast<int32_t>(offsetof(CardboardOnGUIMouse_t2612501887, ___pointerSize_3)); }
	inline Vector2_t4282066565  get_pointerSize_3() const { return ___pointerSize_3; }
	inline Vector2_t4282066565 * get_address_of_pointerSize_3() { return &___pointerSize_3; }
	inline void set_pointerSize_3(Vector2_t4282066565  value)
	{
		___pointerSize_3 = value;
	}

	inline static int32_t get_offset_of_pointerSpot_4() { return static_cast<int32_t>(offsetof(CardboardOnGUIMouse_t2612501887, ___pointerSpot_4)); }
	inline Vector2_t4282066565  get_pointerSpot_4() const { return ___pointerSpot_4; }
	inline Vector2_t4282066565 * get_address_of_pointerSpot_4() { return &___pointerSpot_4; }
	inline void set_pointerSpot_4(Vector2_t4282066565  value)
	{
		___pointerSpot_4 = value;
	}

	inline static int32_t get_offset_of_pointerVisible_5() { return static_cast<int32_t>(offsetof(CardboardOnGUIMouse_t2612501887, ___pointerVisible_5)); }
	inline bool get_pointerVisible_5() const { return ___pointerVisible_5; }
	inline bool* get_address_of_pointerVisible_5() { return &___pointerVisible_5; }
	inline void set_pointerVisible_5(bool value)
	{
		___pointerVisible_5 = value;
	}

	inline static int32_t get_offset_of_pointerX_6() { return static_cast<int32_t>(offsetof(CardboardOnGUIMouse_t2612501887, ___pointerX_6)); }
	inline int32_t get_pointerX_6() const { return ___pointerX_6; }
	inline int32_t* get_address_of_pointerX_6() { return &___pointerX_6; }
	inline void set_pointerX_6(int32_t value)
	{
		___pointerX_6 = value;
	}

	inline static int32_t get_offset_of_pointerY_7() { return static_cast<int32_t>(offsetof(CardboardOnGUIMouse_t2612501887, ___pointerY_7)); }
	inline int32_t get_pointerY_7() const { return ___pointerY_7; }
	inline int32_t* get_address_of_pointerY_7() { return &___pointerY_7; }
	inline void set_pointerY_7(int32_t value)
	{
		___pointerY_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
