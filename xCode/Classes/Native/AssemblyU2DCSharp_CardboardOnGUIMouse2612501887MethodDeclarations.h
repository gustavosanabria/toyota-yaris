﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardOnGUIMouse
struct CardboardOnGUIMouse_t2612501887;

#include "codegen/il2cpp-codegen.h"

// System.Void CardboardOnGUIMouse::.ctor()
extern "C"  void CardboardOnGUIMouse__ctor_m2132161740 (CardboardOnGUIMouse_t2612501887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIMouse::LateUpdate()
extern "C"  void CardboardOnGUIMouse_LateUpdate_m261895431 (CardboardOnGUIMouse_t2612501887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIMouse::DrawPointerImage()
extern "C"  void CardboardOnGUIMouse_DrawPointerImage_m2468475002 (CardboardOnGUIMouse_t2612501887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
