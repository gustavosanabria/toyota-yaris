﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardOnGUIWindow
struct  CardboardOnGUIWindow_t3958675030  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.MeshRenderer CardboardOnGUIWindow::meshRenderer
	MeshRenderer_t2804666580 * ___meshRenderer_2;
	// UnityEngine.Rect CardboardOnGUIWindow::rect
	Rect_t4241904616  ___rect_3;

public:
	inline static int32_t get_offset_of_meshRenderer_2() { return static_cast<int32_t>(offsetof(CardboardOnGUIWindow_t3958675030, ___meshRenderer_2)); }
	inline MeshRenderer_t2804666580 * get_meshRenderer_2() const { return ___meshRenderer_2; }
	inline MeshRenderer_t2804666580 ** get_address_of_meshRenderer_2() { return &___meshRenderer_2; }
	inline void set_meshRenderer_2(MeshRenderer_t2804666580 * value)
	{
		___meshRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___meshRenderer_2, value);
	}

	inline static int32_t get_offset_of_rect_3() { return static_cast<int32_t>(offsetof(CardboardOnGUIWindow_t3958675030, ___rect_3)); }
	inline Rect_t4241904616  get_rect_3() const { return ___rect_3; }
	inline Rect_t4241904616 * get_address_of_rect_3() { return &___rect_3; }
	inline void set_rect_3(Rect_t4241904616  value)
	{
		___rect_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
