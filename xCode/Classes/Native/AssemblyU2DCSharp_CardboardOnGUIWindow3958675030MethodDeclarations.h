﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardOnGUIWindow
struct CardboardOnGUIWindow_t3958675030;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void CardboardOnGUIWindow::.ctor()
extern "C"  void CardboardOnGUIWindow__ctor_m1990755589 (CardboardOnGUIWindow_t3958675030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::Awake()
extern "C"  void CardboardOnGUIWindow_Awake_m2228360808 (CardboardOnGUIWindow_t3958675030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::Reset()
extern "C"  void CardboardOnGUIWindow_Reset_m3932155826 (CardboardOnGUIWindow_t3958675030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::Create(UnityEngine.RenderTexture)
extern "C"  void CardboardOnGUIWindow_Create_m3821779305 (CardboardOnGUIWindow_t3958675030 * __this, RenderTexture_t1963041563 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::OnDisable()
extern "C"  void CardboardOnGUIWindow_OnDisable_m1126944492 (CardboardOnGUIWindow_t3958675030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::LateUpdate()
extern "C"  void CardboardOnGUIWindow_LateUpdate_m3896516718 (CardboardOnGUIWindow_t3958675030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
