﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardProfile
struct CardboardProfile_t3514264339;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Double[]
struct DoubleU5BU5D_t2145413704;
// System.Double[,]
struct DoubleU5BU2CU5D_t2145413705;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CardboardProfile_ScreenSizes2553298538.h"
#include "AssemblyU2DCSharp_CardboardProfile_DeviceTypes1054882471.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_CardboardProfile_Distortion2959612217.h"

// System.Void CardboardProfile::.ctor()
extern "C"  void CardboardProfile__ctor_m2692317416 (CardboardProfile_t3514264339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardProfile::.cctor()
extern "C"  void CardboardProfile__cctor_m1375365061 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CardboardProfile CardboardProfile::Clone()
extern "C"  CardboardProfile_t3514264339 * CardboardProfile_Clone_m3818106969 (CardboardProfile_t3514264339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CardboardProfile::get_VerticalLensOffset()
extern "C"  float CardboardProfile_get_VerticalLensOffset_m3874858808 (CardboardProfile_t3514264339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CardboardProfile CardboardProfile::GetKnownProfile(CardboardProfile/ScreenSizes,CardboardProfile/DeviceTypes)
extern "C"  CardboardProfile_t3514264339 * CardboardProfile_GetKnownProfile_m2697412817 (Il2CppObject * __this /* static, unused */, int32_t ___screenSize0, int32_t ___deviceType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardProfile::GetLeftEyeVisibleTanAngles(System.Single[])
extern "C"  void CardboardProfile_GetLeftEyeVisibleTanAngles_m3891165834 (CardboardProfile_t3514264339 * __this, SingleU5BU5D_t2316563989* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardProfile::GetLeftEyeNoLensTanAngles(System.Single[])
extern "C"  void CardboardProfile_GetLeftEyeNoLensTanAngles_m3355921205 (CardboardProfile_t3514264339 * __this, SingleU5BU5D_t2316563989* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect CardboardProfile::GetLeftEyeVisibleScreenRect(System.Single[])
extern "C"  Rect_t4241904616  CardboardProfile_GetLeftEyeVisibleScreenRect_m4104603223 (CardboardProfile_t3514264339 * __this, SingleU5BU5D_t2316563989* ___undistortedFrustum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CardboardProfile::GetMaxRadius(System.Single[])
extern "C"  float CardboardProfile_GetMaxRadius_m1330854785 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___tanAngleRect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double[] CardboardProfile::solveLeastSquares(System.Double[,],System.Double[])
extern "C"  DoubleU5BU5D_t2145413704* CardboardProfile_solveLeastSquares_m597961519 (Il2CppObject * __this /* static, unused */, DoubleU5BU2CU5D_t2145413705* ___matA0, DoubleU5BU5D_t2145413704* ___vecY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CardboardProfile/Distortion CardboardProfile::ApproximateInverse(System.Single,System.Single,System.Single,System.Int32)
extern "C"  Distortion_t2959612217  CardboardProfile_ApproximateInverse_m2391632852 (Il2CppObject * __this /* static, unused */, float ___k10, float ___k21, float ___maxRadius2, int32_t ___numSamples3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CardboardProfile/Distortion CardboardProfile::ApproximateInverse(CardboardProfile/Distortion,System.Single,System.Int32)
extern "C"  Distortion_t2959612217  CardboardProfile_ApproximateInverse_m151061419 (Il2CppObject * __this /* static, unused */, Distortion_t2959612217  ___distort0, float ___maxRadius1, int32_t ___numSamples2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
