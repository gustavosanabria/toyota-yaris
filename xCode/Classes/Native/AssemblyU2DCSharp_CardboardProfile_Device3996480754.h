﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_CardboardProfile_Lenses4225285320.h"
#include "AssemblyU2DCSharp_CardboardProfile_MaxFOV4250474341.h"
#include "AssemblyU2DCSharp_CardboardProfile_Distortion2959612217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardProfile/Device
struct  Device_t3996480754 
{
public:
	// CardboardProfile/Lenses CardboardProfile/Device::lenses
	Lenses_t4225285320  ___lenses_0;
	// CardboardProfile/MaxFOV CardboardProfile/Device::maxFOV
	MaxFOV_t4250474341  ___maxFOV_1;
	// CardboardProfile/Distortion CardboardProfile/Device::distortion
	Distortion_t2959612217  ___distortion_2;
	// CardboardProfile/Distortion CardboardProfile/Device::inverse
	Distortion_t2959612217  ___inverse_3;

public:
	inline static int32_t get_offset_of_lenses_0() { return static_cast<int32_t>(offsetof(Device_t3996480754, ___lenses_0)); }
	inline Lenses_t4225285320  get_lenses_0() const { return ___lenses_0; }
	inline Lenses_t4225285320 * get_address_of_lenses_0() { return &___lenses_0; }
	inline void set_lenses_0(Lenses_t4225285320  value)
	{
		___lenses_0 = value;
	}

	inline static int32_t get_offset_of_maxFOV_1() { return static_cast<int32_t>(offsetof(Device_t3996480754, ___maxFOV_1)); }
	inline MaxFOV_t4250474341  get_maxFOV_1() const { return ___maxFOV_1; }
	inline MaxFOV_t4250474341 * get_address_of_maxFOV_1() { return &___maxFOV_1; }
	inline void set_maxFOV_1(MaxFOV_t4250474341  value)
	{
		___maxFOV_1 = value;
	}

	inline static int32_t get_offset_of_distortion_2() { return static_cast<int32_t>(offsetof(Device_t3996480754, ___distortion_2)); }
	inline Distortion_t2959612217  get_distortion_2() const { return ___distortion_2; }
	inline Distortion_t2959612217 * get_address_of_distortion_2() { return &___distortion_2; }
	inline void set_distortion_2(Distortion_t2959612217  value)
	{
		___distortion_2 = value;
	}

	inline static int32_t get_offset_of_inverse_3() { return static_cast<int32_t>(offsetof(Device_t3996480754, ___inverse_3)); }
	inline Distortion_t2959612217  get_inverse_3() const { return ___inverse_3; }
	inline Distortion_t2959612217 * get_address_of_inverse_3() { return &___inverse_3; }
	inline void set_inverse_3(Distortion_t2959612217  value)
	{
		___inverse_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: CardboardProfile/Device
struct Device_t3996480754_marshaled_pinvoke
{
	Lenses_t4225285320_marshaled_pinvoke ___lenses_0;
	MaxFOV_t4250474341_marshaled_pinvoke ___maxFOV_1;
	Distortion_t2959612217_marshaled_pinvoke ___distortion_2;
	Distortion_t2959612217_marshaled_pinvoke ___inverse_3;
};
// Native definition for marshalling of: CardboardProfile/Device
struct Device_t3996480754_marshaled_com
{
	Lenses_t4225285320_marshaled_com ___lenses_0;
	MaxFOV_t4250474341_marshaled_com ___maxFOV_1;
	Distortion_t2959612217_marshaled_com ___distortion_2;
	Distortion_t2959612217_marshaled_com ___inverse_3;
};
