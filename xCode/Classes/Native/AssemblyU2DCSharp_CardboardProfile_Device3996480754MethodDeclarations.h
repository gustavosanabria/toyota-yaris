﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardProfile/Device
struct Device_t3996480754;
struct Device_t3996480754_marshaled_pinvoke;
struct Device_t3996480754_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Device_t3996480754;
struct Device_t3996480754_marshaled_pinvoke;

extern "C" void Device_t3996480754_marshal_pinvoke(const Device_t3996480754& unmarshaled, Device_t3996480754_marshaled_pinvoke& marshaled);
extern "C" void Device_t3996480754_marshal_pinvoke_back(const Device_t3996480754_marshaled_pinvoke& marshaled, Device_t3996480754& unmarshaled);
extern "C" void Device_t3996480754_marshal_pinvoke_cleanup(Device_t3996480754_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Device_t3996480754;
struct Device_t3996480754_marshaled_com;

extern "C" void Device_t3996480754_marshal_com(const Device_t3996480754& unmarshaled, Device_t3996480754_marshaled_com& marshaled);
extern "C" void Device_t3996480754_marshal_com_back(const Device_t3996480754_marshaled_com& marshaled, Device_t3996480754& unmarshaled);
extern "C" void Device_t3996480754_marshal_com_cleanup(Device_t3996480754_marshaled_com& marshaled);
