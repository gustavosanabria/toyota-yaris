﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardProfile/Distortion
struct Distortion_t2959612217;
struct Distortion_t2959612217_marshaled_pinvoke;
struct Distortion_t2959612217_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CardboardProfile_Distortion2959612217.h"

// System.Single CardboardProfile/Distortion::distort(System.Single)
extern "C"  float Distortion_distort_m586609828 (Distortion_t2959612217 * __this, float ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CardboardProfile/Distortion::distortInv(System.Single)
extern "C"  float Distortion_distortInv_m2315527615 (Distortion_t2959612217 * __this, float ___radius0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Distortion_t2959612217;
struct Distortion_t2959612217_marshaled_pinvoke;

extern "C" void Distortion_t2959612217_marshal_pinvoke(const Distortion_t2959612217& unmarshaled, Distortion_t2959612217_marshaled_pinvoke& marshaled);
extern "C" void Distortion_t2959612217_marshal_pinvoke_back(const Distortion_t2959612217_marshaled_pinvoke& marshaled, Distortion_t2959612217& unmarshaled);
extern "C" void Distortion_t2959612217_marshal_pinvoke_cleanup(Distortion_t2959612217_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Distortion_t2959612217;
struct Distortion_t2959612217_marshaled_com;

extern "C" void Distortion_t2959612217_marshal_com(const Distortion_t2959612217& unmarshaled, Distortion_t2959612217_marshaled_com& marshaled);
extern "C" void Distortion_t2959612217_marshal_com_back(const Distortion_t2959612217_marshaled_com& marshaled, Distortion_t2959612217& unmarshaled);
extern "C" void Distortion_t2959612217_marshal_com_cleanup(Distortion_t2959612217_marshaled_com& marshaled);
