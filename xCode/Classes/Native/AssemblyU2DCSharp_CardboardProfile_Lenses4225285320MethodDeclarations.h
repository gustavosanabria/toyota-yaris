﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardProfile/Lenses
struct Lenses_t4225285320;
struct Lenses_t4225285320_marshaled_pinvoke;
struct Lenses_t4225285320_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Lenses_t4225285320;
struct Lenses_t4225285320_marshaled_pinvoke;

extern "C" void Lenses_t4225285320_marshal_pinvoke(const Lenses_t4225285320& unmarshaled, Lenses_t4225285320_marshaled_pinvoke& marshaled);
extern "C" void Lenses_t4225285320_marshal_pinvoke_back(const Lenses_t4225285320_marshaled_pinvoke& marshaled, Lenses_t4225285320& unmarshaled);
extern "C" void Lenses_t4225285320_marshal_pinvoke_cleanup(Lenses_t4225285320_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Lenses_t4225285320;
struct Lenses_t4225285320_marshaled_com;

extern "C" void Lenses_t4225285320_marshal_com(const Lenses_t4225285320& unmarshaled, Lenses_t4225285320_marshaled_com& marshaled);
extern "C" void Lenses_t4225285320_marshal_com_back(const Lenses_t4225285320_marshaled_com& marshaled, Lenses_t4225285320& unmarshaled);
extern "C" void Lenses_t4225285320_marshal_com_cleanup(Lenses_t4225285320_marshaled_com& marshaled);
