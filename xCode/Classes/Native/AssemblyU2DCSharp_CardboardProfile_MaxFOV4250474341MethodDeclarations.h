﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardProfile/MaxFOV
struct MaxFOV_t4250474341;
struct MaxFOV_t4250474341_marshaled_pinvoke;
struct MaxFOV_t4250474341_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct MaxFOV_t4250474341;
struct MaxFOV_t4250474341_marshaled_pinvoke;

extern "C" void MaxFOV_t4250474341_marshal_pinvoke(const MaxFOV_t4250474341& unmarshaled, MaxFOV_t4250474341_marshaled_pinvoke& marshaled);
extern "C" void MaxFOV_t4250474341_marshal_pinvoke_back(const MaxFOV_t4250474341_marshaled_pinvoke& marshaled, MaxFOV_t4250474341& unmarshaled);
extern "C" void MaxFOV_t4250474341_marshal_pinvoke_cleanup(MaxFOV_t4250474341_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct MaxFOV_t4250474341;
struct MaxFOV_t4250474341_marshaled_com;

extern "C" void MaxFOV_t4250474341_marshal_com(const MaxFOV_t4250474341& unmarshaled, MaxFOV_t4250474341_marshaled_com& marshaled);
extern "C" void MaxFOV_t4250474341_marshal_com_back(const MaxFOV_t4250474341_marshaled_com& marshaled, MaxFOV_t4250474341& unmarshaled);
extern "C" void MaxFOV_t4250474341_marshal_com_cleanup(MaxFOV_t4250474341_marshaled_com& marshaled);
