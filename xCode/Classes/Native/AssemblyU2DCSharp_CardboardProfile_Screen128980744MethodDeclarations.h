﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardProfile/Screen
struct Screen_t128980744;
struct Screen_t128980744_marshaled_pinvoke;
struct Screen_t128980744_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Screen_t128980744;
struct Screen_t128980744_marshaled_pinvoke;

extern "C" void Screen_t128980744_marshal_pinvoke(const Screen_t128980744& unmarshaled, Screen_t128980744_marshaled_pinvoke& marshaled);
extern "C" void Screen_t128980744_marshal_pinvoke_back(const Screen_t128980744_marshaled_pinvoke& marshaled, Screen_t128980744& unmarshaled);
extern "C" void Screen_t128980744_marshal_pinvoke_cleanup(Screen_t128980744_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Screen_t128980744;
struct Screen_t128980744_marshaled_com;

extern "C" void Screen_t128980744_marshal_com(const Screen_t128980744& unmarshaled, Screen_t128980744_marshaled_com& marshaled);
extern "C" void Screen_t128980744_marshal_com_back(const Screen_t128980744_marshaled_com& marshaled, Screen_t128980744& unmarshaled);
extern "C" void Screen_t128980744_marshal_com_cleanup(Screen_t128980744_marshaled_com& marshaled);
