﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cleaner
struct Cleaner_t2521427094;

#include "codegen/il2cpp-codegen.h"

// System.Void Cleaner::.ctor()
extern "C"  void Cleaner__ctor_m4115729301 (Cleaner_t2521427094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cleaner::Awake()
extern "C"  void Cleaner_Awake_m58367224 (Cleaner_t2521427094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
