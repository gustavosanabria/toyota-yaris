﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UICamera
struct UICamera_t189364953;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UITexture
struct UITexture_t3903132647;
// UIPanel
struct UIPanel_t295209936;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t817370046;
// DescriptionsManager/onVideoEndDel
struct onVideoEndDel_t1248556915;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DescriptionsManager
struct  DescriptionsManager_t373055382  : public MonoBehaviour_t667441552
{
public:
	// System.Single DescriptionsManager::lightIconsDescriptionsTweenSpeed
	float ___lightIconsDescriptionsTweenSpeed_2;
	// UICamera DescriptionsManager::descriptionsUICam
	UICamera_t189364953 * ___descriptionsUICam_3;
	// UnityEngine.GameObject DescriptionsManager::loadingTex
	GameObject_t3674682005 * ___loadingTex_4;
	// UnityEngine.GameObject DescriptionsManager::descriptionsBackground
	GameObject_t3674682005 * ___descriptionsBackground_5;
	// UITexture DescriptionsManager::descriptionTexture
	UITexture_t3903132647 * ___descriptionTexture_6;
	// UnityEngine.GameObject DescriptionsManager::extendedDescriptionContainer
	GameObject_t3674682005 * ___extendedDescriptionContainer_7;
	// System.Single DescriptionsManager::extendedDescriptionScrollValue
	float ___extendedDescriptionScrollValue_8;
	// UIPanel DescriptionsManager::extendedDescriptionScrollPanel
	UIPanel_t295209936 * ___extendedDescriptionScrollPanel_9;
	// UIPanel DescriptionsManager::airbagDescrScrollPanel
	UIPanel_t295209936 * ___airbagDescrScrollPanel_10;
	// UIPanel DescriptionsManager::acCenterScrollPanel
	UIPanel_t295209936 * ___acCenterScrollPanel_11;
	// UIPanel DescriptionsManager::acLeftScrollPanel
	UIPanel_t295209936 * ___acLeftScrollPanel_12;
	// UIPanel DescriptionsManager::acRightScrollPanel
	UIPanel_t295209936 * ___acRightScrollPanel_13;
	// UnityEngine.Vector3 DescriptionsManager::extendedDescriptionOriginalPanelPos
	Vector3_t4282066566  ___extendedDescriptionOriginalPanelPos_14;
	// UnityEngine.Vector3 DescriptionsManager::airbagDescrOriginalPanelPos
	Vector3_t4282066566  ___airbagDescrOriginalPanelPos_15;
	// UnityEngine.Vector3 DescriptionsManager::acCenterDescrOriginalPanelPos
	Vector3_t4282066566  ___acCenterDescrOriginalPanelPos_16;
	// UnityEngine.Vector3 DescriptionsManager::acLeftDescrOriginalPanelPos
	Vector3_t4282066566  ___acLeftDescrOriginalPanelPos_17;
	// UnityEngine.Vector3 DescriptionsManager::acRightDescrOriginalPanelPos
	Vector3_t4282066566  ___acRightDescrOriginalPanelPos_18;
	// UnityEngine.Vector2 DescriptionsManager::extendedDescriptionOriginalClip
	Vector2_t4282066565  ___extendedDescriptionOriginalClip_19;
	// UnityEngine.Vector2 DescriptionsManager::airbagDescrOriginalClip
	Vector2_t4282066565  ___airbagDescrOriginalClip_20;
	// UnityEngine.Vector2 DescriptionsManager::acCenterDescrOriginalClip
	Vector2_t4282066565  ___acCenterDescrOriginalClip_21;
	// UnityEngine.Vector2 DescriptionsManager::acLeftDescrOriginalClip
	Vector2_t4282066565  ___acLeftDescrOriginalClip_22;
	// UnityEngine.Vector2 DescriptionsManager::acRightDescrOriginalClip
	Vector2_t4282066565  ___acRightDescrOriginalClip_23;
	// UnityEngine.Quaternion DescriptionsManager::zAxis180Rotation
	Quaternion_t1553702882  ___zAxis180Rotation_24;
	// UICamera DescriptionsManager::guiCameraUIScript
	UICamera_t189364953 * ___guiCameraUIScript_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> DescriptionsManager::texturesOffset
	Dictionary_2_t817370046 * ___texturesOffset_27;
	// DescriptionsManager/onVideoEndDel DescriptionsManager::evs
	onVideoEndDel_t1248556915 * ___evs_28;
	// UITexture DescriptionsManager::<currDescriptionUITex>k__BackingField
	UITexture_t3903132647 * ___U3CcurrDescriptionUITexU3Ek__BackingField_29;
	// System.Single DescriptionsManager::<currDescrOriginalWidth>k__BackingField
	float ___U3CcurrDescrOriginalWidthU3Ek__BackingField_30;
	// System.Single DescriptionsManager::<currDescrOriginalHeight>k__BackingField
	float ___U3CcurrDescrOriginalHeightU3Ek__BackingField_31;

public:
	inline static int32_t get_offset_of_lightIconsDescriptionsTweenSpeed_2() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___lightIconsDescriptionsTweenSpeed_2)); }
	inline float get_lightIconsDescriptionsTweenSpeed_2() const { return ___lightIconsDescriptionsTweenSpeed_2; }
	inline float* get_address_of_lightIconsDescriptionsTweenSpeed_2() { return &___lightIconsDescriptionsTweenSpeed_2; }
	inline void set_lightIconsDescriptionsTweenSpeed_2(float value)
	{
		___lightIconsDescriptionsTweenSpeed_2 = value;
	}

	inline static int32_t get_offset_of_descriptionsUICam_3() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___descriptionsUICam_3)); }
	inline UICamera_t189364953 * get_descriptionsUICam_3() const { return ___descriptionsUICam_3; }
	inline UICamera_t189364953 ** get_address_of_descriptionsUICam_3() { return &___descriptionsUICam_3; }
	inline void set_descriptionsUICam_3(UICamera_t189364953 * value)
	{
		___descriptionsUICam_3 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionsUICam_3, value);
	}

	inline static int32_t get_offset_of_loadingTex_4() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___loadingTex_4)); }
	inline GameObject_t3674682005 * get_loadingTex_4() const { return ___loadingTex_4; }
	inline GameObject_t3674682005 ** get_address_of_loadingTex_4() { return &___loadingTex_4; }
	inline void set_loadingTex_4(GameObject_t3674682005 * value)
	{
		___loadingTex_4 = value;
		Il2CppCodeGenWriteBarrier(&___loadingTex_4, value);
	}

	inline static int32_t get_offset_of_descriptionsBackground_5() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___descriptionsBackground_5)); }
	inline GameObject_t3674682005 * get_descriptionsBackground_5() const { return ___descriptionsBackground_5; }
	inline GameObject_t3674682005 ** get_address_of_descriptionsBackground_5() { return &___descriptionsBackground_5; }
	inline void set_descriptionsBackground_5(GameObject_t3674682005 * value)
	{
		___descriptionsBackground_5 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionsBackground_5, value);
	}

	inline static int32_t get_offset_of_descriptionTexture_6() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___descriptionTexture_6)); }
	inline UITexture_t3903132647 * get_descriptionTexture_6() const { return ___descriptionTexture_6; }
	inline UITexture_t3903132647 ** get_address_of_descriptionTexture_6() { return &___descriptionTexture_6; }
	inline void set_descriptionTexture_6(UITexture_t3903132647 * value)
	{
		___descriptionTexture_6 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionTexture_6, value);
	}

	inline static int32_t get_offset_of_extendedDescriptionContainer_7() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___extendedDescriptionContainer_7)); }
	inline GameObject_t3674682005 * get_extendedDescriptionContainer_7() const { return ___extendedDescriptionContainer_7; }
	inline GameObject_t3674682005 ** get_address_of_extendedDescriptionContainer_7() { return &___extendedDescriptionContainer_7; }
	inline void set_extendedDescriptionContainer_7(GameObject_t3674682005 * value)
	{
		___extendedDescriptionContainer_7 = value;
		Il2CppCodeGenWriteBarrier(&___extendedDescriptionContainer_7, value);
	}

	inline static int32_t get_offset_of_extendedDescriptionScrollValue_8() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___extendedDescriptionScrollValue_8)); }
	inline float get_extendedDescriptionScrollValue_8() const { return ___extendedDescriptionScrollValue_8; }
	inline float* get_address_of_extendedDescriptionScrollValue_8() { return &___extendedDescriptionScrollValue_8; }
	inline void set_extendedDescriptionScrollValue_8(float value)
	{
		___extendedDescriptionScrollValue_8 = value;
	}

	inline static int32_t get_offset_of_extendedDescriptionScrollPanel_9() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___extendedDescriptionScrollPanel_9)); }
	inline UIPanel_t295209936 * get_extendedDescriptionScrollPanel_9() const { return ___extendedDescriptionScrollPanel_9; }
	inline UIPanel_t295209936 ** get_address_of_extendedDescriptionScrollPanel_9() { return &___extendedDescriptionScrollPanel_9; }
	inline void set_extendedDescriptionScrollPanel_9(UIPanel_t295209936 * value)
	{
		___extendedDescriptionScrollPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___extendedDescriptionScrollPanel_9, value);
	}

	inline static int32_t get_offset_of_airbagDescrScrollPanel_10() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___airbagDescrScrollPanel_10)); }
	inline UIPanel_t295209936 * get_airbagDescrScrollPanel_10() const { return ___airbagDescrScrollPanel_10; }
	inline UIPanel_t295209936 ** get_address_of_airbagDescrScrollPanel_10() { return &___airbagDescrScrollPanel_10; }
	inline void set_airbagDescrScrollPanel_10(UIPanel_t295209936 * value)
	{
		___airbagDescrScrollPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___airbagDescrScrollPanel_10, value);
	}

	inline static int32_t get_offset_of_acCenterScrollPanel_11() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acCenterScrollPanel_11)); }
	inline UIPanel_t295209936 * get_acCenterScrollPanel_11() const { return ___acCenterScrollPanel_11; }
	inline UIPanel_t295209936 ** get_address_of_acCenterScrollPanel_11() { return &___acCenterScrollPanel_11; }
	inline void set_acCenterScrollPanel_11(UIPanel_t295209936 * value)
	{
		___acCenterScrollPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___acCenterScrollPanel_11, value);
	}

	inline static int32_t get_offset_of_acLeftScrollPanel_12() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acLeftScrollPanel_12)); }
	inline UIPanel_t295209936 * get_acLeftScrollPanel_12() const { return ___acLeftScrollPanel_12; }
	inline UIPanel_t295209936 ** get_address_of_acLeftScrollPanel_12() { return &___acLeftScrollPanel_12; }
	inline void set_acLeftScrollPanel_12(UIPanel_t295209936 * value)
	{
		___acLeftScrollPanel_12 = value;
		Il2CppCodeGenWriteBarrier(&___acLeftScrollPanel_12, value);
	}

	inline static int32_t get_offset_of_acRightScrollPanel_13() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acRightScrollPanel_13)); }
	inline UIPanel_t295209936 * get_acRightScrollPanel_13() const { return ___acRightScrollPanel_13; }
	inline UIPanel_t295209936 ** get_address_of_acRightScrollPanel_13() { return &___acRightScrollPanel_13; }
	inline void set_acRightScrollPanel_13(UIPanel_t295209936 * value)
	{
		___acRightScrollPanel_13 = value;
		Il2CppCodeGenWriteBarrier(&___acRightScrollPanel_13, value);
	}

	inline static int32_t get_offset_of_extendedDescriptionOriginalPanelPos_14() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___extendedDescriptionOriginalPanelPos_14)); }
	inline Vector3_t4282066566  get_extendedDescriptionOriginalPanelPos_14() const { return ___extendedDescriptionOriginalPanelPos_14; }
	inline Vector3_t4282066566 * get_address_of_extendedDescriptionOriginalPanelPos_14() { return &___extendedDescriptionOriginalPanelPos_14; }
	inline void set_extendedDescriptionOriginalPanelPos_14(Vector3_t4282066566  value)
	{
		___extendedDescriptionOriginalPanelPos_14 = value;
	}

	inline static int32_t get_offset_of_airbagDescrOriginalPanelPos_15() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___airbagDescrOriginalPanelPos_15)); }
	inline Vector3_t4282066566  get_airbagDescrOriginalPanelPos_15() const { return ___airbagDescrOriginalPanelPos_15; }
	inline Vector3_t4282066566 * get_address_of_airbagDescrOriginalPanelPos_15() { return &___airbagDescrOriginalPanelPos_15; }
	inline void set_airbagDescrOriginalPanelPos_15(Vector3_t4282066566  value)
	{
		___airbagDescrOriginalPanelPos_15 = value;
	}

	inline static int32_t get_offset_of_acCenterDescrOriginalPanelPos_16() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acCenterDescrOriginalPanelPos_16)); }
	inline Vector3_t4282066566  get_acCenterDescrOriginalPanelPos_16() const { return ___acCenterDescrOriginalPanelPos_16; }
	inline Vector3_t4282066566 * get_address_of_acCenterDescrOriginalPanelPos_16() { return &___acCenterDescrOriginalPanelPos_16; }
	inline void set_acCenterDescrOriginalPanelPos_16(Vector3_t4282066566  value)
	{
		___acCenterDescrOriginalPanelPos_16 = value;
	}

	inline static int32_t get_offset_of_acLeftDescrOriginalPanelPos_17() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acLeftDescrOriginalPanelPos_17)); }
	inline Vector3_t4282066566  get_acLeftDescrOriginalPanelPos_17() const { return ___acLeftDescrOriginalPanelPos_17; }
	inline Vector3_t4282066566 * get_address_of_acLeftDescrOriginalPanelPos_17() { return &___acLeftDescrOriginalPanelPos_17; }
	inline void set_acLeftDescrOriginalPanelPos_17(Vector3_t4282066566  value)
	{
		___acLeftDescrOriginalPanelPos_17 = value;
	}

	inline static int32_t get_offset_of_acRightDescrOriginalPanelPos_18() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acRightDescrOriginalPanelPos_18)); }
	inline Vector3_t4282066566  get_acRightDescrOriginalPanelPos_18() const { return ___acRightDescrOriginalPanelPos_18; }
	inline Vector3_t4282066566 * get_address_of_acRightDescrOriginalPanelPos_18() { return &___acRightDescrOriginalPanelPos_18; }
	inline void set_acRightDescrOriginalPanelPos_18(Vector3_t4282066566  value)
	{
		___acRightDescrOriginalPanelPos_18 = value;
	}

	inline static int32_t get_offset_of_extendedDescriptionOriginalClip_19() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___extendedDescriptionOriginalClip_19)); }
	inline Vector2_t4282066565  get_extendedDescriptionOriginalClip_19() const { return ___extendedDescriptionOriginalClip_19; }
	inline Vector2_t4282066565 * get_address_of_extendedDescriptionOriginalClip_19() { return &___extendedDescriptionOriginalClip_19; }
	inline void set_extendedDescriptionOriginalClip_19(Vector2_t4282066565  value)
	{
		___extendedDescriptionOriginalClip_19 = value;
	}

	inline static int32_t get_offset_of_airbagDescrOriginalClip_20() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___airbagDescrOriginalClip_20)); }
	inline Vector2_t4282066565  get_airbagDescrOriginalClip_20() const { return ___airbagDescrOriginalClip_20; }
	inline Vector2_t4282066565 * get_address_of_airbagDescrOriginalClip_20() { return &___airbagDescrOriginalClip_20; }
	inline void set_airbagDescrOriginalClip_20(Vector2_t4282066565  value)
	{
		___airbagDescrOriginalClip_20 = value;
	}

	inline static int32_t get_offset_of_acCenterDescrOriginalClip_21() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acCenterDescrOriginalClip_21)); }
	inline Vector2_t4282066565  get_acCenterDescrOriginalClip_21() const { return ___acCenterDescrOriginalClip_21; }
	inline Vector2_t4282066565 * get_address_of_acCenterDescrOriginalClip_21() { return &___acCenterDescrOriginalClip_21; }
	inline void set_acCenterDescrOriginalClip_21(Vector2_t4282066565  value)
	{
		___acCenterDescrOriginalClip_21 = value;
	}

	inline static int32_t get_offset_of_acLeftDescrOriginalClip_22() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acLeftDescrOriginalClip_22)); }
	inline Vector2_t4282066565  get_acLeftDescrOriginalClip_22() const { return ___acLeftDescrOriginalClip_22; }
	inline Vector2_t4282066565 * get_address_of_acLeftDescrOriginalClip_22() { return &___acLeftDescrOriginalClip_22; }
	inline void set_acLeftDescrOriginalClip_22(Vector2_t4282066565  value)
	{
		___acLeftDescrOriginalClip_22 = value;
	}

	inline static int32_t get_offset_of_acRightDescrOriginalClip_23() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___acRightDescrOriginalClip_23)); }
	inline Vector2_t4282066565  get_acRightDescrOriginalClip_23() const { return ___acRightDescrOriginalClip_23; }
	inline Vector2_t4282066565 * get_address_of_acRightDescrOriginalClip_23() { return &___acRightDescrOriginalClip_23; }
	inline void set_acRightDescrOriginalClip_23(Vector2_t4282066565  value)
	{
		___acRightDescrOriginalClip_23 = value;
	}

	inline static int32_t get_offset_of_zAxis180Rotation_24() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___zAxis180Rotation_24)); }
	inline Quaternion_t1553702882  get_zAxis180Rotation_24() const { return ___zAxis180Rotation_24; }
	inline Quaternion_t1553702882 * get_address_of_zAxis180Rotation_24() { return &___zAxis180Rotation_24; }
	inline void set_zAxis180Rotation_24(Quaternion_t1553702882  value)
	{
		___zAxis180Rotation_24 = value;
	}

	inline static int32_t get_offset_of_guiCameraUIScript_25() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___guiCameraUIScript_25)); }
	inline UICamera_t189364953 * get_guiCameraUIScript_25() const { return ___guiCameraUIScript_25; }
	inline UICamera_t189364953 ** get_address_of_guiCameraUIScript_25() { return &___guiCameraUIScript_25; }
	inline void set_guiCameraUIScript_25(UICamera_t189364953 * value)
	{
		___guiCameraUIScript_25 = value;
		Il2CppCodeGenWriteBarrier(&___guiCameraUIScript_25, value);
	}

	inline static int32_t get_offset_of_texturesOffset_27() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___texturesOffset_27)); }
	inline Dictionary_2_t817370046 * get_texturesOffset_27() const { return ___texturesOffset_27; }
	inline Dictionary_2_t817370046 ** get_address_of_texturesOffset_27() { return &___texturesOffset_27; }
	inline void set_texturesOffset_27(Dictionary_2_t817370046 * value)
	{
		___texturesOffset_27 = value;
		Il2CppCodeGenWriteBarrier(&___texturesOffset_27, value);
	}

	inline static int32_t get_offset_of_evs_28() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___evs_28)); }
	inline onVideoEndDel_t1248556915 * get_evs_28() const { return ___evs_28; }
	inline onVideoEndDel_t1248556915 ** get_address_of_evs_28() { return &___evs_28; }
	inline void set_evs_28(onVideoEndDel_t1248556915 * value)
	{
		___evs_28 = value;
		Il2CppCodeGenWriteBarrier(&___evs_28, value);
	}

	inline static int32_t get_offset_of_U3CcurrDescriptionUITexU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___U3CcurrDescriptionUITexU3Ek__BackingField_29)); }
	inline UITexture_t3903132647 * get_U3CcurrDescriptionUITexU3Ek__BackingField_29() const { return ___U3CcurrDescriptionUITexU3Ek__BackingField_29; }
	inline UITexture_t3903132647 ** get_address_of_U3CcurrDescriptionUITexU3Ek__BackingField_29() { return &___U3CcurrDescriptionUITexU3Ek__BackingField_29; }
	inline void set_U3CcurrDescriptionUITexU3Ek__BackingField_29(UITexture_t3903132647 * value)
	{
		___U3CcurrDescriptionUITexU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrDescriptionUITexU3Ek__BackingField_29, value);
	}

	inline static int32_t get_offset_of_U3CcurrDescrOriginalWidthU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___U3CcurrDescrOriginalWidthU3Ek__BackingField_30)); }
	inline float get_U3CcurrDescrOriginalWidthU3Ek__BackingField_30() const { return ___U3CcurrDescrOriginalWidthU3Ek__BackingField_30; }
	inline float* get_address_of_U3CcurrDescrOriginalWidthU3Ek__BackingField_30() { return &___U3CcurrDescrOriginalWidthU3Ek__BackingField_30; }
	inline void set_U3CcurrDescrOriginalWidthU3Ek__BackingField_30(float value)
	{
		___U3CcurrDescrOriginalWidthU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CcurrDescrOriginalHeightU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382, ___U3CcurrDescrOriginalHeightU3Ek__BackingField_31)); }
	inline float get_U3CcurrDescrOriginalHeightU3Ek__BackingField_31() const { return ___U3CcurrDescrOriginalHeightU3Ek__BackingField_31; }
	inline float* get_address_of_U3CcurrDescrOriginalHeightU3Ek__BackingField_31() { return &___U3CcurrDescrOriginalHeightU3Ek__BackingField_31; }
	inline void set_U3CcurrDescrOriginalHeightU3Ek__BackingField_31(float value)
	{
		___U3CcurrDescrOriginalHeightU3Ek__BackingField_31 = value;
	}
};

struct DescriptionsManager_t373055382_StaticFields
{
public:
	// System.Single DescriptionsManager::OffsetMin
	float ___OffsetMin_26;

public:
	inline static int32_t get_offset_of_OffsetMin_26() { return static_cast<int32_t>(offsetof(DescriptionsManager_t373055382_StaticFields, ___OffsetMin_26)); }
	inline float get_OffsetMin_26() const { return ___OffsetMin_26; }
	inline float* get_address_of_OffsetMin_26() { return &___OffsetMin_26; }
	inline void set_OffsetMin_26(float value)
	{
		___OffsetMin_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
