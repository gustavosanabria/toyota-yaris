﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DescriptionsManager
struct DescriptionsManager_t373055382;
// DescriptionsManager/onVideoEndDel
struct onVideoEndDel_t1248556915;
// UITexture
struct UITexture_t3903132647;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIScrollView
struct UIScrollView_t2113479878;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DescriptionsManager_onVideoEndDe1248556915.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"

// System.Void DescriptionsManager::.ctor()
extern "C"  void DescriptionsManager__ctor_m862248085 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::.cctor()
extern "C"  void DescriptionsManager__cctor_m477790648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::add_evs(DescriptionsManager/onVideoEndDel)
extern "C"  void DescriptionsManager_add_evs_m2924275560 (DescriptionsManager_t373055382 * __this, onVideoEndDel_t1248556915 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::remove_evs(DescriptionsManager/onVideoEndDel)
extern "C"  void DescriptionsManager_remove_evs_m3410262921 (DescriptionsManager_t373055382 * __this, onVideoEndDel_t1248556915 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UITexture DescriptionsManager::get_currDescriptionUITex()
extern "C"  UITexture_t3903132647 * DescriptionsManager_get_currDescriptionUITex_m849563573 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::set_currDescriptionUITex(UITexture)
extern "C"  void DescriptionsManager_set_currDescriptionUITex_m3614304470 (DescriptionsManager_t373055382 * __this, UITexture_t3903132647 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DescriptionsManager::get_currDescrOriginalWidth()
extern "C"  float DescriptionsManager_get_currDescrOriginalWidth_m4235338034 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::set_currDescrOriginalWidth(System.Single)
extern "C"  void DescriptionsManager_set_currDescrOriginalWidth_m3801507321 (DescriptionsManager_t373055382 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DescriptionsManager::get_currDescrOriginalHeight()
extern "C"  float DescriptionsManager_get_currDescrOriginalHeight_m2950274525 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::set_currDescrOriginalHeight(System.Single)
extern "C"  void DescriptionsManager_set_currDescrOriginalHeight_m33658542 (DescriptionsManager_t373055382 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Start()
extern "C"  void DescriptionsManager_Start_m4104353173 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::EnableClick(System.Boolean)
extern "C"  void DescriptionsManager_EnableClick_m209703983 (DescriptionsManager_t373055382 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowLoading()
extern "C"  void DescriptionsManager_ShowLoading_m651446258 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::GetDescriptionByString(System.String)
extern "C"  void DescriptionsManager_GetDescriptionByString_m2407913189 (DescriptionsManager_t373055382 * __this, String_t* ___descrStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::SpeedRegulatorDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_SpeedRegulatorDescription_m1508008973 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___descrContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::AirbagDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_AirbagDescription_m3048353417 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___descrContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Tires_PreCollisionDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_Tires_PreCollisionDescription_m3994629700 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___descrContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::AcCenterDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_AcCenterDescription_m1008893648 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___descrContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Mode_StabilityCtrlDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_Mode_StabilityCtrlDescription_m4226610127 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___descrContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowTireKitVideo()
extern "C"  void DescriptionsManager_ShowTireKitVideo_m117774585 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::DisableTargets()
extern "C"  void DescriptionsManager_DisableTargets_m3546823433 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowTargets()
extern "C"  void DescriptionsManager_ShowTargets_m2732215864 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ResetAndPlayTween(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_ResetAndPlayTween_m97888090 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::CloseGPSConfirmationWindow()
extern "C"  void DescriptionsManager_CloseGPSConfirmationWindow_m3336184198 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::CloseTireKitPopUpWindow()
extern "C"  void DescriptionsManager_CloseTireKitPopUpWindow_m407316601 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowDescription()
extern "C"  void DescriptionsManager_ShowDescription_m3857831986 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowExtendedDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_ShowExtendedDescription_m3112827185 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowAirbagExtendedDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_ShowAirbagExtendedDescription_m1656491027 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowAcCenterExtendedDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_ShowAcCenterExtendedDescription_m2890059930 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowAcLeftExtendedDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_ShowAcLeftExtendedDescription_m2942722664 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ShowAcRightExtendedDescription(UnityEngine.GameObject)
extern "C"  void DescriptionsManager_ShowAcRightExtendedDescription_m1494115629 (DescriptionsManager_t373055382 * __this, GameObject_t3674682005 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::CloseDescription()
extern "C"  void DescriptionsManager_CloseDescription_m4185358067 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::CloseExtendedDescription()
extern "C"  void DescriptionsManager_CloseExtendedDescription_m817426490 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::BtnDownExtendedDescription(UIScrollView)
extern "C"  void DescriptionsManager_BtnDownExtendedDescription_m1442027726 (DescriptionsManager_t373055382 * __this, UIScrollView_t2113479878 * ___uiSV0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::BtnUpExtendedDescription(UIScrollView)
extern "C"  void DescriptionsManager_BtnUpExtendedDescription_m626232949 (DescriptionsManager_t373055382 * __this, UIScrollView_t2113479878 * ___uiSV0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::ScrollDescription(UIScrollView,System.Single)
extern "C"  void DescriptionsManager_ScrollDescription_m294722113 (DescriptionsManager_t373055382 * __this, UIScrollView_t2113479878 * ___scrView0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::SetDictionary()
extern "C"  void DescriptionsManager_SetDictionary_m3986202699 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4WindowsDescription()
extern "C"  void DescriptionsManager_Rav4WindowsDescription_m3979852373 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4MirrorDescription()
extern "C"  void DescriptionsManager_Rav4MirrorDescription_m1897436931 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4LineCrossDescription()
extern "C"  void DescriptionsManager_Rav4LineCrossDescription_m3353836556 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_TrunkDescription()
extern "C"  void DescriptionsManager_Rav4SW_TrunkDescription_m800519145 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_VOLUMEDescription()
extern "C"  void DescriptionsManager_Rav4SW_VOLUMEDescription_m1538926051 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_PHONEDescription()
extern "C"  void DescriptionsManager_Rav4SW_PHONEDescription_m2744179311 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_VOICECOMMANDDescription()
extern "C"  void DescriptionsManager_Rav4SW_VOICECOMMANDDescription_m363167108 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_TFTNAVDescription()
extern "C"  void DescriptionsManager_Rav4SW_TFTNAVDescription_m4211586748 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_RETURNTRIPDescription()
extern "C"  void DescriptionsManager_Rav4SW_RETURNTRIPDescription_m4194550664 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SW_PRECOLLISIONDescription()
extern "C"  void DescriptionsManager_Rav4SW_PRECOLLISIONDescription_m204789550 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4EmergencyStopDescription()
extern "C"  void DescriptionsManager_Rav4EmergencyStopDescription_m2702141797 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4ParkingAssistanceDescription()
extern "C"  void DescriptionsManager_Rav4ParkingAssistanceDescription_m885609256 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4StabilityControlDescription()
extern "C"  void DescriptionsManager_Rav4StabilityControlDescription_m419936558 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4EnginePowerDescription()
extern "C"  void DescriptionsManager_Rav4EnginePowerDescription_m3333872501 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4AC_AUTO_OFFDescription()
extern "C"  void DescriptionsManager_Rav4AC_AUTO_OFFDescription_m729698716 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4AC_MONO_ACDescription()
extern "C"  void DescriptionsManager_Rav4AC_MONO_ACDescription_m3867480001 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4AC_DEMISTERDescription()
extern "C"  void DescriptionsManager_Rav4AC_DEMISTERDescription_m2021968304 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4AC_INFOSCREENDescription()
extern "C"  void DescriptionsManager_Rav4AC_INFOSCREENDescription_m2688749089 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4AC_FANSPEEDDescription()
extern "C"  void DescriptionsManager_Rav4AC_FANSPEEDDescription_m3940675943 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4SEATHEATDescription()
extern "C"  void DescriptionsManager_Rav4SEATHEATDescription_m782736365 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav4USBAUDIODescription()
extern "C"  void DescriptionsManager_Rav4USBAUDIODescription_m3323896752 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::Rav412VOLTDescription()
extern "C"  void DescriptionsManager_Rav412VOLTDescription_m1285262368 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::JumpToTutorial()
extern "C"  void DescriptionsManager_JumpToTutorial_m1652063382 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager::JumpToTFTTutorial()
extern "C"  void DescriptionsManager_JumpToTFTTutorial_m2134188970 (DescriptionsManager_t373055382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DescriptionsManager::GetTexture(System.String,System.String)
extern "C"  Il2CppObject * DescriptionsManager_GetTexture_m3446884946 (DescriptionsManager_t373055382 * __this, String_t* ___name0, String_t* ___pathStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
