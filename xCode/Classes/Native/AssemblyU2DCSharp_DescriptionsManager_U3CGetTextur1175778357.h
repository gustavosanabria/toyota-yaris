﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3731857623;
// System.Object
struct Il2CppObject;
// DescriptionsManager
struct DescriptionsManager_t373055382;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DescriptionsManager/<GetTexture>c__Iterator0
struct  U3CGetTextureU3Ec__Iterator0_t1175778357  : public Il2CppObject
{
public:
	// System.String DescriptionsManager/<GetTexture>c__Iterator0::<path>__0
	String_t* ___U3CpathU3E__0_0;
	// System.String DescriptionsManager/<GetTexture>c__Iterator0::pathStr
	String_t* ___pathStr_1;
	// System.String DescriptionsManager/<GetTexture>c__Iterator0::name
	String_t* ___name_2;
	// UnityEngine.ResourceRequest DescriptionsManager/<GetTexture>c__Iterator0::<resourceRequest>__1
	ResourceRequest_t3731857623 * ___U3CresourceRequestU3E__1_3;
	// System.Int32 DescriptionsManager/<GetTexture>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// System.Object DescriptionsManager/<GetTexture>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.String DescriptionsManager/<GetTexture>c__Iterator0::<$>pathStr
	String_t* ___U3CU24U3EpathStr_6;
	// System.String DescriptionsManager/<GetTexture>c__Iterator0::<$>name
	String_t* ___U3CU24U3Ename_7;
	// DescriptionsManager DescriptionsManager/<GetTexture>c__Iterator0::<>f__this
	DescriptionsManager_t373055382 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CpathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U3CpathU3E__0_0)); }
	inline String_t* get_U3CpathU3E__0_0() const { return ___U3CpathU3E__0_0; }
	inline String_t** get_address_of_U3CpathU3E__0_0() { return &___U3CpathU3E__0_0; }
	inline void set_U3CpathU3E__0_0(String_t* value)
	{
		___U3CpathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__0_0, value);
	}

	inline static int32_t get_offset_of_pathStr_1() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___pathStr_1)); }
	inline String_t* get_pathStr_1() const { return ___pathStr_1; }
	inline String_t** get_address_of_pathStr_1() { return &___pathStr_1; }
	inline void set_pathStr_1(String_t* value)
	{
		___pathStr_1 = value;
		Il2CppCodeGenWriteBarrier(&___pathStr_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_U3CresourceRequestU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U3CresourceRequestU3E__1_3)); }
	inline ResourceRequest_t3731857623 * get_U3CresourceRequestU3E__1_3() const { return ___U3CresourceRequestU3E__1_3; }
	inline ResourceRequest_t3731857623 ** get_address_of_U3CresourceRequestU3E__1_3() { return &___U3CresourceRequestU3E__1_3; }
	inline void set_U3CresourceRequestU3E__1_3(ResourceRequest_t3731857623 * value)
	{
		___U3CresourceRequestU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresourceRequestU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EpathStr_6() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U3CU24U3EpathStr_6)); }
	inline String_t* get_U3CU24U3EpathStr_6() const { return ___U3CU24U3EpathStr_6; }
	inline String_t** get_address_of_U3CU24U3EpathStr_6() { return &___U3CU24U3EpathStr_6; }
	inline void set_U3CU24U3EpathStr_6(String_t* value)
	{
		___U3CU24U3EpathStr_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EpathStr_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ename_7() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U3CU24U3Ename_7)); }
	inline String_t* get_U3CU24U3Ename_7() const { return ___U3CU24U3Ename_7; }
	inline String_t** get_address_of_U3CU24U3Ename_7() { return &___U3CU24U3Ename_7; }
	inline void set_U3CU24U3Ename_7(String_t* value)
	{
		___U3CU24U3Ename_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ename_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__Iterator0_t1175778357, ___U3CU3Ef__this_8)); }
	inline DescriptionsManager_t373055382 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline DescriptionsManager_t373055382 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(DescriptionsManager_t373055382 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
