﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DescriptionsManager/<GetTexture>c__Iterator0
struct U3CGetTextureU3Ec__Iterator0_t1175778357;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DescriptionsManager/<GetTexture>c__Iterator0::.ctor()
extern "C"  void U3CGetTextureU3Ec__Iterator0__ctor_m3612051974 (U3CGetTextureU3Ec__Iterator0_t1175778357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DescriptionsManager/<GetTexture>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetTextureU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m336953366 (U3CGetTextureU3Ec__Iterator0_t1175778357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DescriptionsManager/<GetTexture>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetTextureU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17199018 (U3CGetTextureU3Ec__Iterator0_t1175778357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DescriptionsManager/<GetTexture>c__Iterator0::MoveNext()
extern "C"  bool U3CGetTextureU3Ec__Iterator0_MoveNext_m4255770646 (U3CGetTextureU3Ec__Iterator0_t1175778357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager/<GetTexture>c__Iterator0::Dispose()
extern "C"  void U3CGetTextureU3Ec__Iterator0_Dispose_m746654915 (U3CGetTextureU3Ec__Iterator0_t1175778357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager/<GetTexture>c__Iterator0::Reset()
extern "C"  void U3CGetTextureU3Ec__Iterator0_Reset_m1258484915 (U3CGetTextureU3Ec__Iterator0_t1175778357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
