﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DescriptionsManager/onVideoEndDel
struct onVideoEndDel_t1248556915;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void DescriptionsManager/onVideoEndDel::.ctor(System.Object,System.IntPtr)
extern "C"  void onVideoEndDel__ctor_m2369793482 (onVideoEndDel_t1248556915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager/onVideoEndDel::Invoke()
extern "C"  void onVideoEndDel_Invoke_m2905178788 (onVideoEndDel_t1248556915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult DescriptionsManager/onVideoEndDel::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * onVideoEndDel_BeginInvoke_m4037074791 (onVideoEndDel_t1248556915 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DescriptionsManager/onVideoEndDel::EndInvoke(System.IAsyncResult)
extern "C"  void onVideoEndDel_EndInvoke_m1888883930 (onVideoEndDel_t1248556915 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
