﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DescriptionsStrings
struct  DescriptionsStrings_t1951003243  : public MonoBehaviour_t667441552
{
public:
	// System.String DescriptionsStrings::versoPrecollision
	String_t* ___versoPrecollision_2;
	// System.String DescriptionsStrings::versoLights
	String_t* ___versoLights_3;
	// System.String DescriptionsStrings::versoStabilityControl
	String_t* ___versoStabilityControl_4;
	// System.String DescriptionsStrings::versoParkingAssist
	String_t* ___versoParkingAssist_5;
	// System.String DescriptionsStrings::versoMirror
	String_t* ___versoMirror_6;
	// System.String DescriptionsStrings::versoVoiceCommand
	String_t* ___versoVoiceCommand_7;
	// System.String DescriptionsStrings::versoMode
	String_t* ___versoMode_8;
	// System.String DescriptionsStrings::versoPhone
	String_t* ___versoPhone_9;
	// System.String DescriptionsStrings::versoDispTrip
	String_t* ___versoDispTrip_10;
	// System.String DescriptionsStrings::versoSpeedLimit
	String_t* ___versoSpeedLimit_11;
	// System.String DescriptionsStrings::versoSpeedRegulator
	String_t* ___versoSpeedRegulator_12;
	// System.String DescriptionsStrings::versoLeftTachometer
	String_t* ___versoLeftTachometer_13;
	// System.String DescriptionsStrings::versoRightTachometer
	String_t* ___versoRightTachometer_14;
	// System.String DescriptionsStrings::versoCenterScreen
	String_t* ___versoCenterScreen_15;
	// System.String DescriptionsStrings::versoGPSLeftWheel
	String_t* ___versoGPSLeftWheel_16;
	// System.String DescriptionsStrings::versoGPSRightWheel
	String_t* ___versoGPSRightWheel_17;
	// System.String DescriptionsStrings::versoGPSLeftMenu
	String_t* ___versoGPSLeftMenu_18;
	// System.String DescriptionsStrings::versoGPSRightMenu
	String_t* ___versoGPSRightMenu_19;
	// System.String DescriptionsStrings::versoACLeftMenu
	String_t* ___versoACLeftMenu_20;
	// System.String DescriptionsStrings::versoACCenterMenu
	String_t* ___versoACCenterMenu_21;
	// System.String DescriptionsStrings::versoACRightMenu
	String_t* ___versoACRightMenu_22;
	// System.String DescriptionsStrings::versoAirbag
	String_t* ___versoAirbag_23;
	// System.String DescriptionsStrings::versoWindow
	String_t* ___versoWindow_24;
	// System.String DescriptionsStrings::versoPower
	String_t* ___versoPower_25;
	// System.String DescriptionsStrings::versoA_OFF
	String_t* ___versoA_OFF_26;
	// System.String DescriptionsStrings::doorDescr
	String_t* ___doorDescr_27;
	// System.String DescriptionsStrings::mirrorDescr
	String_t* ___mirrorDescr_28;
	// System.String DescriptionsStrings::tires_preCollissionDescr
	String_t* ___tires_preCollissionDescr_29;
	// System.String DescriptionsStrings::volume_ModeDescr
	String_t* ___volume_ModeDescr_30;
	// System.String DescriptionsStrings::phone_LineCrossDescr
	String_t* ___phone_LineCrossDescr_31;
	// System.String DescriptionsStrings::speedRegulatorDescr
	String_t* ___speedRegulatorDescr_32;
	// System.String DescriptionsStrings::warning_airbag_seatbeltDescr
	String_t* ___warning_airbag_seatbeltDescr_33;
	// System.String DescriptionsStrings::ac_DualDescr
	String_t* ___ac_DualDescr_34;
	// System.String DescriptionsStrings::auto_OffDescr
	String_t* ___auto_OffDescr_35;
	// System.String DescriptionsStrings::front_rearDescr
	String_t* ___front_rearDescr_36;
	// System.String DescriptionsStrings::usb_12VoltDescr
	String_t* ___usb_12VoltDescr_37;
	// System.String DescriptionsStrings::mode_stabilityCtrlDescr
	String_t* ___mode_stabilityCtrlDescr_38;
	// System.String DescriptionsStrings::enginePowerDescr
	String_t* ___enginePowerDescr_39;
	// System.String DescriptionsStrings::fuelTankLidDescr
	String_t* ___fuelTankLidDescr_40;
	// System.String DescriptionsStrings::hoodOpenBtnDescr
	String_t* ___hoodOpenBtnDescr_41;

public:
	inline static int32_t get_offset_of_versoPrecollision_2() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoPrecollision_2)); }
	inline String_t* get_versoPrecollision_2() const { return ___versoPrecollision_2; }
	inline String_t** get_address_of_versoPrecollision_2() { return &___versoPrecollision_2; }
	inline void set_versoPrecollision_2(String_t* value)
	{
		___versoPrecollision_2 = value;
		Il2CppCodeGenWriteBarrier(&___versoPrecollision_2, value);
	}

	inline static int32_t get_offset_of_versoLights_3() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoLights_3)); }
	inline String_t* get_versoLights_3() const { return ___versoLights_3; }
	inline String_t** get_address_of_versoLights_3() { return &___versoLights_3; }
	inline void set_versoLights_3(String_t* value)
	{
		___versoLights_3 = value;
		Il2CppCodeGenWriteBarrier(&___versoLights_3, value);
	}

	inline static int32_t get_offset_of_versoStabilityControl_4() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoStabilityControl_4)); }
	inline String_t* get_versoStabilityControl_4() const { return ___versoStabilityControl_4; }
	inline String_t** get_address_of_versoStabilityControl_4() { return &___versoStabilityControl_4; }
	inline void set_versoStabilityControl_4(String_t* value)
	{
		___versoStabilityControl_4 = value;
		Il2CppCodeGenWriteBarrier(&___versoStabilityControl_4, value);
	}

	inline static int32_t get_offset_of_versoParkingAssist_5() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoParkingAssist_5)); }
	inline String_t* get_versoParkingAssist_5() const { return ___versoParkingAssist_5; }
	inline String_t** get_address_of_versoParkingAssist_5() { return &___versoParkingAssist_5; }
	inline void set_versoParkingAssist_5(String_t* value)
	{
		___versoParkingAssist_5 = value;
		Il2CppCodeGenWriteBarrier(&___versoParkingAssist_5, value);
	}

	inline static int32_t get_offset_of_versoMirror_6() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoMirror_6)); }
	inline String_t* get_versoMirror_6() const { return ___versoMirror_6; }
	inline String_t** get_address_of_versoMirror_6() { return &___versoMirror_6; }
	inline void set_versoMirror_6(String_t* value)
	{
		___versoMirror_6 = value;
		Il2CppCodeGenWriteBarrier(&___versoMirror_6, value);
	}

	inline static int32_t get_offset_of_versoVoiceCommand_7() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoVoiceCommand_7)); }
	inline String_t* get_versoVoiceCommand_7() const { return ___versoVoiceCommand_7; }
	inline String_t** get_address_of_versoVoiceCommand_7() { return &___versoVoiceCommand_7; }
	inline void set_versoVoiceCommand_7(String_t* value)
	{
		___versoVoiceCommand_7 = value;
		Il2CppCodeGenWriteBarrier(&___versoVoiceCommand_7, value);
	}

	inline static int32_t get_offset_of_versoMode_8() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoMode_8)); }
	inline String_t* get_versoMode_8() const { return ___versoMode_8; }
	inline String_t** get_address_of_versoMode_8() { return &___versoMode_8; }
	inline void set_versoMode_8(String_t* value)
	{
		___versoMode_8 = value;
		Il2CppCodeGenWriteBarrier(&___versoMode_8, value);
	}

	inline static int32_t get_offset_of_versoPhone_9() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoPhone_9)); }
	inline String_t* get_versoPhone_9() const { return ___versoPhone_9; }
	inline String_t** get_address_of_versoPhone_9() { return &___versoPhone_9; }
	inline void set_versoPhone_9(String_t* value)
	{
		___versoPhone_9 = value;
		Il2CppCodeGenWriteBarrier(&___versoPhone_9, value);
	}

	inline static int32_t get_offset_of_versoDispTrip_10() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoDispTrip_10)); }
	inline String_t* get_versoDispTrip_10() const { return ___versoDispTrip_10; }
	inline String_t** get_address_of_versoDispTrip_10() { return &___versoDispTrip_10; }
	inline void set_versoDispTrip_10(String_t* value)
	{
		___versoDispTrip_10 = value;
		Il2CppCodeGenWriteBarrier(&___versoDispTrip_10, value);
	}

	inline static int32_t get_offset_of_versoSpeedLimit_11() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoSpeedLimit_11)); }
	inline String_t* get_versoSpeedLimit_11() const { return ___versoSpeedLimit_11; }
	inline String_t** get_address_of_versoSpeedLimit_11() { return &___versoSpeedLimit_11; }
	inline void set_versoSpeedLimit_11(String_t* value)
	{
		___versoSpeedLimit_11 = value;
		Il2CppCodeGenWriteBarrier(&___versoSpeedLimit_11, value);
	}

	inline static int32_t get_offset_of_versoSpeedRegulator_12() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoSpeedRegulator_12)); }
	inline String_t* get_versoSpeedRegulator_12() const { return ___versoSpeedRegulator_12; }
	inline String_t** get_address_of_versoSpeedRegulator_12() { return &___versoSpeedRegulator_12; }
	inline void set_versoSpeedRegulator_12(String_t* value)
	{
		___versoSpeedRegulator_12 = value;
		Il2CppCodeGenWriteBarrier(&___versoSpeedRegulator_12, value);
	}

	inline static int32_t get_offset_of_versoLeftTachometer_13() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoLeftTachometer_13)); }
	inline String_t* get_versoLeftTachometer_13() const { return ___versoLeftTachometer_13; }
	inline String_t** get_address_of_versoLeftTachometer_13() { return &___versoLeftTachometer_13; }
	inline void set_versoLeftTachometer_13(String_t* value)
	{
		___versoLeftTachometer_13 = value;
		Il2CppCodeGenWriteBarrier(&___versoLeftTachometer_13, value);
	}

	inline static int32_t get_offset_of_versoRightTachometer_14() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoRightTachometer_14)); }
	inline String_t* get_versoRightTachometer_14() const { return ___versoRightTachometer_14; }
	inline String_t** get_address_of_versoRightTachometer_14() { return &___versoRightTachometer_14; }
	inline void set_versoRightTachometer_14(String_t* value)
	{
		___versoRightTachometer_14 = value;
		Il2CppCodeGenWriteBarrier(&___versoRightTachometer_14, value);
	}

	inline static int32_t get_offset_of_versoCenterScreen_15() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoCenterScreen_15)); }
	inline String_t* get_versoCenterScreen_15() const { return ___versoCenterScreen_15; }
	inline String_t** get_address_of_versoCenterScreen_15() { return &___versoCenterScreen_15; }
	inline void set_versoCenterScreen_15(String_t* value)
	{
		___versoCenterScreen_15 = value;
		Il2CppCodeGenWriteBarrier(&___versoCenterScreen_15, value);
	}

	inline static int32_t get_offset_of_versoGPSLeftWheel_16() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoGPSLeftWheel_16)); }
	inline String_t* get_versoGPSLeftWheel_16() const { return ___versoGPSLeftWheel_16; }
	inline String_t** get_address_of_versoGPSLeftWheel_16() { return &___versoGPSLeftWheel_16; }
	inline void set_versoGPSLeftWheel_16(String_t* value)
	{
		___versoGPSLeftWheel_16 = value;
		Il2CppCodeGenWriteBarrier(&___versoGPSLeftWheel_16, value);
	}

	inline static int32_t get_offset_of_versoGPSRightWheel_17() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoGPSRightWheel_17)); }
	inline String_t* get_versoGPSRightWheel_17() const { return ___versoGPSRightWheel_17; }
	inline String_t** get_address_of_versoGPSRightWheel_17() { return &___versoGPSRightWheel_17; }
	inline void set_versoGPSRightWheel_17(String_t* value)
	{
		___versoGPSRightWheel_17 = value;
		Il2CppCodeGenWriteBarrier(&___versoGPSRightWheel_17, value);
	}

	inline static int32_t get_offset_of_versoGPSLeftMenu_18() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoGPSLeftMenu_18)); }
	inline String_t* get_versoGPSLeftMenu_18() const { return ___versoGPSLeftMenu_18; }
	inline String_t** get_address_of_versoGPSLeftMenu_18() { return &___versoGPSLeftMenu_18; }
	inline void set_versoGPSLeftMenu_18(String_t* value)
	{
		___versoGPSLeftMenu_18 = value;
		Il2CppCodeGenWriteBarrier(&___versoGPSLeftMenu_18, value);
	}

	inline static int32_t get_offset_of_versoGPSRightMenu_19() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoGPSRightMenu_19)); }
	inline String_t* get_versoGPSRightMenu_19() const { return ___versoGPSRightMenu_19; }
	inline String_t** get_address_of_versoGPSRightMenu_19() { return &___versoGPSRightMenu_19; }
	inline void set_versoGPSRightMenu_19(String_t* value)
	{
		___versoGPSRightMenu_19 = value;
		Il2CppCodeGenWriteBarrier(&___versoGPSRightMenu_19, value);
	}

	inline static int32_t get_offset_of_versoACLeftMenu_20() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoACLeftMenu_20)); }
	inline String_t* get_versoACLeftMenu_20() const { return ___versoACLeftMenu_20; }
	inline String_t** get_address_of_versoACLeftMenu_20() { return &___versoACLeftMenu_20; }
	inline void set_versoACLeftMenu_20(String_t* value)
	{
		___versoACLeftMenu_20 = value;
		Il2CppCodeGenWriteBarrier(&___versoACLeftMenu_20, value);
	}

	inline static int32_t get_offset_of_versoACCenterMenu_21() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoACCenterMenu_21)); }
	inline String_t* get_versoACCenterMenu_21() const { return ___versoACCenterMenu_21; }
	inline String_t** get_address_of_versoACCenterMenu_21() { return &___versoACCenterMenu_21; }
	inline void set_versoACCenterMenu_21(String_t* value)
	{
		___versoACCenterMenu_21 = value;
		Il2CppCodeGenWriteBarrier(&___versoACCenterMenu_21, value);
	}

	inline static int32_t get_offset_of_versoACRightMenu_22() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoACRightMenu_22)); }
	inline String_t* get_versoACRightMenu_22() const { return ___versoACRightMenu_22; }
	inline String_t** get_address_of_versoACRightMenu_22() { return &___versoACRightMenu_22; }
	inline void set_versoACRightMenu_22(String_t* value)
	{
		___versoACRightMenu_22 = value;
		Il2CppCodeGenWriteBarrier(&___versoACRightMenu_22, value);
	}

	inline static int32_t get_offset_of_versoAirbag_23() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoAirbag_23)); }
	inline String_t* get_versoAirbag_23() const { return ___versoAirbag_23; }
	inline String_t** get_address_of_versoAirbag_23() { return &___versoAirbag_23; }
	inline void set_versoAirbag_23(String_t* value)
	{
		___versoAirbag_23 = value;
		Il2CppCodeGenWriteBarrier(&___versoAirbag_23, value);
	}

	inline static int32_t get_offset_of_versoWindow_24() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoWindow_24)); }
	inline String_t* get_versoWindow_24() const { return ___versoWindow_24; }
	inline String_t** get_address_of_versoWindow_24() { return &___versoWindow_24; }
	inline void set_versoWindow_24(String_t* value)
	{
		___versoWindow_24 = value;
		Il2CppCodeGenWriteBarrier(&___versoWindow_24, value);
	}

	inline static int32_t get_offset_of_versoPower_25() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoPower_25)); }
	inline String_t* get_versoPower_25() const { return ___versoPower_25; }
	inline String_t** get_address_of_versoPower_25() { return &___versoPower_25; }
	inline void set_versoPower_25(String_t* value)
	{
		___versoPower_25 = value;
		Il2CppCodeGenWriteBarrier(&___versoPower_25, value);
	}

	inline static int32_t get_offset_of_versoA_OFF_26() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___versoA_OFF_26)); }
	inline String_t* get_versoA_OFF_26() const { return ___versoA_OFF_26; }
	inline String_t** get_address_of_versoA_OFF_26() { return &___versoA_OFF_26; }
	inline void set_versoA_OFF_26(String_t* value)
	{
		___versoA_OFF_26 = value;
		Il2CppCodeGenWriteBarrier(&___versoA_OFF_26, value);
	}

	inline static int32_t get_offset_of_doorDescr_27() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___doorDescr_27)); }
	inline String_t* get_doorDescr_27() const { return ___doorDescr_27; }
	inline String_t** get_address_of_doorDescr_27() { return &___doorDescr_27; }
	inline void set_doorDescr_27(String_t* value)
	{
		___doorDescr_27 = value;
		Il2CppCodeGenWriteBarrier(&___doorDescr_27, value);
	}

	inline static int32_t get_offset_of_mirrorDescr_28() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___mirrorDescr_28)); }
	inline String_t* get_mirrorDescr_28() const { return ___mirrorDescr_28; }
	inline String_t** get_address_of_mirrorDescr_28() { return &___mirrorDescr_28; }
	inline void set_mirrorDescr_28(String_t* value)
	{
		___mirrorDescr_28 = value;
		Il2CppCodeGenWriteBarrier(&___mirrorDescr_28, value);
	}

	inline static int32_t get_offset_of_tires_preCollissionDescr_29() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___tires_preCollissionDescr_29)); }
	inline String_t* get_tires_preCollissionDescr_29() const { return ___tires_preCollissionDescr_29; }
	inline String_t** get_address_of_tires_preCollissionDescr_29() { return &___tires_preCollissionDescr_29; }
	inline void set_tires_preCollissionDescr_29(String_t* value)
	{
		___tires_preCollissionDescr_29 = value;
		Il2CppCodeGenWriteBarrier(&___tires_preCollissionDescr_29, value);
	}

	inline static int32_t get_offset_of_volume_ModeDescr_30() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___volume_ModeDescr_30)); }
	inline String_t* get_volume_ModeDescr_30() const { return ___volume_ModeDescr_30; }
	inline String_t** get_address_of_volume_ModeDescr_30() { return &___volume_ModeDescr_30; }
	inline void set_volume_ModeDescr_30(String_t* value)
	{
		___volume_ModeDescr_30 = value;
		Il2CppCodeGenWriteBarrier(&___volume_ModeDescr_30, value);
	}

	inline static int32_t get_offset_of_phone_LineCrossDescr_31() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___phone_LineCrossDescr_31)); }
	inline String_t* get_phone_LineCrossDescr_31() const { return ___phone_LineCrossDescr_31; }
	inline String_t** get_address_of_phone_LineCrossDescr_31() { return &___phone_LineCrossDescr_31; }
	inline void set_phone_LineCrossDescr_31(String_t* value)
	{
		___phone_LineCrossDescr_31 = value;
		Il2CppCodeGenWriteBarrier(&___phone_LineCrossDescr_31, value);
	}

	inline static int32_t get_offset_of_speedRegulatorDescr_32() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___speedRegulatorDescr_32)); }
	inline String_t* get_speedRegulatorDescr_32() const { return ___speedRegulatorDescr_32; }
	inline String_t** get_address_of_speedRegulatorDescr_32() { return &___speedRegulatorDescr_32; }
	inline void set_speedRegulatorDescr_32(String_t* value)
	{
		___speedRegulatorDescr_32 = value;
		Il2CppCodeGenWriteBarrier(&___speedRegulatorDescr_32, value);
	}

	inline static int32_t get_offset_of_warning_airbag_seatbeltDescr_33() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___warning_airbag_seatbeltDescr_33)); }
	inline String_t* get_warning_airbag_seatbeltDescr_33() const { return ___warning_airbag_seatbeltDescr_33; }
	inline String_t** get_address_of_warning_airbag_seatbeltDescr_33() { return &___warning_airbag_seatbeltDescr_33; }
	inline void set_warning_airbag_seatbeltDescr_33(String_t* value)
	{
		___warning_airbag_seatbeltDescr_33 = value;
		Il2CppCodeGenWriteBarrier(&___warning_airbag_seatbeltDescr_33, value);
	}

	inline static int32_t get_offset_of_ac_DualDescr_34() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___ac_DualDescr_34)); }
	inline String_t* get_ac_DualDescr_34() const { return ___ac_DualDescr_34; }
	inline String_t** get_address_of_ac_DualDescr_34() { return &___ac_DualDescr_34; }
	inline void set_ac_DualDescr_34(String_t* value)
	{
		___ac_DualDescr_34 = value;
		Il2CppCodeGenWriteBarrier(&___ac_DualDescr_34, value);
	}

	inline static int32_t get_offset_of_auto_OffDescr_35() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___auto_OffDescr_35)); }
	inline String_t* get_auto_OffDescr_35() const { return ___auto_OffDescr_35; }
	inline String_t** get_address_of_auto_OffDescr_35() { return &___auto_OffDescr_35; }
	inline void set_auto_OffDescr_35(String_t* value)
	{
		___auto_OffDescr_35 = value;
		Il2CppCodeGenWriteBarrier(&___auto_OffDescr_35, value);
	}

	inline static int32_t get_offset_of_front_rearDescr_36() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___front_rearDescr_36)); }
	inline String_t* get_front_rearDescr_36() const { return ___front_rearDescr_36; }
	inline String_t** get_address_of_front_rearDescr_36() { return &___front_rearDescr_36; }
	inline void set_front_rearDescr_36(String_t* value)
	{
		___front_rearDescr_36 = value;
		Il2CppCodeGenWriteBarrier(&___front_rearDescr_36, value);
	}

	inline static int32_t get_offset_of_usb_12VoltDescr_37() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___usb_12VoltDescr_37)); }
	inline String_t* get_usb_12VoltDescr_37() const { return ___usb_12VoltDescr_37; }
	inline String_t** get_address_of_usb_12VoltDescr_37() { return &___usb_12VoltDescr_37; }
	inline void set_usb_12VoltDescr_37(String_t* value)
	{
		___usb_12VoltDescr_37 = value;
		Il2CppCodeGenWriteBarrier(&___usb_12VoltDescr_37, value);
	}

	inline static int32_t get_offset_of_mode_stabilityCtrlDescr_38() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___mode_stabilityCtrlDescr_38)); }
	inline String_t* get_mode_stabilityCtrlDescr_38() const { return ___mode_stabilityCtrlDescr_38; }
	inline String_t** get_address_of_mode_stabilityCtrlDescr_38() { return &___mode_stabilityCtrlDescr_38; }
	inline void set_mode_stabilityCtrlDescr_38(String_t* value)
	{
		___mode_stabilityCtrlDescr_38 = value;
		Il2CppCodeGenWriteBarrier(&___mode_stabilityCtrlDescr_38, value);
	}

	inline static int32_t get_offset_of_enginePowerDescr_39() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___enginePowerDescr_39)); }
	inline String_t* get_enginePowerDescr_39() const { return ___enginePowerDescr_39; }
	inline String_t** get_address_of_enginePowerDescr_39() { return &___enginePowerDescr_39; }
	inline void set_enginePowerDescr_39(String_t* value)
	{
		___enginePowerDescr_39 = value;
		Il2CppCodeGenWriteBarrier(&___enginePowerDescr_39, value);
	}

	inline static int32_t get_offset_of_fuelTankLidDescr_40() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___fuelTankLidDescr_40)); }
	inline String_t* get_fuelTankLidDescr_40() const { return ___fuelTankLidDescr_40; }
	inline String_t** get_address_of_fuelTankLidDescr_40() { return &___fuelTankLidDescr_40; }
	inline void set_fuelTankLidDescr_40(String_t* value)
	{
		___fuelTankLidDescr_40 = value;
		Il2CppCodeGenWriteBarrier(&___fuelTankLidDescr_40, value);
	}

	inline static int32_t get_offset_of_hoodOpenBtnDescr_41() { return static_cast<int32_t>(offsetof(DescriptionsStrings_t1951003243, ___hoodOpenBtnDescr_41)); }
	inline String_t* get_hoodOpenBtnDescr_41() const { return ___hoodOpenBtnDescr_41; }
	inline String_t** get_address_of_hoodOpenBtnDescr_41() { return &___hoodOpenBtnDescr_41; }
	inline void set_hoodOpenBtnDescr_41(String_t* value)
	{
		___hoodOpenBtnDescr_41 = value;
		Il2CppCodeGenWriteBarrier(&___hoodOpenBtnDescr_41, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
