﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DescriptionsStrings
struct DescriptionsStrings_t1951003243;

#include "codegen/il2cpp-codegen.h"

// System.Void DescriptionsStrings::.ctor()
extern "C"  void DescriptionsStrings__ctor_m2006362464 (DescriptionsStrings_t1951003243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
