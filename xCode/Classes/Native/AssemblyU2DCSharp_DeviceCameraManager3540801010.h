﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t3903132647;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceCameraManager
struct  DeviceCameraManager_t3540801010  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean DeviceCameraManager::autofocusEnabled
	bool ___autofocusEnabled_2;
	// System.Boolean DeviceCameraManager::flashEnabled
	bool ___flashEnabled_3;
	// UITexture DeviceCameraManager::flashBtnUITex
	UITexture_t3903132647 * ___flashBtnUITex_4;
	// UnityEngine.Texture2D DeviceCameraManager::flashOnTex
	Texture2D_t3884108195 * ___flashOnTex_5;
	// UnityEngine.Texture2D DeviceCameraManager::flashOffTex
	Texture2D_t3884108195 * ___flashOffTex_6;
	// UnityEngine.TextMesh DeviceCameraManager::textMesh
	TextMesh_t2567681854 * ___textMesh_7;

public:
	inline static int32_t get_offset_of_autofocusEnabled_2() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t3540801010, ___autofocusEnabled_2)); }
	inline bool get_autofocusEnabled_2() const { return ___autofocusEnabled_2; }
	inline bool* get_address_of_autofocusEnabled_2() { return &___autofocusEnabled_2; }
	inline void set_autofocusEnabled_2(bool value)
	{
		___autofocusEnabled_2 = value;
	}

	inline static int32_t get_offset_of_flashEnabled_3() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t3540801010, ___flashEnabled_3)); }
	inline bool get_flashEnabled_3() const { return ___flashEnabled_3; }
	inline bool* get_address_of_flashEnabled_3() { return &___flashEnabled_3; }
	inline void set_flashEnabled_3(bool value)
	{
		___flashEnabled_3 = value;
	}

	inline static int32_t get_offset_of_flashBtnUITex_4() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t3540801010, ___flashBtnUITex_4)); }
	inline UITexture_t3903132647 * get_flashBtnUITex_4() const { return ___flashBtnUITex_4; }
	inline UITexture_t3903132647 ** get_address_of_flashBtnUITex_4() { return &___flashBtnUITex_4; }
	inline void set_flashBtnUITex_4(UITexture_t3903132647 * value)
	{
		___flashBtnUITex_4 = value;
		Il2CppCodeGenWriteBarrier(&___flashBtnUITex_4, value);
	}

	inline static int32_t get_offset_of_flashOnTex_5() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t3540801010, ___flashOnTex_5)); }
	inline Texture2D_t3884108195 * get_flashOnTex_5() const { return ___flashOnTex_5; }
	inline Texture2D_t3884108195 ** get_address_of_flashOnTex_5() { return &___flashOnTex_5; }
	inline void set_flashOnTex_5(Texture2D_t3884108195 * value)
	{
		___flashOnTex_5 = value;
		Il2CppCodeGenWriteBarrier(&___flashOnTex_5, value);
	}

	inline static int32_t get_offset_of_flashOffTex_6() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t3540801010, ___flashOffTex_6)); }
	inline Texture2D_t3884108195 * get_flashOffTex_6() const { return ___flashOffTex_6; }
	inline Texture2D_t3884108195 ** get_address_of_flashOffTex_6() { return &___flashOffTex_6; }
	inline void set_flashOffTex_6(Texture2D_t3884108195 * value)
	{
		___flashOffTex_6 = value;
		Il2CppCodeGenWriteBarrier(&___flashOffTex_6, value);
	}

	inline static int32_t get_offset_of_textMesh_7() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t3540801010, ___textMesh_7)); }
	inline TextMesh_t2567681854 * get_textMesh_7() const { return ___textMesh_7; }
	inline TextMesh_t2567681854 ** get_address_of_textMesh_7() { return &___textMesh_7; }
	inline void set_textMesh_7(TextMesh_t2567681854 * value)
	{
		___textMesh_7 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
