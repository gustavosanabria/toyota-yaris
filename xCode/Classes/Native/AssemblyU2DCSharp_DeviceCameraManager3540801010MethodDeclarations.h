﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceCameraManager
struct DeviceCameraManager_t3540801010;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceCameraManager::.ctor()
extern "C"  void DeviceCameraManager__ctor_m3431878073 (DeviceCameraManager_t3540801010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::Awake()
extern "C"  void DeviceCameraManager_Awake_m3669483292 (DeviceCameraManager_t3540801010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::EnableDeviceFlash()
extern "C"  void DeviceCameraManager_EnableDeviceFlash_m3942808014 (DeviceCameraManager_t3540801010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::OnApplicationFocus(System.Boolean)
extern "C"  void DeviceCameraManager_OnApplicationFocus_m1428551817 (DeviceCameraManager_t3540801010 * __this, bool ___focused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::Focus()
extern "C"  void DeviceCameraManager_Focus_m3585159599 (DeviceCameraManager_t3540801010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
