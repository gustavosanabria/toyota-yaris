﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadObbExample
struct DownloadObbExample_t1827715139;

#include "codegen/il2cpp-codegen.h"

// System.Void DownloadObbExample::.ctor()
extern "C"  void DownloadObbExample__ctor_m2981946296 (DownloadObbExample_t1827715139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
