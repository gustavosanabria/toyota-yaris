﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FileManager
struct FileManager_t106919537;

#include "codegen/il2cpp-codegen.h"

// System.Void FileManager::.ctor()
extern "C"  void FileManager__ctor_m3057366042 (FileManager_t106919537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
