﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputMod15847059.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GazeInputModule
struct  GazeInputModule_t2064533489  : public BaseInputModule_t15847059
{
public:
	// System.Boolean GazeInputModule::vrModeOnly
	bool ___vrModeOnly_6;
	// UnityEngine.GameObject GazeInputModule::cursor
	GameObject_t3674682005 * ___cursor_7;
	// System.Boolean GazeInputModule::showCursor
	bool ___showCursor_8;
	// System.Boolean GazeInputModule::scaleCursorSize
	bool ___scaleCursorSize_9;
	// System.Single GazeInputModule::clickTime
	float ___clickTime_10;
	// UnityEngine.Vector2 GazeInputModule::hotspot
	Vector2_t4282066565  ___hotspot_11;
	// UnityEngine.EventSystems.PointerEventData GazeInputModule::pointerData
	PointerEventData_t1848751023 * ___pointerData_12;
	// UnityEngine.Vector2 GazeInputModule::lastHeadPose
	Vector2_t4282066565  ___lastHeadPose_13;

public:
	inline static int32_t get_offset_of_vrModeOnly_6() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___vrModeOnly_6)); }
	inline bool get_vrModeOnly_6() const { return ___vrModeOnly_6; }
	inline bool* get_address_of_vrModeOnly_6() { return &___vrModeOnly_6; }
	inline void set_vrModeOnly_6(bool value)
	{
		___vrModeOnly_6 = value;
	}

	inline static int32_t get_offset_of_cursor_7() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___cursor_7)); }
	inline GameObject_t3674682005 * get_cursor_7() const { return ___cursor_7; }
	inline GameObject_t3674682005 ** get_address_of_cursor_7() { return &___cursor_7; }
	inline void set_cursor_7(GameObject_t3674682005 * value)
	{
		___cursor_7 = value;
		Il2CppCodeGenWriteBarrier(&___cursor_7, value);
	}

	inline static int32_t get_offset_of_showCursor_8() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___showCursor_8)); }
	inline bool get_showCursor_8() const { return ___showCursor_8; }
	inline bool* get_address_of_showCursor_8() { return &___showCursor_8; }
	inline void set_showCursor_8(bool value)
	{
		___showCursor_8 = value;
	}

	inline static int32_t get_offset_of_scaleCursorSize_9() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___scaleCursorSize_9)); }
	inline bool get_scaleCursorSize_9() const { return ___scaleCursorSize_9; }
	inline bool* get_address_of_scaleCursorSize_9() { return &___scaleCursorSize_9; }
	inline void set_scaleCursorSize_9(bool value)
	{
		___scaleCursorSize_9 = value;
	}

	inline static int32_t get_offset_of_clickTime_10() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___clickTime_10)); }
	inline float get_clickTime_10() const { return ___clickTime_10; }
	inline float* get_address_of_clickTime_10() { return &___clickTime_10; }
	inline void set_clickTime_10(float value)
	{
		___clickTime_10 = value;
	}

	inline static int32_t get_offset_of_hotspot_11() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___hotspot_11)); }
	inline Vector2_t4282066565  get_hotspot_11() const { return ___hotspot_11; }
	inline Vector2_t4282066565 * get_address_of_hotspot_11() { return &___hotspot_11; }
	inline void set_hotspot_11(Vector2_t4282066565  value)
	{
		___hotspot_11 = value;
	}

	inline static int32_t get_offset_of_pointerData_12() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___pointerData_12)); }
	inline PointerEventData_t1848751023 * get_pointerData_12() const { return ___pointerData_12; }
	inline PointerEventData_t1848751023 ** get_address_of_pointerData_12() { return &___pointerData_12; }
	inline void set_pointerData_12(PointerEventData_t1848751023 * value)
	{
		___pointerData_12 = value;
		Il2CppCodeGenWriteBarrier(&___pointerData_12, value);
	}

	inline static int32_t get_offset_of_lastHeadPose_13() { return static_cast<int32_t>(offsetof(GazeInputModule_t2064533489, ___lastHeadPose_13)); }
	inline Vector2_t4282066565  get_lastHeadPose_13() const { return ___lastHeadPose_13; }
	inline Vector2_t4282066565 * get_address_of_lastHeadPose_13() { return &___lastHeadPose_13; }
	inline void set_lastHeadPose_13(Vector2_t4282066565  value)
	{
		___lastHeadPose_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
