﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlDocument
struct XmlDocument_t730752740;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t405523272;
// GlosarioController
struct GlosarioController_t3609734652;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlosarioController
struct  GlosarioController_t3609734652  : public Il2CppObject
{
public:
	// System.Xml.XmlDocument GlosarioController::_xml
	XmlDocument_t730752740 * ____xml_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> GlosarioController::_dictionary
	Il2CppObject* ____dictionary_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> GlosarioController::_defTitleDic
	Il2CppObject* ____defTitleDic_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> GlosarioController::_palabraKeywordDic
	Il2CppObject* ____palabraKeywordDic_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> GlosarioController::_vrTargetsDic
	Il2CppObject* ____vrTargetsDic_4;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> GlosarioController::_tutCatDic
	Il2CppObject* ____tutCatDic_5;

public:
	inline static int32_t get_offset_of__xml_0() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652, ____xml_0)); }
	inline XmlDocument_t730752740 * get__xml_0() const { return ____xml_0; }
	inline XmlDocument_t730752740 ** get_address_of__xml_0() { return &____xml_0; }
	inline void set__xml_0(XmlDocument_t730752740 * value)
	{
		____xml_0 = value;
		Il2CppCodeGenWriteBarrier(&____xml_0, value);
	}

	inline static int32_t get_offset_of__dictionary_1() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652, ____dictionary_1)); }
	inline Il2CppObject* get__dictionary_1() const { return ____dictionary_1; }
	inline Il2CppObject** get_address_of__dictionary_1() { return &____dictionary_1; }
	inline void set__dictionary_1(Il2CppObject* value)
	{
		____dictionary_1 = value;
		Il2CppCodeGenWriteBarrier(&____dictionary_1, value);
	}

	inline static int32_t get_offset_of__defTitleDic_2() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652, ____defTitleDic_2)); }
	inline Il2CppObject* get__defTitleDic_2() const { return ____defTitleDic_2; }
	inline Il2CppObject** get_address_of__defTitleDic_2() { return &____defTitleDic_2; }
	inline void set__defTitleDic_2(Il2CppObject* value)
	{
		____defTitleDic_2 = value;
		Il2CppCodeGenWriteBarrier(&____defTitleDic_2, value);
	}

	inline static int32_t get_offset_of__palabraKeywordDic_3() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652, ____palabraKeywordDic_3)); }
	inline Il2CppObject* get__palabraKeywordDic_3() const { return ____palabraKeywordDic_3; }
	inline Il2CppObject** get_address_of__palabraKeywordDic_3() { return &____palabraKeywordDic_3; }
	inline void set__palabraKeywordDic_3(Il2CppObject* value)
	{
		____palabraKeywordDic_3 = value;
		Il2CppCodeGenWriteBarrier(&____palabraKeywordDic_3, value);
	}

	inline static int32_t get_offset_of__vrTargetsDic_4() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652, ____vrTargetsDic_4)); }
	inline Il2CppObject* get__vrTargetsDic_4() const { return ____vrTargetsDic_4; }
	inline Il2CppObject** get_address_of__vrTargetsDic_4() { return &____vrTargetsDic_4; }
	inline void set__vrTargetsDic_4(Il2CppObject* value)
	{
		____vrTargetsDic_4 = value;
		Il2CppCodeGenWriteBarrier(&____vrTargetsDic_4, value);
	}

	inline static int32_t get_offset_of__tutCatDic_5() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652, ____tutCatDic_5)); }
	inline Il2CppObject* get__tutCatDic_5() const { return ____tutCatDic_5; }
	inline Il2CppObject** get_address_of__tutCatDic_5() { return &____tutCatDic_5; }
	inline void set__tutCatDic_5(Il2CppObject* value)
	{
		____tutCatDic_5 = value;
		Il2CppCodeGenWriteBarrier(&____tutCatDic_5, value);
	}
};

struct GlosarioController_t3609734652_StaticFields
{
public:
	// GlosarioController GlosarioController::_instance
	GlosarioController_t3609734652 * ____instance_6;

public:
	inline static int32_t get_offset_of__instance_6() { return static_cast<int32_t>(offsetof(GlosarioController_t3609734652_StaticFields, ____instance_6)); }
	inline GlosarioController_t3609734652 * get__instance_6() const { return ____instance_6; }
	inline GlosarioController_t3609734652 ** get_address_of__instance_6() { return &____instance_6; }
	inline void set__instance_6(GlosarioController_t3609734652 * value)
	{
		____instance_6 = value;
		Il2CppCodeGenWriteBarrier(&____instance_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
