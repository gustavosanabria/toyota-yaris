﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlosarioController
struct GlosarioController_t3609734652;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GlosarioController3609734652.h"
#include "mscorlib_System_String7231557.h"

// System.Void GlosarioController::.ctor()
extern "C"  void GlosarioController__ctor_m4012625439 (GlosarioController_t3609734652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioController::set_Instance(GlosarioController)
extern "C"  void GlosarioController_set_Instance_m1874913435 (Il2CppObject * __this /* static, unused */, GlosarioController_t3609734652 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlosarioController GlosarioController::get_Instance()
extern "C"  GlosarioController_t3609734652 * GlosarioController_get_Instance_m4020403940 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioController::LoadGlossary(System.String)
extern "C"  void GlosarioController_LoadGlossary_m2887098937 (GlosarioController_t3609734652 * __this, String_t* ___lang_code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GlosarioController::GetWords()
extern "C"  List_1_t1375417109 * GlosarioController_GetWords_m1032171433 (GlosarioController_t3609734652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlosarioController::GetDefinition(System.String)
extern "C"  String_t* GlosarioController_GetDefinition_m2911982239 (GlosarioController_t3609734652 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlosarioController::GetDefinitionTitle(System.String)
extern "C"  String_t* GlosarioController_GetDefinitionTitle_m763001387 (GlosarioController_t3609734652 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlosarioController::GetDefinitionTarget(System.String)
extern "C"  String_t* GlosarioController_GetDefinitionTarget_m2099185230 (GlosarioController_t3609734652 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlosarioController::GetDefinitionTut(System.String)
extern "C"  String_t* GlosarioController_GetDefinitionTut_m1994366352 (GlosarioController_t3609734652 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GlosarioController::Search(System.String)
extern "C"  List_1_t1375417109 * GlosarioController_Search_m2245596004 (GlosarioController_t3609734652 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioController::Clear()
extern "C"  void GlosarioController_Clear_m1418758730 (GlosarioController_t3609734652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
