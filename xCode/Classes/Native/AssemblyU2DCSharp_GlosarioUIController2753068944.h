﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.Transform
struct Transform_t1659122786;
// UILabel
struct UILabel_t291504320;
// UIScrollBar
struct UIScrollBar_t2839103954;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlosarioUIController
struct  GlosarioUIController_t2753068944  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject GlosarioUIController::_panelIdioma
	GameObject_t3674682005 * ____panelIdioma_2;
	// UnityEngine.GameObject GlosarioUIController::_panelGlosario
	GameObject_t3674682005 * ____panelGlosario_3;
	// UnityEngine.GameObject GlosarioUIController::_body
	GameObject_t3674682005 * ____body_4;
	// UnityEngine.GameObject GlosarioUIController::_panelDefinicion
	GameObject_t3674682005 * ____panelDefinicion_5;
	// UnityEngine.UI.Text GlosarioUIController::txtSearch
	Text_t9039225 * ___txtSearch_6;
	// UnityEngine.Transform GlosarioUIController::buttonsParent
	Transform_t1659122786 * ___buttonsParent_7;
	// UILabel GlosarioUIController::label
	UILabel_t291504320 * ___label_8;
	// UIScrollBar GlosarioUIController::scrollBar
	UIScrollBar_t2839103954 * ___scrollBar_9;
	// System.Int32 GlosarioUIController::btnN
	int32_t ___btnN_10;

public:
	inline static int32_t get_offset_of__panelIdioma_2() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ____panelIdioma_2)); }
	inline GameObject_t3674682005 * get__panelIdioma_2() const { return ____panelIdioma_2; }
	inline GameObject_t3674682005 ** get_address_of__panelIdioma_2() { return &____panelIdioma_2; }
	inline void set__panelIdioma_2(GameObject_t3674682005 * value)
	{
		____panelIdioma_2 = value;
		Il2CppCodeGenWriteBarrier(&____panelIdioma_2, value);
	}

	inline static int32_t get_offset_of__panelGlosario_3() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ____panelGlosario_3)); }
	inline GameObject_t3674682005 * get__panelGlosario_3() const { return ____panelGlosario_3; }
	inline GameObject_t3674682005 ** get_address_of__panelGlosario_3() { return &____panelGlosario_3; }
	inline void set__panelGlosario_3(GameObject_t3674682005 * value)
	{
		____panelGlosario_3 = value;
		Il2CppCodeGenWriteBarrier(&____panelGlosario_3, value);
	}

	inline static int32_t get_offset_of__body_4() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ____body_4)); }
	inline GameObject_t3674682005 * get__body_4() const { return ____body_4; }
	inline GameObject_t3674682005 ** get_address_of__body_4() { return &____body_4; }
	inline void set__body_4(GameObject_t3674682005 * value)
	{
		____body_4 = value;
		Il2CppCodeGenWriteBarrier(&____body_4, value);
	}

	inline static int32_t get_offset_of__panelDefinicion_5() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ____panelDefinicion_5)); }
	inline GameObject_t3674682005 * get__panelDefinicion_5() const { return ____panelDefinicion_5; }
	inline GameObject_t3674682005 ** get_address_of__panelDefinicion_5() { return &____panelDefinicion_5; }
	inline void set__panelDefinicion_5(GameObject_t3674682005 * value)
	{
		____panelDefinicion_5 = value;
		Il2CppCodeGenWriteBarrier(&____panelDefinicion_5, value);
	}

	inline static int32_t get_offset_of_txtSearch_6() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ___txtSearch_6)); }
	inline Text_t9039225 * get_txtSearch_6() const { return ___txtSearch_6; }
	inline Text_t9039225 ** get_address_of_txtSearch_6() { return &___txtSearch_6; }
	inline void set_txtSearch_6(Text_t9039225 * value)
	{
		___txtSearch_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtSearch_6, value);
	}

	inline static int32_t get_offset_of_buttonsParent_7() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ___buttonsParent_7)); }
	inline Transform_t1659122786 * get_buttonsParent_7() const { return ___buttonsParent_7; }
	inline Transform_t1659122786 ** get_address_of_buttonsParent_7() { return &___buttonsParent_7; }
	inline void set_buttonsParent_7(Transform_t1659122786 * value)
	{
		___buttonsParent_7 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsParent_7, value);
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ___label_8)); }
	inline UILabel_t291504320 * get_label_8() const { return ___label_8; }
	inline UILabel_t291504320 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(UILabel_t291504320 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier(&___label_8, value);
	}

	inline static int32_t get_offset_of_scrollBar_9() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ___scrollBar_9)); }
	inline UIScrollBar_t2839103954 * get_scrollBar_9() const { return ___scrollBar_9; }
	inline UIScrollBar_t2839103954 ** get_address_of_scrollBar_9() { return &___scrollBar_9; }
	inline void set_scrollBar_9(UIScrollBar_t2839103954 * value)
	{
		___scrollBar_9 = value;
		Il2CppCodeGenWriteBarrier(&___scrollBar_9, value);
	}

	inline static int32_t get_offset_of_btnN_10() { return static_cast<int32_t>(offsetof(GlosarioUIController_t2753068944, ___btnN_10)); }
	inline int32_t get_btnN_10() const { return ___btnN_10; }
	inline int32_t* get_address_of_btnN_10() { return &___btnN_10; }
	inline void set_btnN_10(int32_t value)
	{
		___btnN_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
