﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlosarioUIController
struct GlosarioUIController_t2753068944;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void GlosarioUIController::.ctor()
extern "C"  void GlosarioUIController__ctor_m1719557387 (GlosarioUIController_t2753068944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController::Start()
extern "C"  void GlosarioUIController_Start_m666695179 (GlosarioUIController_t2753068944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController::Load(System.String)
extern "C"  void GlosarioUIController_Load_m2705381155 (GlosarioUIController_t2753068944 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GlosarioUIController::LoadWords(System.Collections.Generic.List`1<System.String>)
extern "C"  Il2CppObject * GlosarioUIController_LoadWords_m176949688 (GlosarioUIController_t2753068944 * __this, List_1_t1375417109 * ___words0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController::OnClick_Row(System.String)
extern "C"  void GlosarioUIController_OnClick_Row_m4205609365 (GlosarioUIController_t2753068944 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController::Search()
extern "C"  void GlosarioUIController_Search_m3059583105 (GlosarioUIController_t2753068944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GlosarioUIController::LoadRow(System.String)
extern "C"  Il2CppObject * GlosarioUIController_LoadRow_m2913103405 (GlosarioUIController_t2753068944 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController::ClearList()
extern "C"  void GlosarioUIController_ClearList_m3335644148 (GlosarioUIController_t2753068944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController::DestroyAllButtons()
extern "C"  void GlosarioUIController_DestroyAllButtons_m949871075 (GlosarioUIController_t2753068944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
