﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// UIButton
struct UIButton_t179429094;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Object
struct Il2CppObject;
// GlosarioUIController
struct GlosarioUIController_t2753068944;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlosarioUIController/<LoadRow>c__Iterator2
struct  U3CLoadRowU3Ec__Iterator2_t3371403016  : public Il2CppObject
{
public:
	// UnityEngine.GameObject GlosarioUIController/<LoadRow>c__Iterator2::<row>__0
	GameObject_t3674682005 * ___U3CrowU3E__0_0;
	// System.String GlosarioUIController/<LoadRow>c__Iterator2::word
	String_t* ___word_1;
	// UIButton GlosarioUIController/<LoadRow>c__Iterator2::<btn>__1
	UIButton_t179429094 * ___U3CbtnU3E__1_2;
	// EventDelegate GlosarioUIController/<LoadRow>c__Iterator2::<del>__2
	EventDelegate_t4004424223 * ___U3CdelU3E__2_3;
	// System.Int32 GlosarioUIController/<LoadRow>c__Iterator2::$PC
	int32_t ___U24PC_4;
	// System.Object GlosarioUIController/<LoadRow>c__Iterator2::$current
	Il2CppObject * ___U24current_5;
	// System.String GlosarioUIController/<LoadRow>c__Iterator2::<$>word
	String_t* ___U3CU24U3Eword_6;
	// GlosarioUIController GlosarioUIController/<LoadRow>c__Iterator2::<>f__this
	GlosarioUIController_t2753068944 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CrowU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U3CrowU3E__0_0)); }
	inline GameObject_t3674682005 * get_U3CrowU3E__0_0() const { return ___U3CrowU3E__0_0; }
	inline GameObject_t3674682005 ** get_address_of_U3CrowU3E__0_0() { return &___U3CrowU3E__0_0; }
	inline void set_U3CrowU3E__0_0(GameObject_t3674682005 * value)
	{
		___U3CrowU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrowU3E__0_0, value);
	}

	inline static int32_t get_offset_of_word_1() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___word_1)); }
	inline String_t* get_word_1() const { return ___word_1; }
	inline String_t** get_address_of_word_1() { return &___word_1; }
	inline void set_word_1(String_t* value)
	{
		___word_1 = value;
		Il2CppCodeGenWriteBarrier(&___word_1, value);
	}

	inline static int32_t get_offset_of_U3CbtnU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U3CbtnU3E__1_2)); }
	inline UIButton_t179429094 * get_U3CbtnU3E__1_2() const { return ___U3CbtnU3E__1_2; }
	inline UIButton_t179429094 ** get_address_of_U3CbtnU3E__1_2() { return &___U3CbtnU3E__1_2; }
	inline void set_U3CbtnU3E__1_2(UIButton_t179429094 * value)
	{
		___U3CbtnU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbtnU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CdelU3E__2_3() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U3CdelU3E__2_3)); }
	inline EventDelegate_t4004424223 * get_U3CdelU3E__2_3() const { return ___U3CdelU3E__2_3; }
	inline EventDelegate_t4004424223 ** get_address_of_U3CdelU3E__2_3() { return &___U3CdelU3E__2_3; }
	inline void set_U3CdelU3E__2_3(EventDelegate_t4004424223 * value)
	{
		___U3CdelU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdelU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eword_6() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U3CU24U3Eword_6)); }
	inline String_t* get_U3CU24U3Eword_6() const { return ___U3CU24U3Eword_6; }
	inline String_t** get_address_of_U3CU24U3Eword_6() { return &___U3CU24U3Eword_6; }
	inline void set_U3CU24U3Eword_6(String_t* value)
	{
		___U3CU24U3Eword_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eword_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CLoadRowU3Ec__Iterator2_t3371403016, ___U3CU3Ef__this_7)); }
	inline GlosarioUIController_t2753068944 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline GlosarioUIController_t2753068944 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(GlosarioUIController_t2753068944 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
