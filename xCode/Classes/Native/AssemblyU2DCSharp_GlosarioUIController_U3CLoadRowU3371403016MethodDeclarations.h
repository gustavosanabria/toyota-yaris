﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlosarioUIController/<LoadRow>c__Iterator2
struct U3CLoadRowU3Ec__Iterator2_t3371403016;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GlosarioUIController/<LoadRow>c__Iterator2::.ctor()
extern "C"  void U3CLoadRowU3Ec__Iterator2__ctor_m354753171 (U3CLoadRowU3Ec__Iterator2_t3371403016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlosarioUIController/<LoadRow>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadRowU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4078094057 (U3CLoadRowU3Ec__Iterator2_t3371403016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlosarioUIController/<LoadRow>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadRowU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2082061437 (U3CLoadRowU3Ec__Iterator2_t3371403016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlosarioUIController/<LoadRow>c__Iterator2::MoveNext()
extern "C"  bool U3CLoadRowU3Ec__Iterator2_MoveNext_m1765772649 (U3CLoadRowU3Ec__Iterator2_t3371403016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController/<LoadRow>c__Iterator2::Dispose()
extern "C"  void U3CLoadRowU3Ec__Iterator2_Dispose_m1513664016 (U3CLoadRowU3Ec__Iterator2_t3371403016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController/<LoadRow>c__Iterator2::Reset()
extern "C"  void U3CLoadRowU3Ec__Iterator2_Reset_m2296153408 (U3CLoadRowU3Ec__Iterator2_t3371403016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
