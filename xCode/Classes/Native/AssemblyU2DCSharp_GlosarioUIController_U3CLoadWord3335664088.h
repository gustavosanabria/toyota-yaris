﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// GlosarioUIController
struct GlosarioUIController_t2753068944;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1395089879.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlosarioUIController/<LoadWords>c__Iterator1
struct  U3CLoadWordsU3Ec__Iterator1_t3335664088  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> GlosarioUIController/<LoadWords>c__Iterator1::words
	List_1_t1375417109 * ___words_0;
	// System.Collections.Generic.List`1/Enumerator<System.String> GlosarioUIController/<LoadWords>c__Iterator1::<$s_4>__0
	Enumerator_t1395089879  ___U3CU24s_4U3E__0_1;
	// System.String GlosarioUIController/<LoadWords>c__Iterator1::<word>__1
	String_t* ___U3CwordU3E__1_2;
	// System.Int32 GlosarioUIController/<LoadWords>c__Iterator1::$PC
	int32_t ___U24PC_3;
	// System.Object GlosarioUIController/<LoadWords>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// System.Collections.Generic.List`1<System.String> GlosarioUIController/<LoadWords>c__Iterator1::<$>words
	List_1_t1375417109 * ___U3CU24U3Ewords_5;
	// GlosarioUIController GlosarioUIController/<LoadWords>c__Iterator1::<>f__this
	GlosarioUIController_t2753068944 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_words_0() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___words_0)); }
	inline List_1_t1375417109 * get_words_0() const { return ___words_0; }
	inline List_1_t1375417109 ** get_address_of_words_0() { return &___words_0; }
	inline void set_words_0(List_1_t1375417109 * value)
	{
		___words_0 = value;
		Il2CppCodeGenWriteBarrier(&___words_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_4U3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___U3CU24s_4U3E__0_1)); }
	inline Enumerator_t1395089879  get_U3CU24s_4U3E__0_1() const { return ___U3CU24s_4U3E__0_1; }
	inline Enumerator_t1395089879 * get_address_of_U3CU24s_4U3E__0_1() { return &___U3CU24s_4U3E__0_1; }
	inline void set_U3CU24s_4U3E__0_1(Enumerator_t1395089879  value)
	{
		___U3CU24s_4U3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CwordU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___U3CwordU3E__1_2)); }
	inline String_t* get_U3CwordU3E__1_2() const { return ___U3CwordU3E__1_2; }
	inline String_t** get_address_of_U3CwordU3E__1_2() { return &___U3CwordU3E__1_2; }
	inline void set_U3CwordU3E__1_2(String_t* value)
	{
		___U3CwordU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwordU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ewords_5() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___U3CU24U3Ewords_5)); }
	inline List_1_t1375417109 * get_U3CU24U3Ewords_5() const { return ___U3CU24U3Ewords_5; }
	inline List_1_t1375417109 ** get_address_of_U3CU24U3Ewords_5() { return &___U3CU24U3Ewords_5; }
	inline void set_U3CU24U3Ewords_5(List_1_t1375417109 * value)
	{
		___U3CU24U3Ewords_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ewords_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CLoadWordsU3Ec__Iterator1_t3335664088, ___U3CU3Ef__this_6)); }
	inline GlosarioUIController_t2753068944 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline GlosarioUIController_t2753068944 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(GlosarioUIController_t2753068944 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
