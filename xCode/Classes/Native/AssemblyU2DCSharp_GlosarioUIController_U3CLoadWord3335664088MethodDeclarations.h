﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlosarioUIController/<LoadWords>c__Iterator1
struct U3CLoadWordsU3Ec__Iterator1_t3335664088;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GlosarioUIController/<LoadWords>c__Iterator1::.ctor()
extern "C"  void U3CLoadWordsU3Ec__Iterator1__ctor_m3857052611 (U3CLoadWordsU3Ec__Iterator1_t3335664088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlosarioUIController/<LoadWords>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadWordsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2130129529 (U3CLoadWordsU3Ec__Iterator1_t3335664088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlosarioUIController/<LoadWords>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadWordsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2800175117 (U3CLoadWordsU3Ec__Iterator1_t3335664088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlosarioUIController/<LoadWords>c__Iterator1::MoveNext()
extern "C"  bool U3CLoadWordsU3Ec__Iterator1_MoveNext_m1625344313 (U3CLoadWordsU3Ec__Iterator1_t3335664088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController/<LoadWords>c__Iterator1::Dispose()
extern "C"  void U3CLoadWordsU3Ec__Iterator1_Dispose_m4264033088 (U3CLoadWordsU3Ec__Iterator1_t3335664088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlosarioUIController/<LoadWords>c__Iterator1::Reset()
extern "C"  void U3CLoadWordsU3Ec__Iterator1_Reset_m1503485552 (U3CLoadWordsU3Ec__Iterator1_t3335664088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
