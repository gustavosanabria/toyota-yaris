﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t3903132647;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// UILabel
struct UILabel_t291504320;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UIButton
struct UIButton_t179429094;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlossaryMgr
struct  GlossaryMgr_t3182292730  : public MonoBehaviour_t667441552
{
public:
	// UITexture GlossaryMgr::definitionTexture
	UITexture_t3903132647 * ___definitionTexture_2;
	// UnityEngine.GameObject GlossaryMgr::wordsContainer
	GameObject_t3674682005 * ___wordsContainer_3;
	// System.String GlossaryMgr::path
	String_t* ___path_4;
	// UILabel GlossaryMgr::defTitle
	UILabel_t291504320 * ___defTitle_5;
	// UILabel GlossaryMgr::def
	UILabel_t291504320 * ___def_6;
	// UnityEngine.GameObject[] GlossaryMgr::wordsButtons
	GameObjectU5BU5D_t2662109048* ___wordsButtons_7;
	// System.Int32 GlossaryMgr::btnCount
	int32_t ___btnCount_8;
	// UIButton GlossaryMgr::backToMenuBtn
	UIButton_t179429094 * ___backToMenuBtn_9;
	// UnityEngine.GameObject GlossaryMgr::showInVRBtn
	GameObject_t3674682005 * ___showInVRBtn_12;
	// UnityEngine.GameObject GlossaryMgr::showInTUTBtn
	GameObject_t3674682005 * ___showInTUTBtn_13;
	// System.String GlossaryMgr::vrTarget
	String_t* ___vrTarget_14;
	// System.String GlossaryMgr::lastDef
	String_t* ___lastDef_15;
	// System.String GlossaryMgr::fullPath
	String_t* ___fullPath_16;

public:
	inline static int32_t get_offset_of_definitionTexture_2() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___definitionTexture_2)); }
	inline UITexture_t3903132647 * get_definitionTexture_2() const { return ___definitionTexture_2; }
	inline UITexture_t3903132647 ** get_address_of_definitionTexture_2() { return &___definitionTexture_2; }
	inline void set_definitionTexture_2(UITexture_t3903132647 * value)
	{
		___definitionTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___definitionTexture_2, value);
	}

	inline static int32_t get_offset_of_wordsContainer_3() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___wordsContainer_3)); }
	inline GameObject_t3674682005 * get_wordsContainer_3() const { return ___wordsContainer_3; }
	inline GameObject_t3674682005 ** get_address_of_wordsContainer_3() { return &___wordsContainer_3; }
	inline void set_wordsContainer_3(GameObject_t3674682005 * value)
	{
		___wordsContainer_3 = value;
		Il2CppCodeGenWriteBarrier(&___wordsContainer_3, value);
	}

	inline static int32_t get_offset_of_path_4() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___path_4)); }
	inline String_t* get_path_4() const { return ___path_4; }
	inline String_t** get_address_of_path_4() { return &___path_4; }
	inline void set_path_4(String_t* value)
	{
		___path_4 = value;
		Il2CppCodeGenWriteBarrier(&___path_4, value);
	}

	inline static int32_t get_offset_of_defTitle_5() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___defTitle_5)); }
	inline UILabel_t291504320 * get_defTitle_5() const { return ___defTitle_5; }
	inline UILabel_t291504320 ** get_address_of_defTitle_5() { return &___defTitle_5; }
	inline void set_defTitle_5(UILabel_t291504320 * value)
	{
		___defTitle_5 = value;
		Il2CppCodeGenWriteBarrier(&___defTitle_5, value);
	}

	inline static int32_t get_offset_of_def_6() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___def_6)); }
	inline UILabel_t291504320 * get_def_6() const { return ___def_6; }
	inline UILabel_t291504320 ** get_address_of_def_6() { return &___def_6; }
	inline void set_def_6(UILabel_t291504320 * value)
	{
		___def_6 = value;
		Il2CppCodeGenWriteBarrier(&___def_6, value);
	}

	inline static int32_t get_offset_of_wordsButtons_7() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___wordsButtons_7)); }
	inline GameObjectU5BU5D_t2662109048* get_wordsButtons_7() const { return ___wordsButtons_7; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_wordsButtons_7() { return &___wordsButtons_7; }
	inline void set_wordsButtons_7(GameObjectU5BU5D_t2662109048* value)
	{
		___wordsButtons_7 = value;
		Il2CppCodeGenWriteBarrier(&___wordsButtons_7, value);
	}

	inline static int32_t get_offset_of_btnCount_8() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___btnCount_8)); }
	inline int32_t get_btnCount_8() const { return ___btnCount_8; }
	inline int32_t* get_address_of_btnCount_8() { return &___btnCount_8; }
	inline void set_btnCount_8(int32_t value)
	{
		___btnCount_8 = value;
	}

	inline static int32_t get_offset_of_backToMenuBtn_9() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___backToMenuBtn_9)); }
	inline UIButton_t179429094 * get_backToMenuBtn_9() const { return ___backToMenuBtn_9; }
	inline UIButton_t179429094 ** get_address_of_backToMenuBtn_9() { return &___backToMenuBtn_9; }
	inline void set_backToMenuBtn_9(UIButton_t179429094 * value)
	{
		___backToMenuBtn_9 = value;
		Il2CppCodeGenWriteBarrier(&___backToMenuBtn_9, value);
	}

	inline static int32_t get_offset_of_showInVRBtn_12() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___showInVRBtn_12)); }
	inline GameObject_t3674682005 * get_showInVRBtn_12() const { return ___showInVRBtn_12; }
	inline GameObject_t3674682005 ** get_address_of_showInVRBtn_12() { return &___showInVRBtn_12; }
	inline void set_showInVRBtn_12(GameObject_t3674682005 * value)
	{
		___showInVRBtn_12 = value;
		Il2CppCodeGenWriteBarrier(&___showInVRBtn_12, value);
	}

	inline static int32_t get_offset_of_showInTUTBtn_13() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___showInTUTBtn_13)); }
	inline GameObject_t3674682005 * get_showInTUTBtn_13() const { return ___showInTUTBtn_13; }
	inline GameObject_t3674682005 ** get_address_of_showInTUTBtn_13() { return &___showInTUTBtn_13; }
	inline void set_showInTUTBtn_13(GameObject_t3674682005 * value)
	{
		___showInTUTBtn_13 = value;
		Il2CppCodeGenWriteBarrier(&___showInTUTBtn_13, value);
	}

	inline static int32_t get_offset_of_vrTarget_14() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___vrTarget_14)); }
	inline String_t* get_vrTarget_14() const { return ___vrTarget_14; }
	inline String_t** get_address_of_vrTarget_14() { return &___vrTarget_14; }
	inline void set_vrTarget_14(String_t* value)
	{
		___vrTarget_14 = value;
		Il2CppCodeGenWriteBarrier(&___vrTarget_14, value);
	}

	inline static int32_t get_offset_of_lastDef_15() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___lastDef_15)); }
	inline String_t* get_lastDef_15() const { return ___lastDef_15; }
	inline String_t** get_address_of_lastDef_15() { return &___lastDef_15; }
	inline void set_lastDef_15(String_t* value)
	{
		___lastDef_15 = value;
		Il2CppCodeGenWriteBarrier(&___lastDef_15, value);
	}

	inline static int32_t get_offset_of_fullPath_16() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730, ___fullPath_16)); }
	inline String_t* get_fullPath_16() const { return ___fullPath_16; }
	inline String_t** get_address_of_fullPath_16() { return &___fullPath_16; }
	inline void set_fullPath_16(String_t* value)
	{
		___fullPath_16 = value;
		Il2CppCodeGenWriteBarrier(&___fullPath_16, value);
	}
};

struct GlossaryMgr_t3182292730_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> GlossaryMgr::vrTargetsToShow
	List_1_t1375417109 * ___vrTargetsToShow_10;
	// System.String GlossaryMgr::LASTDEFWORD
	String_t* ___LASTDEFWORD_11;

public:
	inline static int32_t get_offset_of_vrTargetsToShow_10() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730_StaticFields, ___vrTargetsToShow_10)); }
	inline List_1_t1375417109 * get_vrTargetsToShow_10() const { return ___vrTargetsToShow_10; }
	inline List_1_t1375417109 ** get_address_of_vrTargetsToShow_10() { return &___vrTargetsToShow_10; }
	inline void set_vrTargetsToShow_10(List_1_t1375417109 * value)
	{
		___vrTargetsToShow_10 = value;
		Il2CppCodeGenWriteBarrier(&___vrTargetsToShow_10, value);
	}

	inline static int32_t get_offset_of_LASTDEFWORD_11() { return static_cast<int32_t>(offsetof(GlossaryMgr_t3182292730_StaticFields, ___LASTDEFWORD_11)); }
	inline String_t* get_LASTDEFWORD_11() const { return ___LASTDEFWORD_11; }
	inline String_t** get_address_of_LASTDEFWORD_11() { return &___LASTDEFWORD_11; }
	inline void set_LASTDEFWORD_11(String_t* value)
	{
		___LASTDEFWORD_11 = value;
		Il2CppCodeGenWriteBarrier(&___LASTDEFWORD_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
