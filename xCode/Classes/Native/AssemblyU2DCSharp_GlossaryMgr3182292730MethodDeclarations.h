﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlossaryMgr
struct GlossaryMgr_t3182292730;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void GlossaryMgr::.ctor()
extern "C"  void GlossaryMgr__ctor_m2130822577 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::.cctor()
extern "C"  void GlossaryMgr__cctor_m1148894236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::Awake()
extern "C"  void GlossaryMgr_Awake_m2368427796 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::EnableButtons(System.Boolean)
extern "C"  void GlossaryMgr_EnableButtons_m3802261636 (GlossaryMgr_t3182292730 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::GetDefinition(System.String,System.String,System.String,System.String)
extern "C"  void GlossaryMgr_GetDefinition_m753400670 (GlossaryMgr_t3182292730 * __this, String_t* ___pDefTitle0, String_t* ___pDef1, String_t* ___pVrTarget2, String_t* ___pTut3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::CloseDefinition()
extern "C"  void GlossaryMgr_CloseDefinition_m2592074746 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GlossaryMgr::ShowDefinition(System.String,System.String)
extern "C"  Il2CppObject * GlossaryMgr_ShowDefinition_m2444276995 (GlossaryMgr_t3182292730 * __this, String_t* ___pDefTitle0, String_t* ___pDef1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::FixedUpdate()
extern "C"  void GlossaryMgr_FixedUpdate_m252893804 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::SetVrTargets()
extern "C"  void GlossaryMgr_SetVrTargets_m1491698391 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::ShowInVR()
extern "C"  void GlossaryMgr_ShowInVR_m2163570737 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr::ShowInTutorial()
extern "C"  void GlossaryMgr_ShowInTutorial_m3022024051 (GlossaryMgr_t3182292730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
