﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// GlossaryMgr
struct GlossaryMgr_t3182292730;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlossaryMgr/<ShowDefinition>c__Iterator3
struct  U3CShowDefinitionU3Ec__Iterator3_t3893704553  : public Il2CppObject
{
public:
	// System.String GlossaryMgr/<ShowDefinition>c__Iterator3::pDefTitle
	String_t* ___pDefTitle_0;
	// System.String GlossaryMgr/<ShowDefinition>c__Iterator3::pDef
	String_t* ___pDef_1;
	// System.Int32 GlossaryMgr/<ShowDefinition>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// System.Object GlossaryMgr/<ShowDefinition>c__Iterator3::$current
	Il2CppObject * ___U24current_3;
	// System.String GlossaryMgr/<ShowDefinition>c__Iterator3::<$>pDefTitle
	String_t* ___U3CU24U3EpDefTitle_4;
	// System.String GlossaryMgr/<ShowDefinition>c__Iterator3::<$>pDef
	String_t* ___U3CU24U3EpDef_5;
	// GlossaryMgr GlossaryMgr/<ShowDefinition>c__Iterator3::<>f__this
	GlossaryMgr_t3182292730 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_pDefTitle_0() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___pDefTitle_0)); }
	inline String_t* get_pDefTitle_0() const { return ___pDefTitle_0; }
	inline String_t** get_address_of_pDefTitle_0() { return &___pDefTitle_0; }
	inline void set_pDefTitle_0(String_t* value)
	{
		___pDefTitle_0 = value;
		Il2CppCodeGenWriteBarrier(&___pDefTitle_0, value);
	}

	inline static int32_t get_offset_of_pDef_1() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___pDef_1)); }
	inline String_t* get_pDef_1() const { return ___pDef_1; }
	inline String_t** get_address_of_pDef_1() { return &___pDef_1; }
	inline void set_pDef_1(String_t* value)
	{
		___pDef_1 = value;
		Il2CppCodeGenWriteBarrier(&___pDef_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EpDefTitle_4() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___U3CU24U3EpDefTitle_4)); }
	inline String_t* get_U3CU24U3EpDefTitle_4() const { return ___U3CU24U3EpDefTitle_4; }
	inline String_t** get_address_of_U3CU24U3EpDefTitle_4() { return &___U3CU24U3EpDefTitle_4; }
	inline void set_U3CU24U3EpDefTitle_4(String_t* value)
	{
		___U3CU24U3EpDefTitle_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EpDefTitle_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EpDef_5() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___U3CU24U3EpDef_5)); }
	inline String_t* get_U3CU24U3EpDef_5() const { return ___U3CU24U3EpDef_5; }
	inline String_t** get_address_of_U3CU24U3EpDef_5() { return &___U3CU24U3EpDef_5; }
	inline void set_U3CU24U3EpDef_5(String_t* value)
	{
		___U3CU24U3EpDef_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EpDef_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CShowDefinitionU3Ec__Iterator3_t3893704553, ___U3CU3Ef__this_6)); }
	inline GlossaryMgr_t3182292730 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline GlossaryMgr_t3182292730 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(GlossaryMgr_t3182292730 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
