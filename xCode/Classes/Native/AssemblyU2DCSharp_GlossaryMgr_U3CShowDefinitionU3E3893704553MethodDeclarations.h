﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlossaryMgr/<ShowDefinition>c__Iterator3
struct U3CShowDefinitionU3Ec__Iterator3_t3893704553;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GlossaryMgr/<ShowDefinition>c__Iterator3::.ctor()
extern "C"  void U3CShowDefinitionU3Ec__Iterator3__ctor_m3613027666 (U3CShowDefinitionU3Ec__Iterator3_t3893704553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlossaryMgr/<ShowDefinition>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowDefinitionU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2912786122 (U3CShowDefinitionU3Ec__Iterator3_t3893704553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlossaryMgr/<ShowDefinition>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowDefinitionU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1591373406 (U3CShowDefinitionU3Ec__Iterator3_t3893704553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlossaryMgr/<ShowDefinition>c__Iterator3::MoveNext()
extern "C"  bool U3CShowDefinitionU3Ec__Iterator3_MoveNext_m2491689802 (U3CShowDefinitionU3Ec__Iterator3_t3893704553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr/<ShowDefinition>c__Iterator3::Dispose()
extern "C"  void U3CShowDefinitionU3Ec__Iterator3_Dispose_m1684294927 (U3CShowDefinitionU3Ec__Iterator3_t3893704553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlossaryMgr/<ShowDefinition>c__Iterator3::Reset()
extern "C"  void U3CShowDefinitionU3Ec__Iterator3_Reset_m1259460607 (U3CShowDefinitionU3Ec__Iterator3_t3893704553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
