﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroscopeController
struct  GyroscopeController_t2265035361  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Quaternion GyroscopeController::baseIdentity
	Quaternion_t1553702882  ___baseIdentity_2;
	// UnityEngine.Quaternion GyroscopeController::referanceRotation
	Quaternion_t1553702882  ___referanceRotation_3;
	// UnityEngine.Quaternion GyroscopeController::cameraBase
	Quaternion_t1553702882  ___cameraBase_4;
	// UnityEngine.Quaternion GyroscopeController::calibration
	Quaternion_t1553702882  ___calibration_5;
	// UnityEngine.Quaternion GyroscopeController::baseOrientation
	Quaternion_t1553702882  ___baseOrientation_6;
	// UnityEngine.Quaternion GyroscopeController::baseOrientationRotationFix
	Quaternion_t1553702882  ___baseOrientationRotationFix_7;
	// System.Boolean GyroscopeController::gyroEnble
	bool ___gyroEnble_8;
	// System.Single GyroscopeController::minSwipeDistY
	float ___minSwipeDistY_9;
	// System.Single GyroscopeController::minSwipeDistX
	float ___minSwipeDistX_10;
	// UnityEngine.Camera GyroscopeController::cam360
	Camera_t2727095145 * ___cam360_11;
	// UnityEngine.Camera GyroscopeController::camButtons
	Camera_t2727095145 * ___camButtons_12;
	// UnityEngine.Vector2 GyroscopeController::startPos
	Vector2_t4282066565  ___startPos_13;
	// System.Single GyroscopeController::swipeDistVertical
	float ___swipeDistVertical_14;
	// System.Single GyroscopeController::swipeTime
	float ___swipeTime_15;
	// System.Boolean GyroscopeController::canCountUp
	bool ___canCountUp_16;
	// UnityEngine.Quaternion GyroscopeController::originalRotation
	Quaternion_t1553702882  ___originalRotation_17;
	// System.Single GyroscopeController::sensitivityX
	float ___sensitivityX_18;
	// System.Single GyroscopeController::sensitivityY
	float ___sensitivityY_19;
	// System.Single GyroscopeController::minimumX
	float ___minimumX_20;
	// System.Single GyroscopeController::maximumX
	float ___maximumX_21;
	// System.Single GyroscopeController::minimumY
	float ___minimumY_22;
	// System.Single GyroscopeController::maximumY
	float ___maximumY_23;
	// System.Single GyroscopeController::rotationY
	float ___rotationY_24;
	// System.Single GyroscopeController::rotationX
	float ___rotationX_25;

public:
	inline static int32_t get_offset_of_baseIdentity_2() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___baseIdentity_2)); }
	inline Quaternion_t1553702882  get_baseIdentity_2() const { return ___baseIdentity_2; }
	inline Quaternion_t1553702882 * get_address_of_baseIdentity_2() { return &___baseIdentity_2; }
	inline void set_baseIdentity_2(Quaternion_t1553702882  value)
	{
		___baseIdentity_2 = value;
	}

	inline static int32_t get_offset_of_referanceRotation_3() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___referanceRotation_3)); }
	inline Quaternion_t1553702882  get_referanceRotation_3() const { return ___referanceRotation_3; }
	inline Quaternion_t1553702882 * get_address_of_referanceRotation_3() { return &___referanceRotation_3; }
	inline void set_referanceRotation_3(Quaternion_t1553702882  value)
	{
		___referanceRotation_3 = value;
	}

	inline static int32_t get_offset_of_cameraBase_4() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___cameraBase_4)); }
	inline Quaternion_t1553702882  get_cameraBase_4() const { return ___cameraBase_4; }
	inline Quaternion_t1553702882 * get_address_of_cameraBase_4() { return &___cameraBase_4; }
	inline void set_cameraBase_4(Quaternion_t1553702882  value)
	{
		___cameraBase_4 = value;
	}

	inline static int32_t get_offset_of_calibration_5() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___calibration_5)); }
	inline Quaternion_t1553702882  get_calibration_5() const { return ___calibration_5; }
	inline Quaternion_t1553702882 * get_address_of_calibration_5() { return &___calibration_5; }
	inline void set_calibration_5(Quaternion_t1553702882  value)
	{
		___calibration_5 = value;
	}

	inline static int32_t get_offset_of_baseOrientation_6() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___baseOrientation_6)); }
	inline Quaternion_t1553702882  get_baseOrientation_6() const { return ___baseOrientation_6; }
	inline Quaternion_t1553702882 * get_address_of_baseOrientation_6() { return &___baseOrientation_6; }
	inline void set_baseOrientation_6(Quaternion_t1553702882  value)
	{
		___baseOrientation_6 = value;
	}

	inline static int32_t get_offset_of_baseOrientationRotationFix_7() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___baseOrientationRotationFix_7)); }
	inline Quaternion_t1553702882  get_baseOrientationRotationFix_7() const { return ___baseOrientationRotationFix_7; }
	inline Quaternion_t1553702882 * get_address_of_baseOrientationRotationFix_7() { return &___baseOrientationRotationFix_7; }
	inline void set_baseOrientationRotationFix_7(Quaternion_t1553702882  value)
	{
		___baseOrientationRotationFix_7 = value;
	}

	inline static int32_t get_offset_of_gyroEnble_8() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___gyroEnble_8)); }
	inline bool get_gyroEnble_8() const { return ___gyroEnble_8; }
	inline bool* get_address_of_gyroEnble_8() { return &___gyroEnble_8; }
	inline void set_gyroEnble_8(bool value)
	{
		___gyroEnble_8 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistY_9() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___minSwipeDistY_9)); }
	inline float get_minSwipeDistY_9() const { return ___minSwipeDistY_9; }
	inline float* get_address_of_minSwipeDistY_9() { return &___minSwipeDistY_9; }
	inline void set_minSwipeDistY_9(float value)
	{
		___minSwipeDistY_9 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistX_10() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___minSwipeDistX_10)); }
	inline float get_minSwipeDistX_10() const { return ___minSwipeDistX_10; }
	inline float* get_address_of_minSwipeDistX_10() { return &___minSwipeDistX_10; }
	inline void set_minSwipeDistX_10(float value)
	{
		___minSwipeDistX_10 = value;
	}

	inline static int32_t get_offset_of_cam360_11() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___cam360_11)); }
	inline Camera_t2727095145 * get_cam360_11() const { return ___cam360_11; }
	inline Camera_t2727095145 ** get_address_of_cam360_11() { return &___cam360_11; }
	inline void set_cam360_11(Camera_t2727095145 * value)
	{
		___cam360_11 = value;
		Il2CppCodeGenWriteBarrier(&___cam360_11, value);
	}

	inline static int32_t get_offset_of_camButtons_12() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___camButtons_12)); }
	inline Camera_t2727095145 * get_camButtons_12() const { return ___camButtons_12; }
	inline Camera_t2727095145 ** get_address_of_camButtons_12() { return &___camButtons_12; }
	inline void set_camButtons_12(Camera_t2727095145 * value)
	{
		___camButtons_12 = value;
		Il2CppCodeGenWriteBarrier(&___camButtons_12, value);
	}

	inline static int32_t get_offset_of_startPos_13() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___startPos_13)); }
	inline Vector2_t4282066565  get_startPos_13() const { return ___startPos_13; }
	inline Vector2_t4282066565 * get_address_of_startPos_13() { return &___startPos_13; }
	inline void set_startPos_13(Vector2_t4282066565  value)
	{
		___startPos_13 = value;
	}

	inline static int32_t get_offset_of_swipeDistVertical_14() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___swipeDistVertical_14)); }
	inline float get_swipeDistVertical_14() const { return ___swipeDistVertical_14; }
	inline float* get_address_of_swipeDistVertical_14() { return &___swipeDistVertical_14; }
	inline void set_swipeDistVertical_14(float value)
	{
		___swipeDistVertical_14 = value;
	}

	inline static int32_t get_offset_of_swipeTime_15() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___swipeTime_15)); }
	inline float get_swipeTime_15() const { return ___swipeTime_15; }
	inline float* get_address_of_swipeTime_15() { return &___swipeTime_15; }
	inline void set_swipeTime_15(float value)
	{
		___swipeTime_15 = value;
	}

	inline static int32_t get_offset_of_canCountUp_16() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___canCountUp_16)); }
	inline bool get_canCountUp_16() const { return ___canCountUp_16; }
	inline bool* get_address_of_canCountUp_16() { return &___canCountUp_16; }
	inline void set_canCountUp_16(bool value)
	{
		___canCountUp_16 = value;
	}

	inline static int32_t get_offset_of_originalRotation_17() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___originalRotation_17)); }
	inline Quaternion_t1553702882  get_originalRotation_17() const { return ___originalRotation_17; }
	inline Quaternion_t1553702882 * get_address_of_originalRotation_17() { return &___originalRotation_17; }
	inline void set_originalRotation_17(Quaternion_t1553702882  value)
	{
		___originalRotation_17 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_18() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___sensitivityX_18)); }
	inline float get_sensitivityX_18() const { return ___sensitivityX_18; }
	inline float* get_address_of_sensitivityX_18() { return &___sensitivityX_18; }
	inline void set_sensitivityX_18(float value)
	{
		___sensitivityX_18 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_19() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___sensitivityY_19)); }
	inline float get_sensitivityY_19() const { return ___sensitivityY_19; }
	inline float* get_address_of_sensitivityY_19() { return &___sensitivityY_19; }
	inline void set_sensitivityY_19(float value)
	{
		___sensitivityY_19 = value;
	}

	inline static int32_t get_offset_of_minimumX_20() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___minimumX_20)); }
	inline float get_minimumX_20() const { return ___minimumX_20; }
	inline float* get_address_of_minimumX_20() { return &___minimumX_20; }
	inline void set_minimumX_20(float value)
	{
		___minimumX_20 = value;
	}

	inline static int32_t get_offset_of_maximumX_21() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___maximumX_21)); }
	inline float get_maximumX_21() const { return ___maximumX_21; }
	inline float* get_address_of_maximumX_21() { return &___maximumX_21; }
	inline void set_maximumX_21(float value)
	{
		___maximumX_21 = value;
	}

	inline static int32_t get_offset_of_minimumY_22() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___minimumY_22)); }
	inline float get_minimumY_22() const { return ___minimumY_22; }
	inline float* get_address_of_minimumY_22() { return &___minimumY_22; }
	inline void set_minimumY_22(float value)
	{
		___minimumY_22 = value;
	}

	inline static int32_t get_offset_of_maximumY_23() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___maximumY_23)); }
	inline float get_maximumY_23() const { return ___maximumY_23; }
	inline float* get_address_of_maximumY_23() { return &___maximumY_23; }
	inline void set_maximumY_23(float value)
	{
		___maximumY_23 = value;
	}

	inline static int32_t get_offset_of_rotationY_24() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___rotationY_24)); }
	inline float get_rotationY_24() const { return ___rotationY_24; }
	inline float* get_address_of_rotationY_24() { return &___rotationY_24; }
	inline void set_rotationY_24(float value)
	{
		___rotationY_24 = value;
	}

	inline static int32_t get_offset_of_rotationX_25() { return static_cast<int32_t>(offsetof(GyroscopeController_t2265035361, ___rotationX_25)); }
	inline float get_rotationX_25() const { return ___rotationX_25; }
	inline float* get_address_of_rotationX_25() { return &___rotationX_25; }
	inline void set_rotationX_25(float value)
	{
		___rotationX_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
