﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GyroscopeController
struct GyroscopeController_t2265035361;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void GyroscopeController::.ctor()
extern "C"  void GyroscopeController__ctor_m213417514 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::Start()
extern "C"  void GyroscopeController_Start_m3455522602 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::Update()
extern "C"  void GyroscopeController_Update_m4047837731 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::AttachGyro()
extern "C"  void GyroscopeController_AttachGyro_m664798382 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::DetachGyro()
extern "C"  void GyroscopeController_DetachGyro_m2236513148 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::CalibrateGyro()
extern "C"  void GyroscopeController_CalibrateGyro_m4267897054 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GyroscopeController::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float GyroscopeController_ClampAngle_m525731919 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::UpdateCalibration(System.Boolean)
extern "C"  void GyroscopeController_UpdateCalibration_m1810327456 (GyroscopeController_t2265035361 * __this, bool ___onlyHorizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::UpdateCameraBaseRotation(System.Boolean)
extern "C"  void GyroscopeController_UpdateCameraBaseRotation_m647407406 (GyroscopeController_t2265035361 * __this, bool ___onlyHorizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroscopeController::ConvertRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  GyroscopeController_ConvertRotation_m3979244204 (GyroscopeController_t2265035361 * __this, Quaternion_t1553702882  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroscopeController::GetRotFix()
extern "C"  Quaternion_t1553702882  GyroscopeController_GetRotFix_m139467652 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::ResetBaseOrientation()
extern "C"  void GyroscopeController_ResetBaseOrientation_m1919598794 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::RecalculateReferenceRotation()
extern "C"  void GyroscopeController_RecalculateReferenceRotation_m2095434480 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeController::countSwipeTimeUp()
extern "C"  void GyroscopeController_countSwipeTimeUp_m696782093 (GyroscopeController_t2265035361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
