﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroscopeControllerFullFreedom
struct  GyroscopeControllerFullFreedom_t685599910  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Quaternion GyroscopeControllerFullFreedom::baseIdentity
	Quaternion_t1553702882  ___baseIdentity_2;
	// UnityEngine.Quaternion GyroscopeControllerFullFreedom::referenceRotation
	Quaternion_t1553702882  ___referenceRotation_3;
	// UnityEngine.Quaternion GyroscopeControllerFullFreedom::cameraBase
	Quaternion_t1553702882  ___cameraBase_4;
	// UnityEngine.Quaternion GyroscopeControllerFullFreedom::calibration
	Quaternion_t1553702882  ___calibration_5;
	// UnityEngine.Quaternion GyroscopeControllerFullFreedom::baseOrientation
	Quaternion_t1553702882  ___baseOrientation_6;
	// System.Boolean GyroscopeControllerFullFreedom::m_gyroEnable
	bool ___m_gyroEnable_7;
	// UnityEngine.Quaternion GyroscopeControllerFullFreedom::originalRotation
	Quaternion_t1553702882  ___originalRotation_8;
	// System.Single GyroscopeControllerFullFreedom::sensitivityX
	float ___sensitivityX_9;
	// System.Single GyroscopeControllerFullFreedom::sensitivityY
	float ___sensitivityY_10;
	// System.Single GyroscopeControllerFullFreedom::minimumX
	float ___minimumX_11;
	// System.Single GyroscopeControllerFullFreedom::maximumX
	float ___maximumX_12;
	// System.Single GyroscopeControllerFullFreedom::minimumY
	float ___minimumY_13;
	// System.Single GyroscopeControllerFullFreedom::maximumY
	float ___maximumY_14;
	// System.Single GyroscopeControllerFullFreedom::rotationY
	float ___rotationY_15;
	// System.Single GyroscopeControllerFullFreedom::rotationX
	float ___rotationX_16;
	// System.Single GyroscopeControllerFullFreedom::m_fovMin
	float ___m_fovMin_17;
	// System.Single GyroscopeControllerFullFreedom::m_fovMax
	float ___m_fovMax_18;

public:
	inline static int32_t get_offset_of_baseIdentity_2() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___baseIdentity_2)); }
	inline Quaternion_t1553702882  get_baseIdentity_2() const { return ___baseIdentity_2; }
	inline Quaternion_t1553702882 * get_address_of_baseIdentity_2() { return &___baseIdentity_2; }
	inline void set_baseIdentity_2(Quaternion_t1553702882  value)
	{
		___baseIdentity_2 = value;
	}

	inline static int32_t get_offset_of_referenceRotation_3() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___referenceRotation_3)); }
	inline Quaternion_t1553702882  get_referenceRotation_3() const { return ___referenceRotation_3; }
	inline Quaternion_t1553702882 * get_address_of_referenceRotation_3() { return &___referenceRotation_3; }
	inline void set_referenceRotation_3(Quaternion_t1553702882  value)
	{
		___referenceRotation_3 = value;
	}

	inline static int32_t get_offset_of_cameraBase_4() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___cameraBase_4)); }
	inline Quaternion_t1553702882  get_cameraBase_4() const { return ___cameraBase_4; }
	inline Quaternion_t1553702882 * get_address_of_cameraBase_4() { return &___cameraBase_4; }
	inline void set_cameraBase_4(Quaternion_t1553702882  value)
	{
		___cameraBase_4 = value;
	}

	inline static int32_t get_offset_of_calibration_5() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___calibration_5)); }
	inline Quaternion_t1553702882  get_calibration_5() const { return ___calibration_5; }
	inline Quaternion_t1553702882 * get_address_of_calibration_5() { return &___calibration_5; }
	inline void set_calibration_5(Quaternion_t1553702882  value)
	{
		___calibration_5 = value;
	}

	inline static int32_t get_offset_of_baseOrientation_6() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___baseOrientation_6)); }
	inline Quaternion_t1553702882  get_baseOrientation_6() const { return ___baseOrientation_6; }
	inline Quaternion_t1553702882 * get_address_of_baseOrientation_6() { return &___baseOrientation_6; }
	inline void set_baseOrientation_6(Quaternion_t1553702882  value)
	{
		___baseOrientation_6 = value;
	}

	inline static int32_t get_offset_of_m_gyroEnable_7() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___m_gyroEnable_7)); }
	inline bool get_m_gyroEnable_7() const { return ___m_gyroEnable_7; }
	inline bool* get_address_of_m_gyroEnable_7() { return &___m_gyroEnable_7; }
	inline void set_m_gyroEnable_7(bool value)
	{
		___m_gyroEnable_7 = value;
	}

	inline static int32_t get_offset_of_originalRotation_8() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___originalRotation_8)); }
	inline Quaternion_t1553702882  get_originalRotation_8() const { return ___originalRotation_8; }
	inline Quaternion_t1553702882 * get_address_of_originalRotation_8() { return &___originalRotation_8; }
	inline void set_originalRotation_8(Quaternion_t1553702882  value)
	{
		___originalRotation_8 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_9() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___sensitivityX_9)); }
	inline float get_sensitivityX_9() const { return ___sensitivityX_9; }
	inline float* get_address_of_sensitivityX_9() { return &___sensitivityX_9; }
	inline void set_sensitivityX_9(float value)
	{
		___sensitivityX_9 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_10() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___sensitivityY_10)); }
	inline float get_sensitivityY_10() const { return ___sensitivityY_10; }
	inline float* get_address_of_sensitivityY_10() { return &___sensitivityY_10; }
	inline void set_sensitivityY_10(float value)
	{
		___sensitivityY_10 = value;
	}

	inline static int32_t get_offset_of_minimumX_11() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___minimumX_11)); }
	inline float get_minimumX_11() const { return ___minimumX_11; }
	inline float* get_address_of_minimumX_11() { return &___minimumX_11; }
	inline void set_minimumX_11(float value)
	{
		___minimumX_11 = value;
	}

	inline static int32_t get_offset_of_maximumX_12() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___maximumX_12)); }
	inline float get_maximumX_12() const { return ___maximumX_12; }
	inline float* get_address_of_maximumX_12() { return &___maximumX_12; }
	inline void set_maximumX_12(float value)
	{
		___maximumX_12 = value;
	}

	inline static int32_t get_offset_of_minimumY_13() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___minimumY_13)); }
	inline float get_minimumY_13() const { return ___minimumY_13; }
	inline float* get_address_of_minimumY_13() { return &___minimumY_13; }
	inline void set_minimumY_13(float value)
	{
		___minimumY_13 = value;
	}

	inline static int32_t get_offset_of_maximumY_14() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___maximumY_14)); }
	inline float get_maximumY_14() const { return ___maximumY_14; }
	inline float* get_address_of_maximumY_14() { return &___maximumY_14; }
	inline void set_maximumY_14(float value)
	{
		___maximumY_14 = value;
	}

	inline static int32_t get_offset_of_rotationY_15() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___rotationY_15)); }
	inline float get_rotationY_15() const { return ___rotationY_15; }
	inline float* get_address_of_rotationY_15() { return &___rotationY_15; }
	inline void set_rotationY_15(float value)
	{
		___rotationY_15 = value;
	}

	inline static int32_t get_offset_of_rotationX_16() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___rotationX_16)); }
	inline float get_rotationX_16() const { return ___rotationX_16; }
	inline float* get_address_of_rotationX_16() { return &___rotationX_16; }
	inline void set_rotationX_16(float value)
	{
		___rotationX_16 = value;
	}

	inline static int32_t get_offset_of_m_fovMin_17() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___m_fovMin_17)); }
	inline float get_m_fovMin_17() const { return ___m_fovMin_17; }
	inline float* get_address_of_m_fovMin_17() { return &___m_fovMin_17; }
	inline void set_m_fovMin_17(float value)
	{
		___m_fovMin_17 = value;
	}

	inline static int32_t get_offset_of_m_fovMax_18() { return static_cast<int32_t>(offsetof(GyroscopeControllerFullFreedom_t685599910, ___m_fovMax_18)); }
	inline float get_m_fovMax_18() const { return ___m_fovMax_18; }
	inline float* get_address_of_m_fovMax_18() { return &___m_fovMax_18; }
	inline void set_m_fovMax_18(float value)
	{
		___m_fovMax_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
