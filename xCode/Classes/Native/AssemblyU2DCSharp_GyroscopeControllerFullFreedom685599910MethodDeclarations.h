﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GyroscopeControllerFullFreedom
struct GyroscopeControllerFullFreedom_t685599910;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"

// System.Void GyroscopeControllerFullFreedom::.ctor()
extern "C"  void GyroscopeControllerFullFreedom__ctor_m1108740789 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::Start()
extern "C"  void GyroscopeControllerFullFreedom_Start_m55878581 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::UpdateRotationGyro()
extern "C"  void GyroscopeControllerFullFreedom_UpdateRotationGyro_m672615589 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::UpdateRotationUser()
extern "C"  void GyroscopeControllerFullFreedom_UpdateRotationUser_m1067498177 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::Update()
extern "C"  void GyroscopeControllerFullFreedom_Update_m1738088184 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::UpdateZoom()
extern "C"  void GyroscopeControllerFullFreedom_UpdateZoom_m14791339 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroscopeControllerFullFreedom::GetGyroRotation()
extern "C"  Quaternion_t1553702882  GyroscopeControllerFullFreedom_GetGyroRotation_m3487495246 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::SmoothZoom(UnityEngine.Touch,UnityEngine.Touch)
extern "C"  void GyroscopeControllerFullFreedom_SmoothZoom_m2468505470 (GyroscopeControllerFullFreedom_t685599910 * __this, Touch_t4210255029  ___touchZero0, Touch_t4210255029  ___touchOne1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::AttachGyro()
extern "C"  void GyroscopeControllerFullFreedom_AttachGyro_m2550896387 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::CalibrateGyro()
extern "C"  void GyroscopeControllerFullFreedom_CalibrateGyro_m1956430441 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GyroscopeControllerFullFreedom::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float GyroscopeControllerFullFreedom_ClampAngle_m66496130 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::UpdateCalibration()
extern "C"  void GyroscopeControllerFullFreedom_UpdateCalibration_m153866740 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::UpdateCameraBaseRotation()
extern "C"  void GyroscopeControllerFullFreedom_UpdateCameraBaseRotation_m4155344780 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroscopeControllerFullFreedom::ConvertRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  GyroscopeControllerFullFreedom_ConvertRotation_m2763434023 (GyroscopeControllerFullFreedom_t685599910 * __this, Quaternion_t1553702882  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroscopeControllerFullFreedom::RecalculateReferenceRotation()
extern "C"  void GyroscopeControllerFullFreedom_RecalculateReferenceRotation_m1038307845 (GyroscopeControllerFullFreedom_t685599910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
