﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t3903132647;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIPanel
struct UIPanel_t295209936;
// UISpriteAnimation
struct UISpriteAnimation_t4279777547;
// UILabel
struct UILabel_t291504320;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UIButton
struct UIButton_t179429094;
// UICamera
struct UICamera_t189364953;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeScreenManager
struct  HomeScreenManager_t2427744994  : public MonoBehaviour_t667441552
{
public:
	// UITexture HomeScreenManager::btnAssistanceUITex
	UITexture_t3903132647 * ___btnAssistanceUITex_2;
	// UnityEngine.Texture2D HomeScreenManager::btnAssistanceGLOWTex
	Texture2D_t3884108195 * ___btnAssistanceGLOWTex_3;
	// UITexture HomeScreenManager::btn360ViewUITex
	UITexture_t3903132647 * ___btn360ViewUITex_4;
	// UnityEngine.Texture2D HomeScreenManager::btn360ViewGLOWTex
	Texture2D_t3884108195 * ___btn360ViewGLOWTex_5;
	// UITexture HomeScreenManager::btnLexiqueUITex
	UITexture_t3903132647 * ___btnLexiqueUITex_6;
	// UITexture HomeScreenManager::btnARUITex
	UITexture_t3903132647 * ___btnARUITex_7;
	// UnityEngine.Texture2D HomeScreenManager::btnARGLOWTex
	Texture2D_t3884108195 * ___btnARGLOWTex_8;
	// UnityEngine.Texture2D HomeScreenManager::btnLexiqueGLOWTex
	Texture2D_t3884108195 * ___btnLexiqueGLOWTex_9;
	// UITexture HomeScreenManager::btnTUTUITex
	UITexture_t3903132647 * ___btnTUTUITex_10;
	// UnityEngine.Texture2D HomeScreenManager::btnTUTGLOWTex
	Texture2D_t3884108195 * ___btnTUTGLOWTex_11;
	// UITexture HomeScreenManager::btnLegalsUITex
	UITexture_t3903132647 * ___btnLegalsUITex_12;
	// UnityEngine.Texture2D HomeScreenManager::btnLegalsGLOWTex
	Texture2D_t3884108195 * ___btnLegalsGLOWTex_13;
	// UnityEngine.GameObject HomeScreenManager::lexiqueFlashGO
	GameObject_t3674682005 * ___lexiqueFlashGO_14;
	// UnityEngine.GameObject HomeScreenManager::arFlashGO
	GameObject_t3674682005 * ___arFlashGO_15;
	// UnityEngine.GameObject HomeScreenManager::v360FlashGO
	GameObject_t3674682005 * ___v360FlashGO_16;
	// UnityEngine.GameObject HomeScreenManager::tutFlashGO
	GameObject_t3674682005 * ___tutFlashGO_17;
	// UnityEngine.Texture2D HomeScreenManager::btnAssitanceNormalTex
	Texture2D_t3884108195 * ___btnAssitanceNormalTex_18;
	// UnityEngine.Texture2D HomeScreenManager::btnARNormalTex
	Texture2D_t3884108195 * ___btnARNormalTex_19;
	// UnityEngine.Texture2D HomeScreenManager::btn360NormalTex
	Texture2D_t3884108195 * ___btn360NormalTex_20;
	// UnityEngine.Texture2D HomeScreenManager::btnTUTNormalTex
	Texture2D_t3884108195 * ___btnTUTNormalTex_21;
	// UnityEngine.Texture2D HomeScreenManager::btnLegalsNormalTex
	Texture2D_t3884108195 * ___btnLegalsNormalTex_22;
	// UnityEngine.GameObject HomeScreenManager::tutoScreen
	GameObject_t3674682005 * ___tutoScreen_23;
	// UITexture HomeScreenManager::arHelpFrameTex
	UITexture_t3903132647 * ___arHelpFrameTex_24;
	// UnityEngine.GameObject HomeScreenManager::arHelpText
	GameObject_t3674682005 * ___arHelpText_25;
	// UnityEngine.GameObject HomeScreenManager::goToARBtn
	GameObject_t3674682005 * ___goToARBtn_26;
	// UnityEngine.GameObject HomeScreenManager::backToMenuBtn
	GameObject_t3674682005 * ___backToMenuBtn_27;
	// UnityEngine.GameObject HomeScreenManager::backToMenuVRBtn
	GameObject_t3674682005 * ___backToMenuVRBtn_28;
	// UnityEngine.GameObject HomeScreenManager::goToVRBtn
	GameObject_t3674682005 * ___goToVRBtn_29;
	// UnityEngine.Texture2D HomeScreenManager::arHelpTex
	Texture2D_t3884108195 * ___arHelpTex_30;
	// UnityEngine.Texture2D HomeScreenManager::arTorchTex
	Texture2D_t3884108195 * ___arTorchTex_31;
	// UnityEngine.GameObject HomeScreenManager::assistanceScreen
	GameObject_t3674682005 * ___assistanceScreen_32;
	// UnityEngine.GameObject HomeScreenManager::arHelpScreen
	GameObject_t3674682005 * ___arHelpScreen_33;
	// UnityEngine.GameObject HomeScreenManager::vrHelpScreen
	GameObject_t3674682005 * ___vrHelpScreen_34;
	// UnityEngine.GameObject HomeScreenManager::legalsScreen
	GameObject_t3674682005 * ___legalsScreen_35;
	// UnityEngine.GameObject HomeScreenManager::v360HelpScreen
	GameObject_t3674682005 * ___v360HelpScreen_36;
	// UIPanel HomeScreenManager::tutBtnsPanel
	UIPanel_t295209936 * ___tutBtnsPanel_37;
	// UnityEngine.Vector2 HomeScreenManager::originalTutButtonsPanelClipPos
	Vector2_t4282066565  ___originalTutButtonsPanelClipPos_38;
	// UnityEngine.Vector3 HomeScreenManager::originalTutButtonsPanelPos
	Vector3_t4282066566  ___originalTutButtonsPanelPos_39;
	// UISpriteAnimation HomeScreenManager::backGroundAnim
	UISpriteAnimation_t4279777547 * ___backGroundAnim_40;
	// UnityEngine.GameObject HomeScreenManager::loadingBar
	GameObject_t3674682005 * ___loadingBar_41;
	// UnityEngine.GameObject HomeScreenManager::loadingFrame
	GameObject_t3674682005 * ___loadingFrame_42;
	// UnityEngine.GameObject HomeScreenManager::vRLoadingBar
	GameObject_t3674682005 * ___vRLoadingBar_43;
	// UnityEngine.GameObject HomeScreenManager::vRLoadingFrame
	GameObject_t3674682005 * ___vRLoadingFrame_44;
	// UILabel HomeScreenManager::arLoadingLabel
	UILabel_t291504320 * ___arLoadingLabel_45;
	// System.Boolean HomeScreenManager::showARProgressBar
	bool ___showARProgressBar_46;
	// System.Boolean HomeScreenManager::showVRProgressBar
	bool ___showVRProgressBar_47;
	// System.Single HomeScreenManager::barProgress
	float ___barProgress_48;
	// UnityEngine.GameObject[] HomeScreenManager::flashs
	GameObjectU5BU5D_t2662109048* ___flashs_49;
	// UIButton HomeScreenManager::tutBtn
	UIButton_t179429094 * ___tutBtn_50;
	// UIButton HomeScreenManager::tutWarningOKBtn
	UIButton_t179429094 * ___tutWarningOKBtn_51;
	// UIButton HomeScreenManager::tutCatPhoneBtn
	UIButton_t179429094 * ___tutCatPhoneBtn_52;
	// UIButton HomeScreenManager::tutCatGPSBtn
	UIButton_t179429094 * ___tutCatGPSBtn_53;
	// UIButton HomeScreenManager::tutCatAppBtn
	UIButton_t179429094 * ___tutCatAppBtn_54;
	// UIButton HomeScreenManager::tutCatRadioBtn
	UIButton_t179429094 * ___tutCatRadioBtn_55;
	// UICamera HomeScreenManager::uiCam
	UICamera_t189364953 * ___uiCam_56;

public:
	inline static int32_t get_offset_of_btnAssistanceUITex_2() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnAssistanceUITex_2)); }
	inline UITexture_t3903132647 * get_btnAssistanceUITex_2() const { return ___btnAssistanceUITex_2; }
	inline UITexture_t3903132647 ** get_address_of_btnAssistanceUITex_2() { return &___btnAssistanceUITex_2; }
	inline void set_btnAssistanceUITex_2(UITexture_t3903132647 * value)
	{
		___btnAssistanceUITex_2 = value;
		Il2CppCodeGenWriteBarrier(&___btnAssistanceUITex_2, value);
	}

	inline static int32_t get_offset_of_btnAssistanceGLOWTex_3() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnAssistanceGLOWTex_3)); }
	inline Texture2D_t3884108195 * get_btnAssistanceGLOWTex_3() const { return ___btnAssistanceGLOWTex_3; }
	inline Texture2D_t3884108195 ** get_address_of_btnAssistanceGLOWTex_3() { return &___btnAssistanceGLOWTex_3; }
	inline void set_btnAssistanceGLOWTex_3(Texture2D_t3884108195 * value)
	{
		___btnAssistanceGLOWTex_3 = value;
		Il2CppCodeGenWriteBarrier(&___btnAssistanceGLOWTex_3, value);
	}

	inline static int32_t get_offset_of_btn360ViewUITex_4() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btn360ViewUITex_4)); }
	inline UITexture_t3903132647 * get_btn360ViewUITex_4() const { return ___btn360ViewUITex_4; }
	inline UITexture_t3903132647 ** get_address_of_btn360ViewUITex_4() { return &___btn360ViewUITex_4; }
	inline void set_btn360ViewUITex_4(UITexture_t3903132647 * value)
	{
		___btn360ViewUITex_4 = value;
		Il2CppCodeGenWriteBarrier(&___btn360ViewUITex_4, value);
	}

	inline static int32_t get_offset_of_btn360ViewGLOWTex_5() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btn360ViewGLOWTex_5)); }
	inline Texture2D_t3884108195 * get_btn360ViewGLOWTex_5() const { return ___btn360ViewGLOWTex_5; }
	inline Texture2D_t3884108195 ** get_address_of_btn360ViewGLOWTex_5() { return &___btn360ViewGLOWTex_5; }
	inline void set_btn360ViewGLOWTex_5(Texture2D_t3884108195 * value)
	{
		___btn360ViewGLOWTex_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn360ViewGLOWTex_5, value);
	}

	inline static int32_t get_offset_of_btnLexiqueUITex_6() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnLexiqueUITex_6)); }
	inline UITexture_t3903132647 * get_btnLexiqueUITex_6() const { return ___btnLexiqueUITex_6; }
	inline UITexture_t3903132647 ** get_address_of_btnLexiqueUITex_6() { return &___btnLexiqueUITex_6; }
	inline void set_btnLexiqueUITex_6(UITexture_t3903132647 * value)
	{
		___btnLexiqueUITex_6 = value;
		Il2CppCodeGenWriteBarrier(&___btnLexiqueUITex_6, value);
	}

	inline static int32_t get_offset_of_btnARUITex_7() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnARUITex_7)); }
	inline UITexture_t3903132647 * get_btnARUITex_7() const { return ___btnARUITex_7; }
	inline UITexture_t3903132647 ** get_address_of_btnARUITex_7() { return &___btnARUITex_7; }
	inline void set_btnARUITex_7(UITexture_t3903132647 * value)
	{
		___btnARUITex_7 = value;
		Il2CppCodeGenWriteBarrier(&___btnARUITex_7, value);
	}

	inline static int32_t get_offset_of_btnARGLOWTex_8() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnARGLOWTex_8)); }
	inline Texture2D_t3884108195 * get_btnARGLOWTex_8() const { return ___btnARGLOWTex_8; }
	inline Texture2D_t3884108195 ** get_address_of_btnARGLOWTex_8() { return &___btnARGLOWTex_8; }
	inline void set_btnARGLOWTex_8(Texture2D_t3884108195 * value)
	{
		___btnARGLOWTex_8 = value;
		Il2CppCodeGenWriteBarrier(&___btnARGLOWTex_8, value);
	}

	inline static int32_t get_offset_of_btnLexiqueGLOWTex_9() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnLexiqueGLOWTex_9)); }
	inline Texture2D_t3884108195 * get_btnLexiqueGLOWTex_9() const { return ___btnLexiqueGLOWTex_9; }
	inline Texture2D_t3884108195 ** get_address_of_btnLexiqueGLOWTex_9() { return &___btnLexiqueGLOWTex_9; }
	inline void set_btnLexiqueGLOWTex_9(Texture2D_t3884108195 * value)
	{
		___btnLexiqueGLOWTex_9 = value;
		Il2CppCodeGenWriteBarrier(&___btnLexiqueGLOWTex_9, value);
	}

	inline static int32_t get_offset_of_btnTUTUITex_10() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnTUTUITex_10)); }
	inline UITexture_t3903132647 * get_btnTUTUITex_10() const { return ___btnTUTUITex_10; }
	inline UITexture_t3903132647 ** get_address_of_btnTUTUITex_10() { return &___btnTUTUITex_10; }
	inline void set_btnTUTUITex_10(UITexture_t3903132647 * value)
	{
		___btnTUTUITex_10 = value;
		Il2CppCodeGenWriteBarrier(&___btnTUTUITex_10, value);
	}

	inline static int32_t get_offset_of_btnTUTGLOWTex_11() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnTUTGLOWTex_11)); }
	inline Texture2D_t3884108195 * get_btnTUTGLOWTex_11() const { return ___btnTUTGLOWTex_11; }
	inline Texture2D_t3884108195 ** get_address_of_btnTUTGLOWTex_11() { return &___btnTUTGLOWTex_11; }
	inline void set_btnTUTGLOWTex_11(Texture2D_t3884108195 * value)
	{
		___btnTUTGLOWTex_11 = value;
		Il2CppCodeGenWriteBarrier(&___btnTUTGLOWTex_11, value);
	}

	inline static int32_t get_offset_of_btnLegalsUITex_12() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnLegalsUITex_12)); }
	inline UITexture_t3903132647 * get_btnLegalsUITex_12() const { return ___btnLegalsUITex_12; }
	inline UITexture_t3903132647 ** get_address_of_btnLegalsUITex_12() { return &___btnLegalsUITex_12; }
	inline void set_btnLegalsUITex_12(UITexture_t3903132647 * value)
	{
		___btnLegalsUITex_12 = value;
		Il2CppCodeGenWriteBarrier(&___btnLegalsUITex_12, value);
	}

	inline static int32_t get_offset_of_btnLegalsGLOWTex_13() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnLegalsGLOWTex_13)); }
	inline Texture2D_t3884108195 * get_btnLegalsGLOWTex_13() const { return ___btnLegalsGLOWTex_13; }
	inline Texture2D_t3884108195 ** get_address_of_btnLegalsGLOWTex_13() { return &___btnLegalsGLOWTex_13; }
	inline void set_btnLegalsGLOWTex_13(Texture2D_t3884108195 * value)
	{
		___btnLegalsGLOWTex_13 = value;
		Il2CppCodeGenWriteBarrier(&___btnLegalsGLOWTex_13, value);
	}

	inline static int32_t get_offset_of_lexiqueFlashGO_14() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___lexiqueFlashGO_14)); }
	inline GameObject_t3674682005 * get_lexiqueFlashGO_14() const { return ___lexiqueFlashGO_14; }
	inline GameObject_t3674682005 ** get_address_of_lexiqueFlashGO_14() { return &___lexiqueFlashGO_14; }
	inline void set_lexiqueFlashGO_14(GameObject_t3674682005 * value)
	{
		___lexiqueFlashGO_14 = value;
		Il2CppCodeGenWriteBarrier(&___lexiqueFlashGO_14, value);
	}

	inline static int32_t get_offset_of_arFlashGO_15() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arFlashGO_15)); }
	inline GameObject_t3674682005 * get_arFlashGO_15() const { return ___arFlashGO_15; }
	inline GameObject_t3674682005 ** get_address_of_arFlashGO_15() { return &___arFlashGO_15; }
	inline void set_arFlashGO_15(GameObject_t3674682005 * value)
	{
		___arFlashGO_15 = value;
		Il2CppCodeGenWriteBarrier(&___arFlashGO_15, value);
	}

	inline static int32_t get_offset_of_v360FlashGO_16() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___v360FlashGO_16)); }
	inline GameObject_t3674682005 * get_v360FlashGO_16() const { return ___v360FlashGO_16; }
	inline GameObject_t3674682005 ** get_address_of_v360FlashGO_16() { return &___v360FlashGO_16; }
	inline void set_v360FlashGO_16(GameObject_t3674682005 * value)
	{
		___v360FlashGO_16 = value;
		Il2CppCodeGenWriteBarrier(&___v360FlashGO_16, value);
	}

	inline static int32_t get_offset_of_tutFlashGO_17() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutFlashGO_17)); }
	inline GameObject_t3674682005 * get_tutFlashGO_17() const { return ___tutFlashGO_17; }
	inline GameObject_t3674682005 ** get_address_of_tutFlashGO_17() { return &___tutFlashGO_17; }
	inline void set_tutFlashGO_17(GameObject_t3674682005 * value)
	{
		___tutFlashGO_17 = value;
		Il2CppCodeGenWriteBarrier(&___tutFlashGO_17, value);
	}

	inline static int32_t get_offset_of_btnAssitanceNormalTex_18() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnAssitanceNormalTex_18)); }
	inline Texture2D_t3884108195 * get_btnAssitanceNormalTex_18() const { return ___btnAssitanceNormalTex_18; }
	inline Texture2D_t3884108195 ** get_address_of_btnAssitanceNormalTex_18() { return &___btnAssitanceNormalTex_18; }
	inline void set_btnAssitanceNormalTex_18(Texture2D_t3884108195 * value)
	{
		___btnAssitanceNormalTex_18 = value;
		Il2CppCodeGenWriteBarrier(&___btnAssitanceNormalTex_18, value);
	}

	inline static int32_t get_offset_of_btnARNormalTex_19() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnARNormalTex_19)); }
	inline Texture2D_t3884108195 * get_btnARNormalTex_19() const { return ___btnARNormalTex_19; }
	inline Texture2D_t3884108195 ** get_address_of_btnARNormalTex_19() { return &___btnARNormalTex_19; }
	inline void set_btnARNormalTex_19(Texture2D_t3884108195 * value)
	{
		___btnARNormalTex_19 = value;
		Il2CppCodeGenWriteBarrier(&___btnARNormalTex_19, value);
	}

	inline static int32_t get_offset_of_btn360NormalTex_20() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btn360NormalTex_20)); }
	inline Texture2D_t3884108195 * get_btn360NormalTex_20() const { return ___btn360NormalTex_20; }
	inline Texture2D_t3884108195 ** get_address_of_btn360NormalTex_20() { return &___btn360NormalTex_20; }
	inline void set_btn360NormalTex_20(Texture2D_t3884108195 * value)
	{
		___btn360NormalTex_20 = value;
		Il2CppCodeGenWriteBarrier(&___btn360NormalTex_20, value);
	}

	inline static int32_t get_offset_of_btnTUTNormalTex_21() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnTUTNormalTex_21)); }
	inline Texture2D_t3884108195 * get_btnTUTNormalTex_21() const { return ___btnTUTNormalTex_21; }
	inline Texture2D_t3884108195 ** get_address_of_btnTUTNormalTex_21() { return &___btnTUTNormalTex_21; }
	inline void set_btnTUTNormalTex_21(Texture2D_t3884108195 * value)
	{
		___btnTUTNormalTex_21 = value;
		Il2CppCodeGenWriteBarrier(&___btnTUTNormalTex_21, value);
	}

	inline static int32_t get_offset_of_btnLegalsNormalTex_22() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___btnLegalsNormalTex_22)); }
	inline Texture2D_t3884108195 * get_btnLegalsNormalTex_22() const { return ___btnLegalsNormalTex_22; }
	inline Texture2D_t3884108195 ** get_address_of_btnLegalsNormalTex_22() { return &___btnLegalsNormalTex_22; }
	inline void set_btnLegalsNormalTex_22(Texture2D_t3884108195 * value)
	{
		___btnLegalsNormalTex_22 = value;
		Il2CppCodeGenWriteBarrier(&___btnLegalsNormalTex_22, value);
	}

	inline static int32_t get_offset_of_tutoScreen_23() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutoScreen_23)); }
	inline GameObject_t3674682005 * get_tutoScreen_23() const { return ___tutoScreen_23; }
	inline GameObject_t3674682005 ** get_address_of_tutoScreen_23() { return &___tutoScreen_23; }
	inline void set_tutoScreen_23(GameObject_t3674682005 * value)
	{
		___tutoScreen_23 = value;
		Il2CppCodeGenWriteBarrier(&___tutoScreen_23, value);
	}

	inline static int32_t get_offset_of_arHelpFrameTex_24() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arHelpFrameTex_24)); }
	inline UITexture_t3903132647 * get_arHelpFrameTex_24() const { return ___arHelpFrameTex_24; }
	inline UITexture_t3903132647 ** get_address_of_arHelpFrameTex_24() { return &___arHelpFrameTex_24; }
	inline void set_arHelpFrameTex_24(UITexture_t3903132647 * value)
	{
		___arHelpFrameTex_24 = value;
		Il2CppCodeGenWriteBarrier(&___arHelpFrameTex_24, value);
	}

	inline static int32_t get_offset_of_arHelpText_25() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arHelpText_25)); }
	inline GameObject_t3674682005 * get_arHelpText_25() const { return ___arHelpText_25; }
	inline GameObject_t3674682005 ** get_address_of_arHelpText_25() { return &___arHelpText_25; }
	inline void set_arHelpText_25(GameObject_t3674682005 * value)
	{
		___arHelpText_25 = value;
		Il2CppCodeGenWriteBarrier(&___arHelpText_25, value);
	}

	inline static int32_t get_offset_of_goToARBtn_26() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___goToARBtn_26)); }
	inline GameObject_t3674682005 * get_goToARBtn_26() const { return ___goToARBtn_26; }
	inline GameObject_t3674682005 ** get_address_of_goToARBtn_26() { return &___goToARBtn_26; }
	inline void set_goToARBtn_26(GameObject_t3674682005 * value)
	{
		___goToARBtn_26 = value;
		Il2CppCodeGenWriteBarrier(&___goToARBtn_26, value);
	}

	inline static int32_t get_offset_of_backToMenuBtn_27() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___backToMenuBtn_27)); }
	inline GameObject_t3674682005 * get_backToMenuBtn_27() const { return ___backToMenuBtn_27; }
	inline GameObject_t3674682005 ** get_address_of_backToMenuBtn_27() { return &___backToMenuBtn_27; }
	inline void set_backToMenuBtn_27(GameObject_t3674682005 * value)
	{
		___backToMenuBtn_27 = value;
		Il2CppCodeGenWriteBarrier(&___backToMenuBtn_27, value);
	}

	inline static int32_t get_offset_of_backToMenuVRBtn_28() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___backToMenuVRBtn_28)); }
	inline GameObject_t3674682005 * get_backToMenuVRBtn_28() const { return ___backToMenuVRBtn_28; }
	inline GameObject_t3674682005 ** get_address_of_backToMenuVRBtn_28() { return &___backToMenuVRBtn_28; }
	inline void set_backToMenuVRBtn_28(GameObject_t3674682005 * value)
	{
		___backToMenuVRBtn_28 = value;
		Il2CppCodeGenWriteBarrier(&___backToMenuVRBtn_28, value);
	}

	inline static int32_t get_offset_of_goToVRBtn_29() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___goToVRBtn_29)); }
	inline GameObject_t3674682005 * get_goToVRBtn_29() const { return ___goToVRBtn_29; }
	inline GameObject_t3674682005 ** get_address_of_goToVRBtn_29() { return &___goToVRBtn_29; }
	inline void set_goToVRBtn_29(GameObject_t3674682005 * value)
	{
		___goToVRBtn_29 = value;
		Il2CppCodeGenWriteBarrier(&___goToVRBtn_29, value);
	}

	inline static int32_t get_offset_of_arHelpTex_30() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arHelpTex_30)); }
	inline Texture2D_t3884108195 * get_arHelpTex_30() const { return ___arHelpTex_30; }
	inline Texture2D_t3884108195 ** get_address_of_arHelpTex_30() { return &___arHelpTex_30; }
	inline void set_arHelpTex_30(Texture2D_t3884108195 * value)
	{
		___arHelpTex_30 = value;
		Il2CppCodeGenWriteBarrier(&___arHelpTex_30, value);
	}

	inline static int32_t get_offset_of_arTorchTex_31() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arTorchTex_31)); }
	inline Texture2D_t3884108195 * get_arTorchTex_31() const { return ___arTorchTex_31; }
	inline Texture2D_t3884108195 ** get_address_of_arTorchTex_31() { return &___arTorchTex_31; }
	inline void set_arTorchTex_31(Texture2D_t3884108195 * value)
	{
		___arTorchTex_31 = value;
		Il2CppCodeGenWriteBarrier(&___arTorchTex_31, value);
	}

	inline static int32_t get_offset_of_assistanceScreen_32() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___assistanceScreen_32)); }
	inline GameObject_t3674682005 * get_assistanceScreen_32() const { return ___assistanceScreen_32; }
	inline GameObject_t3674682005 ** get_address_of_assistanceScreen_32() { return &___assistanceScreen_32; }
	inline void set_assistanceScreen_32(GameObject_t3674682005 * value)
	{
		___assistanceScreen_32 = value;
		Il2CppCodeGenWriteBarrier(&___assistanceScreen_32, value);
	}

	inline static int32_t get_offset_of_arHelpScreen_33() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arHelpScreen_33)); }
	inline GameObject_t3674682005 * get_arHelpScreen_33() const { return ___arHelpScreen_33; }
	inline GameObject_t3674682005 ** get_address_of_arHelpScreen_33() { return &___arHelpScreen_33; }
	inline void set_arHelpScreen_33(GameObject_t3674682005 * value)
	{
		___arHelpScreen_33 = value;
		Il2CppCodeGenWriteBarrier(&___arHelpScreen_33, value);
	}

	inline static int32_t get_offset_of_vrHelpScreen_34() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___vrHelpScreen_34)); }
	inline GameObject_t3674682005 * get_vrHelpScreen_34() const { return ___vrHelpScreen_34; }
	inline GameObject_t3674682005 ** get_address_of_vrHelpScreen_34() { return &___vrHelpScreen_34; }
	inline void set_vrHelpScreen_34(GameObject_t3674682005 * value)
	{
		___vrHelpScreen_34 = value;
		Il2CppCodeGenWriteBarrier(&___vrHelpScreen_34, value);
	}

	inline static int32_t get_offset_of_legalsScreen_35() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___legalsScreen_35)); }
	inline GameObject_t3674682005 * get_legalsScreen_35() const { return ___legalsScreen_35; }
	inline GameObject_t3674682005 ** get_address_of_legalsScreen_35() { return &___legalsScreen_35; }
	inline void set_legalsScreen_35(GameObject_t3674682005 * value)
	{
		___legalsScreen_35 = value;
		Il2CppCodeGenWriteBarrier(&___legalsScreen_35, value);
	}

	inline static int32_t get_offset_of_v360HelpScreen_36() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___v360HelpScreen_36)); }
	inline GameObject_t3674682005 * get_v360HelpScreen_36() const { return ___v360HelpScreen_36; }
	inline GameObject_t3674682005 ** get_address_of_v360HelpScreen_36() { return &___v360HelpScreen_36; }
	inline void set_v360HelpScreen_36(GameObject_t3674682005 * value)
	{
		___v360HelpScreen_36 = value;
		Il2CppCodeGenWriteBarrier(&___v360HelpScreen_36, value);
	}

	inline static int32_t get_offset_of_tutBtnsPanel_37() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutBtnsPanel_37)); }
	inline UIPanel_t295209936 * get_tutBtnsPanel_37() const { return ___tutBtnsPanel_37; }
	inline UIPanel_t295209936 ** get_address_of_tutBtnsPanel_37() { return &___tutBtnsPanel_37; }
	inline void set_tutBtnsPanel_37(UIPanel_t295209936 * value)
	{
		___tutBtnsPanel_37 = value;
		Il2CppCodeGenWriteBarrier(&___tutBtnsPanel_37, value);
	}

	inline static int32_t get_offset_of_originalTutButtonsPanelClipPos_38() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___originalTutButtonsPanelClipPos_38)); }
	inline Vector2_t4282066565  get_originalTutButtonsPanelClipPos_38() const { return ___originalTutButtonsPanelClipPos_38; }
	inline Vector2_t4282066565 * get_address_of_originalTutButtonsPanelClipPos_38() { return &___originalTutButtonsPanelClipPos_38; }
	inline void set_originalTutButtonsPanelClipPos_38(Vector2_t4282066565  value)
	{
		___originalTutButtonsPanelClipPos_38 = value;
	}

	inline static int32_t get_offset_of_originalTutButtonsPanelPos_39() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___originalTutButtonsPanelPos_39)); }
	inline Vector3_t4282066566  get_originalTutButtonsPanelPos_39() const { return ___originalTutButtonsPanelPos_39; }
	inline Vector3_t4282066566 * get_address_of_originalTutButtonsPanelPos_39() { return &___originalTutButtonsPanelPos_39; }
	inline void set_originalTutButtonsPanelPos_39(Vector3_t4282066566  value)
	{
		___originalTutButtonsPanelPos_39 = value;
	}

	inline static int32_t get_offset_of_backGroundAnim_40() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___backGroundAnim_40)); }
	inline UISpriteAnimation_t4279777547 * get_backGroundAnim_40() const { return ___backGroundAnim_40; }
	inline UISpriteAnimation_t4279777547 ** get_address_of_backGroundAnim_40() { return &___backGroundAnim_40; }
	inline void set_backGroundAnim_40(UISpriteAnimation_t4279777547 * value)
	{
		___backGroundAnim_40 = value;
		Il2CppCodeGenWriteBarrier(&___backGroundAnim_40, value);
	}

	inline static int32_t get_offset_of_loadingBar_41() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___loadingBar_41)); }
	inline GameObject_t3674682005 * get_loadingBar_41() const { return ___loadingBar_41; }
	inline GameObject_t3674682005 ** get_address_of_loadingBar_41() { return &___loadingBar_41; }
	inline void set_loadingBar_41(GameObject_t3674682005 * value)
	{
		___loadingBar_41 = value;
		Il2CppCodeGenWriteBarrier(&___loadingBar_41, value);
	}

	inline static int32_t get_offset_of_loadingFrame_42() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___loadingFrame_42)); }
	inline GameObject_t3674682005 * get_loadingFrame_42() const { return ___loadingFrame_42; }
	inline GameObject_t3674682005 ** get_address_of_loadingFrame_42() { return &___loadingFrame_42; }
	inline void set_loadingFrame_42(GameObject_t3674682005 * value)
	{
		___loadingFrame_42 = value;
		Il2CppCodeGenWriteBarrier(&___loadingFrame_42, value);
	}

	inline static int32_t get_offset_of_vRLoadingBar_43() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___vRLoadingBar_43)); }
	inline GameObject_t3674682005 * get_vRLoadingBar_43() const { return ___vRLoadingBar_43; }
	inline GameObject_t3674682005 ** get_address_of_vRLoadingBar_43() { return &___vRLoadingBar_43; }
	inline void set_vRLoadingBar_43(GameObject_t3674682005 * value)
	{
		___vRLoadingBar_43 = value;
		Il2CppCodeGenWriteBarrier(&___vRLoadingBar_43, value);
	}

	inline static int32_t get_offset_of_vRLoadingFrame_44() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___vRLoadingFrame_44)); }
	inline GameObject_t3674682005 * get_vRLoadingFrame_44() const { return ___vRLoadingFrame_44; }
	inline GameObject_t3674682005 ** get_address_of_vRLoadingFrame_44() { return &___vRLoadingFrame_44; }
	inline void set_vRLoadingFrame_44(GameObject_t3674682005 * value)
	{
		___vRLoadingFrame_44 = value;
		Il2CppCodeGenWriteBarrier(&___vRLoadingFrame_44, value);
	}

	inline static int32_t get_offset_of_arLoadingLabel_45() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___arLoadingLabel_45)); }
	inline UILabel_t291504320 * get_arLoadingLabel_45() const { return ___arLoadingLabel_45; }
	inline UILabel_t291504320 ** get_address_of_arLoadingLabel_45() { return &___arLoadingLabel_45; }
	inline void set_arLoadingLabel_45(UILabel_t291504320 * value)
	{
		___arLoadingLabel_45 = value;
		Il2CppCodeGenWriteBarrier(&___arLoadingLabel_45, value);
	}

	inline static int32_t get_offset_of_showARProgressBar_46() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___showARProgressBar_46)); }
	inline bool get_showARProgressBar_46() const { return ___showARProgressBar_46; }
	inline bool* get_address_of_showARProgressBar_46() { return &___showARProgressBar_46; }
	inline void set_showARProgressBar_46(bool value)
	{
		___showARProgressBar_46 = value;
	}

	inline static int32_t get_offset_of_showVRProgressBar_47() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___showVRProgressBar_47)); }
	inline bool get_showVRProgressBar_47() const { return ___showVRProgressBar_47; }
	inline bool* get_address_of_showVRProgressBar_47() { return &___showVRProgressBar_47; }
	inline void set_showVRProgressBar_47(bool value)
	{
		___showVRProgressBar_47 = value;
	}

	inline static int32_t get_offset_of_barProgress_48() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___barProgress_48)); }
	inline float get_barProgress_48() const { return ___barProgress_48; }
	inline float* get_address_of_barProgress_48() { return &___barProgress_48; }
	inline void set_barProgress_48(float value)
	{
		___barProgress_48 = value;
	}

	inline static int32_t get_offset_of_flashs_49() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___flashs_49)); }
	inline GameObjectU5BU5D_t2662109048* get_flashs_49() const { return ___flashs_49; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_flashs_49() { return &___flashs_49; }
	inline void set_flashs_49(GameObjectU5BU5D_t2662109048* value)
	{
		___flashs_49 = value;
		Il2CppCodeGenWriteBarrier(&___flashs_49, value);
	}

	inline static int32_t get_offset_of_tutBtn_50() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutBtn_50)); }
	inline UIButton_t179429094 * get_tutBtn_50() const { return ___tutBtn_50; }
	inline UIButton_t179429094 ** get_address_of_tutBtn_50() { return &___tutBtn_50; }
	inline void set_tutBtn_50(UIButton_t179429094 * value)
	{
		___tutBtn_50 = value;
		Il2CppCodeGenWriteBarrier(&___tutBtn_50, value);
	}

	inline static int32_t get_offset_of_tutWarningOKBtn_51() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutWarningOKBtn_51)); }
	inline UIButton_t179429094 * get_tutWarningOKBtn_51() const { return ___tutWarningOKBtn_51; }
	inline UIButton_t179429094 ** get_address_of_tutWarningOKBtn_51() { return &___tutWarningOKBtn_51; }
	inline void set_tutWarningOKBtn_51(UIButton_t179429094 * value)
	{
		___tutWarningOKBtn_51 = value;
		Il2CppCodeGenWriteBarrier(&___tutWarningOKBtn_51, value);
	}

	inline static int32_t get_offset_of_tutCatPhoneBtn_52() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutCatPhoneBtn_52)); }
	inline UIButton_t179429094 * get_tutCatPhoneBtn_52() const { return ___tutCatPhoneBtn_52; }
	inline UIButton_t179429094 ** get_address_of_tutCatPhoneBtn_52() { return &___tutCatPhoneBtn_52; }
	inline void set_tutCatPhoneBtn_52(UIButton_t179429094 * value)
	{
		___tutCatPhoneBtn_52 = value;
		Il2CppCodeGenWriteBarrier(&___tutCatPhoneBtn_52, value);
	}

	inline static int32_t get_offset_of_tutCatGPSBtn_53() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutCatGPSBtn_53)); }
	inline UIButton_t179429094 * get_tutCatGPSBtn_53() const { return ___tutCatGPSBtn_53; }
	inline UIButton_t179429094 ** get_address_of_tutCatGPSBtn_53() { return &___tutCatGPSBtn_53; }
	inline void set_tutCatGPSBtn_53(UIButton_t179429094 * value)
	{
		___tutCatGPSBtn_53 = value;
		Il2CppCodeGenWriteBarrier(&___tutCatGPSBtn_53, value);
	}

	inline static int32_t get_offset_of_tutCatAppBtn_54() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutCatAppBtn_54)); }
	inline UIButton_t179429094 * get_tutCatAppBtn_54() const { return ___tutCatAppBtn_54; }
	inline UIButton_t179429094 ** get_address_of_tutCatAppBtn_54() { return &___tutCatAppBtn_54; }
	inline void set_tutCatAppBtn_54(UIButton_t179429094 * value)
	{
		___tutCatAppBtn_54 = value;
		Il2CppCodeGenWriteBarrier(&___tutCatAppBtn_54, value);
	}

	inline static int32_t get_offset_of_tutCatRadioBtn_55() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___tutCatRadioBtn_55)); }
	inline UIButton_t179429094 * get_tutCatRadioBtn_55() const { return ___tutCatRadioBtn_55; }
	inline UIButton_t179429094 ** get_address_of_tutCatRadioBtn_55() { return &___tutCatRadioBtn_55; }
	inline void set_tutCatRadioBtn_55(UIButton_t179429094 * value)
	{
		___tutCatRadioBtn_55 = value;
		Il2CppCodeGenWriteBarrier(&___tutCatRadioBtn_55, value);
	}

	inline static int32_t get_offset_of_uiCam_56() { return static_cast<int32_t>(offsetof(HomeScreenManager_t2427744994, ___uiCam_56)); }
	inline UICamera_t189364953 * get_uiCam_56() const { return ___uiCam_56; }
	inline UICamera_t189364953 ** get_address_of_uiCam_56() { return &___uiCam_56; }
	inline void set_uiCam_56(UICamera_t189364953 * value)
	{
		___uiCam_56 = value;
		Il2CppCodeGenWriteBarrier(&___uiCam_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
