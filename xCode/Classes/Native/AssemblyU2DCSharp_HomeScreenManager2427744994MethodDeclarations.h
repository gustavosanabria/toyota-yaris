﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeScreenManager
struct HomeScreenManager_t2427744994;
// UISpriteAnimation
struct UISpriteAnimation_t4279777547;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UISpriteAnimation4279777547.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HomeScreenManager::.ctor()
extern "C"  void HomeScreenManager__ctor_m4006959305 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::Awake()
extern "C"  void HomeScreenManager_Awake_m4244564524 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::EnableHomeUICamera(System.Boolean)
extern "C"  void HomeScreenManager_EnableHomeUICamera_m937272301 (HomeScreenManager_t2427744994 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::Start()
extern "C"  void HomeScreenManager_Start_m2954097097 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::JumpToARHelp()
extern "C"  void HomeScreenManager_JumpToARHelp_m622804534 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ToMainMenu()
extern "C"  void HomeScreenManager_ToMainMenu_m1864594798 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CloseTutoWindow()
extern "C"  void HomeScreenManager_CloseTutoWindow_m636378443 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::PlayBackgroundFlash(UISpriteAnimation)
extern "C"  void HomeScreenManager_PlayBackgroundFlash_m3495562418 (HomeScreenManager_t2427744994 * __this, UISpriteAnimation_t4279777547 * ___anim0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::PlayFlash()
extern "C"  void HomeScreenManager_PlayFlash_m703092995 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CloseAssistanceWindow()
extern "C"  void HomeScreenManager_CloseAssistanceWindow_m4150686023 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CallNumber1()
extern "C"  void HomeScreenManager_CallNumber1_m2228458673 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CallNumber1B()
extern "C"  void HomeScreenManager_CallNumber1B_m362767123 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CallNumber2()
extern "C"  void HomeScreenManager_CallNumber2_m2228459634 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CallNumber2B()
extern "C"  void HomeScreenManager_CallNumber2B_m362796914 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CloseARHelpWindow()
extern "C"  void HomeScreenManager_CloseARHelpWindow_m2843688449 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::Close360HelpWindow()
extern "C"  void HomeScreenManager_Close360HelpWindow_m4263847425 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ButtonAssistanceCLICK()
extern "C"  void HomeScreenManager_ButtonAssistanceCLICK_m382366213 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ButtonLegalsCLICK()
extern "C"  void HomeScreenManager_ButtonLegalsCLICK_m1491285027 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::CloseLegalsWindow()
extern "C"  void HomeScreenManager_CloseLegalsWindow_m1769860073 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ShowARHelpVideo()
extern "C"  void HomeScreenManager_ShowARHelpVideo_m2381804755 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ButtonARCLICK()
extern "C"  void HomeScreenManager_ButtonARCLICK_m3740592908 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ButtonLexiqueCLICK()
extern "C"  void HomeScreenManager_ButtonLexiqueCLICK_m3500985566 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::LoadGlossary()
extern "C"  void HomeScreenManager_LoadGlossary_m2609058175 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::Button360ViewCLICK()
extern "C"  void HomeScreenManager_Button360ViewCLICK_m675861059 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::OnHelpTextSlided()
extern "C"  void HomeScreenManager_OnHelpTextSlided_m3355220635 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::EnableGotoARbtn(System.Boolean)
extern "C"  void HomeScreenManager_EnableGotoARbtn_m1325744227 (HomeScreenManager_t2427744994 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ButtonTutorialCLICK()
extern "C"  void HomeScreenManager_ButtonTutorialCLICK_m1116486687 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::EnableButtons(System.Boolean)
extern "C"  void HomeScreenManager_EnableButtons_m3520829852 (HomeScreenManager_t2427744994 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::EnableFlash(UnityEngine.GameObject)
extern "C"  void HomeScreenManager_EnableFlash_m2853547724 (HomeScreenManager_t2427744994 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::DisableAllFlashs()
extern "C"  void HomeScreenManager_DisableAllFlashs_m3683658455 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ShowVRLoading()
extern "C"  void HomeScreenManager_ShowVRLoading_m195344298 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::ShowARLoading()
extern "C"  void HomeScreenManager_ShowARLoading_m3571584469 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::GoToVrBitches()
extern "C"  void HomeScreenManager_GoToVrBitches_m3617042792 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeScreenManager::GoToArBitches()
extern "C"  void HomeScreenManager_GoToArBitches_m2698315667 (HomeScreenManager_t2427744994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
