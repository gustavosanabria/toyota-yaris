﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// TweenScale[]
struct TweenScaleU5BU5D_t3591996518;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightIconsManager
struct  LightIconsManager_t444881737  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject LightIconsManager::iconsContainer
	GameObject_t3674682005 * ___iconsContainer_2;
	// UnityEngine.GameObject LightIconsManager::target28IconContainer
	GameObject_t3674682005 * ___target28IconContainer_3;
	// UnityEngine.GameObject LightIconsManager::targetSHIFTIconContainer
	GameObject_t3674682005 * ___targetSHIFTIconContainer_4;
	// UnityEngine.GameObject LightIconsManager::target30IconContainer
	GameObject_t3674682005 * ___target30IconContainer_5;
	// TweenScale[] LightIconsManager::lightIconTweens
	TweenScaleU5BU5D_t3591996518* ___lightIconTweens_6;
	// UnityEngine.GameObject LightIconsManager::currentIcon
	GameObject_t3674682005 * ___currentIcon_7;
	// UnityEngine.GameObject LightIconsManager::currentDescription
	GameObject_t3674682005 * ___currentDescription_8;
	// System.Byte LightIconsManager::iconsCount
	uint8_t ___iconsCount_9;
	// System.Boolean LightIconsManager::showingDescription
	bool ___showingDescription_10;
	// UnityEngine.GameObject LightIconsManager::currDescr
	GameObject_t3674682005 * ___currDescr_11;

public:
	inline static int32_t get_offset_of_iconsContainer_2() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___iconsContainer_2)); }
	inline GameObject_t3674682005 * get_iconsContainer_2() const { return ___iconsContainer_2; }
	inline GameObject_t3674682005 ** get_address_of_iconsContainer_2() { return &___iconsContainer_2; }
	inline void set_iconsContainer_2(GameObject_t3674682005 * value)
	{
		___iconsContainer_2 = value;
		Il2CppCodeGenWriteBarrier(&___iconsContainer_2, value);
	}

	inline static int32_t get_offset_of_target28IconContainer_3() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___target28IconContainer_3)); }
	inline GameObject_t3674682005 * get_target28IconContainer_3() const { return ___target28IconContainer_3; }
	inline GameObject_t3674682005 ** get_address_of_target28IconContainer_3() { return &___target28IconContainer_3; }
	inline void set_target28IconContainer_3(GameObject_t3674682005 * value)
	{
		___target28IconContainer_3 = value;
		Il2CppCodeGenWriteBarrier(&___target28IconContainer_3, value);
	}

	inline static int32_t get_offset_of_targetSHIFTIconContainer_4() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___targetSHIFTIconContainer_4)); }
	inline GameObject_t3674682005 * get_targetSHIFTIconContainer_4() const { return ___targetSHIFTIconContainer_4; }
	inline GameObject_t3674682005 ** get_address_of_targetSHIFTIconContainer_4() { return &___targetSHIFTIconContainer_4; }
	inline void set_targetSHIFTIconContainer_4(GameObject_t3674682005 * value)
	{
		___targetSHIFTIconContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetSHIFTIconContainer_4, value);
	}

	inline static int32_t get_offset_of_target30IconContainer_5() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___target30IconContainer_5)); }
	inline GameObject_t3674682005 * get_target30IconContainer_5() const { return ___target30IconContainer_5; }
	inline GameObject_t3674682005 ** get_address_of_target30IconContainer_5() { return &___target30IconContainer_5; }
	inline void set_target30IconContainer_5(GameObject_t3674682005 * value)
	{
		___target30IconContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___target30IconContainer_5, value);
	}

	inline static int32_t get_offset_of_lightIconTweens_6() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___lightIconTweens_6)); }
	inline TweenScaleU5BU5D_t3591996518* get_lightIconTweens_6() const { return ___lightIconTweens_6; }
	inline TweenScaleU5BU5D_t3591996518** get_address_of_lightIconTweens_6() { return &___lightIconTweens_6; }
	inline void set_lightIconTweens_6(TweenScaleU5BU5D_t3591996518* value)
	{
		___lightIconTweens_6 = value;
		Il2CppCodeGenWriteBarrier(&___lightIconTweens_6, value);
	}

	inline static int32_t get_offset_of_currentIcon_7() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___currentIcon_7)); }
	inline GameObject_t3674682005 * get_currentIcon_7() const { return ___currentIcon_7; }
	inline GameObject_t3674682005 ** get_address_of_currentIcon_7() { return &___currentIcon_7; }
	inline void set_currentIcon_7(GameObject_t3674682005 * value)
	{
		___currentIcon_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentIcon_7, value);
	}

	inline static int32_t get_offset_of_currentDescription_8() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___currentDescription_8)); }
	inline GameObject_t3674682005 * get_currentDescription_8() const { return ___currentDescription_8; }
	inline GameObject_t3674682005 ** get_address_of_currentDescription_8() { return &___currentDescription_8; }
	inline void set_currentDescription_8(GameObject_t3674682005 * value)
	{
		___currentDescription_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentDescription_8, value);
	}

	inline static int32_t get_offset_of_iconsCount_9() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___iconsCount_9)); }
	inline uint8_t get_iconsCount_9() const { return ___iconsCount_9; }
	inline uint8_t* get_address_of_iconsCount_9() { return &___iconsCount_9; }
	inline void set_iconsCount_9(uint8_t value)
	{
		___iconsCount_9 = value;
	}

	inline static int32_t get_offset_of_showingDescription_10() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___showingDescription_10)); }
	inline bool get_showingDescription_10() const { return ___showingDescription_10; }
	inline bool* get_address_of_showingDescription_10() { return &___showingDescription_10; }
	inline void set_showingDescription_10(bool value)
	{
		___showingDescription_10 = value;
	}

	inline static int32_t get_offset_of_currDescr_11() { return static_cast<int32_t>(offsetof(LightIconsManager_t444881737, ___currDescr_11)); }
	inline GameObject_t3674682005 * get_currDescr_11() const { return ___currDescr_11; }
	inline GameObject_t3674682005 ** get_address_of_currDescr_11() { return &___currDescr_11; }
	inline void set_currDescr_11(GameObject_t3674682005 * value)
	{
		___currDescr_11 = value;
		Il2CppCodeGenWriteBarrier(&___currDescr_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
