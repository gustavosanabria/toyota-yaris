﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LightIconsManager
struct LightIconsManager_t444881737;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void LightIconsManager::.ctor()
extern "C"  void LightIconsManager__ctor_m3589731394 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::Awake()
extern "C"  void LightIconsManager_Awake_m3827336613 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::DisableIconContainers()
extern "C"  void LightIconsManager_DisableIconContainers_m4170415379 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::SetIconsGroup(UnityEngine.GameObject)
extern "C"  void LightIconsManager_SetIconsGroup_m3950782047 (LightIconsManager_t444881737 * __this, GameObject_t3674682005 * ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::ShowIcons()
extern "C"  void LightIconsManager_ShowIcons_m4044086461 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::HideIcons()
extern "C"  void LightIconsManager_HideIcons_m3535516376 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::DisableIconsContainer()
extern "C"  void LightIconsManager_DisableIconsContainer_m1404551983 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::CloseIconDescription()
extern "C"  void LightIconsManager_CloseIconDescription_m4088646829 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::CloseDescription()
extern "C"  void LightIconsManager_CloseDescription_m2425211174 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::SetCurrentIcon(UnityEngine.GameObject)
extern "C"  void LightIconsManager_SetCurrentIcon_m393969994 (LightIconsManager_t444881737 * __this, GameObject_t3674682005 * ___icon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::SetCurrenDescription(UnityEngine.GameObject)
extern "C"  void LightIconsManager_SetCurrenDescription_m1851516441 (LightIconsManager_t444881737 * __this, GameObject_t3674682005 * ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::CloseCurrenDescription()
extern "C"  void LightIconsManager_CloseCurrenDescription_m563909579 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::StopIconAnimation()
extern "C"  void LightIconsManager_StopIconAnimation_m3797913289 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::EnableIcons(System.Boolean)
extern "C"  void LightIconsManager_EnableIcons_m3824516334 (LightIconsManager_t444881737 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightIconsManager::ToIconSelect()
extern "C"  void LightIconsManager_ToIconSelect_m3578222450 (LightIconsManager_t444881737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
