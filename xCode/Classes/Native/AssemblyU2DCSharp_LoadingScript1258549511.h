﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingScript
struct  LoadingScript_t1258549511  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AsyncOperation LoadingScript::levelAsync
	AsyncOperation_t3699081103 * ___levelAsync_2;
	// System.Boolean LoadingScript::showARProgressBar
	bool ___showARProgressBar_3;
	// UnityEngine.GameObject LoadingScript::loadingBar
	GameObject_t3674682005 * ___loadingBar_4;
	// UITexture LoadingScript::helpTex
	UITexture_t3903132647 * ___helpTex_5;
	// UnityEngine.Texture2D LoadingScript::vrHelpTex
	Texture2D_t3884108195 * ___vrHelpTex_6;

public:
	inline static int32_t get_offset_of_levelAsync_2() { return static_cast<int32_t>(offsetof(LoadingScript_t1258549511, ___levelAsync_2)); }
	inline AsyncOperation_t3699081103 * get_levelAsync_2() const { return ___levelAsync_2; }
	inline AsyncOperation_t3699081103 ** get_address_of_levelAsync_2() { return &___levelAsync_2; }
	inline void set_levelAsync_2(AsyncOperation_t3699081103 * value)
	{
		___levelAsync_2 = value;
		Il2CppCodeGenWriteBarrier(&___levelAsync_2, value);
	}

	inline static int32_t get_offset_of_showARProgressBar_3() { return static_cast<int32_t>(offsetof(LoadingScript_t1258549511, ___showARProgressBar_3)); }
	inline bool get_showARProgressBar_3() const { return ___showARProgressBar_3; }
	inline bool* get_address_of_showARProgressBar_3() { return &___showARProgressBar_3; }
	inline void set_showARProgressBar_3(bool value)
	{
		___showARProgressBar_3 = value;
	}

	inline static int32_t get_offset_of_loadingBar_4() { return static_cast<int32_t>(offsetof(LoadingScript_t1258549511, ___loadingBar_4)); }
	inline GameObject_t3674682005 * get_loadingBar_4() const { return ___loadingBar_4; }
	inline GameObject_t3674682005 ** get_address_of_loadingBar_4() { return &___loadingBar_4; }
	inline void set_loadingBar_4(GameObject_t3674682005 * value)
	{
		___loadingBar_4 = value;
		Il2CppCodeGenWriteBarrier(&___loadingBar_4, value);
	}

	inline static int32_t get_offset_of_helpTex_5() { return static_cast<int32_t>(offsetof(LoadingScript_t1258549511, ___helpTex_5)); }
	inline UITexture_t3903132647 * get_helpTex_5() const { return ___helpTex_5; }
	inline UITexture_t3903132647 ** get_address_of_helpTex_5() { return &___helpTex_5; }
	inline void set_helpTex_5(UITexture_t3903132647 * value)
	{
		___helpTex_5 = value;
		Il2CppCodeGenWriteBarrier(&___helpTex_5, value);
	}

	inline static int32_t get_offset_of_vrHelpTex_6() { return static_cast<int32_t>(offsetof(LoadingScript_t1258549511, ___vrHelpTex_6)); }
	inline Texture2D_t3884108195 * get_vrHelpTex_6() const { return ___vrHelpTex_6; }
	inline Texture2D_t3884108195 ** get_address_of_vrHelpTex_6() { return &___vrHelpTex_6; }
	inline void set_vrHelpTex_6(Texture2D_t3884108195 * value)
	{
		___vrHelpTex_6 = value;
		Il2CppCodeGenWriteBarrier(&___vrHelpTex_6, value);
	}
};

struct LoadingScript_t1258549511_StaticFields
{
public:
	// System.Byte LoadingScript::sceneID
	uint8_t ___sceneID_7;

public:
	inline static int32_t get_offset_of_sceneID_7() { return static_cast<int32_t>(offsetof(LoadingScript_t1258549511_StaticFields, ___sceneID_7)); }
	inline uint8_t get_sceneID_7() const { return ___sceneID_7; }
	inline uint8_t* get_address_of_sceneID_7() { return &___sceneID_7; }
	inline void set_sceneID_7(uint8_t value)
	{
		___sceneID_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
