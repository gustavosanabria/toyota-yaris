﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingScript
struct LoadingScript_t1258549511;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadingScript::.ctor()
extern "C"  void LoadingScript__ctor_m546809924 (LoadingScript_t1258549511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScript::Start()
extern "C"  void LoadingScript_Start_m3788915012 (LoadingScript_t1258549511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScript::SetLevelToLoad(System.Byte)
extern "C"  void LoadingScript_SetLevelToLoad_m123772328 (Il2CppObject * __this /* static, unused */, uint8_t ___pSceneID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScript::LoadScene()
extern "C"  void LoadingScript_LoadScene_m2936848040 (LoadingScript_t1258549511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScript::Update()
extern "C"  void LoadingScript_Update_m1498100553 (LoadingScript_t1258549511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
