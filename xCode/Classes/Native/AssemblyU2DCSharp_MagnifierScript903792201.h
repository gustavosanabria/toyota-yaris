﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UITexture
struct UITexture_t3903132647;
// OldGyroscopeController
struct OldGyroscopeController_t3590691066;
// GyroscopeControllerFullFreedom
struct GyroscopeControllerFullFreedom_t685599910;
// DescriptionsManager
struct DescriptionsManager_t373055382;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagnifierScript
struct  MagnifierScript_t903792201  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean MagnifierScript::magnify
	bool ___magnify_2;
	// UnityEngine.Camera MagnifierScript::guiCamera
	Camera_t2727095145 * ___guiCamera_3;
	// UnityEngine.Vector3 MagnifierScript::firstFingerPos
	Vector3_t4282066566  ___firstFingerPos_4;
	// UnityEngine.Vector3 MagnifierScript::currentFingerPos
	Vector3_t4282066566  ___currentFingerPos_5;
	// UnityEngine.Vector3 MagnifierScript::direction
	Vector3_t4282066566  ___direction_6;
	// UnityEngine.Vector3 MagnifierScript::colPoint
	Vector3_t4282066566  ___colPoint_7;
	// UnityEngine.RaycastHit MagnifierScript::rcH
	RaycastHit_t4003175726  ___rcH_8;
	// UnityEngine.GameObject MagnifierScript::magnifierCamera
	GameObject_t3674682005 * ___magnifierCamera_9;
	// UnityEngine.GameObject MagnifierScript::magnifierContainer
	GameObject_t3674682005 * ___magnifierContainer_10;
	// UITexture MagnifierScript::magnifierFrameUiTex
	UITexture_t3903132647 * ___magnifierFrameUiTex_11;
	// System.Single MagnifierScript::originalZ
	float ___originalZ_12;
	// System.Boolean MagnifierScript::isVr
	bool ___isVr_13;
	// OldGyroscopeController MagnifierScript::gyroControl
	OldGyroscopeController_t3590691066 * ___gyroControl_14;
	// GyroscopeControllerFullFreedom MagnifierScript::gyroControlFull
	GyroscopeControllerFullFreedom_t685599910 * ___gyroControlFull_15;
	// System.Single MagnifierScript::magnifierFactor
	float ___magnifierFactor_16;
	// UITexture MagnifierScript::textureToMagnify
	UITexture_t3903132647 * ___textureToMagnify_17;
	// DescriptionsManager MagnifierScript::descrMgr
	DescriptionsManager_t373055382 * ___descrMgr_18;
	// UnityEngine.Vector2 MagnifierScript::initSize
	Vector2_t4282066565  ___initSize_19;
	// System.Boolean MagnifierScript::magnifierEnabled
	bool ___magnifierEnabled_20;
	// UnityEngine.Touch MagnifierScript::t1
	Touch_t4210255029  ___t1_21;
	// UnityEngine.Touch MagnifierScript::t2
	Touch_t4210255029  ___t2_22;
	// System.Single MagnifierScript::zoomValue
	float ___zoomValue_23;
	// System.Single MagnifierScript::offsetX
	float ___offsetX_24;
	// System.Single MagnifierScript::offsetYMin
	float ___offsetYMin_25;
	// System.Single MagnifierScript::offsetYMax
	float ___offsetYMax_26;
	// UnityEngine.Vector3 MagnifierScript::pos
	Vector3_t4282066566  ___pos_27;
	// UnityEngine.Vector3 MagnifierScript::toPos
	Vector3_t4282066566  ___toPos_28;
	// UnityEngine.Touch MagnifierScript::lastT
	Touch_t4210255029  ___lastT_29;

public:
	inline static int32_t get_offset_of_magnify_2() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___magnify_2)); }
	inline bool get_magnify_2() const { return ___magnify_2; }
	inline bool* get_address_of_magnify_2() { return &___magnify_2; }
	inline void set_magnify_2(bool value)
	{
		___magnify_2 = value;
	}

	inline static int32_t get_offset_of_guiCamera_3() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___guiCamera_3)); }
	inline Camera_t2727095145 * get_guiCamera_3() const { return ___guiCamera_3; }
	inline Camera_t2727095145 ** get_address_of_guiCamera_3() { return &___guiCamera_3; }
	inline void set_guiCamera_3(Camera_t2727095145 * value)
	{
		___guiCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___guiCamera_3, value);
	}

	inline static int32_t get_offset_of_firstFingerPos_4() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___firstFingerPos_4)); }
	inline Vector3_t4282066566  get_firstFingerPos_4() const { return ___firstFingerPos_4; }
	inline Vector3_t4282066566 * get_address_of_firstFingerPos_4() { return &___firstFingerPos_4; }
	inline void set_firstFingerPos_4(Vector3_t4282066566  value)
	{
		___firstFingerPos_4 = value;
	}

	inline static int32_t get_offset_of_currentFingerPos_5() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___currentFingerPos_5)); }
	inline Vector3_t4282066566  get_currentFingerPos_5() const { return ___currentFingerPos_5; }
	inline Vector3_t4282066566 * get_address_of_currentFingerPos_5() { return &___currentFingerPos_5; }
	inline void set_currentFingerPos_5(Vector3_t4282066566  value)
	{
		___currentFingerPos_5 = value;
	}

	inline static int32_t get_offset_of_direction_6() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___direction_6)); }
	inline Vector3_t4282066566  get_direction_6() const { return ___direction_6; }
	inline Vector3_t4282066566 * get_address_of_direction_6() { return &___direction_6; }
	inline void set_direction_6(Vector3_t4282066566  value)
	{
		___direction_6 = value;
	}

	inline static int32_t get_offset_of_colPoint_7() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___colPoint_7)); }
	inline Vector3_t4282066566  get_colPoint_7() const { return ___colPoint_7; }
	inline Vector3_t4282066566 * get_address_of_colPoint_7() { return &___colPoint_7; }
	inline void set_colPoint_7(Vector3_t4282066566  value)
	{
		___colPoint_7 = value;
	}

	inline static int32_t get_offset_of_rcH_8() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___rcH_8)); }
	inline RaycastHit_t4003175726  get_rcH_8() const { return ___rcH_8; }
	inline RaycastHit_t4003175726 * get_address_of_rcH_8() { return &___rcH_8; }
	inline void set_rcH_8(RaycastHit_t4003175726  value)
	{
		___rcH_8 = value;
	}

	inline static int32_t get_offset_of_magnifierCamera_9() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___magnifierCamera_9)); }
	inline GameObject_t3674682005 * get_magnifierCamera_9() const { return ___magnifierCamera_9; }
	inline GameObject_t3674682005 ** get_address_of_magnifierCamera_9() { return &___magnifierCamera_9; }
	inline void set_magnifierCamera_9(GameObject_t3674682005 * value)
	{
		___magnifierCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&___magnifierCamera_9, value);
	}

	inline static int32_t get_offset_of_magnifierContainer_10() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___magnifierContainer_10)); }
	inline GameObject_t3674682005 * get_magnifierContainer_10() const { return ___magnifierContainer_10; }
	inline GameObject_t3674682005 ** get_address_of_magnifierContainer_10() { return &___magnifierContainer_10; }
	inline void set_magnifierContainer_10(GameObject_t3674682005 * value)
	{
		___magnifierContainer_10 = value;
		Il2CppCodeGenWriteBarrier(&___magnifierContainer_10, value);
	}

	inline static int32_t get_offset_of_magnifierFrameUiTex_11() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___magnifierFrameUiTex_11)); }
	inline UITexture_t3903132647 * get_magnifierFrameUiTex_11() const { return ___magnifierFrameUiTex_11; }
	inline UITexture_t3903132647 ** get_address_of_magnifierFrameUiTex_11() { return &___magnifierFrameUiTex_11; }
	inline void set_magnifierFrameUiTex_11(UITexture_t3903132647 * value)
	{
		___magnifierFrameUiTex_11 = value;
		Il2CppCodeGenWriteBarrier(&___magnifierFrameUiTex_11, value);
	}

	inline static int32_t get_offset_of_originalZ_12() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___originalZ_12)); }
	inline float get_originalZ_12() const { return ___originalZ_12; }
	inline float* get_address_of_originalZ_12() { return &___originalZ_12; }
	inline void set_originalZ_12(float value)
	{
		___originalZ_12 = value;
	}

	inline static int32_t get_offset_of_isVr_13() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___isVr_13)); }
	inline bool get_isVr_13() const { return ___isVr_13; }
	inline bool* get_address_of_isVr_13() { return &___isVr_13; }
	inline void set_isVr_13(bool value)
	{
		___isVr_13 = value;
	}

	inline static int32_t get_offset_of_gyroControl_14() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___gyroControl_14)); }
	inline OldGyroscopeController_t3590691066 * get_gyroControl_14() const { return ___gyroControl_14; }
	inline OldGyroscopeController_t3590691066 ** get_address_of_gyroControl_14() { return &___gyroControl_14; }
	inline void set_gyroControl_14(OldGyroscopeController_t3590691066 * value)
	{
		___gyroControl_14 = value;
		Il2CppCodeGenWriteBarrier(&___gyroControl_14, value);
	}

	inline static int32_t get_offset_of_gyroControlFull_15() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___gyroControlFull_15)); }
	inline GyroscopeControllerFullFreedom_t685599910 * get_gyroControlFull_15() const { return ___gyroControlFull_15; }
	inline GyroscopeControllerFullFreedom_t685599910 ** get_address_of_gyroControlFull_15() { return &___gyroControlFull_15; }
	inline void set_gyroControlFull_15(GyroscopeControllerFullFreedom_t685599910 * value)
	{
		___gyroControlFull_15 = value;
		Il2CppCodeGenWriteBarrier(&___gyroControlFull_15, value);
	}

	inline static int32_t get_offset_of_magnifierFactor_16() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___magnifierFactor_16)); }
	inline float get_magnifierFactor_16() const { return ___magnifierFactor_16; }
	inline float* get_address_of_magnifierFactor_16() { return &___magnifierFactor_16; }
	inline void set_magnifierFactor_16(float value)
	{
		___magnifierFactor_16 = value;
	}

	inline static int32_t get_offset_of_textureToMagnify_17() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___textureToMagnify_17)); }
	inline UITexture_t3903132647 * get_textureToMagnify_17() const { return ___textureToMagnify_17; }
	inline UITexture_t3903132647 ** get_address_of_textureToMagnify_17() { return &___textureToMagnify_17; }
	inline void set_textureToMagnify_17(UITexture_t3903132647 * value)
	{
		___textureToMagnify_17 = value;
		Il2CppCodeGenWriteBarrier(&___textureToMagnify_17, value);
	}

	inline static int32_t get_offset_of_descrMgr_18() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___descrMgr_18)); }
	inline DescriptionsManager_t373055382 * get_descrMgr_18() const { return ___descrMgr_18; }
	inline DescriptionsManager_t373055382 ** get_address_of_descrMgr_18() { return &___descrMgr_18; }
	inline void set_descrMgr_18(DescriptionsManager_t373055382 * value)
	{
		___descrMgr_18 = value;
		Il2CppCodeGenWriteBarrier(&___descrMgr_18, value);
	}

	inline static int32_t get_offset_of_initSize_19() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___initSize_19)); }
	inline Vector2_t4282066565  get_initSize_19() const { return ___initSize_19; }
	inline Vector2_t4282066565 * get_address_of_initSize_19() { return &___initSize_19; }
	inline void set_initSize_19(Vector2_t4282066565  value)
	{
		___initSize_19 = value;
	}

	inline static int32_t get_offset_of_magnifierEnabled_20() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___magnifierEnabled_20)); }
	inline bool get_magnifierEnabled_20() const { return ___magnifierEnabled_20; }
	inline bool* get_address_of_magnifierEnabled_20() { return &___magnifierEnabled_20; }
	inline void set_magnifierEnabled_20(bool value)
	{
		___magnifierEnabled_20 = value;
	}

	inline static int32_t get_offset_of_t1_21() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___t1_21)); }
	inline Touch_t4210255029  get_t1_21() const { return ___t1_21; }
	inline Touch_t4210255029 * get_address_of_t1_21() { return &___t1_21; }
	inline void set_t1_21(Touch_t4210255029  value)
	{
		___t1_21 = value;
	}

	inline static int32_t get_offset_of_t2_22() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___t2_22)); }
	inline Touch_t4210255029  get_t2_22() const { return ___t2_22; }
	inline Touch_t4210255029 * get_address_of_t2_22() { return &___t2_22; }
	inline void set_t2_22(Touch_t4210255029  value)
	{
		___t2_22 = value;
	}

	inline static int32_t get_offset_of_zoomValue_23() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___zoomValue_23)); }
	inline float get_zoomValue_23() const { return ___zoomValue_23; }
	inline float* get_address_of_zoomValue_23() { return &___zoomValue_23; }
	inline void set_zoomValue_23(float value)
	{
		___zoomValue_23 = value;
	}

	inline static int32_t get_offset_of_offsetX_24() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___offsetX_24)); }
	inline float get_offsetX_24() const { return ___offsetX_24; }
	inline float* get_address_of_offsetX_24() { return &___offsetX_24; }
	inline void set_offsetX_24(float value)
	{
		___offsetX_24 = value;
	}

	inline static int32_t get_offset_of_offsetYMin_25() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___offsetYMin_25)); }
	inline float get_offsetYMin_25() const { return ___offsetYMin_25; }
	inline float* get_address_of_offsetYMin_25() { return &___offsetYMin_25; }
	inline void set_offsetYMin_25(float value)
	{
		___offsetYMin_25 = value;
	}

	inline static int32_t get_offset_of_offsetYMax_26() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___offsetYMax_26)); }
	inline float get_offsetYMax_26() const { return ___offsetYMax_26; }
	inline float* get_address_of_offsetYMax_26() { return &___offsetYMax_26; }
	inline void set_offsetYMax_26(float value)
	{
		___offsetYMax_26 = value;
	}

	inline static int32_t get_offset_of_pos_27() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___pos_27)); }
	inline Vector3_t4282066566  get_pos_27() const { return ___pos_27; }
	inline Vector3_t4282066566 * get_address_of_pos_27() { return &___pos_27; }
	inline void set_pos_27(Vector3_t4282066566  value)
	{
		___pos_27 = value;
	}

	inline static int32_t get_offset_of_toPos_28() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___toPos_28)); }
	inline Vector3_t4282066566  get_toPos_28() const { return ___toPos_28; }
	inline Vector3_t4282066566 * get_address_of_toPos_28() { return &___toPos_28; }
	inline void set_toPos_28(Vector3_t4282066566  value)
	{
		___toPos_28 = value;
	}

	inline static int32_t get_offset_of_lastT_29() { return static_cast<int32_t>(offsetof(MagnifierScript_t903792201, ___lastT_29)); }
	inline Touch_t4210255029  get_lastT_29() const { return ___lastT_29; }
	inline Touch_t4210255029 * get_address_of_lastT_29() { return &___lastT_29; }
	inline void set_lastT_29(Touch_t4210255029  value)
	{
		___lastT_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
