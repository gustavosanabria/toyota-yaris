﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagnifierScript
struct MagnifierScript_t903792201;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"

// System.Void MagnifierScript::.ctor()
extern "C"  void MagnifierScript__ctor_m3430482754 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::Awake()
extern "C"  void MagnifierScript_Awake_m3668087973 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::Start()
extern "C"  void MagnifierScript_Start_m2377620546 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::Magnify()
extern "C"  void MagnifierScript_Magnify_m84214785 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::ForceMagnifierActive()
extern "C"  void MagnifierScript_ForceMagnifierActive_m1755202491 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::SetMagnifierZoom(System.Single)
extern "C"  void MagnifierScript_SetMagnifierZoom_m3908228378 (MagnifierScript_t903792201 * __this, float ___zoom0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::DisableMagnifier()
extern "C"  void MagnifierScript_DisableMagnifier_m4286973112 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::MagnifierActive(System.Boolean)
extern "C"  void MagnifierScript_MagnifierActive_m2957332411 (MagnifierScript_t903792201 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::SmoothZoom(UnityEngine.Touch,UnityEngine.Touch)
extern "C"  void MagnifierScript_SmoothZoom_m828706571 (MagnifierScript_t903792201 * __this, Touch_t4210255029  ___touchZero0, Touch_t4210255029  ___touchOne1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagnifierScript::Update()
extern "C"  void MagnifierScript_Update_m697645067 (MagnifierScript_t903792201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
