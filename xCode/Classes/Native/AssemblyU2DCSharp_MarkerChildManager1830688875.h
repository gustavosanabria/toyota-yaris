﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenScale[]
struct TweenScaleU5BU5D_t3591996518;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerChildManager
struct  MarkerChildManager_t1830688875  : public MonoBehaviour_t667441552
{
public:
	// TweenScale[] MarkerChildManager::childTweens
	TweenScaleU5BU5D_t3591996518* ___childTweens_2;
	// System.Byte MarkerChildManager::childTweensCount
	uint8_t ___childTweensCount_3;
	// UnityEngine.Vector3 MarkerChildManager::childInitialSize
	Vector3_t4282066566  ___childInitialSize_4;

public:
	inline static int32_t get_offset_of_childTweens_2() { return static_cast<int32_t>(offsetof(MarkerChildManager_t1830688875, ___childTweens_2)); }
	inline TweenScaleU5BU5D_t3591996518* get_childTweens_2() const { return ___childTweens_2; }
	inline TweenScaleU5BU5D_t3591996518** get_address_of_childTweens_2() { return &___childTweens_2; }
	inline void set_childTweens_2(TweenScaleU5BU5D_t3591996518* value)
	{
		___childTweens_2 = value;
		Il2CppCodeGenWriteBarrier(&___childTweens_2, value);
	}

	inline static int32_t get_offset_of_childTweensCount_3() { return static_cast<int32_t>(offsetof(MarkerChildManager_t1830688875, ___childTweensCount_3)); }
	inline uint8_t get_childTweensCount_3() const { return ___childTweensCount_3; }
	inline uint8_t* get_address_of_childTweensCount_3() { return &___childTweensCount_3; }
	inline void set_childTweensCount_3(uint8_t value)
	{
		___childTweensCount_3 = value;
	}

	inline static int32_t get_offset_of_childInitialSize_4() { return static_cast<int32_t>(offsetof(MarkerChildManager_t1830688875, ___childInitialSize_4)); }
	inline Vector3_t4282066566  get_childInitialSize_4() const { return ___childInitialSize_4; }
	inline Vector3_t4282066566 * get_address_of_childInitialSize_4() { return &___childInitialSize_4; }
	inline void set_childInitialSize_4(Vector3_t4282066566  value)
	{
		___childInitialSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
