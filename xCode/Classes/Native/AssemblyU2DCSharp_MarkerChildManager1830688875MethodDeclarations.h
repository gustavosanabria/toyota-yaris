﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerChildManager
struct MarkerChildManager_t1830688875;

#include "codegen/il2cpp-codegen.h"

// System.Void MarkerChildManager::.ctor()
extern "C"  void MarkerChildManager__ctor_m1242902672 (MarkerChildManager_t1830688875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerChildManager::Awake()
extern "C"  void MarkerChildManager_Awake_m1480507891 (MarkerChildManager_t1830688875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerChildManager::AnimateTargets()
extern "C"  void MarkerChildManager_AnimateTargets_m2917730709 (MarkerChildManager_t1830688875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
