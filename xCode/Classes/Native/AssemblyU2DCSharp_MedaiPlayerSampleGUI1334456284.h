﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t3572035536;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MedaiPlayerSampleGUI
struct  MedaiPlayerSampleGUI_t1334456284  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MedaiPlayerSampleGUI::loadingGO
	GameObject_t3674682005 * ___loadingGO_2;
	// MediaPlayerCtrl MedaiPlayerSampleGUI::scrMedia
	MediaPlayerCtrl_t3572035536 * ___scrMedia_3;
	// System.Boolean MedaiPlayerSampleGUI::m_bFinish
	bool ___m_bFinish_4;

public:
	inline static int32_t get_offset_of_loadingGO_2() { return static_cast<int32_t>(offsetof(MedaiPlayerSampleGUI_t1334456284, ___loadingGO_2)); }
	inline GameObject_t3674682005 * get_loadingGO_2() const { return ___loadingGO_2; }
	inline GameObject_t3674682005 ** get_address_of_loadingGO_2() { return &___loadingGO_2; }
	inline void set_loadingGO_2(GameObject_t3674682005 * value)
	{
		___loadingGO_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadingGO_2, value);
	}

	inline static int32_t get_offset_of_scrMedia_3() { return static_cast<int32_t>(offsetof(MedaiPlayerSampleGUI_t1334456284, ___scrMedia_3)); }
	inline MediaPlayerCtrl_t3572035536 * get_scrMedia_3() const { return ___scrMedia_3; }
	inline MediaPlayerCtrl_t3572035536 ** get_address_of_scrMedia_3() { return &___scrMedia_3; }
	inline void set_scrMedia_3(MediaPlayerCtrl_t3572035536 * value)
	{
		___scrMedia_3 = value;
		Il2CppCodeGenWriteBarrier(&___scrMedia_3, value);
	}

	inline static int32_t get_offset_of_m_bFinish_4() { return static_cast<int32_t>(offsetof(MedaiPlayerSampleGUI_t1334456284, ___m_bFinish_4)); }
	inline bool get_m_bFinish_4() const { return ___m_bFinish_4; }
	inline bool* get_address_of_m_bFinish_4() { return &___m_bFinish_4; }
	inline void set_m_bFinish_4(bool value)
	{
		___m_bFinish_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
