﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// MediaPlayerCtrl/VideoReady
struct VideoReady_t259270055;
// MediaPlayerCtrl/VideoEnd
struct VideoEnd_t3177907679;
// MediaPlayerCtrl/VideoError
struct VideoError_t247668236;
// MediaPlayerCtrl/VideoFirstFrameReady
struct VideoFirstFrameReady_t2520860170;
// HomeScreenManager
struct HomeScreenManager_t2427744994;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT1488282328.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIA_SCALE4148698416.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MediaPlayerCtrl
struct  MediaPlayerCtrl_t3572035536  : public MonoBehaviour_t667441552
{
public:
	// System.String MediaPlayerCtrl::m_strFileName
	String_t* ___m_strFileName_2;
	// UnityEngine.GameObject[] MediaPlayerCtrl::m_TargetMaterial
	GameObjectU5BU5D_t2662109048* ___m_TargetMaterial_3;
	// UnityEngine.Texture2D MediaPlayerCtrl::m_VideoTexture
	Texture2D_t3884108195 * ___m_VideoTexture_4;
	// UnityEngine.Texture2D MediaPlayerCtrl::m_VideoTextureDummy
	Texture2D_t3884108195 * ___m_VideoTextureDummy_5;
	// MediaPlayerCtrl/MEDIAPLAYER_STATE MediaPlayerCtrl::m_CurrentState
	int32_t ___m_CurrentState_6;
	// System.Int32 MediaPlayerCtrl::m_iCurrentSeekPosition
	int32_t ___m_iCurrentSeekPosition_7;
	// System.Single MediaPlayerCtrl::m_fVolume
	float ___m_fVolume_8;
	// System.Boolean MediaPlayerCtrl::m_bFullScreen
	bool ___m_bFullScreen_9;
	// System.Boolean MediaPlayerCtrl::m_bSupportRockchip
	bool ___m_bSupportRockchip_10;
	// MediaPlayerCtrl/VideoReady MediaPlayerCtrl::OnReady
	VideoReady_t259270055 * ___OnReady_11;
	// MediaPlayerCtrl/VideoEnd MediaPlayerCtrl::OnEnd
	VideoEnd_t3177907679 * ___OnEnd_12;
	// MediaPlayerCtrl/VideoError MediaPlayerCtrl::OnVideoError
	VideoError_t247668236 * ___OnVideoError_13;
	// MediaPlayerCtrl/VideoFirstFrameReady MediaPlayerCtrl::OnVideoFirstFrameReady
	VideoFirstFrameReady_t2520860170 * ___OnVideoFirstFrameReady_14;
	// System.Int32 MediaPlayerCtrl::m_iPauseFrame
	int32_t ___m_iPauseFrame_15;
	// System.Int32 MediaPlayerCtrl::m_iAndroidMgrID
	int32_t ___m_iAndroidMgrID_16;
	// System.Boolean MediaPlayerCtrl::m_bIsFirstFrameReady
	bool ___m_bIsFirstFrameReady_17;
	// System.Boolean MediaPlayerCtrl::m_bFirst
	bool ___m_bFirst_18;
	// MediaPlayerCtrl/MEDIA_SCALE MediaPlayerCtrl::m_ScaleValue
	int32_t ___m_ScaleValue_19;
	// UnityEngine.GameObject[] MediaPlayerCtrl::m_objResize
	GameObjectU5BU5D_t2662109048* ___m_objResize_20;
	// System.Boolean MediaPlayerCtrl::m_bLoop
	bool ___m_bLoop_21;
	// System.Boolean MediaPlayerCtrl::m_bAutoPlay
	bool ___m_bAutoPlay_22;
	// System.Boolean MediaPlayerCtrl::m_bStop
	bool ___m_bStop_23;
	// System.Boolean MediaPlayerCtrl::m_bInit
	bool ___m_bInit_24;
	// System.Boolean MediaPlayerCtrl::m_bCheckFBO
	bool ___m_bCheckFBO_25;
	// System.Boolean MediaPlayerCtrl::isHelpVideo
	bool ___isHelpVideo_26;
	// System.Boolean MediaPlayerCtrl::terminateOnClick
	bool ___terminateOnClick_27;
	// HomeScreenManager MediaPlayerCtrl::hSM
	HomeScreenManager_t2427744994 * ___hSM_28;
	// System.Boolean MediaPlayerCtrl::m_bPause
	bool ___m_bPause_29;
	// System.Int32 MediaPlayerCtrl::m_iID
	int32_t ___m_iID_30;
	// UnityEngine.Texture2D MediaPlayerCtrl::_videoTexture
	Texture2D_t3884108195 * ____videoTexture_31;

public:
	inline static int32_t get_offset_of_m_strFileName_2() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_strFileName_2)); }
	inline String_t* get_m_strFileName_2() const { return ___m_strFileName_2; }
	inline String_t** get_address_of_m_strFileName_2() { return &___m_strFileName_2; }
	inline void set_m_strFileName_2(String_t* value)
	{
		___m_strFileName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_strFileName_2, value);
	}

	inline static int32_t get_offset_of_m_TargetMaterial_3() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_TargetMaterial_3)); }
	inline GameObjectU5BU5D_t2662109048* get_m_TargetMaterial_3() const { return ___m_TargetMaterial_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_m_TargetMaterial_3() { return &___m_TargetMaterial_3; }
	inline void set_m_TargetMaterial_3(GameObjectU5BU5D_t2662109048* value)
	{
		___m_TargetMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_TargetMaterial_3, value);
	}

	inline static int32_t get_offset_of_m_VideoTexture_4() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_VideoTexture_4)); }
	inline Texture2D_t3884108195 * get_m_VideoTexture_4() const { return ___m_VideoTexture_4; }
	inline Texture2D_t3884108195 ** get_address_of_m_VideoTexture_4() { return &___m_VideoTexture_4; }
	inline void set_m_VideoTexture_4(Texture2D_t3884108195 * value)
	{
		___m_VideoTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_VideoTexture_4, value);
	}

	inline static int32_t get_offset_of_m_VideoTextureDummy_5() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_VideoTextureDummy_5)); }
	inline Texture2D_t3884108195 * get_m_VideoTextureDummy_5() const { return ___m_VideoTextureDummy_5; }
	inline Texture2D_t3884108195 ** get_address_of_m_VideoTextureDummy_5() { return &___m_VideoTextureDummy_5; }
	inline void set_m_VideoTextureDummy_5(Texture2D_t3884108195 * value)
	{
		___m_VideoTextureDummy_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_VideoTextureDummy_5, value);
	}

	inline static int32_t get_offset_of_m_CurrentState_6() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_CurrentState_6)); }
	inline int32_t get_m_CurrentState_6() const { return ___m_CurrentState_6; }
	inline int32_t* get_address_of_m_CurrentState_6() { return &___m_CurrentState_6; }
	inline void set_m_CurrentState_6(int32_t value)
	{
		___m_CurrentState_6 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentSeekPosition_7() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_iCurrentSeekPosition_7)); }
	inline int32_t get_m_iCurrentSeekPosition_7() const { return ___m_iCurrentSeekPosition_7; }
	inline int32_t* get_address_of_m_iCurrentSeekPosition_7() { return &___m_iCurrentSeekPosition_7; }
	inline void set_m_iCurrentSeekPosition_7(int32_t value)
	{
		___m_iCurrentSeekPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_fVolume_8() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_fVolume_8)); }
	inline float get_m_fVolume_8() const { return ___m_fVolume_8; }
	inline float* get_address_of_m_fVolume_8() { return &___m_fVolume_8; }
	inline void set_m_fVolume_8(float value)
	{
		___m_fVolume_8 = value;
	}

	inline static int32_t get_offset_of_m_bFullScreen_9() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bFullScreen_9)); }
	inline bool get_m_bFullScreen_9() const { return ___m_bFullScreen_9; }
	inline bool* get_address_of_m_bFullScreen_9() { return &___m_bFullScreen_9; }
	inline void set_m_bFullScreen_9(bool value)
	{
		___m_bFullScreen_9 = value;
	}

	inline static int32_t get_offset_of_m_bSupportRockchip_10() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bSupportRockchip_10)); }
	inline bool get_m_bSupportRockchip_10() const { return ___m_bSupportRockchip_10; }
	inline bool* get_address_of_m_bSupportRockchip_10() { return &___m_bSupportRockchip_10; }
	inline void set_m_bSupportRockchip_10(bool value)
	{
		___m_bSupportRockchip_10 = value;
	}

	inline static int32_t get_offset_of_OnReady_11() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___OnReady_11)); }
	inline VideoReady_t259270055 * get_OnReady_11() const { return ___OnReady_11; }
	inline VideoReady_t259270055 ** get_address_of_OnReady_11() { return &___OnReady_11; }
	inline void set_OnReady_11(VideoReady_t259270055 * value)
	{
		___OnReady_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnReady_11, value);
	}

	inline static int32_t get_offset_of_OnEnd_12() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___OnEnd_12)); }
	inline VideoEnd_t3177907679 * get_OnEnd_12() const { return ___OnEnd_12; }
	inline VideoEnd_t3177907679 ** get_address_of_OnEnd_12() { return &___OnEnd_12; }
	inline void set_OnEnd_12(VideoEnd_t3177907679 * value)
	{
		___OnEnd_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnEnd_12, value);
	}

	inline static int32_t get_offset_of_OnVideoError_13() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___OnVideoError_13)); }
	inline VideoError_t247668236 * get_OnVideoError_13() const { return ___OnVideoError_13; }
	inline VideoError_t247668236 ** get_address_of_OnVideoError_13() { return &___OnVideoError_13; }
	inline void set_OnVideoError_13(VideoError_t247668236 * value)
	{
		___OnVideoError_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoError_13, value);
	}

	inline static int32_t get_offset_of_OnVideoFirstFrameReady_14() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___OnVideoFirstFrameReady_14)); }
	inline VideoFirstFrameReady_t2520860170 * get_OnVideoFirstFrameReady_14() const { return ___OnVideoFirstFrameReady_14; }
	inline VideoFirstFrameReady_t2520860170 ** get_address_of_OnVideoFirstFrameReady_14() { return &___OnVideoFirstFrameReady_14; }
	inline void set_OnVideoFirstFrameReady_14(VideoFirstFrameReady_t2520860170 * value)
	{
		___OnVideoFirstFrameReady_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoFirstFrameReady_14, value);
	}

	inline static int32_t get_offset_of_m_iPauseFrame_15() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_iPauseFrame_15)); }
	inline int32_t get_m_iPauseFrame_15() const { return ___m_iPauseFrame_15; }
	inline int32_t* get_address_of_m_iPauseFrame_15() { return &___m_iPauseFrame_15; }
	inline void set_m_iPauseFrame_15(int32_t value)
	{
		___m_iPauseFrame_15 = value;
	}

	inline static int32_t get_offset_of_m_iAndroidMgrID_16() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_iAndroidMgrID_16)); }
	inline int32_t get_m_iAndroidMgrID_16() const { return ___m_iAndroidMgrID_16; }
	inline int32_t* get_address_of_m_iAndroidMgrID_16() { return &___m_iAndroidMgrID_16; }
	inline void set_m_iAndroidMgrID_16(int32_t value)
	{
		___m_iAndroidMgrID_16 = value;
	}

	inline static int32_t get_offset_of_m_bIsFirstFrameReady_17() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bIsFirstFrameReady_17)); }
	inline bool get_m_bIsFirstFrameReady_17() const { return ___m_bIsFirstFrameReady_17; }
	inline bool* get_address_of_m_bIsFirstFrameReady_17() { return &___m_bIsFirstFrameReady_17; }
	inline void set_m_bIsFirstFrameReady_17(bool value)
	{
		___m_bIsFirstFrameReady_17 = value;
	}

	inline static int32_t get_offset_of_m_bFirst_18() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bFirst_18)); }
	inline bool get_m_bFirst_18() const { return ___m_bFirst_18; }
	inline bool* get_address_of_m_bFirst_18() { return &___m_bFirst_18; }
	inline void set_m_bFirst_18(bool value)
	{
		___m_bFirst_18 = value;
	}

	inline static int32_t get_offset_of_m_ScaleValue_19() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_ScaleValue_19)); }
	inline int32_t get_m_ScaleValue_19() const { return ___m_ScaleValue_19; }
	inline int32_t* get_address_of_m_ScaleValue_19() { return &___m_ScaleValue_19; }
	inline void set_m_ScaleValue_19(int32_t value)
	{
		___m_ScaleValue_19 = value;
	}

	inline static int32_t get_offset_of_m_objResize_20() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_objResize_20)); }
	inline GameObjectU5BU5D_t2662109048* get_m_objResize_20() const { return ___m_objResize_20; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_m_objResize_20() { return &___m_objResize_20; }
	inline void set_m_objResize_20(GameObjectU5BU5D_t2662109048* value)
	{
		___m_objResize_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_objResize_20, value);
	}

	inline static int32_t get_offset_of_m_bLoop_21() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bLoop_21)); }
	inline bool get_m_bLoop_21() const { return ___m_bLoop_21; }
	inline bool* get_address_of_m_bLoop_21() { return &___m_bLoop_21; }
	inline void set_m_bLoop_21(bool value)
	{
		___m_bLoop_21 = value;
	}

	inline static int32_t get_offset_of_m_bAutoPlay_22() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bAutoPlay_22)); }
	inline bool get_m_bAutoPlay_22() const { return ___m_bAutoPlay_22; }
	inline bool* get_address_of_m_bAutoPlay_22() { return &___m_bAutoPlay_22; }
	inline void set_m_bAutoPlay_22(bool value)
	{
		___m_bAutoPlay_22 = value;
	}

	inline static int32_t get_offset_of_m_bStop_23() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bStop_23)); }
	inline bool get_m_bStop_23() const { return ___m_bStop_23; }
	inline bool* get_address_of_m_bStop_23() { return &___m_bStop_23; }
	inline void set_m_bStop_23(bool value)
	{
		___m_bStop_23 = value;
	}

	inline static int32_t get_offset_of_m_bInit_24() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bInit_24)); }
	inline bool get_m_bInit_24() const { return ___m_bInit_24; }
	inline bool* get_address_of_m_bInit_24() { return &___m_bInit_24; }
	inline void set_m_bInit_24(bool value)
	{
		___m_bInit_24 = value;
	}

	inline static int32_t get_offset_of_m_bCheckFBO_25() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bCheckFBO_25)); }
	inline bool get_m_bCheckFBO_25() const { return ___m_bCheckFBO_25; }
	inline bool* get_address_of_m_bCheckFBO_25() { return &___m_bCheckFBO_25; }
	inline void set_m_bCheckFBO_25(bool value)
	{
		___m_bCheckFBO_25 = value;
	}

	inline static int32_t get_offset_of_isHelpVideo_26() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___isHelpVideo_26)); }
	inline bool get_isHelpVideo_26() const { return ___isHelpVideo_26; }
	inline bool* get_address_of_isHelpVideo_26() { return &___isHelpVideo_26; }
	inline void set_isHelpVideo_26(bool value)
	{
		___isHelpVideo_26 = value;
	}

	inline static int32_t get_offset_of_terminateOnClick_27() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___terminateOnClick_27)); }
	inline bool get_terminateOnClick_27() const { return ___terminateOnClick_27; }
	inline bool* get_address_of_terminateOnClick_27() { return &___terminateOnClick_27; }
	inline void set_terminateOnClick_27(bool value)
	{
		___terminateOnClick_27 = value;
	}

	inline static int32_t get_offset_of_hSM_28() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___hSM_28)); }
	inline HomeScreenManager_t2427744994 * get_hSM_28() const { return ___hSM_28; }
	inline HomeScreenManager_t2427744994 ** get_address_of_hSM_28() { return &___hSM_28; }
	inline void set_hSM_28(HomeScreenManager_t2427744994 * value)
	{
		___hSM_28 = value;
		Il2CppCodeGenWriteBarrier(&___hSM_28, value);
	}

	inline static int32_t get_offset_of_m_bPause_29() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_bPause_29)); }
	inline bool get_m_bPause_29() const { return ___m_bPause_29; }
	inline bool* get_address_of_m_bPause_29() { return &___m_bPause_29; }
	inline void set_m_bPause_29(bool value)
	{
		___m_bPause_29 = value;
	}

	inline static int32_t get_offset_of_m_iID_30() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ___m_iID_30)); }
	inline int32_t get_m_iID_30() const { return ___m_iID_30; }
	inline int32_t* get_address_of_m_iID_30() { return &___m_iID_30; }
	inline void set_m_iID_30(int32_t value)
	{
		___m_iID_30 = value;
	}

	inline static int32_t get_offset_of__videoTexture_31() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t3572035536, ____videoTexture_31)); }
	inline Texture2D_t3884108195 * get__videoTexture_31() const { return ____videoTexture_31; }
	inline Texture2D_t3884108195 ** get_address_of__videoTexture_31() { return &____videoTexture_31; }
	inline void set__videoTexture_31(Texture2D_t3884108195 * value)
	{
		____videoTexture_31 = value;
		Il2CppCodeGenWriteBarrier(&____videoTexture_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
