﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9
struct U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9::.ctor()
extern "C"  void U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9__ctor_m3022520567 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4032263109 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m531341657 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9::MoveNext()
extern "C"  bool U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_MoveNext_m1342920581 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9::Dispose()
extern "C"  void U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_Dispose_m1142655860 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator9::Reset()
extern "C"  void U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_Reset_m668953508 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
