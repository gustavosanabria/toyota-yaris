﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8
struct U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8::.ctor()
extern "C"  void U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8__ctor_m3568023237 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2270719789 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m2795531457 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8::MoveNext()
extern "C"  bool U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_MoveNext_m4216867599 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8::Dispose()
extern "C"  void U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_Dispose_m1384711618 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator8::Reset()
extern "C"  void U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_Reset_m1214456178 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
