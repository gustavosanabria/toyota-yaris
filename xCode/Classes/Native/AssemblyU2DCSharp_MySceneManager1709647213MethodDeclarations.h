﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MySceneManager
struct MySceneManager_t1709647213;

#include "codegen/il2cpp-codegen.h"

// System.Void MySceneManager::.ctor()
extern "C"  void MySceneManager__ctor_m1420653006 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::GoToHomeScreenAndOpenTut()
extern "C"  void MySceneManager_GoToHomeScreenAndOpenTut_m1886156150 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::GoToHomeScreenAndOpenTFTTut()
extern "C"  void MySceneManager_GoToHomeScreenAndOpenTFTTut_m4079577296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::GoToHomeScreen(System.Boolean)
extern "C"  void MySceneManager_GoToHomeScreen_m1290112635 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::GoToHomeScreen()
extern "C"  void MySceneManager_GoToHomeScreen_m4091883524 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::ReturnToGlossary()
extern "C"  void MySceneManager_ReturnToGlossary_m3758830335 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte MySceneManager::GetHomeScene()
extern "C"  uint8_t MySceneManager_GetHomeScene_m1241705785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::GoToARScreen()
extern "C"  void MySceneManager_GoToARScreen_m2595814198 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte MySceneManager::GetARScene()
extern "C"  uint8_t MySceneManager_GetARScene_m3143983239 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::JumpFromGlossaryTo360()
extern "C"  void MySceneManager_JumpFromGlossaryTo360_m3060897320 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::JumpFromGlossaryToTutCategory()
extern "C"  void MySceneManager_JumpFromGlossaryToTutCategory_m53597452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MySceneManager::GoTo360ViewScreen()
extern "C"  void MySceneManager_GoTo360ViewScreen_m1443264647 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte MySceneManager::GetVRScene()
extern "C"  uint8_t MySceneManager_GetVRScene_m3842145180 (MySceneManager_t1709647213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
