﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyTrackeableEventHandler
struct  MyTrackeableEventHandler_t2447116080  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MyTrackeableEventHandler::ARController
	GameObject_t3674682005 * ___ARController_2;
	// Vuforia.TrackableBehaviour MyTrackeableEventHandler::_trackableBehaviour
	TrackableBehaviour_t4179556250 * ____trackableBehaviour_3;

public:
	inline static int32_t get_offset_of_ARController_2() { return static_cast<int32_t>(offsetof(MyTrackeableEventHandler_t2447116080, ___ARController_2)); }
	inline GameObject_t3674682005 * get_ARController_2() const { return ___ARController_2; }
	inline GameObject_t3674682005 ** get_address_of_ARController_2() { return &___ARController_2; }
	inline void set_ARController_2(GameObject_t3674682005 * value)
	{
		___ARController_2 = value;
		Il2CppCodeGenWriteBarrier(&___ARController_2, value);
	}

	inline static int32_t get_offset_of__trackableBehaviour_3() { return static_cast<int32_t>(offsetof(MyTrackeableEventHandler_t2447116080, ____trackableBehaviour_3)); }
	inline TrackableBehaviour_t4179556250 * get__trackableBehaviour_3() const { return ____trackableBehaviour_3; }
	inline TrackableBehaviour_t4179556250 ** get_address_of__trackableBehaviour_3() { return &____trackableBehaviour_3; }
	inline void set__trackableBehaviour_3(TrackableBehaviour_t4179556250 * value)
	{
		____trackableBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier(&____trackableBehaviour_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
