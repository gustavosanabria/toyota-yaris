﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyTrackeableEventHandler
struct MyTrackeableEventHandler_t2447116080;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"

// System.Void MyTrackeableEventHandler::.ctor()
extern "C"  void MyTrackeableEventHandler__ctor_m381853035 (MyTrackeableEventHandler_t2447116080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyTrackeableEventHandler::Start()
extern "C"  void MyTrackeableEventHandler_Start_m3623958123 (MyTrackeableEventHandler_t2447116080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyTrackeableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void MyTrackeableEventHandler_OnTrackableStateChanged_m2472302354 (MyTrackeableEventHandler_t2447116080 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyTrackeableEventHandler::EnableComponents()
extern "C"  void MyTrackeableEventHandler_EnableComponents_m2998347058 (MyTrackeableEventHandler_t2447116080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyTrackeableEventHandler::DisableComponents()
extern "C"  void MyTrackeableEventHandler_DisableComponents_m3840042791 (MyTrackeableEventHandler_t2447116080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyTrackeableEventHandler::OnTrackingFound()
extern "C"  void MyTrackeableEventHandler_OnTrackingFound_m1260754165 (MyTrackeableEventHandler_t2447116080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyTrackeableEventHandler::OnTrackingLost()
extern "C"  void MyTrackeableEventHandler_OnTrackingLost_m350936051 (MyTrackeableEventHandler_t2447116080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
