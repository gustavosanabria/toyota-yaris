﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyVuforiaScript
struct MyVuforiaScript_t3965407553;

#include "codegen/il2cpp-codegen.h"

// System.Void MyVuforiaScript::.ctor()
extern "C"  void MyVuforiaScript__ctor_m3751628618 (MyVuforiaScript_t3965407553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyVuforiaScript::Start()
extern "C"  void MyVuforiaScript_Start_m2698766410 (MyVuforiaScript_t3965407553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyVuforiaScript::Update()
extern "C"  void MyVuforiaScript_Update_m2063232259 (MyVuforiaScript_t3965407553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
