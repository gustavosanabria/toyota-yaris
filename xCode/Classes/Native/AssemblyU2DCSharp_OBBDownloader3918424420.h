﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBBDownloader
struct  OBBDownloader_t3918424420  : public MonoBehaviour_t667441552
{
public:
	// UILabel OBBDownloader::loadingText
	UILabel_t291504320 * ___loadingText_2;
	// System.String OBBDownloader::originalText
	String_t* ___originalText_3;

public:
	inline static int32_t get_offset_of_loadingText_2() { return static_cast<int32_t>(offsetof(OBBDownloader_t3918424420, ___loadingText_2)); }
	inline UILabel_t291504320 * get_loadingText_2() const { return ___loadingText_2; }
	inline UILabel_t291504320 ** get_address_of_loadingText_2() { return &___loadingText_2; }
	inline void set_loadingText_2(UILabel_t291504320 * value)
	{
		___loadingText_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadingText_2, value);
	}

	inline static int32_t get_offset_of_originalText_3() { return static_cast<int32_t>(offsetof(OBBDownloader_t3918424420, ___originalText_3)); }
	inline String_t* get_originalText_3() const { return ___originalText_3; }
	inline String_t** get_address_of_originalText_3() { return &___originalText_3; }
	inline void set_originalText_3(String_t* value)
	{
		___originalText_3 = value;
		Il2CppCodeGenWriteBarrier(&___originalText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
