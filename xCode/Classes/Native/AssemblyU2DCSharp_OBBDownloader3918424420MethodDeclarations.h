﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBBDownloader
struct OBBDownloader_t3918424420;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void OBBDownloader::.ctor()
extern "C"  void OBBDownloader__ctor_m1761757319 (OBBDownloader_t3918424420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBBDownloader::Awake()
extern "C"  void OBBDownloader_Awake_m1999362538 (OBBDownloader_t3918424420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OBBDownloader::Start()
extern "C"  Il2CppObject * OBBDownloader_Start_m1856895439 (OBBDownloader_t3918424420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OBBDownloader::LoadMainMenu()
extern "C"  Il2CppObject * OBBDownloader_LoadMainMenu_m917969107 (OBBDownloader_t3918424420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
