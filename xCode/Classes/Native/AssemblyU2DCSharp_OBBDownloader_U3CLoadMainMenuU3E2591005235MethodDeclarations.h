﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBBDownloader/<LoadMainMenu>c__Iterator5
struct U3CLoadMainMenuU3Ec__Iterator5_t2591005235;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OBBDownloader/<LoadMainMenu>c__Iterator5::.ctor()
extern "C"  void U3CLoadMainMenuU3Ec__Iterator5__ctor_m159485384 (U3CLoadMainMenuU3Ec__Iterator5_t2591005235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OBBDownloader/<LoadMainMenu>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadMainMenuU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2119619604 (U3CLoadMainMenuU3Ec__Iterator5_t2591005235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OBBDownloader/<LoadMainMenu>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadMainMenuU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2025553832 (U3CLoadMainMenuU3Ec__Iterator5_t2591005235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OBBDownloader/<LoadMainMenu>c__Iterator5::MoveNext()
extern "C"  bool U3CLoadMainMenuU3Ec__Iterator5_MoveNext_m3955142420 (U3CLoadMainMenuU3Ec__Iterator5_t2591005235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBBDownloader/<LoadMainMenu>c__Iterator5::Dispose()
extern "C"  void U3CLoadMainMenuU3Ec__Iterator5_Dispose_m2839881733 (U3CLoadMainMenuU3Ec__Iterator5_t2591005235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBBDownloader/<LoadMainMenu>c__Iterator5::Reset()
extern "C"  void U3CLoadMainMenuU3Ec__Iterator5_Reset_m2100885621 (U3CLoadMainMenuU3Ec__Iterator5_t2591005235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
