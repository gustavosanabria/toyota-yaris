﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OBBDownloader/<Start>c__Iterator4
struct U3CStartU3Ec__Iterator4_t3160979344;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OBBDownloader/<Start>c__Iterator4::.ctor()
extern "C"  void U3CStartU3Ec__Iterator4__ctor_m1153542107 (U3CStartU3Ec__Iterator4_t3160979344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OBBDownloader/<Start>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m932630423 (U3CStartU3Ec__Iterator4_t3160979344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OBBDownloader/<Start>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2549145899 (U3CStartU3Ec__Iterator4_t3160979344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OBBDownloader/<Start>c__Iterator4::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator4_MoveNext_m512074041 (U3CStartU3Ec__Iterator4_t3160979344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBBDownloader/<Start>c__Iterator4::Dispose()
extern "C"  void U3CStartU3Ec__Iterator4_Dispose_m350685528 (U3CStartU3Ec__Iterator4_t3160979344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OBBDownloader/<Start>c__Iterator4::Reset()
extern "C"  void U3CStartU3Ec__Iterator4_Reset_m3094942344 (U3CStartU3Ec__Iterator4_t3160979344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
