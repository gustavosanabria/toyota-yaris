﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OldGyroscopeController
struct  OldGyroscopeController_t3590691066  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Quaternion OldGyroscopeController::baseIdentity
	Quaternion_t1553702882  ___baseIdentity_2;
	// UnityEngine.Quaternion OldGyroscopeController::referanceRotation
	Quaternion_t1553702882  ___referanceRotation_3;
	// UnityEngine.Quaternion OldGyroscopeController::cameraBase
	Quaternion_t1553702882  ___cameraBase_4;
	// UnityEngine.Quaternion OldGyroscopeController::calibration
	Quaternion_t1553702882  ___calibration_5;
	// UnityEngine.Quaternion OldGyroscopeController::baseOrientation
	Quaternion_t1553702882  ___baseOrientation_6;
	// UnityEngine.Quaternion OldGyroscopeController::baseOrientationRotationFix
	Quaternion_t1553702882  ___baseOrientationRotationFix_7;
	// System.Boolean OldGyroscopeController::gyroEnble
	bool ___gyroEnble_8;
	// System.Single OldGyroscopeController::minSwipeDistY
	float ___minSwipeDistY_9;
	// System.Single OldGyroscopeController::minSwipeDistX
	float ___minSwipeDistX_10;
	// UnityEngine.Vector2 OldGyroscopeController::startPos
	Vector2_t4282066565  ___startPos_11;
	// System.Single OldGyroscopeController::swipeDistVertical
	float ___swipeDistVertical_12;
	// System.Single OldGyroscopeController::swipeTime
	float ___swipeTime_13;
	// System.Boolean OldGyroscopeController::canCountUp
	bool ___canCountUp_14;
	// UnityEngine.Quaternion OldGyroscopeController::originalRotation
	Quaternion_t1553702882  ___originalRotation_15;
	// System.Single OldGyroscopeController::sensitivityX
	float ___sensitivityX_16;
	// System.Single OldGyroscopeController::sensitivityY
	float ___sensitivityY_17;
	// System.Single OldGyroscopeController::minimumX
	float ___minimumX_18;
	// System.Single OldGyroscopeController::maximumX
	float ___maximumX_19;
	// System.Single OldGyroscopeController::minimumY
	float ___minimumY_20;
	// System.Single OldGyroscopeController::maximumY
	float ___maximumY_21;
	// System.Single OldGyroscopeController::rotationY
	float ___rotationY_22;
	// System.Single OldGyroscopeController::rotationX
	float ___rotationX_23;
	// System.Single OldGyroscopeController::maxUPRot
	float ___maxUPRot_24;
	// System.Single OldGyroscopeController::maxDownRot
	float ___maxDownRot_25;
	// UnityEngine.Vector3 OldGyroscopeController::headRot
	Vector3_t4282066566  ___headRot_26;
	// UnityEngine.Vector3 OldGyroscopeController::headRot2
	Vector3_t4282066566  ___headRot2_27;
	// UnityEngine.Vector3 OldGyroscopeController::newVector
	Vector3_t4282066566  ___newVector_28;
	// System.Single OldGyroscopeController::xRot
	float ___xRot_29;
	// System.Single OldGyroscopeController::xRot2
	float ___xRot2_30;
	// System.Single OldGyroscopeController::xLimit
	float ___xLimit_31;
	// System.Single OldGyroscopeController::newX
	float ___newX_32;
	// System.Single OldGyroscopeController::upX
	float ___upX_33;
	// System.Single OldGyroscopeController::downX
	float ___downX_34;

public:
	inline static int32_t get_offset_of_baseIdentity_2() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___baseIdentity_2)); }
	inline Quaternion_t1553702882  get_baseIdentity_2() const { return ___baseIdentity_2; }
	inline Quaternion_t1553702882 * get_address_of_baseIdentity_2() { return &___baseIdentity_2; }
	inline void set_baseIdentity_2(Quaternion_t1553702882  value)
	{
		___baseIdentity_2 = value;
	}

	inline static int32_t get_offset_of_referanceRotation_3() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___referanceRotation_3)); }
	inline Quaternion_t1553702882  get_referanceRotation_3() const { return ___referanceRotation_3; }
	inline Quaternion_t1553702882 * get_address_of_referanceRotation_3() { return &___referanceRotation_3; }
	inline void set_referanceRotation_3(Quaternion_t1553702882  value)
	{
		___referanceRotation_3 = value;
	}

	inline static int32_t get_offset_of_cameraBase_4() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___cameraBase_4)); }
	inline Quaternion_t1553702882  get_cameraBase_4() const { return ___cameraBase_4; }
	inline Quaternion_t1553702882 * get_address_of_cameraBase_4() { return &___cameraBase_4; }
	inline void set_cameraBase_4(Quaternion_t1553702882  value)
	{
		___cameraBase_4 = value;
	}

	inline static int32_t get_offset_of_calibration_5() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___calibration_5)); }
	inline Quaternion_t1553702882  get_calibration_5() const { return ___calibration_5; }
	inline Quaternion_t1553702882 * get_address_of_calibration_5() { return &___calibration_5; }
	inline void set_calibration_5(Quaternion_t1553702882  value)
	{
		___calibration_5 = value;
	}

	inline static int32_t get_offset_of_baseOrientation_6() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___baseOrientation_6)); }
	inline Quaternion_t1553702882  get_baseOrientation_6() const { return ___baseOrientation_6; }
	inline Quaternion_t1553702882 * get_address_of_baseOrientation_6() { return &___baseOrientation_6; }
	inline void set_baseOrientation_6(Quaternion_t1553702882  value)
	{
		___baseOrientation_6 = value;
	}

	inline static int32_t get_offset_of_baseOrientationRotationFix_7() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___baseOrientationRotationFix_7)); }
	inline Quaternion_t1553702882  get_baseOrientationRotationFix_7() const { return ___baseOrientationRotationFix_7; }
	inline Quaternion_t1553702882 * get_address_of_baseOrientationRotationFix_7() { return &___baseOrientationRotationFix_7; }
	inline void set_baseOrientationRotationFix_7(Quaternion_t1553702882  value)
	{
		___baseOrientationRotationFix_7 = value;
	}

	inline static int32_t get_offset_of_gyroEnble_8() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___gyroEnble_8)); }
	inline bool get_gyroEnble_8() const { return ___gyroEnble_8; }
	inline bool* get_address_of_gyroEnble_8() { return &___gyroEnble_8; }
	inline void set_gyroEnble_8(bool value)
	{
		___gyroEnble_8 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistY_9() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___minSwipeDistY_9)); }
	inline float get_minSwipeDistY_9() const { return ___minSwipeDistY_9; }
	inline float* get_address_of_minSwipeDistY_9() { return &___minSwipeDistY_9; }
	inline void set_minSwipeDistY_9(float value)
	{
		___minSwipeDistY_9 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistX_10() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___minSwipeDistX_10)); }
	inline float get_minSwipeDistX_10() const { return ___minSwipeDistX_10; }
	inline float* get_address_of_minSwipeDistX_10() { return &___minSwipeDistX_10; }
	inline void set_minSwipeDistX_10(float value)
	{
		___minSwipeDistX_10 = value;
	}

	inline static int32_t get_offset_of_startPos_11() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___startPos_11)); }
	inline Vector2_t4282066565  get_startPos_11() const { return ___startPos_11; }
	inline Vector2_t4282066565 * get_address_of_startPos_11() { return &___startPos_11; }
	inline void set_startPos_11(Vector2_t4282066565  value)
	{
		___startPos_11 = value;
	}

	inline static int32_t get_offset_of_swipeDistVertical_12() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___swipeDistVertical_12)); }
	inline float get_swipeDistVertical_12() const { return ___swipeDistVertical_12; }
	inline float* get_address_of_swipeDistVertical_12() { return &___swipeDistVertical_12; }
	inline void set_swipeDistVertical_12(float value)
	{
		___swipeDistVertical_12 = value;
	}

	inline static int32_t get_offset_of_swipeTime_13() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___swipeTime_13)); }
	inline float get_swipeTime_13() const { return ___swipeTime_13; }
	inline float* get_address_of_swipeTime_13() { return &___swipeTime_13; }
	inline void set_swipeTime_13(float value)
	{
		___swipeTime_13 = value;
	}

	inline static int32_t get_offset_of_canCountUp_14() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___canCountUp_14)); }
	inline bool get_canCountUp_14() const { return ___canCountUp_14; }
	inline bool* get_address_of_canCountUp_14() { return &___canCountUp_14; }
	inline void set_canCountUp_14(bool value)
	{
		___canCountUp_14 = value;
	}

	inline static int32_t get_offset_of_originalRotation_15() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___originalRotation_15)); }
	inline Quaternion_t1553702882  get_originalRotation_15() const { return ___originalRotation_15; }
	inline Quaternion_t1553702882 * get_address_of_originalRotation_15() { return &___originalRotation_15; }
	inline void set_originalRotation_15(Quaternion_t1553702882  value)
	{
		___originalRotation_15 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_16() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___sensitivityX_16)); }
	inline float get_sensitivityX_16() const { return ___sensitivityX_16; }
	inline float* get_address_of_sensitivityX_16() { return &___sensitivityX_16; }
	inline void set_sensitivityX_16(float value)
	{
		___sensitivityX_16 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_17() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___sensitivityY_17)); }
	inline float get_sensitivityY_17() const { return ___sensitivityY_17; }
	inline float* get_address_of_sensitivityY_17() { return &___sensitivityY_17; }
	inline void set_sensitivityY_17(float value)
	{
		___sensitivityY_17 = value;
	}

	inline static int32_t get_offset_of_minimumX_18() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___minimumX_18)); }
	inline float get_minimumX_18() const { return ___minimumX_18; }
	inline float* get_address_of_minimumX_18() { return &___minimumX_18; }
	inline void set_minimumX_18(float value)
	{
		___minimumX_18 = value;
	}

	inline static int32_t get_offset_of_maximumX_19() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___maximumX_19)); }
	inline float get_maximumX_19() const { return ___maximumX_19; }
	inline float* get_address_of_maximumX_19() { return &___maximumX_19; }
	inline void set_maximumX_19(float value)
	{
		___maximumX_19 = value;
	}

	inline static int32_t get_offset_of_minimumY_20() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___minimumY_20)); }
	inline float get_minimumY_20() const { return ___minimumY_20; }
	inline float* get_address_of_minimumY_20() { return &___minimumY_20; }
	inline void set_minimumY_20(float value)
	{
		___minimumY_20 = value;
	}

	inline static int32_t get_offset_of_maximumY_21() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___maximumY_21)); }
	inline float get_maximumY_21() const { return ___maximumY_21; }
	inline float* get_address_of_maximumY_21() { return &___maximumY_21; }
	inline void set_maximumY_21(float value)
	{
		___maximumY_21 = value;
	}

	inline static int32_t get_offset_of_rotationY_22() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___rotationY_22)); }
	inline float get_rotationY_22() const { return ___rotationY_22; }
	inline float* get_address_of_rotationY_22() { return &___rotationY_22; }
	inline void set_rotationY_22(float value)
	{
		___rotationY_22 = value;
	}

	inline static int32_t get_offset_of_rotationX_23() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___rotationX_23)); }
	inline float get_rotationX_23() const { return ___rotationX_23; }
	inline float* get_address_of_rotationX_23() { return &___rotationX_23; }
	inline void set_rotationX_23(float value)
	{
		___rotationX_23 = value;
	}

	inline static int32_t get_offset_of_maxUPRot_24() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___maxUPRot_24)); }
	inline float get_maxUPRot_24() const { return ___maxUPRot_24; }
	inline float* get_address_of_maxUPRot_24() { return &___maxUPRot_24; }
	inline void set_maxUPRot_24(float value)
	{
		___maxUPRot_24 = value;
	}

	inline static int32_t get_offset_of_maxDownRot_25() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___maxDownRot_25)); }
	inline float get_maxDownRot_25() const { return ___maxDownRot_25; }
	inline float* get_address_of_maxDownRot_25() { return &___maxDownRot_25; }
	inline void set_maxDownRot_25(float value)
	{
		___maxDownRot_25 = value;
	}

	inline static int32_t get_offset_of_headRot_26() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___headRot_26)); }
	inline Vector3_t4282066566  get_headRot_26() const { return ___headRot_26; }
	inline Vector3_t4282066566 * get_address_of_headRot_26() { return &___headRot_26; }
	inline void set_headRot_26(Vector3_t4282066566  value)
	{
		___headRot_26 = value;
	}

	inline static int32_t get_offset_of_headRot2_27() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___headRot2_27)); }
	inline Vector3_t4282066566  get_headRot2_27() const { return ___headRot2_27; }
	inline Vector3_t4282066566 * get_address_of_headRot2_27() { return &___headRot2_27; }
	inline void set_headRot2_27(Vector3_t4282066566  value)
	{
		___headRot2_27 = value;
	}

	inline static int32_t get_offset_of_newVector_28() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___newVector_28)); }
	inline Vector3_t4282066566  get_newVector_28() const { return ___newVector_28; }
	inline Vector3_t4282066566 * get_address_of_newVector_28() { return &___newVector_28; }
	inline void set_newVector_28(Vector3_t4282066566  value)
	{
		___newVector_28 = value;
	}

	inline static int32_t get_offset_of_xRot_29() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___xRot_29)); }
	inline float get_xRot_29() const { return ___xRot_29; }
	inline float* get_address_of_xRot_29() { return &___xRot_29; }
	inline void set_xRot_29(float value)
	{
		___xRot_29 = value;
	}

	inline static int32_t get_offset_of_xRot2_30() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___xRot2_30)); }
	inline float get_xRot2_30() const { return ___xRot2_30; }
	inline float* get_address_of_xRot2_30() { return &___xRot2_30; }
	inline void set_xRot2_30(float value)
	{
		___xRot2_30 = value;
	}

	inline static int32_t get_offset_of_xLimit_31() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___xLimit_31)); }
	inline float get_xLimit_31() const { return ___xLimit_31; }
	inline float* get_address_of_xLimit_31() { return &___xLimit_31; }
	inline void set_xLimit_31(float value)
	{
		___xLimit_31 = value;
	}

	inline static int32_t get_offset_of_newX_32() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___newX_32)); }
	inline float get_newX_32() const { return ___newX_32; }
	inline float* get_address_of_newX_32() { return &___newX_32; }
	inline void set_newX_32(float value)
	{
		___newX_32 = value;
	}

	inline static int32_t get_offset_of_upX_33() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___upX_33)); }
	inline float get_upX_33() const { return ___upX_33; }
	inline float* get_address_of_upX_33() { return &___upX_33; }
	inline void set_upX_33(float value)
	{
		___upX_33 = value;
	}

	inline static int32_t get_offset_of_downX_34() { return static_cast<int32_t>(offsetof(OldGyroscopeController_t3590691066, ___downX_34)); }
	inline float get_downX_34() const { return ___downX_34; }
	inline float* get_address_of_downX_34() { return &___downX_34; }
	inline void set_downX_34(float value)
	{
		___downX_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
