﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OldGyroscopeController
struct OldGyroscopeController_t3590691066;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"

// System.Void OldGyroscopeController::.ctor()
extern "C"  void OldGyroscopeController__ctor_m3917092065 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::Start()
extern "C"  void OldGyroscopeController_Start_m2864229857 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::Update()
extern "C"  void OldGyroscopeController_Update_m2897631820 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion OldGyroscopeController::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  OldGyroscopeController_ClampRotationAroundXAxis_m3397052457 (OldGyroscopeController_t3590691066 * __this, Quaternion_t1553702882  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OldGyroscopeController::GetLimit(System.Single)
extern "C"  float OldGyroscopeController_GetLimit_m3882689623 (OldGyroscopeController_t3590691066 * __this, float ___currentX0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::SmoothZoom(UnityEngine.Touch,UnityEngine.Touch)
extern "C"  void OldGyroscopeController_SmoothZoom_m1941689770 (OldGyroscopeController_t3590691066 * __this, Touch_t4210255029  ___touchZero0, Touch_t4210255029  ___touchOne1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::AttachGyro()
extern "C"  void OldGyroscopeController_AttachGyro_m1253247063 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::DetachGyro()
extern "C"  void OldGyroscopeController_DetachGyro_m2824961829 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::CalibrateGyro()
extern "C"  void OldGyroscopeController_CalibrateGyro_m2686050453 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OldGyroscopeController::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float OldGyroscopeController_ClampAngle_m3725047726 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::UpdateCalibration(System.Boolean)
extern "C"  void OldGyroscopeController_UpdateCalibration_m1647081367 (OldGyroscopeController_t3590691066 * __this, bool ___onlyHorizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::UpdateCameraBaseRotation(System.Boolean)
extern "C"  void OldGyroscopeController_UpdateCameraBaseRotation_m4103406423 (OldGyroscopeController_t3590691066 * __this, bool ___onlyHorizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion OldGyroscopeController::ConvertRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  OldGyroscopeController_ConvertRotation_m1636989523 (OldGyroscopeController_t3590691066 * __this, Quaternion_t1553702882  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion OldGyroscopeController::GetRotFix()
extern "C"  Quaternion_t1553702882  OldGyroscopeController_GetRotFix_m2844545963 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::ResetBaseOrientation()
extern "C"  void OldGyroscopeController_ResetBaseOrientation_m315622707 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::RecalculateReferenceRotation()
extern "C"  void OldGyroscopeController_RecalculateReferenceRotation_m3856176729 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OldGyroscopeController::countSwipeTimeUp()
extern "C"  void OldGyroscopeController_countSwipeTimeUp_m285863414 (OldGyroscopeController_t3590691066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
