﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RadialUndistortionEffect
struct RadialUndistortionEffect_t3929131462;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void RadialUndistortionEffect::.ctor()
extern "C"  void RadialUndistortionEffect__ctor_m416244629 (RadialUndistortionEffect_t3929131462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialUndistortionEffect::Awake()
extern "C"  void RadialUndistortionEffect_Awake_m653849848 (RadialUndistortionEffect_t3929131462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialUndistortionEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void RadialUndistortionEffect_OnRenderImage_m3164673961 (RadialUndistortionEffect_t3929131462 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
