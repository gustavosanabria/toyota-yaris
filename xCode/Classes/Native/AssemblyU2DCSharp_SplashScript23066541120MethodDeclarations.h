﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplashScript2
struct SplashScript2_t3066541120;

#include "codegen/il2cpp-codegen.h"

// System.Void SplashScript2::.ctor()
extern "C"  void SplashScript2__ctor_m2511959339 (SplashScript2_t3066541120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScript2::Start()
extern "C"  void SplashScript2_Start_m1459097131 (SplashScript2_t3066541120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScript2::ChangeScene()
extern "C"  void SplashScript2_ChangeScene_m920512517 (SplashScript2_t3066541120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
