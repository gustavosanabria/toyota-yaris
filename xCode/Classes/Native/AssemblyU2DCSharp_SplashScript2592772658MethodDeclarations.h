﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplashScript
struct SplashScript_t2592772658;

#include "codegen/il2cpp-codegen.h"

// System.Void SplashScript::.ctor()
extern "C"  void SplashScript__ctor_m3243882665 (SplashScript_t2592772658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScript::Awake()
extern "C"  void SplashScript_Awake_m3481487884 (SplashScript_t2592772658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScript::Update()
extern "C"  void SplashScript_Update_m3502976900 (SplashScript_t2592772658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScript::LoadHomeScreen()
extern "C"  void SplashScript_LoadHomeScreen_m3103177324 (SplashScript_t2592772658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScript::ChangeScene()
extern "C"  void SplashScript_ChangeScene_m1968248067 (SplashScript_t2592772658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
