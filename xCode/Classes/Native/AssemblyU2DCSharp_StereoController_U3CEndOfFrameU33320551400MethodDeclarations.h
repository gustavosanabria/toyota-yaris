﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StereoController/<EndOfFrame>c__Iterator7
struct U3CEndOfFrameU3Ec__Iterator7_t3320551400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StereoController/<EndOfFrame>c__Iterator7::.ctor()
extern "C"  void U3CEndOfFrameU3Ec__Iterator7__ctor_m707412611 (U3CEndOfFrameU3Ec__Iterator7_t3320551400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StereoController/<EndOfFrame>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEndOfFrameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4024939247 (U3CEndOfFrameU3Ec__Iterator7_t3320551400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StereoController/<EndOfFrame>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEndOfFrameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m795047043 (U3CEndOfFrameU3Ec__Iterator7_t3320551400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StereoController/<EndOfFrame>c__Iterator7::MoveNext()
extern "C"  bool U3CEndOfFrameU3Ec__Iterator7_MoveNext_m2763954577 (U3CEndOfFrameU3Ec__Iterator7_t3320551400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoController/<EndOfFrame>c__Iterator7::Dispose()
extern "C"  void U3CEndOfFrameU3Ec__Iterator7_Dispose_m1116969472 (U3CEndOfFrameU3Ec__Iterator7_t3320551400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoController/<EndOfFrame>c__Iterator7::Reset()
extern "C"  void U3CEndOfFrameU3Ec__Iterator7_Reset_m2648812848 (U3CEndOfFrameU3Ec__Iterator7_t3320551400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
