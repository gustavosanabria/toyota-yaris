﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StereoLensFlare
struct StereoLensFlare_t786564152;

#include "codegen/il2cpp-codegen.h"

// System.Void StereoLensFlare::.ctor()
extern "C"  void StereoLensFlare__ctor_m3613065779 (StereoLensFlare_t786564152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoLensFlare::Awake()
extern "C"  void StereoLensFlare_Awake_m3850670998 (StereoLensFlare_t786564152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
