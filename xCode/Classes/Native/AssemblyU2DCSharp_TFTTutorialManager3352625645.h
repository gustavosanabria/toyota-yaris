﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UICamera
struct UICamera_t189364953;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// TweenScale
struct TweenScale_t2936666559;
// UISprite
struct UISprite_t661437049;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TFTTutorialManager
struct  TFTTutorialManager_t3352625645  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject TFTTutorialManager::tftIconsContainer
	GameObject_t3674682005 * ___tftIconsContainer_2;
	// UICamera TFTTutorialManager::mainMenuCam
	UICamera_t189364953 * ___mainMenuCam_3;
	// UnityEngine.GameObject TFTTutorialManager::tftTutContainer
	GameObject_t3674682005 * ___tftTutContainer_4;
	// Tutorial TFTTutorialManager::currentTut
	Tutorial_t257920894  ___currentTut_5;
	// UITexture TFTTutorialManager::tutStepUITex
	UITexture_t3903132647 * ___tutStepUITex_6;
	// UnityEngine.GameObject TFTTutorialManager::tutHandlerGO
	GameObject_t3674682005 * ___tutHandlerGO_7;
	// System.SByte TFTTutorialManager::currentTutStepIndex
	int8_t ___currentTutStepIndex_8;
	// System.Byte TFTTutorialManager::currentTutStepCount
	uint8_t ___currentTutStepCount_9;
	// UnityEngine.AudioSource TFTTutorialManager::audioSrc
	AudioSource_t1740077639 * ___audioSrc_10;
	// UnityEngine.AudioClip TFTTutorialManager::introClip
	AudioClip_t794140988 * ___introClip_11;
	// UnityEngine.GameObject TFTTutorialManager::nextStepArrow
	GameObject_t3674682005 * ___nextStepArrow_12;
	// UnityEngine.GameObject TFTTutorialManager::previousStepArrow
	GameObject_t3674682005 * ___previousStepArrow_13;
	// UnityEngine.GameObject TFTTutorialManager::returnBtn
	GameObject_t3674682005 * ___returnBtn_14;
	// TweenScale TFTTutorialManager::twS
	TweenScale_t2936666559 * ___twS_15;
	// System.Boolean TFTTutorialManager::toggleAudio
	bool ___toggleAudio_16;
	// UISprite TFTTutorialManager::audioBtnSpr
	UISprite_t661437049 * ___audioBtnSpr_17;

public:
	inline static int32_t get_offset_of_tftIconsContainer_2() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___tftIconsContainer_2)); }
	inline GameObject_t3674682005 * get_tftIconsContainer_2() const { return ___tftIconsContainer_2; }
	inline GameObject_t3674682005 ** get_address_of_tftIconsContainer_2() { return &___tftIconsContainer_2; }
	inline void set_tftIconsContainer_2(GameObject_t3674682005 * value)
	{
		___tftIconsContainer_2 = value;
		Il2CppCodeGenWriteBarrier(&___tftIconsContainer_2, value);
	}

	inline static int32_t get_offset_of_mainMenuCam_3() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___mainMenuCam_3)); }
	inline UICamera_t189364953 * get_mainMenuCam_3() const { return ___mainMenuCam_3; }
	inline UICamera_t189364953 ** get_address_of_mainMenuCam_3() { return &___mainMenuCam_3; }
	inline void set_mainMenuCam_3(UICamera_t189364953 * value)
	{
		___mainMenuCam_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainMenuCam_3, value);
	}

	inline static int32_t get_offset_of_tftTutContainer_4() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___tftTutContainer_4)); }
	inline GameObject_t3674682005 * get_tftTutContainer_4() const { return ___tftTutContainer_4; }
	inline GameObject_t3674682005 ** get_address_of_tftTutContainer_4() { return &___tftTutContainer_4; }
	inline void set_tftTutContainer_4(GameObject_t3674682005 * value)
	{
		___tftTutContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___tftTutContainer_4, value);
	}

	inline static int32_t get_offset_of_currentTut_5() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___currentTut_5)); }
	inline Tutorial_t257920894  get_currentTut_5() const { return ___currentTut_5; }
	inline Tutorial_t257920894 * get_address_of_currentTut_5() { return &___currentTut_5; }
	inline void set_currentTut_5(Tutorial_t257920894  value)
	{
		___currentTut_5 = value;
	}

	inline static int32_t get_offset_of_tutStepUITex_6() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___tutStepUITex_6)); }
	inline UITexture_t3903132647 * get_tutStepUITex_6() const { return ___tutStepUITex_6; }
	inline UITexture_t3903132647 ** get_address_of_tutStepUITex_6() { return &___tutStepUITex_6; }
	inline void set_tutStepUITex_6(UITexture_t3903132647 * value)
	{
		___tutStepUITex_6 = value;
		Il2CppCodeGenWriteBarrier(&___tutStepUITex_6, value);
	}

	inline static int32_t get_offset_of_tutHandlerGO_7() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___tutHandlerGO_7)); }
	inline GameObject_t3674682005 * get_tutHandlerGO_7() const { return ___tutHandlerGO_7; }
	inline GameObject_t3674682005 ** get_address_of_tutHandlerGO_7() { return &___tutHandlerGO_7; }
	inline void set_tutHandlerGO_7(GameObject_t3674682005 * value)
	{
		___tutHandlerGO_7 = value;
		Il2CppCodeGenWriteBarrier(&___tutHandlerGO_7, value);
	}

	inline static int32_t get_offset_of_currentTutStepIndex_8() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___currentTutStepIndex_8)); }
	inline int8_t get_currentTutStepIndex_8() const { return ___currentTutStepIndex_8; }
	inline int8_t* get_address_of_currentTutStepIndex_8() { return &___currentTutStepIndex_8; }
	inline void set_currentTutStepIndex_8(int8_t value)
	{
		___currentTutStepIndex_8 = value;
	}

	inline static int32_t get_offset_of_currentTutStepCount_9() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___currentTutStepCount_9)); }
	inline uint8_t get_currentTutStepCount_9() const { return ___currentTutStepCount_9; }
	inline uint8_t* get_address_of_currentTutStepCount_9() { return &___currentTutStepCount_9; }
	inline void set_currentTutStepCount_9(uint8_t value)
	{
		___currentTutStepCount_9 = value;
	}

	inline static int32_t get_offset_of_audioSrc_10() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___audioSrc_10)); }
	inline AudioSource_t1740077639 * get_audioSrc_10() const { return ___audioSrc_10; }
	inline AudioSource_t1740077639 ** get_address_of_audioSrc_10() { return &___audioSrc_10; }
	inline void set_audioSrc_10(AudioSource_t1740077639 * value)
	{
		___audioSrc_10 = value;
		Il2CppCodeGenWriteBarrier(&___audioSrc_10, value);
	}

	inline static int32_t get_offset_of_introClip_11() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___introClip_11)); }
	inline AudioClip_t794140988 * get_introClip_11() const { return ___introClip_11; }
	inline AudioClip_t794140988 ** get_address_of_introClip_11() { return &___introClip_11; }
	inline void set_introClip_11(AudioClip_t794140988 * value)
	{
		___introClip_11 = value;
		Il2CppCodeGenWriteBarrier(&___introClip_11, value);
	}

	inline static int32_t get_offset_of_nextStepArrow_12() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___nextStepArrow_12)); }
	inline GameObject_t3674682005 * get_nextStepArrow_12() const { return ___nextStepArrow_12; }
	inline GameObject_t3674682005 ** get_address_of_nextStepArrow_12() { return &___nextStepArrow_12; }
	inline void set_nextStepArrow_12(GameObject_t3674682005 * value)
	{
		___nextStepArrow_12 = value;
		Il2CppCodeGenWriteBarrier(&___nextStepArrow_12, value);
	}

	inline static int32_t get_offset_of_previousStepArrow_13() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___previousStepArrow_13)); }
	inline GameObject_t3674682005 * get_previousStepArrow_13() const { return ___previousStepArrow_13; }
	inline GameObject_t3674682005 ** get_address_of_previousStepArrow_13() { return &___previousStepArrow_13; }
	inline void set_previousStepArrow_13(GameObject_t3674682005 * value)
	{
		___previousStepArrow_13 = value;
		Il2CppCodeGenWriteBarrier(&___previousStepArrow_13, value);
	}

	inline static int32_t get_offset_of_returnBtn_14() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___returnBtn_14)); }
	inline GameObject_t3674682005 * get_returnBtn_14() const { return ___returnBtn_14; }
	inline GameObject_t3674682005 ** get_address_of_returnBtn_14() { return &___returnBtn_14; }
	inline void set_returnBtn_14(GameObject_t3674682005 * value)
	{
		___returnBtn_14 = value;
		Il2CppCodeGenWriteBarrier(&___returnBtn_14, value);
	}

	inline static int32_t get_offset_of_twS_15() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___twS_15)); }
	inline TweenScale_t2936666559 * get_twS_15() const { return ___twS_15; }
	inline TweenScale_t2936666559 ** get_address_of_twS_15() { return &___twS_15; }
	inline void set_twS_15(TweenScale_t2936666559 * value)
	{
		___twS_15 = value;
		Il2CppCodeGenWriteBarrier(&___twS_15, value);
	}

	inline static int32_t get_offset_of_toggleAudio_16() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___toggleAudio_16)); }
	inline bool get_toggleAudio_16() const { return ___toggleAudio_16; }
	inline bool* get_address_of_toggleAudio_16() { return &___toggleAudio_16; }
	inline void set_toggleAudio_16(bool value)
	{
		___toggleAudio_16 = value;
	}

	inline static int32_t get_offset_of_audioBtnSpr_17() { return static_cast<int32_t>(offsetof(TFTTutorialManager_t3352625645, ___audioBtnSpr_17)); }
	inline UISprite_t661437049 * get_audioBtnSpr_17() const { return ___audioBtnSpr_17; }
	inline UISprite_t661437049 ** get_address_of_audioBtnSpr_17() { return &___audioBtnSpr_17; }
	inline void set_audioBtnSpr_17(UISprite_t661437049 * value)
	{
		___audioBtnSpr_17 = value;
		Il2CppCodeGenWriteBarrier(&___audioBtnSpr_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
