﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TFTTutorialManager
struct TFTTutorialManager_t3352625645;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"

// System.Void TFTTutorialManager::.ctor()
extern "C"  void TFTTutorialManager__ctor_m2170542414 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::Start()
extern "C"  void TFTTutorialManager_Start_m1117680206 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::EnableTFTTutorial()
extern "C"  void TFTTutorialManager_EnableTFTTutorial_m351028969 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::ShowTFTCategory()
extern "C"  void TFTTutorialManager_ShowTFTCategory_m3889737743 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::CloseTFTCategory()
extern "C"  void TFTTutorialManager_CloseTFTCategory_m614019838 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::SetTutorial(System.Byte)
extern "C"  void TFTTutorialManager_SetTutorial_m4043871199 (TFTTutorialManager_t3352625645 * __this, uint8_t ___tutID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::NextStep()
extern "C"  void TFTTutorialManager_NextStep_m2586413141 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::PreviousStep()
extern "C"  void TFTTutorialManager_PreviousStep_m3661644505 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::ChangeStep(System.SByte)
extern "C"  void TFTTutorialManager_ChangeStep_m4163095670 (TFTTutorialManager_t3352625645 * __this, int8_t ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::AnimateReturnButton(System.Boolean)
extern "C"  void TFTTutorialManager_AnimateReturnButton_m2671453094 (TFTTutorialManager_t3352625645 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::EnableButton(UnityEngine.GameObject,System.Boolean)
extern "C"  void TFTTutorialManager_EnableButton_m883157338 (TFTTutorialManager_t3352625645 * __this, GameObject_t3674682005 * ___button0, bool ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::ToggleAudio()
extern "C"  void TFTTutorialManager_ToggleAudio_m2690217614 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::PlayAudio(UnityEngine.AudioClip)
extern "C"  void TFTTutorialManager_PlayAudio_m104658165 (TFTTutorialManager_t3352625645 * __this, AudioClip_t794140988 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::BackToScene()
extern "C"  void TFTTutorialManager_BackToScene_m2934534038 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TFTTutorialManager::Update()
extern "C"  void TFTTutorialManager_Update_m294200191 (TFTTutorialManager_t3352625645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
