﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TTarget
struct  TTarget_t4033793797  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 TTarget::buttonPos
	Vector3_t4282066566  ___buttonPos_2;

public:
	inline static int32_t get_offset_of_buttonPos_2() { return static_cast<int32_t>(offsetof(TTarget_t4033793797, ___buttonPos_2)); }
	inline Vector3_t4282066566  get_buttonPos_2() const { return ___buttonPos_2; }
	inline Vector3_t4282066566 * get_address_of_buttonPos_2() { return &___buttonPos_2; }
	inline void set_buttonPos_2(Vector3_t4282066566  value)
	{
		___buttonPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
