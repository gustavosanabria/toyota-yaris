﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TTarget
struct TTarget_t4033793797;

#include "codegen/il2cpp-codegen.h"

// System.Void TTarget::.ctor()
extern "C"  void TTarget__ctor_m2237822470 (TTarget_t4033793797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TTarget::Start()
extern "C"  void TTarget_Start_m1184960262 (TTarget_t4033793797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
