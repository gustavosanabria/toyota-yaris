﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// VRCameraClamper
struct VRCameraClamper_t2660686439;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetManager
struct  TargetManager_t3938169020  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] TargetManager::targets
	GameObjectU5BU5D_t2662109048* ___targets_2;
	// System.Int32 TargetManager::targetsCount
	int32_t ___targetsCount_3;
	// UICamera TargetManager::targetsCameraUIScript
	UICamera_t189364953 * ___targetsCameraUIScript_4;
	// UnityEngine.Sprite TargetManager::circleTargetHiLight
	Sprite_t3199167241 * ___circleTargetHiLight_5;
	// UnityEngine.Sprite TargetManager::rectTargetHiLight
	Sprite_t3199167241 * ___rectTargetHiLight_6;
	// System.Boolean TargetManager::enableAnims
	bool ___enableAnims_7;
	// UnityEngine.GameObject TargetManager::magnifierGO
	GameObject_t3674682005 * ___magnifierGO_8;
	// System.Boolean TargetManager::_show
	bool ____show_9;
	// UnityEngine.GameObject TargetManager::Targets
	GameObject_t3674682005 * ___Targets_10;
	// VRCameraClamper TargetManager::vrCameraClamper
	VRCameraClamper_t2660686439 * ___vrCameraClamper_11;

public:
	inline static int32_t get_offset_of_targets_2() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___targets_2)); }
	inline GameObjectU5BU5D_t2662109048* get_targets_2() const { return ___targets_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_targets_2() { return &___targets_2; }
	inline void set_targets_2(GameObjectU5BU5D_t2662109048* value)
	{
		___targets_2 = value;
		Il2CppCodeGenWriteBarrier(&___targets_2, value);
	}

	inline static int32_t get_offset_of_targetsCount_3() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___targetsCount_3)); }
	inline int32_t get_targetsCount_3() const { return ___targetsCount_3; }
	inline int32_t* get_address_of_targetsCount_3() { return &___targetsCount_3; }
	inline void set_targetsCount_3(int32_t value)
	{
		___targetsCount_3 = value;
	}

	inline static int32_t get_offset_of_targetsCameraUIScript_4() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___targetsCameraUIScript_4)); }
	inline UICamera_t189364953 * get_targetsCameraUIScript_4() const { return ___targetsCameraUIScript_4; }
	inline UICamera_t189364953 ** get_address_of_targetsCameraUIScript_4() { return &___targetsCameraUIScript_4; }
	inline void set_targetsCameraUIScript_4(UICamera_t189364953 * value)
	{
		___targetsCameraUIScript_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetsCameraUIScript_4, value);
	}

	inline static int32_t get_offset_of_circleTargetHiLight_5() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___circleTargetHiLight_5)); }
	inline Sprite_t3199167241 * get_circleTargetHiLight_5() const { return ___circleTargetHiLight_5; }
	inline Sprite_t3199167241 ** get_address_of_circleTargetHiLight_5() { return &___circleTargetHiLight_5; }
	inline void set_circleTargetHiLight_5(Sprite_t3199167241 * value)
	{
		___circleTargetHiLight_5 = value;
		Il2CppCodeGenWriteBarrier(&___circleTargetHiLight_5, value);
	}

	inline static int32_t get_offset_of_rectTargetHiLight_6() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___rectTargetHiLight_6)); }
	inline Sprite_t3199167241 * get_rectTargetHiLight_6() const { return ___rectTargetHiLight_6; }
	inline Sprite_t3199167241 ** get_address_of_rectTargetHiLight_6() { return &___rectTargetHiLight_6; }
	inline void set_rectTargetHiLight_6(Sprite_t3199167241 * value)
	{
		___rectTargetHiLight_6 = value;
		Il2CppCodeGenWriteBarrier(&___rectTargetHiLight_6, value);
	}

	inline static int32_t get_offset_of_enableAnims_7() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___enableAnims_7)); }
	inline bool get_enableAnims_7() const { return ___enableAnims_7; }
	inline bool* get_address_of_enableAnims_7() { return &___enableAnims_7; }
	inline void set_enableAnims_7(bool value)
	{
		___enableAnims_7 = value;
	}

	inline static int32_t get_offset_of_magnifierGO_8() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___magnifierGO_8)); }
	inline GameObject_t3674682005 * get_magnifierGO_8() const { return ___magnifierGO_8; }
	inline GameObject_t3674682005 ** get_address_of_magnifierGO_8() { return &___magnifierGO_8; }
	inline void set_magnifierGO_8(GameObject_t3674682005 * value)
	{
		___magnifierGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___magnifierGO_8, value);
	}

	inline static int32_t get_offset_of__show_9() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ____show_9)); }
	inline bool get__show_9() const { return ____show_9; }
	inline bool* get_address_of__show_9() { return &____show_9; }
	inline void set__show_9(bool value)
	{
		____show_9 = value;
	}

	inline static int32_t get_offset_of_Targets_10() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___Targets_10)); }
	inline GameObject_t3674682005 * get_Targets_10() const { return ___Targets_10; }
	inline GameObject_t3674682005 ** get_address_of_Targets_10() { return &___Targets_10; }
	inline void set_Targets_10(GameObject_t3674682005 * value)
	{
		___Targets_10 = value;
		Il2CppCodeGenWriteBarrier(&___Targets_10, value);
	}

	inline static int32_t get_offset_of_vrCameraClamper_11() { return static_cast<int32_t>(offsetof(TargetManager_t3938169020, ___vrCameraClamper_11)); }
	inline VRCameraClamper_t2660686439 * get_vrCameraClamper_11() const { return ___vrCameraClamper_11; }
	inline VRCameraClamper_t2660686439 ** get_address_of_vrCameraClamper_11() { return &___vrCameraClamper_11; }
	inline void set_vrCameraClamper_11(VRCameraClamper_t2660686439 * value)
	{
		___vrCameraClamper_11 = value;
		Il2CppCodeGenWriteBarrier(&___vrCameraClamper_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
