﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetManager
struct TargetManager_t3938169020;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void TargetManager::.ctor()
extern "C"  void TargetManager__ctor_m3256075311 (TargetManager_t3938169020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::LateUpdate()
extern "C"  void TargetManager_LateUpdate_m3501181828 (TargetManager_t3938169020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::EnableTargetAnim()
extern "C"  void TargetManager_EnableTargetAnim_m2613867802 (TargetManager_t3938169020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::EnableTargets(System.Boolean)
extern "C"  void TargetManager_EnableTargets_m953070595 (TargetManager_t3938169020 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::ShowTargets(System.Boolean)
extern "C"  void TargetManager_ShowTargets_m660133961 (TargetManager_t3938169020 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::StopTargetAnim(UnityEngine.GameObject)
extern "C"  void TargetManager_StopTargetAnim_m4259443921 (TargetManager_t3938169020 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::EnableTargetUICamera(System.Boolean)
extern "C"  void TargetManager_EnableTargetUICamera_m1273812409 (TargetManager_t3938169020 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::CircleTargetClick(UnityEngine.GameObject)
extern "C"  void TargetManager_CircleTargetClick_m2647685516 (TargetManager_t3938169020 * __this, GameObject_t3674682005 * ___targetGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::RectTargetClick(UnityEngine.GameObject)
extern "C"  void TargetManager_RectTargetClick_m2398553848 (TargetManager_t3938169020 * __this, GameObject_t3674682005 * ___targetGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::RectLongTargetClick(UnityEngine.GameObject)
extern "C"  void TargetManager_RectLongTargetClick_m2251600604 (TargetManager_t3938169020 * __this, GameObject_t3674682005 * ___targetGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::TachometerTarget2Click()
extern "C"  void TargetManager_TachometerTarget2Click_m913600360 (TargetManager_t3938169020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetManager::VolumeControlTargetClick()
extern "C"  void TargetManager_VolumeControlTargetClick_m1693298121 (TargetManager_t3938169020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
