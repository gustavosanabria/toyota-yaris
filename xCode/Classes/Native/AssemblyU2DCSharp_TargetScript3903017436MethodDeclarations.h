﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetScript
struct TargetScript_t3903017436;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetScript::.ctor()
extern "C"  void TargetScript__ctor_m3530356799 (TargetScript_t3903017436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetScript::Awake()
extern "C"  void TargetScript_Awake_m3767962018 (TargetScript_t3903017436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetScript::Update()
extern "C"  void TargetScript_Update_m3793740462 (TargetScript_t3903017436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
