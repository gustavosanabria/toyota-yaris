﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToCenter
struct  ToCenter_t2110661328  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 ToCenter::currentHairPos
	Vector3_t4282066566  ___currentHairPos_2;
	// UnityEngine.Vector3 ToCenter::target
	Vector3_t4282066566  ___target_3;
	// UnityEngine.Vector3 ToCenter::fwd
	Vector3_t4282066566  ___fwd_4;
	// UnityEngine.Vector3 ToCenter::point
	Vector3_t4282066566  ___point_5;
	// UnityEngine.Vector3 ToCenter::toPos
	Vector3_t4282066566  ___toPos_6;
	// UnityEngine.Vector3 ToCenter::originalPos
	Vector3_t4282066566  ___originalPos_7;
	// UnityEngine.Vector3 ToCenter::targetScale
	Vector3_t4282066566  ___targetScale_8;
	// UnityEngine.Vector3 ToCenter::targetRot
	Vector3_t4282066566  ___targetRot_9;
	// UnityEngine.Vector3 ToCenter::originalScale
	Vector3_t4282066566  ___originalScale_10;
	// UnityEngine.RaycastHit ToCenter::hit
	RaycastHit_t4003175726  ___hit_11;
	// UnityEngine.GameObject ToCenter::camara
	GameObject_t3674682005 * ___camara_12;
	// System.Boolean ToCenter::doTween
	bool ___doTween_13;
	// System.Boolean ToCenter::slide
	bool ___slide_14;
	// System.Boolean ToCenter::matchSize
	bool ___matchSize_15;
	// System.Single ToCenter::offset
	float ___offset_16;
	// System.Single ToCenter::slideSpeed
	float ___slideSpeed_17;
	// System.Single ToCenter::scaleFactor
	float ___scaleFactor_18;
	// System.Single ToCenter::originalZ
	float ___originalZ_19;
	// System.Single ToCenter::distance
	float ___distance_20;

public:
	inline static int32_t get_offset_of_currentHairPos_2() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___currentHairPos_2)); }
	inline Vector3_t4282066566  get_currentHairPos_2() const { return ___currentHairPos_2; }
	inline Vector3_t4282066566 * get_address_of_currentHairPos_2() { return &___currentHairPos_2; }
	inline void set_currentHairPos_2(Vector3_t4282066566  value)
	{
		___currentHairPos_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___target_3)); }
	inline Vector3_t4282066566  get_target_3() const { return ___target_3; }
	inline Vector3_t4282066566 * get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Vector3_t4282066566  value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_fwd_4() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___fwd_4)); }
	inline Vector3_t4282066566  get_fwd_4() const { return ___fwd_4; }
	inline Vector3_t4282066566 * get_address_of_fwd_4() { return &___fwd_4; }
	inline void set_fwd_4(Vector3_t4282066566  value)
	{
		___fwd_4 = value;
	}

	inline static int32_t get_offset_of_point_5() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___point_5)); }
	inline Vector3_t4282066566  get_point_5() const { return ___point_5; }
	inline Vector3_t4282066566 * get_address_of_point_5() { return &___point_5; }
	inline void set_point_5(Vector3_t4282066566  value)
	{
		___point_5 = value;
	}

	inline static int32_t get_offset_of_toPos_6() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___toPos_6)); }
	inline Vector3_t4282066566  get_toPos_6() const { return ___toPos_6; }
	inline Vector3_t4282066566 * get_address_of_toPos_6() { return &___toPos_6; }
	inline void set_toPos_6(Vector3_t4282066566  value)
	{
		___toPos_6 = value;
	}

	inline static int32_t get_offset_of_originalPos_7() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___originalPos_7)); }
	inline Vector3_t4282066566  get_originalPos_7() const { return ___originalPos_7; }
	inline Vector3_t4282066566 * get_address_of_originalPos_7() { return &___originalPos_7; }
	inline void set_originalPos_7(Vector3_t4282066566  value)
	{
		___originalPos_7 = value;
	}

	inline static int32_t get_offset_of_targetScale_8() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___targetScale_8)); }
	inline Vector3_t4282066566  get_targetScale_8() const { return ___targetScale_8; }
	inline Vector3_t4282066566 * get_address_of_targetScale_8() { return &___targetScale_8; }
	inline void set_targetScale_8(Vector3_t4282066566  value)
	{
		___targetScale_8 = value;
	}

	inline static int32_t get_offset_of_targetRot_9() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___targetRot_9)); }
	inline Vector3_t4282066566  get_targetRot_9() const { return ___targetRot_9; }
	inline Vector3_t4282066566 * get_address_of_targetRot_9() { return &___targetRot_9; }
	inline void set_targetRot_9(Vector3_t4282066566  value)
	{
		___targetRot_9 = value;
	}

	inline static int32_t get_offset_of_originalScale_10() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___originalScale_10)); }
	inline Vector3_t4282066566  get_originalScale_10() const { return ___originalScale_10; }
	inline Vector3_t4282066566 * get_address_of_originalScale_10() { return &___originalScale_10; }
	inline void set_originalScale_10(Vector3_t4282066566  value)
	{
		___originalScale_10 = value;
	}

	inline static int32_t get_offset_of_hit_11() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___hit_11)); }
	inline RaycastHit_t4003175726  get_hit_11() const { return ___hit_11; }
	inline RaycastHit_t4003175726 * get_address_of_hit_11() { return &___hit_11; }
	inline void set_hit_11(RaycastHit_t4003175726  value)
	{
		___hit_11 = value;
	}

	inline static int32_t get_offset_of_camara_12() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___camara_12)); }
	inline GameObject_t3674682005 * get_camara_12() const { return ___camara_12; }
	inline GameObject_t3674682005 ** get_address_of_camara_12() { return &___camara_12; }
	inline void set_camara_12(GameObject_t3674682005 * value)
	{
		___camara_12 = value;
		Il2CppCodeGenWriteBarrier(&___camara_12, value);
	}

	inline static int32_t get_offset_of_doTween_13() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___doTween_13)); }
	inline bool get_doTween_13() const { return ___doTween_13; }
	inline bool* get_address_of_doTween_13() { return &___doTween_13; }
	inline void set_doTween_13(bool value)
	{
		___doTween_13 = value;
	}

	inline static int32_t get_offset_of_slide_14() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___slide_14)); }
	inline bool get_slide_14() const { return ___slide_14; }
	inline bool* get_address_of_slide_14() { return &___slide_14; }
	inline void set_slide_14(bool value)
	{
		___slide_14 = value;
	}

	inline static int32_t get_offset_of_matchSize_15() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___matchSize_15)); }
	inline bool get_matchSize_15() const { return ___matchSize_15; }
	inline bool* get_address_of_matchSize_15() { return &___matchSize_15; }
	inline void set_matchSize_15(bool value)
	{
		___matchSize_15 = value;
	}

	inline static int32_t get_offset_of_offset_16() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___offset_16)); }
	inline float get_offset_16() const { return ___offset_16; }
	inline float* get_address_of_offset_16() { return &___offset_16; }
	inline void set_offset_16(float value)
	{
		___offset_16 = value;
	}

	inline static int32_t get_offset_of_slideSpeed_17() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___slideSpeed_17)); }
	inline float get_slideSpeed_17() const { return ___slideSpeed_17; }
	inline float* get_address_of_slideSpeed_17() { return &___slideSpeed_17; }
	inline void set_slideSpeed_17(float value)
	{
		___slideSpeed_17 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_18() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___scaleFactor_18)); }
	inline float get_scaleFactor_18() const { return ___scaleFactor_18; }
	inline float* get_address_of_scaleFactor_18() { return &___scaleFactor_18; }
	inline void set_scaleFactor_18(float value)
	{
		___scaleFactor_18 = value;
	}

	inline static int32_t get_offset_of_originalZ_19() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___originalZ_19)); }
	inline float get_originalZ_19() const { return ___originalZ_19; }
	inline float* get_address_of_originalZ_19() { return &___originalZ_19; }
	inline void set_originalZ_19(float value)
	{
		___originalZ_19 = value;
	}

	inline static int32_t get_offset_of_distance_20() { return static_cast<int32_t>(offsetof(ToCenter_t2110661328, ___distance_20)); }
	inline float get_distance_20() const { return ___distance_20; }
	inline float* get_address_of_distance_20() { return &___distance_20; }
	inline void set_distance_20(float value)
	{
		___distance_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
