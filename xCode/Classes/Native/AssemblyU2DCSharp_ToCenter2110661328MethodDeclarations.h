﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToCenter
struct ToCenter_t2110661328;

#include "codegen/il2cpp-codegen.h"

// System.Void ToCenter::.ctor()
extern "C"  void ToCenter__ctor_m1864680907 (ToCenter_t2110661328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToCenter::Awake()
extern "C"  void ToCenter_Awake_m2102286126 (ToCenter_t2110661328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToCenter::Update()
extern "C"  void ToCenter_Update_m3697395362 (ToCenter_t2110661328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
