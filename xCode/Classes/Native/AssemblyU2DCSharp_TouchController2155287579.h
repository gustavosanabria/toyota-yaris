﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchController
struct  TouchController_t2155287579  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Quaternion TouchController::referanceRotation
	Quaternion_t1553702882  ___referanceRotation_2;
	// UnityEngine.Quaternion TouchController::originalRotation
	Quaternion_t1553702882  ___originalRotation_3;
	// System.Single TouchController::sensitivityX
	float ___sensitivityX_4;
	// System.Single TouchController::sensitivityY
	float ___sensitivityY_5;
	// System.Single TouchController::minimumX
	float ___minimumX_6;
	// System.Single TouchController::maximumX
	float ___maximumX_7;
	// System.Single TouchController::minimumY
	float ___minimumY_8;
	// System.Single TouchController::maximumY
	float ___maximumY_9;
	// System.Single TouchController::rotationY
	float ___rotationY_10;
	// System.Single TouchController::rotationX
	float ___rotationX_11;
	// UnityEngine.Quaternion TouchController::rot
	Quaternion_t1553702882  ___rot_12;
	// System.Boolean TouchController::smoothRot
	bool ___smoothRot_13;

public:
	inline static int32_t get_offset_of_referanceRotation_2() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___referanceRotation_2)); }
	inline Quaternion_t1553702882  get_referanceRotation_2() const { return ___referanceRotation_2; }
	inline Quaternion_t1553702882 * get_address_of_referanceRotation_2() { return &___referanceRotation_2; }
	inline void set_referanceRotation_2(Quaternion_t1553702882  value)
	{
		___referanceRotation_2 = value;
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___originalRotation_3)); }
	inline Quaternion_t1553702882  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t1553702882 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t1553702882  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_4() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___sensitivityX_4)); }
	inline float get_sensitivityX_4() const { return ___sensitivityX_4; }
	inline float* get_address_of_sensitivityX_4() { return &___sensitivityX_4; }
	inline void set_sensitivityX_4(float value)
	{
		___sensitivityX_4 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_5() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___sensitivityY_5)); }
	inline float get_sensitivityY_5() const { return ___sensitivityY_5; }
	inline float* get_address_of_sensitivityY_5() { return &___sensitivityY_5; }
	inline void set_sensitivityY_5(float value)
	{
		___sensitivityY_5 = value;
	}

	inline static int32_t get_offset_of_minimumX_6() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___minimumX_6)); }
	inline float get_minimumX_6() const { return ___minimumX_6; }
	inline float* get_address_of_minimumX_6() { return &___minimumX_6; }
	inline void set_minimumX_6(float value)
	{
		___minimumX_6 = value;
	}

	inline static int32_t get_offset_of_maximumX_7() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___maximumX_7)); }
	inline float get_maximumX_7() const { return ___maximumX_7; }
	inline float* get_address_of_maximumX_7() { return &___maximumX_7; }
	inline void set_maximumX_7(float value)
	{
		___maximumX_7 = value;
	}

	inline static int32_t get_offset_of_minimumY_8() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___minimumY_8)); }
	inline float get_minimumY_8() const { return ___minimumY_8; }
	inline float* get_address_of_minimumY_8() { return &___minimumY_8; }
	inline void set_minimumY_8(float value)
	{
		___minimumY_8 = value;
	}

	inline static int32_t get_offset_of_maximumY_9() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___maximumY_9)); }
	inline float get_maximumY_9() const { return ___maximumY_9; }
	inline float* get_address_of_maximumY_9() { return &___maximumY_9; }
	inline void set_maximumY_9(float value)
	{
		___maximumY_9 = value;
	}

	inline static int32_t get_offset_of_rotationY_10() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___rotationY_10)); }
	inline float get_rotationY_10() const { return ___rotationY_10; }
	inline float* get_address_of_rotationY_10() { return &___rotationY_10; }
	inline void set_rotationY_10(float value)
	{
		___rotationY_10 = value;
	}

	inline static int32_t get_offset_of_rotationX_11() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___rotationX_11)); }
	inline float get_rotationX_11() const { return ___rotationX_11; }
	inline float* get_address_of_rotationX_11() { return &___rotationX_11; }
	inline void set_rotationX_11(float value)
	{
		___rotationX_11 = value;
	}

	inline static int32_t get_offset_of_rot_12() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___rot_12)); }
	inline Quaternion_t1553702882  get_rot_12() const { return ___rot_12; }
	inline Quaternion_t1553702882 * get_address_of_rot_12() { return &___rot_12; }
	inline void set_rot_12(Quaternion_t1553702882  value)
	{
		___rot_12 = value;
	}

	inline static int32_t get_offset_of_smoothRot_13() { return static_cast<int32_t>(offsetof(TouchController_t2155287579, ___smoothRot_13)); }
	inline bool get_smoothRot_13() const { return ___smoothRot_13; }
	inline bool* get_address_of_smoothRot_13() { return &___smoothRot_13; }
	inline void set_smoothRot_13(bool value)
	{
		___smoothRot_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
