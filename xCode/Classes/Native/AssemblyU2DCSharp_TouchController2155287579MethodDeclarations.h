﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchController
struct TouchController_t2155287579;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void TouchController::.ctor()
extern "C"  void TouchController__ctor_m1747329456 (TouchController_t2155287579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchController::Start()
extern "C"  void TouchController_Start_m694467248 (TouchController_t2155287579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchController::Update()
extern "C"  void TouchController_Update_m59500381 (TouchController_t2155287579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchController::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float TouchController_ClampAngle_m4274337621 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion TouchController::ConvertRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  TouchController_ConvertRotation_m1442005554 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
