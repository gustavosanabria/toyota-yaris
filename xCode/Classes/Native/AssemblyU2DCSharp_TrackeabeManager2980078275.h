﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// TrackeabeManager
struct TrackeabeManager_t2980078275;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackeabeManager
struct  TrackeabeManager_t2980078275  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] TrackeabeManager::steeringWheelmarkersGO
	GameObjectU5BU5D_t2662109048* ___steeringWheelmarkersGO_2;
	// System.Byte TrackeabeManager::steeringWheelmarkersCount
	uint8_t ___steeringWheelmarkersCount_3;
	// UnityEngine.GameObject[] TrackeabeManager::rav4SWMarkersGO
	GameObjectU5BU5D_t2662109048* ___rav4SWMarkersGO_4;
	// System.Byte TrackeabeManager::rav4SWMarkersCount
	uint8_t ___rav4SWMarkersCount_5;
	// UnityEngine.GameObject[] TrackeabeManager::rav4ACMarkersGO
	GameObjectU5BU5D_t2662109048* ___rav4ACMarkersGO_6;
	// System.Byte TrackeabeManager::rav4ACMarkersCount
	uint8_t ___rav4ACMarkersCount_7;
	// UnityEngine.GameObject[] TrackeabeManager::rav4TachometerGO
	GameObjectU5BU5D_t2662109048* ___rav4TachometerGO_8;
	// System.Byte TrackeabeManager::rav4TachometerMarkersCount
	uint8_t ___rav4TachometerMarkersCount_9;

public:
	inline static int32_t get_offset_of_steeringWheelmarkersGO_2() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___steeringWheelmarkersGO_2)); }
	inline GameObjectU5BU5D_t2662109048* get_steeringWheelmarkersGO_2() const { return ___steeringWheelmarkersGO_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_steeringWheelmarkersGO_2() { return &___steeringWheelmarkersGO_2; }
	inline void set_steeringWheelmarkersGO_2(GameObjectU5BU5D_t2662109048* value)
	{
		___steeringWheelmarkersGO_2 = value;
		Il2CppCodeGenWriteBarrier(&___steeringWheelmarkersGO_2, value);
	}

	inline static int32_t get_offset_of_steeringWheelmarkersCount_3() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___steeringWheelmarkersCount_3)); }
	inline uint8_t get_steeringWheelmarkersCount_3() const { return ___steeringWheelmarkersCount_3; }
	inline uint8_t* get_address_of_steeringWheelmarkersCount_3() { return &___steeringWheelmarkersCount_3; }
	inline void set_steeringWheelmarkersCount_3(uint8_t value)
	{
		___steeringWheelmarkersCount_3 = value;
	}

	inline static int32_t get_offset_of_rav4SWMarkersGO_4() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___rav4SWMarkersGO_4)); }
	inline GameObjectU5BU5D_t2662109048* get_rav4SWMarkersGO_4() const { return ___rav4SWMarkersGO_4; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_rav4SWMarkersGO_4() { return &___rav4SWMarkersGO_4; }
	inline void set_rav4SWMarkersGO_4(GameObjectU5BU5D_t2662109048* value)
	{
		___rav4SWMarkersGO_4 = value;
		Il2CppCodeGenWriteBarrier(&___rav4SWMarkersGO_4, value);
	}

	inline static int32_t get_offset_of_rav4SWMarkersCount_5() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___rav4SWMarkersCount_5)); }
	inline uint8_t get_rav4SWMarkersCount_5() const { return ___rav4SWMarkersCount_5; }
	inline uint8_t* get_address_of_rav4SWMarkersCount_5() { return &___rav4SWMarkersCount_5; }
	inline void set_rav4SWMarkersCount_5(uint8_t value)
	{
		___rav4SWMarkersCount_5 = value;
	}

	inline static int32_t get_offset_of_rav4ACMarkersGO_6() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___rav4ACMarkersGO_6)); }
	inline GameObjectU5BU5D_t2662109048* get_rav4ACMarkersGO_6() const { return ___rav4ACMarkersGO_6; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_rav4ACMarkersGO_6() { return &___rav4ACMarkersGO_6; }
	inline void set_rav4ACMarkersGO_6(GameObjectU5BU5D_t2662109048* value)
	{
		___rav4ACMarkersGO_6 = value;
		Il2CppCodeGenWriteBarrier(&___rav4ACMarkersGO_6, value);
	}

	inline static int32_t get_offset_of_rav4ACMarkersCount_7() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___rav4ACMarkersCount_7)); }
	inline uint8_t get_rav4ACMarkersCount_7() const { return ___rav4ACMarkersCount_7; }
	inline uint8_t* get_address_of_rav4ACMarkersCount_7() { return &___rav4ACMarkersCount_7; }
	inline void set_rav4ACMarkersCount_7(uint8_t value)
	{
		___rav4ACMarkersCount_7 = value;
	}

	inline static int32_t get_offset_of_rav4TachometerGO_8() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___rav4TachometerGO_8)); }
	inline GameObjectU5BU5D_t2662109048* get_rav4TachometerGO_8() const { return ___rav4TachometerGO_8; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_rav4TachometerGO_8() { return &___rav4TachometerGO_8; }
	inline void set_rav4TachometerGO_8(GameObjectU5BU5D_t2662109048* value)
	{
		___rav4TachometerGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___rav4TachometerGO_8, value);
	}

	inline static int32_t get_offset_of_rav4TachometerMarkersCount_9() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275, ___rav4TachometerMarkersCount_9)); }
	inline uint8_t get_rav4TachometerMarkersCount_9() const { return ___rav4TachometerMarkersCount_9; }
	inline uint8_t* get_address_of_rav4TachometerMarkersCount_9() { return &___rav4TachometerMarkersCount_9; }
	inline void set_rav4TachometerMarkersCount_9(uint8_t value)
	{
		___rav4TachometerMarkersCount_9 = value;
	}
};

struct TrackeabeManager_t2980078275_StaticFields
{
public:
	// TrackeabeManager TrackeabeManager::<instance>k__BackingField
	TrackeabeManager_t2980078275 * ___U3CinstanceU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TrackeabeManager_t2980078275_StaticFields, ___U3CinstanceU3Ek__BackingField_10)); }
	inline TrackeabeManager_t2980078275 * get_U3CinstanceU3Ek__BackingField_10() const { return ___U3CinstanceU3Ek__BackingField_10; }
	inline TrackeabeManager_t2980078275 ** get_address_of_U3CinstanceU3Ek__BackingField_10() { return &___U3CinstanceU3Ek__BackingField_10; }
	inline void set_U3CinstanceU3Ek__BackingField_10(TrackeabeManager_t2980078275 * value)
	{
		___U3CinstanceU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinstanceU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
