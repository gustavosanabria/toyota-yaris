﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrackeabeManager
struct TrackeabeManager_t2980078275;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TrackeabeManager2980078275.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void TrackeabeManager::.ctor()
extern "C"  void TrackeabeManager__ctor_m1005858616 (TrackeabeManager_t2980078275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TrackeabeManager TrackeabeManager::get_instance()
extern "C"  TrackeabeManager_t2980078275 * TrackeabeManager_get_instance_m2045824292 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::set_instance(TrackeabeManager)
extern "C"  void TrackeabeManager_set_instance_m3323987675 (Il2CppObject * __this /* static, unused */, TrackeabeManager_t2980078275 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TrackeabeManager TrackeabeManager::GetInstance()
extern "C"  TrackeabeManager_t2980078275 * TrackeabeManager_GetInstance_m3419145031 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::Awake()
extern "C"  void TrackeabeManager_Awake_m1243463835 (TrackeabeManager_t2980078275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::ActivateMarkers(System.Byte)
extern "C"  void TrackeabeManager_ActivateMarkers_m1367472719 (TrackeabeManager_t2980078275 * __this, uint8_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::DeactivateAlternateMarkers(System.Byte,UnityEngine.GameObject)
extern "C"  void TrackeabeManager_DeactivateAlternateMarkers_m2542022194 (TrackeabeManager_t2980078275 * __this, uint8_t ___markerID0, GameObject_t3674682005 * ___workingMarker1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::ActivateAllSteeringWheelMarkers()
extern "C"  void TrackeabeManager_ActivateAllSteeringWheelMarkers_m1927212115 (TrackeabeManager_t2980078275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::DeactivateAlternateSteeringWheelMarker(UnityEngine.GameObject)
extern "C"  void TrackeabeManager_DeactivateAlternateSteeringWheelMarker_m1844736706 (TrackeabeManager_t2980078275 * __this, GameObject_t3674682005 * ___workingMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::ActivateAllRav4SWMarkers()
extern "C"  void TrackeabeManager_ActivateAllRav4SWMarkers_m1794904614 (TrackeabeManager_t2980078275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::DeactivateAlternateRav4SWMarker(UnityEngine.GameObject)
extern "C"  void TrackeabeManager_DeactivateAlternateRav4SWMarker_m2024847679 (TrackeabeManager_t2980078275 * __this, GameObject_t3674682005 * ___workingMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::ActivateAllRav4ACMarkers()
extern "C"  void TrackeabeManager_ActivateAllRav4ACMarkers_m3710560808 (TrackeabeManager_t2980078275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::DeactivateAlternateRav4ACMarker(UnityEngine.GameObject)
extern "C"  void TrackeabeManager_DeactivateAlternateRav4ACMarker_m1125947005 (TrackeabeManager_t2980078275 * __this, GameObject_t3674682005 * ___workingMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::ActivateAllRav4ATachometerMarkers()
extern "C"  void TrackeabeManager_ActivateAllRav4ATachometerMarkers_m3701783325 (TrackeabeManager_t2980078275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackeabeManager::DeactivateAlternateRav4TachometerMarker(UnityEngine.GameObject)
extern "C"  void TrackeabeManager_DeactivateAlternateRav4TachometerMarker_m3380734855 (TrackeabeManager_t2980078275 * __this, GameObject_t3674682005 * ___workingMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
