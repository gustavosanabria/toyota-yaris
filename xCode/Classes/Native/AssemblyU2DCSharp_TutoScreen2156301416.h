﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISpriteAnimation
struct UISpriteAnimation_t4279777547;
// UIScrollView
struct UIScrollView_t2113479878;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutoScreen
struct  TutoScreen_t2156301416  : public MonoBehaviour_t667441552
{
public:
	// UISpriteAnimation TutoScreen::flashTextureAnim
	UISpriteAnimation_t4279777547 * ___flashTextureAnim_2;
	// UIScrollView TutoScreen::buttonsScroll
	UIScrollView_t2113479878 * ___buttonsScroll_3;
	// System.Single TutoScreen::scrollAmmount
	float ___scrollAmmount_4;

public:
	inline static int32_t get_offset_of_flashTextureAnim_2() { return static_cast<int32_t>(offsetof(TutoScreen_t2156301416, ___flashTextureAnim_2)); }
	inline UISpriteAnimation_t4279777547 * get_flashTextureAnim_2() const { return ___flashTextureAnim_2; }
	inline UISpriteAnimation_t4279777547 ** get_address_of_flashTextureAnim_2() { return &___flashTextureAnim_2; }
	inline void set_flashTextureAnim_2(UISpriteAnimation_t4279777547 * value)
	{
		___flashTextureAnim_2 = value;
		Il2CppCodeGenWriteBarrier(&___flashTextureAnim_2, value);
	}

	inline static int32_t get_offset_of_buttonsScroll_3() { return static_cast<int32_t>(offsetof(TutoScreen_t2156301416, ___buttonsScroll_3)); }
	inline UIScrollView_t2113479878 * get_buttonsScroll_3() const { return ___buttonsScroll_3; }
	inline UIScrollView_t2113479878 ** get_address_of_buttonsScroll_3() { return &___buttonsScroll_3; }
	inline void set_buttonsScroll_3(UIScrollView_t2113479878 * value)
	{
		___buttonsScroll_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsScroll_3, value);
	}

	inline static int32_t get_offset_of_scrollAmmount_4() { return static_cast<int32_t>(offsetof(TutoScreen_t2156301416, ___scrollAmmount_4)); }
	inline float get_scrollAmmount_4() const { return ___scrollAmmount_4; }
	inline float* get_address_of_scrollAmmount_4() { return &___scrollAmmount_4; }
	inline void set_scrollAmmount_4(float value)
	{
		___scrollAmmount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
