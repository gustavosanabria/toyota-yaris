﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutoScreen
struct TutoScreen_t2156301416;

#include "codegen/il2cpp-codegen.h"

// System.Void TutoScreen::.ctor()
extern "C"  void TutoScreen__ctor_m3115876659 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickDownArrow()
extern "C"  void TutoScreen_ClickDownArrow_m3459394000 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickUpArrow()
extern "C"  void TutoScreen_ClickUpArrow_m2485364663 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::Scroll(System.Single)
extern "C"  void TutoScreen_Scroll_m2727362861 (TutoScreen_t2156301416 * __this, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn1()
extern "C"  void TutoScreen_ClickTutBtn1_m1062534417 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn2()
extern "C"  void TutoScreen_ClickTutBtn2_m1062535378 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn3()
extern "C"  void TutoScreen_ClickTutBtn3_m1062536339 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn4()
extern "C"  void TutoScreen_ClickTutBtn4_m1062537300 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn5()
extern "C"  void TutoScreen_ClickTutBtn5_m1062538261 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn6()
extern "C"  void TutoScreen_ClickTutBtn6_m1062539222 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn7()
extern "C"  void TutoScreen_ClickTutBtn7_m1062540183 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn8()
extern "C"  void TutoScreen_ClickTutBtn8_m1062541144 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn9()
extern "C"  void TutoScreen_ClickTutBtn9_m1062542105 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn10()
extern "C"  void TutoScreen_ClickTutBtn10_m2873803553 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoScreen::ClickTutBtn11()
extern "C"  void TutoScreen_ClickTutBtn11_m2873804514 (TutoScreen_t2156301416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
