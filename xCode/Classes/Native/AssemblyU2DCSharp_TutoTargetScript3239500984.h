﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Animator
struct Animator_t2776330603;
// TweenScale
struct TweenScale_t2936666559;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutoTargetScript
struct  TutoTargetScript_t3239500984  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite TutoTargetScript::targetHL
	Sprite_t3199167241 * ___targetHL_2;
	// UnityEngine.Animator TutoTargetScript::an
	Animator_t2776330603 * ___an_3;
	// TweenScale TutoTargetScript::tS
	TweenScale_t2936666559 * ___tS_4;
	// UnityEngine.Vector3 TutoTargetScript::initSize
	Vector3_t4282066566  ___initSize_5;

public:
	inline static int32_t get_offset_of_targetHL_2() { return static_cast<int32_t>(offsetof(TutoTargetScript_t3239500984, ___targetHL_2)); }
	inline Sprite_t3199167241 * get_targetHL_2() const { return ___targetHL_2; }
	inline Sprite_t3199167241 ** get_address_of_targetHL_2() { return &___targetHL_2; }
	inline void set_targetHL_2(Sprite_t3199167241 * value)
	{
		___targetHL_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetHL_2, value);
	}

	inline static int32_t get_offset_of_an_3() { return static_cast<int32_t>(offsetof(TutoTargetScript_t3239500984, ___an_3)); }
	inline Animator_t2776330603 * get_an_3() const { return ___an_3; }
	inline Animator_t2776330603 ** get_address_of_an_3() { return &___an_3; }
	inline void set_an_3(Animator_t2776330603 * value)
	{
		___an_3 = value;
		Il2CppCodeGenWriteBarrier(&___an_3, value);
	}

	inline static int32_t get_offset_of_tS_4() { return static_cast<int32_t>(offsetof(TutoTargetScript_t3239500984, ___tS_4)); }
	inline TweenScale_t2936666559 * get_tS_4() const { return ___tS_4; }
	inline TweenScale_t2936666559 ** get_address_of_tS_4() { return &___tS_4; }
	inline void set_tS_4(TweenScale_t2936666559 * value)
	{
		___tS_4 = value;
		Il2CppCodeGenWriteBarrier(&___tS_4, value);
	}

	inline static int32_t get_offset_of_initSize_5() { return static_cast<int32_t>(offsetof(TutoTargetScript_t3239500984, ___initSize_5)); }
	inline Vector3_t4282066566  get_initSize_5() const { return ___initSize_5; }
	inline Vector3_t4282066566 * get_address_of_initSize_5() { return &___initSize_5; }
	inline void set_initSize_5(Vector3_t4282066566  value)
	{
		___initSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
