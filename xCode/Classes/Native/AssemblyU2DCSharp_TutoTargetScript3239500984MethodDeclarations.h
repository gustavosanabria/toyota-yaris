﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutoTargetScript
struct TutoTargetScript_t3239500984;

#include "codegen/il2cpp-codegen.h"

// System.Void TutoTargetScript::.ctor()
extern "C"  void TutoTargetScript__ctor_m2986285283 (TutoTargetScript_t3239500984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoTargetScript::Awake()
extern "C"  void TutoTargetScript_Awake_m3223890502 (TutoTargetScript_t3239500984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoTargetScript::ChangeTargetTexture()
extern "C"  void TutoTargetScript_ChangeTargetTexture_m2084530171 (TutoTargetScript_t3239500984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutoTargetScript::Reset()
extern "C"  void TutoTargetScript_Reset_m632718224 (TutoTargetScript_t3239500984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
