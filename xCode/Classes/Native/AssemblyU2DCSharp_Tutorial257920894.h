﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
struct Vector3_t4282066566_marshaled_pinvoke;
struct Vector3_t4282066566_marshaled_com;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tutorial
struct  Tutorial_t257920894 
{
public:
	// System.Byte Tutorial::id
	uint8_t ___id_0;
	// UnityEngine.Vector3[] Tutorial::targetPos
	Vector3U5BU5D_t215400611* ___targetPos_1;
	// System.String[] Tutorial::steps_names
	StringU5BU5D_t4054002952* ___steps_names_2;
	// System.String Tutorial::clips_prefix
	String_t* ___clips_prefix_3;
	// System.Int32 Tutorial::clips_startIndex
	int32_t ___clips_startIndex_4;
	// System.Int32 Tutorial::clips_endIndex
	int32_t ___clips_endIndex_5;
	// System.Byte Tutorial::automaticIndex
	uint8_t ___automaticIndex_6;
	// System.Byte Tutorial::slidingIndex
	uint8_t ___slidingIndex_7;
	// System.Byte Tutorial::slideSteps
	uint8_t ___slideSteps_8;
	// System.String[] Tutorial::searchLabels
	StringU5BU5D_t4054002952* ___searchLabels_9;
	// System.String[] Tutorial::audio_paths
	StringU5BU5D_t4054002952* ___audio_paths_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___id_0)); }
	inline uint8_t get_id_0() const { return ___id_0; }
	inline uint8_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint8_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_targetPos_1() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___targetPos_1)); }
	inline Vector3U5BU5D_t215400611* get_targetPos_1() const { return ___targetPos_1; }
	inline Vector3U5BU5D_t215400611** get_address_of_targetPos_1() { return &___targetPos_1; }
	inline void set_targetPos_1(Vector3U5BU5D_t215400611* value)
	{
		___targetPos_1 = value;
		Il2CppCodeGenWriteBarrier(&___targetPos_1, value);
	}

	inline static int32_t get_offset_of_steps_names_2() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___steps_names_2)); }
	inline StringU5BU5D_t4054002952* get_steps_names_2() const { return ___steps_names_2; }
	inline StringU5BU5D_t4054002952** get_address_of_steps_names_2() { return &___steps_names_2; }
	inline void set_steps_names_2(StringU5BU5D_t4054002952* value)
	{
		___steps_names_2 = value;
		Il2CppCodeGenWriteBarrier(&___steps_names_2, value);
	}

	inline static int32_t get_offset_of_clips_prefix_3() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___clips_prefix_3)); }
	inline String_t* get_clips_prefix_3() const { return ___clips_prefix_3; }
	inline String_t** get_address_of_clips_prefix_3() { return &___clips_prefix_3; }
	inline void set_clips_prefix_3(String_t* value)
	{
		___clips_prefix_3 = value;
		Il2CppCodeGenWriteBarrier(&___clips_prefix_3, value);
	}

	inline static int32_t get_offset_of_clips_startIndex_4() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___clips_startIndex_4)); }
	inline int32_t get_clips_startIndex_4() const { return ___clips_startIndex_4; }
	inline int32_t* get_address_of_clips_startIndex_4() { return &___clips_startIndex_4; }
	inline void set_clips_startIndex_4(int32_t value)
	{
		___clips_startIndex_4 = value;
	}

	inline static int32_t get_offset_of_clips_endIndex_5() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___clips_endIndex_5)); }
	inline int32_t get_clips_endIndex_5() const { return ___clips_endIndex_5; }
	inline int32_t* get_address_of_clips_endIndex_5() { return &___clips_endIndex_5; }
	inline void set_clips_endIndex_5(int32_t value)
	{
		___clips_endIndex_5 = value;
	}

	inline static int32_t get_offset_of_automaticIndex_6() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___automaticIndex_6)); }
	inline uint8_t get_automaticIndex_6() const { return ___automaticIndex_6; }
	inline uint8_t* get_address_of_automaticIndex_6() { return &___automaticIndex_6; }
	inline void set_automaticIndex_6(uint8_t value)
	{
		___automaticIndex_6 = value;
	}

	inline static int32_t get_offset_of_slidingIndex_7() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___slidingIndex_7)); }
	inline uint8_t get_slidingIndex_7() const { return ___slidingIndex_7; }
	inline uint8_t* get_address_of_slidingIndex_7() { return &___slidingIndex_7; }
	inline void set_slidingIndex_7(uint8_t value)
	{
		___slidingIndex_7 = value;
	}

	inline static int32_t get_offset_of_slideSteps_8() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___slideSteps_8)); }
	inline uint8_t get_slideSteps_8() const { return ___slideSteps_8; }
	inline uint8_t* get_address_of_slideSteps_8() { return &___slideSteps_8; }
	inline void set_slideSteps_8(uint8_t value)
	{
		___slideSteps_8 = value;
	}

	inline static int32_t get_offset_of_searchLabels_9() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___searchLabels_9)); }
	inline StringU5BU5D_t4054002952* get_searchLabels_9() const { return ___searchLabels_9; }
	inline StringU5BU5D_t4054002952** get_address_of_searchLabels_9() { return &___searchLabels_9; }
	inline void set_searchLabels_9(StringU5BU5D_t4054002952* value)
	{
		___searchLabels_9 = value;
		Il2CppCodeGenWriteBarrier(&___searchLabels_9, value);
	}

	inline static int32_t get_offset_of_audio_paths_10() { return static_cast<int32_t>(offsetof(Tutorial_t257920894, ___audio_paths_10)); }
	inline StringU5BU5D_t4054002952* get_audio_paths_10() const { return ___audio_paths_10; }
	inline StringU5BU5D_t4054002952** get_address_of_audio_paths_10() { return &___audio_paths_10; }
	inline void set_audio_paths_10(StringU5BU5D_t4054002952* value)
	{
		___audio_paths_10 = value;
		Il2CppCodeGenWriteBarrier(&___audio_paths_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Tutorial
struct Tutorial_t257920894_marshaled_pinvoke
{
	uint8_t ___id_0;
	Vector3_t4282066566_marshaled_pinvoke* ___targetPos_1;
	char** ___steps_names_2;
	char* ___clips_prefix_3;
	int32_t ___clips_startIndex_4;
	int32_t ___clips_endIndex_5;
	uint8_t ___automaticIndex_6;
	uint8_t ___slidingIndex_7;
	uint8_t ___slideSteps_8;
	char** ___searchLabels_9;
	char** ___audio_paths_10;
};
// Native definition for marshalling of: Tutorial
struct Tutorial_t257920894_marshaled_com
{
	uint8_t ___id_0;
	Vector3_t4282066566_marshaled_com* ___targetPos_1;
	Il2CppChar** ___steps_names_2;
	Il2CppChar* ___clips_prefix_3;
	int32_t ___clips_startIndex_4;
	int32_t ___clips_endIndex_5;
	uint8_t ___automaticIndex_6;
	uint8_t ___slidingIndex_7;
	uint8_t ___slideSteps_8;
	Il2CppChar** ___searchLabels_9;
	Il2CppChar** ___audio_paths_10;
};
