﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// System.String[]
struct StringU5BU5D_t4054002952;
// Tutorial
struct Tutorial_t257920894;
struct Tutorial_t257920894_marshaled_pinvoke;
struct Tutorial_t257920894_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"

// UnityEngine.Vector3[] Tutorial::GetTargetPos()
extern "C"  Vector3U5BU5D_t215400611* Tutorial_GetTargetPos_m340469164 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::GenerateAudioPaths()
extern "C"  void Tutorial_GenerateAudioPaths_m1195175188 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Tutorial::Count()
extern "C"  uint8_t Tutorial_Count_m1055393214 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Tutorial::LoadTexture(System.Int32)
extern "C"  Texture2D_t3884108195 * Tutorial_LoadTexture_m3610187652 (Tutorial_t257920894 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip Tutorial::LoadAudio(System.Int32)
extern "C"  AudioClip_t794140988 * Tutorial_LoadAudio_m2868246040 (Tutorial_t257920894 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Tutorial::GetAutomaticIndex()
extern "C"  uint8_t Tutorial_GetAutomaticIndex_m507271820 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Tutorial::GetSlidingIndex()
extern "C"  uint8_t Tutorial_GetSlidingIndex_m1471973929 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Tutorial::GetSlideSteps()
extern "C"  uint8_t Tutorial_GetSlideSteps_m2279171931 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Tutorial::GetSearchLabels()
extern "C"  StringU5BU5D_t4054002952* Tutorial_GetSearchLabels_m1572097943 (Tutorial_t257920894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Tutorial_t257920894;
struct Tutorial_t257920894_marshaled_pinvoke;

extern "C" void Tutorial_t257920894_marshal_pinvoke(const Tutorial_t257920894& unmarshaled, Tutorial_t257920894_marshaled_pinvoke& marshaled);
extern "C" void Tutorial_t257920894_marshal_pinvoke_back(const Tutorial_t257920894_marshaled_pinvoke& marshaled, Tutorial_t257920894& unmarshaled);
extern "C" void Tutorial_t257920894_marshal_pinvoke_cleanup(Tutorial_t257920894_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Tutorial_t257920894;
struct Tutorial_t257920894_marshaled_com;

extern "C" void Tutorial_t257920894_marshal_com(const Tutorial_t257920894& unmarshaled, Tutorial_t257920894_marshaled_com& marshaled);
extern "C" void Tutorial_t257920894_marshal_com_back(const Tutorial_t257920894_marshaled_com& marshaled, Tutorial_t257920894& unmarshaled);
extern "C" void Tutorial_t257920894_marshal_com_cleanup(Tutorial_t257920894_marshaled_com& marshaled);
