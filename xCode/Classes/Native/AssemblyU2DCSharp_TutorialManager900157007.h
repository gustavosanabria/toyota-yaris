﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UICamera
struct UICamera_t189364953;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Collider
struct Collider_t2939674232;
// TutorialsHandler
struct TutorialsHandler_t2864335061;
// UIScrollView
struct UIScrollView_t2113479878;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// TweenScale
struct TweenScale_t2936666559;
// UISprite
struct UISprite_t661437049;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager
struct  TutorialManager_t900157007  : public MonoBehaviour_t667441552
{
public:
	// UICamera TutorialManager::mainMenuCam
	UICamera_t189364953 * ___mainMenuCam_2;
	// UnityEngine.GameObject TutorialManager::tutorialBackground
	GameObject_t3674682005 * ___tutorialBackground_3;
	// UnityEngine.GameObject TutorialManager::tutoWarning
	GameObject_t3674682005 * ___tutoWarning_4;
	// UnityEngine.GameObject TutorialManager::phoneBtnGO
	GameObject_t3674682005 * ___phoneBtnGO_5;
	// UnityEngine.Texture2D TutorialManager::phoneBtnHL
	Texture2D_t3884108195 * ___phoneBtnHL_6;
	// UnityEngine.Texture2D TutorialManager::phoneBtn
	Texture2D_t3884108195 * ___phoneBtn_7;
	// UnityEngine.GameObject TutorialManager::gpsBtnGO
	GameObject_t3674682005 * ___gpsBtnGO_8;
	// UnityEngine.Texture2D TutorialManager::gpsBtnHL
	Texture2D_t3884108195 * ___gpsBtnHL_9;
	// UnityEngine.Texture2D TutorialManager::gpsBtn
	Texture2D_t3884108195 * ___gpsBtn_10;
	// UnityEngine.GameObject TutorialManager::applicationBtnGO
	GameObject_t3674682005 * ___applicationBtnGO_11;
	// UnityEngine.Texture2D TutorialManager::applicationBtnHL
	Texture2D_t3884108195 * ___applicationBtnHL_12;
	// UnityEngine.Texture2D TutorialManager::applicationBtn
	Texture2D_t3884108195 * ___applicationBtn_13;
	// UnityEngine.GameObject TutorialManager::radioBtnGO
	GameObject_t3674682005 * ___radioBtnGO_14;
	// UnityEngine.Texture2D TutorialManager::radioBtnHL
	Texture2D_t3884108195 * ___radioBtnHL_15;
	// UnityEngine.Texture2D TutorialManager::radioBtn
	Texture2D_t3884108195 * ___radioBtn_16;
	// UnityEngine.GameObject TutorialManager::warningContainer
	GameObject_t3674682005 * ___warningContainer_17;
	// UnityEngine.GameObject TutorialManager::categoriesContainer
	GameObject_t3674682005 * ___categoriesContainer_18;
	// UnityEngine.GameObject TutorialManager::phoneBtnsContainer
	GameObject_t3674682005 * ___phoneBtnsContainer_19;
	// UITexture TutorialManager::tutStepScreen
	UITexture_t3903132647 * ___tutStepScreen_20;
	// UnityEngine.Vector3[] TutorialManager::currentTutTargetPos
	Vector3U5BU5D_t215400611* ___currentTutTargetPos_21;
	// System.Byte TutorialManager::currentTutStepsCount
	uint8_t ___currentTutStepsCount_22;
	// System.Byte TutorialManager::currentTutAutomaticStepIndex
	uint8_t ___currentTutAutomaticStepIndex_23;
	// System.SByte TutorialManager::currentTutStepIndex
	int8_t ___currentTutStepIndex_24;
	// System.SByte TutorialManager::tutStepDir
	int8_t ___tutStepDir_25;
	// UnityEngine.GameObject TutorialManager::previousScreen
	GameObject_t3674682005 * ___previousScreen_26;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TutorialManager::screensList
	List_1_t747900261 * ___screensList_27;
	// System.Byte TutorialManager::screensCount
	uint8_t ___screensCount_28;
	// UnityEngine.GameObject TutorialManager::tutorialContainer
	GameObject_t3674682005 * ___tutorialContainer_29;
	// Tutorial TutorialManager::currentTut
	Tutorial_t257920894  ___currentTut_30;
	// System.Single TutorialManager::targetSpeed
	float ___targetSpeed_31;
	// UnityEngine.Transform TutorialManager::targetTrans
	Transform_t1659122786 * ___targetTrans_32;
	// UnityEngine.Sprite TutorialManager::targetHLTex
	Sprite_t3199167241 * ___targetHLTex_33;
	// UnityEngine.Collider TutorialManager::prevStepBtnCol
	Collider_t2939674232 * ___prevStepBtnCol_34;
	// UnityEngine.Collider TutorialManager::audioBtnCol
	Collider_t2939674232 * ___audioBtnCol_35;
	// UnityEngine.GameObject TutorialManager::menuBtn
	GameObject_t3674682005 * ___menuBtn_36;
	// TutorialsHandler TutorialManager::tutoHandler
	TutorialsHandler_t2864335061 * ___tutoHandler_37;
	// UIScrollView TutorialManager::timeLineScroll
	UIScrollView_t2113479878 * ___timeLineScroll_38;
	// System.Byte TutorialManager::slidingIndex
	uint8_t ___slidingIndex_39;
	// System.Byte TutorialManager::slideSteps
	uint8_t ___slideSteps_40;
	// UnityEngine.GameObject TutorialManager::nextScreen
	GameObject_t3674682005 * ___nextScreen_41;
	// System.Boolean TutorialManager::slidedForward
	bool ___slidedForward_42;
	// System.Boolean TutorialManager::slidedBackwards
	bool ___slidedBackwards_43;
	// UnityEngine.Collider[] TutorialManager::stepsCols
	ColliderU5BU5D_t2697150633* ___stepsCols_44;
	// System.Byte TutorialManager::stepsColsCount
	uint8_t ___stepsColsCount_45;
	// UnityEngine.AudioSource TutorialManager::audioSrc
	AudioSource_t1740077639 * ___audioSrc_46;
	// UnityEngine.Vector3 TutorialManager::originalTimeLineToPos
	Vector3_t4282066566  ___originalTimeLineToPos_47;
	// UnityEngine.Vector3 TutorialManager::originalTimeLineMaskTOPos
	Vector3_t4282066566  ___originalTimeLineMaskTOPos_48;
	// System.Boolean TutorialManager::deployTutMenu
	bool ___deployTutMenu_49;
	// TweenScale TutorialManager::tutMenuBtnsTween
	TweenScale_t2936666559 * ___tutMenuBtnsTween_50;
	// System.Single TutorialManager::automaticStepDelay
	float ___automaticStepDelay_51;
	// System.Single TutorialManager::scrollValue
	float ___scrollValue_52;
	// System.Single TutorialManager::timeLineMaskSlideValue
	float ___timeLineMaskSlideValue_53;
	// System.Single TutorialManager::slideSpeed
	float ___slideSpeed_54;
	// UnityEngine.Transform TutorialManager::timeLineTrans
	Transform_t1659122786 * ___timeLineTrans_55;
	// UnityEngine.Transform TutorialManager::timeLineMaskTrans
	Transform_t1659122786 * ___timeLineMaskTrans_56;
	// UnityEngine.Vector3 TutorialManager::timeLineMaskTOPos
	Vector3_t4282066566  ___timeLineMaskTOPos_57;
	// System.Boolean TutorialManager::updateTimeLine
	bool ___updateTimeLine_58;
	// System.Boolean TutorialManager::slideTimeLine
	bool ___slideTimeLine_59;
	// System.Boolean TutorialManager::toggleAudio
	bool ___toggleAudio_60;
	// UISprite TutorialManager::audioBtnSpr
	UISprite_t661437049 * ___audioBtnSpr_61;
	// UnityEngine.Vector3 TutorialManager::timeLineToPos
	Vector3_t4282066566  ___timeLineToPos_62;
	// TweenScale TutorialManager::twS
	TweenScale_t2936666559 * ___twS_63;

public:
	inline static int32_t get_offset_of_mainMenuCam_2() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___mainMenuCam_2)); }
	inline UICamera_t189364953 * get_mainMenuCam_2() const { return ___mainMenuCam_2; }
	inline UICamera_t189364953 ** get_address_of_mainMenuCam_2() { return &___mainMenuCam_2; }
	inline void set_mainMenuCam_2(UICamera_t189364953 * value)
	{
		___mainMenuCam_2 = value;
		Il2CppCodeGenWriteBarrier(&___mainMenuCam_2, value);
	}

	inline static int32_t get_offset_of_tutorialBackground_3() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutorialBackground_3)); }
	inline GameObject_t3674682005 * get_tutorialBackground_3() const { return ___tutorialBackground_3; }
	inline GameObject_t3674682005 ** get_address_of_tutorialBackground_3() { return &___tutorialBackground_3; }
	inline void set_tutorialBackground_3(GameObject_t3674682005 * value)
	{
		___tutorialBackground_3 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialBackground_3, value);
	}

	inline static int32_t get_offset_of_tutoWarning_4() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutoWarning_4)); }
	inline GameObject_t3674682005 * get_tutoWarning_4() const { return ___tutoWarning_4; }
	inline GameObject_t3674682005 ** get_address_of_tutoWarning_4() { return &___tutoWarning_4; }
	inline void set_tutoWarning_4(GameObject_t3674682005 * value)
	{
		___tutoWarning_4 = value;
		Il2CppCodeGenWriteBarrier(&___tutoWarning_4, value);
	}

	inline static int32_t get_offset_of_phoneBtnGO_5() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___phoneBtnGO_5)); }
	inline GameObject_t3674682005 * get_phoneBtnGO_5() const { return ___phoneBtnGO_5; }
	inline GameObject_t3674682005 ** get_address_of_phoneBtnGO_5() { return &___phoneBtnGO_5; }
	inline void set_phoneBtnGO_5(GameObject_t3674682005 * value)
	{
		___phoneBtnGO_5 = value;
		Il2CppCodeGenWriteBarrier(&___phoneBtnGO_5, value);
	}

	inline static int32_t get_offset_of_phoneBtnHL_6() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___phoneBtnHL_6)); }
	inline Texture2D_t3884108195 * get_phoneBtnHL_6() const { return ___phoneBtnHL_6; }
	inline Texture2D_t3884108195 ** get_address_of_phoneBtnHL_6() { return &___phoneBtnHL_6; }
	inline void set_phoneBtnHL_6(Texture2D_t3884108195 * value)
	{
		___phoneBtnHL_6 = value;
		Il2CppCodeGenWriteBarrier(&___phoneBtnHL_6, value);
	}

	inline static int32_t get_offset_of_phoneBtn_7() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___phoneBtn_7)); }
	inline Texture2D_t3884108195 * get_phoneBtn_7() const { return ___phoneBtn_7; }
	inline Texture2D_t3884108195 ** get_address_of_phoneBtn_7() { return &___phoneBtn_7; }
	inline void set_phoneBtn_7(Texture2D_t3884108195 * value)
	{
		___phoneBtn_7 = value;
		Il2CppCodeGenWriteBarrier(&___phoneBtn_7, value);
	}

	inline static int32_t get_offset_of_gpsBtnGO_8() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___gpsBtnGO_8)); }
	inline GameObject_t3674682005 * get_gpsBtnGO_8() const { return ___gpsBtnGO_8; }
	inline GameObject_t3674682005 ** get_address_of_gpsBtnGO_8() { return &___gpsBtnGO_8; }
	inline void set_gpsBtnGO_8(GameObject_t3674682005 * value)
	{
		___gpsBtnGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___gpsBtnGO_8, value);
	}

	inline static int32_t get_offset_of_gpsBtnHL_9() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___gpsBtnHL_9)); }
	inline Texture2D_t3884108195 * get_gpsBtnHL_9() const { return ___gpsBtnHL_9; }
	inline Texture2D_t3884108195 ** get_address_of_gpsBtnHL_9() { return &___gpsBtnHL_9; }
	inline void set_gpsBtnHL_9(Texture2D_t3884108195 * value)
	{
		___gpsBtnHL_9 = value;
		Il2CppCodeGenWriteBarrier(&___gpsBtnHL_9, value);
	}

	inline static int32_t get_offset_of_gpsBtn_10() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___gpsBtn_10)); }
	inline Texture2D_t3884108195 * get_gpsBtn_10() const { return ___gpsBtn_10; }
	inline Texture2D_t3884108195 ** get_address_of_gpsBtn_10() { return &___gpsBtn_10; }
	inline void set_gpsBtn_10(Texture2D_t3884108195 * value)
	{
		___gpsBtn_10 = value;
		Il2CppCodeGenWriteBarrier(&___gpsBtn_10, value);
	}

	inline static int32_t get_offset_of_applicationBtnGO_11() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___applicationBtnGO_11)); }
	inline GameObject_t3674682005 * get_applicationBtnGO_11() const { return ___applicationBtnGO_11; }
	inline GameObject_t3674682005 ** get_address_of_applicationBtnGO_11() { return &___applicationBtnGO_11; }
	inline void set_applicationBtnGO_11(GameObject_t3674682005 * value)
	{
		___applicationBtnGO_11 = value;
		Il2CppCodeGenWriteBarrier(&___applicationBtnGO_11, value);
	}

	inline static int32_t get_offset_of_applicationBtnHL_12() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___applicationBtnHL_12)); }
	inline Texture2D_t3884108195 * get_applicationBtnHL_12() const { return ___applicationBtnHL_12; }
	inline Texture2D_t3884108195 ** get_address_of_applicationBtnHL_12() { return &___applicationBtnHL_12; }
	inline void set_applicationBtnHL_12(Texture2D_t3884108195 * value)
	{
		___applicationBtnHL_12 = value;
		Il2CppCodeGenWriteBarrier(&___applicationBtnHL_12, value);
	}

	inline static int32_t get_offset_of_applicationBtn_13() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___applicationBtn_13)); }
	inline Texture2D_t3884108195 * get_applicationBtn_13() const { return ___applicationBtn_13; }
	inline Texture2D_t3884108195 ** get_address_of_applicationBtn_13() { return &___applicationBtn_13; }
	inline void set_applicationBtn_13(Texture2D_t3884108195 * value)
	{
		___applicationBtn_13 = value;
		Il2CppCodeGenWriteBarrier(&___applicationBtn_13, value);
	}

	inline static int32_t get_offset_of_radioBtnGO_14() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___radioBtnGO_14)); }
	inline GameObject_t3674682005 * get_radioBtnGO_14() const { return ___radioBtnGO_14; }
	inline GameObject_t3674682005 ** get_address_of_radioBtnGO_14() { return &___radioBtnGO_14; }
	inline void set_radioBtnGO_14(GameObject_t3674682005 * value)
	{
		___radioBtnGO_14 = value;
		Il2CppCodeGenWriteBarrier(&___radioBtnGO_14, value);
	}

	inline static int32_t get_offset_of_radioBtnHL_15() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___radioBtnHL_15)); }
	inline Texture2D_t3884108195 * get_radioBtnHL_15() const { return ___radioBtnHL_15; }
	inline Texture2D_t3884108195 ** get_address_of_radioBtnHL_15() { return &___radioBtnHL_15; }
	inline void set_radioBtnHL_15(Texture2D_t3884108195 * value)
	{
		___radioBtnHL_15 = value;
		Il2CppCodeGenWriteBarrier(&___radioBtnHL_15, value);
	}

	inline static int32_t get_offset_of_radioBtn_16() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___radioBtn_16)); }
	inline Texture2D_t3884108195 * get_radioBtn_16() const { return ___radioBtn_16; }
	inline Texture2D_t3884108195 ** get_address_of_radioBtn_16() { return &___radioBtn_16; }
	inline void set_radioBtn_16(Texture2D_t3884108195 * value)
	{
		___radioBtn_16 = value;
		Il2CppCodeGenWriteBarrier(&___radioBtn_16, value);
	}

	inline static int32_t get_offset_of_warningContainer_17() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___warningContainer_17)); }
	inline GameObject_t3674682005 * get_warningContainer_17() const { return ___warningContainer_17; }
	inline GameObject_t3674682005 ** get_address_of_warningContainer_17() { return &___warningContainer_17; }
	inline void set_warningContainer_17(GameObject_t3674682005 * value)
	{
		___warningContainer_17 = value;
		Il2CppCodeGenWriteBarrier(&___warningContainer_17, value);
	}

	inline static int32_t get_offset_of_categoriesContainer_18() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___categoriesContainer_18)); }
	inline GameObject_t3674682005 * get_categoriesContainer_18() const { return ___categoriesContainer_18; }
	inline GameObject_t3674682005 ** get_address_of_categoriesContainer_18() { return &___categoriesContainer_18; }
	inline void set_categoriesContainer_18(GameObject_t3674682005 * value)
	{
		___categoriesContainer_18 = value;
		Il2CppCodeGenWriteBarrier(&___categoriesContainer_18, value);
	}

	inline static int32_t get_offset_of_phoneBtnsContainer_19() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___phoneBtnsContainer_19)); }
	inline GameObject_t3674682005 * get_phoneBtnsContainer_19() const { return ___phoneBtnsContainer_19; }
	inline GameObject_t3674682005 ** get_address_of_phoneBtnsContainer_19() { return &___phoneBtnsContainer_19; }
	inline void set_phoneBtnsContainer_19(GameObject_t3674682005 * value)
	{
		___phoneBtnsContainer_19 = value;
		Il2CppCodeGenWriteBarrier(&___phoneBtnsContainer_19, value);
	}

	inline static int32_t get_offset_of_tutStepScreen_20() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutStepScreen_20)); }
	inline UITexture_t3903132647 * get_tutStepScreen_20() const { return ___tutStepScreen_20; }
	inline UITexture_t3903132647 ** get_address_of_tutStepScreen_20() { return &___tutStepScreen_20; }
	inline void set_tutStepScreen_20(UITexture_t3903132647 * value)
	{
		___tutStepScreen_20 = value;
		Il2CppCodeGenWriteBarrier(&___tutStepScreen_20, value);
	}

	inline static int32_t get_offset_of_currentTutTargetPos_21() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTutTargetPos_21)); }
	inline Vector3U5BU5D_t215400611* get_currentTutTargetPos_21() const { return ___currentTutTargetPos_21; }
	inline Vector3U5BU5D_t215400611** get_address_of_currentTutTargetPos_21() { return &___currentTutTargetPos_21; }
	inline void set_currentTutTargetPos_21(Vector3U5BU5D_t215400611* value)
	{
		___currentTutTargetPos_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentTutTargetPos_21, value);
	}

	inline static int32_t get_offset_of_currentTutStepsCount_22() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTutStepsCount_22)); }
	inline uint8_t get_currentTutStepsCount_22() const { return ___currentTutStepsCount_22; }
	inline uint8_t* get_address_of_currentTutStepsCount_22() { return &___currentTutStepsCount_22; }
	inline void set_currentTutStepsCount_22(uint8_t value)
	{
		___currentTutStepsCount_22 = value;
	}

	inline static int32_t get_offset_of_currentTutAutomaticStepIndex_23() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTutAutomaticStepIndex_23)); }
	inline uint8_t get_currentTutAutomaticStepIndex_23() const { return ___currentTutAutomaticStepIndex_23; }
	inline uint8_t* get_address_of_currentTutAutomaticStepIndex_23() { return &___currentTutAutomaticStepIndex_23; }
	inline void set_currentTutAutomaticStepIndex_23(uint8_t value)
	{
		___currentTutAutomaticStepIndex_23 = value;
	}

	inline static int32_t get_offset_of_currentTutStepIndex_24() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTutStepIndex_24)); }
	inline int8_t get_currentTutStepIndex_24() const { return ___currentTutStepIndex_24; }
	inline int8_t* get_address_of_currentTutStepIndex_24() { return &___currentTutStepIndex_24; }
	inline void set_currentTutStepIndex_24(int8_t value)
	{
		___currentTutStepIndex_24 = value;
	}

	inline static int32_t get_offset_of_tutStepDir_25() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutStepDir_25)); }
	inline int8_t get_tutStepDir_25() const { return ___tutStepDir_25; }
	inline int8_t* get_address_of_tutStepDir_25() { return &___tutStepDir_25; }
	inline void set_tutStepDir_25(int8_t value)
	{
		___tutStepDir_25 = value;
	}

	inline static int32_t get_offset_of_previousScreen_26() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___previousScreen_26)); }
	inline GameObject_t3674682005 * get_previousScreen_26() const { return ___previousScreen_26; }
	inline GameObject_t3674682005 ** get_address_of_previousScreen_26() { return &___previousScreen_26; }
	inline void set_previousScreen_26(GameObject_t3674682005 * value)
	{
		___previousScreen_26 = value;
		Il2CppCodeGenWriteBarrier(&___previousScreen_26, value);
	}

	inline static int32_t get_offset_of_screensList_27() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___screensList_27)); }
	inline List_1_t747900261 * get_screensList_27() const { return ___screensList_27; }
	inline List_1_t747900261 ** get_address_of_screensList_27() { return &___screensList_27; }
	inline void set_screensList_27(List_1_t747900261 * value)
	{
		___screensList_27 = value;
		Il2CppCodeGenWriteBarrier(&___screensList_27, value);
	}

	inline static int32_t get_offset_of_screensCount_28() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___screensCount_28)); }
	inline uint8_t get_screensCount_28() const { return ___screensCount_28; }
	inline uint8_t* get_address_of_screensCount_28() { return &___screensCount_28; }
	inline void set_screensCount_28(uint8_t value)
	{
		___screensCount_28 = value;
	}

	inline static int32_t get_offset_of_tutorialContainer_29() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutorialContainer_29)); }
	inline GameObject_t3674682005 * get_tutorialContainer_29() const { return ___tutorialContainer_29; }
	inline GameObject_t3674682005 ** get_address_of_tutorialContainer_29() { return &___tutorialContainer_29; }
	inline void set_tutorialContainer_29(GameObject_t3674682005 * value)
	{
		___tutorialContainer_29 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialContainer_29, value);
	}

	inline static int32_t get_offset_of_currentTut_30() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTut_30)); }
	inline Tutorial_t257920894  get_currentTut_30() const { return ___currentTut_30; }
	inline Tutorial_t257920894 * get_address_of_currentTut_30() { return &___currentTut_30; }
	inline void set_currentTut_30(Tutorial_t257920894  value)
	{
		___currentTut_30 = value;
	}

	inline static int32_t get_offset_of_targetSpeed_31() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___targetSpeed_31)); }
	inline float get_targetSpeed_31() const { return ___targetSpeed_31; }
	inline float* get_address_of_targetSpeed_31() { return &___targetSpeed_31; }
	inline void set_targetSpeed_31(float value)
	{
		___targetSpeed_31 = value;
	}

	inline static int32_t get_offset_of_targetTrans_32() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___targetTrans_32)); }
	inline Transform_t1659122786 * get_targetTrans_32() const { return ___targetTrans_32; }
	inline Transform_t1659122786 ** get_address_of_targetTrans_32() { return &___targetTrans_32; }
	inline void set_targetTrans_32(Transform_t1659122786 * value)
	{
		___targetTrans_32 = value;
		Il2CppCodeGenWriteBarrier(&___targetTrans_32, value);
	}

	inline static int32_t get_offset_of_targetHLTex_33() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___targetHLTex_33)); }
	inline Sprite_t3199167241 * get_targetHLTex_33() const { return ___targetHLTex_33; }
	inline Sprite_t3199167241 ** get_address_of_targetHLTex_33() { return &___targetHLTex_33; }
	inline void set_targetHLTex_33(Sprite_t3199167241 * value)
	{
		___targetHLTex_33 = value;
		Il2CppCodeGenWriteBarrier(&___targetHLTex_33, value);
	}

	inline static int32_t get_offset_of_prevStepBtnCol_34() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___prevStepBtnCol_34)); }
	inline Collider_t2939674232 * get_prevStepBtnCol_34() const { return ___prevStepBtnCol_34; }
	inline Collider_t2939674232 ** get_address_of_prevStepBtnCol_34() { return &___prevStepBtnCol_34; }
	inline void set_prevStepBtnCol_34(Collider_t2939674232 * value)
	{
		___prevStepBtnCol_34 = value;
		Il2CppCodeGenWriteBarrier(&___prevStepBtnCol_34, value);
	}

	inline static int32_t get_offset_of_audioBtnCol_35() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___audioBtnCol_35)); }
	inline Collider_t2939674232 * get_audioBtnCol_35() const { return ___audioBtnCol_35; }
	inline Collider_t2939674232 ** get_address_of_audioBtnCol_35() { return &___audioBtnCol_35; }
	inline void set_audioBtnCol_35(Collider_t2939674232 * value)
	{
		___audioBtnCol_35 = value;
		Il2CppCodeGenWriteBarrier(&___audioBtnCol_35, value);
	}

	inline static int32_t get_offset_of_menuBtn_36() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___menuBtn_36)); }
	inline GameObject_t3674682005 * get_menuBtn_36() const { return ___menuBtn_36; }
	inline GameObject_t3674682005 ** get_address_of_menuBtn_36() { return &___menuBtn_36; }
	inline void set_menuBtn_36(GameObject_t3674682005 * value)
	{
		___menuBtn_36 = value;
		Il2CppCodeGenWriteBarrier(&___menuBtn_36, value);
	}

	inline static int32_t get_offset_of_tutoHandler_37() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutoHandler_37)); }
	inline TutorialsHandler_t2864335061 * get_tutoHandler_37() const { return ___tutoHandler_37; }
	inline TutorialsHandler_t2864335061 ** get_address_of_tutoHandler_37() { return &___tutoHandler_37; }
	inline void set_tutoHandler_37(TutorialsHandler_t2864335061 * value)
	{
		___tutoHandler_37 = value;
		Il2CppCodeGenWriteBarrier(&___tutoHandler_37, value);
	}

	inline static int32_t get_offset_of_timeLineScroll_38() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___timeLineScroll_38)); }
	inline UIScrollView_t2113479878 * get_timeLineScroll_38() const { return ___timeLineScroll_38; }
	inline UIScrollView_t2113479878 ** get_address_of_timeLineScroll_38() { return &___timeLineScroll_38; }
	inline void set_timeLineScroll_38(UIScrollView_t2113479878 * value)
	{
		___timeLineScroll_38 = value;
		Il2CppCodeGenWriteBarrier(&___timeLineScroll_38, value);
	}

	inline static int32_t get_offset_of_slidingIndex_39() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___slidingIndex_39)); }
	inline uint8_t get_slidingIndex_39() const { return ___slidingIndex_39; }
	inline uint8_t* get_address_of_slidingIndex_39() { return &___slidingIndex_39; }
	inline void set_slidingIndex_39(uint8_t value)
	{
		___slidingIndex_39 = value;
	}

	inline static int32_t get_offset_of_slideSteps_40() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___slideSteps_40)); }
	inline uint8_t get_slideSteps_40() const { return ___slideSteps_40; }
	inline uint8_t* get_address_of_slideSteps_40() { return &___slideSteps_40; }
	inline void set_slideSteps_40(uint8_t value)
	{
		___slideSteps_40 = value;
	}

	inline static int32_t get_offset_of_nextScreen_41() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___nextScreen_41)); }
	inline GameObject_t3674682005 * get_nextScreen_41() const { return ___nextScreen_41; }
	inline GameObject_t3674682005 ** get_address_of_nextScreen_41() { return &___nextScreen_41; }
	inline void set_nextScreen_41(GameObject_t3674682005 * value)
	{
		___nextScreen_41 = value;
		Il2CppCodeGenWriteBarrier(&___nextScreen_41, value);
	}

	inline static int32_t get_offset_of_slidedForward_42() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___slidedForward_42)); }
	inline bool get_slidedForward_42() const { return ___slidedForward_42; }
	inline bool* get_address_of_slidedForward_42() { return &___slidedForward_42; }
	inline void set_slidedForward_42(bool value)
	{
		___slidedForward_42 = value;
	}

	inline static int32_t get_offset_of_slidedBackwards_43() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___slidedBackwards_43)); }
	inline bool get_slidedBackwards_43() const { return ___slidedBackwards_43; }
	inline bool* get_address_of_slidedBackwards_43() { return &___slidedBackwards_43; }
	inline void set_slidedBackwards_43(bool value)
	{
		___slidedBackwards_43 = value;
	}

	inline static int32_t get_offset_of_stepsCols_44() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___stepsCols_44)); }
	inline ColliderU5BU5D_t2697150633* get_stepsCols_44() const { return ___stepsCols_44; }
	inline ColliderU5BU5D_t2697150633** get_address_of_stepsCols_44() { return &___stepsCols_44; }
	inline void set_stepsCols_44(ColliderU5BU5D_t2697150633* value)
	{
		___stepsCols_44 = value;
		Il2CppCodeGenWriteBarrier(&___stepsCols_44, value);
	}

	inline static int32_t get_offset_of_stepsColsCount_45() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___stepsColsCount_45)); }
	inline uint8_t get_stepsColsCount_45() const { return ___stepsColsCount_45; }
	inline uint8_t* get_address_of_stepsColsCount_45() { return &___stepsColsCount_45; }
	inline void set_stepsColsCount_45(uint8_t value)
	{
		___stepsColsCount_45 = value;
	}

	inline static int32_t get_offset_of_audioSrc_46() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___audioSrc_46)); }
	inline AudioSource_t1740077639 * get_audioSrc_46() const { return ___audioSrc_46; }
	inline AudioSource_t1740077639 ** get_address_of_audioSrc_46() { return &___audioSrc_46; }
	inline void set_audioSrc_46(AudioSource_t1740077639 * value)
	{
		___audioSrc_46 = value;
		Il2CppCodeGenWriteBarrier(&___audioSrc_46, value);
	}

	inline static int32_t get_offset_of_originalTimeLineToPos_47() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___originalTimeLineToPos_47)); }
	inline Vector3_t4282066566  get_originalTimeLineToPos_47() const { return ___originalTimeLineToPos_47; }
	inline Vector3_t4282066566 * get_address_of_originalTimeLineToPos_47() { return &___originalTimeLineToPos_47; }
	inline void set_originalTimeLineToPos_47(Vector3_t4282066566  value)
	{
		___originalTimeLineToPos_47 = value;
	}

	inline static int32_t get_offset_of_originalTimeLineMaskTOPos_48() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___originalTimeLineMaskTOPos_48)); }
	inline Vector3_t4282066566  get_originalTimeLineMaskTOPos_48() const { return ___originalTimeLineMaskTOPos_48; }
	inline Vector3_t4282066566 * get_address_of_originalTimeLineMaskTOPos_48() { return &___originalTimeLineMaskTOPos_48; }
	inline void set_originalTimeLineMaskTOPos_48(Vector3_t4282066566  value)
	{
		___originalTimeLineMaskTOPos_48 = value;
	}

	inline static int32_t get_offset_of_deployTutMenu_49() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___deployTutMenu_49)); }
	inline bool get_deployTutMenu_49() const { return ___deployTutMenu_49; }
	inline bool* get_address_of_deployTutMenu_49() { return &___deployTutMenu_49; }
	inline void set_deployTutMenu_49(bool value)
	{
		___deployTutMenu_49 = value;
	}

	inline static int32_t get_offset_of_tutMenuBtnsTween_50() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutMenuBtnsTween_50)); }
	inline TweenScale_t2936666559 * get_tutMenuBtnsTween_50() const { return ___tutMenuBtnsTween_50; }
	inline TweenScale_t2936666559 ** get_address_of_tutMenuBtnsTween_50() { return &___tutMenuBtnsTween_50; }
	inline void set_tutMenuBtnsTween_50(TweenScale_t2936666559 * value)
	{
		___tutMenuBtnsTween_50 = value;
		Il2CppCodeGenWriteBarrier(&___tutMenuBtnsTween_50, value);
	}

	inline static int32_t get_offset_of_automaticStepDelay_51() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___automaticStepDelay_51)); }
	inline float get_automaticStepDelay_51() const { return ___automaticStepDelay_51; }
	inline float* get_address_of_automaticStepDelay_51() { return &___automaticStepDelay_51; }
	inline void set_automaticStepDelay_51(float value)
	{
		___automaticStepDelay_51 = value;
	}

	inline static int32_t get_offset_of_scrollValue_52() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___scrollValue_52)); }
	inline float get_scrollValue_52() const { return ___scrollValue_52; }
	inline float* get_address_of_scrollValue_52() { return &___scrollValue_52; }
	inline void set_scrollValue_52(float value)
	{
		___scrollValue_52 = value;
	}

	inline static int32_t get_offset_of_timeLineMaskSlideValue_53() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___timeLineMaskSlideValue_53)); }
	inline float get_timeLineMaskSlideValue_53() const { return ___timeLineMaskSlideValue_53; }
	inline float* get_address_of_timeLineMaskSlideValue_53() { return &___timeLineMaskSlideValue_53; }
	inline void set_timeLineMaskSlideValue_53(float value)
	{
		___timeLineMaskSlideValue_53 = value;
	}

	inline static int32_t get_offset_of_slideSpeed_54() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___slideSpeed_54)); }
	inline float get_slideSpeed_54() const { return ___slideSpeed_54; }
	inline float* get_address_of_slideSpeed_54() { return &___slideSpeed_54; }
	inline void set_slideSpeed_54(float value)
	{
		___slideSpeed_54 = value;
	}

	inline static int32_t get_offset_of_timeLineTrans_55() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___timeLineTrans_55)); }
	inline Transform_t1659122786 * get_timeLineTrans_55() const { return ___timeLineTrans_55; }
	inline Transform_t1659122786 ** get_address_of_timeLineTrans_55() { return &___timeLineTrans_55; }
	inline void set_timeLineTrans_55(Transform_t1659122786 * value)
	{
		___timeLineTrans_55 = value;
		Il2CppCodeGenWriteBarrier(&___timeLineTrans_55, value);
	}

	inline static int32_t get_offset_of_timeLineMaskTrans_56() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___timeLineMaskTrans_56)); }
	inline Transform_t1659122786 * get_timeLineMaskTrans_56() const { return ___timeLineMaskTrans_56; }
	inline Transform_t1659122786 ** get_address_of_timeLineMaskTrans_56() { return &___timeLineMaskTrans_56; }
	inline void set_timeLineMaskTrans_56(Transform_t1659122786 * value)
	{
		___timeLineMaskTrans_56 = value;
		Il2CppCodeGenWriteBarrier(&___timeLineMaskTrans_56, value);
	}

	inline static int32_t get_offset_of_timeLineMaskTOPos_57() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___timeLineMaskTOPos_57)); }
	inline Vector3_t4282066566  get_timeLineMaskTOPos_57() const { return ___timeLineMaskTOPos_57; }
	inline Vector3_t4282066566 * get_address_of_timeLineMaskTOPos_57() { return &___timeLineMaskTOPos_57; }
	inline void set_timeLineMaskTOPos_57(Vector3_t4282066566  value)
	{
		___timeLineMaskTOPos_57 = value;
	}

	inline static int32_t get_offset_of_updateTimeLine_58() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___updateTimeLine_58)); }
	inline bool get_updateTimeLine_58() const { return ___updateTimeLine_58; }
	inline bool* get_address_of_updateTimeLine_58() { return &___updateTimeLine_58; }
	inline void set_updateTimeLine_58(bool value)
	{
		___updateTimeLine_58 = value;
	}

	inline static int32_t get_offset_of_slideTimeLine_59() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___slideTimeLine_59)); }
	inline bool get_slideTimeLine_59() const { return ___slideTimeLine_59; }
	inline bool* get_address_of_slideTimeLine_59() { return &___slideTimeLine_59; }
	inline void set_slideTimeLine_59(bool value)
	{
		___slideTimeLine_59 = value;
	}

	inline static int32_t get_offset_of_toggleAudio_60() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___toggleAudio_60)); }
	inline bool get_toggleAudio_60() const { return ___toggleAudio_60; }
	inline bool* get_address_of_toggleAudio_60() { return &___toggleAudio_60; }
	inline void set_toggleAudio_60(bool value)
	{
		___toggleAudio_60 = value;
	}

	inline static int32_t get_offset_of_audioBtnSpr_61() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___audioBtnSpr_61)); }
	inline UISprite_t661437049 * get_audioBtnSpr_61() const { return ___audioBtnSpr_61; }
	inline UISprite_t661437049 ** get_address_of_audioBtnSpr_61() { return &___audioBtnSpr_61; }
	inline void set_audioBtnSpr_61(UISprite_t661437049 * value)
	{
		___audioBtnSpr_61 = value;
		Il2CppCodeGenWriteBarrier(&___audioBtnSpr_61, value);
	}

	inline static int32_t get_offset_of_timeLineToPos_62() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___timeLineToPos_62)); }
	inline Vector3_t4282066566  get_timeLineToPos_62() const { return ___timeLineToPos_62; }
	inline Vector3_t4282066566 * get_address_of_timeLineToPos_62() { return &___timeLineToPos_62; }
	inline void set_timeLineToPos_62(Vector3_t4282066566  value)
	{
		___timeLineToPos_62 = value;
	}

	inline static int32_t get_offset_of_twS_63() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___twS_63)); }
	inline TweenScale_t2936666559 * get_twS_63() const { return ___twS_63; }
	inline TweenScale_t2936666559 ** get_address_of_twS_63() { return &___twS_63; }
	inline void set_twS_63(TweenScale_t2936666559 * value)
	{
		___twS_63 = value;
		Il2CppCodeGenWriteBarrier(&___twS_63, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
