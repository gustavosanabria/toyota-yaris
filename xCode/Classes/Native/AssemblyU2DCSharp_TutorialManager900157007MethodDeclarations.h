﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialManager
struct TutorialManager_t900157007;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void TutorialManager::.ctor()
extern "C"  void TutorialManager__ctor_m3414303228 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::Start()
extern "C"  void TutorialManager_Start_m2361441020 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::SaveOriginalParameters()
extern "C"  void TutorialManager_SaveOriginalParameters_m2180892928 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::LoadParameters()
extern "C"  void TutorialManager_LoadParameters_m1988603512 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::TutorialMenuBtnClick(UnityEngine.GameObject)
extern "C"  void TutorialManager_TutorialMenuBtnClick_m3099429321 (TutorialManager_t900157007 * __this, GameObject_t3674682005 * ___buttonsContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::OpenUrlBtn()
extern "C"  void TutorialManager_OpenUrlBtn_m3101243935 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::JumpToGlossary()
extern "C"  void TutorialManager_JumpToGlossary_m3747727823 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::SetTutorial(System.Byte)
extern "C"  void TutorialManager_SetTutorial_m2447834353 (TutorialManager_t900157007 * __this, uint8_t ___tutID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::NextScreen(UnityEngine.GameObject)
extern "C"  void TutorialManager_NextScreen_m1689660479 (TutorialManager_t900157007 * __this, GameObject_t3674682005 * ___nextScr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::BackToPreviousScreen()
extern "C"  void TutorialManager_BackToPreviousScreen_m3502533933 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::CloseTutoWindow()
extern "C"  void TutorialManager_CloseTutoWindow_m1434682046 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::CloseTutAndGoBackToShit()
extern "C"  void TutorialManager_CloseTutAndGoBackToShit_m3267581056 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::CloseTutAndGoBackToPreviousScreen()
extern "C"  void TutorialManager_CloseTutAndGoBackToPreviousScreen_m4055061763 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::EnableWarning()
extern "C"  void TutorialManager_EnableWarning_m2812679283 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ShowTutorialCategories()
extern "C"  void TutorialManager_ShowTutorialCategories_m1176836159 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::OpenCategory()
extern "C"  void TutorialManager_OpenCategory_m1178814512 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::PhoneBtnClick(UnityEngine.GameObject)
extern "C"  void TutorialManager_PhoneBtnClick_m440529964 (TutorialManager_t900157007 * __this, GameObject_t3674682005 * ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ResetPhoneBtn()
extern "C"  void TutorialManager_ResetPhoneBtn_m1647157079 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::GPSBtnClick(UnityEngine.GameObject)
extern "C"  void TutorialManager_GPSBtnClick_m690394536 (TutorialManager_t900157007 * __this, GameObject_t3674682005 * ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ResetGPSBtn()
extern "C"  void TutorialManager_ResetGPSBtn_m967821211 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ApplicationBtnClick(UnityEngine.GameObject)
extern "C"  void TutorialManager_ApplicationBtnClick_m2629913934 (TutorialManager_t900157007 * __this, GameObject_t3674682005 * ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ResetApplicationBtn()
extern "C"  void TutorialManager_ResetApplicationBtn_m548830901 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::RadioBtnClick(UnityEngine.GameObject)
extern "C"  void TutorialManager_RadioBtnClick_m1632982713 (TutorialManager_t900157007 * __this, GameObject_t3674682005 * ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ResetRadionBtn()
extern "C"  void TutorialManager_ResetRadionBtn_m1380821378 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ResetAllScreens()
extern "C"  void TutorialManager_ResetAllScreens_m4006306287 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::GoToNextTutStep()
extern "C"  void TutorialManager_GoToNextTutStep_m2966669603 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::GoToPrevTutStep()
extern "C"  void TutorialManager_GoToPrevTutStep_m3447249379 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ChangeTutStep()
extern "C"  void TutorialManager_ChangeTutStep_m3115558921 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::JumpToStep(System.SByte)
extern "C"  void TutorialManager_JumpToStep_m2158852321 (TutorialManager_t900157007 * __this, int8_t ___pStepIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::SlideTimeLineMask(System.Single)
extern "C"  void TutorialManager_SlideTimeLineMask_m3735610195 (TutorialManager_t900157007 * __this, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::ToggleAudio()
extern "C"  void TutorialManager_ToggleAudio_m4263642300 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::PlayAudio()
extern "C"  void TutorialManager_PlayAudio_m3940129564 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::SlideTimeLine()
extern "C"  void TutorialManager_SlideTimeLine_m1743553228 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::SlideTimeLine(System.Single)
extern "C"  void TutorialManager_SlideTimeLine_m940993119 (TutorialManager_t900157007 * __this, float ___slideValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::Update()
extern "C"  void TutorialManager_Update_m196079761 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::EnableTarget(System.Boolean,System.Boolean)
extern "C"  void TutorialManager_EnableTarget_m3702480362 (TutorialManager_t900157007 * __this, bool ___anim0, bool ___collider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::AnimateReturnButton(System.Boolean)
extern "C"  void TutorialManager_AnimateReturnButton_m3471752020 (TutorialManager_t900157007 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::TargetClick()
extern "C"  void TutorialManager_TargetClick_m1531299089 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::DestroyMe()
extern "C"  void TutorialManager_DestroyMe_m3195560268 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
