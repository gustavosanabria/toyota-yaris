﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tutorial[]
struct TutorialU5BU5D_t2291760331;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialsHandler
struct  TutorialsHandler_t2864335061  : public MonoBehaviour_t667441552
{
public:
	// Tutorial[] TutorialsHandler::tutorials
	TutorialU5BU5D_t2291760331* ___tutorials_2;
	// System.Byte TutorialsHandler::tutorialsCount
	uint8_t ___tutorialsCount_3;

public:
	inline static int32_t get_offset_of_tutorials_2() { return static_cast<int32_t>(offsetof(TutorialsHandler_t2864335061, ___tutorials_2)); }
	inline TutorialU5BU5D_t2291760331* get_tutorials_2() const { return ___tutorials_2; }
	inline TutorialU5BU5D_t2291760331** get_address_of_tutorials_2() { return &___tutorials_2; }
	inline void set_tutorials_2(TutorialU5BU5D_t2291760331* value)
	{
		___tutorials_2 = value;
		Il2CppCodeGenWriteBarrier(&___tutorials_2, value);
	}

	inline static int32_t get_offset_of_tutorialsCount_3() { return static_cast<int32_t>(offsetof(TutorialsHandler_t2864335061, ___tutorialsCount_3)); }
	inline uint8_t get_tutorialsCount_3() const { return ___tutorialsCount_3; }
	inline uint8_t* get_address_of_tutorialsCount_3() { return &___tutorialsCount_3; }
	inline void set_tutorialsCount_3(uint8_t value)
	{
		___tutorialsCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
