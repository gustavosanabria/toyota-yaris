﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialsHandler
struct TutorialsHandler_t2864335061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"

// System.Void TutorialsHandler::.ctor()
extern "C"  void TutorialsHandler__ctor_m2546400614 (TutorialsHandler_t2864335061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialsHandler::Awake()
extern "C"  void TutorialsHandler_Awake_m2784005833 (TutorialsHandler_t2864335061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tutorial TutorialsHandler::GetTutorial(System.Byte)
extern "C"  Tutorial_t257920894  TutorialsHandler_GetTutorial_m3557412498 (TutorialsHandler_t2864335061 * __this, uint8_t ___pID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
