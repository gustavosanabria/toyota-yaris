﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialsIDS
struct  TutorialsIDS_t774177219  : public MonoBehaviour_t667441552
{
public:
	// System.SByte TutorialsIDS::TUT_STEP1
	int8_t ___TUT_STEP1_2;
	// System.SByte TutorialsIDS::TUT_STEP2
	int8_t ___TUT_STEP2_3;
	// System.SByte TutorialsIDS::TUT_STEP3
	int8_t ___TUT_STEP3_4;
	// System.SByte TutorialsIDS::TUT_STEP4
	int8_t ___TUT_STEP4_5;
	// System.SByte TutorialsIDS::TUT_STEP5
	int8_t ___TUT_STEP5_6;
	// System.SByte TutorialsIDS::TUT_STEP6
	int8_t ___TUT_STEP6_7;
	// System.SByte TutorialsIDS::TUT_STEP7
	int8_t ___TUT_STEP7_8;
	// System.SByte TutorialsIDS::TUT_STEP8
	int8_t ___TUT_STEP8_9;
	// System.SByte TutorialsIDS::TUT_STEP9
	int8_t ___TUT_STEP9_10;
	// System.SByte TutorialsIDS::TUT_STEP10
	int8_t ___TUT_STEP10_11;
	// System.SByte TutorialsIDS::TUT_STEP11
	int8_t ___TUT_STEP11_12;
	// System.SByte TutorialsIDS::TUT_STEP12
	int8_t ___TUT_STEP12_13;
	// System.SByte TutorialsIDS::TUT_STEP13
	int8_t ___TUT_STEP13_14;
	// System.Byte TutorialsIDS::TUT_PHONE_BLUETOOTH
	uint8_t ___TUT_PHONE_BLUETOOTH_15;
	// System.Byte TutorialsIDS::TUT_PHONE_INTERNET
	uint8_t ___TUT_PHONE_INTERNET_16;
	// System.Byte TutorialsIDS::TUT_PHONE_TUT_3
	uint8_t ___TUT_PHONE_TUT_3_17;
	// System.Byte TutorialsIDS::TUT_PHONE_TUT_4
	uint8_t ___TUT_PHONE_TUT_4_18;
	// System.Byte TutorialsIDS::TUT_GPS_TUT_1
	uint8_t ___TUT_GPS_TUT_1_19;
	// System.Byte TutorialsIDS::TUT_GPS_TUT_2
	uint8_t ___TUT_GPS_TUT_2_20;
	// System.Byte TutorialsIDS::TUT_GPS_TUT_3
	uint8_t ___TUT_GPS_TUT_3_21;
	// System.Byte TutorialsIDS::TUT_GPS_TUT_4
	uint8_t ___TUT_GPS_TUT_4_22;
	// System.Byte TutorialsIDS::TUT_GPS_TUT_5
	uint8_t ___TUT_GPS_TUT_5_23;
	// System.Byte TutorialsIDS::TUT_APP_TUT_1
	uint8_t ___TUT_APP_TUT_1_24;
	// System.Byte TutorialsIDS::TUT_RADIO_TUT_1
	uint8_t ___TUT_RADIO_TUT_1_25;
	// System.Byte TutorialsIDS::TUT_TFT_TUT_1
	uint8_t ___TUT_TFT_TUT_1_26;
	// System.Byte TutorialsIDS::TUT_TFT_TUT_2
	uint8_t ___TUT_TFT_TUT_2_27;
	// System.Byte TutorialsIDS::TUT_TFT_TUT_3
	uint8_t ___TUT_TFT_TUT_3_28;
	// System.Byte TutorialsIDS::TUT_TFT_TUT_4
	uint8_t ___TUT_TFT_TUT_4_29;
	// System.Byte TutorialsIDS::TUT_TFT_TUT_5
	uint8_t ___TUT_TFT_TUT_5_30;
	// System.Byte TutorialsIDS::TUT_TFT_TUT_6
	uint8_t ___TUT_TFT_TUT_6_31;

public:
	inline static int32_t get_offset_of_TUT_STEP1_2() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP1_2)); }
	inline int8_t get_TUT_STEP1_2() const { return ___TUT_STEP1_2; }
	inline int8_t* get_address_of_TUT_STEP1_2() { return &___TUT_STEP1_2; }
	inline void set_TUT_STEP1_2(int8_t value)
	{
		___TUT_STEP1_2 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP2_3() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP2_3)); }
	inline int8_t get_TUT_STEP2_3() const { return ___TUT_STEP2_3; }
	inline int8_t* get_address_of_TUT_STEP2_3() { return &___TUT_STEP2_3; }
	inline void set_TUT_STEP2_3(int8_t value)
	{
		___TUT_STEP2_3 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP3_4() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP3_4)); }
	inline int8_t get_TUT_STEP3_4() const { return ___TUT_STEP3_4; }
	inline int8_t* get_address_of_TUT_STEP3_4() { return &___TUT_STEP3_4; }
	inline void set_TUT_STEP3_4(int8_t value)
	{
		___TUT_STEP3_4 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP4_5() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP4_5)); }
	inline int8_t get_TUT_STEP4_5() const { return ___TUT_STEP4_5; }
	inline int8_t* get_address_of_TUT_STEP4_5() { return &___TUT_STEP4_5; }
	inline void set_TUT_STEP4_5(int8_t value)
	{
		___TUT_STEP4_5 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP5_6() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP5_6)); }
	inline int8_t get_TUT_STEP5_6() const { return ___TUT_STEP5_6; }
	inline int8_t* get_address_of_TUT_STEP5_6() { return &___TUT_STEP5_6; }
	inline void set_TUT_STEP5_6(int8_t value)
	{
		___TUT_STEP5_6 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP6_7() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP6_7)); }
	inline int8_t get_TUT_STEP6_7() const { return ___TUT_STEP6_7; }
	inline int8_t* get_address_of_TUT_STEP6_7() { return &___TUT_STEP6_7; }
	inline void set_TUT_STEP6_7(int8_t value)
	{
		___TUT_STEP6_7 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP7_8() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP7_8)); }
	inline int8_t get_TUT_STEP7_8() const { return ___TUT_STEP7_8; }
	inline int8_t* get_address_of_TUT_STEP7_8() { return &___TUT_STEP7_8; }
	inline void set_TUT_STEP7_8(int8_t value)
	{
		___TUT_STEP7_8 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP8_9() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP8_9)); }
	inline int8_t get_TUT_STEP8_9() const { return ___TUT_STEP8_9; }
	inline int8_t* get_address_of_TUT_STEP8_9() { return &___TUT_STEP8_9; }
	inline void set_TUT_STEP8_9(int8_t value)
	{
		___TUT_STEP8_9 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP9_10() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP9_10)); }
	inline int8_t get_TUT_STEP9_10() const { return ___TUT_STEP9_10; }
	inline int8_t* get_address_of_TUT_STEP9_10() { return &___TUT_STEP9_10; }
	inline void set_TUT_STEP9_10(int8_t value)
	{
		___TUT_STEP9_10 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP10_11() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP10_11)); }
	inline int8_t get_TUT_STEP10_11() const { return ___TUT_STEP10_11; }
	inline int8_t* get_address_of_TUT_STEP10_11() { return &___TUT_STEP10_11; }
	inline void set_TUT_STEP10_11(int8_t value)
	{
		___TUT_STEP10_11 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP11_12() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP11_12)); }
	inline int8_t get_TUT_STEP11_12() const { return ___TUT_STEP11_12; }
	inline int8_t* get_address_of_TUT_STEP11_12() { return &___TUT_STEP11_12; }
	inline void set_TUT_STEP11_12(int8_t value)
	{
		___TUT_STEP11_12 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP12_13() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP12_13)); }
	inline int8_t get_TUT_STEP12_13() const { return ___TUT_STEP12_13; }
	inline int8_t* get_address_of_TUT_STEP12_13() { return &___TUT_STEP12_13; }
	inline void set_TUT_STEP12_13(int8_t value)
	{
		___TUT_STEP12_13 = value;
	}

	inline static int32_t get_offset_of_TUT_STEP13_14() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_STEP13_14)); }
	inline int8_t get_TUT_STEP13_14() const { return ___TUT_STEP13_14; }
	inline int8_t* get_address_of_TUT_STEP13_14() { return &___TUT_STEP13_14; }
	inline void set_TUT_STEP13_14(int8_t value)
	{
		___TUT_STEP13_14 = value;
	}

	inline static int32_t get_offset_of_TUT_PHONE_BLUETOOTH_15() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_PHONE_BLUETOOTH_15)); }
	inline uint8_t get_TUT_PHONE_BLUETOOTH_15() const { return ___TUT_PHONE_BLUETOOTH_15; }
	inline uint8_t* get_address_of_TUT_PHONE_BLUETOOTH_15() { return &___TUT_PHONE_BLUETOOTH_15; }
	inline void set_TUT_PHONE_BLUETOOTH_15(uint8_t value)
	{
		___TUT_PHONE_BLUETOOTH_15 = value;
	}

	inline static int32_t get_offset_of_TUT_PHONE_INTERNET_16() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_PHONE_INTERNET_16)); }
	inline uint8_t get_TUT_PHONE_INTERNET_16() const { return ___TUT_PHONE_INTERNET_16; }
	inline uint8_t* get_address_of_TUT_PHONE_INTERNET_16() { return &___TUT_PHONE_INTERNET_16; }
	inline void set_TUT_PHONE_INTERNET_16(uint8_t value)
	{
		___TUT_PHONE_INTERNET_16 = value;
	}

	inline static int32_t get_offset_of_TUT_PHONE_TUT_3_17() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_PHONE_TUT_3_17)); }
	inline uint8_t get_TUT_PHONE_TUT_3_17() const { return ___TUT_PHONE_TUT_3_17; }
	inline uint8_t* get_address_of_TUT_PHONE_TUT_3_17() { return &___TUT_PHONE_TUT_3_17; }
	inline void set_TUT_PHONE_TUT_3_17(uint8_t value)
	{
		___TUT_PHONE_TUT_3_17 = value;
	}

	inline static int32_t get_offset_of_TUT_PHONE_TUT_4_18() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_PHONE_TUT_4_18)); }
	inline uint8_t get_TUT_PHONE_TUT_4_18() const { return ___TUT_PHONE_TUT_4_18; }
	inline uint8_t* get_address_of_TUT_PHONE_TUT_4_18() { return &___TUT_PHONE_TUT_4_18; }
	inline void set_TUT_PHONE_TUT_4_18(uint8_t value)
	{
		___TUT_PHONE_TUT_4_18 = value;
	}

	inline static int32_t get_offset_of_TUT_GPS_TUT_1_19() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_GPS_TUT_1_19)); }
	inline uint8_t get_TUT_GPS_TUT_1_19() const { return ___TUT_GPS_TUT_1_19; }
	inline uint8_t* get_address_of_TUT_GPS_TUT_1_19() { return &___TUT_GPS_TUT_1_19; }
	inline void set_TUT_GPS_TUT_1_19(uint8_t value)
	{
		___TUT_GPS_TUT_1_19 = value;
	}

	inline static int32_t get_offset_of_TUT_GPS_TUT_2_20() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_GPS_TUT_2_20)); }
	inline uint8_t get_TUT_GPS_TUT_2_20() const { return ___TUT_GPS_TUT_2_20; }
	inline uint8_t* get_address_of_TUT_GPS_TUT_2_20() { return &___TUT_GPS_TUT_2_20; }
	inline void set_TUT_GPS_TUT_2_20(uint8_t value)
	{
		___TUT_GPS_TUT_2_20 = value;
	}

	inline static int32_t get_offset_of_TUT_GPS_TUT_3_21() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_GPS_TUT_3_21)); }
	inline uint8_t get_TUT_GPS_TUT_3_21() const { return ___TUT_GPS_TUT_3_21; }
	inline uint8_t* get_address_of_TUT_GPS_TUT_3_21() { return &___TUT_GPS_TUT_3_21; }
	inline void set_TUT_GPS_TUT_3_21(uint8_t value)
	{
		___TUT_GPS_TUT_3_21 = value;
	}

	inline static int32_t get_offset_of_TUT_GPS_TUT_4_22() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_GPS_TUT_4_22)); }
	inline uint8_t get_TUT_GPS_TUT_4_22() const { return ___TUT_GPS_TUT_4_22; }
	inline uint8_t* get_address_of_TUT_GPS_TUT_4_22() { return &___TUT_GPS_TUT_4_22; }
	inline void set_TUT_GPS_TUT_4_22(uint8_t value)
	{
		___TUT_GPS_TUT_4_22 = value;
	}

	inline static int32_t get_offset_of_TUT_GPS_TUT_5_23() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_GPS_TUT_5_23)); }
	inline uint8_t get_TUT_GPS_TUT_5_23() const { return ___TUT_GPS_TUT_5_23; }
	inline uint8_t* get_address_of_TUT_GPS_TUT_5_23() { return &___TUT_GPS_TUT_5_23; }
	inline void set_TUT_GPS_TUT_5_23(uint8_t value)
	{
		___TUT_GPS_TUT_5_23 = value;
	}

	inline static int32_t get_offset_of_TUT_APP_TUT_1_24() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_APP_TUT_1_24)); }
	inline uint8_t get_TUT_APP_TUT_1_24() const { return ___TUT_APP_TUT_1_24; }
	inline uint8_t* get_address_of_TUT_APP_TUT_1_24() { return &___TUT_APP_TUT_1_24; }
	inline void set_TUT_APP_TUT_1_24(uint8_t value)
	{
		___TUT_APP_TUT_1_24 = value;
	}

	inline static int32_t get_offset_of_TUT_RADIO_TUT_1_25() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_RADIO_TUT_1_25)); }
	inline uint8_t get_TUT_RADIO_TUT_1_25() const { return ___TUT_RADIO_TUT_1_25; }
	inline uint8_t* get_address_of_TUT_RADIO_TUT_1_25() { return &___TUT_RADIO_TUT_1_25; }
	inline void set_TUT_RADIO_TUT_1_25(uint8_t value)
	{
		___TUT_RADIO_TUT_1_25 = value;
	}

	inline static int32_t get_offset_of_TUT_TFT_TUT_1_26() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_TFT_TUT_1_26)); }
	inline uint8_t get_TUT_TFT_TUT_1_26() const { return ___TUT_TFT_TUT_1_26; }
	inline uint8_t* get_address_of_TUT_TFT_TUT_1_26() { return &___TUT_TFT_TUT_1_26; }
	inline void set_TUT_TFT_TUT_1_26(uint8_t value)
	{
		___TUT_TFT_TUT_1_26 = value;
	}

	inline static int32_t get_offset_of_TUT_TFT_TUT_2_27() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_TFT_TUT_2_27)); }
	inline uint8_t get_TUT_TFT_TUT_2_27() const { return ___TUT_TFT_TUT_2_27; }
	inline uint8_t* get_address_of_TUT_TFT_TUT_2_27() { return &___TUT_TFT_TUT_2_27; }
	inline void set_TUT_TFT_TUT_2_27(uint8_t value)
	{
		___TUT_TFT_TUT_2_27 = value;
	}

	inline static int32_t get_offset_of_TUT_TFT_TUT_3_28() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_TFT_TUT_3_28)); }
	inline uint8_t get_TUT_TFT_TUT_3_28() const { return ___TUT_TFT_TUT_3_28; }
	inline uint8_t* get_address_of_TUT_TFT_TUT_3_28() { return &___TUT_TFT_TUT_3_28; }
	inline void set_TUT_TFT_TUT_3_28(uint8_t value)
	{
		___TUT_TFT_TUT_3_28 = value;
	}

	inline static int32_t get_offset_of_TUT_TFT_TUT_4_29() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_TFT_TUT_4_29)); }
	inline uint8_t get_TUT_TFT_TUT_4_29() const { return ___TUT_TFT_TUT_4_29; }
	inline uint8_t* get_address_of_TUT_TFT_TUT_4_29() { return &___TUT_TFT_TUT_4_29; }
	inline void set_TUT_TFT_TUT_4_29(uint8_t value)
	{
		___TUT_TFT_TUT_4_29 = value;
	}

	inline static int32_t get_offset_of_TUT_TFT_TUT_5_30() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_TFT_TUT_5_30)); }
	inline uint8_t get_TUT_TFT_TUT_5_30() const { return ___TUT_TFT_TUT_5_30; }
	inline uint8_t* get_address_of_TUT_TFT_TUT_5_30() { return &___TUT_TFT_TUT_5_30; }
	inline void set_TUT_TFT_TUT_5_30(uint8_t value)
	{
		___TUT_TFT_TUT_5_30 = value;
	}

	inline static int32_t get_offset_of_TUT_TFT_TUT_6_31() { return static_cast<int32_t>(offsetof(TutorialsIDS_t774177219, ___TUT_TFT_TUT_6_31)); }
	inline uint8_t get_TUT_TFT_TUT_6_31() const { return ___TUT_TFT_TUT_6_31; }
	inline uint8_t* get_address_of_TUT_TFT_TUT_6_31() { return &___TUT_TFT_TUT_6_31; }
	inline void set_TUT_TFT_TUT_6_31(uint8_t value)
	{
		___TUT_TFT_TUT_6_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
