﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialsIDS
struct TutorialsIDS_t774177219;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialsIDS::.ctor()
extern "C"  void TutorialsIDS__ctor_m3500489784 (TutorialsIDS_t774177219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
