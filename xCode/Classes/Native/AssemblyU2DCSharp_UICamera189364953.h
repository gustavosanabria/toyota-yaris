﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BetterList`1<UICamera>
struct BetterList_1_t1686332965;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2745734774;
// UICamera/GetAxisFunc
struct GetAxisFunc_t783615269;
// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t4194451799;
// UICamera/OnScreenResize
struct OnScreenResize_t3828578773;
// System.String
struct String_t;
// UICamera/OnCustomInput
struct OnCustomInput_t736305828;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Camera
struct Camera_t2727095145;
// UICamera/OnSchemeChange
struct OnSchemeChange_t2731530058;
// UICamera/MouseOrTouch
struct MouseOrTouch_t2057376333;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UICamera/VoidDelegate
struct VoidDelegate_t3135042255;
// UICamera/BoolDelegate
struct BoolDelegate_t2094540325;
// UICamera/FloatDelegate
struct FloatDelegate_t1146521387;
// UICamera/VectorDelegate
struct VectorDelegate_t2747913726;
// UICamera/ObjectDelegate
struct ObjectDelegate_t3703364602;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t4120455995;
// UICamera/MoveDelegate
struct MoveDelegate_t1906135052;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t546398688;
// System.Collections.Generic.List`1<UICamera/MouseOrTouch>
struct List_1_t3425561885;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t2642582481;
// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t3435560085;
// UICamera/GetTouchCallback
struct GetTouchCallback_t1934354820;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t1687333339;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_UICamera_EventType3387030814.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t189364953  : public MonoBehaviour_t667441552
{
public:
	// UICamera/EventType UICamera::eventType
	int32_t ___eventType_9;
	// System.Boolean UICamera::eventsGoToColliders
	bool ___eventsGoToColliders_10;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t3236759763  ___eventReceiverMask_11;
	// System.Boolean UICamera::debug
	bool ___debug_12;
	// System.Boolean UICamera::useMouse
	bool ___useMouse_13;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_14;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_15;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_16;
	// System.Boolean UICamera::useController
	bool ___useController_17;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_18;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_19;
	// System.Boolean UICamera::longPressTooltip
	bool ___longPressTooltip_20;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_21;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_22;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_23;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_24;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_25;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_26;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_27;
	// System.String UICamera::horizontalPanAxisName
	String_t* ___horizontalPanAxisName_28;
	// System.String UICamera::verticalPanAxisName
	String_t* ___verticalPanAxisName_29;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_30;
	// System.Boolean UICamera::commandClick
	bool ___commandClick_31;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_32;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_33;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_34;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_35;
	// System.Boolean UICamera::autoHideCursor
	bool ___autoHideCursor_36;
	// UnityEngine.Camera UICamera::mCam
	Camera_t2727095145 * ___mCam_76;
	// System.Single UICamera::mNextRaycast
	float ___mNextRaycast_78;

public:
	inline static int32_t get_offset_of_eventType_9() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventType_9)); }
	inline int32_t get_eventType_9() const { return ___eventType_9; }
	inline int32_t* get_address_of_eventType_9() { return &___eventType_9; }
	inline void set_eventType_9(int32_t value)
	{
		___eventType_9 = value;
	}

	inline static int32_t get_offset_of_eventsGoToColliders_10() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventsGoToColliders_10)); }
	inline bool get_eventsGoToColliders_10() const { return ___eventsGoToColliders_10; }
	inline bool* get_address_of_eventsGoToColliders_10() { return &___eventsGoToColliders_10; }
	inline void set_eventsGoToColliders_10(bool value)
	{
		___eventsGoToColliders_10 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_11() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventReceiverMask_11)); }
	inline LayerMask_t3236759763  get_eventReceiverMask_11() const { return ___eventReceiverMask_11; }
	inline LayerMask_t3236759763 * get_address_of_eventReceiverMask_11() { return &___eventReceiverMask_11; }
	inline void set_eventReceiverMask_11(LayerMask_t3236759763  value)
	{
		___eventReceiverMask_11 = value;
	}

	inline static int32_t get_offset_of_debug_12() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___debug_12)); }
	inline bool get_debug_12() const { return ___debug_12; }
	inline bool* get_address_of_debug_12() { return &___debug_12; }
	inline void set_debug_12(bool value)
	{
		___debug_12 = value;
	}

	inline static int32_t get_offset_of_useMouse_13() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useMouse_13)); }
	inline bool get_useMouse_13() const { return ___useMouse_13; }
	inline bool* get_address_of_useMouse_13() { return &___useMouse_13; }
	inline void set_useMouse_13(bool value)
	{
		___useMouse_13 = value;
	}

	inline static int32_t get_offset_of_useTouch_14() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useTouch_14)); }
	inline bool get_useTouch_14() const { return ___useTouch_14; }
	inline bool* get_address_of_useTouch_14() { return &___useTouch_14; }
	inline void set_useTouch_14(bool value)
	{
		___useTouch_14 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_15() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___allowMultiTouch_15)); }
	inline bool get_allowMultiTouch_15() const { return ___allowMultiTouch_15; }
	inline bool* get_address_of_allowMultiTouch_15() { return &___allowMultiTouch_15; }
	inline void set_allowMultiTouch_15(bool value)
	{
		___allowMultiTouch_15 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_16() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useKeyboard_16)); }
	inline bool get_useKeyboard_16() const { return ___useKeyboard_16; }
	inline bool* get_address_of_useKeyboard_16() { return &___useKeyboard_16; }
	inline void set_useKeyboard_16(bool value)
	{
		___useKeyboard_16 = value;
	}

	inline static int32_t get_offset_of_useController_17() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useController_17)); }
	inline bool get_useController_17() const { return ___useController_17; }
	inline bool* get_address_of_useController_17() { return &___useController_17; }
	inline void set_useController_17(bool value)
	{
		___useController_17 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_18() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___stickyTooltip_18)); }
	inline bool get_stickyTooltip_18() const { return ___stickyTooltip_18; }
	inline bool* get_address_of_stickyTooltip_18() { return &___stickyTooltip_18; }
	inline void set_stickyTooltip_18(bool value)
	{
		___stickyTooltip_18 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_19() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___tooltipDelay_19)); }
	inline float get_tooltipDelay_19() const { return ___tooltipDelay_19; }
	inline float* get_address_of_tooltipDelay_19() { return &___tooltipDelay_19; }
	inline void set_tooltipDelay_19(float value)
	{
		___tooltipDelay_19 = value;
	}

	inline static int32_t get_offset_of_longPressTooltip_20() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___longPressTooltip_20)); }
	inline bool get_longPressTooltip_20() const { return ___longPressTooltip_20; }
	inline bool* get_address_of_longPressTooltip_20() { return &___longPressTooltip_20; }
	inline void set_longPressTooltip_20(bool value)
	{
		___longPressTooltip_20 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_21() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mouseDragThreshold_21)); }
	inline float get_mouseDragThreshold_21() const { return ___mouseDragThreshold_21; }
	inline float* get_address_of_mouseDragThreshold_21() { return &___mouseDragThreshold_21; }
	inline void set_mouseDragThreshold_21(float value)
	{
		___mouseDragThreshold_21 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_22() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mouseClickThreshold_22)); }
	inline float get_mouseClickThreshold_22() const { return ___mouseClickThreshold_22; }
	inline float* get_address_of_mouseClickThreshold_22() { return &___mouseClickThreshold_22; }
	inline void set_mouseClickThreshold_22(float value)
	{
		___mouseClickThreshold_22 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_23() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___touchDragThreshold_23)); }
	inline float get_touchDragThreshold_23() const { return ___touchDragThreshold_23; }
	inline float* get_address_of_touchDragThreshold_23() { return &___touchDragThreshold_23; }
	inline void set_touchDragThreshold_23(float value)
	{
		___touchDragThreshold_23 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_24() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___touchClickThreshold_24)); }
	inline float get_touchClickThreshold_24() const { return ___touchClickThreshold_24; }
	inline float* get_address_of_touchClickThreshold_24() { return &___touchClickThreshold_24; }
	inline void set_touchClickThreshold_24(float value)
	{
		___touchClickThreshold_24 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_25() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___rangeDistance_25)); }
	inline float get_rangeDistance_25() const { return ___rangeDistance_25; }
	inline float* get_address_of_rangeDistance_25() { return &___rangeDistance_25; }
	inline void set_rangeDistance_25(float value)
	{
		___rangeDistance_25 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_26() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___horizontalAxisName_26)); }
	inline String_t* get_horizontalAxisName_26() const { return ___horizontalAxisName_26; }
	inline String_t** get_address_of_horizontalAxisName_26() { return &___horizontalAxisName_26; }
	inline void set_horizontalAxisName_26(String_t* value)
	{
		___horizontalAxisName_26 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalAxisName_26, value);
	}

	inline static int32_t get_offset_of_verticalAxisName_27() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___verticalAxisName_27)); }
	inline String_t* get_verticalAxisName_27() const { return ___verticalAxisName_27; }
	inline String_t** get_address_of_verticalAxisName_27() { return &___verticalAxisName_27; }
	inline void set_verticalAxisName_27(String_t* value)
	{
		___verticalAxisName_27 = value;
		Il2CppCodeGenWriteBarrier(&___verticalAxisName_27, value);
	}

	inline static int32_t get_offset_of_horizontalPanAxisName_28() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___horizontalPanAxisName_28)); }
	inline String_t* get_horizontalPanAxisName_28() const { return ___horizontalPanAxisName_28; }
	inline String_t** get_address_of_horizontalPanAxisName_28() { return &___horizontalPanAxisName_28; }
	inline void set_horizontalPanAxisName_28(String_t* value)
	{
		___horizontalPanAxisName_28 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalPanAxisName_28, value);
	}

	inline static int32_t get_offset_of_verticalPanAxisName_29() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___verticalPanAxisName_29)); }
	inline String_t* get_verticalPanAxisName_29() const { return ___verticalPanAxisName_29; }
	inline String_t** get_address_of_verticalPanAxisName_29() { return &___verticalPanAxisName_29; }
	inline void set_verticalPanAxisName_29(String_t* value)
	{
		___verticalPanAxisName_29 = value;
		Il2CppCodeGenWriteBarrier(&___verticalPanAxisName_29, value);
	}

	inline static int32_t get_offset_of_scrollAxisName_30() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___scrollAxisName_30)); }
	inline String_t* get_scrollAxisName_30() const { return ___scrollAxisName_30; }
	inline String_t** get_address_of_scrollAxisName_30() { return &___scrollAxisName_30; }
	inline void set_scrollAxisName_30(String_t* value)
	{
		___scrollAxisName_30 = value;
		Il2CppCodeGenWriteBarrier(&___scrollAxisName_30, value);
	}

	inline static int32_t get_offset_of_commandClick_31() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___commandClick_31)); }
	inline bool get_commandClick_31() const { return ___commandClick_31; }
	inline bool* get_address_of_commandClick_31() { return &___commandClick_31; }
	inline void set_commandClick_31(bool value)
	{
		___commandClick_31 = value;
	}

	inline static int32_t get_offset_of_submitKey0_32() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___submitKey0_32)); }
	inline int32_t get_submitKey0_32() const { return ___submitKey0_32; }
	inline int32_t* get_address_of_submitKey0_32() { return &___submitKey0_32; }
	inline void set_submitKey0_32(int32_t value)
	{
		___submitKey0_32 = value;
	}

	inline static int32_t get_offset_of_submitKey1_33() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___submitKey1_33)); }
	inline int32_t get_submitKey1_33() const { return ___submitKey1_33; }
	inline int32_t* get_address_of_submitKey1_33() { return &___submitKey1_33; }
	inline void set_submitKey1_33(int32_t value)
	{
		___submitKey1_33 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_34() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___cancelKey0_34)); }
	inline int32_t get_cancelKey0_34() const { return ___cancelKey0_34; }
	inline int32_t* get_address_of_cancelKey0_34() { return &___cancelKey0_34; }
	inline void set_cancelKey0_34(int32_t value)
	{
		___cancelKey0_34 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_35() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___cancelKey1_35)); }
	inline int32_t get_cancelKey1_35() const { return ___cancelKey1_35; }
	inline int32_t* get_address_of_cancelKey1_35() { return &___cancelKey1_35; }
	inline void set_cancelKey1_35(int32_t value)
	{
		___cancelKey1_35 = value;
	}

	inline static int32_t get_offset_of_autoHideCursor_36() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___autoHideCursor_36)); }
	inline bool get_autoHideCursor_36() const { return ___autoHideCursor_36; }
	inline bool* get_address_of_autoHideCursor_36() { return &___autoHideCursor_36; }
	inline void set_autoHideCursor_36(bool value)
	{
		___autoHideCursor_36 = value;
	}

	inline static int32_t get_offset_of_mCam_76() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mCam_76)); }
	inline Camera_t2727095145 * get_mCam_76() const { return ___mCam_76; }
	inline Camera_t2727095145 ** get_address_of_mCam_76() { return &___mCam_76; }
	inline void set_mCam_76(Camera_t2727095145 * value)
	{
		___mCam_76 = value;
		Il2CppCodeGenWriteBarrier(&___mCam_76, value);
	}

	inline static int32_t get_offset_of_mNextRaycast_78() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mNextRaycast_78)); }
	inline float get_mNextRaycast_78() const { return ___mNextRaycast_78; }
	inline float* get_address_of_mNextRaycast_78() { return &___mNextRaycast_78; }
	inline void set_mNextRaycast_78(float value)
	{
		___mNextRaycast_78 = value;
	}
};

struct UICamera_t189364953_StaticFields
{
public:
	// BetterList`1<UICamera> UICamera::list
	BetterList_1_t1686332965 * ___list_2;
	// UICamera/GetKeyStateFunc UICamera::GetKeyDown
	GetKeyStateFunc_t2745734774 * ___GetKeyDown_3;
	// UICamera/GetKeyStateFunc UICamera::GetKeyUp
	GetKeyStateFunc_t2745734774 * ___GetKeyUp_4;
	// UICamera/GetKeyStateFunc UICamera::GetKey
	GetKeyStateFunc_t2745734774 * ___GetKey_5;
	// UICamera/GetAxisFunc UICamera::GetAxis
	GetAxisFunc_t783615269 * ___GetAxis_6;
	// UICamera/GetAnyKeyFunc UICamera::GetAnyKeyDown
	GetAnyKeyFunc_t4194451799 * ___GetAnyKeyDown_7;
	// UICamera/OnScreenResize UICamera::onScreenResize
	OnScreenResize_t3828578773 * ___onScreenResize_8;
	// UICamera/OnCustomInput UICamera::onCustomInput
	OnCustomInput_t736305828 * ___onCustomInput_37;
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_38;
	// System.Boolean UICamera::mDisableController
	bool ___mDisableController_39;
	// UnityEngine.Vector2 UICamera::mLastPos
	Vector2_t4282066565  ___mLastPos_40;
	// UnityEngine.Vector3 UICamera::lastWorldPosition
	Vector3_t4282066566  ___lastWorldPosition_41;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t4003175726  ___lastHit_42;
	// UICamera UICamera::current
	UICamera_t189364953 * ___current_43;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t2727095145 * ___currentCamera_44;
	// UICamera/OnSchemeChange UICamera::onSchemeChange
	OnSchemeChange_t2731530058 * ___onSchemeChange_45;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_46;
	// UnityEngine.KeyCode UICamera::mCurrentKey
	int32_t ___mCurrentKey_47;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t2057376333 * ___currentTouch_48;
	// System.Boolean UICamera::mInputFocus
	bool ___mInputFocus_49;
	// UnityEngine.GameObject UICamera::mGenericHandler
	GameObject_t3674682005 * ___mGenericHandler_50;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t3674682005 * ___fallThrough_51;
	// UICamera/VoidDelegate UICamera::onClick
	VoidDelegate_t3135042255 * ___onClick_52;
	// UICamera/VoidDelegate UICamera::onDoubleClick
	VoidDelegate_t3135042255 * ___onDoubleClick_53;
	// UICamera/BoolDelegate UICamera::onHover
	BoolDelegate_t2094540325 * ___onHover_54;
	// UICamera/BoolDelegate UICamera::onPress
	BoolDelegate_t2094540325 * ___onPress_55;
	// UICamera/BoolDelegate UICamera::onSelect
	BoolDelegate_t2094540325 * ___onSelect_56;
	// UICamera/FloatDelegate UICamera::onScroll
	FloatDelegate_t1146521387 * ___onScroll_57;
	// UICamera/VectorDelegate UICamera::onDrag
	VectorDelegate_t2747913726 * ___onDrag_58;
	// UICamera/VoidDelegate UICamera::onDragStart
	VoidDelegate_t3135042255 * ___onDragStart_59;
	// UICamera/ObjectDelegate UICamera::onDragOver
	ObjectDelegate_t3703364602 * ___onDragOver_60;
	// UICamera/ObjectDelegate UICamera::onDragOut
	ObjectDelegate_t3703364602 * ___onDragOut_61;
	// UICamera/VoidDelegate UICamera::onDragEnd
	VoidDelegate_t3135042255 * ___onDragEnd_62;
	// UICamera/ObjectDelegate UICamera::onDrop
	ObjectDelegate_t3703364602 * ___onDrop_63;
	// UICamera/KeyCodeDelegate UICamera::onKey
	KeyCodeDelegate_t4120455995 * ___onKey_64;
	// UICamera/KeyCodeDelegate UICamera::onNavigate
	KeyCodeDelegate_t4120455995 * ___onNavigate_65;
	// UICamera/VectorDelegate UICamera::onPan
	VectorDelegate_t2747913726 * ___onPan_66;
	// UICamera/BoolDelegate UICamera::onTooltip
	BoolDelegate_t2094540325 * ___onTooltip_67;
	// UICamera/MoveDelegate UICamera::onMouseMove
	MoveDelegate_t1906135052 * ___onMouseMove_68;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t546398688* ___mMouse_69;
	// UICamera/MouseOrTouch UICamera::controller
	MouseOrTouch_t2057376333 * ___controller_70;
	// System.Collections.Generic.List`1<UICamera/MouseOrTouch> UICamera::activeTouches
	List_1_t3425561885 * ___activeTouches_71;
	// System.Collections.Generic.List`1<System.Int32> UICamera::mTouchIDs
	List_1_t2522024052 * ___mTouchIDs_72;
	// System.Int32 UICamera::mWidth
	int32_t ___mWidth_73;
	// System.Int32 UICamera::mHeight
	int32_t ___mHeight_74;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t3674682005 * ___mTooltip_75;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_77;
	// System.Boolean UICamera::isDragging
	bool ___isDragging_79;
	// UnityEngine.GameObject UICamera::mRayHitObject
	GameObject_t3674682005 * ___mRayHitObject_80;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t3674682005 * ___mHover_81;
	// UnityEngine.GameObject UICamera::mSelected
	GameObject_t3674682005 * ___mSelected_82;
	// UICamera/DepthEntry UICamera::mHit
	DepthEntry_t1145614469  ___mHit_83;
	// BetterList`1<UICamera/DepthEntry> UICamera::mHits
	BetterList_1_t2642582481 * ___mHits_84;
	// UnityEngine.Plane UICamera::m2DPlane
	Plane_t4206452690  ___m2DPlane_85;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_86;
	// System.Int32 UICamera::mNotifying
	int32_t ___mNotifying_87;
	// System.Boolean UICamera::mUsingTouchEvents
	bool ___mUsingTouchEvents_88;
	// UICamera/GetTouchCountCallback UICamera::GetInputTouchCount
	GetTouchCountCallback_t3435560085 * ___GetInputTouchCount_89;
	// UICamera/GetTouchCallback UICamera::GetInputTouch
	GetTouchCallback_t1934354820 * ___GetInputTouch_90;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache59
	CompareFunc_t1687333339 * ___U3CU3Ef__amU24cache59_91;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache5A
	CompareFunc_t1687333339 * ___U3CU3Ef__amU24cache5A_92;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___list_2)); }
	inline BetterList_1_t1686332965 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t1686332965 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t1686332965 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier(&___list_2, value);
	}

	inline static int32_t get_offset_of_GetKeyDown_3() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKeyDown_3)); }
	inline GetKeyStateFunc_t2745734774 * get_GetKeyDown_3() const { return ___GetKeyDown_3; }
	inline GetKeyStateFunc_t2745734774 ** get_address_of_GetKeyDown_3() { return &___GetKeyDown_3; }
	inline void set_GetKeyDown_3(GetKeyStateFunc_t2745734774 * value)
	{
		___GetKeyDown_3 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyDown_3, value);
	}

	inline static int32_t get_offset_of_GetKeyUp_4() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKeyUp_4)); }
	inline GetKeyStateFunc_t2745734774 * get_GetKeyUp_4() const { return ___GetKeyUp_4; }
	inline GetKeyStateFunc_t2745734774 ** get_address_of_GetKeyUp_4() { return &___GetKeyUp_4; }
	inline void set_GetKeyUp_4(GetKeyStateFunc_t2745734774 * value)
	{
		___GetKeyUp_4 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyUp_4, value);
	}

	inline static int32_t get_offset_of_GetKey_5() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKey_5)); }
	inline GetKeyStateFunc_t2745734774 * get_GetKey_5() const { return ___GetKey_5; }
	inline GetKeyStateFunc_t2745734774 ** get_address_of_GetKey_5() { return &___GetKey_5; }
	inline void set_GetKey_5(GetKeyStateFunc_t2745734774 * value)
	{
		___GetKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___GetKey_5, value);
	}

	inline static int32_t get_offset_of_GetAxis_6() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetAxis_6)); }
	inline GetAxisFunc_t783615269 * get_GetAxis_6() const { return ___GetAxis_6; }
	inline GetAxisFunc_t783615269 ** get_address_of_GetAxis_6() { return &___GetAxis_6; }
	inline void set_GetAxis_6(GetAxisFunc_t783615269 * value)
	{
		___GetAxis_6 = value;
		Il2CppCodeGenWriteBarrier(&___GetAxis_6, value);
	}

	inline static int32_t get_offset_of_GetAnyKeyDown_7() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetAnyKeyDown_7)); }
	inline GetAnyKeyFunc_t4194451799 * get_GetAnyKeyDown_7() const { return ___GetAnyKeyDown_7; }
	inline GetAnyKeyFunc_t4194451799 ** get_address_of_GetAnyKeyDown_7() { return &___GetAnyKeyDown_7; }
	inline void set_GetAnyKeyDown_7(GetAnyKeyFunc_t4194451799 * value)
	{
		___GetAnyKeyDown_7 = value;
		Il2CppCodeGenWriteBarrier(&___GetAnyKeyDown_7, value);
	}

	inline static int32_t get_offset_of_onScreenResize_8() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onScreenResize_8)); }
	inline OnScreenResize_t3828578773 * get_onScreenResize_8() const { return ___onScreenResize_8; }
	inline OnScreenResize_t3828578773 ** get_address_of_onScreenResize_8() { return &___onScreenResize_8; }
	inline void set_onScreenResize_8(OnScreenResize_t3828578773 * value)
	{
		___onScreenResize_8 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenResize_8, value);
	}

	inline static int32_t get_offset_of_onCustomInput_37() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onCustomInput_37)); }
	inline OnCustomInput_t736305828 * get_onCustomInput_37() const { return ___onCustomInput_37; }
	inline OnCustomInput_t736305828 ** get_address_of_onCustomInput_37() { return &___onCustomInput_37; }
	inline void set_onCustomInput_37(OnCustomInput_t736305828 * value)
	{
		___onCustomInput_37 = value;
		Il2CppCodeGenWriteBarrier(&___onCustomInput_37, value);
	}

	inline static int32_t get_offset_of_showTooltips_38() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___showTooltips_38)); }
	inline bool get_showTooltips_38() const { return ___showTooltips_38; }
	inline bool* get_address_of_showTooltips_38() { return &___showTooltips_38; }
	inline void set_showTooltips_38(bool value)
	{
		___showTooltips_38 = value;
	}

	inline static int32_t get_offset_of_mDisableController_39() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mDisableController_39)); }
	inline bool get_mDisableController_39() const { return ___mDisableController_39; }
	inline bool* get_address_of_mDisableController_39() { return &___mDisableController_39; }
	inline void set_mDisableController_39(bool value)
	{
		___mDisableController_39 = value;
	}

	inline static int32_t get_offset_of_mLastPos_40() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mLastPos_40)); }
	inline Vector2_t4282066565  get_mLastPos_40() const { return ___mLastPos_40; }
	inline Vector2_t4282066565 * get_address_of_mLastPos_40() { return &___mLastPos_40; }
	inline void set_mLastPos_40(Vector2_t4282066565  value)
	{
		___mLastPos_40 = value;
	}

	inline static int32_t get_offset_of_lastWorldPosition_41() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastWorldPosition_41)); }
	inline Vector3_t4282066566  get_lastWorldPosition_41() const { return ___lastWorldPosition_41; }
	inline Vector3_t4282066566 * get_address_of_lastWorldPosition_41() { return &___lastWorldPosition_41; }
	inline void set_lastWorldPosition_41(Vector3_t4282066566  value)
	{
		___lastWorldPosition_41 = value;
	}

	inline static int32_t get_offset_of_lastHit_42() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastHit_42)); }
	inline RaycastHit_t4003175726  get_lastHit_42() const { return ___lastHit_42; }
	inline RaycastHit_t4003175726 * get_address_of_lastHit_42() { return &___lastHit_42; }
	inline void set_lastHit_42(RaycastHit_t4003175726  value)
	{
		___lastHit_42 = value;
	}

	inline static int32_t get_offset_of_current_43() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___current_43)); }
	inline UICamera_t189364953 * get_current_43() const { return ___current_43; }
	inline UICamera_t189364953 ** get_address_of_current_43() { return &___current_43; }
	inline void set_current_43(UICamera_t189364953 * value)
	{
		___current_43 = value;
		Il2CppCodeGenWriteBarrier(&___current_43, value);
	}

	inline static int32_t get_offset_of_currentCamera_44() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentCamera_44)); }
	inline Camera_t2727095145 * get_currentCamera_44() const { return ___currentCamera_44; }
	inline Camera_t2727095145 ** get_address_of_currentCamera_44() { return &___currentCamera_44; }
	inline void set_currentCamera_44(Camera_t2727095145 * value)
	{
		___currentCamera_44 = value;
		Il2CppCodeGenWriteBarrier(&___currentCamera_44, value);
	}

	inline static int32_t get_offset_of_onSchemeChange_45() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onSchemeChange_45)); }
	inline OnSchemeChange_t2731530058 * get_onSchemeChange_45() const { return ___onSchemeChange_45; }
	inline OnSchemeChange_t2731530058 ** get_address_of_onSchemeChange_45() { return &___onSchemeChange_45; }
	inline void set_onSchemeChange_45(OnSchemeChange_t2731530058 * value)
	{
		___onSchemeChange_45 = value;
		Il2CppCodeGenWriteBarrier(&___onSchemeChange_45, value);
	}

	inline static int32_t get_offset_of_currentTouchID_46() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentTouchID_46)); }
	inline int32_t get_currentTouchID_46() const { return ___currentTouchID_46; }
	inline int32_t* get_address_of_currentTouchID_46() { return &___currentTouchID_46; }
	inline void set_currentTouchID_46(int32_t value)
	{
		___currentTouchID_46 = value;
	}

	inline static int32_t get_offset_of_mCurrentKey_47() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mCurrentKey_47)); }
	inline int32_t get_mCurrentKey_47() const { return ___mCurrentKey_47; }
	inline int32_t* get_address_of_mCurrentKey_47() { return &___mCurrentKey_47; }
	inline void set_mCurrentKey_47(int32_t value)
	{
		___mCurrentKey_47 = value;
	}

	inline static int32_t get_offset_of_currentTouch_48() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentTouch_48)); }
	inline MouseOrTouch_t2057376333 * get_currentTouch_48() const { return ___currentTouch_48; }
	inline MouseOrTouch_t2057376333 ** get_address_of_currentTouch_48() { return &___currentTouch_48; }
	inline void set_currentTouch_48(MouseOrTouch_t2057376333 * value)
	{
		___currentTouch_48 = value;
		Il2CppCodeGenWriteBarrier(&___currentTouch_48, value);
	}

	inline static int32_t get_offset_of_mInputFocus_49() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mInputFocus_49)); }
	inline bool get_mInputFocus_49() const { return ___mInputFocus_49; }
	inline bool* get_address_of_mInputFocus_49() { return &___mInputFocus_49; }
	inline void set_mInputFocus_49(bool value)
	{
		___mInputFocus_49 = value;
	}

	inline static int32_t get_offset_of_mGenericHandler_50() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mGenericHandler_50)); }
	inline GameObject_t3674682005 * get_mGenericHandler_50() const { return ___mGenericHandler_50; }
	inline GameObject_t3674682005 ** get_address_of_mGenericHandler_50() { return &___mGenericHandler_50; }
	inline void set_mGenericHandler_50(GameObject_t3674682005 * value)
	{
		___mGenericHandler_50 = value;
		Il2CppCodeGenWriteBarrier(&___mGenericHandler_50, value);
	}

	inline static int32_t get_offset_of_fallThrough_51() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___fallThrough_51)); }
	inline GameObject_t3674682005 * get_fallThrough_51() const { return ___fallThrough_51; }
	inline GameObject_t3674682005 ** get_address_of_fallThrough_51() { return &___fallThrough_51; }
	inline void set_fallThrough_51(GameObject_t3674682005 * value)
	{
		___fallThrough_51 = value;
		Il2CppCodeGenWriteBarrier(&___fallThrough_51, value);
	}

	inline static int32_t get_offset_of_onClick_52() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onClick_52)); }
	inline VoidDelegate_t3135042255 * get_onClick_52() const { return ___onClick_52; }
	inline VoidDelegate_t3135042255 ** get_address_of_onClick_52() { return &___onClick_52; }
	inline void set_onClick_52(VoidDelegate_t3135042255 * value)
	{
		___onClick_52 = value;
		Il2CppCodeGenWriteBarrier(&___onClick_52, value);
	}

	inline static int32_t get_offset_of_onDoubleClick_53() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDoubleClick_53)); }
	inline VoidDelegate_t3135042255 * get_onDoubleClick_53() const { return ___onDoubleClick_53; }
	inline VoidDelegate_t3135042255 ** get_address_of_onDoubleClick_53() { return &___onDoubleClick_53; }
	inline void set_onDoubleClick_53(VoidDelegate_t3135042255 * value)
	{
		___onDoubleClick_53 = value;
		Il2CppCodeGenWriteBarrier(&___onDoubleClick_53, value);
	}

	inline static int32_t get_offset_of_onHover_54() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onHover_54)); }
	inline BoolDelegate_t2094540325 * get_onHover_54() const { return ___onHover_54; }
	inline BoolDelegate_t2094540325 ** get_address_of_onHover_54() { return &___onHover_54; }
	inline void set_onHover_54(BoolDelegate_t2094540325 * value)
	{
		___onHover_54 = value;
		Il2CppCodeGenWriteBarrier(&___onHover_54, value);
	}

	inline static int32_t get_offset_of_onPress_55() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onPress_55)); }
	inline BoolDelegate_t2094540325 * get_onPress_55() const { return ___onPress_55; }
	inline BoolDelegate_t2094540325 ** get_address_of_onPress_55() { return &___onPress_55; }
	inline void set_onPress_55(BoolDelegate_t2094540325 * value)
	{
		___onPress_55 = value;
		Il2CppCodeGenWriteBarrier(&___onPress_55, value);
	}

	inline static int32_t get_offset_of_onSelect_56() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onSelect_56)); }
	inline BoolDelegate_t2094540325 * get_onSelect_56() const { return ___onSelect_56; }
	inline BoolDelegate_t2094540325 ** get_address_of_onSelect_56() { return &___onSelect_56; }
	inline void set_onSelect_56(BoolDelegate_t2094540325 * value)
	{
		___onSelect_56 = value;
		Il2CppCodeGenWriteBarrier(&___onSelect_56, value);
	}

	inline static int32_t get_offset_of_onScroll_57() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onScroll_57)); }
	inline FloatDelegate_t1146521387 * get_onScroll_57() const { return ___onScroll_57; }
	inline FloatDelegate_t1146521387 ** get_address_of_onScroll_57() { return &___onScroll_57; }
	inline void set_onScroll_57(FloatDelegate_t1146521387 * value)
	{
		___onScroll_57 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_57, value);
	}

	inline static int32_t get_offset_of_onDrag_58() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDrag_58)); }
	inline VectorDelegate_t2747913726 * get_onDrag_58() const { return ___onDrag_58; }
	inline VectorDelegate_t2747913726 ** get_address_of_onDrag_58() { return &___onDrag_58; }
	inline void set_onDrag_58(VectorDelegate_t2747913726 * value)
	{
		___onDrag_58 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_58, value);
	}

	inline static int32_t get_offset_of_onDragStart_59() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragStart_59)); }
	inline VoidDelegate_t3135042255 * get_onDragStart_59() const { return ___onDragStart_59; }
	inline VoidDelegate_t3135042255 ** get_address_of_onDragStart_59() { return &___onDragStart_59; }
	inline void set_onDragStart_59(VoidDelegate_t3135042255 * value)
	{
		___onDragStart_59 = value;
		Il2CppCodeGenWriteBarrier(&___onDragStart_59, value);
	}

	inline static int32_t get_offset_of_onDragOver_60() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragOver_60)); }
	inline ObjectDelegate_t3703364602 * get_onDragOver_60() const { return ___onDragOver_60; }
	inline ObjectDelegate_t3703364602 ** get_address_of_onDragOver_60() { return &___onDragOver_60; }
	inline void set_onDragOver_60(ObjectDelegate_t3703364602 * value)
	{
		___onDragOver_60 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOver_60, value);
	}

	inline static int32_t get_offset_of_onDragOut_61() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragOut_61)); }
	inline ObjectDelegate_t3703364602 * get_onDragOut_61() const { return ___onDragOut_61; }
	inline ObjectDelegate_t3703364602 ** get_address_of_onDragOut_61() { return &___onDragOut_61; }
	inline void set_onDragOut_61(ObjectDelegate_t3703364602 * value)
	{
		___onDragOut_61 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOut_61, value);
	}

	inline static int32_t get_offset_of_onDragEnd_62() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragEnd_62)); }
	inline VoidDelegate_t3135042255 * get_onDragEnd_62() const { return ___onDragEnd_62; }
	inline VoidDelegate_t3135042255 ** get_address_of_onDragEnd_62() { return &___onDragEnd_62; }
	inline void set_onDragEnd_62(VoidDelegate_t3135042255 * value)
	{
		___onDragEnd_62 = value;
		Il2CppCodeGenWriteBarrier(&___onDragEnd_62, value);
	}

	inline static int32_t get_offset_of_onDrop_63() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDrop_63)); }
	inline ObjectDelegate_t3703364602 * get_onDrop_63() const { return ___onDrop_63; }
	inline ObjectDelegate_t3703364602 ** get_address_of_onDrop_63() { return &___onDrop_63; }
	inline void set_onDrop_63(ObjectDelegate_t3703364602 * value)
	{
		___onDrop_63 = value;
		Il2CppCodeGenWriteBarrier(&___onDrop_63, value);
	}

	inline static int32_t get_offset_of_onKey_64() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onKey_64)); }
	inline KeyCodeDelegate_t4120455995 * get_onKey_64() const { return ___onKey_64; }
	inline KeyCodeDelegate_t4120455995 ** get_address_of_onKey_64() { return &___onKey_64; }
	inline void set_onKey_64(KeyCodeDelegate_t4120455995 * value)
	{
		___onKey_64 = value;
		Il2CppCodeGenWriteBarrier(&___onKey_64, value);
	}

	inline static int32_t get_offset_of_onNavigate_65() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onNavigate_65)); }
	inline KeyCodeDelegate_t4120455995 * get_onNavigate_65() const { return ___onNavigate_65; }
	inline KeyCodeDelegate_t4120455995 ** get_address_of_onNavigate_65() { return &___onNavigate_65; }
	inline void set_onNavigate_65(KeyCodeDelegate_t4120455995 * value)
	{
		___onNavigate_65 = value;
		Il2CppCodeGenWriteBarrier(&___onNavigate_65, value);
	}

	inline static int32_t get_offset_of_onPan_66() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onPan_66)); }
	inline VectorDelegate_t2747913726 * get_onPan_66() const { return ___onPan_66; }
	inline VectorDelegate_t2747913726 ** get_address_of_onPan_66() { return &___onPan_66; }
	inline void set_onPan_66(VectorDelegate_t2747913726 * value)
	{
		___onPan_66 = value;
		Il2CppCodeGenWriteBarrier(&___onPan_66, value);
	}

	inline static int32_t get_offset_of_onTooltip_67() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onTooltip_67)); }
	inline BoolDelegate_t2094540325 * get_onTooltip_67() const { return ___onTooltip_67; }
	inline BoolDelegate_t2094540325 ** get_address_of_onTooltip_67() { return &___onTooltip_67; }
	inline void set_onTooltip_67(BoolDelegate_t2094540325 * value)
	{
		___onTooltip_67 = value;
		Il2CppCodeGenWriteBarrier(&___onTooltip_67, value);
	}

	inline static int32_t get_offset_of_onMouseMove_68() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onMouseMove_68)); }
	inline MoveDelegate_t1906135052 * get_onMouseMove_68() const { return ___onMouseMove_68; }
	inline MoveDelegate_t1906135052 ** get_address_of_onMouseMove_68() { return &___onMouseMove_68; }
	inline void set_onMouseMove_68(MoveDelegate_t1906135052 * value)
	{
		___onMouseMove_68 = value;
		Il2CppCodeGenWriteBarrier(&___onMouseMove_68, value);
	}

	inline static int32_t get_offset_of_mMouse_69() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mMouse_69)); }
	inline MouseOrTouchU5BU5D_t546398688* get_mMouse_69() const { return ___mMouse_69; }
	inline MouseOrTouchU5BU5D_t546398688** get_address_of_mMouse_69() { return &___mMouse_69; }
	inline void set_mMouse_69(MouseOrTouchU5BU5D_t546398688* value)
	{
		___mMouse_69 = value;
		Il2CppCodeGenWriteBarrier(&___mMouse_69, value);
	}

	inline static int32_t get_offset_of_controller_70() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___controller_70)); }
	inline MouseOrTouch_t2057376333 * get_controller_70() const { return ___controller_70; }
	inline MouseOrTouch_t2057376333 ** get_address_of_controller_70() { return &___controller_70; }
	inline void set_controller_70(MouseOrTouch_t2057376333 * value)
	{
		___controller_70 = value;
		Il2CppCodeGenWriteBarrier(&___controller_70, value);
	}

	inline static int32_t get_offset_of_activeTouches_71() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___activeTouches_71)); }
	inline List_1_t3425561885 * get_activeTouches_71() const { return ___activeTouches_71; }
	inline List_1_t3425561885 ** get_address_of_activeTouches_71() { return &___activeTouches_71; }
	inline void set_activeTouches_71(List_1_t3425561885 * value)
	{
		___activeTouches_71 = value;
		Il2CppCodeGenWriteBarrier(&___activeTouches_71, value);
	}

	inline static int32_t get_offset_of_mTouchIDs_72() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTouchIDs_72)); }
	inline List_1_t2522024052 * get_mTouchIDs_72() const { return ___mTouchIDs_72; }
	inline List_1_t2522024052 ** get_address_of_mTouchIDs_72() { return &___mTouchIDs_72; }
	inline void set_mTouchIDs_72(List_1_t2522024052 * value)
	{
		___mTouchIDs_72 = value;
		Il2CppCodeGenWriteBarrier(&___mTouchIDs_72, value);
	}

	inline static int32_t get_offset_of_mWidth_73() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mWidth_73)); }
	inline int32_t get_mWidth_73() const { return ___mWidth_73; }
	inline int32_t* get_address_of_mWidth_73() { return &___mWidth_73; }
	inline void set_mWidth_73(int32_t value)
	{
		___mWidth_73 = value;
	}

	inline static int32_t get_offset_of_mHeight_74() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHeight_74)); }
	inline int32_t get_mHeight_74() const { return ___mHeight_74; }
	inline int32_t* get_address_of_mHeight_74() { return &___mHeight_74; }
	inline void set_mHeight_74(int32_t value)
	{
		___mHeight_74 = value;
	}

	inline static int32_t get_offset_of_mTooltip_75() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTooltip_75)); }
	inline GameObject_t3674682005 * get_mTooltip_75() const { return ___mTooltip_75; }
	inline GameObject_t3674682005 ** get_address_of_mTooltip_75() { return &___mTooltip_75; }
	inline void set_mTooltip_75(GameObject_t3674682005 * value)
	{
		___mTooltip_75 = value;
		Il2CppCodeGenWriteBarrier(&___mTooltip_75, value);
	}

	inline static int32_t get_offset_of_mTooltipTime_77() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTooltipTime_77)); }
	inline float get_mTooltipTime_77() const { return ___mTooltipTime_77; }
	inline float* get_address_of_mTooltipTime_77() { return &___mTooltipTime_77; }
	inline void set_mTooltipTime_77(float value)
	{
		___mTooltipTime_77 = value;
	}

	inline static int32_t get_offset_of_isDragging_79() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___isDragging_79)); }
	inline bool get_isDragging_79() const { return ___isDragging_79; }
	inline bool* get_address_of_isDragging_79() { return &___isDragging_79; }
	inline void set_isDragging_79(bool value)
	{
		___isDragging_79 = value;
	}

	inline static int32_t get_offset_of_mRayHitObject_80() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mRayHitObject_80)); }
	inline GameObject_t3674682005 * get_mRayHitObject_80() const { return ___mRayHitObject_80; }
	inline GameObject_t3674682005 ** get_address_of_mRayHitObject_80() { return &___mRayHitObject_80; }
	inline void set_mRayHitObject_80(GameObject_t3674682005 * value)
	{
		___mRayHitObject_80 = value;
		Il2CppCodeGenWriteBarrier(&___mRayHitObject_80, value);
	}

	inline static int32_t get_offset_of_mHover_81() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHover_81)); }
	inline GameObject_t3674682005 * get_mHover_81() const { return ___mHover_81; }
	inline GameObject_t3674682005 ** get_address_of_mHover_81() { return &___mHover_81; }
	inline void set_mHover_81(GameObject_t3674682005 * value)
	{
		___mHover_81 = value;
		Il2CppCodeGenWriteBarrier(&___mHover_81, value);
	}

	inline static int32_t get_offset_of_mSelected_82() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mSelected_82)); }
	inline GameObject_t3674682005 * get_mSelected_82() const { return ___mSelected_82; }
	inline GameObject_t3674682005 ** get_address_of_mSelected_82() { return &___mSelected_82; }
	inline void set_mSelected_82(GameObject_t3674682005 * value)
	{
		___mSelected_82 = value;
		Il2CppCodeGenWriteBarrier(&___mSelected_82, value);
	}

	inline static int32_t get_offset_of_mHit_83() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHit_83)); }
	inline DepthEntry_t1145614469  get_mHit_83() const { return ___mHit_83; }
	inline DepthEntry_t1145614469 * get_address_of_mHit_83() { return &___mHit_83; }
	inline void set_mHit_83(DepthEntry_t1145614469  value)
	{
		___mHit_83 = value;
	}

	inline static int32_t get_offset_of_mHits_84() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHits_84)); }
	inline BetterList_1_t2642582481 * get_mHits_84() const { return ___mHits_84; }
	inline BetterList_1_t2642582481 ** get_address_of_mHits_84() { return &___mHits_84; }
	inline void set_mHits_84(BetterList_1_t2642582481 * value)
	{
		___mHits_84 = value;
		Il2CppCodeGenWriteBarrier(&___mHits_84, value);
	}

	inline static int32_t get_offset_of_m2DPlane_85() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___m2DPlane_85)); }
	inline Plane_t4206452690  get_m2DPlane_85() const { return ___m2DPlane_85; }
	inline Plane_t4206452690 * get_address_of_m2DPlane_85() { return &___m2DPlane_85; }
	inline void set_m2DPlane_85(Plane_t4206452690  value)
	{
		___m2DPlane_85 = value;
	}

	inline static int32_t get_offset_of_mNextEvent_86() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNextEvent_86)); }
	inline float get_mNextEvent_86() const { return ___mNextEvent_86; }
	inline float* get_address_of_mNextEvent_86() { return &___mNextEvent_86; }
	inline void set_mNextEvent_86(float value)
	{
		___mNextEvent_86 = value;
	}

	inline static int32_t get_offset_of_mNotifying_87() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNotifying_87)); }
	inline int32_t get_mNotifying_87() const { return ___mNotifying_87; }
	inline int32_t* get_address_of_mNotifying_87() { return &___mNotifying_87; }
	inline void set_mNotifying_87(int32_t value)
	{
		___mNotifying_87 = value;
	}

	inline static int32_t get_offset_of_mUsingTouchEvents_88() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mUsingTouchEvents_88)); }
	inline bool get_mUsingTouchEvents_88() const { return ___mUsingTouchEvents_88; }
	inline bool* get_address_of_mUsingTouchEvents_88() { return &___mUsingTouchEvents_88; }
	inline void set_mUsingTouchEvents_88(bool value)
	{
		___mUsingTouchEvents_88 = value;
	}

	inline static int32_t get_offset_of_GetInputTouchCount_89() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetInputTouchCount_89)); }
	inline GetTouchCountCallback_t3435560085 * get_GetInputTouchCount_89() const { return ___GetInputTouchCount_89; }
	inline GetTouchCountCallback_t3435560085 ** get_address_of_GetInputTouchCount_89() { return &___GetInputTouchCount_89; }
	inline void set_GetInputTouchCount_89(GetTouchCountCallback_t3435560085 * value)
	{
		___GetInputTouchCount_89 = value;
		Il2CppCodeGenWriteBarrier(&___GetInputTouchCount_89, value);
	}

	inline static int32_t get_offset_of_GetInputTouch_90() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetInputTouch_90)); }
	inline GetTouchCallback_t1934354820 * get_GetInputTouch_90() const { return ___GetInputTouch_90; }
	inline GetTouchCallback_t1934354820 ** get_address_of_GetInputTouch_90() { return &___GetInputTouch_90; }
	inline void set_GetInputTouch_90(GetTouchCallback_t1934354820 * value)
	{
		___GetInputTouch_90 = value;
		Il2CppCodeGenWriteBarrier(&___GetInputTouch_90, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache59_91() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache59_91)); }
	inline CompareFunc_t1687333339 * get_U3CU3Ef__amU24cache59_91() const { return ___U3CU3Ef__amU24cache59_91; }
	inline CompareFunc_t1687333339 ** get_address_of_U3CU3Ef__amU24cache59_91() { return &___U3CU3Ef__amU24cache59_91; }
	inline void set_U3CU3Ef__amU24cache59_91(CompareFunc_t1687333339 * value)
	{
		___U3CU3Ef__amU24cache59_91 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache59_91, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5A_92() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache5A_92)); }
	inline CompareFunc_t1687333339 * get_U3CU3Ef__amU24cache5A_92() const { return ___U3CU3Ef__amU24cache5A_92; }
	inline CompareFunc_t1687333339 ** get_address_of_U3CU3Ef__amU24cache5A_92() { return &___U3CU3Ef__amU24cache5A_92; }
	inline void set_U3CU3Ef__amU24cache5A_92(CompareFunc_t1687333339 * value)
	{
		___U3CU3Ef__amU24cache5A_92 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5A_92, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
