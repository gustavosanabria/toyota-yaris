﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIKeyBinding
struct UIKeyBinding_t2313833690;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

// System.Void UIKeyBinding::.ctor()
extern "C"  void UIKeyBinding__ctor_m1497201409 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::.cctor()
extern "C"  void UIKeyBinding__cctor_m2981474508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBinding::IsBound(UnityEngine.KeyCode)
extern "C"  bool UIKeyBinding_IsBound_m1457684094 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnEnable()
extern "C"  void UIKeyBinding_OnEnable_m1963385061 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnDisable()
extern "C"  void UIKeyBinding_OnDisable_m1176332008 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::Start()
extern "C"  void UIKeyBinding_Start_m444339201 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnSubmit()
extern "C"  void UIKeyBinding_OnSubmit_m2549197370 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBinding::IsModifierActive()
extern "C"  bool UIKeyBinding_IsModifierActive_m4070177950 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::Update()
extern "C"  void UIKeyBinding_Update_m895465516 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnBindingPress(System.Boolean)
extern "C"  void UIKeyBinding_OnBindingPress_m678603287 (UIKeyBinding_t2313833690 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnBindingClick()
extern "C"  void UIKeyBinding_OnBindingClick_m630288549 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
