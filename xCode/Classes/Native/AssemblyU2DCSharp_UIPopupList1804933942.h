﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPopupList
struct UIPopupList_t1804933942;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIAtlas
struct UIAtlas_t281921111;
// UIFont
struct UIFont_t2503090435;
// UnityEngine.Font
struct Font_t4241557075;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UIPanel
struct UIPanel_t295209936;
// UISprite
struct UISprite_t661437049;
// UILabel
struct UILabel_t291504320;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t1659689872;
// UIPopupList/LegacyEvent
struct LegacyEvent_t524649176;

#include "AssemblyU2DCSharp_UIWidgetContainer1520767337.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "AssemblyU2DCSharp_UIPopupList_Position2071232514.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3426431694.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIPopupList_OpenOn1064537826.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList
struct  UIPopupList_t1804933942  : public UIWidgetContainer_t1520767337
{
public:
	// UIAtlas UIPopupList::atlas
	UIAtlas_t281921111 * ___atlas_6;
	// UIFont UIPopupList::bitmapFont
	UIFont_t2503090435 * ___bitmapFont_7;
	// UnityEngine.Font UIPopupList::trueTypeFont
	Font_t4241557075 * ___trueTypeFont_8;
	// System.Int32 UIPopupList::fontSize
	int32_t ___fontSize_9;
	// UnityEngine.FontStyle UIPopupList::fontStyle
	int32_t ___fontStyle_10;
	// System.String UIPopupList::backgroundSprite
	String_t* ___backgroundSprite_11;
	// System.String UIPopupList::highlightSprite
	String_t* ___highlightSprite_12;
	// UIPopupList/Position UIPopupList::position
	int32_t ___position_13;
	// NGUIText/Alignment UIPopupList::alignment
	int32_t ___alignment_14;
	// System.Collections.Generic.List`1<System.String> UIPopupList::items
	List_1_t1375417109 * ___items_15;
	// System.Collections.Generic.List`1<System.Object> UIPopupList::itemData
	List_1_t1244034627 * ___itemData_16;
	// UnityEngine.Vector2 UIPopupList::padding
	Vector2_t4282066565  ___padding_17;
	// UnityEngine.Color UIPopupList::textColor
	Color_t4194546905  ___textColor_18;
	// UnityEngine.Color UIPopupList::backgroundColor
	Color_t4194546905  ___backgroundColor_19;
	// UnityEngine.Color UIPopupList::highlightColor
	Color_t4194546905  ___highlightColor_20;
	// System.Boolean UIPopupList::isAnimated
	bool ___isAnimated_21;
	// System.Boolean UIPopupList::isLocalized
	bool ___isLocalized_22;
	// System.Boolean UIPopupList::separatePanel
	bool ___separatePanel_23;
	// UIPopupList/OpenOn UIPopupList::openOn
	int32_t ___openOn_24;
	// System.Collections.Generic.List`1<EventDelegate> UIPopupList::onChange
	List_1_t1077642479 * ___onChange_25;
	// System.String UIPopupList::mSelectedItem
	String_t* ___mSelectedItem_26;
	// UIPanel UIPopupList::mPanel
	UIPanel_t295209936 * ___mPanel_27;
	// UISprite UIPopupList::mBackground
	UISprite_t661437049 * ___mBackground_28;
	// UISprite UIPopupList::mHighlight
	UISprite_t661437049 * ___mHighlight_29;
	// UILabel UIPopupList::mHighlightedLabel
	UILabel_t291504320 * ___mHighlightedLabel_30;
	// System.Collections.Generic.List`1<UILabel> UIPopupList::mLabelList
	List_1_t1659689872 * ___mLabelList_31;
	// System.Single UIPopupList::mBgBorder
	float ___mBgBorder_32;
	// UnityEngine.GameObject UIPopupList::mSelection
	GameObject_t3674682005 * ___mSelection_33;
	// System.Int32 UIPopupList::mOpenFrame
	int32_t ___mOpenFrame_34;
	// UnityEngine.GameObject UIPopupList::eventReceiver
	GameObject_t3674682005 * ___eventReceiver_35;
	// System.String UIPopupList::functionName
	String_t* ___functionName_36;
	// System.Single UIPopupList::textScale
	float ___textScale_37;
	// UIFont UIPopupList::font
	UIFont_t2503090435 * ___font_38;
	// UILabel UIPopupList::textLabel
	UILabel_t291504320 * ___textLabel_39;
	// UIPopupList/LegacyEvent UIPopupList::mLegacyEvent
	LegacyEvent_t524649176 * ___mLegacyEvent_40;
	// System.Boolean UIPopupList::mExecuting
	bool ___mExecuting_41;
	// System.Boolean UIPopupList::mUseDynamicFont
	bool ___mUseDynamicFont_42;
	// System.Boolean UIPopupList::mTweening
	bool ___mTweening_43;
	// UnityEngine.GameObject UIPopupList::source
	GameObject_t3674682005 * ___source_44;

public:
	inline static int32_t get_offset_of_atlas_6() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___atlas_6)); }
	inline UIAtlas_t281921111 * get_atlas_6() const { return ___atlas_6; }
	inline UIAtlas_t281921111 ** get_address_of_atlas_6() { return &___atlas_6; }
	inline void set_atlas_6(UIAtlas_t281921111 * value)
	{
		___atlas_6 = value;
		Il2CppCodeGenWriteBarrier(&___atlas_6, value);
	}

	inline static int32_t get_offset_of_bitmapFont_7() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___bitmapFont_7)); }
	inline UIFont_t2503090435 * get_bitmapFont_7() const { return ___bitmapFont_7; }
	inline UIFont_t2503090435 ** get_address_of_bitmapFont_7() { return &___bitmapFont_7; }
	inline void set_bitmapFont_7(UIFont_t2503090435 * value)
	{
		___bitmapFont_7 = value;
		Il2CppCodeGenWriteBarrier(&___bitmapFont_7, value);
	}

	inline static int32_t get_offset_of_trueTypeFont_8() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___trueTypeFont_8)); }
	inline Font_t4241557075 * get_trueTypeFont_8() const { return ___trueTypeFont_8; }
	inline Font_t4241557075 ** get_address_of_trueTypeFont_8() { return &___trueTypeFont_8; }
	inline void set_trueTypeFont_8(Font_t4241557075 * value)
	{
		___trueTypeFont_8 = value;
		Il2CppCodeGenWriteBarrier(&___trueTypeFont_8, value);
	}

	inline static int32_t get_offset_of_fontSize_9() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___fontSize_9)); }
	inline int32_t get_fontSize_9() const { return ___fontSize_9; }
	inline int32_t* get_address_of_fontSize_9() { return &___fontSize_9; }
	inline void set_fontSize_9(int32_t value)
	{
		___fontSize_9 = value;
	}

	inline static int32_t get_offset_of_fontStyle_10() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___fontStyle_10)); }
	inline int32_t get_fontStyle_10() const { return ___fontStyle_10; }
	inline int32_t* get_address_of_fontStyle_10() { return &___fontStyle_10; }
	inline void set_fontStyle_10(int32_t value)
	{
		___fontStyle_10 = value;
	}

	inline static int32_t get_offset_of_backgroundSprite_11() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___backgroundSprite_11)); }
	inline String_t* get_backgroundSprite_11() const { return ___backgroundSprite_11; }
	inline String_t** get_address_of_backgroundSprite_11() { return &___backgroundSprite_11; }
	inline void set_backgroundSprite_11(String_t* value)
	{
		___backgroundSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_11, value);
	}

	inline static int32_t get_offset_of_highlightSprite_12() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___highlightSprite_12)); }
	inline String_t* get_highlightSprite_12() const { return ___highlightSprite_12; }
	inline String_t** get_address_of_highlightSprite_12() { return &___highlightSprite_12; }
	inline void set_highlightSprite_12(String_t* value)
	{
		___highlightSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___highlightSprite_12, value);
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___position_13)); }
	inline int32_t get_position_13() const { return ___position_13; }
	inline int32_t* get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(int32_t value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_alignment_14() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___alignment_14)); }
	inline int32_t get_alignment_14() const { return ___alignment_14; }
	inline int32_t* get_address_of_alignment_14() { return &___alignment_14; }
	inline void set_alignment_14(int32_t value)
	{
		___alignment_14 = value;
	}

	inline static int32_t get_offset_of_items_15() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___items_15)); }
	inline List_1_t1375417109 * get_items_15() const { return ___items_15; }
	inline List_1_t1375417109 ** get_address_of_items_15() { return &___items_15; }
	inline void set_items_15(List_1_t1375417109 * value)
	{
		___items_15 = value;
		Il2CppCodeGenWriteBarrier(&___items_15, value);
	}

	inline static int32_t get_offset_of_itemData_16() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___itemData_16)); }
	inline List_1_t1244034627 * get_itemData_16() const { return ___itemData_16; }
	inline List_1_t1244034627 ** get_address_of_itemData_16() { return &___itemData_16; }
	inline void set_itemData_16(List_1_t1244034627 * value)
	{
		___itemData_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemData_16, value);
	}

	inline static int32_t get_offset_of_padding_17() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___padding_17)); }
	inline Vector2_t4282066565  get_padding_17() const { return ___padding_17; }
	inline Vector2_t4282066565 * get_address_of_padding_17() { return &___padding_17; }
	inline void set_padding_17(Vector2_t4282066565  value)
	{
		___padding_17 = value;
	}

	inline static int32_t get_offset_of_textColor_18() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___textColor_18)); }
	inline Color_t4194546905  get_textColor_18() const { return ___textColor_18; }
	inline Color_t4194546905 * get_address_of_textColor_18() { return &___textColor_18; }
	inline void set_textColor_18(Color_t4194546905  value)
	{
		___textColor_18 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_19() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___backgroundColor_19)); }
	inline Color_t4194546905  get_backgroundColor_19() const { return ___backgroundColor_19; }
	inline Color_t4194546905 * get_address_of_backgroundColor_19() { return &___backgroundColor_19; }
	inline void set_backgroundColor_19(Color_t4194546905  value)
	{
		___backgroundColor_19 = value;
	}

	inline static int32_t get_offset_of_highlightColor_20() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___highlightColor_20)); }
	inline Color_t4194546905  get_highlightColor_20() const { return ___highlightColor_20; }
	inline Color_t4194546905 * get_address_of_highlightColor_20() { return &___highlightColor_20; }
	inline void set_highlightColor_20(Color_t4194546905  value)
	{
		___highlightColor_20 = value;
	}

	inline static int32_t get_offset_of_isAnimated_21() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___isAnimated_21)); }
	inline bool get_isAnimated_21() const { return ___isAnimated_21; }
	inline bool* get_address_of_isAnimated_21() { return &___isAnimated_21; }
	inline void set_isAnimated_21(bool value)
	{
		___isAnimated_21 = value;
	}

	inline static int32_t get_offset_of_isLocalized_22() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___isLocalized_22)); }
	inline bool get_isLocalized_22() const { return ___isLocalized_22; }
	inline bool* get_address_of_isLocalized_22() { return &___isLocalized_22; }
	inline void set_isLocalized_22(bool value)
	{
		___isLocalized_22 = value;
	}

	inline static int32_t get_offset_of_separatePanel_23() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___separatePanel_23)); }
	inline bool get_separatePanel_23() const { return ___separatePanel_23; }
	inline bool* get_address_of_separatePanel_23() { return &___separatePanel_23; }
	inline void set_separatePanel_23(bool value)
	{
		___separatePanel_23 = value;
	}

	inline static int32_t get_offset_of_openOn_24() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___openOn_24)); }
	inline int32_t get_openOn_24() const { return ___openOn_24; }
	inline int32_t* get_address_of_openOn_24() { return &___openOn_24; }
	inline void set_openOn_24(int32_t value)
	{
		___openOn_24 = value;
	}

	inline static int32_t get_offset_of_onChange_25() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___onChange_25)); }
	inline List_1_t1077642479 * get_onChange_25() const { return ___onChange_25; }
	inline List_1_t1077642479 ** get_address_of_onChange_25() { return &___onChange_25; }
	inline void set_onChange_25(List_1_t1077642479 * value)
	{
		___onChange_25 = value;
		Il2CppCodeGenWriteBarrier(&___onChange_25, value);
	}

	inline static int32_t get_offset_of_mSelectedItem_26() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mSelectedItem_26)); }
	inline String_t* get_mSelectedItem_26() const { return ___mSelectedItem_26; }
	inline String_t** get_address_of_mSelectedItem_26() { return &___mSelectedItem_26; }
	inline void set_mSelectedItem_26(String_t* value)
	{
		___mSelectedItem_26 = value;
		Il2CppCodeGenWriteBarrier(&___mSelectedItem_26, value);
	}

	inline static int32_t get_offset_of_mPanel_27() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mPanel_27)); }
	inline UIPanel_t295209936 * get_mPanel_27() const { return ___mPanel_27; }
	inline UIPanel_t295209936 ** get_address_of_mPanel_27() { return &___mPanel_27; }
	inline void set_mPanel_27(UIPanel_t295209936 * value)
	{
		___mPanel_27 = value;
		Il2CppCodeGenWriteBarrier(&___mPanel_27, value);
	}

	inline static int32_t get_offset_of_mBackground_28() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mBackground_28)); }
	inline UISprite_t661437049 * get_mBackground_28() const { return ___mBackground_28; }
	inline UISprite_t661437049 ** get_address_of_mBackground_28() { return &___mBackground_28; }
	inline void set_mBackground_28(UISprite_t661437049 * value)
	{
		___mBackground_28 = value;
		Il2CppCodeGenWriteBarrier(&___mBackground_28, value);
	}

	inline static int32_t get_offset_of_mHighlight_29() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mHighlight_29)); }
	inline UISprite_t661437049 * get_mHighlight_29() const { return ___mHighlight_29; }
	inline UISprite_t661437049 ** get_address_of_mHighlight_29() { return &___mHighlight_29; }
	inline void set_mHighlight_29(UISprite_t661437049 * value)
	{
		___mHighlight_29 = value;
		Il2CppCodeGenWriteBarrier(&___mHighlight_29, value);
	}

	inline static int32_t get_offset_of_mHighlightedLabel_30() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mHighlightedLabel_30)); }
	inline UILabel_t291504320 * get_mHighlightedLabel_30() const { return ___mHighlightedLabel_30; }
	inline UILabel_t291504320 ** get_address_of_mHighlightedLabel_30() { return &___mHighlightedLabel_30; }
	inline void set_mHighlightedLabel_30(UILabel_t291504320 * value)
	{
		___mHighlightedLabel_30 = value;
		Il2CppCodeGenWriteBarrier(&___mHighlightedLabel_30, value);
	}

	inline static int32_t get_offset_of_mLabelList_31() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mLabelList_31)); }
	inline List_1_t1659689872 * get_mLabelList_31() const { return ___mLabelList_31; }
	inline List_1_t1659689872 ** get_address_of_mLabelList_31() { return &___mLabelList_31; }
	inline void set_mLabelList_31(List_1_t1659689872 * value)
	{
		___mLabelList_31 = value;
		Il2CppCodeGenWriteBarrier(&___mLabelList_31, value);
	}

	inline static int32_t get_offset_of_mBgBorder_32() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mBgBorder_32)); }
	inline float get_mBgBorder_32() const { return ___mBgBorder_32; }
	inline float* get_address_of_mBgBorder_32() { return &___mBgBorder_32; }
	inline void set_mBgBorder_32(float value)
	{
		___mBgBorder_32 = value;
	}

	inline static int32_t get_offset_of_mSelection_33() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mSelection_33)); }
	inline GameObject_t3674682005 * get_mSelection_33() const { return ___mSelection_33; }
	inline GameObject_t3674682005 ** get_address_of_mSelection_33() { return &___mSelection_33; }
	inline void set_mSelection_33(GameObject_t3674682005 * value)
	{
		___mSelection_33 = value;
		Il2CppCodeGenWriteBarrier(&___mSelection_33, value);
	}

	inline static int32_t get_offset_of_mOpenFrame_34() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mOpenFrame_34)); }
	inline int32_t get_mOpenFrame_34() const { return ___mOpenFrame_34; }
	inline int32_t* get_address_of_mOpenFrame_34() { return &___mOpenFrame_34; }
	inline void set_mOpenFrame_34(int32_t value)
	{
		___mOpenFrame_34 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_35() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___eventReceiver_35)); }
	inline GameObject_t3674682005 * get_eventReceiver_35() const { return ___eventReceiver_35; }
	inline GameObject_t3674682005 ** get_address_of_eventReceiver_35() { return &___eventReceiver_35; }
	inline void set_eventReceiver_35(GameObject_t3674682005 * value)
	{
		___eventReceiver_35 = value;
		Il2CppCodeGenWriteBarrier(&___eventReceiver_35, value);
	}

	inline static int32_t get_offset_of_functionName_36() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___functionName_36)); }
	inline String_t* get_functionName_36() const { return ___functionName_36; }
	inline String_t** get_address_of_functionName_36() { return &___functionName_36; }
	inline void set_functionName_36(String_t* value)
	{
		___functionName_36 = value;
		Il2CppCodeGenWriteBarrier(&___functionName_36, value);
	}

	inline static int32_t get_offset_of_textScale_37() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___textScale_37)); }
	inline float get_textScale_37() const { return ___textScale_37; }
	inline float* get_address_of_textScale_37() { return &___textScale_37; }
	inline void set_textScale_37(float value)
	{
		___textScale_37 = value;
	}

	inline static int32_t get_offset_of_font_38() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___font_38)); }
	inline UIFont_t2503090435 * get_font_38() const { return ___font_38; }
	inline UIFont_t2503090435 ** get_address_of_font_38() { return &___font_38; }
	inline void set_font_38(UIFont_t2503090435 * value)
	{
		___font_38 = value;
		Il2CppCodeGenWriteBarrier(&___font_38, value);
	}

	inline static int32_t get_offset_of_textLabel_39() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___textLabel_39)); }
	inline UILabel_t291504320 * get_textLabel_39() const { return ___textLabel_39; }
	inline UILabel_t291504320 ** get_address_of_textLabel_39() { return &___textLabel_39; }
	inline void set_textLabel_39(UILabel_t291504320 * value)
	{
		___textLabel_39 = value;
		Il2CppCodeGenWriteBarrier(&___textLabel_39, value);
	}

	inline static int32_t get_offset_of_mLegacyEvent_40() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mLegacyEvent_40)); }
	inline LegacyEvent_t524649176 * get_mLegacyEvent_40() const { return ___mLegacyEvent_40; }
	inline LegacyEvent_t524649176 ** get_address_of_mLegacyEvent_40() { return &___mLegacyEvent_40; }
	inline void set_mLegacyEvent_40(LegacyEvent_t524649176 * value)
	{
		___mLegacyEvent_40 = value;
		Il2CppCodeGenWriteBarrier(&___mLegacyEvent_40, value);
	}

	inline static int32_t get_offset_of_mExecuting_41() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mExecuting_41)); }
	inline bool get_mExecuting_41() const { return ___mExecuting_41; }
	inline bool* get_address_of_mExecuting_41() { return &___mExecuting_41; }
	inline void set_mExecuting_41(bool value)
	{
		___mExecuting_41 = value;
	}

	inline static int32_t get_offset_of_mUseDynamicFont_42() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mUseDynamicFont_42)); }
	inline bool get_mUseDynamicFont_42() const { return ___mUseDynamicFont_42; }
	inline bool* get_address_of_mUseDynamicFont_42() { return &___mUseDynamicFont_42; }
	inline void set_mUseDynamicFont_42(bool value)
	{
		___mUseDynamicFont_42 = value;
	}

	inline static int32_t get_offset_of_mTweening_43() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___mTweening_43)); }
	inline bool get_mTweening_43() const { return ___mTweening_43; }
	inline bool* get_address_of_mTweening_43() { return &___mTweening_43; }
	inline void set_mTweening_43(bool value)
	{
		___mTweening_43 = value;
	}

	inline static int32_t get_offset_of_source_44() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942, ___source_44)); }
	inline GameObject_t3674682005 * get_source_44() const { return ___source_44; }
	inline GameObject_t3674682005 ** get_address_of_source_44() { return &___source_44; }
	inline void set_source_44(GameObject_t3674682005 * value)
	{
		___source_44 = value;
		Il2CppCodeGenWriteBarrier(&___source_44, value);
	}
};

struct UIPopupList_t1804933942_StaticFields
{
public:
	// UIPopupList UIPopupList::current
	UIPopupList_t1804933942 * ___current_3;
	// UnityEngine.GameObject UIPopupList::mChild
	GameObject_t3674682005 * ___mChild_4;
	// System.Single UIPopupList::mFadeOutComplete
	float ___mFadeOutComplete_5;

public:
	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942_StaticFields, ___current_3)); }
	inline UIPopupList_t1804933942 * get_current_3() const { return ___current_3; }
	inline UIPopupList_t1804933942 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UIPopupList_t1804933942 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier(&___current_3, value);
	}

	inline static int32_t get_offset_of_mChild_4() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942_StaticFields, ___mChild_4)); }
	inline GameObject_t3674682005 * get_mChild_4() const { return ___mChild_4; }
	inline GameObject_t3674682005 ** get_address_of_mChild_4() { return &___mChild_4; }
	inline void set_mChild_4(GameObject_t3674682005 * value)
	{
		___mChild_4 = value;
		Il2CppCodeGenWriteBarrier(&___mChild_4, value);
	}

	inline static int32_t get_offset_of_mFadeOutComplete_5() { return static_cast<int32_t>(offsetof(UIPopupList_t1804933942_StaticFields, ___mFadeOutComplete_5)); }
	inline float get_mFadeOutComplete_5() const { return ___mFadeOutComplete_5; }
	inline float* get_address_of_mFadeOutComplete_5() { return &___mFadeOutComplete_5; }
	inline void set_mFadeOutComplete_5(float value)
	{
		___mFadeOutComplete_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
