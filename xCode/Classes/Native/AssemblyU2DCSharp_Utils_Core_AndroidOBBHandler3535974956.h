﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode>
struct Action_1_t3655021551;
// System.Action
struct Action_t3771233898;
// System.Action`1<System.Single>
struct Action_1_t392767812;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Utils_SingletonObject_1_gen3364670738.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.Core.AndroidOBBHandler
struct  AndroidOBBHandler_t3535974956  : public SingletonObject_1_t3364670738
{
public:
	// System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode> Utils.Core.AndroidOBBHandler::OnError
	Action_1_t3655021551 * ___OnError_3;
	// System.Action Utils.Core.AndroidOBBHandler::OnCompleted
	Action_t3771233898 * ___OnCompleted_4;
	// System.Action`1<System.Single> Utils.Core.AndroidOBBHandler::OnProgress
	Action_1_t392767812 * ___OnProgress_5;
	// System.String Utils.Core.AndroidOBBHandler::_expPath
	String_t* ____expPath_6;
	// System.Boolean Utils.Core.AndroidOBBHandler::_downloadStarted
	bool ____downloadStarted_7;
	// System.String Utils.Core.AndroidOBBHandler::<base64AppKey>k__BackingField
	String_t* ___U3Cbase64AppKeyU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_OnError_3() { return static_cast<int32_t>(offsetof(AndroidOBBHandler_t3535974956, ___OnError_3)); }
	inline Action_1_t3655021551 * get_OnError_3() const { return ___OnError_3; }
	inline Action_1_t3655021551 ** get_address_of_OnError_3() { return &___OnError_3; }
	inline void set_OnError_3(Action_1_t3655021551 * value)
	{
		___OnError_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnError_3, value);
	}

	inline static int32_t get_offset_of_OnCompleted_4() { return static_cast<int32_t>(offsetof(AndroidOBBHandler_t3535974956, ___OnCompleted_4)); }
	inline Action_t3771233898 * get_OnCompleted_4() const { return ___OnCompleted_4; }
	inline Action_t3771233898 ** get_address_of_OnCompleted_4() { return &___OnCompleted_4; }
	inline void set_OnCompleted_4(Action_t3771233898 * value)
	{
		___OnCompleted_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnCompleted_4, value);
	}

	inline static int32_t get_offset_of_OnProgress_5() { return static_cast<int32_t>(offsetof(AndroidOBBHandler_t3535974956, ___OnProgress_5)); }
	inline Action_1_t392767812 * get_OnProgress_5() const { return ___OnProgress_5; }
	inline Action_1_t392767812 ** get_address_of_OnProgress_5() { return &___OnProgress_5; }
	inline void set_OnProgress_5(Action_1_t392767812 * value)
	{
		___OnProgress_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnProgress_5, value);
	}

	inline static int32_t get_offset_of__expPath_6() { return static_cast<int32_t>(offsetof(AndroidOBBHandler_t3535974956, ____expPath_6)); }
	inline String_t* get__expPath_6() const { return ____expPath_6; }
	inline String_t** get_address_of__expPath_6() { return &____expPath_6; }
	inline void set__expPath_6(String_t* value)
	{
		____expPath_6 = value;
		Il2CppCodeGenWriteBarrier(&____expPath_6, value);
	}

	inline static int32_t get_offset_of__downloadStarted_7() { return static_cast<int32_t>(offsetof(AndroidOBBHandler_t3535974956, ____downloadStarted_7)); }
	inline bool get__downloadStarted_7() const { return ____downloadStarted_7; }
	inline bool* get_address_of__downloadStarted_7() { return &____downloadStarted_7; }
	inline void set__downloadStarted_7(bool value)
	{
		____downloadStarted_7 = value;
	}

	inline static int32_t get_offset_of_U3Cbase64AppKeyU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AndroidOBBHandler_t3535974956, ___U3Cbase64AppKeyU3Ek__BackingField_8)); }
	inline String_t* get_U3Cbase64AppKeyU3Ek__BackingField_8() const { return ___U3Cbase64AppKeyU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3Cbase64AppKeyU3Ek__BackingField_8() { return &___U3Cbase64AppKeyU3Ek__BackingField_8; }
	inline void set_U3Cbase64AppKeyU3Ek__BackingField_8(String_t* value)
	{
		___U3Cbase64AppKeyU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cbase64AppKeyU3Ek__BackingField_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
