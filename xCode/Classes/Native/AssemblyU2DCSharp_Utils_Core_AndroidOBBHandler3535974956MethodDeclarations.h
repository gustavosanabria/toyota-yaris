﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Core.AndroidOBBHandler
struct AndroidOBBHandler_t3535974956;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Utils.Core.AndroidOBBHandler::.ctor()
extern "C"  void AndroidOBBHandler__ctor_m3730580739 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.AndroidOBBHandler::get_base64AppKey()
extern "C"  String_t* AndroidOBBHandler_get_base64AppKey_m2086676314 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.AndroidOBBHandler::set_base64AppKey(System.String)
extern "C"  void AndroidOBBHandler_set_base64AppKey_m2997703735 (AndroidOBBHandler_t3535974956 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.AndroidOBBHandler::SetAppKey(System.String)
extern "C"  void AndroidOBBHandler_SetAppKey_m3889606401 (AndroidOBBHandler_t3535974956 * __this, String_t* ___appKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utils.Core.AndroidOBBHandler::StartDownload()
extern "C"  bool AndroidOBBHandler_StartDownload_m3227493495 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Utils.Core.AndroidOBBHandler::download()
extern "C"  Il2CppObject * AndroidOBBHandler_download_m1886416017 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
