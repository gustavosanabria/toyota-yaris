﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Core.AndroidOBBHandler/<download>c__IteratorE
struct U3CdownloadU3Ec__IteratorE_t3158326693;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils.Core.AndroidOBBHandler/<download>c__IteratorE::.ctor()
extern "C"  void U3CdownloadU3Ec__IteratorE__ctor_m1812119398 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Utils.Core.AndroidOBBHandler/<download>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4219348012 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Utils.Core.AndroidOBBHandler/<download>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m720764864 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utils.Core.AndroidOBBHandler/<download>c__IteratorE::MoveNext()
extern "C"  bool U3CdownloadU3Ec__IteratorE_MoveNext_m816774862 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.AndroidOBBHandler/<download>c__IteratorE::Dispose()
extern "C"  void U3CdownloadU3Ec__IteratorE_Dispose_m1883269667 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.AndroidOBBHandler/<download>c__IteratorE::Reset()
extern "C"  void U3CdownloadU3Ec__IteratorE_Reset_m3753519635 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
