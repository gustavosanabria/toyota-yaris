﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.FileStream
struct FileStream_t2141505868;
// System.IO.BinaryWriter
struct BinaryWriter_t4146364100;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.Core.DataBuilder
struct  DataBuilder_t3810402835  : public Il2CppObject
{
public:
	// System.IO.FileStream Utils.Core.DataBuilder::_file
	FileStream_t2141505868 * ____file_0;
	// System.IO.BinaryWriter Utils.Core.DataBuilder::_writer
	BinaryWriter_t4146364100 * ____writer_1;

public:
	inline static int32_t get_offset_of__file_0() { return static_cast<int32_t>(offsetof(DataBuilder_t3810402835, ____file_0)); }
	inline FileStream_t2141505868 * get__file_0() const { return ____file_0; }
	inline FileStream_t2141505868 ** get_address_of__file_0() { return &____file_0; }
	inline void set__file_0(FileStream_t2141505868 * value)
	{
		____file_0 = value;
		Il2CppCodeGenWriteBarrier(&____file_0, value);
	}

	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(DataBuilder_t3810402835, ____writer_1)); }
	inline BinaryWriter_t4146364100 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t4146364100 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t4146364100 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier(&____writer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
