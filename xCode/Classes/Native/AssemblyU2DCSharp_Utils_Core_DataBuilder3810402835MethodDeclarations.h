﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Core.DataBuilder
struct DataBuilder_t3810402835;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Utils.Core.DataBuilder::.ctor(System.String)
extern "C"  void DataBuilder__ctor_m2749382022 (DataBuilder_t3810402835 * __this, String_t* ____path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.DataBuilder::Add(System.String)
extern "C"  void DataBuilder_Add_m3818414119 (DataBuilder_t3810402835 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.DataBuilder::End()
extern "C"  void DataBuilder_End_m306851957 (DataBuilder_t3810402835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
