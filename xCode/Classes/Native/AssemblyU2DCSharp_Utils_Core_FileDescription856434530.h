﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.Core.FileDescription
struct  FileDescription_t856434530  : public Il2CppObject
{
public:
	// System.String Utils.Core.FileDescription::<path>k__BackingField
	String_t* ___U3CpathU3Ek__BackingField_0;
	// System.String Utils.Core.FileDescription::<filename>k__BackingField
	String_t* ___U3CfilenameU3Ek__BackingField_1;
	// System.String Utils.Core.FileDescription::<extension>k__BackingField
	String_t* ___U3CextensionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FileDescription_t856434530, ___U3CpathU3Ek__BackingField_0)); }
	inline String_t* get_U3CpathU3Ek__BackingField_0() const { return ___U3CpathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CpathU3Ek__BackingField_0() { return &___U3CpathU3Ek__BackingField_0; }
	inline void set_U3CpathU3Ek__BackingField_0(String_t* value)
	{
		___U3CpathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CfilenameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FileDescription_t856434530, ___U3CfilenameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfilenameU3Ek__BackingField_1() const { return ___U3CfilenameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfilenameU3Ek__BackingField_1() { return &___U3CfilenameU3Ek__BackingField_1; }
	inline void set_U3CfilenameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfilenameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilenameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CextensionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FileDescription_t856434530, ___U3CextensionU3Ek__BackingField_2)); }
	inline String_t* get_U3CextensionU3Ek__BackingField_2() const { return ___U3CextensionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CextensionU3Ek__BackingField_2() { return &___U3CextensionU3Ek__BackingField_2; }
	inline void set_U3CextensionU3Ek__BackingField_2(String_t* value)
	{
		___U3CextensionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CextensionU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
