﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Core.FileDescription
struct FileDescription_t856434530;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Utils.Core.FileDescription::.ctor()
extern "C"  void FileDescription__ctor_m2891698829 (FileDescription_t856434530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileDescription::get_path()
extern "C"  String_t* FileDescription_get_path_m682857928 (FileDescription_t856434530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.FileDescription::set_path(System.String)
extern "C"  void FileDescription_set_path_m1773124681 (FileDescription_t856434530 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileDescription::get_filename()
extern "C"  String_t* FileDescription_get_filename_m2317865514 (FileDescription_t856434530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.FileDescription::set_filename(System.String)
extern "C"  void FileDescription_set_filename_m2885717927 (FileDescription_t856434530 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileDescription::get_extension()
extern "C"  String_t* FileDescription_get_extension_m920864990 (FileDescription_t856434530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.FileDescription::set_extension(System.String)
extern "C"  void FileDescription_set_extension_m1352110645 (FileDescription_t856434530 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
