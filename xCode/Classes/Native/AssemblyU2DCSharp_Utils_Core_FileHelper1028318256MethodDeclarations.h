﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Core.FileHelper
struct FileHelper_t1028318256;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// Utils.Core.FileDescription
struct FileDescription_t856434530;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Utils.Core.DataBuilder
struct DataBuilder_t3810402835;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_EFolder738843808.h"

// System.Void Utils.Core.FileHelper::.ctor()
extern "C"  void FileHelper__ctor_m634924623 (FileHelper_t1028318256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utils.Core.FileHelper::GetDownloadStream(System.String,UnityEngine.WWW&,System.String)
extern "C"  bool FileHelper_GetDownloadStream_m219723097 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWW_t3134621005 ** ___www1, String_t* ____folder2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Utils.Core.FileDescription Utils.Core.FileHelper::GetFileDescription(System.String)
extern "C"  FileDescription_t856434530 * FileHelper_GetFileDescription_m4037595026 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::PersistentDataPath(System.String,System.Boolean)
extern "C"  String_t* FileHelper_PersistentDataPath_m2607611251 (Il2CppObject * __this /* static, unused */, String_t* ___file0, bool ___filePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::StreamingDataPath(System.String)
extern "C"  String_t* FileHelper_StreamingDataPath_m929338273 (Il2CppObject * __this /* static, unused */, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::DataPath(Utils.Core.FileHelper/EFolder,System.String)
extern "C"  String_t* FileHelper_DataPath_m1267838125 (Il2CppObject * __this /* static, unused */, int32_t ___folder0, String_t* ___file1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.FileHelper::CreateFoldersForFile(System.String,Utils.Core.FileHelper/EFolder)
extern "C"  void FileHelper_CreateFoldersForFile_m576049309 (Il2CppObject * __this /* static, unused */, String_t* ___file0, int32_t ___folder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::CopyToPersistent(System.String,System.Boolean)
extern "C"  String_t* FileHelper_CopyToPersistent_m1849145044 (Il2CppObject * __this /* static, unused */, String_t* ___file0, bool ___overwrite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Utils.Core.FileHelper::CopyToPersistent(System.String,System.Action`1<System.String>,System.Boolean)
extern "C"  Il2CppObject * FileHelper_CopyToPersistent_m1002064137 (Il2CppObject * __this /* static, unused */, String_t* ___file0, Action_1_t403047693 * ___callback1, bool ___overwrite2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::ReadText(System.String)
extern "C"  String_t* FileHelper_ReadText_m1911600301 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Utils.Core.FileHelper::Read(System.String)
extern "C"  ByteU5BU5D_t4260760469* FileHelper_Read_m2873044513 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::Write(System.String,System.Byte[])
extern "C"  String_t* FileHelper_Write_m1586799798 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils.Core.FileHelper::Write(System.String,System.String)
extern "C"  String_t* FileHelper_Write_m3644605423 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, String_t* ___stringData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Utils.Core.DataBuilder Utils.Core.FileHelper::BeginWrite(System.String)
extern "C"  DataBuilder_t3810402835 * FileHelper_BeginWrite_m9346805 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utils.Core.FileHelper::Delete(System.String)
extern "C"  bool FileHelper_Delete_m3140953270 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utils.Core.FileHelper::Exists(System.String,System.Boolean)
extern "C"  bool FileHelper_Exists_m3784167160 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___isAbsulute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Utils.Core.FileHelper::GetFiles(System.String,Utils.Core.FileHelper/EFolder)
extern "C"  StringU5BU5D_t4054002952* FileHelper_GetFiles_m975275321 (Il2CppObject * __this /* static, unused */, String_t* ____root0, int32_t ___folder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
