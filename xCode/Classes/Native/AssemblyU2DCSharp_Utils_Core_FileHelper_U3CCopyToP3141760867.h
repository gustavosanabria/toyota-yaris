﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF
struct  U3CCopyToPersistentU3Ec__IteratorF_t3141760867  : public Il2CppObject
{
public:
	// System.String Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::file
	String_t* ___file_0;
	// System.String Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::<_path>__0
	String_t* ___U3C_pathU3E__0_1;
	// System.Boolean Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::overwrite
	bool ___overwrite_2;
	// System.String Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::<_source>__1
	String_t* ___U3C_sourceU3E__1_3;
	// System.Action`1<System.String> Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::callback
	Action_1_t403047693 * ___callback_4;
	// System.Int32 Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::$PC
	int32_t ___U24PC_5;
	// System.Object Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::$current
	Il2CppObject * ___U24current_6;
	// System.String Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::<$>file
	String_t* ___U3CU24U3Efile_7;
	// System.Boolean Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::<$>overwrite
	bool ___U3CU24U3Eoverwrite_8;
	// System.Action`1<System.String> Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::<$>callback
	Action_1_t403047693 * ___U3CU24U3Ecallback_9;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___file_0)); }
	inline String_t* get_file_0() const { return ___file_0; }
	inline String_t** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(String_t* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier(&___file_0, value);
	}

	inline static int32_t get_offset_of_U3C_pathU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U3C_pathU3E__0_1)); }
	inline String_t* get_U3C_pathU3E__0_1() const { return ___U3C_pathU3E__0_1; }
	inline String_t** get_address_of_U3C_pathU3E__0_1() { return &___U3C_pathU3E__0_1; }
	inline void set_U3C_pathU3E__0_1(String_t* value)
	{
		___U3C_pathU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_pathU3E__0_1, value);
	}

	inline static int32_t get_offset_of_overwrite_2() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___overwrite_2)); }
	inline bool get_overwrite_2() const { return ___overwrite_2; }
	inline bool* get_address_of_overwrite_2() { return &___overwrite_2; }
	inline void set_overwrite_2(bool value)
	{
		___overwrite_2 = value;
	}

	inline static int32_t get_offset_of_U3C_sourceU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U3C_sourceU3E__1_3)); }
	inline String_t* get_U3C_sourceU3E__1_3() const { return ___U3C_sourceU3E__1_3; }
	inline String_t** get_address_of_U3C_sourceU3E__1_3() { return &___U3C_sourceU3E__1_3; }
	inline void set_U3C_sourceU3E__1_3(String_t* value)
	{
		___U3C_sourceU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_sourceU3E__1_3, value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___callback_4)); }
	inline Action_1_t403047693 * get_callback_4() const { return ___callback_4; }
	inline Action_1_t403047693 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t403047693 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier(&___callback_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efile_7() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U3CU24U3Efile_7)); }
	inline String_t* get_U3CU24U3Efile_7() const { return ___U3CU24U3Efile_7; }
	inline String_t** get_address_of_U3CU24U3Efile_7() { return &___U3CU24U3Efile_7; }
	inline void set_U3CU24U3Efile_7(String_t* value)
	{
		___U3CU24U3Efile_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efile_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eoverwrite_8() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U3CU24U3Eoverwrite_8)); }
	inline bool get_U3CU24U3Eoverwrite_8() const { return ___U3CU24U3Eoverwrite_8; }
	inline bool* get_address_of_U3CU24U3Eoverwrite_8() { return &___U3CU24U3Eoverwrite_8; }
	inline void set_U3CU24U3Eoverwrite_8(bool value)
	{
		___U3CU24U3Eoverwrite_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecallback_9() { return static_cast<int32_t>(offsetof(U3CCopyToPersistentU3Ec__IteratorF_t3141760867, ___U3CU24U3Ecallback_9)); }
	inline Action_1_t403047693 * get_U3CU24U3Ecallback_9() const { return ___U3CU24U3Ecallback_9; }
	inline Action_1_t403047693 ** get_address_of_U3CU24U3Ecallback_9() { return &___U3CU24U3Ecallback_9; }
	inline void set_U3CU24U3Ecallback_9(Action_1_t403047693 * value)
	{
		___U3CU24U3Ecallback_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecallback_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
