﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF
struct U3CCopyToPersistentU3Ec__IteratorF_t3141760867;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::.ctor()
extern "C"  void U3CCopyToPersistentU3Ec__IteratorF__ctor_m2209555096 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCopyToPersistentU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1477433796 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCopyToPersistentU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3440764248 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::MoveNext()
extern "C"  bool U3CCopyToPersistentU3Ec__IteratorF_MoveNext_m1207548484 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::Dispose()
extern "C"  void U3CCopyToPersistentU3Ec__IteratorF_Dispose_m1566886101 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::Reset()
extern "C"  void U3CCopyToPersistentU3Ec__IteratorF_Reset_m4150955333 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
