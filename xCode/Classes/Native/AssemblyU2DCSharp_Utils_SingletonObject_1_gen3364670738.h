﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Utils.Core.AndroidOBBHandler
struct AndroidOBBHandler_t3535974956;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SingletonObject`1<Utils.Core.AndroidOBBHandler>
struct  SingletonObject_1_t3364670738  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SingletonObject_1_t3364670738_StaticFields
{
public:
	// _Ty Utils.SingletonObject`1::_instance
	AndroidOBBHandler_t3535974956 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(SingletonObject_1_t3364670738_StaticFields, ____instance_2)); }
	inline AndroidOBBHandler_t3535974956 * get__instance_2() const { return ____instance_2; }
	inline AndroidOBBHandler_t3535974956 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(AndroidOBBHandler_t3535974956 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
