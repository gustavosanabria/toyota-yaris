﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Utils_SingletonObject_1_gen3999512153MethodDeclarations.h"

// System.Void Utils.SingletonObject`1<Utils.Core.AndroidOBBHandler>::.ctor()
#define SingletonObject_1__ctor_m4177696451(__this, method) ((  void (*) (SingletonObject_1_t3364670738 *, const MethodInfo*))SingletonObject_1__ctor_m3638598397_gshared)(__this, method)
// System.Void Utils.SingletonObject`1<Utils.Core.AndroidOBBHandler>::.cctor()
#define SingletonObject_1__cctor_m177474890(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1__cctor_m645304400_gshared)(__this /* static, unused */, method)
// _Ty Utils.SingletonObject`1<Utils.Core.AndroidOBBHandler>::get_instance()
#define SingletonObject_1_get_instance_m31326064(__this /* static, unused */, method) ((  AndroidOBBHandler_t3535974956 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1_get_instance_m2836779764_gshared)(__this /* static, unused */, method)
// _Ty Utils.SingletonObject`1<Utils.Core.AndroidOBBHandler>::Get()
#define SingletonObject_1_Get_m3282406662(__this /* static, unused */, method) ((  AndroidOBBHandler_t3535974956 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1_Get_m387773698_gshared)(__this /* static, unused */, method)
// System.Void Utils.SingletonObject`1<Utils.Core.AndroidOBBHandler>::DestroyInstance()
#define SingletonObject_1_DestroyInstance_m2700866256(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1_DestroyInstance_m301299338_gshared)(__this /* static, unused */, method)
