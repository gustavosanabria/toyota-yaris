﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.SingletonObject`1<System.Object>
struct SingletonObject_1_t3999512153;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils.SingletonObject`1<System.Object>::.ctor()
extern "C"  void SingletonObject_1__ctor_m3638598397_gshared (SingletonObject_1_t3999512153 * __this, const MethodInfo* method);
#define SingletonObject_1__ctor_m3638598397(__this, method) ((  void (*) (SingletonObject_1_t3999512153 *, const MethodInfo*))SingletonObject_1__ctor_m3638598397_gshared)(__this, method)
// System.Void Utils.SingletonObject`1<System.Object>::.cctor()
extern "C"  void SingletonObject_1__cctor_m645304400_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SingletonObject_1__cctor_m645304400(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1__cctor_m645304400_gshared)(__this /* static, unused */, method)
// _Ty Utils.SingletonObject`1<System.Object>::get_instance()
extern "C"  Il2CppObject * SingletonObject_1_get_instance_m2836779764_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SingletonObject_1_get_instance_m2836779764(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1_get_instance_m2836779764_gshared)(__this /* static, unused */, method)
// _Ty Utils.SingletonObject`1<System.Object>::Get()
extern "C"  Il2CppObject * SingletonObject_1_Get_m387773698_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SingletonObject_1_Get_m387773698(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1_Get_m387773698_gshared)(__this /* static, unused */, method)
// System.Void Utils.SingletonObject`1<System.Object>::DestroyInstance()
extern "C"  void SingletonObject_1_DestroyInstance_m301299338_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SingletonObject_1_DestroyInstance_m301299338(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonObject_1_DestroyInstance_m301299338_gshared)(__this /* static, unused */, method)
