﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.DataSet
struct DataSet_t2095838082;
// Vuforia.ObjectTracker
struct ObjectTracker_t455954211;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.Vuforia.DataSetHandler
struct  DataSetHandler_t896450754  : public MonoBehaviour_t667441552
{
public:
	// Vuforia.DataSet Utils.Vuforia.DataSetHandler::mDataset
	DataSet_t2095838082 * ___mDataset_2;
	// Vuforia.ObjectTracker Utils.Vuforia.DataSetHandler::tracker
	ObjectTracker_t455954211 * ___tracker_3;
	// System.String Utils.Vuforia.DataSetHandler::_dataSet
	String_t* ____dataSet_4;
	// System.Int32 Utils.Vuforia.DataSetHandler::_projectID
	int32_t ____projectID_5;

public:
	inline static int32_t get_offset_of_mDataset_2() { return static_cast<int32_t>(offsetof(DataSetHandler_t896450754, ___mDataset_2)); }
	inline DataSet_t2095838082 * get_mDataset_2() const { return ___mDataset_2; }
	inline DataSet_t2095838082 ** get_address_of_mDataset_2() { return &___mDataset_2; }
	inline void set_mDataset_2(DataSet_t2095838082 * value)
	{
		___mDataset_2 = value;
		Il2CppCodeGenWriteBarrier(&___mDataset_2, value);
	}

	inline static int32_t get_offset_of_tracker_3() { return static_cast<int32_t>(offsetof(DataSetHandler_t896450754, ___tracker_3)); }
	inline ObjectTracker_t455954211 * get_tracker_3() const { return ___tracker_3; }
	inline ObjectTracker_t455954211 ** get_address_of_tracker_3() { return &___tracker_3; }
	inline void set_tracker_3(ObjectTracker_t455954211 * value)
	{
		___tracker_3 = value;
		Il2CppCodeGenWriteBarrier(&___tracker_3, value);
	}

	inline static int32_t get_offset_of__dataSet_4() { return static_cast<int32_t>(offsetof(DataSetHandler_t896450754, ____dataSet_4)); }
	inline String_t* get__dataSet_4() const { return ____dataSet_4; }
	inline String_t** get_address_of__dataSet_4() { return &____dataSet_4; }
	inline void set__dataSet_4(String_t* value)
	{
		____dataSet_4 = value;
		Il2CppCodeGenWriteBarrier(&____dataSet_4, value);
	}

	inline static int32_t get_offset_of__projectID_5() { return static_cast<int32_t>(offsetof(DataSetHandler_t896450754, ____projectID_5)); }
	inline int32_t get__projectID_5() const { return ____projectID_5; }
	inline int32_t* get_address_of__projectID_5() { return &____projectID_5; }
	inline void set__projectID_5(int32_t value)
	{
		____projectID_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
