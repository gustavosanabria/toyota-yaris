﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils.Vuforia.DataSetHandler
struct DataSetHandler_t896450754;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils.Vuforia.DataSetHandler::.ctor()
extern "C"  void DataSetHandler__ctor_m2282493888 (DataSetHandler_t896450754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Vuforia.DataSetHandler::Start()
extern "C"  void DataSetHandler_Start_m1229631680 (DataSetHandler_t896450754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.Vuforia.DataSetHandler::LoadDataSet()
extern "C"  void DataSetHandler_LoadDataSet_m355100464 (DataSetHandler_t896450754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
