﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardboardHead
struct CardboardHead_t2975823030;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UITexture
struct UITexture_t3903132647;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCameraClamper
struct  VRCameraClamper_t2660686439  : public MonoBehaviour_t667441552
{
public:
	// CardboardHead VRCameraClamper::cBHead
	CardboardHead_t2975823030 * ___cBHead_2;
	// UnityEngine.Transform VRCameraClamper::headTrans
	Transform_t1659122786 * ___headTrans_3;
	// UnityEngine.TextMesh VRCameraClamper::cBHeadRotTM
	TextMesh_t2567681854 * ___cBHeadRotTM_4;
	// System.Single VRCameraClamper::maxUPRot
	float ___maxUPRot_5;
	// System.Single VRCameraClamper::maxDownRot
	float ___maxDownRot_6;
	// System.Boolean VRCameraClamper::debugRotation
	bool ___debugRotation_7;
	// System.Boolean VRCameraClamper::updateRotation
	bool ___updateRotation_8;
	// System.Boolean VRCameraClamper::SmoothRotation
	bool ___SmoothRotation_9;
	// System.Boolean VRCameraClamper::clampRotation
	bool ___clampRotation_10;
	// System.Boolean VRCameraClamper::doubleTapZoom
	bool ___doubleTapZoom_11;
	// System.Single VRCameraClamper::newX
	float ___newX_12;
	// System.Single VRCameraClamper::xRot
	float ___xRot_13;
	// System.Single VRCameraClamper::xRot2
	float ___xRot2_14;
	// System.Single VRCameraClamper::xLimit
	float ___xLimit_15;
	// System.Single VRCameraClamper::originalMaxUpRot
	float ___originalMaxUpRot_16;
	// System.Single VRCameraClamper::originalMaxDownRot
	float ___originalMaxDownRot_17;
	// UnityEngine.Vector3 VRCameraClamper::newVector
	Vector3_t4282066566  ___newVector_18;
	// UnityEngine.Vector3 VRCameraClamper::headRot
	Vector3_t4282066566  ___headRot_19;
	// UnityEngine.Vector3 VRCameraClamper::headRot2
	Vector3_t4282066566  ___headRot2_20;
	// UnityEngine.Texture2D VRCameraClamper::gyroBtnTex
	Texture2D_t3884108195 * ___gyroBtnTex_21;
	// UnityEngine.Texture2D VRCameraClamper::swipeBtnTex
	Texture2D_t3884108195 * ___swipeBtnTex_22;
	// UITexture VRCameraClamper::vrControlModeBtn
	UITexture_t3903132647 * ___vrControlModeBtn_23;
	// System.Boolean VRCameraClamper::enableGyro
	bool ___enableGyro_24;
	// System.Byte VRCameraClamper::tapCount
	uint8_t ___tapCount_25;
	// UnityEngine.Touch VRCameraClamper::t
	Touch_t4210255029  ___t_26;
	// UnityEngine.Touch VRCameraClamper::t1
	Touch_t4210255029  ___t1_27;
	// UnityEngine.Touch VRCameraClamper::t2
	Touch_t4210255029  ___t2_28;
	// System.Boolean VRCameraClamper::zoomIn
	bool ___zoomIn_29;
	// System.Single VRCameraClamper::tolerance
	float ___tolerance_30;
	// System.Single VRCameraClamper::prevDistance
	float ___prevDistance_31;
	// System.Single VRCameraClamper::distance
	float ___distance_32;
	// UnityEngine.Vector3 VRCameraClamper::t1Pos
	Vector3_t4282066566  ___t1Pos_33;
	// UnityEngine.Vector3 VRCameraClamper::t2Pos
	Vector3_t4282066566  ___t2Pos_34;
	// UnityEngine.Vector3 VRCameraClamper::posibleRotation
	Vector3_t4282066566  ___posibleRotation_35;

public:
	inline static int32_t get_offset_of_cBHead_2() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___cBHead_2)); }
	inline CardboardHead_t2975823030 * get_cBHead_2() const { return ___cBHead_2; }
	inline CardboardHead_t2975823030 ** get_address_of_cBHead_2() { return &___cBHead_2; }
	inline void set_cBHead_2(CardboardHead_t2975823030 * value)
	{
		___cBHead_2 = value;
		Il2CppCodeGenWriteBarrier(&___cBHead_2, value);
	}

	inline static int32_t get_offset_of_headTrans_3() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___headTrans_3)); }
	inline Transform_t1659122786 * get_headTrans_3() const { return ___headTrans_3; }
	inline Transform_t1659122786 ** get_address_of_headTrans_3() { return &___headTrans_3; }
	inline void set_headTrans_3(Transform_t1659122786 * value)
	{
		___headTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___headTrans_3, value);
	}

	inline static int32_t get_offset_of_cBHeadRotTM_4() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___cBHeadRotTM_4)); }
	inline TextMesh_t2567681854 * get_cBHeadRotTM_4() const { return ___cBHeadRotTM_4; }
	inline TextMesh_t2567681854 ** get_address_of_cBHeadRotTM_4() { return &___cBHeadRotTM_4; }
	inline void set_cBHeadRotTM_4(TextMesh_t2567681854 * value)
	{
		___cBHeadRotTM_4 = value;
		Il2CppCodeGenWriteBarrier(&___cBHeadRotTM_4, value);
	}

	inline static int32_t get_offset_of_maxUPRot_5() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___maxUPRot_5)); }
	inline float get_maxUPRot_5() const { return ___maxUPRot_5; }
	inline float* get_address_of_maxUPRot_5() { return &___maxUPRot_5; }
	inline void set_maxUPRot_5(float value)
	{
		___maxUPRot_5 = value;
	}

	inline static int32_t get_offset_of_maxDownRot_6() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___maxDownRot_6)); }
	inline float get_maxDownRot_6() const { return ___maxDownRot_6; }
	inline float* get_address_of_maxDownRot_6() { return &___maxDownRot_6; }
	inline void set_maxDownRot_6(float value)
	{
		___maxDownRot_6 = value;
	}

	inline static int32_t get_offset_of_debugRotation_7() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___debugRotation_7)); }
	inline bool get_debugRotation_7() const { return ___debugRotation_7; }
	inline bool* get_address_of_debugRotation_7() { return &___debugRotation_7; }
	inline void set_debugRotation_7(bool value)
	{
		___debugRotation_7 = value;
	}

	inline static int32_t get_offset_of_updateRotation_8() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___updateRotation_8)); }
	inline bool get_updateRotation_8() const { return ___updateRotation_8; }
	inline bool* get_address_of_updateRotation_8() { return &___updateRotation_8; }
	inline void set_updateRotation_8(bool value)
	{
		___updateRotation_8 = value;
	}

	inline static int32_t get_offset_of_SmoothRotation_9() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___SmoothRotation_9)); }
	inline bool get_SmoothRotation_9() const { return ___SmoothRotation_9; }
	inline bool* get_address_of_SmoothRotation_9() { return &___SmoothRotation_9; }
	inline void set_SmoothRotation_9(bool value)
	{
		___SmoothRotation_9 = value;
	}

	inline static int32_t get_offset_of_clampRotation_10() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___clampRotation_10)); }
	inline bool get_clampRotation_10() const { return ___clampRotation_10; }
	inline bool* get_address_of_clampRotation_10() { return &___clampRotation_10; }
	inline void set_clampRotation_10(bool value)
	{
		___clampRotation_10 = value;
	}

	inline static int32_t get_offset_of_doubleTapZoom_11() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___doubleTapZoom_11)); }
	inline bool get_doubleTapZoom_11() const { return ___doubleTapZoom_11; }
	inline bool* get_address_of_doubleTapZoom_11() { return &___doubleTapZoom_11; }
	inline void set_doubleTapZoom_11(bool value)
	{
		___doubleTapZoom_11 = value;
	}

	inline static int32_t get_offset_of_newX_12() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___newX_12)); }
	inline float get_newX_12() const { return ___newX_12; }
	inline float* get_address_of_newX_12() { return &___newX_12; }
	inline void set_newX_12(float value)
	{
		___newX_12 = value;
	}

	inline static int32_t get_offset_of_xRot_13() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___xRot_13)); }
	inline float get_xRot_13() const { return ___xRot_13; }
	inline float* get_address_of_xRot_13() { return &___xRot_13; }
	inline void set_xRot_13(float value)
	{
		___xRot_13 = value;
	}

	inline static int32_t get_offset_of_xRot2_14() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___xRot2_14)); }
	inline float get_xRot2_14() const { return ___xRot2_14; }
	inline float* get_address_of_xRot2_14() { return &___xRot2_14; }
	inline void set_xRot2_14(float value)
	{
		___xRot2_14 = value;
	}

	inline static int32_t get_offset_of_xLimit_15() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___xLimit_15)); }
	inline float get_xLimit_15() const { return ___xLimit_15; }
	inline float* get_address_of_xLimit_15() { return &___xLimit_15; }
	inline void set_xLimit_15(float value)
	{
		___xLimit_15 = value;
	}

	inline static int32_t get_offset_of_originalMaxUpRot_16() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___originalMaxUpRot_16)); }
	inline float get_originalMaxUpRot_16() const { return ___originalMaxUpRot_16; }
	inline float* get_address_of_originalMaxUpRot_16() { return &___originalMaxUpRot_16; }
	inline void set_originalMaxUpRot_16(float value)
	{
		___originalMaxUpRot_16 = value;
	}

	inline static int32_t get_offset_of_originalMaxDownRot_17() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___originalMaxDownRot_17)); }
	inline float get_originalMaxDownRot_17() const { return ___originalMaxDownRot_17; }
	inline float* get_address_of_originalMaxDownRot_17() { return &___originalMaxDownRot_17; }
	inline void set_originalMaxDownRot_17(float value)
	{
		___originalMaxDownRot_17 = value;
	}

	inline static int32_t get_offset_of_newVector_18() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___newVector_18)); }
	inline Vector3_t4282066566  get_newVector_18() const { return ___newVector_18; }
	inline Vector3_t4282066566 * get_address_of_newVector_18() { return &___newVector_18; }
	inline void set_newVector_18(Vector3_t4282066566  value)
	{
		___newVector_18 = value;
	}

	inline static int32_t get_offset_of_headRot_19() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___headRot_19)); }
	inline Vector3_t4282066566  get_headRot_19() const { return ___headRot_19; }
	inline Vector3_t4282066566 * get_address_of_headRot_19() { return &___headRot_19; }
	inline void set_headRot_19(Vector3_t4282066566  value)
	{
		___headRot_19 = value;
	}

	inline static int32_t get_offset_of_headRot2_20() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___headRot2_20)); }
	inline Vector3_t4282066566  get_headRot2_20() const { return ___headRot2_20; }
	inline Vector3_t4282066566 * get_address_of_headRot2_20() { return &___headRot2_20; }
	inline void set_headRot2_20(Vector3_t4282066566  value)
	{
		___headRot2_20 = value;
	}

	inline static int32_t get_offset_of_gyroBtnTex_21() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___gyroBtnTex_21)); }
	inline Texture2D_t3884108195 * get_gyroBtnTex_21() const { return ___gyroBtnTex_21; }
	inline Texture2D_t3884108195 ** get_address_of_gyroBtnTex_21() { return &___gyroBtnTex_21; }
	inline void set_gyroBtnTex_21(Texture2D_t3884108195 * value)
	{
		___gyroBtnTex_21 = value;
		Il2CppCodeGenWriteBarrier(&___gyroBtnTex_21, value);
	}

	inline static int32_t get_offset_of_swipeBtnTex_22() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___swipeBtnTex_22)); }
	inline Texture2D_t3884108195 * get_swipeBtnTex_22() const { return ___swipeBtnTex_22; }
	inline Texture2D_t3884108195 ** get_address_of_swipeBtnTex_22() { return &___swipeBtnTex_22; }
	inline void set_swipeBtnTex_22(Texture2D_t3884108195 * value)
	{
		___swipeBtnTex_22 = value;
		Il2CppCodeGenWriteBarrier(&___swipeBtnTex_22, value);
	}

	inline static int32_t get_offset_of_vrControlModeBtn_23() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___vrControlModeBtn_23)); }
	inline UITexture_t3903132647 * get_vrControlModeBtn_23() const { return ___vrControlModeBtn_23; }
	inline UITexture_t3903132647 ** get_address_of_vrControlModeBtn_23() { return &___vrControlModeBtn_23; }
	inline void set_vrControlModeBtn_23(UITexture_t3903132647 * value)
	{
		___vrControlModeBtn_23 = value;
		Il2CppCodeGenWriteBarrier(&___vrControlModeBtn_23, value);
	}

	inline static int32_t get_offset_of_enableGyro_24() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___enableGyro_24)); }
	inline bool get_enableGyro_24() const { return ___enableGyro_24; }
	inline bool* get_address_of_enableGyro_24() { return &___enableGyro_24; }
	inline void set_enableGyro_24(bool value)
	{
		___enableGyro_24 = value;
	}

	inline static int32_t get_offset_of_tapCount_25() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___tapCount_25)); }
	inline uint8_t get_tapCount_25() const { return ___tapCount_25; }
	inline uint8_t* get_address_of_tapCount_25() { return &___tapCount_25; }
	inline void set_tapCount_25(uint8_t value)
	{
		___tapCount_25 = value;
	}

	inline static int32_t get_offset_of_t_26() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___t_26)); }
	inline Touch_t4210255029  get_t_26() const { return ___t_26; }
	inline Touch_t4210255029 * get_address_of_t_26() { return &___t_26; }
	inline void set_t_26(Touch_t4210255029  value)
	{
		___t_26 = value;
	}

	inline static int32_t get_offset_of_t1_27() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___t1_27)); }
	inline Touch_t4210255029  get_t1_27() const { return ___t1_27; }
	inline Touch_t4210255029 * get_address_of_t1_27() { return &___t1_27; }
	inline void set_t1_27(Touch_t4210255029  value)
	{
		___t1_27 = value;
	}

	inline static int32_t get_offset_of_t2_28() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___t2_28)); }
	inline Touch_t4210255029  get_t2_28() const { return ___t2_28; }
	inline Touch_t4210255029 * get_address_of_t2_28() { return &___t2_28; }
	inline void set_t2_28(Touch_t4210255029  value)
	{
		___t2_28 = value;
	}

	inline static int32_t get_offset_of_zoomIn_29() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___zoomIn_29)); }
	inline bool get_zoomIn_29() const { return ___zoomIn_29; }
	inline bool* get_address_of_zoomIn_29() { return &___zoomIn_29; }
	inline void set_zoomIn_29(bool value)
	{
		___zoomIn_29 = value;
	}

	inline static int32_t get_offset_of_tolerance_30() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___tolerance_30)); }
	inline float get_tolerance_30() const { return ___tolerance_30; }
	inline float* get_address_of_tolerance_30() { return &___tolerance_30; }
	inline void set_tolerance_30(float value)
	{
		___tolerance_30 = value;
	}

	inline static int32_t get_offset_of_prevDistance_31() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___prevDistance_31)); }
	inline float get_prevDistance_31() const { return ___prevDistance_31; }
	inline float* get_address_of_prevDistance_31() { return &___prevDistance_31; }
	inline void set_prevDistance_31(float value)
	{
		___prevDistance_31 = value;
	}

	inline static int32_t get_offset_of_distance_32() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___distance_32)); }
	inline float get_distance_32() const { return ___distance_32; }
	inline float* get_address_of_distance_32() { return &___distance_32; }
	inline void set_distance_32(float value)
	{
		___distance_32 = value;
	}

	inline static int32_t get_offset_of_t1Pos_33() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___t1Pos_33)); }
	inline Vector3_t4282066566  get_t1Pos_33() const { return ___t1Pos_33; }
	inline Vector3_t4282066566 * get_address_of_t1Pos_33() { return &___t1Pos_33; }
	inline void set_t1Pos_33(Vector3_t4282066566  value)
	{
		___t1Pos_33 = value;
	}

	inline static int32_t get_offset_of_t2Pos_34() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___t2Pos_34)); }
	inline Vector3_t4282066566  get_t2Pos_34() const { return ___t2Pos_34; }
	inline Vector3_t4282066566 * get_address_of_t2Pos_34() { return &___t2Pos_34; }
	inline void set_t2Pos_34(Vector3_t4282066566  value)
	{
		___t2Pos_34 = value;
	}

	inline static int32_t get_offset_of_posibleRotation_35() { return static_cast<int32_t>(offsetof(VRCameraClamper_t2660686439, ___posibleRotation_35)); }
	inline Vector3_t4282066566  get_posibleRotation_35() const { return ___posibleRotation_35; }
	inline Vector3_t4282066566 * get_address_of_posibleRotation_35() { return &___posibleRotation_35; }
	inline void set_posibleRotation_35(Vector3_t4282066566  value)
	{
		___posibleRotation_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
