﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRCameraClamper
struct VRCameraClamper_t2660686439;

#include "codegen/il2cpp-codegen.h"

// System.Void VRCameraClamper::.ctor()
extern "C"  void VRCameraClamper__ctor_m271757540 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::Awake()
extern "C"  void VRCameraClamper_Awake_m509362759 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::EnableGyro()
extern "C"  void VRCameraClamper_EnableGyro_m3968714482 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::ActivateModeSwitchBtn(System.Boolean)
extern "C"  void VRCameraClamper_ActivateModeSwitchBtn_m1132214251 (VRCameraClamper_t2660686439 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::EnableSwipeNavigation(System.Boolean)
extern "C"  void VRCameraClamper_EnableSwipeNavigation_m33859812 (VRCameraClamper_t2660686439 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRCameraClamper::GetLimit(System.Single)
extern "C"  float VRCameraClamper_GetLimit_m2552387954 (VRCameraClamper_t2660686439 * __this, float ___currentX0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::Update()
extern "C"  void VRCameraClamper_Update_m1561411241 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::SetZoom(System.Boolean)
extern "C"  void VRCameraClamper_SetZoom_m4294401294 (VRCameraClamper_t2660686439 * __this, bool ___pZoom0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::ClearTapCount()
extern "C"  void VRCameraClamper_ClearTapCount_m1011363675 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::SmoothZoom(System.Single)
extern "C"  void VRCameraClamper_SmoothZoom_m2419153994 (VRCameraClamper_t2660686439 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::Zoom()
extern "C"  void VRCameraClamper_Zoom_m3496135411 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCameraClamper::LateUpdate()
extern "C"  void VRCameraClamper_LateUpdate_m2447476719 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
