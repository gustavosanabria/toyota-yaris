﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRModeManager
struct  VRModeManager_t3340808238  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject VRModeManager::blackQuad
	GameObject_t3674682005 * ___blackQuad_2;
	// UnityEngine.GameObject VRModeManager::vrContainer
	GameObject_t3674682005 * ___vrContainer_3;
	// System.Boolean VRModeManager::loadSkybox
	bool ___loadSkybox_4;
	// UnityEngine.GameObject VRModeManager::returnToGlossaryBtn
	GameObject_t3674682005 * ___returnToGlossaryBtn_5;

public:
	inline static int32_t get_offset_of_blackQuad_2() { return static_cast<int32_t>(offsetof(VRModeManager_t3340808238, ___blackQuad_2)); }
	inline GameObject_t3674682005 * get_blackQuad_2() const { return ___blackQuad_2; }
	inline GameObject_t3674682005 ** get_address_of_blackQuad_2() { return &___blackQuad_2; }
	inline void set_blackQuad_2(GameObject_t3674682005 * value)
	{
		___blackQuad_2 = value;
		Il2CppCodeGenWriteBarrier(&___blackQuad_2, value);
	}

	inline static int32_t get_offset_of_vrContainer_3() { return static_cast<int32_t>(offsetof(VRModeManager_t3340808238, ___vrContainer_3)); }
	inline GameObject_t3674682005 * get_vrContainer_3() const { return ___vrContainer_3; }
	inline GameObject_t3674682005 ** get_address_of_vrContainer_3() { return &___vrContainer_3; }
	inline void set_vrContainer_3(GameObject_t3674682005 * value)
	{
		___vrContainer_3 = value;
		Il2CppCodeGenWriteBarrier(&___vrContainer_3, value);
	}

	inline static int32_t get_offset_of_loadSkybox_4() { return static_cast<int32_t>(offsetof(VRModeManager_t3340808238, ___loadSkybox_4)); }
	inline bool get_loadSkybox_4() const { return ___loadSkybox_4; }
	inline bool* get_address_of_loadSkybox_4() { return &___loadSkybox_4; }
	inline void set_loadSkybox_4(bool value)
	{
		___loadSkybox_4 = value;
	}

	inline static int32_t get_offset_of_returnToGlossaryBtn_5() { return static_cast<int32_t>(offsetof(VRModeManager_t3340808238, ___returnToGlossaryBtn_5)); }
	inline GameObject_t3674682005 * get_returnToGlossaryBtn_5() const { return ___returnToGlossaryBtn_5; }
	inline GameObject_t3674682005 ** get_address_of_returnToGlossaryBtn_5() { return &___returnToGlossaryBtn_5; }
	inline void set_returnToGlossaryBtn_5(GameObject_t3674682005 * value)
	{
		___returnToGlossaryBtn_5 = value;
		Il2CppCodeGenWriteBarrier(&___returnToGlossaryBtn_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
