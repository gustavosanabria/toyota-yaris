﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRModeManager
struct VRModeManager_t3340808238;

#include "codegen/il2cpp-codegen.h"

// System.Void VRModeManager::.ctor()
extern "C"  void VRModeManager__ctor_m2257716477 (VRModeManager_t3340808238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRModeManager::Awake()
extern "C"  void VRModeManager_Awake_m2495321696 (VRModeManager_t3340808238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRModeManager::SetVROnSplitMode()
extern "C"  void VRModeManager_SetVROnSplitMode_m2901616359 (VRModeManager_t3340808238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRModeManager::SetVROnFullMode()
extern "C"  void VRModeManager_SetVROnFullMode_m3362341610 (VRModeManager_t3340808238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRModeManager::EnableVR(System.Boolean)
extern "C"  void VRModeManager_EnableVR_m2368246877 (VRModeManager_t3340808238 * __this, bool ___enableSplitMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
