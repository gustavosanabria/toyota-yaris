﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefin1531824314.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t3016919996  : public UserDefinedTargetBuildingAbstractBehaviour_t1531824314
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
