﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t1845338141;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb1091759131.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t1845338141  : public VuforiaAbstractBehaviour_t1091759131
{
public:

public:
};

struct VuforiaBehaviour_t1845338141_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t1845338141 * ___mVuforiaBehaviour_52;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_52() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t1845338141_StaticFields, ___mVuforiaBehaviour_52)); }
	inline VuforiaBehaviour_t1845338141 * get_mVuforiaBehaviour_52() const { return ___mVuforiaBehaviour_52; }
	inline VuforiaBehaviour_t1845338141 ** get_address_of_mVuforiaBehaviour_52() { return &___mVuforiaBehaviour_52; }
	inline void set_mVuforiaBehaviour_52(VuforiaBehaviour_t1845338141 * value)
	{
		___mVuforiaBehaviour_52 = value;
		Il2CppCodeGenWriteBarrier(&___mVuforiaBehaviour_52, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
