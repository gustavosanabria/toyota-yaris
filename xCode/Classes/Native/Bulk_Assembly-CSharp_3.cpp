﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UITweener
struct UITweener_t105489188;
// EventDelegate/Callback
struct Callback_t1094463061;
// EventDelegate
struct EventDelegate_t4004424223;
// UIViewport
struct UIViewport_t2937361242;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Object
struct Il2CppObject;
// UIWidget
struct UIWidget_t769069560;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2651898963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t2212926951;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIPanel
struct UIPanel_t295209936;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t1484067283;
// UIWidget/HitCheck
struct HitCheck_t3889696652;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t3695058769;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t3030491454;
// UIWidgetContainer
struct UIWidgetContainer_t1520767337;
// UIWrapContent
struct UIWrapContent_t33025435;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIScrollView
struct UIScrollView_t2113479878;
// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t2759048342;
// Utils.Core.AndroidOBBHandler
struct AndroidOBBHandler_t3535974956;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Utils.Core.AndroidOBBHandler/<download>c__IteratorE
struct U3CdownloadU3Ec__IteratorE_t3158326693;
// Utils.Core.DataBuilder
struct DataBuilder_t3810402835;
// Utils.Core.FileDescription
struct FileDescription_t856434530;
// Utils.Core.FileHelper
struct FileHelper_t1028318256;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;
// Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF
struct U3CCopyToPersistentU3Ec__IteratorF_t3141760867;
// Utils.Vuforia.DataSetHandler
struct DataSetHandler_t896450754;
// Vuforia.ObjectTracker
struct ObjectTracker_t455954211;
// VideoCopyTexture
struct VideoCopyTexture_t908209291;
// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;
// VRCameraClamper
struct VRCameraClamper_t2660686439;
// CardboardHead
struct CardboardHead_t2975823030;
// TouchController
struct TouchController_t2155287579;
// VRIntegrationHelper
struct VRIntegrationHelper_t1694779206;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t465046465;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t1927425041;
// VRModeManager
struct VRModeManager_t3340808238;
// Cardboard
struct Cardboard_t1761541558;
// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t1535493961;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t3993405419;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t2489055709;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t1686921617;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t3619649022;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t1850077856;
// Vuforia.DatabaseLoadBehaviour
struct DatabaseLoadBehaviour_t2989373670;
// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t3746290221;
// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t543367463;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t3695833539;
// Vuforia.Prop
struct Prop_t2165309157;
// Vuforia.Surface
struct Surface_t3094389719;
// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t4108278998;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;
// MarkerChildManager
struct MarkerChildManager_t1830688875;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t440051646;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;
// Vuforia.GLErrorHandler
struct GLErrorHandler_t4275497481;
// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t439862723;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t1735871187;
// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t2749231339;
// Vuforia.KeepAliveBehaviour
struct KeepAliveBehaviour_t1171410903;
// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t3170674893;
// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t1204933533;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t2222860085;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t2508606743;
// Vuforia.PropBehaviour
struct PropBehaviour_t3213561028;
// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t582925864;
// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t442856755;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t3457144850;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t892056475;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t1278464685;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t3289283011;
// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t3016919996;
// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t3391125558;
// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t1585639557;
// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t2253448098;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t1845338141;
// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t900168586;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t3409682331;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t1191238304;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t1299549867;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t2347335889;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t865486027;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t3565607731;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t1139674014;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t545947899;
// Vuforia.WordBehaviour
struct WordBehaviour_t2034767101;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t137142681;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t637796117;
// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t2450634316;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t433318935;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2716570836;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t3463640687;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t1176995246;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UITweener105489188MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1077642479MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1077642479.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction1859285089.h"
#include "AssemblyU2DCSharp_RealTime3499460011MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_UITweener_Style3365907174.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener_Style3365907174MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIViewport2937361242.h"
#include "AssemblyU2DCSharp_UIViewport2937361242MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIWidget769069560MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIRect2503437976MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2651898963.h"
#include "mscorlib_System_MulticastDelegate3389745971MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_MulticastDelegate3389745971.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_NGUIMath3886757557MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource788096023.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint3581172420.h"
#include "AssemblyU2DCSharp_NGUITools237277134MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "AssemblyU2DCSharp_UIPanel295209936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_BoxCollider2D2212926951.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint3581172420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Bounds2711641849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3695058769MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3695058769.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067282.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067281.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2095821700.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067283.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource788096023MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3889696652.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3889696652MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback3030491454.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback3030491454MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3027308338MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3027308338.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved1586118419MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved1586118419.h"
#include "mscorlib_System_Comparison_1_gen375483973MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIGrid2503122938.h"
#include "AssemblyU2DCSharp_UIGrid2503122938MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen375483973.h"
#include "AssemblyU2DCSharp_NGUITools237277134.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement2769919992.h"
#include "AssemblyU2DCSharp_UICamera189364953MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2759048342MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2759048342.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler3535974956.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler3535974956MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_SingletonObject_1_gen3364670738MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_U3C3158326693MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_U3C3158326693.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_EEr3259205415.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_EEr3259205415MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_DataBuilder3810402835.h"
#include "AssemblyU2DCSharp_Utils_Core_DataBuilder3810402835MethodDeclarations.h"
#include "mscorlib_System_IO_File667612524MethodDeclarations.h"
#include "mscorlib_System_IO_BinaryWriter4146364100MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream2141505868.h"
#include "mscorlib_System_IO_FileMode3233790127.h"
#include "mscorlib_System_IO_BinaryWriter4146364100.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_Stream1561764144MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_FileDescription856434530.h"
#include "AssemblyU2DCSharp_Utils_Core_FileDescription856434530MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper1028318256.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper1028318256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_IO_Path667902997MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW3134621005MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_EFolder738843808.h"
#include "mscorlib_System_IO_Directory1148685675MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"
#include "mscorlib_System_Action_1_gen403047693.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_U3CCopyToP3141760867MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_U3CCopyToP3141760867.h"
#include "mscorlib_System_IO_StreamReader2549717843MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader2148718976MethodDeclarations.h"
#include "mscorlib_System_IO_StreamReader2549717843.h"
#include "mscorlib_System_IO_TextReader2148718976.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_IO_BinaryReader3990958868MethodDeclarations.h"
#include "mscorlib_System_IO_BinaryReader3990958868.h"
#include "mscorlib_System_IO_FileStream2141505868MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Text_Encoding2012439129MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_Action_1_gen403047693MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_EFolder738843808MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Vuforia_DataSetHandler896450754.h"
#include "AssemblyU2DCSharp_Utils_Vuforia_DataSetHandler896450754MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb1091759131MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb1091759131.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "System_Core_System_Action3771233898.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMa2355365335MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMa2355365335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac455954211.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac455954211MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn2677974245.h"
#include "AssemblyU2DCSharp_VideoCopyTexture908209291.h"
#include "AssemblyU2DCSharp_VideoCopyTexture908209291MethodDeclarations.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl3572035536MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl3572035536.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT1488282328.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "AssemblyU2DCSharp_VRCameraClamper2660686439.h"
#include "AssemblyU2DCSharp_VRCameraClamper2660686439MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cardboard1761541558MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pose3D2396367586MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_AppLibrary3292178298.h"
#include "AssemblyU2DCSharp_AppLibrary3292178298MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardboardHead2975823030.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "AssemblyU2DCSharp_Cardboard1761541558.h"
#include "AssemblyU2DCSharp_Pose3D2396367586.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_TouchController2155287579.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Touch4210255029MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper1694779206.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper1694779206MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour1845338141MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour1845338141.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcess465046465.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Backgroun2608219151MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcess465046465MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour1927425041.h"
#include "AssemblyU2DCSharp_VRModeManager3340808238.h"
#include "AssemblyU2DCSharp_VRModeManager3340808238MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SkyboxHelper1246069112MethodDeclarations.h"
#include "AssemblyU2DCSharp_GlossaryMgr3182292730.h"
#include "AssemblyU2DCSharp_GlossaryMgr3182292730MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer1535493961.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer1535493961MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUti743372383MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn1557074901MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour1927425041MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3993405419.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3993405419MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudReco1213605353MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB2489055709.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB2489055709MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1686921617MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1686921617.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1706594387.h"
#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1706594387MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySe2197853779.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponent900168586MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Behaviour2065637972MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponent900168586.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour1850077856.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour1850077856MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderT1139674014MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour2989373670.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour2989373670MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseL1625730020MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErr3746290221.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErr3746290221MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen828034096MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen828034096.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction2749288659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction2749288659.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH543367463.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH543367463MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2561125293MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru1860057025MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3490205855MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour3695833539.h"
#include "mscorlib_System_Action_1_gen2561125293.h"
#include "mscorlib_System_Action_1_gen3490205855.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour3213561028.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstr1293468098.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour3457144850.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbs142368528.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan4108278998.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan4108278998MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"
#include "AssemblyU2DCSharp_MarkerChildManager1830688875MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerChildManager1830688875.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler4275497481.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler4275497481MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour439862723.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour439862723MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour1735871187.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour1735871187MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2347335889MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer2749231339.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer2749231339MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1171410903.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1171410903MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAlive2256617941MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour3170674893.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour3170674893MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbst865486027MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour1204933533.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour1204933533MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAb3409682331MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRu1069467038MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAb3409682331.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour2222860085.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour2222860085MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTarg3565607731MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour2508606743.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour2508606743MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTarg637796117MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour3213561028MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstr1293468098MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour3695833539MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetB582925864.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetB582925864MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstruc147246886MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehavi442856755.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehavi442856755MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr1152863025MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour3457144850MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbs142368528MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour892056475.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour892056475MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAb137142681MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour1278464685.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour1278464685MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAb1299549867MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour3289283011.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour3289283011MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin3016919996.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin3016919996MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefin1531824314MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3391125558.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3391125558MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBack2475465524MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer1585639557.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer1585639557MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoText4076028962MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2253448098.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2253448098MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1191238304MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityP531631577MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeU2903303273MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityP531631577.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeU2903303273.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1191238304.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAb1299549867.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2347335889.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbst865486027.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTarg3565607731.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderT1139674014.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstra545947899.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour2034767101.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAb137142681.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTarg637796117.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour2450634316.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour2450634316MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbst725705546MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour433318935.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour433318935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1442390413MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL2267613321MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1442390413.h"
#include "UnityEngine_UnityEngine_Gizmos2849394813MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH3463640687.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH3463640687MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour2034767101MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstra545947899MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t2727095145_m342242277(__this, method) ((  Camera_t2727095145 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t2939674232_m3246438266(__this, method) ((  Collider_t2939674232 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t2212926951_m2584023519(__this, method) ((  BoxCollider2D_t2212926951 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPanel>()
#define Component_GetComponent_TisUIPanel_t295209936_m1836641897(__this, method) ((  UIPanel_t295209936 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * p0, const MethodInfo* method);
#define NGUITools_FindInParents_TisIl2CppObject_m485072941(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared)(__this /* static, unused */, p0, method)
// !!0 NGUITools::FindInParents<UIPanel>(UnityEngine.GameObject)
#define NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243(__this /* static, unused */, p0, method) ((  UIPanel_t295209936 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UIScrollView>()
#define Component_GetComponent_TisUIScrollView_t2113479878_m1783064831(__this, method) ((  UIScrollView_t2113479878 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 Vuforia.TrackerManager::GetTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManager_GetTracker_TisIl2CppObject_m2486054351_gshared (TrackerManager_t2355365335 * __this, const MethodInfo* method);
#define TrackerManager_GetTracker_TisIl2CppObject_m2486054351(__this, method) ((  Il2CppObject * (*) (TrackerManager_t2355365335 *, const MethodInfo*))TrackerManager_GetTracker_TisIl2CppObject_m2486054351_gshared)(__this, method)
// !!0 Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()
#define TrackerManager_GetTracker_TisObjectTracker_t455954211_m3912055616(__this, method) ((  ObjectTracker_t455954211 * (*) (TrackerManager_t2355365335 *, const MethodInfo*))TrackerManager_GetTracker_TisIl2CppObject_m2486054351_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t2804666580_m541358362(__this, method) ((  MeshRenderer_t2804666580 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m329627835(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<CardboardHead>()
#define Component_GetComponentInChildren_TisCardboardHead_t2975823030_m3898267871(__this, method) ((  CardboardHead_t2975823030 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<TouchController>()
#define Component_GetComponentInChildren_TisTouchController_t2155287579_m1612592602(__this, method) ((  TouchController_t2155287579 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.HideExcessAreaAbstractBehaviour>()
#define Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t465046465_m2348691886(__this, method) ((  HideExcessAreaAbstractBehaviour_t465046465 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t1927425041_m2233635214(__this, method) ((  BackgroundPlaneBehaviour_t1927425041 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2438332479_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2438332479(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2438332479_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Cardboard>()
#define GameObject_GetComponent_TisCardboard_t1761541558_m954238187(__this, method) ((  Cardboard_t1761541558 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2438332479_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1244034627 * Enumerable_ToList_TisIl2CppObject_m755958755_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m755958755(__this /* static, unused */, p0, method) ((  List_1_t1244034627 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m755958755_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m285312770(__this /* static, unused */, p0, method) ((  List_1_t1686921617 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m755958755_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t3695833539_m55479916(__this, method) ((  ReconstructionBehaviour_t3695833539 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169(__this, method) ((  TrackableBehaviour_t4179556250 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MarkerChildManager>()
#define Component_GetComponent_TisMarkerChildManager_t1830688875_m2821499578(__this, method) ((  MarkerChildManager_t1830688875 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared (Component_t3501516275 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m230616125(__this, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363(__this, p0, method) ((  RendererU5BU5D_t440051646* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322(__this, p0, method) ((  ColliderU5BU5D_t2697150633* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t3076687687_m3123788487(__this, method) ((  Renderer_t3076687687 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3839065225_m3768884677(__this, method) ((  MeshFilter_t3839065225 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m328503754_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m328503754(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2489055709_m2183989835(__this, method) ((  ComponentFactoryStarterBehaviour_t2489055709 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m594707695_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m594707695(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m594707695_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t1845338141_m259102150(__this /* static, unused */, method) ((  VuforiaBehaviour_t1845338141 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m594707695_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t1204933533_m4225605259(__this, method) ((  MaskOutBehaviour_t1204933533 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t2253448098_m2099003942(__this, method) ((  VirtualButtonBehaviour_t2253448098 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t1278464685_m63866235(__this, method) ((  TurnOffBehaviour_t1278464685 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t1735871187_m1658831381(__this, method) ((  ImageTargetBehaviour_t1735871187 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
#define GameObject_AddComponent_TisMarkerBehaviour_t3170674893_m1585383317(__this, method) ((  MarkerBehaviour_t3170674893 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t2222860085_m1145565811(__this, method) ((  MultiTargetBehaviour_t2222860085 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t1850077856_m1470876642(__this, method) ((  CylinderTargetBehaviour_t1850077856 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t2034767101_m2299282597(__this, method) ((  WordBehaviour_t2034767101 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t892056475_m1686624967(__this, method) ((  TextRecoBehaviour_t892056475 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t2508606743_m1474978123(__this, method) ((  ObjectTargetBehaviour_t2508606743 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m328503754_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t2727095145_m1504240198(__this, method) ((  CameraU5BU5D_t2716570836* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628(__this, p0, method) ((  WireframeBehaviourU5BU5D_t1176995246* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UITweener::.ctor()
extern Il2CppClass* KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3667593487_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1077642479_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1287572350_MethodInfo_var;
extern const uint32_t UITweener__ctor_m2521610951_MetadataUsageId;
extern "C"  void UITweener__ctor_m2521610951 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener__ctor_m2521610951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		KeyframeU5BU5D_t3589549831* L_0 = ((KeyframeU5BU5D_t3589549831*)SZArrayNew(KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Keyframe_t4079056114  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m3412708539(&L_1, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t3589549831* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Keyframe_t4079056114  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m3412708539(&L_3, (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		AnimationCurve_t3667593487 * L_4 = (AnimationCurve_t3667593487 *)il2cpp_codegen_object_new(AnimationCurve_t3667593487_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2436282331(L_4, L_2, /*hidden argument*/NULL);
		__this->set_animationCurve_5(L_4);
		__this->set_ignoreTimeScale_6((bool)1);
		__this->set_duration_8((1.0f));
		List_1_t1077642479 * L_5 = (List_1_t1077642479 *)il2cpp_codegen_object_new(List_1_t1077642479_il2cpp_TypeInfo_var);
		List_1__ctor_m1287572350(L_5, /*hidden argument*/List_1__ctor_m1287572350_MethodInfo_var);
		__this->set_onFinished_11(L_5);
		__this->set_mAmountPerDelta_17((1000.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UITweener::get_amountPerDelta()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_get_amountPerDelta_m342139981_MetadataUsageId;
extern "C"  float UITweener_get_amountPerDelta_m342139981 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_get_amountPerDelta_m342139981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UITweener_t105489188 * G_B3_0 = NULL;
	UITweener_t105489188 * G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	UITweener_t105489188 * G_B4_1 = NULL;
	{
		float L_0 = __this->get_mDuration_16();
		float L_1 = __this->get_duration_8();
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_005a;
		}
	}
	{
		float L_2 = __this->get_duration_8();
		__this->set_mDuration_16(L_2);
		float L_3 = __this->get_duration_8();
		G_B2_0 = __this;
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			G_B3_0 = __this;
			goto IL_003f;
		}
	}
	{
		float L_4 = __this->get_duration_8();
		G_B4_0 = ((float)((float)(1.0f)/(float)L_4));
		G_B4_1 = G_B2_0;
		goto IL_0044;
	}

IL_003f:
	{
		G_B4_0 = (1000.0f);
		G_B4_1 = G_B3_0;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_5 = fabsf(G_B4_0);
		float L_6 = __this->get_mAmountPerDelta_17();
		float L_7 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(G_B4_1);
		G_B4_1->set_mAmountPerDelta_17(((float)((float)L_5*(float)L_7)));
	}

IL_005a:
	{
		float L_8 = __this->get_mAmountPerDelta_17();
		return L_8;
	}
}
// System.Single UITweener::get_tweenFactor()
extern "C"  float UITweener_get_tweenFactor_m3023342018 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_mFactor_18();
		return L_0;
	}
}
// System.Void UITweener::set_tweenFactor(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_set_tweenFactor_m826357225_MetadataUsageId;
extern "C"  void UITweener_set_tweenFactor_m826357225 (UITweener_t105489188 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_set_tweenFactor_m826357225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_mFactor_18(L_1);
		return;
	}
}
// AnimationOrTween.Direction UITweener::get_direction()
extern "C"  int32_t UITweener_get_direction_m1737209411 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void UITweener::Reset()
extern "C"  void UITweener_Reset_m168043892 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mStarted_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UITweener::SetStartToCurrentValue() */, __this);
		VirtActionInvoker0::Invoke(7 /* System.Void UITweener::SetEndToCurrentValue() */, __this);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UITweener::Start()
extern "C"  void UITweener_Start_m1468748743 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Update_m2587390246(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Update()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* UITweener_t105489188_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1077642479_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1287572350_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1680510305_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2669468980_MethodInfo_var;
extern const uint32_t UITweener_Update_m2587390246_MetadataUsageId;
extern "C"  void UITweener_Update_m2587390246 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Update_m2587390246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	UITweener_t105489188 * V_2 = NULL;
	int32_t V_3 = 0;
	EventDelegate_t4004424223 * V_4 = NULL;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		bool L_0 = __this->get_ignoreTimeScale_6();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001a;
	}

IL_0015:
	{
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001a:
	{
		V_0 = G_B3_0;
		bool L_3 = __this->get_ignoreTimeScale_6();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		float L_4 = RealTime_get_time_m3537010614(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		goto IL_0035;
	}

IL_0030:
	{
		float L_5 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_5;
	}

IL_0035:
	{
		V_1 = G_B6_0;
		bool L_6 = __this->get_mStarted_14();
		if (L_6)
		{
			goto IL_0056;
		}
	}
	{
		__this->set_mStarted_14((bool)1);
		float L_7 = V_1;
		float L_8 = __this->get_delay_7();
		__this->set_mStartTime_15(((float)((float)L_7+(float)L_8)));
	}

IL_0056:
	{
		float L_9 = V_1;
		float L_10 = __this->get_mStartTime_15();
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0063;
		}
	}
	{
		return;
	}

IL_0063:
	{
		float L_11 = __this->get_mFactor_18();
		float L_12 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		float L_13 = V_0;
		__this->set_mFactor_18(((float)((float)L_11+(float)((float)((float)L_12*(float)L_13)))));
		int32_t L_14 = __this->get_style_4();
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_00b1;
		}
	}
	{
		float L_15 = __this->get_mFactor_18();
		if ((!(((float)L_15) > ((float)(1.0f)))))
		{
			goto IL_00ac;
		}
	}
	{
		float L_16 = __this->get_mFactor_18();
		float L_17 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_18 = floorf(L_17);
		__this->set_mFactor_18(((float)((float)L_16-(float)L_18)));
	}

IL_00ac:
	{
		goto IL_013f;
	}

IL_00b1:
	{
		int32_t L_19 = __this->get_style_4();
		if ((!(((uint32_t)L_19) == ((uint32_t)2))))
		{
			goto IL_013f;
		}
	}
	{
		float L_20 = __this->get_mFactor_18();
		if ((!(((float)L_20) > ((float)(1.0f)))))
		{
			goto IL_00fd;
		}
	}
	{
		float L_21 = __this->get_mFactor_18();
		float L_22 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_23 = floorf(L_22);
		__this->set_mFactor_18(((float)((float)(1.0f)-(float)((float)((float)L_21-(float)L_23)))));
		float L_24 = __this->get_mAmountPerDelta_17();
		__this->set_mAmountPerDelta_17(((-L_24)));
		goto IL_013f;
	}

IL_00fd:
	{
		float L_25 = __this->get_mFactor_18();
		if ((!(((float)L_25) < ((float)(0.0f)))))
		{
			goto IL_013f;
		}
	}
	{
		float L_26 = __this->get_mFactor_18();
		__this->set_mFactor_18(((-L_26)));
		float L_27 = __this->get_mFactor_18();
		float L_28 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_29 = floorf(L_28);
		__this->set_mFactor_18(((float)((float)L_27-(float)L_29)));
		float L_30 = __this->get_mAmountPerDelta_17();
		__this->set_mAmountPerDelta_17(((-L_30)));
	}

IL_013f:
	{
		int32_t L_31 = __this->get_style_4();
		if (L_31)
		{
			goto IL_027f;
		}
	}
	{
		float L_32 = __this->get_duration_8();
		if ((((float)L_32) == ((float)(0.0f))))
		{
			goto IL_017a;
		}
	}
	{
		float L_33 = __this->get_mFactor_18();
		if ((((float)L_33) > ((float)(1.0f))))
		{
			goto IL_017a;
		}
	}
	{
		float L_34 = __this->get_mFactor_18();
		if ((!(((float)L_34) < ((float)(0.0f)))))
		{
			goto IL_027f;
		}
	}

IL_017a:
	{
		float L_35 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		__this->set_mFactor_18(L_36);
		float L_37 = __this->get_mFactor_18();
		UITweener_Sample_m1154056121(__this, L_37, (bool)1, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		UITweener_t105489188 * L_38 = ((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_39 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_38, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_027a;
		}
	}
	{
		UITweener_t105489188 * L_40 = ((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		V_2 = L_40;
		((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->set_current_2(__this);
		List_1_t1077642479 * L_41 = __this->get_onFinished_11();
		if (!L_41)
		{
			goto IL_0240;
		}
	}
	{
		List_1_t1077642479 * L_42 = __this->get_onFinished_11();
		__this->set_mTemp_19(L_42);
		List_1_t1077642479 * L_43 = (List_1_t1077642479 *)il2cpp_codegen_object_new(List_1_t1077642479_il2cpp_TypeInfo_var);
		List_1__ctor_m1287572350(L_43, /*hidden argument*/List_1__ctor_m1287572350_MethodInfo_var);
		__this->set_onFinished_11(L_43);
		List_1_t1077642479 * L_44 = __this->get_mTemp_19();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Execute_m895247138(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0228;
	}

IL_01ef:
	{
		List_1_t1077642479 * L_45 = __this->get_mTemp_19();
		int32_t L_46 = V_3;
		NullCheck(L_45);
		EventDelegate_t4004424223 * L_47 = List_1_get_Item_m1680510305(L_45, L_46, /*hidden argument*/List_1_get_Item_m1680510305_MethodInfo_var);
		V_4 = L_47;
		EventDelegate_t4004424223 * L_48 = V_4;
		if (!L_48)
		{
			goto IL_0224;
		}
	}
	{
		EventDelegate_t4004424223 * L_49 = V_4;
		NullCheck(L_49);
		bool L_50 = L_49->get_oneShot_3();
		if (L_50)
		{
			goto IL_0224;
		}
	}
	{
		List_1_t1077642479 * L_51 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_52 = V_4;
		EventDelegate_t4004424223 * L_53 = V_4;
		NullCheck(L_53);
		bool L_54 = L_53->get_oneShot_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m3670177826(NULL /*static, unused*/, L_51, L_52, L_54, /*hidden argument*/NULL);
	}

IL_0224:
	{
		int32_t L_55 = V_3;
		V_3 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_0228:
	{
		int32_t L_56 = V_3;
		List_1_t1077642479 * L_57 = __this->get_mTemp_19();
		NullCheck(L_57);
		int32_t L_58 = List_1_get_Count_m2669468980(L_57, /*hidden argument*/List_1_get_Count_m2669468980_MethodInfo_var);
		if ((((int32_t)L_56) < ((int32_t)L_58)))
		{
			goto IL_01ef;
		}
	}
	{
		__this->set_mTemp_19((List_1_t1077642479 *)NULL);
	}

IL_0240:
	{
		GameObject_t3674682005 * L_59 = __this->get_eventReceiver_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_59, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0274;
		}
	}
	{
		String_t* L_61 = __this->get_callWhenFinished_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_62 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_0274;
		}
	}
	{
		GameObject_t3674682005 * L_63 = __this->get_eventReceiver_12();
		String_t* L_64 = __this->get_callWhenFinished_13();
		NullCheck(L_63);
		GameObject_SendMessage_m423373689(L_63, L_64, __this, 1, /*hidden argument*/NULL);
	}

IL_0274:
	{
		UITweener_t105489188 * L_65 = V_2;
		((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->set_current_2(L_65);
	}

IL_027a:
	{
		goto IL_028c;
	}

IL_027f:
	{
		float L_66 = __this->get_mFactor_18();
		UITweener_Sample_m1154056121(__this, L_66, (bool)0, /*hidden argument*/NULL);
	}

IL_028c:
	{
		return;
	}
}
// System.Void UITweener::SetOnFinished(EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_SetOnFinished_m2532336547_MetadataUsageId;
extern "C"  void UITweener_SetOnFinished_m2532336547 (UITweener_t105489188 * __this, Callback_t1094463061 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_SetOnFinished_m2532336547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1077642479 * L_0 = __this->get_onFinished_11();
		Callback_t1094463061 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Set_m3964967374(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::SetOnFinished(EventDelegate)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_SetOnFinished_m4129108443_MetadataUsageId;
extern "C"  void UITweener_SetOnFinished_m4129108443 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_SetOnFinished_m4129108443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1077642479 * L_0 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Set_m2854322620(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::AddOnFinished(EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_AddOnFinished_m1398296610_MetadataUsageId;
extern "C"  void UITweener_AddOnFinished_m1398296610 (UITweener_t105489188 * __this, Callback_t1094463061 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_AddOnFinished_m1398296610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1077642479 * L_0 = __this->get_onFinished_11();
		Callback_t1094463061 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::AddOnFinished(EventDelegate)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_AddOnFinished_m2961468028_MetadataUsageId;
extern "C"  void UITweener_AddOnFinished_m2961468028 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_AddOnFinished_m2961468028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1077642479 * L_0 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m4032808443(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::RemoveOnFinished(EventDelegate)
extern const MethodInfo* List_1_Remove_m520354415_MethodInfo_var;
extern const uint32_t UITweener_RemoveOnFinished_m1549062977_MetadataUsageId;
extern "C"  void UITweener_RemoveOnFinished_m1549062977 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_RemoveOnFinished_m1549062977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1077642479 * L_0 = __this->get_onFinished_11();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		List_1_t1077642479 * L_1 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_2 = ___del0;
		NullCheck(L_1);
		List_1_Remove_m520354415(L_1, L_2, /*hidden argument*/List_1_Remove_m520354415_MethodInfo_var);
	}

IL_0018:
	{
		List_1_t1077642479 * L_3 = __this->get_mTemp_19();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		List_1_t1077642479 * L_4 = __this->get_mTemp_19();
		EventDelegate_t4004424223 * L_5 = ___del0;
		NullCheck(L_4);
		List_1_Remove_m520354415(L_4, L_5, /*hidden argument*/List_1_Remove_m520354415_MethodInfo_var);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UITweener::OnDisable()
extern "C"  void UITweener_OnDisable_m3864744878 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		__this->set_mStarted_14((bool)0);
		return;
	}
}
// System.Void UITweener::Sample(System.Single,System.Boolean)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_Sample_m1154056121_MetadataUsageId;
extern "C"  void UITweener_Sample_m1154056121 (UITweener_t105489188 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Sample_m1154056121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	UITweener_t105489188 * G_B18_0 = NULL;
	UITweener_t105489188 * G_B17_0 = NULL;
	float G_B19_0 = 0.0f;
	UITweener_t105489188 * G_B19_1 = NULL;
	{
		float L_0 = ___factor0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_method_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = sinf(((float)((float)(1.57079637f)*(float)((float)((float)(1.0f)-(float)L_3)))));
		V_0 = ((float)((float)(1.0f)-(float)L_4));
		bool L_5 = __this->get_steeperCurves_9();
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		float L_6 = V_0;
		float L_7 = V_0;
		V_0 = ((float)((float)L_6*(float)L_7));
	}

IL_003b:
	{
		goto IL_0121;
	}

IL_0040:
	{
		int32_t L_8 = __this->get_method_3();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_007b;
		}
	}
	{
		float L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_10 = sinf(((float)((float)(1.57079637f)*(float)L_9)));
		V_0 = L_10;
		bool L_11 = __this->get_steeperCurves_9();
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		float L_12 = V_0;
		V_0 = ((float)((float)(1.0f)-(float)L_12));
		float L_13 = V_0;
		float L_14 = V_0;
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)L_13*(float)L_14))));
	}

IL_0076:
	{
		goto IL_0121;
	}

IL_007b:
	{
		int32_t L_15 = __this->get_method_3();
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_00e8;
		}
	}
	{
		float L_16 = V_0;
		float L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_18 = sinf(((float)((float)L_17*(float)(6.28318548f))));
		V_0 = ((float)((float)L_16-(float)((float)((float)L_18/(float)(6.28318548f)))));
		bool L_19 = __this->get_steeperCurves_9();
		if (!L_19)
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_0;
		V_0 = ((float)((float)((float)((float)L_20*(float)(2.0f)))-(float)(1.0f)));
		float L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		float L_23 = V_0;
		float L_24 = fabsf(L_23);
		V_0 = ((float)((float)(1.0f)-(float)L_24));
		float L_25 = V_0;
		float L_26 = V_0;
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)L_25*(float)L_26))));
		float L_27 = V_2;
		float L_28 = V_0;
		V_0 = ((float)((float)((float)((float)((float)((float)L_27*(float)L_28))*(float)(0.5f)))+(float)(0.5f)));
	}

IL_00e3:
	{
		goto IL_0121;
	}

IL_00e8:
	{
		int32_t L_29 = __this->get_method_3();
		if ((!(((uint32_t)L_29) == ((uint32_t)4))))
		{
			goto IL_0101;
		}
	}
	{
		float L_30 = V_0;
		float L_31 = UITweener_BounceLogic_m384101156(__this, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		goto IL_0121;
	}

IL_0101:
	{
		int32_t L_32 = __this->get_method_3();
		if ((!(((uint32_t)L_32) == ((uint32_t)5))))
		{
			goto IL_0121;
		}
	}
	{
		float L_33 = V_0;
		float L_34 = UITweener_BounceLogic_m384101156(__this, ((float)((float)(1.0f)-(float)L_33)), /*hidden argument*/NULL);
		V_0 = ((float)((float)(1.0f)-(float)L_34));
	}

IL_0121:
	{
		AnimationCurve_t3667593487 * L_35 = __this->get_animationCurve_5();
		G_B17_0 = __this;
		if (!L_35)
		{
			G_B18_0 = __this;
			goto IL_013e;
		}
	}
	{
		AnimationCurve_t3667593487 * L_36 = __this->get_animationCurve_5();
		float L_37 = V_0;
		NullCheck(L_36);
		float L_38 = AnimationCurve_Evaluate_m547727012(L_36, L_37, /*hidden argument*/NULL);
		G_B19_0 = L_38;
		G_B19_1 = G_B17_0;
		goto IL_013f;
	}

IL_013e:
	{
		float L_39 = V_0;
		G_B19_0 = L_39;
		G_B19_1 = G_B18_0;
	}

IL_013f:
	{
		bool L_40 = ___isFinished1;
		NullCheck(G_B19_1);
		VirtActionInvoker2< float, bool >::Invoke(5 /* System.Void UITweener::OnUpdate(System.Single,System.Boolean) */, G_B19_1, G_B19_0, L_40);
		return;
	}
}
// System.Single UITweener::BounceLogic(System.Single)
extern "C"  float UITweener_BounceLogic_m384101156 (UITweener_t105489188 * __this, float ___val0, const MethodInfo* method)
{
	{
		float L_0 = ___val0;
		if ((!(((float)L_0) < ((float)(0.363636f)))))
		{
			goto IL_001b;
		}
	}
	{
		float L_1 = ___val0;
		float L_2 = ___val0;
		___val0 = ((float)((float)((float)((float)(7.5685f)*(float)L_1))*(float)L_2));
		goto IL_0089;
	}

IL_001b:
	{
		float L_3 = ___val0;
		if ((!(((float)L_3) < ((float)(0.727272f)))))
		{
			goto IL_0045;
		}
	}
	{
		float L_4 = ___val0;
		float L_5 = ((float)((float)L_4-(float)(0.545454f)));
		___val0 = L_5;
		float L_6 = ___val0;
		___val0 = ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_5))*(float)L_6))+(float)(0.75f)));
		goto IL_0089;
	}

IL_0045:
	{
		float L_7 = ___val0;
		if ((!(((float)L_7) < ((float)(0.90909f)))))
		{
			goto IL_006f;
		}
	}
	{
		float L_8 = ___val0;
		float L_9 = ((float)((float)L_8-(float)(0.818181f)));
		___val0 = L_9;
		float L_10 = ___val0;
		___val0 = ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_9))*(float)L_10))+(float)(0.9375f)));
		goto IL_0089;
	}

IL_006f:
	{
		float L_11 = ___val0;
		float L_12 = ((float)((float)L_11-(float)(0.9545454f)));
		___val0 = L_12;
		float L_13 = ___val0;
		___val0 = ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_12))*(float)L_13))+(float)(0.984375f)));
	}

IL_0089:
	{
		float L_14 = ___val0;
		return L_14;
	}
}
// System.Void UITweener::Play()
extern "C"  void UITweener_Play_m3833433041 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Play_m1190576008(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::PlayForward()
extern "C"  void UITweener_PlayForward_m3184544150 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Play_m1190576008(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::PlayReverse()
extern "C"  void UITweener_PlayReverse_m1477146227 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Play_m1190576008(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Play(System.Boolean)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_Play_m1190576008_MetadataUsageId;
extern "C"  void UITweener_Play_m1190576008 (UITweener_t105489188 * __this, bool ___forward0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Play_m1190576008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		__this->set_mAmountPerDelta_17(L_1);
		bool L_2 = ___forward0;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		float L_3 = __this->get_mAmountPerDelta_17();
		__this->set_mAmountPerDelta_17(((-L_3)));
	}

IL_0024:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
		UITweener_Update_m2587390246(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::ResetToBeginning()
extern "C"  void UITweener_ResetToBeginning_m292257456 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	UITweener_t105489188 * G_B2_0 = NULL;
	UITweener_t105489188 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	UITweener_t105489188 * G_B3_1 = NULL;
	{
		__this->set_mStarted_14((bool)0);
		float L_0 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0022;
		}
	}
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0022:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mFactor_18(G_B3_0);
		float L_1 = __this->get_mFactor_18();
		UITweener_Sample_m1154056121(__this, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Toggle()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_Toggle_m48266481_MetadataUsageId;
extern "C"  void UITweener_Toggle_m48266481 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Toggle_m48266481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_mFactor_18();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		__this->set_mAmountPerDelta_17(((-L_1)));
		goto IL_0033;
	}

IL_0022:
	{
		float L_2 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		__this->set_mAmountPerDelta_17(L_3);
	}

IL_0033:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::SetStartToCurrentValue()
extern "C"  void UITweener_SetStartToCurrentValue_m1490938032 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UITweener::SetEndToCurrentValue()
extern "C"  void UITweener_SetEndToCurrentValue_m3403311529 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIViewport::.ctor()
extern "C"  void UIViewport__ctor_m2969447041 (UIViewport_t2937361242 * __this, const MethodInfo* method)
{
	{
		__this->set_fullSize_5((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIViewport::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m342242277_MethodInfo_var;
extern const uint32_t UIViewport_Start_m1916584833_MetadataUsageId;
extern "C"  void UIViewport_Start_m1916584833 (UIViewport_t2937361242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_Start_m1916584833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m342242277(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m342242277_MethodInfo_var);
		__this->set_mCam_6(L_0);
		Camera_t2727095145 * L_1 = __this->get_sourceCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Camera_t2727095145 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sourceCamera_2(L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UIViewport::LateUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIViewport_LateUpdate_m1044547698_MetadataUsageId;
extern "C"  void UIViewport_LateUpdate_m1044547698 (UIViewport_t2937361242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_LateUpdate_m1044547698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Transform_t1659122786 * L_0 = __this->get_topLeft_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t1659122786 * L_2 = __this->get_bottomRight_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t1659122786 * L_4 = __this->get_topLeft_3();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeInHierarchy_m612450965(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0113;
		}
	}
	{
		Camera_t2727095145 * L_7 = __this->get_sourceCamera_2();
		Transform_t1659122786 * L_8 = __this->get_topLeft_3();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_10 = Camera_WorldToScreenPoint_m2400233676(L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Camera_t2727095145 * L_11 = __this->get_sourceCamera_2();
		Transform_t1659122786 * L_12 = __this->get_bottomRight_4();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_14 = Camera_WorldToScreenPoint_m2400233676(L_11, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_0)->get_x_1();
		int32_t L_16 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = (&V_1)->get_y_2();
		int32_t L_18 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_x_1();
		float L_20 = (&V_0)->get_x_1();
		int32_t L_21 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = (&V_0)->get_y_2();
		float L_23 = (&V_1)->get_y_2();
		int32_t L_24 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m3291325233((&V_2), ((float)((float)L_15/(float)(((float)((float)L_16))))), ((float)((float)L_17/(float)(((float)((float)L_18))))), ((float)((float)((float)((float)L_19-(float)L_20))/(float)(((float)((float)L_21))))), ((float)((float)((float)((float)L_22-(float)L_23))/(float)(((float)((float)L_24))))), /*hidden argument*/NULL);
		float L_25 = __this->get_fullSize_5();
		float L_26 = Rect_get_height_m2154960823((&V_2), /*hidden argument*/NULL);
		V_3 = ((float)((float)L_25*(float)L_26));
		Rect_t4241904616  L_27 = V_2;
		Camera_t2727095145 * L_28 = __this->get_mCam_6();
		NullCheck(L_28);
		Rect_t4241904616  L_29 = Camera_get_rect_m3083266205(L_28, /*hidden argument*/NULL);
		bool L_30 = Rect_op_Inequality_m2236552616(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e5;
		}
	}
	{
		Camera_t2727095145 * L_31 = __this->get_mCam_6();
		Rect_t4241904616  L_32 = V_2;
		NullCheck(L_31);
		Camera_set_rect_m1907189602(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		Camera_t2727095145 * L_33 = __this->get_mCam_6();
		NullCheck(L_33);
		float L_34 = Camera_get_orthographicSize_m3215515490(L_33, /*hidden argument*/NULL);
		float L_35 = V_3;
		if ((((float)L_34) == ((float)L_35)))
		{
			goto IL_0102;
		}
	}
	{
		Camera_t2727095145 * L_36 = __this->get_mCam_6();
		float L_37 = V_3;
		NullCheck(L_36);
		Camera_set_orthographicSize_m3910539041(L_36, L_37, /*hidden argument*/NULL);
	}

IL_0102:
	{
		Camera_t2727095145 * L_38 = __this->get_mCam_6();
		NullCheck(L_38);
		Behaviour_set_enabled_m2046806933(L_38, (bool)1, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_0113:
	{
		Camera_t2727095145 * L_39 = __this->get_mCam_6();
		NullCheck(L_39);
		Behaviour_set_enabled_m2046806933(L_39, (bool)0, /*hidden argument*/NULL);
	}

IL_011f:
	{
		return;
	}
}
// System.Void UIWidget::.ctor()
extern Il2CppClass* UIGeometry_t3586695974_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* UIRect_t2503437976_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget__ctor_m70411171_MetadataUsageId;
extern "C"  void UIWidget__ctor_m70411171 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget__ctor_m70411171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mColor_22(L_0);
		__this->set_mPivot_23(4);
		__this->set_mWidth_24(((int32_t)100));
		__this->set_mHeight_25(((int32_t)100));
		__this->set_aspectRatio_33((1.0f));
		UIGeometry_t3586695974 * L_1 = (UIGeometry_t3586695974 *)il2cpp_codegen_object_new(UIGeometry_t3586695974_il2cpp_TypeInfo_var);
		UIGeometry__ctor_m3438354485(L_1, /*hidden argument*/NULL);
		__this->set_geometry_36(L_1);
		__this->set_fillGeometry_37((bool)1);
		__this->set_mPlayMode_38((bool)1);
		Vector4_t4282066567  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m2441427762(&L_2, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mDrawRegion_39(L_2);
		__this->set_mIsVisibleByAlpha_41((bool)1);
		__this->set_mIsVisibleByPanel_42((bool)1);
		__this->set_mIsInFront_43((bool)1);
		__this->set_mCorners_47(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_mAlphaFrameID_48((-1));
		__this->set_mMatrixFrame_49((-1));
		IL2CPP_RUNTIME_CLASS_INIT(UIRect_t2503437976_il2cpp_TypeInfo_var);
		UIRect__ctor_m1059512579(__this, /*hidden argument*/NULL);
		return;
	}
}
// UIDrawCall/OnRenderCallback UIWidget::get_onRender()
extern "C"  OnRenderCallback_t2651898963 * UIWidget_get_onRender_m4268807263 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		OnRenderCallback_t2651898963 * L_0 = __this->get_mOnRender_29();
		return L_0;
	}
}
// System.Void UIWidget::set_onRender(UIDrawCall/OnRenderCallback)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* OnRenderCallback_t2651898963_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_onRender_m3581219820_MetadataUsageId;
extern "C"  void UIWidget_set_onRender_m3581219820 (UIWidget_t769069560 * __this, OnRenderCallback_t2651898963 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_onRender_m3581219820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnRenderCallback_t2651898963 * L_0 = __this->get_mOnRender_29();
		OnRenderCallback_t2651898963 * L_1 = ___value0;
		bool L_2 = MulticastDelegate_op_Inequality_m3462187897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t913273974 * L_3 = __this->get_drawCall_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t913273974 * L_5 = __this->get_drawCall_46();
		NullCheck(L_5);
		OnRenderCallback_t2651898963 * L_6 = L_5->get_onRender_33();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		OnRenderCallback_t2651898963 * L_7 = __this->get_mOnRender_29();
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t913273974 * L_8 = __this->get_drawCall_46();
		UIDrawCall_t913273974 * L_9 = L_8;
		NullCheck(L_9);
		OnRenderCallback_t2651898963 * L_10 = L_9->get_onRender_33();
		OnRenderCallback_t2651898963 * L_11 = __this->get_mOnRender_29();
		Delegate_t3310234105 * L_12 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_onRender_33(((OnRenderCallback_t2651898963 *)CastclassSealed(L_12, OnRenderCallback_t2651898963_il2cpp_TypeInfo_var)));
	}

IL_005e:
	{
		OnRenderCallback_t2651898963 * L_13 = ___value0;
		__this->set_mOnRender_29(L_13);
		UIDrawCall_t913273974 * L_14 = __this->get_drawCall_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t913273974 * L_16 = __this->get_drawCall_46();
		UIDrawCall_t913273974 * L_17 = L_16;
		NullCheck(L_17);
		OnRenderCallback_t2651898963 * L_18 = L_17->get_onRender_33();
		OnRenderCallback_t2651898963 * L_19 = ___value0;
		Delegate_t3310234105 * L_20 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_onRender_33(((OnRenderCallback_t2651898963 *)CastclassSealed(L_20, OnRenderCallback_t2651898963_il2cpp_TypeInfo_var)));
	}

IL_0092:
	{
		return;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawRegion()
extern "C"  Vector4_t4282066567  UIWidget_get_drawRegion_m382413595 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = __this->get_mDrawRegion_39();
		return L_0;
	}
}
// System.Void UIWidget::set_drawRegion(UnityEngine.Vector4)
extern "C"  void UIWidget_set_drawRegion_m231796482 (UIWidget_t769069560 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = __this->get_mDrawRegion_39();
		Vector4_t4282066567  L_1 = ___value0;
		bool L_2 = Vector4_op_Inequality_m3118756897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector4_t4282066567  L_3 = ___value0;
		__this->set_mDrawRegion_39(L_3);
		bool L_4 = __this->get_autoResizeBoxCollider_30();
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002f:
	{
		return;
	}
}
// UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern "C"  Vector2_t4282066565  UIWidget_get_pivotOffset_m2000981938 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = UIWidget_get_pivot_m3551772392(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = NGUIMath_GetPivotOffset_m2178153133(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UIWidget::get_width()
extern "C"  int32_t UIWidget_get_width_m293857264 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mWidth_24();
		return L_0;
	}
}
// System.Void UIWidget::set_width(System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_width_m3480390811_MetadataUsageId;
extern "C"  void UIWidget_set_width_m3480390811 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_width_m3480390811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mWidth_24();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UIRect::get_isAnchoredHorizontally() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t3581172420 * L_8 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t3581172420 * L_11 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = L_11->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)3)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if (L_16)
		{
			goto IL_00a6;
		}
	}

IL_0083:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)L_18))))), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a6:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if ((((int32_t)L_19) == ((int32_t)8)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)5)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mWidth_24();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (((float)((float)((int32_t)((int32_t)L_22-(int32_t)L_23))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mWidth_24();
		V_1 = ((int32_t)((int32_t)L_24-(int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, ((float)((float)(((float)((float)((-L_29)))))*(float)(0.5f))), (0.0f), ((float)((float)(((float)((float)L_30)))*(float)(0.5f))), (0.0f), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t3581172420 * L_31 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = L_31->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_32, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_34-(int32_t)L_35))))), (0.0f), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mWidth_24();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (((float)((float)((int32_t)((int32_t)L_36-(int32_t)L_37))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = ___value0;
		int32_t L_39 = __this->get_mHeight_25();
		UIWidget_SetDimensions_m163939926(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// System.Int32 UIWidget::get_height()
extern "C"  int32_t UIWidget_get_height_m1023454943 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mHeight_25();
		return L_0;
	}
}
// System.Void UIWidget::set_height(System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_height_m1838784918_MetadataUsageId;
extern "C"  void UIWidget_set_height_m1838784918 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_height_m1838784918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mHeight_25();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UIRect::get_isAnchoredVertically() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t3581172420 * L_8 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t3581172420 * L_11 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = L_11->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)7)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_16) == ((uint32_t)8))))
		{
			goto IL_00a7;
		}
	}

IL_0084:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)L_18))))), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a7:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if (!L_19)
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)1)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mHeight_25();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)((int32_t)L_22-(int32_t)L_23))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mHeight_25();
		V_1 = ((int32_t)((int32_t)L_24-(int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), ((float)((float)(((float)((float)((-L_29)))))*(float)(0.5f))), (0.0f), ((float)((float)(((float)((float)L_30)))*(float)(0.5f))), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t3581172420 * L_31 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = L_31->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_32, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_34-(int32_t)L_35))))), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mHeight_25();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)((int32_t)L_36-(int32_t)L_37))))), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = __this->get_mWidth_24();
		int32_t L_39 = ___value0;
		UIWidget_SetDimensions_m163939926(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// UnityEngine.Color UIWidget::get_color()
extern "C"  Color_t4194546905  UIWidget_get_color_m2224265652 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = __this->get_mColor_22();
		return L_0;
	}
}
// System.Void UIWidget::set_color(UnityEngine.Color)
extern "C"  void UIWidget_set_color_m1905035359 (UIWidget_t769069560 * __this, Color_t4194546905  ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Color_t4194546905  L_0 = __this->get_mColor_22();
		Color_t4194546905  L_1 = ___value0;
		bool L_2 = Color_op_Inequality_m1917261071(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Color_t4194546905 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = L_3->get_a_3();
		float L_5 = (&___value0)->get_a_3();
		V_0 = (bool)((((int32_t)((((float)L_4) == ((float)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Color_t4194546905  L_6 = ___value0;
		__this->set_mColor_22(L_6);
		bool L_7 = V_0;
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIWidget::Invalidate(System.Boolean) */, __this, L_7);
	}

IL_0037:
	{
		return;
	}
}
// System.Single UIWidget::get_alpha()
extern "C"  float UIWidget_get_alpha_m3456740746 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		return L_1;
	}
}
// System.Void UIWidget::set_alpha(System.Single)
extern "C"  void UIWidget_set_alpha_m3960663305 (UIWidget_t769069560 * __this, float ___value0, const MethodInfo* method)
{
	{
		Color_t4194546905 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		float L_2 = ___value0;
		if ((((float)L_1) == ((float)L_2)))
		{
			goto IL_0024;
		}
	}
	{
		Color_t4194546905 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = ___value0;
		L_3->set_a_3(L_4);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIWidget::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean UIWidget::get_isVisible()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_isVisible_m4257222444_MetadataUsageId;
extern "C"  bool UIWidget_get_isVisible_m4257222444 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_isVisible_m4257222444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		bool L_0 = __this->get_mIsVisibleByPanel_42();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		bool L_1 = __this->get_mIsVisibleByAlpha_41();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		bool L_2 = __this->get_mIsInFront_43();
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = ((UIRect_t2503437976 *)__this)->get_finalAlpha_20();
		if ((!(((float)L_3) > ((float)(0.001f)))))
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_4 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_4));
		goto IL_003a;
	}

IL_0039:
	{
		G_B6_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean UIWidget::get_hasVertices()
extern "C"  bool UIWidget_get_hasVertices_m262926455 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UIGeometry_t3586695974 * L_0 = __this->get_geometry_36();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UIGeometry_t3586695974 * L_1 = __this->get_geometry_36();
		NullCheck(L_1);
		bool L_2 = UIGeometry_get_hasVertices_m167811977(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// UIWidget/Pivot UIWidget::get_rawPivot()
extern "C"  int32_t UIWidget_get_rawPivot_m978044726 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_rawPivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_rawPivot_m1762094445 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_mPivot_23(L_2);
		bool L_3 = __this->get_autoResizeBoxCollider_30();
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002a:
	{
		return;
	}
}
// UIWidget/Pivot UIWidget::get_pivot()
extern "C"  int32_t UIWidget_get_pivot_m3551772392 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_pivot(UIWidget/Pivot)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_pivot_m2063605531_MetadataUsageId;
extern "C"  void UIWidget_set_pivot_m2063605531 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_pivot_m2063605531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t1659122786 * V_2 = NULL;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_00ea;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(11 /* UnityEngine.Vector3[] UIWidget::get_worldCorners() */, __this);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		V_0 = (*(Vector3_t4282066566 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t L_3 = ___value0;
		__this->set_mPivot_23(L_3);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		Vector3U5BU5D_t215400611* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(11 /* UnityEngine.Vector3[] UIWidget::get_worldCorners() */, __this);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		V_1 = (*(Vector3_t4282066566 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Transform_t1659122786 * L_5 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		Transform_t1659122786 * L_6 = V_2;
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Transform_t1659122786 * L_8 = V_2;
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_z_3();
		V_4 = L_10;
		Vector3_t4282066566 * L_11 = (&V_3);
		float L_12 = L_11->get_x_1();
		float L_13 = (&V_0)->get_x_1();
		float L_14 = (&V_1)->get_x_1();
		L_11->set_x_1(((float)((float)L_12+(float)((float)((float)L_13-(float)L_14)))));
		Vector3_t4282066566 * L_15 = (&V_3);
		float L_16 = L_15->get_y_2();
		float L_17 = (&V_0)->get_y_2();
		float L_18 = (&V_1)->get_y_2();
		L_15->set_y_2(((float)((float)L_16+(float)((float)((float)L_17-(float)L_18)))));
		Transform_t1659122786 * L_19 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = V_3;
		NullCheck(L_19);
		Transform_set_position_m3111394108(L_19, L_20, /*hidden argument*/NULL);
		Transform_t1659122786 * L_21 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_localPosition_m668140784(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = bankers_roundf(L_23);
		(&V_3)->set_x_1(L_24);
		float L_25 = (&V_3)->get_y_2();
		float L_26 = bankers_roundf(L_25);
		(&V_3)->set_y_2(L_26);
		float L_27 = V_4;
		(&V_3)->set_z_3(L_27);
		Transform_t1659122786 * L_28 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29 = V_3;
		NullCheck(L_28);
		Transform_set_localPosition_m3504330903(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		return;
	}
}
// System.Int32 UIWidget::get_depth()
extern "C"  int32_t UIWidget_get_depth_m507722157 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mDepth_26();
		return L_0;
	}
}
// System.Void UIWidget::set_depth(System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_depth_m1098656472_MetadataUsageId;
extern "C"  void UIWidget_set_depth_m1098656472 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_depth_m1098656472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_mDepth_26();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		UIPanel_t295209936 * L_4 = __this->get_panel_35();
		NullCheck(L_4);
		UIPanel_RemoveWidget_m3627052857(L_4, __this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = ___value0;
		__this->set_mDepth_26(L_5);
		UIPanel_t295209936 * L_6 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t295209936 * L_8 = __this->get_panel_35();
		NullCheck(L_8);
		UIPanel_AddWidget_m1579877318(L_8, __this, /*hidden argument*/NULL);
		bool L_9 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t295209936 * L_10 = __this->get_panel_35();
		NullCheck(L_10);
		UIPanel_SortWidgets_m710919082(L_10, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_11 = __this->get_panel_35();
		NullCheck(L_11);
		UIPanel_RebuildAllDrawCalls_m1475381252(L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Int32 UIWidget::get_raycastDepth()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_raycastDepth_m697183634_MetadataUsageId;
extern "C"  int32_t UIWidget_get_raycastDepth_m697183634 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_raycastDepth_m697183634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = __this->get_mDepth_26();
		UIPanel_t295209936 * L_5 = __this->get_panel_35();
		NullCheck(L_5);
		int32_t L_6 = UIPanel_get_depth_m2725856257(L_5, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)1000)))));
		goto IL_004c;
	}

IL_0046:
	{
		int32_t L_7 = __this->get_mDepth_26();
		G_B5_0 = L_7;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3[] UIWidget::get_localCorners()
extern "C"  Vector3U5BU5D_t215400611* UIWidget_get_localCorners_m4123646229 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector2_t4282066565  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Vector3U5BU5D_t215400611* L_9 = __this->get_mCorners_47();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		float L_10 = V_1;
		float L_11 = V_2;
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m1846874791(&L_12, L_10, L_11, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_12;
		Vector3U5BU5D_t215400611* L_13 = __this->get_mCorners_47();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		float L_14 = V_1;
		float L_15 = V_4;
		Vector3_t4282066566  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1846874791(&L_16, L_14, L_15, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_16;
		Vector3U5BU5D_t215400611* L_17 = __this->get_mCorners_47();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		float L_18 = V_3;
		float L_19 = V_4;
		Vector3_t4282066566  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m1846874791(&L_20, L_18, L_19, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_20;
		Vector3U5BU5D_t215400611* L_21 = __this->get_mCorners_47();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		float L_22 = V_3;
		float L_23 = V_2;
		Vector3_t4282066566  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1846874791(&L_24, L_22, L_23, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_24;
		Vector3U5BU5D_t215400611* L_25 = __this->get_mCorners_47();
		return L_25;
	}
}
// UnityEngine.Vector2 UIWidget::get_localSize()
extern "C"  Vector2_t4282066565  UIWidget_get_localSize_m237983625 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t215400611* V_0 = NULL;
	{
		Vector3U5BU5D_t215400611* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(10 /* UnityEngine.Vector3[] UIWidget::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t215400611* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 2);
		Vector3U5BU5D_t215400611* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		Vector3_t4282066566  L_3 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, (*(Vector3_t4282066566 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (*(Vector3_t4282066566 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 UIWidget::get_localCenter()
extern "C"  Vector3_t4282066566  UIWidget_get_localCenter_m64081310 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t215400611* V_0 = NULL;
	{
		Vector3U5BU5D_t215400611* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(10 /* UnityEngine.Vector3[] UIWidget::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t215400611* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		Vector3U5BU5D_t215400611* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		Vector3_t4282066566  L_3 = Vector3_Lerp_m650470329(NULL /*static, unused*/, (*(Vector3_t4282066566 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t4282066566 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3[] UIWidget::get_worldCorners()
extern "C"  Vector3U5BU5D_t215400611* UIWidget_get_worldCorners_m3779221710 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Transform_t1659122786 * V_5 = NULL;
	{
		Vector2_t4282066565  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Transform_t1659122786 * L_9 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_5 = L_9;
		Vector3U5BU5D_t215400611* L_10 = __this->get_mCorners_47();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		Transform_t1659122786 * L_11 = V_5;
		float L_12 = V_1;
		float L_13 = V_2;
		NullCheck(L_11);
		Vector3_t4282066566  L_14 = Transform_TransformPoint_m3094713172(L_11, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_14;
		Vector3U5BU5D_t215400611* L_15 = __this->get_mCorners_47();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		Transform_t1659122786 * L_16 = V_5;
		float L_17 = V_1;
		float L_18 = V_4;
		NullCheck(L_16);
		Vector3_t4282066566  L_19 = Transform_TransformPoint_m3094713172(L_16, L_17, L_18, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_19;
		Vector3U5BU5D_t215400611* L_20 = __this->get_mCorners_47();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		Transform_t1659122786 * L_21 = V_5;
		float L_22 = V_3;
		float L_23 = V_4;
		NullCheck(L_21);
		Vector3_t4282066566  L_24 = Transform_TransformPoint_m3094713172(L_21, L_22, L_23, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_24;
		Vector3U5BU5D_t215400611* L_25 = __this->get_mCorners_47();
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 3);
		Transform_t1659122786 * L_26 = V_5;
		float L_27 = V_3;
		float L_28 = V_2;
		NullCheck(L_26);
		Vector3_t4282066566  L_29 = Transform_TransformPoint_m3094713172(L_26, L_27, L_28, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_29;
		Vector3U5BU5D_t215400611* L_30 = __this->get_mCorners_47();
		return L_30;
	}
}
// UnityEngine.Vector3 UIWidget::get_worldCenter()
extern "C"  Vector3_t4282066566  UIWidget_get_worldCenter_m3655201477 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = UIWidget_get_localCenter_m64081310(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_2 = Transform_TransformPoint_m437395512(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawingDimensions()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_drawingDimensions_m3426169802_MetadataUsageId;
extern "C"  Vector4_t4282066567  UIWidget_get_drawingDimensions_m3426169802 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_drawingDimensions_m3426169802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B11_2 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B10_1 = 0.0f;
	float G_B10_2 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B12_2 = 0.0f;
	float G_B12_3 = 0.0f;
	{
		Vector2_t4282066565  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Vector4_t4282066567 * L_9 = __this->get_address_of_mDrawRegion_39();
		float L_10 = L_9->get_x_1();
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_11 = V_1;
		G_B3_0 = L_11;
		goto IL_006b;
	}

IL_0059:
	{
		float L_12 = V_1;
		float L_13 = V_3;
		Vector4_t4282066567 * L_14 = __this->get_address_of_mDrawRegion_39();
		float L_15 = L_14->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		G_B3_0 = L_16;
	}

IL_006b:
	{
		Vector4_t4282066567 * L_17 = __this->get_address_of_mDrawRegion_39();
		float L_18 = L_17->get_y_2();
		G_B4_0 = G_B3_0;
		if ((!(((float)L_18) == ((float)(0.0f)))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0086;
		}
	}
	{
		float L_19 = V_2;
		G_B6_0 = L_19;
		G_B6_1 = G_B4_0;
		goto IL_0099;
	}

IL_0086:
	{
		float L_20 = V_2;
		float L_21 = V_4;
		Vector4_t4282066567 * L_22 = __this->get_address_of_mDrawRegion_39();
		float L_23 = L_22->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_20, L_21, L_23, /*hidden argument*/NULL);
		G_B6_0 = L_24;
		G_B6_1 = G_B5_0;
	}

IL_0099:
	{
		Vector4_t4282066567 * L_25 = __this->get_address_of_mDrawRegion_39();
		float L_26 = L_25->get_z_3();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if ((!(((float)L_26) == ((float)(1.0f)))))
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			goto IL_00b4;
		}
	}
	{
		float L_27 = V_3;
		G_B9_0 = L_27;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c6;
	}

IL_00b4:
	{
		float L_28 = V_1;
		float L_29 = V_3;
		Vector4_t4282066567 * L_30 = __this->get_address_of_mDrawRegion_39();
		float L_31 = L_30->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_28, L_29, L_31, /*hidden argument*/NULL);
		G_B9_0 = L_32;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c6:
	{
		Vector4_t4282066567 * L_33 = __this->get_address_of_mDrawRegion_39();
		float L_34 = L_33->get_w_4();
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		if ((!(((float)L_34) == ((float)(1.0f)))))
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			G_B11_2 = G_B9_2;
			goto IL_00e2;
		}
	}
	{
		float L_35 = V_4;
		G_B12_0 = L_35;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_00f5;
	}

IL_00e2:
	{
		float L_36 = V_2;
		float L_37 = V_4;
		Vector4_t4282066567 * L_38 = __this->get_address_of_mDrawRegion_39();
		float L_39 = L_38->get_w_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_40 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_36, L_37, L_39, /*hidden argument*/NULL);
		G_B12_0 = L_40;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_00f5:
	{
		Vector4_t4282066567  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector4__ctor_m2441427762(&L_41, G_B12_3, G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		return L_41;
	}
}
// UnityEngine.Material UIWidget::get_material()
extern "C"  Material_t3870600107 * UIWidget_get_material_m1008364976 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		return (Material_t3870600107 *)NULL;
	}
}
// System.Void UIWidget::set_material(UnityEngine.Material)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4087805087;
extern const uint32_t UIWidget_set_material_m1657715431_MetadataUsageId;
extern "C"  void UIWidget_set_material_m1657715431 (UIWidget_t769069560 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_material_m1657715431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, L_0, _stringLiteral4087805087, /*hidden argument*/NULL);
		NotImplementedException_t1912495542 * L_2 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Texture UIWidget::get_mainTexture()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_mainTexture_m1554632171_MetadataUsageId;
extern "C"  Texture_t2526458961 * UIWidget_get_mainTexture_m1554632171 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_mainTexture_m1554632171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * V_0 = NULL;
	Texture_t2526458961 * G_B3_0 = NULL;
	{
		Material_t3870600107 * L_0 = VirtFuncInvoker0< Material_t3870600107 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t3870600107 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t3870600107 * L_3 = V_0;
		NullCheck(L_3);
		Texture_t2526458961 * L_4 = Material_get_mainTexture_m1012267054(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Texture_t2526458961 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_mainTexture(UnityEngine.Texture)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660659926;
extern const uint32_t UIWidget_set_mainTexture_m1243757896_MetadataUsageId;
extern "C"  void UIWidget_set_mainTexture_m1243757896 (UIWidget_t769069560 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_mainTexture_m1243757896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, L_0, _stringLiteral3660659926, /*hidden argument*/NULL);
		NotImplementedException_t1912495542 * L_2 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Shader UIWidget::get_shader()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_shader_m1849353712_MetadataUsageId;
extern "C"  Shader_t3191267369 * UIWidget_get_shader_m1849353712 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_shader_m1849353712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * V_0 = NULL;
	Shader_t3191267369 * G_B3_0 = NULL;
	{
		Material_t3870600107 * L_0 = VirtFuncInvoker0< Material_t3870600107 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t3870600107 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t3870600107 * L_3 = V_0;
		NullCheck(L_3);
		Shader_t3191267369 * L_4 = Material_get_shader_m2881845503(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Shader_t3191267369 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_shader(UnityEngine.Shader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1613366753;
extern const uint32_t UIWidget_set_shader_m1936742439_MetadataUsageId;
extern "C"  void UIWidget_set_shader_m1936742439 (UIWidget_t769069560 * __this, Shader_t3191267369 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_shader_m1936742439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, L_0, _stringLiteral1613366753, /*hidden argument*/NULL);
		NotImplementedException_t1912495542 * L_2 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Vector2 UIWidget::get_relativeSize()
extern "C"  Vector2_t4282066565  UIWidget_get_relativeSize_m726361522 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UIWidget::get_hasBoxCollider()
extern Il2CppClass* BoxCollider_t2538127765_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t2939674232_m3246438266_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t2212926951_m2584023519_MethodInfo_var;
extern const uint32_t UIWidget_get_hasBoxCollider_m2767222083_MetadataUsageId;
extern "C"  bool UIWidget_get_hasBoxCollider_m2767222083 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_hasBoxCollider_m2767222083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BoxCollider_t2538127765 * V_0 = NULL;
	{
		Collider_t2939674232 * L_0 = Component_GetComponent_TisCollider_t2939674232_m3246438266(__this, /*hidden argument*/Component_GetComponent_TisCollider_t2939674232_m3246438266_MethodInfo_var);
		V_0 = ((BoxCollider_t2538127765 *)IsInstSealed(L_0, BoxCollider_t2538127765_il2cpp_TypeInfo_var));
		BoxCollider_t2538127765 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		BoxCollider2D_t2212926951 * L_3 = Component_GetComponent_TisBoxCollider2D_t2212926951_m2584023519(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t2212926951_m2584023519_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetDimensions_m163939926_MetadataUsageId;
extern "C"  void UIWidget_SetDimensions_m163939926 (UIWidget_t769069560 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDimensions_m163939926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_mWidth_24();
		int32_t L_1 = ___w0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_mHeight_25();
		int32_t L_3 = ___h1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_00b8;
		}
	}

IL_0018:
	{
		int32_t L_4 = ___w0;
		__this->set_mWidth_24(L_4);
		int32_t L_5 = ___h1;
		__this->set_mHeight_25(L_5);
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_7 = __this->get_mWidth_24();
		float L_8 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_7)))/(float)L_8)), /*hidden argument*/NULL);
		__this->set_mHeight_25(L_9);
		goto IL_009a;
	}

IL_0050:
	{
		int32_t L_10 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_11 = __this->get_mHeight_25();
		float L_12 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_11)))*(float)L_12)), /*hidden argument*/NULL);
		__this->set_mWidth_24(L_13);
		goto IL_009a;
	}

IL_007a:
	{
		int32_t L_14 = __this->get_keepAspectRatio_32();
		if (L_14)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_15 = __this->get_mWidth_24();
		int32_t L_16 = __this->get_mHeight_25();
		__this->set_aspectRatio_33(((float)((float)(((float)((float)L_15)))/(float)(((float)((float)L_16))))));
	}

IL_009a:
	{
		__this->set_mMoved_45((bool)1);
		bool L_17 = __this->get_autoResizeBoxCollider_30();
		if (!L_17)
		{
			goto IL_00b2;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_00b8:
	{
		return;
	}
}
// UnityEngine.Vector3[] UIWidget::GetSides(UnityEngine.Transform)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_GetSides_m715844286_MetadataUsageId;
extern "C"  Vector3U5BU5D_t215400611* UIWidget_GetSides_m715844286 (UIWidget_t769069560 * __this, Transform_t1659122786 * ___relativeTo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_GetSides_m715844286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Transform_t1659122786 * V_7 = NULL;
	int32_t V_8 = 0;
	{
		Vector2_t4282066565  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		float L_9 = V_1;
		float L_10 = V_3;
		V_5 = ((float)((float)((float)((float)L_9+(float)L_10))*(float)(0.5f)));
		float L_11 = V_2;
		float L_12 = V_4;
		V_6 = ((float)((float)((float)((float)L_11+(float)L_12))*(float)(0.5f)));
		Transform_t1659122786 * L_13 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_7 = L_13;
		Vector3U5BU5D_t215400611* L_14 = __this->get_mCorners_47();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		Transform_t1659122786 * L_15 = V_7;
		float L_16 = V_1;
		float L_17 = V_6;
		NullCheck(L_15);
		Vector3_t4282066566  L_18 = Transform_TransformPoint_m3094713172(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_18;
		Vector3U5BU5D_t215400611* L_19 = __this->get_mCorners_47();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		Transform_t1659122786 * L_20 = V_7;
		float L_21 = V_5;
		float L_22 = V_4;
		NullCheck(L_20);
		Vector3_t4282066566  L_23 = Transform_TransformPoint_m3094713172(L_20, L_21, L_22, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_23;
		Vector3U5BU5D_t215400611* L_24 = __this->get_mCorners_47();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		Transform_t1659122786 * L_25 = V_7;
		float L_26 = V_3;
		float L_27 = V_6;
		NullCheck(L_25);
		Vector3_t4282066566  L_28 = Transform_TransformPoint_m3094713172(L_25, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_28;
		Vector3U5BU5D_t215400611* L_29 = __this->get_mCorners_47();
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		Transform_t1659122786 * L_30 = V_7;
		float L_31 = V_5;
		float L_32 = V_2;
		NullCheck(L_30);
		Vector3_t4282066566  L_33 = Transform_TransformPoint_m3094713172(L_30, L_31, L_32, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_33;
		Transform_t1659122786 * L_34 = ___relativeTo0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_34, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012a;
		}
	}
	{
		V_8 = 0;
		goto IL_0122;
	}

IL_00f2:
	{
		Vector3U5BU5D_t215400611* L_36 = __this->get_mCorners_47();
		int32_t L_37 = V_8;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		Transform_t1659122786 * L_38 = ___relativeTo0;
		Vector3U5BU5D_t215400611* L_39 = __this->get_mCorners_47();
		int32_t L_40 = V_8;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		NullCheck(L_38);
		Vector3_t4282066566  L_41 = Transform_InverseTransformPoint_m1626812000(L_38, (*(Vector3_t4282066566 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))) = L_41;
		int32_t L_42 = V_8;
		V_8 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_0122:
	{
		int32_t L_43 = V_8;
		if ((((int32_t)L_43) < ((int32_t)4)))
		{
			goto IL_00f2;
		}
	}

IL_012a:
	{
		Vector3U5BU5D_t215400611* L_44 = __this->get_mCorners_47();
		return L_44;
	}
}
// System.Single UIWidget::CalculateFinalAlpha(System.Int32)
extern "C"  float UIWidget_CalculateFinalAlpha_m3596038484 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mAlphaFrameID_48();
		int32_t L_1 = ___frameID0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___frameID0;
		__this->set_mAlphaFrameID_48(L_2);
		int32_t L_3 = ___frameID0;
		UIWidget_UpdateFinalAlpha_m4090184323(__this, L_3, /*hidden argument*/NULL);
	}

IL_001a:
	{
		float L_4 = ((UIRect_t2503437976 *)__this)->get_finalAlpha_20();
		return L_4;
	}
}
// System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpdateFinalAlpha_m4090184323_MetadataUsageId;
extern "C"  void UIWidget_UpdateFinalAlpha_m4090184323 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateFinalAlpha_m4090184323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIRect_t2503437976 * V_0 = NULL;
	UIWidget_t769069560 * G_B5_0 = NULL;
	UIWidget_t769069560 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	UIWidget_t769069560 * G_B6_1 = NULL;
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_41();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_mIsInFront_43();
		if (L_1)
		{
			goto IL_0026;
		}
	}

IL_0016:
	{
		((UIRect_t2503437976 *)__this)->set_finalAlpha_20((0.0f));
		goto IL_0062;
	}

IL_0026:
	{
		UIRect_t2503437976 * L_2 = UIRect_get_parent_m2985047705(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		UIRect_t2503437976 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if (!L_4)
		{
			G_B5_0 = __this;
			goto IL_0052;
		}
	}
	{
		UIRect_t2503437976 * L_5 = V_0;
		int32_t L_6 = ___frameID0;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_5, L_6);
		Color_t4194546905 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B6_0 = ((float)((float)L_7*(float)L_9));
		G_B6_1 = G_B4_0;
		goto IL_005d;
	}

IL_0052:
	{
		Color_t4194546905 * L_10 = __this->get_address_of_mColor_22();
		float L_11 = L_10->get_a_3();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
	}

IL_005d:
	{
		NullCheck(G_B6_1);
		((UIRect_t2503437976 *)G_B6_1)->set_finalAlpha_20(G_B6_0);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UIWidget::Invalidate(System.Boolean)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_Invalidate_m466609203_MetadataUsageId;
extern "C"  void UIWidget_Invalidate_m466609203 (UIWidget_t769069560 * __this, bool ___includeChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_Invalidate_m466609203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		__this->set_mAlphaFrameID_48((-1));
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007f;
		}
	}
	{
		bool L_2 = __this->get_hideIfOffScreen_31();
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		UIPanel_t295209936 * L_3 = __this->get_panel_35();
		NullCheck(L_3);
		bool L_4 = UIPanel_get_hasCumulativeClipping_m3545993459(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}

IL_003a:
	{
		UIPanel_t295209936 * L_5 = __this->get_panel_35();
		NullCheck(L_5);
		bool L_6 = UIPanel_IsVisible_m3718013533(L_5, __this, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 1;
	}

IL_004c:
	{
		V_0 = (bool)G_B5_0;
		int32_t L_7 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = UIWidget_CalculateCumulativeAlpha_m2518513923(__this, L_7, /*hidden argument*/NULL);
		bool L_9 = V_0;
		UIWidget_UpdateVisibility_m143185718(__this, (bool)((((float)L_8) > ((float)(0.001f)))? 1 : 0), L_9, /*hidden argument*/NULL);
		int32_t L_10 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		UIWidget_UpdateFinalAlpha_m4090184323(__this, L_10, /*hidden argument*/NULL);
		bool L_11 = ___includeChildren0;
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		UIRect_Invalidate_m3602426579(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CalculateCumulativeAlpha_m2518513923_MetadataUsageId;
extern "C"  float UIWidget_CalculateCumulativeAlpha_m2518513923 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CalculateCumulativeAlpha_m2518513923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIRect_t2503437976 * V_0 = NULL;
	float G_B3_0 = 0.0f;
	{
		UIRect_t2503437976 * L_0 = UIRect_get_parent_m2985047705(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		UIRect_t2503437976 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		UIRect_t2503437976 * L_3 = V_0;
		int32_t L_4 = ___frameID0;
		NullCheck(L_3);
		float L_5 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_3, L_4);
		Color_t4194546905 * L_6 = __this->get_address_of_mColor_22();
		float L_7 = L_6->get_a_3();
		G_B3_0 = ((float)((float)L_5*(float)L_7));
		goto IL_0036;
	}

IL_002b:
	{
		Color_t4194546905 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetRect_m2259467731_MetadataUsageId;
extern "C"  void UIWidget_SetRect_m2259467731 (UIWidget_t769069560 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetRect_m2259467731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Transform_t1659122786 * V_5 = NULL;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Vector2_t4282066565  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___x0;
		float L_2 = ___x0;
		float L_3 = ___width2;
		float L_4 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_1, ((float)((float)L_2+(float)L_3)), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = ___y1;
		float L_7 = ___y1;
		float L_8 = ___height3;
		float L_9 = (&V_0)->get_y_2();
		float L_10 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_6, ((float)((float)L_7+(float)L_8)), L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = ___width2;
		int32_t L_12 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)L_11+(float)(0.5f))), /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = ___height3;
		int32_t L_14 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)L_13+(float)(0.5f))), /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = (&V_0)->get_x_1();
		if ((!(((float)L_15) == ((float)(0.5f)))))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16>>(int32_t)1))<<(int32_t)1));
	}

IL_005d:
	{
		float L_17 = (&V_0)->get_y_2();
		if ((!(((float)L_17) == ((float)(0.5f)))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18>>(int32_t)1))<<(int32_t)1));
	}

IL_0076:
	{
		Transform_t1659122786 * L_19 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_5 = L_19;
		Transform_t1659122786 * L_20 = V_5;
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_localPosition_m668140784(L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		float L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_23 = floorf(((float)((float)L_22+(float)(0.5f))));
		(&V_6)->set_x_1(L_23);
		float L_24 = V_2;
		float L_25 = floorf(((float)((float)L_24+(float)(0.5f))));
		(&V_6)->set_y_2(L_25);
		int32_t L_26 = V_3;
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_3 = L_28;
	}

IL_00c0:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_29) >= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_4 = L_31;
	}

IL_00d5:
	{
		Transform_t1659122786 * L_32 = V_5;
		Vector3_t4282066566  L_33 = V_6;
		NullCheck(L_32);
		Transform_set_localPosition_m3504330903(L_32, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		UIWidget_set_width_m3480390811(__this, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_4;
		UIWidget_set_height_m1838784918(__this, L_35, /*hidden argument*/NULL);
		bool L_36 = UIRect_get_isAnchored_m3752430236(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0192;
		}
	}
	{
		Transform_t1659122786 * L_37 = V_5;
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = Transform_get_parent_m2236876972(L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		AnchorPoint_t3581172420 * L_39 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_39);
		Transform_t1659122786 * L_40 = L_39->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		AnchorPoint_t3581172420 * L_42 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t1659122786 * L_43 = V_5;
		float L_44 = ___x0;
		NullCheck(L_42);
		AnchorPoint_SetHorizontal_m2786298215(L_42, L_43, L_44, /*hidden argument*/NULL);
	}

IL_0124:
	{
		AnchorPoint_t3581172420 * L_45 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_45);
		Transform_t1659122786 * L_46 = L_45->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0149;
		}
	}
	{
		AnchorPoint_t3581172420 * L_48 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		Transform_t1659122786 * L_49 = V_5;
		float L_50 = ___x0;
		float L_51 = ___width2;
		NullCheck(L_48);
		AnchorPoint_SetHorizontal_m2786298215(L_48, L_49, ((float)((float)L_50+(float)L_51)), /*hidden argument*/NULL);
	}

IL_0149:
	{
		AnchorPoint_t3581172420 * L_52 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_52);
		Transform_t1659122786 * L_53 = L_52->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		AnchorPoint_t3581172420 * L_55 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		Transform_t1659122786 * L_56 = V_5;
		float L_57 = ___y1;
		NullCheck(L_55);
		AnchorPoint_SetVertical_m4045095893(L_55, L_56, L_57, /*hidden argument*/NULL);
	}

IL_016c:
	{
		AnchorPoint_t3581172420 * L_58 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_58);
		Transform_t1659122786 * L_59 = L_58->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0192;
		}
	}
	{
		AnchorPoint_t3581172420 * L_61 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		Transform_t1659122786 * L_62 = V_5;
		float L_63 = ___y1;
		float L_64 = ___height3;
		NullCheck(L_61);
		AnchorPoint_SetVertical_m4045095893(L_61, L_62, ((float)((float)L_63+(float)L_64)), /*hidden argument*/NULL);
	}

IL_0192:
	{
		return;
	}
}
// System.Void UIWidget::ResizeCollider()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_ResizeCollider_m654642185_MetadataUsageId;
extern "C"  void UIWidget_ResizeCollider_m654642185 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ResizeCollider_m654642185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m2709673216(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 UIWidget::FullCompareFunc(UIWidget,UIWidget)
extern Il2CppClass* UIPanel_t295209936_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_FullCompareFunc_m3896523897_MetadataUsageId;
extern "C"  int32_t UIWidget_FullCompareFunc_m3896523897 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___left0, UIWidget_t769069560 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_FullCompareFunc_m3896523897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		UIWidget_t769069560 * L_0 = ___left0;
		NullCheck(L_0);
		UIPanel_t295209936 * L_1 = L_0->get_panel_35();
		UIWidget_t769069560 * L_2 = ___right1;
		NullCheck(L_2);
		UIPanel_t295209936 * L_3 = L_2->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t295209936_il2cpp_TypeInfo_var);
		int32_t L_4 = UIPanel_CompareFunc_m951703214(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_t769069560 * L_6 = ___left0;
		UIWidget_t769069560 * L_7 = ___right1;
		int32_t L_8 = UIWidget_PanelCompareFunc_m1938899026(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		goto IL_0025;
	}

IL_0024:
	{
		int32_t L_9 = V_0;
		G_B3_0 = L_9;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_PanelCompareFunc_m1938899026_MetadataUsageId;
extern "C"  int32_t UIWidget_PanelCompareFunc_m1938899026 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___left0, UIWidget_t769069560 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_PanelCompareFunc_m1938899026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * V_0 = NULL;
	Material_t3870600107 * V_1 = NULL;
	int32_t G_B13_0 = 0;
	{
		UIWidget_t769069560 * L_0 = ___left0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_mDepth_26();
		UIWidget_t769069560 * L_2 = ___right1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_mDepth_26();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		UIWidget_t769069560 * L_4 = ___left0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_mDepth_26();
		UIWidget_t769069560 * L_6 = ___right1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_mDepth_26();
		if ((((int32_t)L_5) <= ((int32_t)L_7)))
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		UIWidget_t769069560 * L_8 = ___left0;
		NullCheck(L_8);
		Material_t3870600107 * L_9 = VirtFuncInvoker0< Material_t3870600107 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, L_8);
		V_0 = L_9;
		UIWidget_t769069560 * L_10 = ___right1;
		NullCheck(L_10);
		Material_t3870600107 * L_11 = VirtFuncInvoker0< Material_t3870600107 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, L_10);
		V_1 = L_11;
		Material_t3870600107 * L_12 = V_0;
		Material_t3870600107 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}

IL_0042:
	{
		Material_t3870600107 * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_15, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0050;
		}
	}
	{
		return 1;
	}

IL_0050:
	{
		Material_t3870600107 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_17, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_005e;
		}
	}
	{
		return (-1);
	}

IL_005e:
	{
		Material_t3870600107 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = Object_GetInstanceID_m200424466(L_19, /*hidden argument*/NULL);
		Material_t3870600107 * L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = Object_GetInstanceID_m200424466(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_20) >= ((int32_t)L_22)))
		{
			goto IL_0075;
		}
	}
	{
		G_B13_0 = (-1);
		goto IL_0076;
	}

IL_0075:
	{
		G_B13_0 = 1;
	}

IL_0076:
	{
		return G_B13_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds()
extern "C"  Bounds_t2711641849  UIWidget_CalculateBounds_m2567301067 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Bounds_t2711641849  L_0 = UIWidget_CalculateBounds_m1943782482(__this, (Transform_t1659122786 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CalculateBounds_m1943782482_MetadataUsageId;
extern "C"  Bounds_t2711641849  UIWidget_CalculateBounds_m1943782482 (UIWidget_t769069560 * __this, Transform_t1659122786 * ___relativeParent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CalculateBounds_m1943782482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3U5BU5D_t215400611* V_0 = NULL;
	Bounds_t2711641849  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Matrix4x4_t1651859333  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3U5BU5D_t215400611* V_4 = NULL;
	Bounds_t2711641849  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	{
		Transform_t1659122786 * L_0 = ___relativeParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(10 /* UnityEngine.Vector3[] UIWidget::get_localCorners() */, __this);
		V_0 = L_2;
		Vector3U5BU5D_t215400611* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		Vector3_t4282066566  L_4 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m4160293652((&V_1), (*(Vector3_t4282066566 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), L_4, /*hidden argument*/NULL);
		V_2 = 1;
		goto IL_0049;
	}

IL_0032:
	{
		Vector3U5BU5D_t215400611* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Bounds_Encapsulate_m3624685234((&V_1), (*(Vector3_t4282066566 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) < ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		Bounds_t2711641849  L_9 = V_1;
		return L_9;
	}

IL_0052:
	{
		Transform_t1659122786 * L_10 = ___relativeParent0;
		NullCheck(L_10);
		Matrix4x4_t1651859333  L_11 = Transform_get_worldToLocalMatrix_m3792395652(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Vector3U5BU5D_t215400611* L_12 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(11 /* UnityEngine.Vector3[] UIWidget::get_worldCorners() */, __this);
		V_4 = L_12;
		Vector3U5BU5D_t215400611* L_13 = V_4;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		Vector3_t4282066566  L_14 = Matrix4x4_MultiplyPoint3x4_m2198174902((&V_3), (*(Vector3_t4282066566 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m4160293652((&V_5), L_14, L_15, /*hidden argument*/NULL);
		V_6 = 1;
		goto IL_00ab;
	}

IL_0089:
	{
		Vector3U5BU5D_t215400611* L_16 = V_4;
		int32_t L_17 = V_6;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector3_t4282066566  L_18 = Matrix4x4_MultiplyPoint3x4_m2198174902((&V_3), (*(Vector3_t4282066566 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))), /*hidden argument*/NULL);
		Bounds_Encapsulate_m3624685234((&V_5), L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00ab:
	{
		int32_t L_20 = V_6;
		if ((((int32_t)L_20) < ((int32_t)4)))
		{
			goto IL_0089;
		}
	}
	{
		Bounds_t2711641849  L_21 = V_5;
		return L_21;
	}
}
// System.Void UIWidget::SetDirty()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetDirty_m3979225553_MetadataUsageId;
extern "C"  void UIWidget_SetDirty_m3979225553 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDirty_m3979225553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIDrawCall_t913273974 * L_0 = __this->get_drawCall_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		UIDrawCall_t913273974 * L_2 = __this->get_drawCall_46();
		NullCheck(L_2);
		L_2->set_isDirty_31((bool)1);
		goto IL_003f;
	}

IL_0022:
	{
		bool L_3 = UIWidget_get_isVisible_m4257222444(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = UIWidget_get_hasVertices_m262926455(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void UIWidget::RemoveFromPanel()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_RemoveFromPanel_m3233223575_MetadataUsageId;
extern "C"  void UIWidget_RemoveFromPanel_m3233223575 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_RemoveFromPanel_m3233223575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		NullCheck(L_2);
		UIPanel_RemoveWidget_m3627052857(L_2, __this, /*hidden argument*/NULL);
		__this->set_panel_35((UIPanel_t295209936 *)NULL);
	}

IL_0024:
	{
		__this->set_drawCall_46((UIDrawCall_t913273974 *)NULL);
		return;
	}
}
// System.Void UIWidget::MarkAsChanged()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_MarkAsChanged_m3551758678_MetadataUsageId;
extern "C"  void UIWidget_MarkAsChanged_m3551758678 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MarkAsChanged_m3551758678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0055;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		UIPanel_t295209936 * L_1 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		bool L_6 = __this->get_mPlayMode_38();
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		UIWidget_SetDirty_m3979225553(__this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m388028458(__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// UIPanel UIWidget::CreatePanel()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* UIPanel_t295209936_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CreatePanel_m851962502_MetadataUsageId;
extern "C"  UIPanel_t295209936 * UIWidget_CreatePanel_m851962502 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CreatePanel_m851962502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((UIRect_t2503437976 *)__this)->get_mStarted_11();
		if (!L_0)
		{
			goto IL_0085;
		}
	}
	{
		UIPanel_t295209936 * L_1 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0085;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		Transform_t1659122786 * L_6 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_7 = UIRect_get_cachedGameObject_m418520850(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = GameObject_get_layer_m1648550306(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t295209936_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_9 = UIPanel_Find_m1125240312(NULL /*static, unused*/, L_6, (bool)1, L_8, /*hidden argument*/NULL);
		__this->set_panel_35(L_9);
		UIPanel_t295209936 * L_10 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0085;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mParentFound_12((bool)0);
		UIPanel_t295209936 * L_12 = __this->get_panel_35();
		NullCheck(L_12);
		UIPanel_AddWidget_m1579877318(L_12, __this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m388028458(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIWidget::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0085:
	{
		UIPanel_t295209936 * L_13 = __this->get_panel_35();
		return L_13;
	}
}
// System.Void UIWidget::CheckLayer()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral427372784;
extern const uint32_t UIWidget_CheckLayer_m388028458_MetadataUsageId;
extern "C"  void UIWidget_CheckLayer_m388028458 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CheckLayer_m388028458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m1648550306(L_3, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = GameObject_get_layer_m1648550306(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_6)))
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m4097176146(NULL /*static, unused*/, _stringLiteral427372784, __this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_8 = __this->get_panel_35();
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = GameObject_get_layer_m1648550306(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_set_layer_m1872241535(L_7, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UIWidget::ParentHasChanged()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* UIPanel_t295209936_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_ParentHasChanged_m3250288453_MetadataUsageId;
extern "C"  void UIWidget_ParentHasChanged_m3250288453 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ParentHasChanged_m3250288453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIPanel_t295209936 * V_0 = NULL;
	{
		UIRect_ParentHasChanged_m2258951653(__this, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t1659122786 * L_2 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = UIRect_get_cachedGameObject_m418520850(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m1648550306(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t295209936_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_5 = UIPanel_Find_m1125240312(NULL /*static, unused*/, L_2, (bool)1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		UIPanel_t295209936 * L_6 = __this->get_panel_35();
		UIPanel_t295209936 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UIWidget::Awake()
extern "C"  void UIWidget_Awake_m308016390 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		((UIRect_t2503437976 *)__this)->set_mGo_7(L_0);
		bool L_1 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_38(L_1);
		return;
	}
}
// System.Void UIWidget::OnInit()
extern "C"  void UIWidget_OnInit_m3801346320 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		UIRect_OnInit_m103751600(__this, /*hidden argument*/NULL);
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)1);
		int32_t L_0 = __this->get_mWidth_24();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_1 = __this->get_mHeight_25();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_0060;
		}
	}
	{
		Transform_t1659122786 * L_2 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_localScale_m3886572677(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)(8.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		VirtActionInvoker0::Invoke(32 /* System.Void UIWidget::UpgradeFrom265() */, __this);
		Transform_t1659122786 * L_5 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m310756934(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0060:
	{
		UIRect_Update_m212013674(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::UpgradeFrom265()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpgradeFrom265_m3823217484_MetadataUsageId;
extern "C"  void UIWidget_UpgradeFrom265_m3823217484 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpgradeFrom265_m3823217484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t1659122786 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_localScale_m3886572677(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = Mathf_Abs_m4265466780(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_mWidth_24(L_4);
		float L_5 = (&V_0)->get_y_2();
		int32_t L_6 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = Mathf_Abs_m4265466780(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mHeight_25(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m1680432765(NULL /*static, unused*/, L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnStart()
extern "C"  void UIWidget_OnStart_m2327121348 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnAnchor()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_OnAnchor_m1133360565_MetadataUsageId;
extern "C"  void UIWidget_OnAnchor_m1133360565 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnAnchor_m1133360565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Transform_t1659122786 * V_4 = NULL;
	Transform_t1659122786 * V_5 = NULL;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t4282066565  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3U5BU5D_t215400611* V_8 = NULL;
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3U5BU5D_t215400611* V_10 = NULL;
	Vector3U5BU5D_t215400611* V_11 = NULL;
	Vector3U5BU5D_t215400611* V_12 = NULL;
	Vector3U5BU5D_t215400611* V_13 = NULL;
	Vector3_t4282066566  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	Vector3_t4282066566  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t4282066566  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t4282066566  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t4282066566  V_20;
	memset(&V_20, 0, sizeof(V_20));
	UIWidget_t769069560 * G_B7_0 = NULL;
	UIWidget_t769069560 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	UIWidget_t769069560 * G_B8_1 = NULL;
	{
		Transform_t1659122786 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_4 = L_0;
		Transform_t1659122786 * L_1 = V_4;
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_get_parent_m2236876972(L_1, /*hidden argument*/NULL);
		V_5 = L_2;
		Transform_t1659122786 * L_3 = V_4;
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Transform_get_localPosition_m668140784(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		Vector2_t4282066565  L_5 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_7 = L_5;
		AnchorPoint_t3581172420 * L_6 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = L_6->get_target_0();
		AnchorPoint_t3581172420 * L_8 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t3581172420 * L_11 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = L_11->get_target_0();
		AnchorPoint_t3581172420 * L_13 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = L_13->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t3581172420 * L_16 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = L_16->get_target_0();
		AnchorPoint_t3581172420 * L_18 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = L_18->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t3581172420 * L_21 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t1659122786 * L_22 = V_5;
		NullCheck(L_21);
		Vector3U5BU5D_t215400611* L_23 = AnchorPoint_GetSides_m1119345522(L_21, L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		Vector3U5BU5D_t215400611* L_24 = V_8;
		if (!L_24)
		{
			goto IL_0184;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_25 = V_8;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		float L_26 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t215400611* L_27 = V_8;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		float L_28 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t3581172420 * L_29 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_29);
		float L_30 = L_29->get_relative_1();
		float L_31 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_26, L_28, L_30, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_32 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_32);
		int32_t L_33 = L_32->get_absolute_2();
		V_0 = ((float)((float)L_31+(float)(((float)((float)L_33)))));
		Vector3U5BU5D_t215400611* L_34 = V_8;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		float L_35 = ((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t215400611* L_36 = V_8;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		float L_37 = ((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t3581172420 * L_38 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_38);
		float L_39 = L_38->get_relative_1();
		float L_40 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_41 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_absolute_2();
		V_2 = ((float)((float)L_40+(float)(((float)((float)L_42)))));
		Vector3U5BU5D_t215400611* L_43 = V_8;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 3);
		float L_44 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t215400611* L_45 = V_8;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		float L_46 = ((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t3581172420 * L_47 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_47);
		float L_48 = L_47->get_relative_1();
		float L_49 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_44, L_46, L_48, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_50 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_50);
		int32_t L_51 = L_50->get_absolute_2();
		V_1 = ((float)((float)L_49+(float)(((float)((float)L_51)))));
		Vector3U5BU5D_t215400611* L_52 = V_8;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 3);
		float L_53 = ((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t215400611* L_54 = V_8;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
		float L_55 = ((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t3581172420 * L_56 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_56);
		float L_57 = L_56->get_relative_1();
		float L_58 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_53, L_55, L_57, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_59 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_59);
		int32_t L_60 = L_59->get_absolute_2();
		V_3 = ((float)((float)L_58+(float)(((float)((float)L_60)))));
		__this->set_mIsInFront_43((bool)1);
		goto IL_020d;
	}

IL_0184:
	{
		AnchorPoint_t3581172420 * L_61 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t1659122786 * L_62 = V_5;
		Vector3_t4282066566  L_63 = UIRect_GetLocalPos_m763968249(__this, L_61, L_62, /*hidden argument*/NULL);
		V_9 = L_63;
		float L_64 = (&V_9)->get_x_1();
		AnchorPoint_t3581172420 * L_65 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_65);
		int32_t L_66 = L_65->get_absolute_2();
		V_0 = ((float)((float)L_64+(float)(((float)((float)L_66)))));
		float L_67 = (&V_9)->get_y_2();
		AnchorPoint_t3581172420 * L_68 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_68);
		int32_t L_69 = L_68->get_absolute_2();
		V_1 = ((float)((float)L_67+(float)(((float)((float)L_69)))));
		float L_70 = (&V_9)->get_x_1();
		AnchorPoint_t3581172420 * L_71 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_absolute_2();
		V_2 = ((float)((float)L_70+(float)(((float)((float)L_72)))));
		float L_73 = (&V_9)->get_y_2();
		AnchorPoint_t3581172420 * L_74 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_74);
		int32_t L_75 = L_74->get_absolute_2();
		V_3 = ((float)((float)L_73+(float)(((float)((float)L_75)))));
		bool L_76 = __this->get_hideIfOffScreen_31();
		G_B6_0 = __this;
		if (!L_76)
		{
			G_B7_0 = __this;
			goto IL_0207;
		}
	}
	{
		float L_77 = (&V_9)->get_z_3();
		G_B8_0 = ((((int32_t)((!(((float)L_77) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B8_1 = G_B6_0;
		goto IL_0208;
	}

IL_0207:
	{
		G_B8_0 = 1;
		G_B8_1 = G_B7_0;
	}

IL_0208:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mIsInFront_43((bool)G_B8_0);
	}

IL_020d:
	{
		goto IL_04d1;
	}

IL_0212:
	{
		__this->set_mIsInFront_43((bool)1);
		AnchorPoint_t3581172420 * L_78 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_78);
		Transform_t1659122786 * L_79 = L_78->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_80 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02ab;
		}
	}
	{
		AnchorPoint_t3581172420 * L_81 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t1659122786 * L_82 = V_5;
		NullCheck(L_81);
		Vector3U5BU5D_t215400611* L_83 = AnchorPoint_GetSides_m1119345522(L_81, L_82, /*hidden argument*/NULL);
		V_10 = L_83;
		Vector3U5BU5D_t215400611* L_84 = V_10;
		if (!L_84)
		{
			goto IL_0281;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_85 = V_10;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 0);
		float L_86 = ((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t215400611* L_87 = V_10;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 2);
		float L_88 = ((L_87)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t3581172420 * L_89 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_89);
		float L_90 = L_89->get_relative_1();
		float L_91 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_86, L_88, L_90, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_92 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_92);
		int32_t L_93 = L_92->get_absolute_2();
		V_0 = ((float)((float)L_91+(float)(((float)((float)L_93)))));
		goto IL_02a6;
	}

IL_0281:
	{
		AnchorPoint_t3581172420 * L_94 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t1659122786 * L_95 = V_5;
		Vector3_t4282066566  L_96 = UIRect_GetLocalPos_m763968249(__this, L_94, L_95, /*hidden argument*/NULL);
		V_17 = L_96;
		float L_97 = (&V_17)->get_x_1();
		AnchorPoint_t3581172420 * L_98 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_98);
		int32_t L_99 = L_98->get_absolute_2();
		V_0 = ((float)((float)L_97+(float)(((float)((float)L_99)))));
	}

IL_02a6:
	{
		goto IL_02c3;
	}

IL_02ab:
	{
		float L_100 = (&V_6)->get_x_1();
		float L_101 = (&V_7)->get_x_1();
		int32_t L_102 = __this->get_mWidth_24();
		V_0 = ((float)((float)L_100-(float)((float)((float)L_101*(float)(((float)((float)L_102)))))));
	}

IL_02c3:
	{
		AnchorPoint_t3581172420 * L_103 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_103);
		Transform_t1659122786 * L_104 = L_103->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_105 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_0355;
		}
	}
	{
		AnchorPoint_t3581172420 * L_106 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		Transform_t1659122786 * L_107 = V_5;
		NullCheck(L_106);
		Vector3U5BU5D_t215400611* L_108 = AnchorPoint_GetSides_m1119345522(L_106, L_107, /*hidden argument*/NULL);
		V_11 = L_108;
		Vector3U5BU5D_t215400611* L_109 = V_11;
		if (!L_109)
		{
			goto IL_032b;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_110 = V_11;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, 0);
		float L_111 = ((L_110)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t215400611* L_112 = V_11;
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 2);
		float L_113 = ((L_112)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t3581172420 * L_114 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_114);
		float L_115 = L_114->get_relative_1();
		float L_116 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_111, L_113, L_115, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_117 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_117);
		int32_t L_118 = L_117->get_absolute_2();
		V_2 = ((float)((float)L_116+(float)(((float)((float)L_118)))));
		goto IL_0350;
	}

IL_032b:
	{
		AnchorPoint_t3581172420 * L_119 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		Transform_t1659122786 * L_120 = V_5;
		Vector3_t4282066566  L_121 = UIRect_GetLocalPos_m763968249(__this, L_119, L_120, /*hidden argument*/NULL);
		V_18 = L_121;
		float L_122 = (&V_18)->get_x_1();
		AnchorPoint_t3581172420 * L_123 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_123);
		int32_t L_124 = L_123->get_absolute_2();
		V_2 = ((float)((float)L_122+(float)(((float)((float)L_124)))));
	}

IL_0350:
	{
		goto IL_0375;
	}

IL_0355:
	{
		float L_125 = (&V_6)->get_x_1();
		float L_126 = (&V_7)->get_x_1();
		int32_t L_127 = __this->get_mWidth_24();
		int32_t L_128 = __this->get_mWidth_24();
		V_2 = ((float)((float)((float)((float)L_125-(float)((float)((float)L_126*(float)(((float)((float)L_127)))))))+(float)(((float)((float)L_128)))));
	}

IL_0375:
	{
		AnchorPoint_t3581172420 * L_129 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_129);
		Transform_t1659122786 * L_130 = L_129->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_131 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_130, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0407;
		}
	}
	{
		AnchorPoint_t3581172420 * L_132 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		Transform_t1659122786 * L_133 = V_5;
		NullCheck(L_132);
		Vector3U5BU5D_t215400611* L_134 = AnchorPoint_GetSides_m1119345522(L_132, L_133, /*hidden argument*/NULL);
		V_12 = L_134;
		Vector3U5BU5D_t215400611* L_135 = V_12;
		if (!L_135)
		{
			goto IL_03dd;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_136 = V_12;
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, 3);
		float L_137 = ((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t215400611* L_138 = V_12;
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, 1);
		float L_139 = ((L_138)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t3581172420 * L_140 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_140);
		float L_141 = L_140->get_relative_1();
		float L_142 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_137, L_139, L_141, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_143 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_143);
		int32_t L_144 = L_143->get_absolute_2();
		V_1 = ((float)((float)L_142+(float)(((float)((float)L_144)))));
		goto IL_0402;
	}

IL_03dd:
	{
		AnchorPoint_t3581172420 * L_145 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		Transform_t1659122786 * L_146 = V_5;
		Vector3_t4282066566  L_147 = UIRect_GetLocalPos_m763968249(__this, L_145, L_146, /*hidden argument*/NULL);
		V_19 = L_147;
		float L_148 = (&V_19)->get_y_2();
		AnchorPoint_t3581172420 * L_149 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_149);
		int32_t L_150 = L_149->get_absolute_2();
		V_1 = ((float)((float)L_148+(float)(((float)((float)L_150)))));
	}

IL_0402:
	{
		goto IL_041f;
	}

IL_0407:
	{
		float L_151 = (&V_6)->get_y_2();
		float L_152 = (&V_7)->get_y_2();
		int32_t L_153 = __this->get_mHeight_25();
		V_1 = ((float)((float)L_151-(float)((float)((float)L_152*(float)(((float)((float)L_153)))))));
	}

IL_041f:
	{
		AnchorPoint_t3581172420 * L_154 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_154);
		Transform_t1659122786 * L_155 = L_154->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_156 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_155, /*hidden argument*/NULL);
		if (!L_156)
		{
			goto IL_04b1;
		}
	}
	{
		AnchorPoint_t3581172420 * L_157 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		Transform_t1659122786 * L_158 = V_5;
		NullCheck(L_157);
		Vector3U5BU5D_t215400611* L_159 = AnchorPoint_GetSides_m1119345522(L_157, L_158, /*hidden argument*/NULL);
		V_13 = L_159;
		Vector3U5BU5D_t215400611* L_160 = V_13;
		if (!L_160)
		{
			goto IL_0487;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_161 = V_13;
		NullCheck(L_161);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_161, 3);
		float L_162 = ((L_161)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t215400611* L_163 = V_13;
		NullCheck(L_163);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_163, 1);
		float L_164 = ((L_163)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t3581172420 * L_165 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_165);
		float L_166 = L_165->get_relative_1();
		float L_167 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_162, L_164, L_166, /*hidden argument*/NULL);
		AnchorPoint_t3581172420 * L_168 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_168);
		int32_t L_169 = L_168->get_absolute_2();
		V_3 = ((float)((float)L_167+(float)(((float)((float)L_169)))));
		goto IL_04ac;
	}

IL_0487:
	{
		AnchorPoint_t3581172420 * L_170 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		Transform_t1659122786 * L_171 = V_5;
		Vector3_t4282066566  L_172 = UIRect_GetLocalPos_m763968249(__this, L_170, L_171, /*hidden argument*/NULL);
		V_20 = L_172;
		float L_173 = (&V_20)->get_y_2();
		AnchorPoint_t3581172420 * L_174 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_174);
		int32_t L_175 = L_174->get_absolute_2();
		V_3 = ((float)((float)L_173+(float)(((float)((float)L_175)))));
	}

IL_04ac:
	{
		goto IL_04d1;
	}

IL_04b1:
	{
		float L_176 = (&V_6)->get_y_2();
		float L_177 = (&V_7)->get_y_2();
		int32_t L_178 = __this->get_mHeight_25();
		int32_t L_179 = __this->get_mHeight_25();
		V_3 = ((float)((float)((float)((float)L_176-(float)((float)((float)L_177*(float)(((float)((float)L_178)))))))+(float)(((float)((float)L_179)))));
	}

IL_04d1:
	{
		float L_180 = V_0;
		float L_181 = V_2;
		float L_182 = (&V_7)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_183 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_180, L_181, L_182, /*hidden argument*/NULL);
		float L_184 = V_1;
		float L_185 = V_3;
		float L_186 = (&V_7)->get_y_2();
		float L_187 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_184, L_185, L_186, /*hidden argument*/NULL);
		float L_188 = (&V_6)->get_z_3();
		Vector3__ctor_m2926210380((&V_14), L_183, L_187, L_188, /*hidden argument*/NULL);
		float L_189 = (&V_14)->get_x_1();
		float L_190 = bankers_roundf(L_189);
		(&V_14)->set_x_1(L_190);
		float L_191 = (&V_14)->get_y_2();
		float L_192 = bankers_roundf(L_191);
		(&V_14)->set_y_2(L_192);
		float L_193 = V_2;
		float L_194 = V_0;
		int32_t L_195 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)((float)((float)L_193-(float)L_194))+(float)(0.5f))), /*hidden argument*/NULL);
		V_15 = L_195;
		float L_196 = V_3;
		float L_197 = V_1;
		int32_t L_198 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)((float)((float)L_196-(float)L_197))+(float)(0.5f))), /*hidden argument*/NULL);
		V_16 = L_198;
		int32_t L_199 = __this->get_keepAspectRatio_32();
		if (!L_199)
		{
			goto IL_058f;
		}
	}
	{
		float L_200 = __this->get_aspectRatio_33();
		if ((((float)L_200) == ((float)(0.0f))))
		{
			goto IL_058f;
		}
	}
	{
		int32_t L_201 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_201) == ((uint32_t)2))))
		{
			goto IL_057e;
		}
	}
	{
		int32_t L_202 = V_16;
		float L_203 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_204 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_202)))*(float)L_203)), /*hidden argument*/NULL);
		V_15 = L_204;
		goto IL_058f;
	}

IL_057e:
	{
		int32_t L_205 = V_15;
		float L_206 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_207 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_205)))/(float)L_206)), /*hidden argument*/NULL);
		V_16 = L_207;
	}

IL_058f:
	{
		int32_t L_208 = V_15;
		int32_t L_209 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_208) >= ((int32_t)L_209)))
		{
			goto IL_05a4;
		}
	}
	{
		int32_t L_210 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_15 = L_210;
	}

IL_05a4:
	{
		int32_t L_211 = V_16;
		int32_t L_212 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_211) >= ((int32_t)L_212)))
		{
			goto IL_05b9;
		}
	}
	{
		int32_t L_213 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_16 = L_213;
	}

IL_05b9:
	{
		Vector3_t4282066566  L_214 = V_6;
		Vector3_t4282066566  L_215 = V_14;
		Vector3_t4282066566  L_216 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_214, L_215, /*hidden argument*/NULL);
		float L_217 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_216, /*hidden argument*/NULL);
		if ((!(((float)L_217) > ((float)(0.001f)))))
		{
			goto IL_05f0;
		}
	}
	{
		Transform_t1659122786 * L_218 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_219 = V_14;
		NullCheck(L_218);
		Transform_set_localPosition_m3504330903(L_218, L_219, /*hidden argument*/NULL);
		bool L_220 = __this->get_mIsInFront_43();
		if (!L_220)
		{
			goto IL_05f0;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_05f0:
	{
		int32_t L_221 = __this->get_mWidth_24();
		int32_t L_222 = V_15;
		if ((!(((uint32_t)L_221) == ((uint32_t)L_222))))
		{
			goto IL_060a;
		}
	}
	{
		int32_t L_223 = __this->get_mHeight_25();
		int32_t L_224 = V_16;
		if ((((int32_t)L_223) == ((int32_t)L_224)))
		{
			goto IL_063d;
		}
	}

IL_060a:
	{
		int32_t L_225 = V_15;
		__this->set_mWidth_24(L_225);
		int32_t L_226 = V_16;
		__this->set_mHeight_25(L_226);
		bool L_227 = __this->get_mIsInFront_43();
		if (!L_227)
		{
			goto IL_062c;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_062c:
	{
		bool L_228 = __this->get_autoResizeBoxCollider_30();
		if (!L_228)
		{
			goto IL_063d;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_063d:
	{
		return;
	}
}
// System.Void UIWidget::OnUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_OnUpdate_m3427137225_MetadataUsageId;
extern "C"  void UIWidget_OnUpdate_m3427137225 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnUpdate_m3427137225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UIWidget::OnApplicationPause(System.Boolean)
extern "C"  void UIWidget_OnApplicationPause_m307910013 (UIWidget_t769069560 * __this, bool ___paused0, const MethodInfo* method)
{
	{
		bool L_0 = ___paused0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_000c:
	{
		return;
	}
}
// System.Void UIWidget::OnDisable()
extern "C"  void UIWidget_OnDisable_m625553034 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		UIRect_OnDisable_m2902457322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnDestroy()
extern "C"  void UIWidget_OnDestroy_m2803085084 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern "C"  bool UIWidget_UpdateVisibility_m143185718 (UIWidget_t769069560 * __this, bool ___visibleByAlpha0, bool ___visibleByPanel1, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_41();
		bool L_1 = ___visibleByAlpha0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		bool L_2 = __this->get_mIsVisibleByPanel_42();
		bool L_3 = ___visibleByPanel1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002f;
		}
	}

IL_0018:
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		bool L_4 = ___visibleByAlpha0;
		__this->set_mIsVisibleByAlpha_41(L_4);
		bool L_5 = ___visibleByPanel1;
		__this->set_mIsVisibleByPanel_42(L_5);
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean UIWidget::UpdateTransform(System.Int32)
extern "C"  bool UIWidget_UpdateTransform_m2760912577 (UIWidget_t769069560 * __this, int32_t ___frame0, const MethodInfo* method)
{
	Transform_t1659122786 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t4282066565  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t G_B13_0 = 0;
	{
		Transform_t1659122786 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_38(L_1);
		bool L_2 = __this->get_mMoved_45();
		if (!L_2)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_mMoved_45((bool)1);
		__this->set_mMatrixFrame_49((-1));
		Transform_t1659122786 * L_3 = V_0;
		NullCheck(L_3);
		Transform_set_hasChanged_m1599503109(L_3, (bool)0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_1();
		int32_t L_6 = __this->get_mWidth_24();
		V_2 = ((float)((float)((-L_5))*(float)(((float)((float)L_6)))));
		float L_7 = (&V_1)->get_y_2();
		int32_t L_8 = __this->get_mHeight_25();
		V_3 = ((float)((float)((-L_7))*(float)(((float)((float)L_8)))));
		float L_9 = V_2;
		int32_t L_10 = __this->get_mWidth_24();
		V_4 = ((float)((float)L_9+(float)(((float)((float)L_10)))));
		float L_11 = V_3;
		int32_t L_12 = __this->get_mHeight_25();
		V_5 = ((float)((float)L_11+(float)(((float)((float)L_12)))));
		UIPanel_t295209936 * L_13 = __this->get_panel_35();
		NullCheck(L_13);
		Matrix4x4_t1651859333 * L_14 = L_13->get_address_of_worldToLocal_35();
		Transform_t1659122786 * L_15 = V_0;
		float L_16 = V_2;
		float L_17 = V_3;
		NullCheck(L_15);
		Vector3_t4282066566  L_18 = Transform_TransformPoint_m3094713172(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_14, L_18, /*hidden argument*/NULL);
		__this->set_mOldV0_50(L_19);
		UIPanel_t295209936 * L_20 = __this->get_panel_35();
		NullCheck(L_20);
		Matrix4x4_t1651859333 * L_21 = L_20->get_address_of_worldToLocal_35();
		Transform_t1659122786 * L_22 = V_0;
		float L_23 = V_4;
		float L_24 = V_5;
		NullCheck(L_22);
		Vector3_t4282066566  L_25 = Transform_TransformPoint_m3094713172(L_22, L_23, L_24, (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_21, L_25, /*hidden argument*/NULL);
		__this->set_mOldV1_51(L_26);
		goto IL_01bc;
	}

IL_00be:
	{
		UIPanel_t295209936 * L_27 = __this->get_panel_35();
		NullCheck(L_27);
		bool L_28 = L_27->get_widgetsAreStatic_26();
		if (L_28)
		{
			goto IL_01bc;
		}
	}
	{
		Transform_t1659122786 * L_29 = V_0;
		NullCheck(L_29);
		bool L_30 = Transform_get_hasChanged_m248005108(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_01bc;
		}
	}
	{
		__this->set_mMatrixFrame_49((-1));
		Transform_t1659122786 * L_31 = V_0;
		NullCheck(L_31);
		Transform_set_hasChanged_m1599503109(L_31, (bool)0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_32 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = (&V_6)->get_x_1();
		int32_t L_34 = __this->get_mWidth_24();
		V_7 = ((float)((float)((-L_33))*(float)(((float)((float)L_34)))));
		float L_35 = (&V_6)->get_y_2();
		int32_t L_36 = __this->get_mHeight_25();
		V_8 = ((float)((float)((-L_35))*(float)(((float)((float)L_36)))));
		float L_37 = V_7;
		int32_t L_38 = __this->get_mWidth_24();
		V_9 = ((float)((float)L_37+(float)(((float)((float)L_38)))));
		float L_39 = V_8;
		int32_t L_40 = __this->get_mHeight_25();
		V_10 = ((float)((float)L_39+(float)(((float)((float)L_40)))));
		UIPanel_t295209936 * L_41 = __this->get_panel_35();
		NullCheck(L_41);
		Matrix4x4_t1651859333 * L_42 = L_41->get_address_of_worldToLocal_35();
		Transform_t1659122786 * L_43 = V_0;
		float L_44 = V_7;
		float L_45 = V_8;
		NullCheck(L_43);
		Vector3_t4282066566  L_46 = Transform_TransformPoint_m3094713172(L_43, L_44, L_45, (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_47 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_42, L_46, /*hidden argument*/NULL);
		V_11 = L_47;
		UIPanel_t295209936 * L_48 = __this->get_panel_35();
		NullCheck(L_48);
		Matrix4x4_t1651859333 * L_49 = L_48->get_address_of_worldToLocal_35();
		Transform_t1659122786 * L_50 = V_0;
		float L_51 = V_9;
		float L_52 = V_10;
		NullCheck(L_50);
		Vector3_t4282066566  L_53 = Transform_TransformPoint_m3094713172(L_50, L_51, L_52, (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_54 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_49, L_53, /*hidden argument*/NULL);
		V_12 = L_54;
		Vector3_t4282066566  L_55 = __this->get_mOldV0_50();
		Vector3_t4282066566  L_56 = V_11;
		Vector3_t4282066566  L_57 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if ((((float)L_58) > ((float)(1.0E-06f))))
		{
			goto IL_01a5;
		}
	}
	{
		Vector3_t4282066566  L_59 = __this->get_mOldV1_51();
		Vector3_t4282066566  L_60 = V_12;
		Vector3_t4282066566  L_61 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		float L_62 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if ((!(((float)L_62) > ((float)(1.0E-06f)))))
		{
			goto IL_01bc;
		}
	}

IL_01a5:
	{
		__this->set_mMoved_45((bool)1);
		Vector3_t4282066566  L_63 = V_11;
		__this->set_mOldV0_50(L_63);
		Vector3_t4282066566  L_64 = V_12;
		__this->set_mOldV1_51(L_64);
	}

IL_01bc:
	{
		bool L_65 = __this->get_mMoved_45();
		if (!L_65)
		{
			goto IL_01dd;
		}
	}
	{
		OnDimensionsChanged_t3695058769 * L_66 = __this->get_onChange_27();
		if (!L_66)
		{
			goto IL_01dd;
		}
	}
	{
		OnDimensionsChanged_t3695058769 * L_67 = __this->get_onChange_27();
		NullCheck(L_67);
		OnDimensionsChanged_Invoke_m2470653394(L_67, /*hidden argument*/NULL);
	}

IL_01dd:
	{
		bool L_68 = __this->get_mMoved_45();
		if (L_68)
		{
			goto IL_01f0;
		}
	}
	{
		bool L_69 = ((UIRect_t2503437976 *)__this)->get_mChanged_10();
		G_B13_0 = ((int32_t)(L_69));
		goto IL_01f1;
	}

IL_01f0:
	{
		G_B13_0 = 1;
	}

IL_01f1:
	{
		return (bool)G_B13_0;
	}
}
// System.Boolean UIWidget::UpdateGeometry(System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpdateGeometry_m76295649_MetadataUsageId;
extern "C"  bool UIWidget_UpdateGeometry_m76295649 (UIWidget_t769069560 * __this, int32_t ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateGeometry_m76295649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		int32_t L_0 = ___frame0;
		float L_1 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIWidget::CalculateFinalAlpha(System.Int32) */, __this, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_mIsVisibleByAlpha_41();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		float L_3 = __this->get_mLastAlpha_44();
		float L_4 = V_0;
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_0026:
	{
		float L_5 = V_0;
		__this->set_mLastAlpha_44(L_5);
		bool L_6 = ((UIRect_t2503437976 *)__this)->get_mChanged_10();
		if (!L_6)
		{
			goto IL_015c;
		}
	}
	{
		bool L_7 = __this->get_mIsVisibleByAlpha_41();
		if (!L_7)
		{
			goto IL_0121;
		}
	}
	{
		float L_8 = V_0;
		if ((!(((float)L_8) > ((float)(0.001f)))))
		{
			goto IL_0121;
		}
	}
	{
		Shader_t3191267369 * L_9 = VirtFuncInvoker0< Shader_t3191267369 * >::Invoke(28 /* UnityEngine.Shader UIWidget::get_shader() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0121;
		}
	}
	{
		UIGeometry_t3586695974 * L_11 = __this->get_geometry_36();
		NullCheck(L_11);
		bool L_12 = UIGeometry_get_hasVertices_m167811977(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		bool L_13 = __this->get_fillGeometry_37();
		if (!L_13)
		{
			goto IL_00a8;
		}
	}
	{
		UIGeometry_t3586695974 * L_14 = __this->get_geometry_36();
		NullCheck(L_14);
		UIGeometry_Clear_m844487776(L_14, /*hidden argument*/NULL);
		UIGeometry_t3586695974 * L_15 = __this->get_geometry_36();
		NullCheck(L_15);
		BetterList_1_t1484067282 * L_16 = L_15->get_verts_0();
		UIGeometry_t3586695974 * L_17 = __this->get_geometry_36();
		NullCheck(L_17);
		BetterList_1_t1484067281 * L_18 = L_17->get_uvs_1();
		UIGeometry_t3586695974 * L_19 = __this->get_geometry_36();
		NullCheck(L_19);
		BetterList_1_t2095821700 * L_20 = L_19->get_cols_2();
		VirtActionInvoker3< BetterList_1_t1484067282 *, BetterList_1_t1484067281 *, BetterList_1_t2095821700 * >::Invoke(38 /* System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>) */, __this, L_16, L_18, L_20);
	}

IL_00a8:
	{
		UIGeometry_t3586695974 * L_21 = __this->get_geometry_36();
		NullCheck(L_21);
		bool L_22 = UIGeometry_get_hasVertices_m167811977(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_23 = __this->get_mMatrixFrame_49();
		int32_t L_24 = ___frame0;
		if ((((int32_t)L_23) == ((int32_t)L_24)))
		{
			goto IL_00ec;
		}
	}
	{
		UIPanel_t295209936 * L_25 = __this->get_panel_35();
		NullCheck(L_25);
		Matrix4x4_t1651859333  L_26 = L_25->get_worldToLocal_35();
		Transform_t1659122786 * L_27 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Matrix4x4_t1651859333  L_28 = Transform_get_localToWorldMatrix_m3571020210(L_27, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_29 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_40(L_29);
		int32_t L_30 = ___frame0;
		__this->set_mMatrixFrame_49(L_30);
	}

IL_00ec:
	{
		UIGeometry_t3586695974 * L_31 = __this->get_geometry_36();
		Matrix4x4_t1651859333  L_32 = __this->get_mLocalToPanel_40();
		UIPanel_t295209936 * L_33 = __this->get_panel_35();
		NullCheck(L_33);
		bool L_34 = L_33->get_generateNormals_25();
		NullCheck(L_31);
		UIGeometry_ApplyTransform_m336861776(L_31, L_32, L_34, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_0118:
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		bool L_35 = V_1;
		return L_35;
	}

IL_0121:
	{
		UIGeometry_t3586695974 * L_36 = __this->get_geometry_36();
		NullCheck(L_36);
		bool L_37 = UIGeometry_get_hasVertices_m167811977(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0157;
		}
	}
	{
		bool L_38 = __this->get_fillGeometry_37();
		if (!L_38)
		{
			goto IL_0147;
		}
	}
	{
		UIGeometry_t3586695974 * L_39 = __this->get_geometry_36();
		NullCheck(L_39);
		UIGeometry_Clear_m844487776(L_39, /*hidden argument*/NULL);
	}

IL_0147:
	{
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_0157:
	{
		goto IL_01d7;
	}

IL_015c:
	{
		bool L_40 = __this->get_mMoved_45();
		if (!L_40)
		{
			goto IL_01d7;
		}
	}
	{
		UIGeometry_t3586695974 * L_41 = __this->get_geometry_36();
		NullCheck(L_41);
		bool L_42 = UIGeometry_get_hasVertices_m167811977(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01d7;
		}
	}
	{
		int32_t L_43 = __this->get_mMatrixFrame_49();
		int32_t L_44 = ___frame0;
		if ((((int32_t)L_43) == ((int32_t)L_44)))
		{
			goto IL_01ab;
		}
	}
	{
		UIPanel_t295209936 * L_45 = __this->get_panel_35();
		NullCheck(L_45);
		Matrix4x4_t1651859333  L_46 = L_45->get_worldToLocal_35();
		Transform_t1659122786 * L_47 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Matrix4x4_t1651859333  L_48 = Transform_get_localToWorldMatrix_m3571020210(L_47, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_49 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_40(L_49);
		int32_t L_50 = ___frame0;
		__this->set_mMatrixFrame_49(L_50);
	}

IL_01ab:
	{
		UIGeometry_t3586695974 * L_51 = __this->get_geometry_36();
		Matrix4x4_t1651859333  L_52 = __this->get_mLocalToPanel_40();
		UIPanel_t295209936 * L_53 = __this->get_panel_35();
		NullCheck(L_53);
		bool L_54 = L_53->get_generateNormals_25();
		NullCheck(L_51);
		UIGeometry_ApplyTransform_m336861776(L_51, L_52, L_54, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_01d7:
	{
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)0;
	}
}
// System.Void UIWidget::WriteToBuffers(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector4>)
extern "C"  void UIWidget_WriteToBuffers_m4077282832 (UIWidget_t769069560 * __this, BetterList_1_t1484067282 * ___v0, BetterList_1_t1484067281 * ___u1, BetterList_1_t2095821700 * ___c2, BetterList_1_t1484067282 * ___n3, BetterList_1_t1484067283 * ___t4, const MethodInfo* method)
{
	{
		UIGeometry_t3586695974 * L_0 = __this->get_geometry_36();
		BetterList_1_t1484067282 * L_1 = ___v0;
		BetterList_1_t1484067281 * L_2 = ___u1;
		BetterList_1_t2095821700 * L_3 = ___c2;
		BetterList_1_t1484067282 * L_4 = ___n3;
		BetterList_1_t1484067283 * L_5 = ___t4;
		NullCheck(L_0);
		UIGeometry_WriteToBuffers_m3819043362(L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::MakePixelPerfect()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_MakePixelPerfect_m1839593398_MetadataUsageId;
extern "C"  void UIWidget_MakePixelPerfect_m1839593398 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MakePixelPerfect_m1839593398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t1659122786 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_localPosition_m668140784(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(L_2);
		(&V_0)->set_z_3(L_3);
		float L_4 = (&V_0)->get_x_1();
		float L_5 = bankers_roundf(L_4);
		(&V_0)->set_x_1(L_5);
		float L_6 = (&V_0)->get_y_2();
		float L_7 = bankers_roundf(L_6);
		(&V_0)->set_y_2(L_7);
		Transform_t1659122786 * L_8 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_localPosition_m3504330903(L_8, L_9, /*hidden argument*/NULL);
		Transform_t1659122786 * L_10 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_localScale_m3886572677(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		Transform_t1659122786 * L_12 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		float L_13 = (&V_1)->get_x_1();
		float L_14 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = (&V_1)->get_y_2();
		float L_16 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Vector3_t4282066566  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, L_14, L_16, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m310756934(L_12, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UIWidget::get_minWidth()
extern "C"  int32_t UIWidget_get_minWidth_m3490344780 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		return 2;
	}
}
// System.Int32 UIWidget::get_minHeight()
extern "C"  int32_t UIWidget_get_minHeight_m1330320131 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		return 2;
	}
}
// UnityEngine.Vector4 UIWidget::get_border()
extern "C"  Vector4_t4282066567  UIWidget_get_border_m4155026927 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UIWidget::set_border(UnityEngine.Vector4)
extern "C"  void UIWidget_set_border_m2644475438 (UIWidget_t769069560 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIWidget_OnFill_m620094002 (UIWidget_t769069560 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIWidget/HitCheck::.ctor(System.Object,System.IntPtr)
extern "C"  void HitCheck__ctor_m17538403 (HitCheck_t3889696652 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UIWidget/HitCheck::Invoke(UnityEngine.Vector3)
extern "C"  bool HitCheck_Invoke_m3382841584 (HitCheck_t3889696652 * __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		HitCheck_Invoke_m3382841584((HitCheck_t3889696652 *)__this->get_prev_9(),___worldPos0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___worldPos0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___worldPos0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper_HitCheck_t3889696652 (HitCheck_t3889696652 * __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(Vector3_t4282066566_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___worldPos0' to native representation
	Vector3_t4282066566_marshaled_pinvoke ____worldPos0_marshaled = { };
	Vector3_t4282066566_marshal_pinvoke(___worldPos0, ____worldPos0_marshaled);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____worldPos0_marshaled);

	// Marshaling cleanup of parameter '___worldPos0' native representation
	Vector3_t4282066566_marshal_pinvoke_cleanup(____worldPos0_marshaled);

	return returnValue;
}
// System.IAsyncResult UIWidget/HitCheck::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t HitCheck_BeginInvoke_m1200520187_MetadataUsageId;
extern "C"  Il2CppObject * HitCheck_BeginInvoke_m1200520187 (HitCheck_t3889696652 * __this, Vector3_t4282066566  ___worldPos0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitCheck_BeginInvoke_m1200520187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___worldPos0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UIWidget/HitCheck::EndInvoke(System.IAsyncResult)
extern "C"  bool HitCheck_EndInvoke_m1381750975 (HitCheck_t3889696652 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void UIWidget/OnDimensionsChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDimensionsChanged__ctor_m148577144 (OnDimensionsChanged_t3695058769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnDimensionsChanged::Invoke()
extern "C"  void OnDimensionsChanged_Invoke_m2470653394 (OnDimensionsChanged_t3695058769 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDimensionsChanged_Invoke_m2470653394((OnDimensionsChanged_t3695058769 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnDimensionsChanged_t3695058769 (OnDimensionsChanged_t3695058769 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UIWidget/OnDimensionsChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDimensionsChanged_BeginInvoke_m602434801 (OnDimensionsChanged_t3695058769 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UIWidget/OnDimensionsChanged::EndInvoke(System.IAsyncResult)
extern "C"  void OnDimensionsChanged_EndInvoke_m1144457608 (OnDimensionsChanged_t3695058769 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIWidget/OnPostFillCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostFillCallback__ctor_m2088632341 (OnPostFillCallback_t3030491454 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnPostFillCallback::Invoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void OnPostFillCallback_Invoke_m320025295 (OnPostFillCallback_t3030491454 * __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t1484067282 * ___verts2, BetterList_1_t1484067281 * ___uvs3, BetterList_1_t2095821700 * ___cols4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnPostFillCallback_Invoke_m320025295((OnPostFillCallback_t3030491454 *)__this->get_prev_9(),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t1484067282 * ___verts2, BetterList_1_t1484067281 * ___uvs3, BetterList_1_t2095821700 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t1484067282 * ___verts2, BetterList_1_t1484067281 * ___uvs3, BetterList_1_t2095821700 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___bufferOffset1, BetterList_1_t1484067282 * ___verts2, BetterList_1_t1484067281 * ___uvs3, BetterList_1_t2095821700 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UIWidget/OnPostFillCallback::BeginInvoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t OnPostFillCallback_BeginInvoke_m387558244_MetadataUsageId;
extern "C"  Il2CppObject * OnPostFillCallback_BeginInvoke_m387558244 (OnPostFillCallback_t3030491454 * __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t1484067282 * ___verts2, BetterList_1_t1484067281 * ___uvs3, BetterList_1_t2095821700 * ___cols4, AsyncCallback_t1369114871 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnPostFillCallback_BeginInvoke_m387558244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = ___widget0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___bufferOffset1);
	__d_args[2] = ___verts2;
	__d_args[3] = ___uvs3;
	__d_args[4] = ___cols4;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Void UIWidget/OnPostFillCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostFillCallback_EndInvoke_m2996225189 (OnPostFillCallback_t3030491454 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIWidgetContainer::.ctor()
extern "C"  void UIWidgetContainer__ctor_m2037457442 (UIWidgetContainer_t1520767337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::.ctor()
extern Il2CppClass* List_1_t3027308338_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1640872948_MethodInfo_var;
extern const uint32_t UIWrapContent__ctor_m282366512_MetadataUsageId;
extern "C"  void UIWrapContent__ctor_m282366512 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent__ctor_m282366512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_itemSize_2(((int32_t)100));
		__this->set_cullContent_3((bool)1);
		__this->set_mFirstTime_11((bool)1);
		List_1_t3027308338 * L_0 = (List_1_t3027308338 *)il2cpp_codegen_object_new(List_1_t3027308338_il2cpp_TypeInfo_var);
		List_1__ctor_m1640872948(L_0, /*hidden argument*/List_1__ctor_m1640872948_MethodInfo_var);
		__this->set_mChildren_12(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* OnClippingMoved_t1586118419_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var;
extern const uint32_t UIWrapContent_Start_m3524471600_MetadataUsageId;
extern "C"  void UIWrapContent_Start_m3524471600 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_Start_m3524471600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UIWrapContent::SortBasedOnScrollMovement() */, __this);
		VirtActionInvoker0::Invoke(9 /* System.Void UIWrapContent::WrapContent() */, __this);
		UIScrollView_t2113479878 * L_0 = __this->get_mScroll_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		UIScrollView_t2113479878 * L_2 = __this->get_mScroll_9();
		NullCheck(L_2);
		UIPanel_t295209936 * L_3 = Component_GetComponent_TisUIPanel_t295209936_m1836641897(L_2, /*hidden argument*/Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 5));
		OnClippingMoved_t1586118419 * L_5 = (OnClippingMoved_t1586118419 *)il2cpp_codegen_object_new(OnClippingMoved_t1586118419_il2cpp_TypeInfo_var);
		OnClippingMoved__ctor_m3635995498(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_onClipMove_37(L_5);
	}

IL_003a:
	{
		__this->set_mFirstTime_11((bool)0);
		return;
	}
}
// System.Void UIWrapContent::OnMove(UIPanel)
extern "C"  void UIWrapContent_OnMove_m3675848574 (UIWrapContent_t33025435 * __this, UIPanel_t295209936 * ___panel0, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void UIWrapContent::WrapContent() */, __this);
		return;
	}
}
// System.Void UIWrapContent::SortBasedOnScrollMovement()
extern Il2CppClass* Comparison_1_t375483973_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3341973535_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1335233764_MethodInfo_var;
extern const MethodInfo* UIGrid_SortHorizontal_m646783841_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1879963539_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2423802008_MethodInfo_var;
extern const MethodInfo* UIGrid_SortVertical_m3217656271_MethodInfo_var;
extern const uint32_t UIWrapContent_SortBasedOnScrollMovement_m695931902_MetadataUsageId;
extern "C"  void UIWrapContent_SortBasedOnScrollMovement_m695931902 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortBasedOnScrollMovement_m695931902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m1949504758(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t3027308338 * L_1 = __this->get_mChildren_12();
		NullCheck(L_1);
		List_1_Clear_m3341973535(L_1, /*hidden argument*/List_1_Clear_m3341973535_MethodInfo_var);
		V_0 = 0;
		goto IL_0039;
	}

IL_001e:
	{
		List_1_t3027308338 * L_2 = __this->get_mChildren_12();
		Transform_t1659122786 * L_3 = __this->get_mTrans_7();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Add_m1335233764(L_2, L_5, /*hidden argument*/List_1_Add_m1335233764_MethodInfo_var);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_7 = V_0;
		Transform_t1659122786 * L_8 = __this->get_mTrans_7();
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m2107810675(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_001e;
		}
	}
	{
		bool L_10 = __this->get_mHorizontal_10();
		if (!L_10)
		{
			goto IL_0071;
		}
	}
	{
		List_1_t3027308338 * L_11 = __this->get_mChildren_12();
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)UIGrid_SortHorizontal_m646783841_MethodInfo_var);
		Comparison_1_t375483973 * L_13 = (Comparison_1_t375483973 *)il2cpp_codegen_object_new(Comparison_1_t375483973_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_13, NULL, L_12, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_11);
		List_1_Sort_m2423802008(L_11, L_13, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		goto IL_0088;
	}

IL_0071:
	{
		List_1_t3027308338 * L_14 = __this->get_mChildren_12();
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)UIGrid_SortVertical_m3217656271_MethodInfo_var);
		Comparison_1_t375483973 * L_16 = (Comparison_1_t375483973 *)il2cpp_codegen_object_new(Comparison_1_t375483973_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_16, NULL, L_15, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_14);
		List_1_Sort_m2423802008(L_14, L_16, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
	}

IL_0088:
	{
		VirtActionInvoker0::Invoke(8 /* System.Void UIWrapContent::ResetChildPositions() */, __this);
		return;
	}
}
// System.Void UIWrapContent::SortAlphabetically()
extern Il2CppClass* Comparison_1_t375483973_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3341973535_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1335233764_MethodInfo_var;
extern const MethodInfo* UIGrid_SortByName_m3179856323_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1879963539_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2423802008_MethodInfo_var;
extern const uint32_t UIWrapContent_SortAlphabetically_m1473128343_MetadataUsageId;
extern "C"  void UIWrapContent_SortAlphabetically_m1473128343 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortAlphabetically_m1473128343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m1949504758(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t3027308338 * L_1 = __this->get_mChildren_12();
		NullCheck(L_1);
		List_1_Clear_m3341973535(L_1, /*hidden argument*/List_1_Clear_m3341973535_MethodInfo_var);
		V_0 = 0;
		goto IL_0039;
	}

IL_001e:
	{
		List_1_t3027308338 * L_2 = __this->get_mChildren_12();
		Transform_t1659122786 * L_3 = __this->get_mTrans_7();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Add_m1335233764(L_2, L_5, /*hidden argument*/List_1_Add_m1335233764_MethodInfo_var);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_7 = V_0;
		Transform_t1659122786 * L_8 = __this->get_mTrans_7();
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m2107810675(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t3027308338 * L_10 = __this->get_mChildren_12();
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)UIGrid_SortByName_m3179856323_MethodInfo_var);
		Comparison_1_t375483973 * L_12 = (Comparison_1_t375483973 *)il2cpp_codegen_object_new(Comparison_1_t375483973_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_12, NULL, L_11, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_10);
		List_1_Sort_m2423802008(L_10, L_12, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		VirtActionInvoker0::Invoke(8 /* System.Void UIWrapContent::ResetChildPositions() */, __this);
		return;
	}
}
// System.Boolean UIWrapContent::CacheScrollView()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIScrollView_t2113479878_m1783064831_MethodInfo_var;
extern const uint32_t UIWrapContent_CacheScrollView_m1949504758_MetadataUsageId;
extern "C"  bool UIWrapContent_CacheScrollView_m1949504758 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_CacheScrollView_m1949504758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_7(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_2 = NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243(NULL /*static, unused*/, L_1, /*hidden argument*/NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243_MethodInfo_var);
		__this->set_mPanel_8(L_2);
		UIPanel_t295209936 * L_3 = __this->get_mPanel_8();
		NullCheck(L_3);
		UIScrollView_t2113479878 * L_4 = Component_GetComponent_TisUIScrollView_t2113479878_m1783064831(L_3, /*hidden argument*/Component_GetComponent_TisUIScrollView_t2113479878_m1783064831_MethodInfo_var);
		__this->set_mScroll_9(L_4);
		UIScrollView_t2113479878 * L_5 = __this->get_mScroll_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		UIScrollView_t2113479878 * L_7 = __this->get_mScroll_9();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_movement_3();
		if (L_8)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_mHorizontal_10((bool)1);
		goto IL_007c;
	}

IL_005d:
	{
		UIScrollView_t2113479878 * L_9 = __this->get_mScroll_9();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_movement_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		__this->set_mHorizontal_10((bool)0);
		goto IL_007c;
	}

IL_007a:
	{
		return (bool)0;
	}

IL_007c:
	{
		return (bool)1;
	}
}
// System.Void UIWrapContent::ResetChildPositions()
extern const MethodInfo* List_1_get_Count_m261083306_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4268610603_MethodInfo_var;
extern const uint32_t UIWrapContent_ResetChildPositions_m1267405611_MetadataUsageId;
extern "C"  void UIWrapContent_ResetChildPositions_m1267405611 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_ResetChildPositions_m1267405611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Transform_t1659122786 * V_2 = NULL;
	Transform_t1659122786 * G_B3_0 = NULL;
	Transform_t1659122786 * G_B2_0 = NULL;
	Vector3_t4282066566  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Transform_t1659122786 * G_B4_1 = NULL;
	{
		V_0 = 0;
		List_1_t3027308338 * L_0 = __this->get_mChildren_12();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m261083306(L_0, /*hidden argument*/List_1_get_Count_m261083306_MethodInfo_var);
		V_1 = L_1;
		goto IL_0073;
	}

IL_0013:
	{
		List_1_t3027308338 * L_2 = __this->get_mChildren_12();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t1659122786 * L_4 = List_1_get_Item_m4268610603(L_2, L_3, /*hidden argument*/List_1_get_Item_m4268610603_MethodInfo_var);
		V_2 = L_4;
		Transform_t1659122786 * L_5 = V_2;
		bool L_6 = __this->get_mHorizontal_10();
		G_B2_0 = L_5;
		if (!L_6)
		{
			G_B3_0 = L_5;
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_itemSize_2();
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, (((float)((float)((int32_t)((int32_t)L_7*(int32_t)L_8))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_9;
		G_B4_1 = G_B2_0;
		goto IL_0062;
	}

IL_0049:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_itemSize_2();
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, (0.0f), (((float)((float)((int32_t)((int32_t)((-L_10))*(int32_t)L_11))))), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0062:
	{
		NullCheck(G_B4_1);
		Transform_set_localPosition_m3504330903(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = V_2;
		int32_t L_14 = V_0;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_13, L_14);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UIWrapContent::WrapContent()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m261083306_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4268610603_MethodInfo_var;
extern const uint32_t UIWrapContent_WrapContent_m2368030365_MetadataUsageId;
extern "C"  void UIWrapContent_WrapContent_m2368030365 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_WrapContent_m2368030365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3U5BU5D_t215400611* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	bool V_5 = false;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Transform_t1659122786 * V_11 = NULL;
	float V_12 = 0.0f;
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	int32_t V_14 = 0;
	Vector3_t4282066566  V_15;
	memset(&V_15, 0, sizeof(V_15));
	int32_t V_16 = 0;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	Transform_t1659122786 * V_21 = NULL;
	float V_22 = 0.0f;
	Vector3_t4282066566  V_23;
	memset(&V_23, 0, sizeof(V_23));
	int32_t V_24 = 0;
	Vector3_t4282066566  V_25;
	memset(&V_25, 0, sizeof(V_25));
	int32_t V_26 = 0;
	Vector3_t4282066566  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector2_t4282066565  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t4282066566  V_29;
	memset(&V_29, 0, sizeof(V_29));
	Vector3_t4282066566  V_30;
	memset(&V_30, 0, sizeof(V_30));
	Vector2_t4282066565  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t4282066566  V_32;
	memset(&V_32, 0, sizeof(V_32));
	GameObject_t3674682005 * G_B25_0 = NULL;
	GameObject_t3674682005 * G_B24_0 = NULL;
	int32_t G_B26_0 = 0;
	GameObject_t3674682005 * G_B26_1 = NULL;
	GameObject_t3674682005 * G_B51_0 = NULL;
	GameObject_t3674682005 * G_B50_0 = NULL;
	int32_t G_B52_0 = 0;
	GameObject_t3674682005 * G_B52_1 = NULL;
	{
		int32_t L_0 = __this->get_itemSize_2();
		List_1_t3027308338 * L_1 = __this->get_mChildren_12();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m261083306(L_1, /*hidden argument*/List_1_get_Count_m261083306_MethodInfo_var);
		V_0 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_0*(int32_t)L_2)))))*(float)(0.5f)));
		UIPanel_t295209936 * L_3 = __this->get_mPanel_8();
		NullCheck(L_3);
		Vector3U5BU5D_t215400611* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t215400611* >::Invoke(11 /* UnityEngine.Vector3[] UIPanel::get_worldCorners() */, L_3);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0058;
	}

IL_002d:
	{
		Vector3U5BU5D_t215400611* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		V_3 = (*(Vector3_t4282066566 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		Transform_t1659122786 * L_7 = __this->get_mTrans_7();
		Vector3_t4282066566  L_8 = V_3;
		NullCheck(L_7);
		Vector3_t4282066566  L_9 = Transform_InverseTransformPoint_m1626812000(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3U5BU5D_t215400611* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Vector3_t4282066566  L_12 = V_3;
		(*(Vector3_t4282066566 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))) = L_12;
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) < ((int32_t)4)))
		{
			goto IL_002d;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_15 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		Vector3U5BU5D_t215400611* L_16 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Vector3_t4282066566  L_17 = Vector3_Lerp_m650470329(NULL /*static, unused*/, (*(Vector3_t4282066566 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t4282066566 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		V_4 = L_17;
		V_5 = (bool)1;
		float L_18 = V_0;
		V_6 = ((float)((float)L_18*(float)(2.0f)));
		bool L_19 = __this->get_mHorizontal_10();
		if (!L_19)
		{
			goto IL_02bd;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_20 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		float L_21 = ((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		int32_t L_22 = __this->get_itemSize_2();
		V_7 = ((float)((float)L_21-(float)(((float)((float)L_22)))));
		Vector3U5BU5D_t215400611* L_23 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		float L_24 = ((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		int32_t L_25 = __this->get_itemSize_2();
		V_8 = ((float)((float)L_24+(float)(((float)((float)L_25)))));
		V_9 = 0;
		List_1_t3027308338 * L_26 = __this->get_mChildren_12();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m261083306(L_26, /*hidden argument*/List_1_get_Count_m261083306_MethodInfo_var);
		V_10 = L_27;
		goto IL_02af;
	}

IL_00db:
	{
		List_1_t3027308338 * L_28 = __this->get_mChildren_12();
		int32_t L_29 = V_9;
		NullCheck(L_28);
		Transform_t1659122786 * L_30 = List_1_get_Item_m4268610603(L_28, L_29, /*hidden argument*/List_1_get_Item_m4268610603_MethodInfo_var);
		V_11 = L_30;
		Transform_t1659122786 * L_31 = V_11;
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_localPosition_m668140784(L_31, /*hidden argument*/NULL);
		V_27 = L_32;
		float L_33 = (&V_27)->get_x_1();
		float L_34 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_33-(float)L_34));
		float L_35 = V_12;
		float L_36 = V_0;
		if ((!(((float)L_35) < ((float)((-L_36))))))
		{
			goto IL_0198;
		}
	}
	{
		Transform_t1659122786 * L_37 = V_11;
		NullCheck(L_37);
		Vector3_t4282066566  L_38 = Transform_get_localPosition_m668140784(L_37, /*hidden argument*/NULL);
		V_13 = L_38;
		Vector3_t4282066566 * L_39 = (&V_13);
		float L_40 = L_39->get_x_1();
		float L_41 = V_6;
		L_39->set_x_1(((float)((float)L_40+(float)L_41)));
		float L_42 = (&V_13)->get_x_1();
		float L_43 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_42-(float)L_43));
		float L_44 = (&V_13)->get_x_1();
		int32_t L_45 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_46 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_44/(float)(((float)((float)L_45))))), /*hidden argument*/NULL);
		V_14 = L_46;
		int32_t L_47 = __this->get_minIndex_4();
		int32_t L_48 = __this->get_maxIndex_5();
		if ((((int32_t)L_47) == ((int32_t)L_48)))
		{
			goto IL_0178;
		}
	}
	{
		int32_t L_49 = __this->get_minIndex_4();
		int32_t L_50 = V_14;
		if ((((int32_t)L_49) > ((int32_t)L_50)))
		{
			goto IL_0190;
		}
	}
	{
		int32_t L_51 = V_14;
		int32_t L_52 = __this->get_maxIndex_5();
		if ((((int32_t)L_51) > ((int32_t)L_52)))
		{
			goto IL_0190;
		}
	}

IL_0178:
	{
		Transform_t1659122786 * L_53 = V_11;
		Vector3_t4282066566  L_54 = V_13;
		NullCheck(L_53);
		Transform_set_localPosition_m3504330903(L_53, L_54, /*hidden argument*/NULL);
		Transform_t1659122786 * L_55 = V_11;
		int32_t L_56 = V_9;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_55, L_56);
		goto IL_0193;
	}

IL_0190:
	{
		V_5 = (bool)0;
	}

IL_0193:
	{
		goto IL_0240;
	}

IL_0198:
	{
		float L_57 = V_12;
		float L_58 = V_0;
		if ((!(((float)L_57) > ((float)L_58))))
		{
			goto IL_022b;
		}
	}
	{
		Transform_t1659122786 * L_59 = V_11;
		NullCheck(L_59);
		Vector3_t4282066566  L_60 = Transform_get_localPosition_m668140784(L_59, /*hidden argument*/NULL);
		V_15 = L_60;
		Vector3_t4282066566 * L_61 = (&V_15);
		float L_62 = L_61->get_x_1();
		float L_63 = V_6;
		L_61->set_x_1(((float)((float)L_62-(float)L_63)));
		float L_64 = (&V_15)->get_x_1();
		float L_65 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_64-(float)L_65));
		float L_66 = (&V_15)->get_x_1();
		int32_t L_67 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_68 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_66/(float)(((float)((float)L_67))))), /*hidden argument*/NULL);
		V_16 = L_68;
		int32_t L_69 = __this->get_minIndex_4();
		int32_t L_70 = __this->get_maxIndex_5();
		if ((((int32_t)L_69) == ((int32_t)L_70)))
		{
			goto IL_020b;
		}
	}
	{
		int32_t L_71 = __this->get_minIndex_4();
		int32_t L_72 = V_16;
		if ((((int32_t)L_71) > ((int32_t)L_72)))
		{
			goto IL_0223;
		}
	}
	{
		int32_t L_73 = V_16;
		int32_t L_74 = __this->get_maxIndex_5();
		if ((((int32_t)L_73) > ((int32_t)L_74)))
		{
			goto IL_0223;
		}
	}

IL_020b:
	{
		Transform_t1659122786 * L_75 = V_11;
		Vector3_t4282066566  L_76 = V_15;
		NullCheck(L_75);
		Transform_set_localPosition_m3504330903(L_75, L_76, /*hidden argument*/NULL);
		Transform_t1659122786 * L_77 = V_11;
		int32_t L_78 = V_9;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_77, L_78);
		goto IL_0226;
	}

IL_0223:
	{
		V_5 = (bool)0;
	}

IL_0226:
	{
		goto IL_0240;
	}

IL_022b:
	{
		bool L_79 = __this->get_mFirstTime_11();
		if (!L_79)
		{
			goto IL_0240;
		}
	}
	{
		Transform_t1659122786 * L_80 = V_11;
		int32_t L_81 = V_9;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_80, L_81);
	}

IL_0240:
	{
		bool L_82 = __this->get_cullContent_3();
		if (!L_82)
		{
			goto IL_02a9;
		}
	}
	{
		float L_83 = V_12;
		UIPanel_t295209936 * L_84 = __this->get_mPanel_8();
		NullCheck(L_84);
		Vector2_t4282066565  L_85 = UIPanel_get_clipOffset_m3093353146(L_84, /*hidden argument*/NULL);
		V_28 = L_85;
		float L_86 = (&V_28)->get_x_1();
		Transform_t1659122786 * L_87 = __this->get_mTrans_7();
		NullCheck(L_87);
		Vector3_t4282066566  L_88 = Transform_get_localPosition_m668140784(L_87, /*hidden argument*/NULL);
		V_29 = L_88;
		float L_89 = (&V_29)->get_x_1();
		V_12 = ((float)((float)L_83+(float)((float)((float)L_86-(float)L_89))));
		Transform_t1659122786 * L_90 = V_11;
		NullCheck(L_90);
		GameObject_t3674682005 * L_91 = Component_get_gameObject_m1170635899(L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		bool L_92 = UICamera_IsPressed_m2263685468(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (L_92)
		{
			goto IL_02a9;
		}
	}
	{
		Transform_t1659122786 * L_93 = V_11;
		NullCheck(L_93);
		GameObject_t3674682005 * L_94 = Component_get_gameObject_m1170635899(L_93, /*hidden argument*/NULL);
		float L_95 = V_12;
		float L_96 = V_7;
		G_B24_0 = L_94;
		if ((!(((float)L_95) > ((float)L_96))))
		{
			G_B25_0 = L_94;
			goto IL_02a2;
		}
	}
	{
		float L_97 = V_12;
		float L_98 = V_8;
		G_B26_0 = ((((float)L_97) < ((float)L_98))? 1 : 0);
		G_B26_1 = G_B24_0;
		goto IL_02a3;
	}

IL_02a2:
	{
		G_B26_0 = 0;
		G_B26_1 = G_B25_0;
	}

IL_02a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m1380602331(NULL /*static, unused*/, G_B26_1, (bool)G_B26_0, (bool)0, /*hidden argument*/NULL);
	}

IL_02a9:
	{
		int32_t L_99 = V_9;
		V_9 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_02af:
	{
		int32_t L_100 = V_9;
		int32_t L_101 = V_10;
		if ((((int32_t)L_100) < ((int32_t)L_101)))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_04db;
	}

IL_02bd:
	{
		Vector3U5BU5D_t215400611* L_102 = V_1;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 0);
		float L_103 = ((L_102)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_y_2();
		int32_t L_104 = __this->get_itemSize_2();
		V_17 = ((float)((float)L_103-(float)(((float)((float)L_104)))));
		Vector3U5BU5D_t215400611* L_105 = V_1;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 2);
		float L_106 = ((L_105)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_y_2();
		int32_t L_107 = __this->get_itemSize_2();
		V_18 = ((float)((float)L_106+(float)(((float)((float)L_107)))));
		V_19 = 0;
		List_1_t3027308338 * L_108 = __this->get_mChildren_12();
		NullCheck(L_108);
		int32_t L_109 = List_1_get_Count_m261083306(L_108, /*hidden argument*/List_1_get_Count_m261083306_MethodInfo_var);
		V_20 = L_109;
		goto IL_04d2;
	}

IL_02fe:
	{
		List_1_t3027308338 * L_110 = __this->get_mChildren_12();
		int32_t L_111 = V_19;
		NullCheck(L_110);
		Transform_t1659122786 * L_112 = List_1_get_Item_m4268610603(L_110, L_111, /*hidden argument*/List_1_get_Item_m4268610603_MethodInfo_var);
		V_21 = L_112;
		Transform_t1659122786 * L_113 = V_21;
		NullCheck(L_113);
		Vector3_t4282066566  L_114 = Transform_get_localPosition_m668140784(L_113, /*hidden argument*/NULL);
		V_30 = L_114;
		float L_115 = (&V_30)->get_y_2();
		float L_116 = (&V_4)->get_y_2();
		V_22 = ((float)((float)L_115-(float)L_116));
		float L_117 = V_22;
		float L_118 = V_0;
		if ((!(((float)L_117) < ((float)((-L_118))))))
		{
			goto IL_03bb;
		}
	}
	{
		Transform_t1659122786 * L_119 = V_21;
		NullCheck(L_119);
		Vector3_t4282066566  L_120 = Transform_get_localPosition_m668140784(L_119, /*hidden argument*/NULL);
		V_23 = L_120;
		Vector3_t4282066566 * L_121 = (&V_23);
		float L_122 = L_121->get_y_2();
		float L_123 = V_6;
		L_121->set_y_2(((float)((float)L_122+(float)L_123)));
		float L_124 = (&V_23)->get_y_2();
		float L_125 = (&V_4)->get_y_2();
		V_22 = ((float)((float)L_124-(float)L_125));
		float L_126 = (&V_23)->get_y_2();
		int32_t L_127 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_128 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_126/(float)(((float)((float)L_127))))), /*hidden argument*/NULL);
		V_24 = L_128;
		int32_t L_129 = __this->get_minIndex_4();
		int32_t L_130 = __this->get_maxIndex_5();
		if ((((int32_t)L_129) == ((int32_t)L_130)))
		{
			goto IL_039b;
		}
	}
	{
		int32_t L_131 = __this->get_minIndex_4();
		int32_t L_132 = V_24;
		if ((((int32_t)L_131) > ((int32_t)L_132)))
		{
			goto IL_03b3;
		}
	}
	{
		int32_t L_133 = V_24;
		int32_t L_134 = __this->get_maxIndex_5();
		if ((((int32_t)L_133) > ((int32_t)L_134)))
		{
			goto IL_03b3;
		}
	}

IL_039b:
	{
		Transform_t1659122786 * L_135 = V_21;
		Vector3_t4282066566  L_136 = V_23;
		NullCheck(L_135);
		Transform_set_localPosition_m3504330903(L_135, L_136, /*hidden argument*/NULL);
		Transform_t1659122786 * L_137 = V_21;
		int32_t L_138 = V_19;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_137, L_138);
		goto IL_03b6;
	}

IL_03b3:
	{
		V_5 = (bool)0;
	}

IL_03b6:
	{
		goto IL_0463;
	}

IL_03bb:
	{
		float L_139 = V_22;
		float L_140 = V_0;
		if ((!(((float)L_139) > ((float)L_140))))
		{
			goto IL_044e;
		}
	}
	{
		Transform_t1659122786 * L_141 = V_21;
		NullCheck(L_141);
		Vector3_t4282066566  L_142 = Transform_get_localPosition_m668140784(L_141, /*hidden argument*/NULL);
		V_25 = L_142;
		Vector3_t4282066566 * L_143 = (&V_25);
		float L_144 = L_143->get_y_2();
		float L_145 = V_6;
		L_143->set_y_2(((float)((float)L_144-(float)L_145)));
		float L_146 = (&V_25)->get_y_2();
		float L_147 = (&V_4)->get_y_2();
		V_22 = ((float)((float)L_146-(float)L_147));
		float L_148 = (&V_25)->get_y_2();
		int32_t L_149 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_150 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_148/(float)(((float)((float)L_149))))), /*hidden argument*/NULL);
		V_26 = L_150;
		int32_t L_151 = __this->get_minIndex_4();
		int32_t L_152 = __this->get_maxIndex_5();
		if ((((int32_t)L_151) == ((int32_t)L_152)))
		{
			goto IL_042e;
		}
	}
	{
		int32_t L_153 = __this->get_minIndex_4();
		int32_t L_154 = V_26;
		if ((((int32_t)L_153) > ((int32_t)L_154)))
		{
			goto IL_0446;
		}
	}
	{
		int32_t L_155 = V_26;
		int32_t L_156 = __this->get_maxIndex_5();
		if ((((int32_t)L_155) > ((int32_t)L_156)))
		{
			goto IL_0446;
		}
	}

IL_042e:
	{
		Transform_t1659122786 * L_157 = V_21;
		Vector3_t4282066566  L_158 = V_25;
		NullCheck(L_157);
		Transform_set_localPosition_m3504330903(L_157, L_158, /*hidden argument*/NULL);
		Transform_t1659122786 * L_159 = V_21;
		int32_t L_160 = V_19;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_159, L_160);
		goto IL_0449;
	}

IL_0446:
	{
		V_5 = (bool)0;
	}

IL_0449:
	{
		goto IL_0463;
	}

IL_044e:
	{
		bool L_161 = __this->get_mFirstTime_11();
		if (!L_161)
		{
			goto IL_0463;
		}
	}
	{
		Transform_t1659122786 * L_162 = V_21;
		int32_t L_163 = V_19;
		VirtActionInvoker2< Transform_t1659122786 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_162, L_163);
	}

IL_0463:
	{
		bool L_164 = __this->get_cullContent_3();
		if (!L_164)
		{
			goto IL_04cc;
		}
	}
	{
		float L_165 = V_22;
		UIPanel_t295209936 * L_166 = __this->get_mPanel_8();
		NullCheck(L_166);
		Vector2_t4282066565  L_167 = UIPanel_get_clipOffset_m3093353146(L_166, /*hidden argument*/NULL);
		V_31 = L_167;
		float L_168 = (&V_31)->get_y_2();
		Transform_t1659122786 * L_169 = __this->get_mTrans_7();
		NullCheck(L_169);
		Vector3_t4282066566  L_170 = Transform_get_localPosition_m668140784(L_169, /*hidden argument*/NULL);
		V_32 = L_170;
		float L_171 = (&V_32)->get_y_2();
		V_22 = ((float)((float)L_165+(float)((float)((float)L_168-(float)L_171))));
		Transform_t1659122786 * L_172 = V_21;
		NullCheck(L_172);
		GameObject_t3674682005 * L_173 = Component_get_gameObject_m1170635899(L_172, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		bool L_174 = UICamera_IsPressed_m2263685468(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		if (L_174)
		{
			goto IL_04cc;
		}
	}
	{
		Transform_t1659122786 * L_175 = V_21;
		NullCheck(L_175);
		GameObject_t3674682005 * L_176 = Component_get_gameObject_m1170635899(L_175, /*hidden argument*/NULL);
		float L_177 = V_22;
		float L_178 = V_17;
		G_B50_0 = L_176;
		if ((!(((float)L_177) > ((float)L_178))))
		{
			G_B51_0 = L_176;
			goto IL_04c5;
		}
	}
	{
		float L_179 = V_22;
		float L_180 = V_18;
		G_B52_0 = ((((float)L_179) < ((float)L_180))? 1 : 0);
		G_B52_1 = G_B50_0;
		goto IL_04c6;
	}

IL_04c5:
	{
		G_B52_0 = 0;
		G_B52_1 = G_B51_0;
	}

IL_04c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m1380602331(NULL /*static, unused*/, G_B52_1, (bool)G_B52_0, (bool)0, /*hidden argument*/NULL);
	}

IL_04cc:
	{
		int32_t L_181 = V_19;
		V_19 = ((int32_t)((int32_t)L_181+(int32_t)1));
	}

IL_04d2:
	{
		int32_t L_182 = V_19;
		int32_t L_183 = V_20;
		if ((((int32_t)L_182) < ((int32_t)L_183)))
		{
			goto IL_02fe;
		}
	}

IL_04db:
	{
		UIScrollView_t2113479878 * L_184 = __this->get_mScroll_9();
		bool L_185 = V_5;
		NullCheck(L_184);
		L_184->set_restrictWithinPanel_5((bool)((((int32_t)L_185) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void UIWrapContent::OnValidate()
extern "C"  void UIWrapContent_OnValidate_m3655927209 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_maxIndex_5();
		int32_t L_1 = __this->get_minIndex_4();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_2);
	}

IL_001d:
	{
		int32_t L_3 = __this->get_minIndex_4();
		int32_t L_4 = __this->get_maxIndex_5();
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_5);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t UIWrapContent_UpdateItem_m3898182666_MetadataUsageId;
extern "C"  void UIWrapContent_UpdateItem_m3898182666 (UIWrapContent_t33025435 * __this, Transform_t1659122786 * ___item0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_UpdateItem_m3898182666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B4_0 = 0;
	{
		OnInitializeItem_t2759048342 * L_0 = __this->get_onInitializeItem_6();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		UIScrollView_t2113479878 * L_1 = __this->get_mScroll_9();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_movement_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_003c;
		}
	}
	{
		Transform_t1659122786 * L_3 = ___item0;
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Transform_get_localPosition_m668140784(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		int32_t L_6 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_5/(float)(((float)((float)L_6))))), /*hidden argument*/NULL);
		G_B4_0 = L_7;
		goto IL_0057;
	}

IL_003c:
	{
		Transform_t1659122786 * L_8 = ___item0;
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		int32_t L_11 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_10/(float)(((float)((float)L_11))))), /*hidden argument*/NULL);
		G_B4_0 = L_12;
	}

IL_0057:
	{
		V_0 = G_B4_0;
		OnInitializeItem_t2759048342 * L_13 = __this->get_onInitializeItem_6();
		Transform_t1659122786 * L_14 = ___item0;
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = Component_get_gameObject_m1170635899(L_14, /*hidden argument*/NULL);
		int32_t L_16 = ___index1;
		int32_t L_17 = V_0;
		NullCheck(L_13);
		OnInitializeItem_Invoke_m2616578991(L_13, L_15, L_16, L_17, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void UIWrapContent/OnInitializeItem::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitializeItem__ctor_m953163837 (OnInitializeItem_t2759048342 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWrapContent/OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void OnInitializeItem_Invoke_m2616578991 (OnInitializeItem_t2759048342 * __this, GameObject_t3674682005 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnInitializeItem_Invoke_m2616578991((OnInitializeItem_t2759048342 *)__this->get_prev_9(),___go0, ___wrapIndex1, ___realIndex2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t3674682005 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, GameObject_t3674682005 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UIWrapContent/OnInitializeItem::BeginInvoke(UnityEngine.GameObject,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t OnInitializeItem_BeginInvoke_m3195738938_MetadataUsageId;
extern "C"  Il2CppObject * OnInitializeItem_BeginInvoke_m3195738938 (OnInitializeItem_t2759048342 * __this, GameObject_t3674682005 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnInitializeItem_BeginInvoke_m3195738938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___go0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___wrapIndex1);
	__d_args[2] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___realIndex2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UIWrapContent/OnInitializeItem::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitializeItem_EndInvoke_m2249144525 (OnInitializeItem_t2759048342 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Utils.Core.AndroidOBBHandler::.ctor()
extern Il2CppClass* SingletonObject_1_t3364670738_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonObject_1__ctor_m4177696451_MethodInfo_var;
extern const uint32_t AndroidOBBHandler__ctor_m3730580739_MetadataUsageId;
extern "C"  void AndroidOBBHandler__ctor_m3730580739 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidOBBHandler__ctor_m3730580739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonObject_1_t3364670738_il2cpp_TypeInfo_var);
		SingletonObject_1__ctor_m4177696451(__this, /*hidden argument*/SingletonObject_1__ctor_m4177696451_MethodInfo_var);
		return;
	}
}
// System.String Utils.Core.AndroidOBBHandler::get_base64AppKey()
extern "C"  String_t* AndroidOBBHandler_get_base64AppKey_m2086676314 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3Cbase64AppKeyU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Utils.Core.AndroidOBBHandler::set_base64AppKey(System.String)
extern "C"  void AndroidOBBHandler_set_base64AppKey_m2997703735 (AndroidOBBHandler_t3535974956 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cbase64AppKeyU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void Utils.Core.AndroidOBBHandler::SetAppKey(System.String)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral736526603;
extern const uint32_t AndroidOBBHandler_SetAppKey_m3889606401_MetadataUsageId;
extern "C"  void AndroidOBBHandler_SetAppKey_m3889606401 (AndroidOBBHandler_t3535974956 * __this, String_t* ___appKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidOBBHandler_SetAppKey_m3889606401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral736526603, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Utils.Core.AndroidOBBHandler::StartDownload()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral736526603;
extern const uint32_t AndroidOBBHandler_StartDownload_m3227493495_MetadataUsageId;
extern "C"  bool AndroidOBBHandler_StartDownload_m3227493495 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidOBBHandler_StartDownload_m3227493495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral736526603, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Collections.IEnumerator Utils.Core.AndroidOBBHandler::download()
extern Il2CppClass* U3CdownloadU3Ec__IteratorE_t3158326693_il2cpp_TypeInfo_var;
extern const uint32_t AndroidOBBHandler_download_m1886416017_MetadataUsageId;
extern "C"  Il2CppObject * AndroidOBBHandler_download_m1886416017 (AndroidOBBHandler_t3535974956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidOBBHandler_download_m1886416017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CdownloadU3Ec__IteratorE_t3158326693 * V_0 = NULL;
	{
		U3CdownloadU3Ec__IteratorE_t3158326693 * L_0 = (U3CdownloadU3Ec__IteratorE_t3158326693 *)il2cpp_codegen_object_new(U3CdownloadU3Ec__IteratorE_t3158326693_il2cpp_TypeInfo_var);
		U3CdownloadU3Ec__IteratorE__ctor_m1812119398(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CdownloadU3Ec__IteratorE_t3158326693 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Utils.Core.AndroidOBBHandler/<download>c__IteratorE::.ctor()
extern "C"  void U3CdownloadU3Ec__IteratorE__ctor_m1812119398 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Utils.Core.AndroidOBBHandler/<download>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4219348012 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Utils.Core.AndroidOBBHandler/<download>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m720764864 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Utils.Core.AndroidOBBHandler/<download>c__IteratorE::MoveNext()
extern "C"  bool U3CdownloadU3Ec__IteratorE_MoveNext_m816774862 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_003d;
	}

IL_0034:
	{
		__this->set_U24PC_0((-1));
	}

IL_003b:
	{
		return (bool)0;
	}

IL_003d:
	{
		return (bool)1;
	}
	// Dead block : IL_003f: ldloc.1
}
// System.Void Utils.Core.AndroidOBBHandler/<download>c__IteratorE::Dispose()
extern "C"  void U3CdownloadU3Ec__IteratorE_Dispose_m1883269667 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Utils.Core.AndroidOBBHandler/<download>c__IteratorE::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CdownloadU3Ec__IteratorE_Reset_m3753519635_MetadataUsageId;
extern "C"  void U3CdownloadU3Ec__IteratorE_Reset_m3753519635 (U3CdownloadU3Ec__IteratorE_t3158326693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CdownloadU3Ec__IteratorE_Reset_m3753519635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Utils.Core.DataBuilder::.ctor(System.String)
extern Il2CppClass* BinaryWriter_t4146364100_il2cpp_TypeInfo_var;
extern const uint32_t DataBuilder__ctor_m2749382022_MetadataUsageId;
extern "C"  void DataBuilder__ctor_m2749382022 (DataBuilder_t3810402835 * __this, String_t* ____path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataBuilder__ctor_m2749382022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____path0;
		FileStream_t2141505868 * L_1 = File_Open_m1918038371(NULL /*static, unused*/, L_0, 4, /*hidden argument*/NULL);
		__this->set__file_0(L_1);
		FileStream_t2141505868 * L_2 = __this->get__file_0();
		BinaryWriter_t4146364100 * L_3 = (BinaryWriter_t4146364100 *)il2cpp_codegen_object_new(BinaryWriter_t4146364100_il2cpp_TypeInfo_var);
		BinaryWriter__ctor_m3820043020(L_3, L_2, /*hidden argument*/NULL);
		__this->set__writer_1(L_3);
		return;
	}
}
// System.Void Utils.Core.DataBuilder::Add(System.String)
extern "C"  void DataBuilder_Add_m3818414119 (DataBuilder_t3810402835 * __this, String_t* ___data0, const MethodInfo* method)
{
	{
		BinaryWriter_t4146364100 * L_0 = __this->get__writer_1();
		String_t* L_1 = ___data0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void System.IO.BinaryWriter::Write(System.String) */, L_0, L_1);
		BinaryWriter_t4146364100 * L_2 = __this->get__writer_1();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.BinaryWriter::Close() */, L_2);
		__this->set__writer_1((BinaryWriter_t4146364100 *)NULL);
		return;
	}
}
// System.Void Utils.Core.DataBuilder::End()
extern "C"  void DataBuilder_End_m306851957 (DataBuilder_t3810402835 * __this, const MethodInfo* method)
{
	{
		FileStream_t2141505868 * L_0 = __this->get__file_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_0);
		FileStream_t2141505868 * L_1 = __this->get__file_0();
		NullCheck(L_1);
		Stream_Dispose_m2904306374(L_1, /*hidden argument*/NULL);
		__this->set__file_0((FileStream_t2141505868 *)NULL);
		return;
	}
}
// System.Void Utils.Core.FileDescription::.ctor()
extern "C"  void FileDescription__ctor_m2891698829 (FileDescription_t856434530 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Utils.Core.FileDescription::get_path()
extern "C"  String_t* FileDescription_get_path_m682857928 (FileDescription_t856434530 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CpathU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Utils.Core.FileDescription::set_path(System.String)
extern "C"  void FileDescription_set_path_m1773124681 (FileDescription_t856434530 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CpathU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Utils.Core.FileDescription::get_filename()
extern "C"  String_t* FileDescription_get_filename_m2317865514 (FileDescription_t856434530 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CfilenameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Utils.Core.FileDescription::set_filename(System.String)
extern "C"  void FileDescription_set_filename_m2885717927 (FileDescription_t856434530 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfilenameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String Utils.Core.FileDescription::get_extension()
extern "C"  String_t* FileDescription_get_extension_m920864990 (FileDescription_t856434530 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CextensionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Utils.Core.FileDescription::set_extension(System.String)
extern "C"  void FileDescription_set_extension_m1352110645 (FileDescription_t856434530 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CextensionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Utils.Core.FileHelper::.ctor()
extern "C"  void FileHelper__ctor_m634924623 (FileHelper_t1028318256 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Utils.Core.FileHelper::GetDownloadStream(System.String,UnityEngine.WWW&,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t FileHelper_GetDownloadStream_m219723097_MetadataUsageId;
extern "C"  bool FileHelper_GetDownloadStream_m219723097 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWW_t3134621005 ** ___www1, String_t* ____folder2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_GetDownloadStream_m219723097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ____folder2;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		goto IL_001b;
	}

IL_0010:
	{
		String_t* L_2 = ____folder2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, L_2, _stringLiteral47, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_001b:
	{
		String_t* L_4 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_5 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, G_B3_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		bool L_8 = FileHelper_Exists_m3784167160(NULL /*static, unused*/, L_7, (bool)0, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		WWW_t3134621005 ** L_9 = ___www1;
		String_t* L_10 = V_0;
		String_t* L_11 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_10, (bool)1, /*hidden argument*/NULL);
		WWW_t3134621005 * L_12 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_12, L_11, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)L_12;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)L_12);
		return (bool)0;
	}

IL_0043:
	{
		WWW_t3134621005 ** L_13 = ___www1;
		String_t* L_14 = ___url0;
		WWW_t3134621005 * L_15 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_15, L_14, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)L_15;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)L_15);
		return (bool)1;
	}
}
// Utils.Core.FileDescription Utils.Core.FileHelper::GetFileDescription(System.String)
extern Il2CppClass* FileDescription_t856434530_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_GetFileDescription_m4037595026_MetadataUsageId;
extern "C"  FileDescription_t856434530 * FileHelper_GetFileDescription_m4037595026 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_GetFileDescription_m4037595026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FileDescription_t856434530 * V_0 = NULL;
	{
		FileDescription_t856434530 * L_0 = (FileDescription_t856434530 *)il2cpp_codegen_object_new(FileDescription_t856434530_il2cpp_TypeInfo_var);
		FileDescription__ctor_m2891698829(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FileDescription_t856434530 * L_1 = V_0;
		String_t* L_2 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_GetDirectoryName_m1772680861(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		FileDescription_set_path_m1773124681(L_1, L_3, /*hidden argument*/NULL);
		FileDescription_t856434530 * L_4 = V_0;
		String_t* L_5 = ___path0;
		String_t* L_6 = Path_GetFileNameWithoutExtension_m4224094767(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		FileDescription_set_filename_m2885717927(L_4, L_6, /*hidden argument*/NULL);
		FileDescription_t856434530 * L_7 = V_0;
		String_t* L_8 = ___path0;
		String_t* L_9 = Path_GetExtension_m2722066454(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		FileDescription_set_extension_m1352110645(L_7, L_9, /*hidden argument*/NULL);
		FileDescription_t856434530 * L_10 = V_0;
		return L_10;
	}
}
// System.String Utils.Core.FileHelper::PersistentDataPath(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral3439929502;
extern const uint32_t FileHelper_PersistentDataPath_m2607611251_MetadataUsageId;
extern "C"  String_t* FileHelper_PersistentDataPath_m2607611251 (Il2CppObject * __this /* static, unused */, String_t* ___file0, bool ___filePath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_PersistentDataPath_m2607611251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___file0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, L_0, _stringLiteral47, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = ___filePath1;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		V_1 = _stringLiteral3439929502;
		String_t* L_4 = V_1;
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0025:
	{
		String_t* L_7 = V_0;
		return L_7;
	}
}
// System.String Utils.Core.FileHelper::StreamingDataPath(System.String)
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_StreamingDataPath_m929338273_MetadataUsageId;
extern "C"  String_t* FileHelper_StreamingDataPath_m929338273 (Il2CppObject * __this /* static, unused */, String_t* ___file0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_StreamingDataPath_m929338273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Application_get_streamingAssetsPath_m1181082379(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___file0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m4122812896(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Utils.Core.FileHelper::DataPath(Utils.Core.FileHelper/EFolder,System.String)
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_DataPath_m1267838125_MetadataUsageId;
extern "C"  String_t* FileHelper_DataPath_m1267838125 (Il2CppObject * __this /* static, unused */, int32_t ___folder0, String_t* ___file1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_DataPath_m1267838125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___folder0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___file1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_Combine_m4122812896(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		String_t* L_4 = Application_get_streamingAssetsPath_m1181082379(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___file1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_6 = Path_Combine_m4122812896(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void Utils.Core.FileHelper::CreateFoldersForFile(System.String,Utils.Core.FileHelper/EFolder)
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t FileHelper_CreateFoldersForFile_m576049309_MetadataUsageId;
extern "C"  void FileHelper_CreateFoldersForFile_m576049309 (Il2CppObject * __this /* static, unused */, String_t* ___file0, int32_t ___folder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_CreateFoldersForFile_m576049309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* G_B6_0 = NULL;
	{
		String_t* L_0 = ___file0;
		CharU5BU5D_t3324145743* L_1 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		NullCheck(L_0);
		StringU5BU5D_t4054002952* L_2 = String_Split_m290179486(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t4054002952* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_4 = ___folder1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_5 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_5;
		goto IL_0042;
	}

IL_002c:
	{
		int32_t L_6 = ___folder1;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_7 = Application_get_streamingAssetsPath_m1181082379(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_7;
		goto IL_0042;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_8;
	}

IL_0042:
	{
		V_1 = G_B6_0;
		V_2 = 0;
		goto IL_007e;
	}

IL_004a:
	{
		StringU5BU5D_t4054002952* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		String_t* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007a;
		}
	}
	{
		String_t* L_14 = V_1;
		StringU5BU5D_t4054002952* L_15 = V_0;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		String_t* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1825781833(NULL /*static, unused*/, L_14, _stringLiteral47, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		String_t* L_20 = V_3;
		bool L_21 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_22 = V_3;
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_0078:
	{
		String_t* L_23 = V_3;
		V_1 = L_23;
	}

IL_007a:
	{
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_007e:
	{
		int32_t L_25 = V_2;
		StringU5BU5D_t4054002952* L_26 = V_0;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))-(int32_t)1)))))
		{
			goto IL_004a;
		}
	}

IL_0089:
	{
		return;
	}
}
// System.String Utils.Core.FileHelper::CopyToPersistent(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2104325847;
extern Il2CppCodeGenString* _stringLiteral73243929;
extern const uint32_t FileHelper_CopyToPersistent_m1849145044_MetadataUsageId;
extern "C"  String_t* FileHelper_CopyToPersistent_m1849145044 (Il2CppObject * __this /* static, unused */, String_t* ___file0, bool ___overwrite1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_CopyToPersistent_m1849145044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___file0;
		String_t* L_1 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___overwrite1;
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_3 = V_0;
		bool L_4 = File_Exists_m1326262381(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0056;
		}
	}

IL_0019:
	{
		String_t* L_5 = ___file0;
		String_t* L_6 = FileHelper_StreamingDataPath_m929338273(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = ___file0;
		FileHelper_CreateFoldersForFile_m576049309(NULL /*static, unused*/, L_7, 1, /*hidden argument*/NULL);
		String_t* L_8 = V_1;
		bool L_9 = File_Exists_m1326262381(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_10 = V_1;
		String_t* L_11 = V_0;
		File_Copy_m4125374219(NULL /*static, unused*/, L_10, L_11, (bool)1, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_003f:
	{
		V_0 = (String_t*)NULL;
		String_t* L_12 = ___file0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2104325847, L_12, _stringLiteral73243929, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0056:
	{
		String_t* L_14 = V_0;
		return L_14;
	}
}
// System.Collections.IEnumerator Utils.Core.FileHelper::CopyToPersistent(System.String,System.Action`1<System.String>,System.Boolean)
extern Il2CppClass* U3CCopyToPersistentU3Ec__IteratorF_t3141760867_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_CopyToPersistent_m1002064137_MetadataUsageId;
extern "C"  Il2CppObject * FileHelper_CopyToPersistent_m1002064137 (Il2CppObject * __this /* static, unused */, String_t* ___file0, Action_1_t403047693 * ___callback1, bool ___overwrite2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_CopyToPersistent_m1002064137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * V_0 = NULL;
	{
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_0 = (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 *)il2cpp_codegen_object_new(U3CCopyToPersistentU3Ec__IteratorF_t3141760867_il2cpp_TypeInfo_var);
		U3CCopyToPersistentU3Ec__IteratorF__ctor_m2209555096(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_1 = V_0;
		String_t* L_2 = ___file0;
		NullCheck(L_1);
		L_1->set_file_0(L_2);
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_3 = V_0;
		bool L_4 = ___overwrite2;
		NullCheck(L_3);
		L_3->set_overwrite_2(L_4);
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_5 = V_0;
		Action_1_t403047693 * L_6 = ___callback1;
		NullCheck(L_5);
		L_5->set_callback_4(L_6);
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_7 = V_0;
		String_t* L_8 = ___file0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Efile_7(L_8);
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_9 = V_0;
		bool L_10 = ___overwrite2;
		NullCheck(L_9);
		L_9->set_U3CU24U3Eoverwrite_8(L_10);
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_11 = V_0;
		Action_1_t403047693 * L_12 = ___callback1;
		NullCheck(L_11);
		L_11->set_U3CU24U3Ecallback_9(L_12);
		U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * L_13 = V_0;
		return L_13;
	}
}
// System.String Utils.Core.FileHelper::ReadText(System.String)
extern Il2CppClass* StreamReader_t2549717843_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_ReadText_m1911600301_MetadataUsageId;
extern "C"  String_t* FileHelper_ReadText_m1911600301 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_ReadText_m1911600301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StreamReader_t2549717843 * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		String_t* L_0 = ___filename0;
		String_t* L_1 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = File_Exists_m1326262381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0015:
	{
		String_t* L_4 = V_0;
		StreamReader_t2549717843 * L_5 = (StreamReader_t2549717843 *)il2cpp_codegen_object_new(StreamReader_t2549717843_il2cpp_TypeInfo_var);
		StreamReader__ctor_m3789405692(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		StreamReader_t2549717843 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.StreamReader::ReadToEnd() */, L_6);
		V_2 = L_7;
		StreamReader_t2549717843 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.StreamReader::Close() */, L_8);
		StreamReader_t2549717843 * L_9 = V_1;
		NullCheck(L_9);
		TextReader_Dispose_m377592054(L_9, /*hidden argument*/NULL);
		V_1 = (StreamReader_t2549717843 *)NULL;
		String_t* L_10 = V_2;
		return L_10;
	}
}
// System.Byte[] Utils.Core.FileHelper::Read(System.String)
extern Il2CppClass* BinaryReader_t3990958868_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_Read_m2873044513_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* FileHelper_Read_m2873044513 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_Read_m2873044513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	FileStream_t2141505868 * V_1 = NULL;
	BinaryReader_t3990958868 * V_2 = NULL;
	ByteU5BU5D_t4260760469* V_3 = NULL;
	{
		String_t* L_0 = ___filename0;
		String_t* L_1 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = File_Exists_m1326262381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return (ByteU5BU5D_t4260760469*)NULL;
	}

IL_0015:
	{
		String_t* L_4 = V_0;
		FileStream_t2141505868 * L_5 = File_Open_m1918038371(NULL /*static, unused*/, L_4, 3, /*hidden argument*/NULL);
		V_1 = L_5;
		FileStream_t2141505868 * L_6 = V_1;
		BinaryReader_t3990958868 * L_7 = (BinaryReader_t3990958868 *)il2cpp_codegen_object_new(BinaryReader_t3990958868_il2cpp_TypeInfo_var);
		BinaryReader__ctor_m449904828(L_7, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		BinaryReader_t3990958868 * L_8 = V_2;
		FileStream_t2141505868 * L_9 = V_1;
		NullCheck(L_9);
		int64_t L_10 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.FileStream::get_Length() */, L_9);
		NullCheck(L_8);
		ByteU5BU5D_t4260760469* L_11 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, int32_t >::Invoke(14 /* System.Byte[] System.IO.BinaryReader::ReadBytes(System.Int32) */, L_8, (((int32_t)((int32_t)L_10))));
		V_3 = L_11;
		FileStream_t2141505868 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_12);
		FileStream_t2141505868 * L_13 = V_1;
		NullCheck(L_13);
		Stream_Dispose_m2904306374(L_13, /*hidden argument*/NULL);
		V_1 = (FileStream_t2141505868 *)NULL;
		BinaryReader_t3990958868 * L_14 = V_2;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(6 /* System.Void System.IO.BinaryReader::Close() */, L_14);
		V_2 = (BinaryReader_t3990958868 *)NULL;
		ByteU5BU5D_t4260760469* L_15 = V_3;
		return L_15;
	}
}
// System.String Utils.Core.FileHelper::Write(System.String,System.Byte[])
extern Il2CppClass* BinaryWriter_t4146364100_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_Write_m1586799798_MetadataUsageId;
extern "C"  String_t* FileHelper_Write_m1586799798 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_Write_m1586799798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	FileStream_t2141505868 * V_1 = NULL;
	BinaryWriter_t4146364100 * V_2 = NULL;
	{
		String_t* L_0 = ___filename0;
		String_t* L_1 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = File_Exists_m1326262381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_4 = ___filename0;
		FileHelper_CreateFoldersForFile_m576049309(NULL /*static, unused*/, L_4, 1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		String_t* L_5 = V_0;
		FileStream_t2141505868 * L_6 = File_Open_m1918038371(NULL /*static, unused*/, L_5, 4, /*hidden argument*/NULL);
		V_1 = L_6;
		ByteU5BU5D_t4260760469* L_7 = ___data1;
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		FileStream_t2141505868 * L_8 = V_1;
		BinaryWriter_t4146364100 * L_9 = (BinaryWriter_t4146364100 *)il2cpp_codegen_object_new(BinaryWriter_t4146364100_il2cpp_TypeInfo_var);
		BinaryWriter__ctor_m3820043020(L_9, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		BinaryWriter_t4146364100 * L_10 = V_2;
		ByteU5BU5D_t4260760469* L_11 = ___data1;
		NullCheck(L_10);
		VirtActionInvoker1< ByteU5BU5D_t4260760469* >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte[]) */, L_10, L_11);
		BinaryWriter_t4146364100 * L_12 = V_2;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.BinaryWriter::Close() */, L_12);
		V_2 = (BinaryWriter_t4146364100 *)NULL;
	}

IL_003e:
	{
		FileStream_t2141505868 * L_13 = V_1;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_13);
		FileStream_t2141505868 * L_14 = V_1;
		NullCheck(L_14);
		Stream_Dispose_m2904306374(L_14, /*hidden argument*/NULL);
		V_1 = (FileStream_t2141505868 *)NULL;
		String_t* L_15 = V_0;
		return L_15;
	}
}
// System.String Utils.Core.FileHelper::Write(System.String,System.String)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_Write_m3644605423_MetadataUsageId;
extern "C"  String_t* FileHelper_Write_m3644605423 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, String_t* ___stringData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_Write_m3644605423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___filename0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_1 = Encoding_get_ASCII_m1425378925(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___stringData1;
		NullCheck(L_1);
		ByteU5BU5D_t4260760469* L_3 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, L_2);
		String_t* L_4 = FileHelper_Write_m1586799798(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Utils.Core.DataBuilder Utils.Core.FileHelper::BeginWrite(System.String)
extern Il2CppClass* DataBuilder_t3810402835_il2cpp_TypeInfo_var;
extern const uint32_t FileHelper_BeginWrite_m9346805_MetadataUsageId;
extern "C"  DataBuilder_t3810402835 * FileHelper_BeginWrite_m9346805 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FileHelper_BeginWrite_m9346805_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___filename0;
		String_t* L_1 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = File_Exists_m1326262381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_4 = ___filename0;
		FileHelper_CreateFoldersForFile_m576049309(NULL /*static, unused*/, L_4, 1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		String_t* L_5 = V_0;
		DataBuilder_t3810402835 * L_6 = (DataBuilder_t3810402835 *)il2cpp_codegen_object_new(DataBuilder_t3810402835_il2cpp_TypeInfo_var);
		DataBuilder__ctor_m2749382022(L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Utils.Core.FileHelper::Delete(System.String)
extern "C"  bool FileHelper_Delete_m3140953270 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___filename0;
		String_t* L_1 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = File_Exists_m1326262381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_4 = V_0;
		File_Delete_m760984832(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_001b:
	{
		return (bool)0;
	}
}
// System.Boolean Utils.Core.FileHelper::Exists(System.String,System.Boolean)
extern "C"  bool FileHelper_Exists_m3784167160 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___isAbsulute1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = ___isAbsulute1;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		String_t* L_1 = ___name0;
		G_B3_0 = L_1;
		goto IL_0013;
	}

IL_000c:
	{
		String_t* L_2 = ___name0;
		String_t* L_3 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_2, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0013:
	{
		V_0 = G_B3_0;
		String_t* L_4 = V_0;
		bool L_5 = File_Exists_m1326262381(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_6 = V_0;
		bool L_7 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_7));
		goto IL_0028;
	}

IL_0027:
	{
		G_B6_0 = 1;
	}

IL_0028:
	{
		return (bool)G_B6_0;
	}
}
// System.String[] Utils.Core.FileHelper::GetFiles(System.String,Utils.Core.FileHelper/EFolder)
extern "C"  StringU5BU5D_t4054002952* FileHelper_GetFiles_m975275321 (Il2CppObject * __this /* static, unused */, String_t* ____root0, int32_t ___folder1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___folder1;
		String_t* L_1 = ____root0;
		String_t* L_2 = FileHelper_DataPath_m1267838125(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		StringU5BU5D_t4054002952* L_4 = Directory_GetFiles_m3665304654(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::.ctor()
extern "C"  void U3CCopyToPersistentU3Ec__IteratorF__ctor_m2209555096 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCopyToPersistentU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1477433796 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCopyToPersistentU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3440764248 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3709300246_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2104325847;
extern Il2CppCodeGenString* _stringLiteral73243929;
extern const uint32_t U3CCopyToPersistentU3Ec__IteratorF_MoveNext_m1207548484_MetadataUsageId;
extern "C"  bool U3CCopyToPersistentU3Ec__IteratorF_MoveNext_m1207548484 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCopyToPersistentU3Ec__IteratorF_MoveNext_m1207548484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00c6;
		}
	}
	{
		goto IL_00e9;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_file_0();
		String_t* L_3 = FileHelper_PersistentDataPath_m2607611251(NULL /*static, unused*/, L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_U3C_pathU3E__0_1(L_3);
		bool L_4 = __this->get_overwrite_2();
		if (L_4)
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_5 = __this->get_U3C_pathU3E__0_1();
		bool L_6 = File_Exists_m1326262381(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_00b3;
		}
	}

IL_004e:
	{
		String_t* L_7 = __this->get_file_0();
		String_t* L_8 = FileHelper_StreamingDataPath_m929338273(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_U3C_sourceU3E__1_3(L_8);
		String_t* L_9 = __this->get_file_0();
		FileHelper_CreateFoldersForFile_m576049309(NULL /*static, unused*/, L_9, 1, /*hidden argument*/NULL);
		String_t* L_10 = __this->get_U3C_sourceU3E__1_3();
		bool L_11 = File_Exists_m1326262381(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0092;
		}
	}
	{
		String_t* L_12 = __this->get_U3C_sourceU3E__1_3();
		String_t* L_13 = __this->get_U3C_pathU3E__0_1();
		File_Copy_m4125374219(NULL /*static, unused*/, L_12, L_13, (bool)1, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0092:
	{
		__this->set_U3C_pathU3E__0_1((String_t*)NULL);
		String_t* L_14 = __this->get_file_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2104325847, L_14, _stringLiteral73243929, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		__this->set_U24current_6(NULL);
		__this->set_U24PC_5(1);
		goto IL_00eb;
	}

IL_00c6:
	{
		Action_1_t403047693 * L_16 = __this->get_callback_4();
		if (!L_16)
		{
			goto IL_00e2;
		}
	}
	{
		Action_1_t403047693 * L_17 = __this->get_callback_4();
		String_t* L_18 = __this->get_U3C_pathU3E__0_1();
		NullCheck(L_17);
		Action_1_Invoke_m3709300246(L_17, L_18, /*hidden argument*/Action_1_Invoke_m3709300246_MethodInfo_var);
	}

IL_00e2:
	{
		__this->set_U24PC_5((-1));
	}

IL_00e9:
	{
		return (bool)0;
	}

IL_00eb:
	{
		return (bool)1;
	}
	// Dead block : IL_00ed: ldloc.1
}
// System.Void Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::Dispose()
extern "C"  void U3CCopyToPersistentU3Ec__IteratorF_Dispose_m1566886101 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Utils.Core.FileHelper/<CopyToPersistent>c__IteratorF::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCopyToPersistentU3Ec__IteratorF_Reset_m4150955333_MetadataUsageId;
extern "C"  void U3CCopyToPersistentU3Ec__IteratorF_Reset_m4150955333 (U3CCopyToPersistentU3Ec__IteratorF_t3141760867 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCopyToPersistentU3Ec__IteratorF_Reset_m4150955333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Utils.Vuforia.DataSetHandler::.ctor()
extern Il2CppCodeGenString* _stringLiteral268998770;
extern const uint32_t DataSetHandler__ctor_m2282493888_MetadataUsageId;
extern "C"  void DataSetHandler__ctor_m2282493888 (DataSetHandler_t896450754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataSetHandler__ctor_m2282493888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__dataSet_4(_stringLiteral268998770);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils.Vuforia.DataSetHandler::Start()
extern const Il2CppType* VuforiaAbstractBehaviour_t1091759131_0_0_0_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaAbstractBehaviour_t1091759131_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* DataSetHandler_LoadDataSet_m355100464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3189511828;
extern Il2CppCodeGenString* _stringLiteral2120296634;
extern Il2CppCodeGenString* _stringLiteral587098826;
extern const uint32_t DataSetHandler_Start_m1229631680_MetadataUsageId;
extern "C"  void DataSetHandler_Start_m1229631680 (DataSetHandler_t896450754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataSetHandler_Start_m1229631680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t1091759131 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3189511828, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t1091759131_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t1091759131 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t1091759131_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t1091759131 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		VuforiaAbstractBehaviour_t1091759131 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)DataSetHandler_LoadDataSet_m355100464_MethodInfo_var);
		Action_t3771233898 * L_6 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_RegisterVuforiaStartedCallback_m2995837188(L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2120296634, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral587098826, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void Utils.Vuforia.DataSetHandler::LoadDataSet()
extern Il2CppClass* TrackerManager_t2355365335_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1101783743_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4007703131_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t455954211_m3912055616_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral76866252;
extern Il2CppCodeGenString* _stringLiteral1489193;
extern Il2CppCodeGenString* _stringLiteral1469609;
extern Il2CppCodeGenString* _stringLiteral2168224380;
extern Il2CppCodeGenString* _stringLiteral1398638946;
extern Il2CppCodeGenString* _stringLiteral4294767299;
extern const uint32_t DataSetHandler_LoadDataSet_m355100464_MetadataUsageId;
extern "C"  void DataSetHandler_LoadDataSet_m355100464 (DataSetHandler_t896450754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataSetHandler_LoadDataSet_m355100464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FileDescription_t856434530 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	DataSet_t2095838082 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t2355365335_il2cpp_TypeInfo_var);
		TrackerManager_t2355365335 * L_0 = TrackerManager_get_Instance_m2256350144(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t455954211 * L_1 = GenericVirtFuncInvoker0< ObjectTracker_t455954211 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t455954211_m3912055616_MethodInfo_var, L_0);
		__this->set_tracker_3(L_1);
		String_t* L_2 = __this->get__dataSet_4();
		FileDescription_t856434530 * L_3 = FileHelper_GetFileDescription_m4037595026(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FileDescription_t856434530 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = FileDescription_get_filename_m2317865514(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral76866252, L_5, _stringLiteral1489193, /*hidden argument*/NULL);
		V_1 = L_6;
		FileDescription_t856434530 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = FileDescription_get_filename_m2317865514(L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral76866252, L_8, _stringLiteral1469609, /*hidden argument*/NULL);
		V_2 = L_9;
		ObjectTracker_t455954211 * L_10 = __this->get_tracker_3();
		NullCheck(L_10);
		Il2CppObject* L_11 = VirtFuncInvoker0< Il2CppObject* >::Invoke(15 /* System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetDataSets() */, L_10);
		NullCheck(L_11);
		Il2CppObject* L_12 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>::GetEnumerator() */, IEnumerable_1_t1101783743_il2cpp_TypeInfo_var, L_11);
		V_4 = L_12;
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0099;
		}

IL_005f:
		{
			Il2CppObject* L_13 = V_4;
			NullCheck(L_13);
			DataSet_t2095838082 * L_14 = InterfaceFuncInvoker0< DataSet_t2095838082 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>::get_Current() */, IEnumerator_1_t4007703131_il2cpp_TypeInfo_var, L_13);
			V_3 = L_14;
			DataSet_t2095838082 * L_15 = V_3;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.DataSet::get_Path() */, L_15);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_17 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2168224380, L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
			Debug_Log_m1731103628(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
			DataSet_t2095838082 * L_18 = V_3;
			NullCheck(L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.DataSet::get_Path() */, L_18);
			String_t* L_20 = V_1;
			NullCheck(L_19);
			bool L_21 = String_Contains_m3032019141(L_19, L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_0099;
			}
		}

IL_008d:
		{
			DataSet_t2095838082 * L_22 = V_3;
			__this->set_mDataset_2(L_22);
			goto IL_00a5;
		}

IL_0099:
		{
			Il2CppObject* L_23 = V_4;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_005f;
			}
		}

IL_00a5:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_00aa);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00aa;
	}

FINALLY_00aa:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_25 = V_4;
			if (L_25)
			{
				goto IL_00af;
			}
		}

IL_00ae:
		{
			IL2CPP_END_FINALLY(170)
		}

IL_00af:
		{
			Il2CppObject* L_26 = V_4;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_26);
			IL2CPP_END_FINALLY(170)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(170)
	{
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00b7:
	{
		DataSet_t2095838082 * L_27 = __this->get_mDataset_2();
		if (L_27)
		{
			goto IL_012a;
		}
	}
	{
		ObjectTracker_t455954211 * L_28 = __this->get_tracker_3();
		NullCheck(L_28);
		DataSet_t2095838082 * L_29 = VirtFuncInvoker0< DataSet_t2095838082 * >::Invoke(10 /* Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet() */, L_28);
		__this->set_mDataset_2(L_29);
		String_t* L_30 = V_1;
		String_t* L_31 = FileHelper_CopyToPersistent_m1849145044(NULL /*static, unused*/, L_30, (bool)0, /*hidden argument*/NULL);
		V_5 = L_31;
		String_t* L_32 = V_2;
		String_t* L_33 = FileHelper_CopyToPersistent_m1849145044(NULL /*static, unused*/, L_32, (bool)0, /*hidden argument*/NULL);
		V_6 = L_33;
		String_t* L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1398638946, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		DataSet_t2095838082 * L_36 = __this->get_mDataset_2();
		String_t* L_37 = V_5;
		NullCheck(L_36);
		bool L_38 = VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(7 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.VuforiaUnity/StorageType) */, L_36, L_37, 2);
		if (L_38)
		{
			goto IL_0118;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral4294767299, /*hidden argument*/NULL);
		goto IL_012a;
	}

IL_0118:
	{
		ObjectTracker_t455954211 * L_39 = __this->get_tracker_3();
		DataSet_t2095838082 * L_40 = __this->get_mDataset_2();
		NullCheck(L_39);
		VirtFuncInvoker1< bool, DataSet_t2095838082 * >::Invoke(12 /* System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet) */, L_39, L_40);
	}

IL_012a:
	{
		VirtActionInvoker0::Invoke(4 /* System.Void Utils.Vuforia.DataSetHandler::OnDataSetLoaded() */, __this);
		return;
	}
}
// System.Void VideoCopyTexture::.ctor()
extern "C"  void VideoCopyTexture__ctor_m3150243440 (VideoCopyTexture_t908209291 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoCopyTexture::Start()
extern "C"  void VideoCopyTexture_Start_m2097381232 (VideoCopyTexture_t908209291 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VideoCopyTexture::Update()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var;
extern const uint32_t VideoCopyTexture_Update_m600160925_MetadataUsageId;
extern "C"  void VideoCopyTexture_Update_m600160925 (VideoCopyTexture_t908209291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoCopyTexture_Update_m600160925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get_m_srcVideo_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		MediaPlayerCtrl_t3572035536 * L_2 = __this->get_m_srcVideo_2();
		NullCheck(L_2);
		int32_t L_3 = MediaPlayerCtrl_GetCurrentState_m372257026(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_0033;
		}
	}
	{
		MediaPlayerCtrl_t3572035536 * L_4 = __this->get_m_srcVideo_2();
		NullCheck(L_4);
		int32_t L_5 = MediaPlayerCtrl_GetCurrentState_m372257026(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_0093;
		}
	}

IL_0033:
	{
		MediaPlayerCtrl_t3572035536 * L_6 = __this->get_m_srcVideo_2();
		NullCheck(L_6);
		Texture2D_t3884108195 * L_7 = MediaPlayerCtrl_GetVideoTexture_m3882756556(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0093;
		}
	}
	{
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		MeshRenderer_t2804666580 * L_10 = Component_GetComponent_TisMeshRenderer_t2804666580_m541358362(L_9, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var);
		NullCheck(L_10);
		Material_t3870600107 * L_11 = Renderer_get_material_m2720864603(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Texture_t2526458961 * L_12 = Material_get_mainTexture_m1012267054(L_11, /*hidden argument*/NULL);
		MediaPlayerCtrl_t3572035536 * L_13 = __this->get_m_srcVideo_2();
		NullCheck(L_13);
		Texture2D_t3884108195 * L_14 = MediaPlayerCtrl_GetVideoTexture_m3882756556(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0093;
		}
	}
	{
		Transform_t1659122786 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		MeshRenderer_t2804666580 * L_17 = Component_GetComponent_TisMeshRenderer_t2804666580_m541358362(L_16, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var);
		NullCheck(L_17);
		Material_t3870600107 * L_18 = Renderer_get_material_m2720864603(L_17, /*hidden argument*/NULL);
		MediaPlayerCtrl_t3572035536 * L_19 = __this->get_m_srcVideo_2();
		NullCheck(L_19);
		Texture2D_t3884108195 * L_20 = MediaPlayerCtrl_GetVideoTexture_m3882756556(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Material_set_mainTexture_m3116438437(L_18, L_20, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void VRCameraClamper::.ctor()
extern "C"  void VRCameraClamper__ctor_m271757540 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	{
		__this->set_enableGyro_24((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRCameraClamper::Awake()
extern Il2CppClass* AppLibrary_t3292178298_il2cpp_TypeInfo_var;
extern Il2CppClass* Cardboard_t1761541558_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisCardboardHead_t2975823030_m3898267871_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1966836989;
extern const uint32_t VRCameraClamper_Awake_m509362759_MetadataUsageId;
extern "C"  void VRCameraClamper_Awake_m509362759 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRCameraClamper_Awake_m509362759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	VRCameraClamper_t2660686439 * G_B6_0 = NULL;
	VRCameraClamper_t2660686439 * G_B5_0 = NULL;
	float G_B7_0 = 0.0f;
	VRCameraClamper_t2660686439 * G_B7_1 = NULL;
	{
		__this->set_updateRotation_8((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(AppLibrary_t3292178298_il2cpp_TypeInfo_var);
		uint8_t L_0 = ((AppLibrary_t3292178298_StaticFields*)AppLibrary_t3292178298_il2cpp_TypeInfo_var->static_fields)->get_sceneLoad_27();
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppLibrary_t3292178298_il2cpp_TypeInfo_var);
		uint8_t L_1 = ((AppLibrary_t3292178298_StaticFields*)AppLibrary_t3292178298_il2cpp_TypeInfo_var->static_fields)->get_sceneLoad_27();
		((AppLibrary_t3292178298_StaticFields*)AppLibrary_t3292178298_il2cpp_TypeInfo_var->static_fields)->set_sceneLoad_27((((int32_t)((uint8_t)((int32_t)((int32_t)L_1+(int32_t)1))))));
		int32_t L_2 = Application_get_loadedLevel_m946446301(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_3 = __this->get_maxUPRot_5();
		__this->set_originalMaxUpRot_16(L_3);
		float L_4 = __this->get_maxDownRot_6();
		__this->set_originalMaxDownRot_17(L_4);
		CardboardHead_t2975823030 * L_5 = Component_GetComponentInChildren_TisCardboardHead_t2975823030_m3898267871(__this, /*hidden argument*/Component_GetComponentInChildren_TisCardboardHead_t2975823030_m3898267871_MethodInfo_var);
		__this->set_cBHead_2(L_5);
		bool L_6 = __this->get_debugRotation_7();
		if (L_6)
		{
			goto IL_0069;
		}
	}
	{
		TextMesh_t2567681854 * L_7 = __this->get_cBHeadRotTM_4();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)0, /*hidden argument*/NULL);
	}

IL_0069:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t1761541558_il2cpp_TypeInfo_var);
		Cardboard_t1761541558 * L_9 = Cardboard_get_SDK_m2351368065(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Pose3D_t2396367586 * L_10 = Cardboard_get_HeadPose_m2115147790(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Quaternion_t1553702882  L_11 = Pose3D_get_Orientation_m114671670(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		Vector3_t4282066566  L_12 = Quaternion_get_eulerAngles_m997303795((&V_0), /*hidden argument*/NULL);
		__this->set_headRot2_20(L_12);
		Vector3_t4282066566 * L_13 = __this->get_address_of_headRot2_20();
		float L_14 = L_13->get_x_1();
		__this->set_xRot2_14(L_14);
		float L_15 = __this->get_maxDownRot_6();
		float L_16 = __this->get_xRot2_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_17 = fabsf(((float)((float)L_15-(float)L_16)));
		float L_18 = __this->get_maxUPRot_5();
		float L_19 = __this->get_xRot2_14();
		float L_20 = fabsf(((float)((float)L_18-(float)L_19)));
		G_B5_0 = __this;
		if ((!(((float)L_17) < ((float)L_20))))
		{
			G_B6_0 = __this;
			goto IL_00d2;
		}
	}
	{
		float L_21 = __this->get_maxDownRot_6();
		G_B7_0 = ((float)((float)L_21*(float)(0.9f)));
		G_B7_1 = G_B5_0;
		goto IL_00de;
	}

IL_00d2:
	{
		float L_22 = __this->get_maxUPRot_5();
		G_B7_0 = ((float)((float)L_22*(float)(0.9f)));
		G_B7_1 = G_B6_0;
	}

IL_00de:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_newX_12(G_B7_0);
		float L_23 = __this->get_xRot2_14();
		float L_24 = __this->get_maxDownRot_6();
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_0105;
		}
	}
	{
		float L_25 = __this->get_xRot2_14();
		float L_26 = __this->get_maxUPRot_5();
		if ((((float)L_25) < ((float)L_26)))
		{
			goto IL_0127;
		}
	}

IL_0105:
	{
		float L_27 = __this->get_xRot2_14();
		float L_28 = __this->get_maxUPRot_5();
		if ((!(((float)L_27) < ((float)L_28))))
		{
			goto IL_018b;
		}
	}
	{
		float L_29 = __this->get_xRot2_14();
		float L_30 = __this->get_maxDownRot_6();
		if ((!(((float)L_29) > ((float)L_30))))
		{
			goto IL_018b;
		}
	}

IL_0127:
	{
		Transform_t1659122786 * L_31 = __this->get_headTrans_3();
		float L_32 = __this->get_newX_12();
		Vector3_t4282066566 * L_33 = __this->get_address_of_headRot2_20();
		float L_34 = L_33->get_y_2();
		Vector3_t4282066566 * L_35 = __this->get_address_of_headRot2_20();
		float L_36 = L_35->get_z_3();
		Quaternion_t1553702882  L_37 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, L_32, L_34, L_36, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_rotation_m1525803229(L_31, L_37, /*hidden argument*/NULL);
		TextMesh_t2567681854 * L_38 = __this->get_cBHeadRotTM_4();
		Color_t4194546905  L_39 = Color_get_blue_m3657252170(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		TextMesh_set_color_m115266575(L_38, L_39, /*hidden argument*/NULL);
		TextMesh_t2567681854 * L_40 = __this->get_cBHeadRotTM_4();
		Transform_t1659122786 * L_41 = __this->get_headTrans_3();
		NullCheck(L_41);
		Vector3_t4282066566  L_42 = Transform_get_eulerAngles_m1058084741(L_41, /*hidden argument*/NULL);
		V_1 = L_42;
		String_t* L_43 = Vector3_ToString_m3566373060((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1966836989, L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		TextMesh_set_text_m3628430759(L_40, L_44, /*hidden argument*/NULL);
	}

IL_018b:
	{
		return;
	}
}
// System.Void VRCameraClamper::EnableGyro()
extern "C"  void VRCameraClamper_EnableGyro_m3968714482 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VRCameraClamper::ActivateModeSwitchBtn(System.Boolean)
extern "C"  void VRCameraClamper_ActivateModeSwitchBtn_m1132214251 (VRCameraClamper_t2660686439 * __this, bool ___b0, const MethodInfo* method)
{
	{
		UITexture_t3903132647 * L_0 = __this->get_vrControlModeBtn_23();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		bool L_2 = ___b0;
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRCameraClamper::EnableSwipeNavigation(System.Boolean)
extern const MethodInfo* Component_GetComponentInChildren_TisTouchController_t2155287579_m1612592602_MethodInfo_var;
extern const uint32_t VRCameraClamper_EnableSwipeNavigation_m33859812_MetadataUsageId;
extern "C"  void VRCameraClamper_EnableSwipeNavigation_m33859812 (VRCameraClamper_t2660686439 * __this, bool ___b0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRCameraClamper_EnableSwipeNavigation_m33859812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_enableGyro_24();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		TouchController_t2155287579 * L_1 = Component_GetComponentInChildren_TisTouchController_t2155287579_m1612592602(__this, /*hidden argument*/Component_GetComponentInChildren_TisTouchController_t2155287579_m1612592602_MethodInfo_var);
		bool L_2 = ___b0;
		NullCheck(L_1);
		Behaviour_set_enabled_m2046806933(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single VRCameraClamper::GetLimit(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t VRCameraClamper_GetLimit_m2552387954_MetadataUsageId;
extern "C"  float VRCameraClamper_GetLimit_m2552387954 (VRCameraClamper_t2660686439 * __this, float ___currentX0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRCameraClamper_GetLimit_m2552387954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = 0.0f;
	{
		float L_0 = __this->get_maxDownRot_6();
		float L_1 = ___currentX0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = __this->get_maxUPRot_5();
		float L_4 = ___currentX0;
		float L_5 = fabsf(((float)((float)L_3-(float)L_4)));
		if ((!(((float)L_2) < ((float)L_5))))
		{
			goto IL_002a;
		}
	}
	{
		float L_6 = __this->get_maxDownRot_6();
		G_B3_0 = L_6;
		goto IL_0030;
	}

IL_002a:
	{
		float L_7 = __this->get_maxUPRot_5();
		G_B3_0 = L_7;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Void VRCameraClamper::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3823541369;
extern const uint32_t VRCameraClamper_Update_m1561411241_MetadataUsageId;
extern "C"  void VRCameraClamper_Update_m1561411241 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRCameraClamper_Update_m1561411241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		bool L_1 = __this->get_doubleTapZoom_11();
		if (!L_1)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3635654872* L_2 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		__this->set_t_26((*(Touch_t4210255029 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
		Touch_t4210255029 * L_3 = __this->get_address_of_t_26();
		int32_t L_4 = Touch_get_phase_m3314549414(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_0075;
		}
	}
	{
		uint8_t L_5 = __this->get_tapCount_25();
		__this->set_tapCount_25((((int32_t)((uint8_t)((int32_t)((int32_t)L_5+(int32_t)1))))));
		uint8_t L_6 = __this->get_tapCount_25();
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_005f;
		}
	}
	{
		VRCameraClamper_Zoom_m3496135411(__this, /*hidden argument*/NULL);
		return;
	}

IL_005f:
	{
		MonoBehaviour_CancelInvoke_m3230208631(__this, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3823541369, (0.3f), /*hidden argument*/NULL);
	}

IL_0075:
	{
		goto IL_00ff;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_7 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_00ff;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3635654872* L_8 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		__this->set_t1_27((*(Touch_t4210255029 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
		TouchU5BU5D_t3635654872* L_9 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		__this->set_t2_28((*(Touch_t4210255029 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))));
		Touch_t4210255029 * L_10 = __this->get_address_of_t1_27();
		int32_t L_11 = Touch_get_phase_m3314549414(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_00d4;
		}
	}
	{
		Touch_t4210255029 * L_12 = __this->get_address_of_t2_28();
		int32_t L_13 = Touch_get_phase_m3314549414(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) == ((int32_t)1)))
		{
			goto IL_00d4;
		}
	}
	{
		return;
	}

IL_00d4:
	{
		Touch_t4210255029 * L_14 = __this->get_address_of_t1_27();
		Vector2_t4282066565  L_15 = Touch_get_position_m1943849441(L_14, /*hidden argument*/NULL);
		Vector3_t4282066566  L_16 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Touch_t4210255029 * L_17 = __this->get_address_of_t2_28();
		Vector2_t4282066565  L_18 = Touch_get_position_m1943849441(L_17, /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_20 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		VRCameraClamper_SmoothZoom_m2419153994(__this, L_20, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		return;
	}
}
// System.Void VRCameraClamper::SetZoom(System.Boolean)
extern "C"  void VRCameraClamper_SetZoom_m4294401294 (VRCameraClamper_t2660686439 * __this, bool ___pZoom0, const MethodInfo* method)
{
	Camera_t2727095145 * G_B2_0 = NULL;
	Camera_t2727095145 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Camera_t2727095145 * G_B3_1 = NULL;
	VRCameraClamper_t2660686439 * G_B5_0 = NULL;
	VRCameraClamper_t2660686439 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	VRCameraClamper_t2660686439 * G_B6_1 = NULL;
	VRCameraClamper_t2660686439 * G_B8_0 = NULL;
	VRCameraClamper_t2660686439 * G_B7_0 = NULL;
	float G_B9_0 = 0.0f;
	VRCameraClamper_t2660686439 * G_B9_1 = NULL;
	{
		bool L_0 = ___pZoom0;
		__this->set_zoomIn_29(L_0);
		Camera_t2727095145 * L_1 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = __this->get_zoomIn_29();
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = ((int32_t)27);
		G_B3_1 = G_B1_0;
		goto IL_0020;
	}

IL_001e:
	{
		G_B3_0 = ((int32_t)60);
		G_B3_1 = G_B2_0;
	}

IL_0020:
	{
		NullCheck(G_B3_1);
		Camera_set_fieldOfView_m809388684(G_B3_1, (((float)((float)G_B3_0))), /*hidden argument*/NULL);
		bool L_3 = __this->get_zoomIn_29();
		G_B4_0 = __this;
		if (!L_3)
		{
			G_B5_0 = __this;
			goto IL_003c;
		}
	}
	{
		G_B6_0 = (325.0f);
		G_B6_1 = G_B4_0;
		goto IL_0042;
	}

IL_003c:
	{
		float L_4 = __this->get_originalMaxUpRot_16();
		G_B6_0 = L_4;
		G_B6_1 = G_B5_0;
	}

IL_0042:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_maxUPRot_5(G_B6_0);
		bool L_5 = __this->get_zoomIn_29();
		G_B7_0 = __this;
		if (!L_5)
		{
			G_B8_0 = __this;
			goto IL_005d;
		}
	}
	{
		G_B9_0 = (35.0f);
		G_B9_1 = G_B7_0;
		goto IL_0063;
	}

IL_005d:
	{
		float L_6 = __this->get_originalMaxDownRot_17();
		G_B9_0 = L_6;
		G_B9_1 = G_B8_0;
	}

IL_0063:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_maxDownRot_6(G_B9_0);
		return;
	}
}
// System.Void VRCameraClamper::ClearTapCount()
extern "C"  void VRCameraClamper_ClearTapCount_m1011363675 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	{
		__this->set_tapCount_25(0);
		return;
	}
}
// System.Void VRCameraClamper::SmoothZoom(System.Single)
extern "C"  void VRCameraClamper_SmoothZoom_m2419153994 (VRCameraClamper_t2660686439 * __this, float ___distance0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___distance0;
		if ((!(((float)L_0) > ((float)(700.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		___distance0 = (700.0f);
	}

IL_0012:
	{
		float L_1 = ___distance0;
		if ((!(((float)L_1) < ((float)(200.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		___distance0 = (200.0f);
	}

IL_0024:
	{
		float L_2 = ___distance0;
		___distance0 = ((float)((float)L_2-(float)(200.0f)));
		float L_3 = ___distance0;
		V_0 = ((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_3/(float)(500.0f)))))*(float)(33.0f)))+(float)(27.0f)));
		Camera_t2727095145 * L_4 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = V_0;
		NullCheck(L_4);
		Camera_set_fieldOfView_m809388684(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRCameraClamper::Zoom()
extern "C"  void VRCameraClamper_Zoom_m3496135411 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	Camera_t2727095145 * G_B2_0 = NULL;
	Camera_t2727095145 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Camera_t2727095145 * G_B3_1 = NULL;
	VRCameraClamper_t2660686439 * G_B5_0 = NULL;
	VRCameraClamper_t2660686439 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	VRCameraClamper_t2660686439 * G_B6_1 = NULL;
	VRCameraClamper_t2660686439 * G_B8_0 = NULL;
	VRCameraClamper_t2660686439 * G_B7_0 = NULL;
	float G_B9_0 = 0.0f;
	VRCameraClamper_t2660686439 * G_B9_1 = NULL;
	{
		MonoBehaviour_CancelInvoke_m3230208631(__this, /*hidden argument*/NULL);
		VRCameraClamper_ClearTapCount_m1011363675(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_zoomIn_29();
		__this->set_zoomIn_29((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		Camera_t2727095145 * L_1 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = __this->get_zoomIn_29();
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0032;
		}
	}
	{
		G_B3_0 = ((int32_t)27);
		G_B3_1 = G_B1_0;
		goto IL_0034;
	}

IL_0032:
	{
		G_B3_0 = ((int32_t)60);
		G_B3_1 = G_B2_0;
	}

IL_0034:
	{
		NullCheck(G_B3_1);
		Camera_set_fieldOfView_m809388684(G_B3_1, (((float)((float)G_B3_0))), /*hidden argument*/NULL);
		bool L_3 = __this->get_zoomIn_29();
		G_B4_0 = __this;
		if (!L_3)
		{
			G_B5_0 = __this;
			goto IL_0050;
		}
	}
	{
		G_B6_0 = (325.0f);
		G_B6_1 = G_B4_0;
		goto IL_0056;
	}

IL_0050:
	{
		float L_4 = __this->get_originalMaxUpRot_16();
		G_B6_0 = L_4;
		G_B6_1 = G_B5_0;
	}

IL_0056:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_maxUPRot_5(G_B6_0);
		bool L_5 = __this->get_zoomIn_29();
		G_B7_0 = __this;
		if (!L_5)
		{
			G_B8_0 = __this;
			goto IL_0071;
		}
	}
	{
		G_B9_0 = (35.0f);
		G_B9_1 = G_B7_0;
		goto IL_0077;
	}

IL_0071:
	{
		float L_6 = __this->get_originalMaxDownRot_17();
		G_B9_0 = L_6;
		G_B9_1 = G_B8_0;
	}

IL_0077:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_maxDownRot_6(G_B9_0);
		return;
	}
}
// System.Void VRCameraClamper::LateUpdate()
extern Il2CppClass* Cardboard_t1761541558_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1966836989;
extern const uint32_t VRCameraClamper_LateUpdate_m2447476719_MetadataUsageId;
extern "C"  void VRCameraClamper_LateUpdate_m2447476719 (VRCameraClamper_t2660686439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRCameraClamper_LateUpdate_m2447476719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_updateRotation_8();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t1761541558_il2cpp_TypeInfo_var);
		Cardboard_t1761541558 * L_1 = Cardboard_get_SDK_m2351368065(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Pose3D_t2396367586 * L_2 = Cardboard_get_HeadPose_m2115147790(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t1553702882  L_3 = Pose3D_get_Orientation_m114671670(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t4282066566  L_4 = Quaternion_get_eulerAngles_m997303795((&V_0), /*hidden argument*/NULL);
		__this->set_headRot_19(L_4);
		Vector3_t4282066566 * L_5 = __this->get_address_of_headRot_19();
		float L_6 = L_5->get_x_1();
		__this->set_xRot_13(L_6);
		float L_7 = __this->get_xRot_13();
		float L_8 = __this->get_maxDownRot_6();
		if ((!(((float)L_7) < ((float)L_8))))
		{
			goto IL_005b;
		}
	}
	{
		float L_9 = __this->get_xRot_13();
		if ((((float)L_9) > ((float)(0.0f))))
		{
			goto IL_007c;
		}
	}

IL_005b:
	{
		float L_10 = __this->get_xRot_13();
		float L_11 = __this->get_maxUPRot_5();
		if ((!(((float)L_10) > ((float)L_11))))
		{
			goto IL_0098;
		}
	}
	{
		float L_12 = __this->get_xRot_13();
		if ((!(((float)L_12) < ((float)(360.1f)))))
		{
			goto IL_0098;
		}
	}

IL_007c:
	{
		__this->set_updateRotation_8((bool)1);
		TextMesh_t2567681854 * L_13 = __this->get_cBHeadRotTM_4();
		Color_t4194546905  L_14 = Color_get_green_m2005284533(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		TextMesh_set_color_m115266575(L_13, L_14, /*hidden argument*/NULL);
		goto IL_00ed;
	}

IL_0098:
	{
		float L_15 = __this->get_xRot_13();
		float L_16 = VRCameraClamper_GetLimit_m2552387954(__this, L_15, /*hidden argument*/NULL);
		__this->set_xLimit_15(L_16);
		float L_17 = __this->get_xLimit_15();
		Vector3_t4282066566 * L_18 = __this->get_address_of_headRot_19();
		float L_19 = L_18->get_y_2();
		Vector3_t4282066566 * L_20 = __this->get_address_of_headRot_19();
		float L_21 = L_20->get_z_3();
		Vector3_t4282066566  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2926210380(&L_22, L_17, L_19, L_21, /*hidden argument*/NULL);
		__this->set_newVector_18(L_22);
		Vector3_t4282066566  L_23 = __this->get_newVector_18();
		__this->set_headRot_19(L_23);
		TextMesh_t2567681854 * L_24 = __this->get_cBHeadRotTM_4();
		Color_t4194546905  L_25 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		TextMesh_set_color_m115266575(L_24, L_25, /*hidden argument*/NULL);
	}

IL_00ed:
	{
		bool L_26 = __this->get_updateRotation_8();
		if (!L_26)
		{
			goto IL_014f;
		}
	}
	{
		bool L_27 = __this->get_SmoothRotation_9();
		if (!L_27)
		{
			goto IL_0139;
		}
	}
	{
		Transform_t1659122786 * L_28 = __this->get_headTrans_3();
		Transform_t1659122786 * L_29 = __this->get_headTrans_3();
		NullCheck(L_29);
		Quaternion_t1553702882  L_30 = Transform_get_rotation_m11483428(L_29, /*hidden argument*/NULL);
		Vector3_t4282066566  L_31 = __this->get_headRot_19();
		Quaternion_t1553702882  L_32 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		float L_33 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_34 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_30, L_32, ((float)((float)L_33*(float)(8.0f))), /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_rotation_m1525803229(L_28, L_34, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_0139:
	{
		Transform_t1659122786 * L_35 = __this->get_headTrans_3();
		Vector3_t4282066566  L_36 = __this->get_headRot_19();
		Quaternion_t1553702882  L_37 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_set_rotation_m1525803229(L_35, L_37, /*hidden argument*/NULL);
	}

IL_014f:
	{
		bool L_38 = __this->get_debugRotation_7();
		if (L_38)
		{
			goto IL_015b;
		}
	}
	{
		return;
	}

IL_015b:
	{
		TextMesh_t2567681854 * L_39 = __this->get_cBHeadRotTM_4();
		Transform_t1659122786 * L_40 = __this->get_headTrans_3();
		NullCheck(L_40);
		Vector3_t4282066566  L_41 = Transform_get_eulerAngles_m1058084741(L_40, /*hidden argument*/NULL);
		V_1 = L_41;
		String_t* L_42 = Vector3_ToString_m3566373060((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1966836989, L_42, /*hidden argument*/NULL);
		NullCheck(L_39);
		TextMesh_set_text_m3628430759(L_39, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::.ctor()
extern "C"  void VRIntegrationHelper__ctor_m3423509221 (VRIntegrationHelper_t1694779206 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::.cctor()
extern "C"  void VRIntegrationHelper__cctor_m2567474536 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VRIntegrationHelper::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m342242277_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_Awake_m3661114440_MetadataUsageId;
extern "C"  void VRIntegrationHelper_Awake_m3661114440 (VRIntegrationHelper_t1694779206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_Awake_m3661114440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m342242277(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m342242277_MethodInfo_var);
		NullCheck(L_0);
		Camera_set_fieldOfView_m809388684(L_0, (90.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::Start()
extern Il2CppClass* VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* VRIntegrationHelper_OnVuforiaStarted_m1418148573_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_Start_m2370647013_MetadataUsageId;
extern "C"  void VRIntegrationHelper_Start_m2370647013 (VRIntegrationHelper_t1694779206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_Start_m2370647013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_0 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)VRIntegrationHelper_OnVuforiaStarted_m1418148573_MethodInfo_var);
		Action_t3771233898 * L_2 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaAbstractBehaviour_RegisterVuforiaStartedCallback_m2995837188(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::OnVuforiaStarted()
extern Il2CppClass* VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var;
extern Il2CppClass* VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t465046465_m2348691886_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_OnVuforiaStarted_m1418148573_MetadataUsageId;
extern "C"  void VRIntegrationHelper_OnVuforiaStarted_m1418148573 (VRIntegrationHelper_t1694779206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_OnVuforiaStarted_m1418148573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_0 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t2727095145 * L_1 = VuforiaAbstractBehaviour_get_PrimaryCamera_m612524469(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mLeftCamera_4(L_1);
		VuforiaBehaviour_t1845338141 * L_2 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_t2727095145 * L_3 = VuforiaAbstractBehaviour_get_SecondaryCamera_m3360373735(L_2, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mRightCamera_5(L_3);
		Camera_t2727095145 * L_4 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_4);
		HideExcessAreaAbstractBehaviour_t465046465 * L_5 = Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t465046465_m2348691886(L_4, /*hidden argument*/Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t465046465_m2348691886_MethodInfo_var);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mLeftExcessAreaBehaviour_6(L_5);
		Camera_t2727095145 * L_6 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_6);
		HideExcessAreaAbstractBehaviour_t465046465 * L_7 = Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t465046465_m2348691886(L_6, /*hidden argument*/Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t465046465_m2348691886_MethodInfo_var);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mRightExcessAreaBehaviour_7(L_7);
		return;
	}
}
// System.Void VRIntegrationHelper::LateUpdate()
extern Il2CppClass* VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t1927425041_m2233635214_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_LateUpdate_m1453110926_MetadataUsageId;
extern "C"  void VRIntegrationHelper_LateUpdate_m1453110926 (VRIntegrationHelper_t1694779206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_LateUpdate_m1453110926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	BackgroundPlaneBehaviour_t1927425041 * V_6 = NULL;
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		bool L_0 = __this->get_IsLeft_12();
		if (!L_0)
		{
			goto IL_02a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		bool L_1 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraDataAcquired_10();
		if (!L_1)
		{
			goto IL_02a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		bool L_2 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraDataAcquired_11();
		if (!L_2)
		{
			goto IL_02a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_3 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = VuforiaAbstractBehaviour_get_CentralAnchorPoint_m4230245087(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		Camera_t2727095145 * L_5 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Transform_get_localRotation_m3343229381(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_m3719981474(L_4, L_7, /*hidden argument*/NULL);
		VuforiaBehaviour_t1845338141 * L_8 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = VuforiaAbstractBehaviour_get_CentralAnchorPoint_m4230245087(L_8, /*hidden argument*/NULL);
		Camera_t2727095145 * L_10 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = Component_get_transform_m4257140443(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Transform_get_localPosition_m668140784(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localPosition_m3504330903(L_9, L_12, /*hidden argument*/NULL);
		Camera_t2727095145 * L_13 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = Component_get_transform_m4257140443(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_localPosition_m668140784(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		Camera_t2727095145 * L_16 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_16);
		Rect_t4241904616  L_17 = Camera_get_pixelRect_m936851539(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Camera_t2727095145 * L_18 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_right_m2070836824(L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		Vector3_t4282066566  L_21 = Vector3_get_normalized_m2650940353((&V_7), /*hidden argument*/NULL);
		Camera_t2727095145 * L_22 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_22);
		float L_23 = Camera_get_stereoSeparation_m743605551(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_24, (-0.5f), /*hidden argument*/NULL);
		V_2 = L_25;
		Camera_t2727095145 * L_26 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = Component_get_transform_m4257140443(L_26, /*hidden argument*/NULL);
		Camera_t2727095145 * L_28 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = Component_get_transform_m4257140443(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t4282066566  L_30 = Transform_get_position_m2211398607(L_29, /*hidden argument*/NULL);
		Vector3_t4282066566  L_31 = V_2;
		Vector3_t4282066566  L_32 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m3111394108(L_27, L_32, /*hidden argument*/NULL);
		Camera_t2727095145 * L_33 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		Rect_t4241904616  L_34 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraPixelRect_8();
		NullCheck(L_33);
		Camera_set_pixelRect_m1891083544(L_33, L_34, /*hidden argument*/NULL);
		Camera_t2727095145 * L_35 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = Component_get_transform_m4257140443(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t4282066566  L_37 = Transform_get_localPosition_m668140784(L_36, /*hidden argument*/NULL);
		V_3 = L_37;
		Camera_t2727095145 * L_38 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_38);
		Rect_t4241904616  L_39 = Camera_get_pixelRect_m936851539(L_38, /*hidden argument*/NULL);
		V_4 = L_39;
		Camera_t2727095145 * L_40 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = Component_get_transform_m4257140443(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4282066566  L_42 = Transform_get_right_m2070836824(L_41, /*hidden argument*/NULL);
		V_8 = L_42;
		Vector3_t4282066566  L_43 = Vector3_get_normalized_m2650940353((&V_8), /*hidden argument*/NULL);
		Camera_t2727095145 * L_44 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_44);
		float L_45 = Camera_get_stereoSeparation_m743605551(L_44, /*hidden argument*/NULL);
		Vector3_t4282066566  L_46 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		Vector3_t4282066566  L_47 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_46, (0.5f), /*hidden argument*/NULL);
		V_5 = L_47;
		Camera_t2727095145 * L_48 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_48);
		Transform_t1659122786 * L_49 = Component_get_transform_m4257140443(L_48, /*hidden argument*/NULL);
		Camera_t2727095145 * L_50 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = Component_get_transform_m4257140443(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t4282066566  L_52 = Transform_get_position_m2211398607(L_51, /*hidden argument*/NULL);
		Vector3_t4282066566  L_53 = V_5;
		Vector3_t4282066566  L_54 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_position_m3111394108(L_49, L_54, /*hidden argument*/NULL);
		Camera_t2727095145 * L_55 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		Rect_t4241904616  L_56 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraPixelRect_9();
		NullCheck(L_55);
		Camera_set_pixelRect_m1891083544(L_55, L_56, /*hidden argument*/NULL);
		Camera_t2727095145 * L_57 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_57);
		BackgroundPlaneBehaviour_t1927425041 * L_58 = Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t1927425041_m2233635214(L_57, /*hidden argument*/Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t1927425041_m2233635214_MethodInfo_var);
		V_6 = L_58;
		BackgroundPlaneBehaviour_t1927425041 * L_59 = V_6;
		Camera_t2727095145 * L_60 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_60);
		Transform_t1659122786 * L_61 = Component_get_transform_m4257140443(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t4282066566  L_62 = Transform_get_position_m2211398607(L_61, /*hidden argument*/NULL);
		VuforiaBehaviour_t1845338141 * L_63 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_t1659122786 * L_64 = VuforiaAbstractBehaviour_get_CentralAnchorPoint_m4230245087(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t4282066566  L_65 = Transform_get_position_m2211398607(L_64, /*hidden argument*/NULL);
		Vector3_t4282066566  L_66 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_62, L_65, /*hidden argument*/NULL);
		NullCheck(L_59);
		BackgroundPlaneAbstractBehaviour_set_BackgroundOffset_m3367168245(L_59, L_66, /*hidden argument*/NULL);
		HideExcessAreaAbstractBehaviour_t465046465 * L_67 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftExcessAreaBehaviour_6();
		Camera_t2727095145 * L_68 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_68);
		Transform_t1659122786 * L_69 = Component_get_transform_m4257140443(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Vector3_t4282066566  L_70 = Transform_get_position_m2211398607(L_69, /*hidden argument*/NULL);
		VuforiaBehaviour_t1845338141 * L_71 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_t1659122786 * L_72 = VuforiaAbstractBehaviour_get_CentralAnchorPoint_m4230245087(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t4282066566  L_73 = Transform_get_position_m2211398607(L_72, /*hidden argument*/NULL);
		Vector3_t4282066566  L_74 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		NullCheck(L_67);
		HideExcessAreaAbstractBehaviour_set_PlaneOffset_m3511314981(L_67, L_74, /*hidden argument*/NULL);
		HideExcessAreaAbstractBehaviour_t465046465 * L_75 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightExcessAreaBehaviour_7();
		Camera_t2727095145 * L_76 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_76);
		Transform_t1659122786 * L_77 = Component_get_transform_m4257140443(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3_t4282066566  L_78 = Transform_get_position_m2211398607(L_77, /*hidden argument*/NULL);
		VuforiaBehaviour_t1845338141 * L_79 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_79);
		Transform_t1659122786 * L_80 = VuforiaAbstractBehaviour_get_CentralAnchorPoint_m4230245087(L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t4282066566  L_81 = Transform_get_position_m2211398607(L_80, /*hidden argument*/NULL);
		Vector3_t4282066566  L_82 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_78, L_81, /*hidden argument*/NULL);
		NullCheck(L_75);
		HideExcessAreaAbstractBehaviour_set_PlaneOffset_m3511314981(L_75, L_82, /*hidden argument*/NULL);
		Transform_t1659122786 * L_83 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_83, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_020f;
		}
	}
	{
		Transform_t1659122786 * L_85 = __this->get_TrackableParent_13();
		Vector3_t4282066566  L_86 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_85);
		Transform_set_localPosition_m3504330903(L_85, L_86, /*hidden argument*/NULL);
	}

IL_020f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_87 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_87);
		VuforiaAbstractBehaviour_UpdateState_m3936241742(L_87, (bool)0, (bool)1, /*hidden argument*/NULL);
		Transform_t1659122786 * L_88 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_88, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0249;
		}
	}
	{
		Transform_t1659122786 * L_90 = __this->get_TrackableParent_13();
		Transform_t1659122786 * L_91 = L_90;
		NullCheck(L_91);
		Vector3_t4282066566  L_92 = Transform_get_position_m2211398607(L_91, /*hidden argument*/NULL);
		BackgroundPlaneBehaviour_t1927425041 * L_93 = V_6;
		NullCheck(L_93);
		Vector3_t4282066566  L_94 = BackgroundPlaneAbstractBehaviour_get_BackgroundOffset_m778610282(L_93, /*hidden argument*/NULL);
		Vector3_t4282066566  L_95 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_92, L_94, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_set_position_m3111394108(L_91, L_95, /*hidden argument*/NULL);
	}

IL_0249:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_96 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		Matrix4x4_t1651859333  L_97 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraMatrixOriginal_2();
		NullCheck(L_96);
		VuforiaAbstractBehaviour_ApplyCorrectedProjectionMatrix_m3495720800(L_96, L_97, (bool)1, /*hidden argument*/NULL);
		VuforiaBehaviour_t1845338141 * L_98 = VuforiaBehaviour_get_Instance_m2318468804(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_99 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraMatrixOriginal_3();
		NullCheck(L_98);
		VuforiaAbstractBehaviour_ApplyCorrectedProjectionMatrix_m3495720800(L_98, L_99, (bool)0, /*hidden argument*/NULL);
		Camera_t2727095145 * L_100 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_100);
		Transform_t1659122786 * L_101 = Component_get_transform_m4257140443(L_100, /*hidden argument*/NULL);
		Vector3_t4282066566  L_102 = V_0;
		NullCheck(L_101);
		Transform_set_localPosition_m3504330903(L_101, L_102, /*hidden argument*/NULL);
		Camera_t2727095145 * L_103 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		Rect_t4241904616  L_104 = V_1;
		NullCheck(L_103);
		Camera_set_pixelRect_m1891083544(L_103, L_104, /*hidden argument*/NULL);
		Camera_t2727095145 * L_105 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_105);
		Transform_t1659122786 * L_106 = Component_get_transform_m4257140443(L_105, /*hidden argument*/NULL);
		Vector3_t4282066566  L_107 = V_3;
		NullCheck(L_106);
		Transform_set_localPosition_m3504330903(L_106, L_107, /*hidden argument*/NULL);
		Camera_t2727095145 * L_108 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		Rect_t4241904616  L_109 = V_4;
		NullCheck(L_108);
		Camera_set_pixelRect_m1891083544(L_108, L_109, /*hidden argument*/NULL);
	}

IL_02a0:
	{
		return;
	}
}
// System.Void VRIntegrationHelper::OnPreRender()
extern Il2CppClass* VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var;
extern const uint32_t VRIntegrationHelper_OnPreRender_m2251470589_MetadataUsageId;
extern "C"  void VRIntegrationHelper_OnPreRender_m2251470589 (VRIntegrationHelper_t1694779206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_OnPreRender_m2251470589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_IsLeft_12();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		bool L_1 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraDataAcquired_10();
		if (L_1)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		Camera_t2727095145 * L_2 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_2);
		Matrix4x4_t1651859333  L_3 = Camera_get_projectionMatrix_m3070982480(L_2, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mLeftCameraMatrixOriginal_2(L_3);
		Camera_t2727095145 * L_4 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_4);
		Rect_t4241904616  L_5 = Camera_get_pixelRect_m936851539(L_4, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mLeftCameraPixelRect_8(L_5);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mLeftCameraDataAcquired_10((bool)1);
		goto IL_006c;
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		bool L_6 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraDataAcquired_11();
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var);
		Camera_t2727095145 * L_7 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_7);
		Matrix4x4_t1651859333  L_8 = Camera_get_projectionMatrix_m3070982480(L_7, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mRightCameraMatrixOriginal_3(L_8);
		Camera_t2727095145 * L_9 = ((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_9);
		Rect_t4241904616  L_10 = Camera_get_pixelRect_m936851539(L_9, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mRightCameraPixelRect_9(L_10);
		((VRIntegrationHelper_t1694779206_StaticFields*)VRIntegrationHelper_t1694779206_il2cpp_TypeInfo_var->static_fields)->set_mRightCameraDataAcquired_11((bool)1);
	}

IL_006c:
	{
		return;
	}
}
// System.Void VRModeManager::.ctor()
extern "C"  void VRModeManager__ctor_m2257716477 (VRModeManager_t3340808238 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRModeManager::Awake()
extern Il2CppClass* GlossaryMgr_t3182292730_il2cpp_TypeInfo_var;
extern Il2CppClass* SkyboxHelper_t1246069112_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m935595982_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3328575232;
extern const uint32_t VRModeManager_Awake_m2495321696_MetadataUsageId;
extern "C"  void VRModeManager_Awake_m2495321696 (VRModeManager_t3340808238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRModeManager_Awake_m2495321696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_returnToGlossaryBtn_5();
		IL2CPP_RUNTIME_CLASS_INIT(GlossaryMgr_t3182292730_il2cpp_TypeInfo_var);
		List_1_t1375417109 * L_1 = ((GlossaryMgr_t3182292730_StaticFields*)GlossaryMgr_t3182292730_il2cpp_TypeInfo_var->static_fields)->get_vrTargetsToShow_10();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m935595982(L_1, /*hidden argument*/List_1_get_Count_m935595982_MethodInfo_var);
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_3 = __this->get_loadSkybox_4();
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral3328575232, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SkyboxHelper_t1246069112_il2cpp_TypeInfo_var);
		SkyboxHelper_LoadFromResources_m3614614794(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void VRModeManager::SetVROnSplitMode()
extern "C"  void VRModeManager_SetVROnSplitMode_m2901616359 (VRModeManager_t3340808238 * __this, const MethodInfo* method)
{
	{
		VRModeManager_EnableVR_m2368246877(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRModeManager::SetVROnFullMode()
extern "C"  void VRModeManager_SetVROnFullMode_m3362341610 (VRModeManager_t3340808238 * __this, const MethodInfo* method)
{
	{
		VRModeManager_EnableVR_m2368246877(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRModeManager::EnableVR(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisCardboard_t1761541558_m954238187_MethodInfo_var;
extern const uint32_t VRModeManager_EnableVR_m2368246877_MetadataUsageId;
extern "C"  void VRModeManager_EnableVR_m2368246877 (VRModeManager_t3340808238 * __this, bool ___enableSplitMode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VRModeManager_EnableVR_m2368246877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_vrContainer_3();
		NullCheck(L_0);
		Cardboard_t1761541558 * L_1 = GameObject_GetComponent_TisCardboard_t1761541558_m954238187(L_0, /*hidden argument*/GameObject_GetComponent_TisCardboard_t1761541558_m954238187_MethodInfo_var);
		bool L_2 = ___enableSplitMode0;
		NullCheck(L_1);
		Cardboard_set_VRModeEnabled_m3570037647(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_vrContainer_3();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = __this->get_blackQuad_2();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C"  void AndroidUnityPlayer__ctor_m2334148552 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C"  void AndroidUnityPlayer_LoadNativeLibraries_m1361526690 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m3781406894(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C"  void AndroidUnityPlayer_InitializePlatform_m1086235807 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m439238776(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern "C"  int32_t AndroidUnityPlayer_Start_m3846487503 (AndroidUnityPlayer_t1535493961 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey0;
		int32_t L_1 = AndroidUnityPlayer_InitVuforia_m608520152(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m1980877091(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern Il2CppClass* SurfaceUtilities_t743372383_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_Update_m1071023173_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_Update_m1071023173 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_Update_m1071023173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t743372383_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m3719711414(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m1980877091(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m1193220576(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m2025837220(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m971597220(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = __this->get_mFramesSinceLastOrientationReset_4();
		__this->set_mFramesSinceLastOrientationReset_4(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern "C"  void AndroidUnityPlayer_OnPause_m1337313597 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnPause_m3553757101(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern "C"  void AndroidUnityPlayer_OnResume_m1192602344 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnResume_m1182874232(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern "C"  void AndroidUnityPlayer_OnDestroy_m2121857217 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_Deinit_m3194148381(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C"  void AndroidUnityPlayer_Dispose_m1042113029 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C"  void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m3781406894 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C"  void AndroidUnityPlayer_InitAndroidPlatform_m439238776 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitVuforia(System.String)
extern "C"  int32_t AndroidUnityPlayer_InitVuforia_m608520152 (AndroidUnityPlayer_t1535493961 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern Il2CppClass* SurfaceUtilities_t743372383_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_InitializeSurface_m1980877091_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_InitializeSurface_m1980877091 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_InitializeSurface_m1980877091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t743372383_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m2755562960(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m2025837220(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m971597220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C"  void AndroidUnityPlayer_ResetUnityScreenOrientation_m2025837220 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m1193220576(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_2(L_0);
		__this->set_mFramesSinceLastOrientationReset_4(0);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern Il2CppClass* SurfaceUtilities_t743372383_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_CheckOrientation_m971597220_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_CheckOrientation_m971597220 (AndroidUnityPlayer_t1535493961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_CheckOrientation_m971597220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_mFramesSinceLastOrientationReset_4();
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = __this->get_mFramesSinceLastJavaOrientationCheck_5();
		V_0 = (bool)((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = __this->get_mScreenOrientation_2();
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = __this->get_mJavaScreenOrientation_3();
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->set_mJavaScreenOrientation_3(L_8);
		int32_t L_9 = __this->get_mJavaScreenOrientation_3();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t743372383_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m2671033300(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->set_mFramesSinceLastJavaOrientationCheck_5(0);
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = __this->get_mFramesSinceLastJavaOrientationCheck_5();
		__this->set_mFramesSinceLastJavaOrientationCheck_5(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern Il2CppClass* BackgroundPlaneAbstractBehaviour_t2608219151_il2cpp_TypeInfo_var;
extern const uint32_t BackgroundPlaneBehaviour__ctor_m3632419328_MetadataUsageId;
extern "C"  void BackgroundPlaneBehaviour__ctor_m3632419328 (BackgroundPlaneBehaviour_t1927425041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BackgroundPlaneBehaviour__ctor_m3632419328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t2608219151_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m738935102(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C"  void CloudRecoBehaviour__ctor_m1945181542 (CloudRecoBehaviour_t3993405419 * __this, const MethodInfo* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m2947617700(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C"  void ComponentFactoryStarterBehaviour__ctor_m207729076 (ComponentFactoryStarterBehaviour_t2489055709 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const Il2CppType* Action_t3771233898_0_0_0_var;
extern Il2CppClass* Attribute_t2523058482_il2cpp_TypeInfo_var;
extern Il2CppClass* FactorySetter_t2197853779_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1706594387_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m285312770_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1121202012_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2963716331_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3729693911_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m348843493_MethodInfo_var;
extern const uint32_t ComponentFactoryStarterBehaviour_Awake_m445334295_MetadataUsageId;
extern "C"  void ComponentFactoryStarterBehaviour_Awake_m445334295 (ComponentFactoryStarterBehaviour_t2489055709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactoryStarterBehaviour_Awake_m445334295_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1686921617 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Enumerator_t1706594387  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Attribute_t2523058482 * V_3 = NULL;
	ObjectU5BU5D_t1108656482* V_4 = NULL;
	int32_t V_5 = 0;
	Action_t3771233898 * V_6 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t2824366364* L_1 = VirtFuncInvoker1< MethodInfoU5BU5D_t2824366364*, int32_t >::Invoke(53 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t1686921617 * L_2 = Enumerable_ToList_TisMethodInfo_t_m285312770(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m285312770_MethodInfo_var);
		V_0 = L_2;
		List_1_t1686921617 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t2824366364* L_5 = VirtFuncInvoker1< MethodInfoU5BU5D_t2824366364*, int32_t >::Invoke(53 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m1121202012(L_3, (Il2CppObject*)(Il2CppObject*)L_5, /*hidden argument*/List_1_AddRange_m1121202012_MethodInfo_var);
		List_1_t1686921617 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t1706594387  L_7 = List_1_GetEnumerator_m2963716331(L_6, /*hidden argument*/List_1_GetEnumerator_m2963716331_MethodInfo_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m3729693911((&V_2), /*hidden argument*/Enumerator_get_Current_m3729693911_MethodInfo_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t1108656482* L_10 = VirtFuncInvoker1< ObjectU5BU5D_t1108656482*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, (bool)1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t1108656482* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
			int32_t L_13 = L_12;
			Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
			V_3 = ((Attribute_t2523058482 *)CastclassClass(L_14, Attribute_t2523058482_il2cpp_TypeInfo_var));
			Attribute_t2523058482 * L_15 = V_3;
			if (!((FactorySetter_t2197853779 *)IsInstClass(L_15, FactorySetter_t2197853779_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_16 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Action_t3771233898_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_17 = V_1;
			Delegate_t3310234105 * L_18 = Delegate_CreateDelegate_m3460497746(NULL /*static, unused*/, L_16, __this, L_17, /*hidden argument*/NULL);
			V_6 = ((Action_t3771233898 *)IsInstSealed(L_18, Action_t3771233898_il2cpp_TypeInfo_var));
			Action_t3771233898 * L_19 = V_6;
			if (!L_19)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t3771233898 * L_20 = V_6;
			NullCheck(L_20);
			Action_Invoke_m1445970038(L_20, /*hidden argument*/NULL);
		}

IL_0087:
		{
			int32_t L_21 = V_5;
			V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_22 = V_5;
			ObjectU5BU5D_t1108656482* L_23 = V_4;
			NullCheck(L_23);
			if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_24 = Enumerator_MoveNext_m348843493((&V_2), /*hidden argument*/Enumerator_MoveNext_m348843493_MethodInfo_var);
			if (L_24)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t1706594387  L_25 = V_2;
		Enumerator_t1706594387  L_26 = L_25;
		Il2CppObject * L_27 = Box(Enumerator_t1706594387_il2cpp_TypeInfo_var, &L_26);
		NullCheck((Il2CppObject *)L_27);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_27);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00b5:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaBehaviourComponentFactory_t900168586_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2180208154;
extern const uint32_t ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m2689784824_MetadataUsageId;
extern "C"  void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m2689784824 (ComponentFactoryStarterBehaviour_t2489055709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m2689784824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2180208154, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t900168586 * L_0 = (VuforiaBehaviourComponentFactory_t900168586 *)il2cpp_codegen_object_new(VuforiaBehaviourComponentFactory_t900168586_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m3340288679(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m2067262968(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C"  void CylinderTargetBehaviour__ctor_m4187651361 (CylinderTargetBehaviour_t1850077856 * __this, const MethodInfo* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m1578473703(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DatabaseLoadBehaviour::.ctor()
extern "C"  void DatabaseLoadBehaviour__ctor_m1540652955 (DatabaseLoadBehaviour_t2989373670 * __this, const MethodInfo* method)
{
	{
		DatabaseLoadAbstractBehaviour__ctor_m2623435361(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DatabaseLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void DatabaseLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m3933877709 (DatabaseLoadBehaviour_t2989373670 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DefaultInitializationErrorHandler__ctor_m2102149684_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler__ctor_m2102149684 (DefaultInitializationErrorHandler_t3746290221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler__ctor_m2102149684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mErrorText_3(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const Il2CppType* VuforiaAbstractBehaviour_t1091759131_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaAbstractBehaviour_t1091759131_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t828034096_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2793021901_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m626199895_MethodInfo_var;
extern const uint32_t DefaultInitializationErrorHandler_Awake_m2339754903_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_Awake_m2339754903 (DefaultInitializationErrorHandler_t3746290221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_Awake_m2339754903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t1091759131 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t1091759131_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t1091759131 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t1091759131_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t1091759131 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		VuforiaAbstractBehaviour_t1091759131 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2793021901_MethodInfo_var);
		Action_1_t828034096 * L_6 = (Action_1_t828034096 *)il2cpp_codegen_object_new(Action_1_t828034096_il2cpp_TypeInfo_var);
		Action_1__ctor_m626199895(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m626199895_MethodInfo_var);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_RegisterVuforiaInitErrorCallback_m2457484226(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern Il2CppClass* WindowFunction_t2749288659_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_DrawWindowContent_m1550878440_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral921186246;
extern const uint32_t DefaultInitializationErrorHandler_OnGUI_m1597548334_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_OnGUI_m1597548334 (DefaultInitializationErrorHandler_t3746290221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnGUI_m1597548334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_mErrorOccurred_4();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m3291325233(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_DrawWindowContent_m1550878440_MethodInfo_var);
		WindowFunction_t2749288659 * L_5 = (WindowFunction_t2749288659 *)il2cpp_codegen_object_new(WindowFunction_t2749288659_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m732638321(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Window_m2314976695(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral921186246, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const Il2CppType* VuforiaAbstractBehaviour_t1091759131_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaAbstractBehaviour_t1091759131_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t828034096_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2793021901_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m626199895_MethodInfo_var;
extern const uint32_t DefaultInitializationErrorHandler_OnDestroy_m738843949_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_OnDestroy_m738843949 (DefaultInitializationErrorHandler_t3746290221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnDestroy_m738843949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t1091759131 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t1091759131_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t1091759131 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t1091759131_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t1091759131 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		VuforiaAbstractBehaviour_t1091759131 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2793021901_MethodInfo_var);
		Action_1_t828034096 * L_6 = (Action_1_t828034096 *)il2cpp_codegen_object_new(Action_1_t828034096_il2cpp_TypeInfo_var);
		Action_1__ctor_m626199895(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m626199895_MethodInfo_var);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_UnregisterVuforiaInitErrorCallback_m20288137(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral65203672;
extern const uint32_t DefaultInitializationErrorHandler_DrawWindowContent_m1550878440_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_DrawWindowContent_m1550878440 (DefaultInitializationErrorHandler_t3746290221 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_DrawWindowContent_m1550878440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_mErrorText_3();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m885093907(NULL /*static, unused*/, L_6, _stringLiteral65203672, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2905941413;
extern Il2CppCodeGenString* _stringLiteral1786194993;
extern Il2CppCodeGenString* _stringLiteral1416929054;
extern Il2CppCodeGenString* _stringLiteral3487739072;
extern Il2CppCodeGenString* _stringLiteral1427061168;
extern Il2CppCodeGenString* _stringLiteral1137196578;
extern Il2CppCodeGenString* _stringLiteral1113348665;
extern Il2CppCodeGenString* _stringLiteral3550496933;
extern Il2CppCodeGenString* _stringLiteral3287795045;
extern Il2CppCodeGenString* _stringLiteral4247658613;
extern Il2CppCodeGenString* _stringLiteral205685978;
extern const uint32_t DefaultInitializationErrorHandler_SetErrorCode_m2157300411_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_SetErrorCode_m2157300411 (DefaultInitializationErrorHandler_t3746290221 * __this, int32_t ___errorCode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_SetErrorCode_m2157300411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = __this->get_mErrorText_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2905941413, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode0;
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 0)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 1)
		{
			goto IL_00ad;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 3)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 6)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 7)
		{
			goto IL_00bd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 8)
		{
			goto IL_00cd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 9)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ed;
	}

IL_004d:
	{
		__this->set_mErrorText_3(_stringLiteral1786194993);
		goto IL_00ed;
	}

IL_005d:
	{
		__this->set_mErrorText_3(_stringLiteral1416929054);
		goto IL_00ed;
	}

IL_006d:
	{
		__this->set_mErrorText_3(_stringLiteral3487739072);
		goto IL_00ed;
	}

IL_007d:
	{
		__this->set_mErrorText_3(_stringLiteral1427061168);
		goto IL_00ed;
	}

IL_008d:
	{
		__this->set_mErrorText_3(_stringLiteral1137196578);
		goto IL_00ed;
	}

IL_009d:
	{
		__this->set_mErrorText_3(_stringLiteral1113348665);
		goto IL_00ed;
	}

IL_00ad:
	{
		__this->set_mErrorText_3(_stringLiteral3550496933);
		goto IL_00ed;
	}

IL_00bd:
	{
		__this->set_mErrorText_3(_stringLiteral3287795045);
		goto IL_00ed;
	}

IL_00cd:
	{
		__this->set_mErrorText_3(_stringLiteral4247658613);
		goto IL_00ed;
	}

IL_00dd:
	{
		__this->set_mErrorText_3(_stringLiteral205685978);
		goto IL_00ed;
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void DefaultInitializationErrorHandler_SetErrorOccurred_m3897300882 (DefaultInitializationErrorHandler_t3746290221 * __this, bool ___errorOccurred0, const MethodInfo* method)
{
	{
		bool L_0 = ___errorOccurred0;
		__this->set_mErrorOccurred_4(L_0);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2793021901 (DefaultInitializationErrorHandler_t3746290221 * __this, int32_t ___initError0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___initError0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError0;
		DefaultInitializationErrorHandler_SetErrorCode_m2157300411(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m3897300882(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C"  void DefaultSmartTerrainEventHandler__ctor_m2430750330 (DefaultSmartTerrainEventHandler_t543367463 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2561125293_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3490205855_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionBehaviour_t3695833539_m55479916_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3478346704_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3424246874_MethodInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_Start_m1377888122_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_Start_m1377888122 (DefaultSmartTerrainEventHandler_t543367463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_Start_m1377888122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t3695833539 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t3695833539_m55479916(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t3695833539_m55479916_MethodInfo_var);
		__this->set_mReconstructionBehaviour_2(L_0);
		ReconstructionBehaviour_t3695833539 * L_1 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t3695833539 * L_3 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799_MethodInfo_var);
		Action_1_t2561125293 * L_5 = (Action_1_t2561125293 *)il2cpp_codegen_object_new(Action_1_t2561125293_il2cpp_TypeInfo_var);
		Action_1__ctor_m3478346704(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m3478346704_MethodInfo_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m958985328(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t3695833539 * L_6 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389_MethodInfo_var);
		Action_1_t3490205855 * L_8 = (Action_1_t3490205855 *)il2cpp_codegen_object_new(Action_1_t3490205855_il2cpp_TypeInfo_var);
		Action_1__ctor_m3424246874(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m3424246874_MethodInfo_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m553006512(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2561125293_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3490205855_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3478346704_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3424246874_MethodInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_OnDestroy_m831805043_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_OnDestroy_m831805043 (DefaultSmartTerrainEventHandler_t543367463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnDestroy_m831805043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t3695833539 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t3695833539 * L_2 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799_MethodInfo_var);
		Action_1_t2561125293 * L_4 = (Action_1_t2561125293 *)il2cpp_codegen_object_new(Action_1_t2561125293_il2cpp_TypeInfo_var);
		Action_1__ctor_m3478346704(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3478346704_MethodInfo_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m3888109065(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t3695833539 * L_5 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389_MethodInfo_var);
		Action_1_t3490205855 * L_7 = (Action_1_t3490205855 *)il2cpp_codegen_object_new(Action_1_t3490205855_il2cpp_TypeInfo_var);
		Action_1__ctor_m3424246874(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m3424246874_MethodInfo_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m4273586313(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799 (DefaultSmartTerrainEventHandler_t543367463 * __this, Il2CppObject * ___prop0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnPropCreated_m2482270799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t3695833539 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t3695833539 * L_2 = __this->get_mReconstructionBehaviour_2();
		PropBehaviour_t3213561028 * L_3 = __this->get_PropTemplate_3();
		Il2CppObject * L_4 = ___prop0;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m3949632283(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389 (DefaultSmartTerrainEventHandler_t543367463 * __this, Il2CppObject * ___surface0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1775765389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t3695833539 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t3695833539 * L_2 = __this->get_mReconstructionBehaviour_2();
		SurfaceBehaviour_t3457144850 * L_3 = __this->get_SurfaceTemplate_4();
		Il2CppObject * L_4 = ___surface0;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m2064989829(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C"  void DefaultTrackableEventHandler__ctor_m2932499163 (DefaultTrackableEventHandler_t4108278998 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169_MethodInfo_var;
extern const uint32_t DefaultTrackableEventHandler_Start_m1879636955_MetadataUsageId;
extern "C"  void DefaultTrackableEventHandler_Start_m1879636955 (DefaultTrackableEventHandler_t4108278998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_Start_m1879636955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t4179556250 * L_0 = Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169_MethodInfo_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t4179556250 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t4179556250 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1688554955(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void DefaultTrackableEventHandler_OnTrackableStateChanged_m1552340898 (DefaultTrackableEventHandler_t4108278998 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m3892924005(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m4176622723(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMarkerChildManager_t1830688875_m2821499578_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3154712507;
extern Il2CppCodeGenString* _stringLiteral1013754722;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingFound_m3892924005_MetadataUsageId;
extern "C"  void DefaultTrackableEventHandler_OnTrackingFound_m3892924005 (DefaultTrackableEventHandler_t4108278998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingFound_m3892924005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MarkerChildManager_t1830688875 * V_0 = NULL;
	RendererU5BU5D_t440051646* V_1 = NULL;
	ColliderU5BU5D_t2697150633* V_2 = NULL;
	Renderer_t3076687687 * V_3 = NULL;
	RendererU5BU5D_t440051646* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t2939674232 * V_6 = NULL;
	ColliderU5BU5D_t2697150633* V_7 = NULL;
	int32_t V_8 = 0;
	{
		MarkerChildManager_t1830688875 * L_0 = Component_GetComponent_TisMarkerChildManager_t1830688875_m2821499578(__this, /*hidden argument*/Component_GetComponent_TisMarkerChildManager_t1830688875_m2821499578_MethodInfo_var);
		V_0 = L_0;
		MarkerChildManager_t1830688875 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		MarkerChildManager_t1830688875 * L_3 = Component_GetComponent_TisMarkerChildManager_t1830688875_m2821499578(__this, /*hidden argument*/Component_GetComponent_TisMarkerChildManager_t1830688875_m2821499578_MethodInfo_var);
		NullCheck(L_3);
		MarkerChildManager_AnimateTargets_m2917730709(L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		RendererU5BU5D_t440051646* L_4 = Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var);
		V_1 = L_4;
		ColliderU5BU5D_t2697150633* L_5 = Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var);
		V_2 = L_5;
		RendererU5BU5D_t440051646* L_6 = V_1;
		V_4 = L_6;
		V_5 = 0;
		goto IL_004b;
	}

IL_0038:
	{
		RendererU5BU5D_t440051646* L_7 = V_4;
		int32_t L_8 = V_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		Renderer_t3076687687 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_3 = L_10;
		Renderer_t3076687687 * L_11 = V_3;
		NullCheck(L_11);
		Renderer_set_enabled_m2514140131(L_11, (bool)1, /*hidden argument*/NULL);
		int32_t L_12 = V_5;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_13 = V_5;
		RendererU5BU5D_t440051646* L_14 = V_4;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0038;
		}
	}
	{
		ColliderU5BU5D_t2697150633* L_15 = V_2;
		V_7 = L_15;
		V_8 = 0;
		goto IL_0076;
	}

IL_0061:
	{
		ColliderU5BU5D_t2697150633* L_16 = V_7;
		int32_t L_17 = V_8;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		Collider_t2939674232 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_6 = L_19;
		Collider_t2939674232 * L_20 = V_6;
		NullCheck(L_20);
		Collider_set_enabled_m2575670866(L_20, (bool)1, /*hidden argument*/NULL);
		int32_t L_21 = V_8;
		V_8 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_22 = V_8;
		ColliderU5BU5D_t2697150633* L_23 = V_7;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0061;
		}
	}
	{
		TrackableBehaviour_t4179556250 * L_24 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_24);
		String_t* L_25 = TrackableBehaviour_get_TrackableName_m2076218645(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3154712507, L_25, _stringLiteral1013754722, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3154712507;
extern Il2CppCodeGenString* _stringLiteral32880452;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingLost_m4176622723_MetadataUsageId;
extern "C"  void DefaultTrackableEventHandler_OnTrackingLost_m4176622723 (DefaultTrackableEventHandler_t4108278998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingLost_m4176622723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t440051646* V_0 = NULL;
	ColliderU5BU5D_t2697150633* V_1 = NULL;
	Renderer_t3076687687 * V_2 = NULL;
	RendererU5BU5D_t440051646* V_3 = NULL;
	int32_t V_4 = 0;
	Collider_t2939674232 * V_5 = NULL;
	ColliderU5BU5D_t2697150633* V_6 = NULL;
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t440051646* L_0 = Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t2697150633* L_1 = Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t440051646* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t440051646* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Renderer_t3076687687 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Renderer_t3076687687 * L_7 = V_2;
		NullCheck(L_7);
		Renderer_set_enabled_m2514140131(L_7, (bool)0, /*hidden argument*/NULL);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_4;
		RendererU5BU5D_t440051646* L_10 = V_3;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t2697150633* L_11 = V_1;
		V_6 = L_11;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t2697150633* L_12 = V_6;
		int32_t L_13 = V_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Collider_t2939674232 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Collider_t2939674232 * L_16 = V_5;
		NullCheck(L_16);
		Collider_set_enabled_m2575670866(L_16, (bool)0, /*hidden argument*/NULL);
		int32_t L_17 = V_7;
		V_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_18 = V_7;
		ColliderU5BU5D_t2697150633* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t4179556250 * L_20 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_20);
		String_t* L_21 = TrackableBehaviour_get_TrackableName_m2076218645(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3154712507, L_21, _stringLiteral32880452, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C"  void GLErrorHandler__ctor_m3261991176 (GLErrorHandler_t4275497481 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GLErrorHandler_t4275497481_il2cpp_TypeInfo_var;
extern const uint32_t GLErrorHandler__cctor_m1855382437_MetadataUsageId;
extern "C"  void GLErrorHandler__cctor_m1855382437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler__cctor_m1855382437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((GLErrorHandler_t4275497481_StaticFields*)GLErrorHandler_t4275497481_il2cpp_TypeInfo_var->static_fields)->set_mErrorText_3(L_0);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern Il2CppClass* GLErrorHandler_t4275497481_il2cpp_TypeInfo_var;
extern const uint32_t GLErrorHandler_SetError_m3405171520_MetadataUsageId;
extern "C"  void GLErrorHandler_SetError_m3405171520 (Il2CppObject * __this /* static, unused */, String_t* ___errorText0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_SetError_m3405171520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___errorText0;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t4275497481_il2cpp_TypeInfo_var);
		((GLErrorHandler_t4275497481_StaticFields*)GLErrorHandler_t4275497481_il2cpp_TypeInfo_var->static_fields)->set_mErrorText_3(L_0);
		((GLErrorHandler_t4275497481_StaticFields*)GLErrorHandler_t4275497481_il2cpp_TypeInfo_var->static_fields)->set_mErrorOccurred_4((bool)1);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern Il2CppClass* GLErrorHandler_t4275497481_il2cpp_TypeInfo_var;
extern Il2CppClass* WindowFunction_t2749288659_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const MethodInfo* GLErrorHandler_DrawWindowContent_m1646268860_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3068614674;
extern const uint32_t GLErrorHandler_OnGUI_m2757389826_MetadataUsageId;
extern "C"  void GLErrorHandler_OnGUI_m2757389826 (GLErrorHandler_t4275497481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_OnGUI_m2757389826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t4275497481_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t4275497481_StaticFields*)GLErrorHandler_t4275497481_il2cpp_TypeInfo_var->static_fields)->get_mErrorOccurred_4();
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m3291325233(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GLErrorHandler_DrawWindowContent_m1646268860_MethodInfo_var);
		WindowFunction_t2749288659 * L_5 = (WindowFunction_t2749288659 *)il2cpp_codegen_object_new(WindowFunction_t2749288659_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m732638321(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Window_m2314976695(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral3068614674, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern Il2CppClass* GLErrorHandler_t4275497481_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral65203672;
extern const uint32_t GLErrorHandler_DrawWindowContent_m1646268860_MetadataUsageId;
extern "C"  void GLErrorHandler_DrawWindowContent_m1646268860 (GLErrorHandler_t4275497481 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_DrawWindowContent_m1646268860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t4275497481_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t4275497481_StaticFields*)GLErrorHandler_t4275497481_il2cpp_TypeInfo_var->static_fields)->get_mErrorText_3();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m885093907(NULL /*static, unused*/, L_6, _stringLiteral65203672, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C"  void HideExcessAreaBehaviour__ctor_m454066526 (HideExcessAreaBehaviour_t439862723 * __this, const MethodInfo* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m261492772(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C"  void ImageTargetBehaviour__ctor_m3469834622 (ImageTargetBehaviour_t1735871187 * __this, const MethodInfo* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m1753622716(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C"  void IOSUnityPlayer__ctor_m918790246 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern "C"  void IOSUnityPlayer_LoadNativeLibraries_m1165765568 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C"  void IOSUnityPlayer_InitializePlatform_m2049752257 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m3740021168(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern "C"  int32_t IOSUnityPlayer_Start_m970549361 (IOSUnityPlayer_t2749231339 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m1193220576(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey0;
		int32_t L_2 = IOSUnityPlayer_initQCARiOS_m531358653(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m72295617(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return (int32_t)(L_4);
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern Il2CppClass* SurfaceUtilities_t743372383_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_Update_m144588647_MetadataUsageId;
extern "C"  void IOSUnityPlayer_Update_m144588647 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_Update_m144588647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t743372383_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m3719711414(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m72295617(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m1193220576(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m3959835541(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C"  void IOSUnityPlayer_Dispose_m2387413795 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern "C"  void IOSUnityPlayer_OnPause_m2682614363 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnPause_m3553757101(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern "C"  void IOSUnityPlayer_OnResume_m4242220426 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnResume_m1182874232(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern "C"  void IOSUnityPlayer_OnDestroy_m2170737247 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_Deinit_m3194148381(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern Il2CppClass* SurfaceUtilities_t743372383_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_InitializeSurface_m72295617_MetadataUsageId;
extern "C"  void IOSUnityPlayer_InitializeSurface_m72295617 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_InitializeSurface_m72295617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t743372383_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m2755562960(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m3959835541(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern Il2CppClass* SurfaceUtilities_t743372383_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_SetUnityScreenOrientation_m3959835541_MetadataUsageId;
extern "C"  void IOSUnityPlayer_SetUnityScreenOrientation_m3959835541 (IOSUnityPlayer_t2749231339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_SetUnityScreenOrientation_m3959835541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m1193220576(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_0(L_0);
		int32_t L_1 = __this->get_mScreenOrientation_0();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t743372383_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m2671033300(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		IOSUnityPlayer_setSurfaceOrientationiOS_m2809900247(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL setPlatFormNative();
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C"  void IOSUnityPlayer_setPlatFormNative_m3740021168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setPlatFormNative)();

}
extern "C" int32_t DEFAULT_CALL initQCARiOS(int32_t, char*);
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern "C"  int32_t IOSUnityPlayer_initQCARiOS_m531358653 (Il2CppObject * __this /* static, unused */, int32_t ___screenOrientation0, String_t* ___licenseKey1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);

	// Marshaling of parameter '___licenseKey1' to native representation
	char* ____licenseKey1_marshaled = NULL;
	____licenseKey1_marshaled = il2cpp_codegen_marshal_string(___licenseKey1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(initQCARiOS)(___screenOrientation0, ____licenseKey1_marshaled);

	// Marshaling cleanup of parameter '___licenseKey1' native representation
	il2cpp_codegen_marshal_free(____licenseKey1_marshaled);
	____licenseKey1_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C"  void IOSUnityPlayer_setSurfaceOrientationiOS_m2809900247 (Il2CppObject * __this /* static, unused */, int32_t ___screenOrientation0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setSurfaceOrientationiOS)(___screenOrientation0);

}
// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern "C"  void KeepAliveBehaviour__ctor_m4213800698 (KeepAliveBehaviour_t1171410903 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour__ctor_m612933944(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C"  void MarkerBehaviour__ctor_m3207254420 (MarkerBehaviour_t3170674893 * __this, const MethodInfo* method)
{
	{
		MarkerAbstractBehaviour__ctor_m856755802(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C"  void MaskOutBehaviour__ctor_m3882090484 (MaskOutBehaviour_t1204933533 * __this, const MethodInfo* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m3524327218(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern Il2CppClass* VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var;
extern Il2CppClass* MaterialU5BU5D_t170856778_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m3123788487_MethodInfo_var;
extern const uint32_t MaskOutBehaviour_Start_m2829228276_MetadataUsageId;
extern "C"  void MaskOutBehaviour_Start_m2829228276 (MaskOutBehaviour_t1204933533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MaskOutBehaviour_Start_m2829228276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t3076687687 * V_0 = NULL;
	int32_t V_1 = 0;
	MaterialU5BU5D_t170856778* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3074936826(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t3076687687 * L_1 = Component_GetComponent_TisRenderer_t3076687687_m3123788487(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m3123788487_MethodInfo_var);
		V_0 = L_1;
		Renderer_t3076687687 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t170856778* L_3 = Renderer_get_materials_m3755041148(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t3076687687 * L_5 = V_0;
		Material_t3870600107 * L_6 = ((MaskOutAbstractBehaviour_t3409682331 *)__this)->get_maskMaterial_2();
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m1064371045(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t170856778*)SZArrayNew(MaterialU5BU5D_t170856778_il2cpp_TypeInfo_var, (uint32_t)L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t170856778* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t3870600107 * L_10 = ((MaskOutAbstractBehaviour_t3409682331 *)__this)->get_maskMaterial_2();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Material_t3870600107 *)L_10);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t3076687687 * L_14 = V_0;
		MaterialU5BU5D_t170856778* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m1255100914(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C"  void MultiTargetBehaviour__ctor_m2581556060 (MultiTargetBehaviour_t2222860085 * __this, const MethodInfo* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m3597360282(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C"  void ObjectTargetBehaviour__ctor_m979473034 (ObjectTargetBehaviour_t2508606743 * __this, const MethodInfo* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m3650954832(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PropBehaviour::.ctor()
extern "C"  void PropBehaviour__ctor_m1666223485 (PropBehaviour_t3213561028 * __this, const MethodInfo* method)
{
	{
		PropAbstractBehaviour__ctor_m1034535491(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C"  void ReconstructionBehaviour__ctor_m749720926 (ReconstructionBehaviour_t3695833539 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m2258723876(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C"  void ReconstructionFromTargetBehaviour__ctor_m2641012889 (ReconstructionFromTargetBehaviour_t582925864 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m3469137759(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C"  void SmartTerrainTrackerBehaviour__ctor_m21848862 (SmartTerrainTrackerBehaviour_t442856755 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackerAbstractBehaviour__ctor_m1776629340(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C"  void SurfaceBehaviour__ctor_m1379610399 (SurfaceBehaviour_t3457144850 * __this, const MethodInfo* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m3989293917(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C"  void TextRecoBehaviour__ctor_m3821401222 (TextRecoBehaviour_t892056475 * __this, const MethodInfo* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m293498444(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C"  void TurnOffBehaviour__ctor_m2892493540 (TurnOffBehaviour_t1278464685 * __this, const MethodInfo* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m2502621730(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern Il2CppClass* VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3839065225_m3768884677_MethodInfo_var;
extern const uint32_t TurnOffBehaviour_Awake_m3130098759_MetadataUsageId;
extern "C"  void TurnOffBehaviour_Awake_m3130098759 (TurnOffBehaviour_t1278464685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnOffBehaviour_Awake_m3130098759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t2804666580 * V_0 = NULL;
	MeshFilter_t3839065225 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3074936826(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t2804666580 * L_1 = Component_GetComponent_TisMeshRenderer_t2804666580_m541358362(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t2804666580 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t3839065225 * L_3 = Component_GetComponent_TisMeshFilter_t3839065225_m3768884677(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3839065225_m3768884677_MethodInfo_var);
		V_1 = L_3;
		MeshFilter_t3839065225 * L_4 = V_1;
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C"  void TurnOffWordBehaviour__ctor_m3813152910 (TurnOffWordBehaviour_t3289283011 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern Il2CppClass* VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2603341;
extern const uint32_t TurnOffWordBehaviour_Awake_m4050758129_MetadataUsageId;
extern "C"  void TurnOffWordBehaviour_Awake_m4050758129 (TurnOffWordBehaviour_t3289283011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnOffWordBehaviour_Awake_m4050758129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t2804666580 * V_0 = NULL;
	Transform_t1659122786 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3074936826(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t2804666580 * L_1 = Component_GetComponent_TisMeshRenderer_t2804666580_m541358362(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t2804666580_m541358362_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t2804666580 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_FindChild_m2149912886(L_3, _stringLiteral2603341, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t1659122786 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t1659122786 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C"  void UserDefinedTargetBuildingBehaviour__ctor_m4162495669 (UserDefinedTargetBuildingBehaviour_t3016919996 * __this, const MethodInfo* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m443376627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern Il2CppClass* VideoBackgroundAbstractBehaviour_t2475465524_il2cpp_TypeInfo_var;
extern const uint32_t VideoBackgroundBehaviour__ctor_m3812024187_MetadataUsageId;
extern "C"  void VideoBackgroundBehaviour__ctor_m3812024187 (VideoBackgroundBehaviour_t3391125558 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBackgroundBehaviour__ctor_m3812024187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VideoBackgroundAbstractBehaviour_t2475465524_il2cpp_TypeInfo_var);
		VideoBackgroundAbstractBehaviour__ctor_m818160569(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C"  void VideoTextureRenderer__ctor_m3307215884 (VideoTextureRenderer_t1585639557 * __this, const MethodInfo* method)
{
	{
		VideoTextureRendererAbstractBehaviour__ctor_m1609339875(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C"  void VirtualButtonBehaviour__ctor_m3254727567 (VirtualButtonBehaviour_t2253448098 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m3745206989(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C"  void VuforiaBehaviour__ctor_m745948020 (VuforiaBehaviour_t1845338141 * __this, const MethodInfo* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m4033329842(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C"  void VuforiaBehaviour__cctor_m1167455929 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern Il2CppClass* NullUnityPlayer_t531631577_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidUnityPlayer_t1535493961_il2cpp_TypeInfo_var;
extern Il2CppClass* IOSUnityPlayer_t2749231339_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayModeUnityPlayer_t2903303273_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2489055709_m2183989835_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_Awake_m983553239_MetadataUsageId;
extern "C"  void VuforiaBehaviour_Awake_m983553239 (VuforiaBehaviour_t1845338141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_Awake_m983553239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t531631577 * L_0 = (NullUnityPlayer_t531631577 *)il2cpp_codegen_object_new(NullUnityPlayer_t531631577_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m2430877964(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t1535493961 * L_2 = (AndroidUnityPlayer_t1535493961 *)il2cpp_codegen_object_new(AndroidUnityPlayer_t1535493961_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m2334148552(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t2749231339 * L_4 = (IOSUnityPlayer_t2749231339 *)il2cpp_codegen_object_new(IOSUnityPlayer_t2749231339_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m918790246(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0043;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m1446042578(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		PlayModeUnityPlayer_t2903303273 * L_6 = (PlayModeUnityPlayer_t2903303273 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t2903303273_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m4139710076(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0043:
	{
		Il2CppObject * L_7 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m2167247423(__this, L_7, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2489055709_m2183989835(L_8, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2489055709_m2183989835_MethodInfo_var);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern Il2CppClass* VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVuforiaBehaviour_t1845338141_m259102150_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_get_Instance_m2318468804_MetadataUsageId;
extern "C"  VuforiaBehaviour_t1845338141 * VuforiaBehaviour_get_Instance_m2318468804 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_get_Instance_m2318468804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_0 = ((VuforiaBehaviour_t1845338141_StaticFields*)VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var->static_fields)->get_mVuforiaBehaviour_52();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t1845338141_m259102150(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t1845338141_m259102150_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t1845338141_StaticFields*)VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var->static_fields)->set_mVuforiaBehaviour_52(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t1845338141 * L_3 = ((VuforiaBehaviour_t1845338141_StaticFields*)VuforiaBehaviour_t1845338141_il2cpp_TypeInfo_var->static_fields)->get_mVuforiaBehaviour_52();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C"  void VuforiaBehaviourComponentFactory__ctor_m3340288679 (VuforiaBehaviourComponentFactory_t900168586 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t1204933533_m4225605259_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3938351745_MetadataUsageId;
extern "C"  MaskOutAbstractBehaviour_t3409682331 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3938351745 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3938351745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MaskOutBehaviour_t1204933533 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t1204933533_m4225605259(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t1204933533_m4225605259_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t2253448098_m2099003942_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m3055054859_MetadataUsageId;
extern "C"  VirtualButtonAbstractBehaviour_t1191238304 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m3055054859 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m3055054859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VirtualButtonBehaviour_t2253448098 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t2253448098_m2099003942(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t2253448098_m2099003942_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t1278464685_m63866235_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1457392801_MetadataUsageId;
extern "C"  TurnOffAbstractBehaviour_t1299549867 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1457392801 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1457392801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TurnOffBehaviour_t1278464685 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t1278464685_m63866235(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t1278464685_m63866235_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t1735871187_m1658831381_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m3362790253_MetadataUsageId;
extern "C"  ImageTargetAbstractBehaviour_t2347335889 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m3362790253 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m3362790253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ImageTargetBehaviour_t1735871187 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t1735871187_m1658831381(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t1735871187_m1658831381_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMarkerBehaviour_t3170674893_m1585383317_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m996536629_MetadataUsageId;
extern "C"  MarkerAbstractBehaviour_t865486027 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m996536629 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m996536629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MarkerBehaviour_t3170674893 * L_1 = GameObject_AddComponent_TisMarkerBehaviour_t3170674893_m1585383317(L_0, /*hidden argument*/GameObject_AddComponent_TisMarkerBehaviour_t3170674893_m1585383317_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t2222860085_m1145565811_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m3336152497_MetadataUsageId;
extern "C"  MultiTargetAbstractBehaviour_t3565607731 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m3336152497 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m3336152497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MultiTargetBehaviour_t2222860085 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t2222860085_m1145565811(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t2222860085_m1145565811_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t1850077856_m1470876642_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m3555530773_MetadataUsageId;
extern "C"  CylinderTargetAbstractBehaviour_t1139674014 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m3555530773 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m3555530773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		CylinderTargetBehaviour_t1850077856 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t1850077856_m1470876642(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t1850077856_m1470876642_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t2034767101_m2299282597_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddWordBehaviour_m1348270901_MetadataUsageId;
extern "C"  WordAbstractBehaviour_t545947899 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m1348270901 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddWordBehaviour_m1348270901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		WordBehaviour_t2034767101 * L_1 = GameObject_AddComponent_TisWordBehaviour_t2034767101_m2299282597(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t2034767101_m2299282597_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t892056475_m1686624967_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2953291125_MetadataUsageId;
extern "C"  TextRecoAbstractBehaviour_t137142681 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2953291125 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2953291125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TextRecoBehaviour_t892056475 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t892056475_m1686624967(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t892056475_m1686624967_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t2508606743_m1474978123_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m100001525_MetadataUsageId;
extern "C"  ObjectTargetAbstractBehaviour_t637796117 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m100001525 (VuforiaBehaviourComponentFactory_t900168586 * __this, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m100001525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ObjectTargetBehaviour_t2508606743 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t2508606743_m1474978123(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t2508606743_m1474978123_MethodInfo_var);
		return L_1;
	}
}
// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C"  void WebCamBehaviour__ctor_m3169026805 (WebCamBehaviour_t2450634316 * __this, const MethodInfo* method)
{
	{
		WebCamAbstractBehaviour__ctor_m2004274875(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C"  void WireframeBehaviour__ctor_m3952323258 (WireframeBehaviour_t433318935 * __this, const MethodInfo* method)
{
	{
		__this->set_ShowLines_3((bool)1);
		Color_t4194546905  L_0 = Color_get_green_m2005284533(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_LineColor_4(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4100016661;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral1207192980;
extern const uint32_t WireframeBehaviour_CreateLineMaterial_m2368566817_MetadataUsageId;
extern "C"  void WireframeBehaviour_CreateLineMaterial_m2368566817 (WireframeBehaviour_t433318935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CreateLineMaterial_m2368566817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral4100016661);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4100016661);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		Color_t4194546905 * L_2 = __this->get_address_of_LineColor_4();
		float L_3 = L_2->get_r_0();
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral44);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		Color_t4194546905 * L_8 = __this->get_address_of_LineColor_4();
		float L_9 = L_8->get_g_1();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral44);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_13 = L_12;
		Color_t4194546905 * L_14 = __this->get_address_of_LineColor_4();
		float L_15 = L_14->get_b_2();
		float L_16 = L_15;
		Il2CppObject * L_17 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_17);
		ObjectU5BU5D_t1108656482* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral44);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_19 = L_18;
		Color_t4194546905 * L_20 = __this->get_address_of_LineColor_4();
		float L_21 = L_20->get_a_3();
		float L_22 = L_21;
		Il2CppObject * L_23 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_23);
		ObjectU5BU5D_t1108656482* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, _stringLiteral1207192980);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral1207192980);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m3016520001(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Material_t3870600107 * L_26 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m1122544796(L_26, L_25, /*hidden argument*/NULL);
		__this->set_mLineMaterial_2(L_26);
		Material_t3870600107 * L_27 = __this->get_mLineMaterial_2();
		NullCheck(L_27);
		Object_set_hideFlags_m41317712(L_27, ((int32_t)61), /*hidden argument*/NULL);
		Material_t3870600107 * L_28 = __this->get_mLineMaterial_2();
		NullCheck(L_28);
		Shader_t3191267369 * L_29 = Material_get_shader_m2881845503(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_hideFlags_m41317712(L_29, ((int32_t)61), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern Il2CppClass* VuforiaManager_t1442390413_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t2727095145_m1504240198_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3839065225_m3768884677_MethodInfo_var;
extern const uint32_t WireframeBehaviour_OnRenderObject_m1672635134_MetadataUsageId;
extern "C"  void WireframeBehaviour_OnRenderObject_m1672635134 (WireframeBehaviour_t433318935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnRenderObject_m1672635134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	CameraU5BU5D_t2716570836* V_1 = NULL;
	bool V_2 = false;
	Camera_t2727095145 * V_3 = NULL;
	CameraU5BU5D_t2716570836* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3839065225 * V_6 = NULL;
	Mesh_t4241756145 * V_7 = NULL;
	Vector3U5BU5D_t215400611* V_8 = NULL;
	Int32U5BU5D_t3230847821* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t1442390413_il2cpp_TypeInfo_var);
		VuforiaManager_t1442390413 * L_0 = VuforiaManager_get_Instance_m700178176(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = VirtFuncInvoker0< Transform_t1659122786 * >::Invoke(8 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t2716570836* L_4 = GameObject_GetComponentsInChildren_TisCamera_t2727095145_m1504240198(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t2727095145_m1504240198_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t2716570836* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t2716570836* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Camera_t2727095145 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t2727095145 * L_10 = Camera_get_current_m475592003(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t2727095145 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t2716570836* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3839065225 * L_18 = Component_GetComponent_TisMeshFilter_t3839065225_m3768884677(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3839065225_m3768884677_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3839065225 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t3870600107 * L_21 = __this->get_mLineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_21, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m2368566817(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		MeshFilter_t3839065225 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t4241756145 * L_24 = MeshFilter_get_sharedMesh_m2700148450(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t4241756145 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t215400611* L_26 = Mesh_get_vertices_m3685486174(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t4241756145 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3230847821* L_28 = Mesh_get_triangles_m2145908418(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t1659122786 * L_29 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t1651859333  L_30 = Transform_get_localToWorldMatrix_m3571020210(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m1618741133(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t3870600107 * L_31 = __this->get_mLineMaterial_2();
		NullCheck(L_31);
		Material_SetPass_m4241824642(L_31, 0, /*hidden argument*/NULL);
		GL_Begin_m3089952800(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		Vector3U5BU5D_t215400611* L_32 = V_8;
		Int32U5BU5D_t3230847821* L_33 = V_9;
		int32_t L_34 = V_10;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_36);
		V_11 = (*(Vector3_t4282066566 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36))));
		Vector3U5BU5D_t215400611* L_37 = V_8;
		Int32U5BU5D_t3230847821* L_38 = V_9;
		int32_t L_39 = V_10;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)((int32_t)L_39+(int32_t)1)));
		int32_t L_40 = ((int32_t)((int32_t)L_39+(int32_t)1));
		int32_t L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_41);
		V_12 = (*(Vector3_t4282066566 *)((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41))));
		Vector3U5BU5D_t215400611* L_42 = V_8;
		Int32U5BU5D_t3230847821* L_43 = V_9;
		int32_t L_44 = V_10;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)((int32_t)L_44+(int32_t)2)));
		int32_t L_45 = ((int32_t)((int32_t)L_44+(int32_t)2));
		int32_t L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_46);
		V_13 = (*(Vector3_t4282066566 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46))));
		Vector3_t4282066566  L_47 = V_11;
		GL_Vertex_m1061829497(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Vector3_t4282066566  L_48 = V_12;
		GL_Vertex_m1061829497(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Vector3_t4282066566  L_49 = V_12;
		GL_Vertex_m1061829497(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t4282066566  L_50 = V_13;
		GL_Vertex_m1061829497(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t4282066566  L_51 = V_13;
		GL_Vertex_m1061829497(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t4282066566  L_52 = V_11;
		GL_Vertex_m1061829497(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		int32_t L_53 = V_10;
		V_10 = ((int32_t)((int32_t)L_53+(int32_t)3));
	}

IL_0144:
	{
		int32_t L_54 = V_10;
		Int32U5BU5D_t3230847821* L_55 = V_9;
		NullCheck(L_55);
		if ((((int32_t)L_54) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_55)->max_length)))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3839065225_m3768884677_MethodInfo_var;
extern const uint32_t WireframeBehaviour_OnDrawGizmos_m1652408614_MetadataUsageId;
extern "C"  void WireframeBehaviour_OnDrawGizmos_m1652408614 (WireframeBehaviour_t433318935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnDrawGizmos_m1652408614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t3839065225 * V_0 = NULL;
	Mesh_t4241756145 * V_1 = NULL;
	Vector3U5BU5D_t215400611* V_2 = NULL;
	Int32U5BU5D_t3230847821* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t3839065225 * L_2 = Component_GetComponent_TisMeshFilter_t3839065225_m3768884677(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3839065225_m3768884677_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3839065225 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t1553702882  L_10 = Transform_get_rotation_m11483428(L_9, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_lossyScale_m3749612506(L_12, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_14 = Matrix4x4_TRS_m3596398659(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m3443030764(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t4194546905  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m3649224910(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3839065225 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t4241756145 * L_17 = MeshFilter_get_sharedMesh_m2700148450(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t4241756145 * L_18 = V_1;
		NullCheck(L_18);
		Vector3U5BU5D_t215400611* L_19 = Mesh_get_vertices_m3685486174(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Mesh_t4241756145 * L_20 = V_1;
		NullCheck(L_20);
		Int32U5BU5D_t3230847821* L_21 = Mesh_get_triangles_m2145908418(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		Vector3U5BU5D_t215400611* L_22 = V_2;
		Int32U5BU5D_t3230847821* L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		int32_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_26);
		V_5 = (*(Vector3_t4282066566 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26))));
		Vector3U5BU5D_t215400611* L_27 = V_2;
		Int32U5BU5D_t3230847821* L_28 = V_3;
		int32_t L_29 = V_4;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)((int32_t)L_29+(int32_t)1)));
		int32_t L_30 = ((int32_t)((int32_t)L_29+(int32_t)1));
		int32_t L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_31);
		V_6 = (*(Vector3_t4282066566 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_31))));
		Vector3U5BU5D_t215400611* L_32 = V_2;
		Int32U5BU5D_t3230847821* L_33 = V_3;
		int32_t L_34 = V_4;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)((int32_t)L_34+(int32_t)2)));
		int32_t L_35 = ((int32_t)((int32_t)L_34+(int32_t)2));
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_36);
		V_7 = (*(Vector3_t4282066566 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36))));
		Vector3_t4282066566  L_37 = V_5;
		Vector3_t4282066566  L_38 = V_6;
		Gizmos_DrawLine_m4199765284(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		Vector3_t4282066566  L_39 = V_6;
		Vector3_t4282066566  L_40 = V_7;
		Gizmos_DrawLine_m4199765284(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t4282066566  L_41 = V_7;
		Vector3_t4282066566  L_42 = V_5;
		Gizmos_DrawLine_m4199765284(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		int32_t L_43 = V_4;
		V_4 = ((int32_t)((int32_t)L_43+(int32_t)3));
	}

IL_00e3:
	{
		int32_t L_44 = V_4;
		Int32U5BU5D_t3230847821* L_45 = V_3;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length)))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C"  void WireframeTrackableEventHandler__ctor_m4160184162 (WireframeTrackableEventHandler_t3463640687 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169_MethodInfo_var;
extern const uint32_t WireframeTrackableEventHandler_Start_m3107321954_MetadataUsageId;
extern "C"  void WireframeTrackableEventHandler_Start_m3107321954 (WireframeTrackableEventHandler_t3463640687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_Start_m3107321954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t4179556250 * L_0 = Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t4179556250_m43655169_MethodInfo_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t4179556250 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t4179556250 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1688554955(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OnTrackableStateChanged_m2625207675 (WireframeTrackableEventHandler_t3463640687 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m1321569836(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m1599823836(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3154712507;
extern Il2CppCodeGenString* _stringLiteral1013754722;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingFound_m1321569836_MetadataUsageId;
extern "C"  void WireframeTrackableEventHandler_OnTrackingFound_m1321569836 (WireframeTrackableEventHandler_t3463640687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingFound_m1321569836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t440051646* V_0 = NULL;
	ColliderU5BU5D_t2697150633* V_1 = NULL;
	WireframeBehaviourU5BU5D_t1176995246* V_2 = NULL;
	Renderer_t3076687687 * V_3 = NULL;
	RendererU5BU5D_t440051646* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t2939674232 * V_6 = NULL;
	ColliderU5BU5D_t2697150633* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t433318935 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t1176995246* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t440051646* L_0 = Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t2697150633* L_1 = Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t1176995246* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t440051646* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t440051646* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Renderer_t3076687687 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t3076687687 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m2514140131(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t440051646* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t2697150633* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t2697150633* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Collider_t2939674232 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t2939674232 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m2575670866(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t2697150633* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t1176995246* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t1176995246* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		WireframeBehaviour_t433318935 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t433318935 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m2046806933(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t1176995246* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t4179556250 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m2076218645(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3154712507, L_31, _stringLiteral1013754722, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3154712507;
extern Il2CppCodeGenString* _stringLiteral32880452;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingLost_m1599823836_MetadataUsageId;
extern "C"  void WireframeTrackableEventHandler_OnTrackingLost_m1599823836 (WireframeTrackableEventHandler_t3463640687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingLost_m1599823836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t440051646* V_0 = NULL;
	ColliderU5BU5D_t2697150633* V_1 = NULL;
	WireframeBehaviourU5BU5D_t1176995246* V_2 = NULL;
	Renderer_t3076687687 * V_3 = NULL;
	RendererU5BU5D_t440051646* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t2939674232 * V_6 = NULL;
	ColliderU5BU5D_t2697150633* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t433318935 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t1176995246* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t440051646* L_0 = Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t3076687687_m2167501363_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t2697150633* L_1 = Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2939674232_m3250668322_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t1176995246* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t433318935_m965959628_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t440051646* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t440051646* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Renderer_t3076687687 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t3076687687 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m2514140131(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t440051646* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t2697150633* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t2697150633* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Collider_t2939674232 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t2939674232 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m2575670866(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t2697150633* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t1176995246* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t1176995246* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		WireframeBehaviour_t433318935 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t433318935 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m2046806933(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t1176995246* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t4179556250 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m2076218645(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3154712507, L_31, _stringLiteral32880452, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C"  void WordBehaviour__ctor_m4262901092 (WordBehaviour_t2034767101 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m3910734634(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
