﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>
struct U3CGetEnumeratorU3Ec__IteratorD_t3105276340;
// System.Object
struct Il2CppObject;
// BetterList`1/<GetEnumerator>c__IteratorD<System.Object>
struct U3CGetEnumeratorU3Ec__IteratorD_t1827286915;
// BetterList`1/<GetEnumerator>c__IteratorD<System.Single>
struct U3CGetEnumeratorU3Ec__IteratorD_t1948389516;
// BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>
struct U3CGetEnumeratorU3Ec__IteratorD_t514942645;
// BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>
struct U3CGetEnumeratorU3Ec__IteratorD_t3097052309;
// BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>
struct U3CGetEnumeratorU3Ec__IteratorD_t1851017449;
// BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>
struct U3CGetEnumeratorU3Ec__IteratorD_t2550291528;
// BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>
struct U3CGetEnumeratorU3Ec__IteratorD_t1938537109;
// BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>
struct U3CGetEnumeratorU3Ec__IteratorD_t1938537110;
// BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>
struct U3CGetEnumeratorU3Ec__IteratorD_t1938537111;
// BetterList`1/CompareFunc<System.Int32>
struct CompareFunc_t1695557370;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// BetterList`1/CompareFunc<System.Object>
struct CompareFunc_t417567945;
// BetterList`1/CompareFunc<System.Single>
struct CompareFunc_t538670546;
// BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>
struct CompareFunc_t3400190971;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t1687333339;
// BetterList`1/CompareFunc<UnityEngine.Color>
struct CompareFunc_t441298479;
// BetterList`1/CompareFunc<UnityEngine.Color32>
struct CompareFunc_t1140572558;
// BetterList`1/CompareFunc<UnityEngine.Vector2>
struct CompareFunc_t528818139;
// BetterList`1/CompareFunc<UnityEngine.Vector3>
struct CompareFunc_t528818140;
// BetterList`1/CompareFunc<UnityEngine.Vector4>
struct CompareFunc_t528818141;
// BetterList`1<System.Int32>
struct BetterList_1_t2650806512;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// BetterList`1<System.Object>
struct BetterList_1_t1372817087;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// BetterList`1<System.Single>
struct BetterList_1_t1493919688;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1908816725;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// BetterList`1<TypewriterEffect/FadeEntry>
struct BetterList_1_t60472817;
// System.Collections.Generic.IEnumerator`1<TypewriterEffect/FadeEntry>
struct IEnumerator_1_t475369854;
// TypewriterEffect/FadeEntry[]
struct FadeEntryU5BU5D_t3343031592;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t2642582481;
// System.Collections.Generic.IEnumerator`1<UICamera/DepthEntry>
struct IEnumerator_1_t3057479518;
// UICamera/DepthEntry[]
struct DepthEntryU5BU5D_t1472539592;
// BetterList`1<UnityEngine.Color>
struct BetterList_1_t1396547621;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t1811444658;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t2510718737;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t1898964318;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1898964319;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t1484067283;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t1898964320;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// System.Action`1<System.Object>
struct Action_1_t271665211;
// System.Action`1<System.Single>
struct Action_1_t392767812;
// System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode>
struct Action_1_t3655021551;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t983143684;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t828034096;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3206263253;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2095059871;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2336740304;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1756209413;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Exception
struct Exception_t3991598821;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t645006031;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t676510742;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t886686464;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t918191175;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E3105276340.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E3105276340MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_UInt3224667981.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2650806512.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1827286915.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1827286915MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1372817087.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1948389516.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1948389516MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1493919688.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec514942645.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec514942645MethodDeclarations.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry2858472101.h"
#include "AssemblyU2DCSharp_BetterList_1_gen60472817.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E3097052309.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E3097052309MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2642582481.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1851017449.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1851017449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1396547621.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2550291528.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2550291528MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2095821700.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1938537109.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1938537109MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067281.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1938537110.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1938537110MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067282.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1938537111.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1938537111MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067283.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1695557370.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1695557370MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen417567945.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen417567945MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen538670546.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen538670546MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3400190971.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3400190971MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1687333339.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1687333339MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen441298479.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen441298479MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1140572558.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1140572558MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen528818139.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen528818139MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen528818140.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen528818140MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen528818141.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen528818141MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2650806512MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare261675381.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare261675381MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1372817087MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3278653252.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3278653252MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1493919688MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3399755853.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3399755853MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen60472817MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1966308982.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1966308982MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2642582481MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare253451350.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare253451350MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1396547621MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3302383786.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3302383786MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2095821700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4001657865.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4001657865MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067281MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903446.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903446MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067282MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903447.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903447MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1484067283MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903448.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903448MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen271665211.h"
#include "mscorlib_System_Action_1_gen271665211MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen392767812.h"
#include "mscorlib_System_Action_1_gen392767812MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3655021551.h"
#include "mscorlib_System_Action_1_gen3655021551MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_EEr3259205415.h"
#include "mscorlib_System_Action_1_gen983143684.h"
#include "mscorlib_System_Action_1_gen983143684MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerra587327548.h"
#include "mscorlib_System_Action_1_gen828034096.h"
#include "mscorlib_System_Action_1_gen828034096MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3206263253.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3206263253MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1756209413.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2095059871.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2095059871MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen645006031.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2336740304.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2336740304MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen886686464.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1756209413MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen645006031MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen886686464MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155190829.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155190829MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3372848153.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1949385224.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1949385224MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3167042548.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen798349321.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen798349321MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2016006645.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen970376284.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen970376284MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2188033608.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3554108690.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3554108690MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644952336.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644952336MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644965214.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644965214MethodDeclarations.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen533949290.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen533949290MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen904941831.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen904941831MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2122599155.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4127192417.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4127192417MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2849202992.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2849202992MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3369230641.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3369230641MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_291920669.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432734252.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432734252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_355424280.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1327961296.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1327961296MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005001078.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005001078MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen727011653.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen727011653MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen848114254.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen848114254MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen875830501.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen875830501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2812853368.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2812853368MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030510692.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen500425236.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen500425236MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21718082560.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen846010146.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen846010146MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2063667470.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1042872857.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1042872857MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2260530181.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen854365966.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen854365966MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot2072023290.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3066004003.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3066004003MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen736693307.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen736693307MethodDeclarations.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2650569241.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2650569241MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148414MethodDeclarations.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148472.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148472MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148567.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148567MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2792744647.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2792744647MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1841955665.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1841955665MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2083636098.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2083636098MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1990166460.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1990166460MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3207823784.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3737689414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3737689414MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi660379442.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen136423630.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen136423630MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo1354080954.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3819239998.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3819239998MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen896245509.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen896245509MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC2113902833.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2795948550.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2795948550MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4013605874.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1203046106.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1203046106MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2420703430.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4239079749.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4239079749MethodDeclarations.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3844211903.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3844211903MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3074261648.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3074261648MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2593882473.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2593882473MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3490832959.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3490832959MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977895.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977895MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977953.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977953MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101978048.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101978048MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen73011651.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen73011651MethodDeclarations.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2440554239.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2440554239MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3658211563.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen531556423.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen531556423MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope1749213747.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3335230866.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3335230866MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1640814777.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1640814777MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4222924441.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4222924441MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1493984525.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1493984525MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2976889581.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2976889581MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3676163660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3676163660MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3320393320.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3320393320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"

// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240* p0, CustomAttributeNamedArgument_t3059612989  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251* p0, CustomAttributeTypedArgument_t3301293422  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t3372848153  Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249(__this, p0, method) ((  TableRange_t3372848153  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2016006645  Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273(__this, p0, method) ((  TagName_t2016006645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2188033608  Array_InternalArray__get_Item_TisArraySegment_1_t2188033608_m3907926617_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2188033608_m3907926617(__this, p0, method) ((  ArraySegment_1_t2188033608  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2188033608_m3907926617_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t476798718_m1243823257_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t476798718_m1243823257(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t476798718_m1243823257_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t2862609660_m3484475127_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t2862609660_m3484475127(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t2862609660_m3484475127_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t2862622538_m125306601_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t2862622538_m125306601(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t2862622538_m125306601_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t1751606614  Array_InternalArray__get_Item_TisDictionaryEntry_t1751606614_m297283038_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t1751606614_m297283038(__this, p0, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t1751606614_m297283038_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t2122599155  Array_InternalArray__get_Item_TisLink_t2122599155_m1741943033_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2122599155_m1741943033(__this, p0, method) ((  Link_t2122599155  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2122599155_m1741943033_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t1049882445  Array_InternalArray__get_Item_TisKeyValuePair_2_t1049882445_m876792353_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1049882445_m876792353(__this, p0, method) ((  KeyValuePair_2_t1049882445  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1049882445_m876792353_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t4066860316  Array_InternalArray__get_Item_TisKeyValuePair_2_t4066860316_m203509488_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t4066860316_m203509488(__this, p0, method) ((  KeyValuePair_2_t4066860316  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t4066860316_m203509488_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32)
extern "C"  KeyValuePair_2_t291920669  Array_InternalArray__get_Item_TisKeyValuePair_2_t291920669_m63444870_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t291920669_m63444870(__this, p0, method) ((  KeyValuePair_2_t291920669  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t291920669_m63444870_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
extern "C"  KeyValuePair_2_t355424280  Array_InternalArray__get_Item_TisKeyValuePair_2_t355424280_m299048577_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t355424280_m299048577(__this, p0, method) ((  KeyValuePair_2_t355424280  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t355424280_m299048577_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t2545618620  Array_InternalArray__get_Item_TisKeyValuePair_2_t2545618620_m2380168102_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2545618620_m2380168102(__this, p0, method) ((  KeyValuePair_2_t2545618620  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2545618620_m2380168102_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3222658402  Array_InternalArray__get_Item_TisKeyValuePair_2_t3222658402_m1457368332_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3222658402_m1457368332(__this, p0, method) ((  KeyValuePair_2_t3222658402  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3222658402_m1457368332_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1944668977  Array_InternalArray__get_Item_TisKeyValuePair_2_t1944668977_m1021495653_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1944668977_m1021495653(__this, p0, method) ((  KeyValuePair_2_t1944668977  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1944668977_m1021495653_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>(System.Int32)
extern "C"  KeyValuePair_2_t2065771578  Array_InternalArray__get_Item_TisKeyValuePair_2_t2065771578_m405895278_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2065771578_m405895278(__this, p0, method) ((  KeyValuePair_2_t2065771578  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2065771578_m405895278_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
extern "C"  KeyValuePair_2_t2093487825  Array_InternalArray__get_Item_TisKeyValuePair_2_t2093487825_m3874903301_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2093487825_m3874903301(__this, p0, method) ((  KeyValuePair_2_t2093487825  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2093487825_m3874903301_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
extern "C"  KeyValuePair_2_t4030510692  Array_InternalArray__get_Item_TisKeyValuePair_2_t4030510692_m3707519917_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t4030510692_m3707519917(__this, p0, method) ((  KeyValuePair_2_t4030510692  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t4030510692_m3707519917_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1718082560  Array_InternalArray__get_Item_TisKeyValuePair_2_t1718082560_m3696534513_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1718082560_m3696534513(__this, p0, method) ((  KeyValuePair_2_t1718082560  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1718082560_m3696534513_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2063667470  Array_InternalArray__get_Item_TisLink_t2063667470_m818406549_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2063667470_m818406549(__this, p0, method) ((  Link_t2063667470  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2063667470_m818406549_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2260530181  Array_InternalArray__get_Item_TisSlot_t2260530181_m2684325401_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2260530181_m2684325401(__this, p0, method) ((  Slot_t2260530181  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2260530181_m2684325401_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2072023290  Array_InternalArray__get_Item_TisSlot_t2072023290_m2216062440_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2072023290_m2216062440(__this, p0, method) ((  Slot_t2072023290  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2072023290_m2216062440_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t4283661327  Array_InternalArray__get_Item_TisDateTime_t4283661327_m185788548_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t4283661327_m185788548(__this, p0, method) ((  DateTime_t4283661327  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t4283661327_m185788548_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t1954350631  Array_InternalArray__get_Item_TisDecimal_t1954350631_m4127931984_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1954350631_m4127931984(__this, p0, method) ((  Decimal_t1954350631  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1954350631_m4127931984_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t3868226565_m3142342990_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t3868226565_m3142342990(__this, p0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t3868226565_m3142342990_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t1153838442_m2614347053_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t1153838442_m2614347053(__this, p0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t1153838442_m2614347053_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t1153838500_m3068135859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t1153838500_m3068135859(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t1153838500_m3068135859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t1153838595_m1812029300_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t1153838595_m1812029300(__this, p0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t1153838595_m1812029300_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m1819425504(__this, p0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m1537058848(__this, p0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t3059612989  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667(__this, p0, method) ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t3301293422  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746(__this, p0, method) ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t3207823784  Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434(__this, p0, method) ((  LabelData_t3207823784  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t660379442  Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500(__this, p0, method) ((  LabelFixup_t660379442  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t1354080954  Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117(__this, p0, method) ((  ILTokenInfo_t1354080954  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t741930026  Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410(__this, p0, method) ((  ParameterModifier_t741930026  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t2113902833  Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445(__this, p0, method) ((  ResourceCacheItem_t2113902833  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t4013605874  Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776(__this, p0, method) ((  ResourceInfo_t4013605874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230(__this, p0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t766901931  Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218(__this, p0, method) ((  X509ChainStatus_t766901931  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3811539797  Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3811539797_m160824484(__this, p0, method) ((  Mark_t3811539797  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t413522987  Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760(__this, p0, method) ((  TimeSpan_t413522987  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t1290668975  Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123(__this, p0, method) ((  UriScheme_t1290668975  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t3658211563  Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851(__this, p0, method) ((  NsDecl_t3658211563  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t1749213747  Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815(__this, p0, method) ((  NsScope_t1749213747  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Tutorial>(System.Int32)
extern "C"  Tutorial_t257920894  Array_InternalArray__get_Item_TisTutorial_t257920894_m3414223204_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTutorial_t257920894_m3414223204(__this, p0, method) ((  Tutorial_t257920894  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTutorial_t257920894_m3414223204_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern "C"  FadeEntry_t2858472101  Array_InternalArray__get_Item_TisFadeEntry_t2858472101_m2179658461_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFadeEntry_t2858472101_m2179658461(__this, p0, method) ((  FadeEntry_t2858472101  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFadeEntry_t2858472101_m2179658461_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern "C"  DepthEntry_t1145614469  Array_InternalArray__get_Item_TisDepthEntry_t1145614469_m1694698009_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDepthEntry_t1145614469_m1694698009(__this, p0, method) ((  DepthEntry_t1145614469  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDepthEntry_t1145614469_m1694698009_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern "C"  Bounds_t2711641849  Array_InternalArray__get_Item_TisBounds_t2711641849_m3868888566_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBounds_t2711641849_m3868888566(__this, p0, method) ((  Bounds_t2711641849  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBounds_t2711641849_m3868888566_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t4194546905  Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850(__this, p0, method) ((  Color_t4194546905  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t598853688  Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403(__this, p0, method) ((  Color32_t598853688  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t243083348  Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859(__this, p0, method) ((  ContactPoint_t243083348  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m1718713285_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2964315886_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3369904907_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_2();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m1034705143_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t2650806512 * L_2 = (BetterList_1_t2650806512 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t2650806512 * L_4 = (BetterList_1_t2650806512 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t2650806512 * L_11 = (BetterList_1_t2650806512 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m2314308290_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Int32>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m3660113522_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m3660113522_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m3660113522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m3193527314_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3529633817_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1115631892_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m2121714850_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t1372817087 * L_2 = (BetterList_1_t1372817087 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t1372817087 * L_4 = (BetterList_1_t1372817087 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t1372817087 * L_11 = (BetterList_1_t1372817087 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m2271382479_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m839960255_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m839960255_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m839960255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Single>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m2851489243_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<System.Single>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  float U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1983038434_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1120723179_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_U24current_2();
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<System.Single>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m127967801_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t1493919688 * L_2 = (BetterList_1_t1493919688 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		SingleU5BU5D_t2316563989* L_3 = (SingleU5BU5D_t2316563989*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t1493919688 * L_4 = (BetterList_1_t1493919688 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		SingleU5BU5D_t2316563989* L_5 = (SingleU5BU5D_t2316563989*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		float L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t1493919688 * L_11 = (BetterList_1_t1493919688 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Single>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m4285278040_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<System.Single>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m497922184_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m497922184_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m497922184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m2264592155_gshared (U3CGetEnumeratorU3Ec__IteratorD_t514942645 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  FadeEntry_t2858472101  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2830196868_gshared (U3CGetEnumeratorU3Ec__IteratorD_t514942645 * __this, const MethodInfo* method)
{
	{
		FadeEntry_t2858472101  L_0 = (FadeEntry_t2858472101 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3430429877_gshared (U3CGetEnumeratorU3Ec__IteratorD_t514942645 * __this, const MethodInfo* method)
{
	{
		FadeEntry_t2858472101  L_0 = (FadeEntry_t2858472101 )__this->get_U24current_2();
		FadeEntry_t2858472101  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m2103317217_gshared (U3CGetEnumeratorU3Ec__IteratorD_t514942645 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t60472817 * L_2 = (BetterList_1_t60472817 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		FadeEntryU5BU5D_t3343031592* L_3 = (FadeEntryU5BU5D_t3343031592*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t60472817 * L_4 = (BetterList_1_t60472817 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		FadeEntryU5BU5D_t3343031592* L_5 = (FadeEntryU5BU5D_t3343031592*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		FadeEntry_t2858472101  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t60472817 * L_11 = (BetterList_1_t60472817 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m2917892248_gshared (U3CGetEnumeratorU3Ec__IteratorD_t514942645 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<TypewriterEffect/FadeEntry>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m4205992392_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m4205992392_gshared (U3CGetEnumeratorU3Ec__IteratorD_t514942645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m4205992392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m1091762361_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  DepthEntry_t1145614469  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3363284864_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * __this, const MethodInfo* method)
{
	{
		DepthEntry_t1145614469  L_0 = (DepthEntry_t1145614469 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1861223565_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * __this, const MethodInfo* method)
{
	{
		DepthEntry_t1145614469  L_0 = (DepthEntry_t1145614469 )__this->get_U24current_2();
		DepthEntry_t1145614469  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m2116538779_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t2642582481 * L_2 = (BetterList_1_t2642582481 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		DepthEntryU5BU5D_t1472539592* L_3 = (DepthEntryU5BU5D_t1472539592*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t2642582481 * L_4 = (BetterList_1_t2642582481 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		DepthEntryU5BU5D_t1472539592* L_5 = (DepthEntryU5BU5D_t1472539592*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		DepthEntry_t1145614469  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t2642582481 * L_11 = (BetterList_1_t2642582481 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m1109891766_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UICamera/DepthEntry>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m3033162598_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m3033162598_gshared (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m3033162598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m2307115968_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color_t4194546905  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m513946695_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = (Color_t4194546905 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m2030332070_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = (Color_t4194546905 )__this->get_U24current_2();
		Color_t4194546905  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m17420340_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t1396547621 * L_2 = (BetterList_1_t1396547621 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ColorU5BU5D_t2441545636* L_3 = (ColorU5BU5D_t2441545636*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t1396547621 * L_4 = (BetterList_1_t1396547621 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		ColorU5BU5D_t2441545636* L_5 = (ColorU5BU5D_t2441545636*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Color_t4194546905  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t1396547621 * L_11 = (BetterList_1_t1396547621 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m833603581_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m4248516205_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m4248516205_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m4248516205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m3524404383_gshared (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color32_t598853688  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1911893862_gshared (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * __this, const MethodInfo* method)
{
	{
		Color32_t598853688  L_0 = (Color32_t598853688 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m836268135_gshared (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * __this, const MethodInfo* method)
{
	{
		Color32_t598853688  L_0 = (Color32_t598853688 )__this->get_U24current_2();
		Color32_t598853688  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m3971830773_gshared (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t2095821700 * L_2 = (BetterList_1_t2095821700 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Color32U5BU5D_t2960766953* L_3 = (Color32U5BU5D_t2960766953*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t2095821700 * L_4 = (BetterList_1_t2095821700 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Color32U5BU5D_t2960766953* L_5 = (Color32U5BU5D_t2960766953*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Color32_t598853688  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t2095821700 * L_11 = (BetterList_1_t2095821700 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m2416665884_gshared (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Color32>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m1170837324_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m1170837324_gshared (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m1170837324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m1489487532_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector2_t4282066565  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3894125427_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = (Vector2_t4282066565 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1620181626_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = (Vector2_t4282066565 )__this->get_U24current_2();
		Vector2_t4282066565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m932338376_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t1484067281 * L_2 = (BetterList_1_t1484067281 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Vector2U5BU5D_t4024180168* L_3 = (Vector2U5BU5D_t4024180168*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t1484067281 * L_4 = (BetterList_1_t1484067281 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Vector2U5BU5D_t4024180168* L_5 = (Vector2U5BU5D_t4024180168*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Vector2_t4282066565  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t1484067281 * L_11 = (BetterList_1_t1484067281 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m1071691753_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector2>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m3430887769_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m3430887769_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m3430887769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m3987503469_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector3_t4282066566  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4174475316_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = (Vector3_t4282066566 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m2589762649_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = (Vector3_t4282066566 )__this->get_U24current_2();
		Vector3_t4282066566  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m426779751_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t1484067282 * L_2 = (BetterList_1_t1484067282 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Vector3U5BU5D_t215400611* L_3 = (Vector3U5BU5D_t215400611*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t1484067282 * L_4 = (BetterList_1_t1484067282 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Vector3U5BU5D_t215400611* L_5 = (Vector3U5BU5D_t215400611*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Vector3_t4282066566  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t1484067282 * L_11 = (BetterList_1_t1484067282 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m778288746_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector3>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m1633936410_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m1633936410_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m1633936410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD__ctor_m2190552110_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector4_t4282066567  U3CGetEnumeratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m159857909_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * __this, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = (Vector4_t4282066567 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3559343672_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * __this, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = (Vector4_t4282066567 )__this->get_U24current_2();
		Vector4_t4282066567  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__IteratorD_MoveNext_m4216188422_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t1484067283 * L_2 = (BetterList_1_t1484067283 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Vector4U5BU5D_t701588350* L_3 = (Vector4U5BU5D_t701588350*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t1484067283 * L_4 = (BetterList_1_t1484067283 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Vector4U5BU5D_t701588350* L_5 = (Vector4U5BU5D_t701588350*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Vector4_t4282066567  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t1484067283 * L_11 = (BetterList_1_t1484067283 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		int32_t L_12 = (int32_t)L_11->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Dispose_m484885739_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__IteratorD<UnityEngine.Vector4>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__IteratorD_Reset_m4131952347_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__IteratorD_Reset_m4131952347_gshared (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__IteratorD_Reset_m4131952347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/CompareFunc<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m2813669491_gshared (CompareFunc_t1695557370 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<System.Int32>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m2755697915_gshared (CompareFunc_t1695557370 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m2755697915((CompareFunc_t1695557370 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<System.Int32>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m1350408102_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m1350408102_gshared (CompareFunc_t1695557370 * __this, int32_t ___left0, int32_t ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m1350408102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2654356369_gshared (CompareFunc_t1695557370 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m3334039642_gshared (CompareFunc_t417567945 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<System.Object>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m529220816_gshared (CompareFunc_t417567945 * __this, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m529220816((CompareFunc_t417567945 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m2181926439_gshared (CompareFunc_t417567945 * __this, Il2CppObject * ___left0, Il2CppObject * ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___left0;
	__d_args[1] = ___right1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2267781532_gshared (CompareFunc_t417567945 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m3446778097_gshared (CompareFunc_t538670546 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<System.Single>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m3147571737_gshared (CompareFunc_t538670546 * __this, float ___left0, float ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m3147571737((CompareFunc_t538670546 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, float ___left0, float ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, float ___left0, float ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<System.Single>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m753512048_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m753512048_gshared (CompareFunc_t538670546 * __this, float ___left0, float ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m753512048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2488384947_gshared (CompareFunc_t538670546 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m2987098333_gshared (CompareFunc_t3400190971 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m2695003217_gshared (CompareFunc_t3400190971 * __this, FadeEntry_t2858472101  ___left0, FadeEntry_t2858472101  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m2695003217((CompareFunc_t3400190971 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, FadeEntry_t2858472101  ___left0, FadeEntry_t2858472101  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, FadeEntry_t2858472101  ___left0, FadeEntry_t2858472101  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* FadeEntry_t2858472101_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m2795945852_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m2795945852_gshared (CompareFunc_t3400190971 * __this, FadeEntry_t2858472101  ___left0, FadeEntry_t2858472101  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m2795945852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FadeEntry_t2858472101_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(FadeEntry_t2858472101_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m642779259_gshared (CompareFunc_t3400190971 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UICamera/DepthEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m1393544147_gshared (CompareFunc_t1687333339 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UICamera/DepthEntry>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m3104379639_gshared (CompareFunc_t1687333339 * __this, DepthEntry_t1145614469  ___left0, DepthEntry_t1145614469  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m3104379639((CompareFunc_t1687333339 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, DepthEntry_t1145614469  ___left0, DepthEntry_t1145614469  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, DepthEntry_t1145614469  ___left0, DepthEntry_t1145614469  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UICamera/DepthEntry>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* DepthEntry_t1145614469_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m3444521166_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m3444521166_gshared (CompareFunc_t1687333339 * __this, DepthEntry_t1145614469  ___left0, DepthEntry_t1145614469  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m3444521166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(DepthEntry_t1145614469_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(DepthEntry_t1145614469_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UICamera/DepthEntry>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2125902869_gshared (CompareFunc_t1687333339 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m1713620716_gshared (CompareFunc_t441298479 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Color>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m283192190_gshared (CompareFunc_t441298479 * __this, Color_t4194546905  ___left0, Color_t4194546905  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m283192190((CompareFunc_t441298479 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t4194546905  ___left0, Color_t4194546905  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Color_t4194546905  ___left0, Color_t4194546905  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Color>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m1887011797_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m1887011797_gshared (CompareFunc_t441298479 * __this, Color_t4194546905  ___left0, Color_t4194546905  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m1887011797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color_t4194546905_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Color_t4194546905_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m4199524398_gshared (CompareFunc_t441298479 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m2461687853_gshared (CompareFunc_t1140572558 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Color32>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m488889309_gshared (CompareFunc_t1140572558 * __this, Color32_t598853688  ___left0, Color32_t598853688  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m488889309((CompareFunc_t1140572558 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t598853688  ___left0, Color32_t598853688  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Color32_t598853688  ___left0, Color32_t598853688  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Color32>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t598853688_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m149463988_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m149463988_gshared (CompareFunc_t1140572558 * __this, Color32_t598853688  ___left0, Color32_t598853688  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m149463988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color32_t598853688_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Color32_t598853688_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m3946677615_gshared (CompareFunc_t1140572558 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m70049280_gshared (CompareFunc_t528818139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector2>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m753905514_gshared (CompareFunc_t528818139 * __this, Vector2_t4282066565  ___left0, Vector2_t4282066565  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m753905514((CompareFunc_t528818139 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t4282066565  ___left0, Vector2_t4282066565  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector2_t4282066565  ___left0, Vector2_t4282066565  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Vector2>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m2841454145_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m2841454145_gshared (CompareFunc_t528818139 * __this, Vector2_t4282066565  ___left0, Vector2_t4282066565  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m2841454145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m1902503106_gshared (CompareFunc_t528818139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m1702853279_gshared (CompareFunc_t528818140 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector3>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m2261457323_gshared (CompareFunc_t528818140 * __this, Vector3_t4282066566  ___left0, Vector3_t4282066566  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m2261457323((CompareFunc_t528818140 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t4282066566  ___left0, Vector3_t4282066566  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector3_t4282066566  ___left0, Vector3_t4282066566  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Vector3>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m3534846850_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m3534846850_gshared (CompareFunc_t528818140 * __this, Vector3_t4282066566  ___left0, Vector3_t4282066566  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m3534846850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2927995105_gshared (CompareFunc_t528818140 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m3335657278_gshared (CompareFunc_t528818141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector4>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m3769009132_gshared (CompareFunc_t528818141 * __this, Vector4_t4282066567  ___left0, Vector4_t4282066567  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m3769009132((CompareFunc_t528818141 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t4282066567  ___left0, Vector4_t4282066567  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector4_t4282066567  ___left0, Vector4_t4282066567  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Vector4>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m4228239555_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m4228239555_gshared (CompareFunc_t528818141 * __this, Vector4_t4282066567  ___left0, Vector4_t4282066567  ___right1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m4228239555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m3953487104_gshared (CompareFunc_t528818141 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1<System.Int32>::.ctor()
extern "C"  void BetterList_1__ctor_m1752750761_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<System.Int32>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m2458670693_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t3105276340 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t3105276340 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t BetterList_1_get_Item_m2264406119_gshared (BetterList_1_t2650806512 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<System.Int32>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m4189497524_gshared (BetterList_1_t2650806512 * __this, int32_t ___i0, int32_t ___value1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		int32_t L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int32_t)L_2);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m1734071085_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m1734071085_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m1734071085_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3230847821* V_0 = NULL;
	Int32U5BU5D_t3230847821* G_B3_0 = NULL;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Int32U5BU5D_t3230847821*)G_B3_0;
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		Int32U5BU5D_t3230847821* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Int32U5BU5D_t3230847821* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Trim()
extern "C"  void BetterList_1_Trim_m3513273629_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	Int32U5BU5D_t3230847821* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Int32U5BU5D_t3230847821* L_2 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Int32U5BU5D_t3230847821* L_4 = V_0;
		int32_t L_5 = V_1;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Int32U5BU5D_t3230847821*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Clear()
extern "C"  void BetterList_1_Clear_m3453851348_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Release()
extern "C"  void BetterList_1_Release_m503310478_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Int32U5BU5D_t3230847821*)NULL);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Add(T)
extern "C"  void BetterList_1_Add_m2956176566_gshared (BetterList_1_t2650806512 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Int32U5BU5D_t3230847821* L_2 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2650806512 *)__this);
		((  void (*) (BetterList_1_t2650806512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t2650806512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		int32_t L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m2719791901_gshared (BetterList_1_t2650806512 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Int32U5BU5D_t3230847821* L_2 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2650806512 *)__this);
		((  void (*) (BetterList_1_t2650806512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t2650806512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Int32U5BU5D_t3230847821* L_9 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_16 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		int32_t L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (int32_t)L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		int32_t L_20 = ___item1;
		NullCheck((BetterList_1_t2650806512 *)__this);
		((  void (*) (BetterList_1_t2650806512 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t2650806512 *)__this, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<System.Int32>::Contains(T)
extern "C"  bool BetterList_1_Contains_m1402378384_gshared (BetterList_1_t2650806512 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = ___item0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Int32_Equals_m4061110258((int32_t*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<System.Int32>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m2360506524_gshared (BetterList_1_t2650806512 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = ___item0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Int32_Equals_m4061110258((int32_t*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<System.Int32>::Remove(T)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m1069038731_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m1069038731_gshared (BetterList_1_t2650806512 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m1069038731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t261675381 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t261675381 * L_1 = ((  EqualityComparer_1_t261675381 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t261675381 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t261675381 * L_2 = V_0;
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = ___item0;
		NullCheck((EqualityComparer_1_t261675381 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(!0,!0) */, (EqualityComparer_1_t261675381 *)L_2, (int32_t)L_6, (int32_t)L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_10 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_3));
		int32_t L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int32_t)L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		Int32U5BU5D_t3230847821* L_14 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		Int32U5BU5D_t3230847821* L_16 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (int32_t)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_23 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (int32_t)L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<System.Int32>::RemoveAt(System.Int32)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m593644771_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m593644771_gshared (BetterList_1_t2650806512 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m593644771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Int32U5BU5D_t3230847821* L_9 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Int32U5BU5D_t3230847821* L_11 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (int32_t)L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (int32_t)L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<System.Int32>::Pop()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m3036224441_MetadataUsageId;
extern "C"  int32_t BetterList_1_Pop_m3036224441_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m3036224441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_2 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (int32_t)L_7;
		Int32U5BU5D_t3230847821* L_8 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int32_t)L_10);
		int32_t L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_3));
		int32_t L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t3230847821* BetterList_1_ToArray_m3197892328_gshared (BetterList_1_t2650806512 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t2650806512 *)__this);
		((  void (*) (BetterList_1_t2650806512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t2650806512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<System.Int32>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m89075084_gshared (BetterList_1_t2650806512 * __this, CompareFunc_t1695557370 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t1695557370 * L_2 = ___comparer0;
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t1695557370 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t1695557370 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t1695557370 *)L_2, (int32_t)L_6, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_12 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (int32_t)L_15;
		Int32U5BU5D_t3230847821* L_16 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (int32_t)L_21);
		Int32U5BU5D_t3230847821* L_22 = (Int32U5BU5D_t3230847821*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (int32_t)L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<System.Object>::.ctor()
extern "C"  void BetterList_1__ctor_m4248689070_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m3505227298_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1827286915 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t1827286915 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * BetterList_1_get_Item_m638315300_gshared (BetterList_1_t1372817087 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m747172943_gshared (BetterList_1_t1372817087 * __this, int32_t ___i0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		return;
	}
}
// System.Void BetterList`1<System.Object>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m19911816_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m19911816_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m19911816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	ObjectU5BU5D_t1108656482* G_B3_0 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (ObjectU5BU5D_t1108656482*)G_B3_0;
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		ObjectU5BU5D_t1108656482* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		ObjectU5BU5D_t1108656482* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Trim()
extern "C"  void BetterList_1_Trim_m3593787768_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		ObjectU5BU5D_t1108656482* L_4 = V_0;
		int32_t L_5 = V_1;
		ObjectU5BU5D_t1108656482* L_6 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((ObjectU5BU5D_t1108656482*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<System.Object>::Clear()
extern "C"  void BetterList_1_Clear_m1654822361_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Release()
extern "C"  void BetterList_1_Release_m2508274259_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((ObjectU5BU5D_t1108656482*)NULL);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Add(T)
extern "C"  void BetterList_1_Add_m3036690705_gshared (BetterList_1_t1372817087 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1372817087 *)__this);
		((  void (*) (BetterList_1_t1372817087 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1372817087 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Il2CppObject * L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_7);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m548614520_gshared (BetterList_1_t1372817087 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1372817087 *)__this);
		((  void (*) (BetterList_1_t1372817087 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1372817087 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		ObjectU5BU5D_t1108656482* L_7 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		ObjectU5BU5D_t1108656482* L_9 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Il2CppObject *)L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_16 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		Il2CppObject * L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Il2CppObject *)L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Il2CppObject * L_20 = ___item1;
		NullCheck((BetterList_1_t1372817087 *)__this);
		((  void (*) (BetterList_1_t1372817087 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t1372817087 *)__this, (Il2CppObject *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<System.Object>::Contains(T)
extern "C"  bool BetterList_1_Contains_m1227426045_gshared (BetterList_1_t1372817087 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Il2CppObject * L_3 = ___item0;
		NullCheck((Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<System.Object>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m3505521179_gshared (BetterList_1_t1372817087 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Il2CppObject * L_3 = ___item0;
		NullCheck((Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_5 = V_0;
		return L_5;
	}

IL_0038:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<System.Object>::Remove(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m1444275256_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m1444275256_gshared (BetterList_1_t1372817087 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m1444275256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t3278653252 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t3278653252 * L_1 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t3278653252 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t3278653252 * L_2 = V_0;
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Il2CppObject * L_7 = ___item0;
		NullCheck((EqualityComparer_1_t3278653252 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t3278653252 *)L_2, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		ObjectU5BU5D_t1108656482* L_10 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Il2CppObject *)L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		ObjectU5BU5D_t1108656482* L_14 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		ObjectU5BU5D_t1108656482* L_16 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		Il2CppObject * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_23 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Il2CppObject *)L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m2717434686_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m2717434686_gshared (BetterList_1_t1372817087 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m2717434686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		ObjectU5BU5D_t1108656482* L_9 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_18 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Il2CppObject *)L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<System.Object>::Pop()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1337402204_MetadataUsageId;
extern "C"  Il2CppObject * BetterList_1_Pop_m1337402204_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1337402204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (Il2CppObject *)L_7;
		ObjectU5BU5D_t1108656482* L_8 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Il2CppObject *)L_10);
		Il2CppObject * L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t1108656482* BetterList_1_ToArray_m1744488967_gshared (BetterList_1_t1372817087 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t1372817087 *)__this);
		((  void (*) (BetterList_1_t1372817087 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t1372817087 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<System.Object>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1264107601_gshared (BetterList_1_t1372817087 * __this, CompareFunc_t417567945 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t417567945 * L_2 = ___comparer0;
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		ObjectU5BU5D_t1108656482* L_7 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t417567945 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t417567945 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t417567945 *)L_2, (Il2CppObject *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_12 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (Il2CppObject *)L_15;
		ObjectU5BU5D_t1108656482* L_16 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		ObjectU5BU5D_t1108656482* L_18 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		Il2CppObject * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Il2CppObject *)L_21);
		ObjectU5BU5D_t1108656482* L_22 = (ObjectU5BU5D_t1108656482*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		Il2CppObject * L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (Il2CppObject *)L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<System.Single>::.ctor()
extern "C"  void BetterList_1__ctor_m3906650999_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<System.Single>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m323786475_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1948389516 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t1948389516 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<System.Single>::get_Item(System.Int32)
extern "C"  float BetterList_1_get_Item_m3982956091_gshared (BetterList_1_t1493919688 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<System.Single>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m2311435686_gshared (BetterList_1_t1493919688 * __this, int32_t ___i0, float ___value1, const MethodInfo* method)
{
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		float L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (float)L_2);
		return;
	}
}
// System.Void BetterList`1<System.Single>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m2411136671_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m2411136671_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m2411136671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t2316563989* V_0 = NULL;
	SingleU5BU5D_t2316563989* G_B3_0 = NULL;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_1 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((SingleU5BU5D_t2316563989*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((SingleU5BU5D_t2316563989*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (SingleU5BU5D_t2316563989*)G_B3_0;
		SingleU5BU5D_t2316563989* L_3 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_5 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		SingleU5BU5D_t2316563989* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		SingleU5BU5D_t2316563989* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Trim()
extern "C"  void BetterList_1_Trim_m534712975_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	SingleU5BU5D_t2316563989* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		SingleU5BU5D_t2316563989* L_2 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (SingleU5BU5D_t2316563989*)((SingleU5BU5D_t2316563989*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		SingleU5BU5D_t2316563989* L_4 = V_0;
		int32_t L_5 = V_1;
		SingleU5BU5D_t2316563989* L_6 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		float L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (float)L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((SingleU5BU5D_t2316563989*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<System.Single>::Clear()
extern "C"  void BetterList_1_Clear_m1312784290_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Release()
extern "C"  void BetterList_1_Release_m227202524_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((SingleU5BU5D_t2316563989*)NULL);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Add(T)
extern "C"  void BetterList_1_Add_m4272583208_gshared (BetterList_1_t1493919688 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		SingleU5BU5D_t2316563989* L_2 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1493919688 *)__this);
		((  void (*) (BetterList_1_t1493919688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1493919688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		SingleU5BU5D_t2316563989* L_3 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		float L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_7);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m3893255311_gshared (BetterList_1_t1493919688 * __this, int32_t ___index0, float ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		SingleU5BU5D_t2316563989* L_2 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1493919688 *)__this);
		((  void (*) (BetterList_1_t1493919688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1493919688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		SingleU5BU5D_t2316563989* L_7 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		SingleU5BU5D_t2316563989* L_9 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		float L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_16 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		float L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (float)L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		float L_20 = ___item1;
		NullCheck((BetterList_1_t1493919688 *)__this);
		((  void (*) (BetterList_1_t1493919688 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t1493919688 *)__this, (float)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<System.Single>::Contains(T)
extern "C"  bool BetterList_1_Contains_m3845776966_gshared (BetterList_1_t1493919688 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		SingleU5BU5D_t2316563989* L_1 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		float L_3 = ___item0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Single_Equals_m2650902624((float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<System.Single>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1511774130_gshared (BetterList_1_t1493919688 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		SingleU5BU5D_t2316563989* L_1 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		float L_3 = ___item0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Single_Equals_m2650902624((float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<System.Single>::Remove(T)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m3458170817_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m3458170817_gshared (BetterList_1_t1493919688 * __this, float ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m3458170817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t3399755853 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t3399755853 * L_1 = ((  EqualityComparer_1_t3399755853 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t3399755853 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t3399755853 * L_2 = V_0;
		SingleU5BU5D_t2316563989* L_3 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		float L_7 = ___item0;
		NullCheck((EqualityComparer_1_t3399755853 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(!0,!0) */, (EqualityComparer_1_t3399755853 *)L_2, (float)L_6, (float)L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		SingleU5BU5D_t2316563989* L_10 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_3));
		float L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (float)L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		SingleU5BU5D_t2316563989* L_14 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		SingleU5BU5D_t2316563989* L_16 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		float L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (float)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_23 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_4));
		float L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (float)L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<System.Single>::RemoveAt(System.Int32)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m1767108181_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m1767108181_gshared (BetterList_1_t1493919688 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m1767108181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		SingleU5BU5D_t2316563989* L_5 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_1));
		float L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		SingleU5BU5D_t2316563989* L_9 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		SingleU5BU5D_t2316563989* L_11 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		float L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (float)L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_18 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_2));
		float L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (float)L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<System.Single>::Pop()
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m2347101029_MetadataUsageId;
extern "C"  float BetterList_1_Pop_m2347101029_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m2347101029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_2 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		float L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (float)L_7;
		SingleU5BU5D_t2316563989* L_8 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_2));
		float L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (float)L_10);
		float L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_3));
		float L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<System.Single>::ToArray()
extern "C"  SingleU5BU5D_t2316563989* BetterList_1_ToArray_m3758384528_gshared (BetterList_1_t1493919688 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t1493919688 *)__this);
		((  void (*) (BetterList_1_t1493919688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t1493919688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		SingleU5BU5D_t2316563989* L_0 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<System.Single>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1960480986_gshared (BetterList_1_t1493919688 * __this, CompareFunc_t538670546 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t538670546 * L_2 = ___comparer0;
		SingleU5BU5D_t2316563989* L_3 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t2316563989* L_7 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		float L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t538670546 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t538670546 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t538670546 *)L_2, (float)L_6, (float)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_12 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		float L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (float)L_15;
		SingleU5BU5D_t2316563989* L_16 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		SingleU5BU5D_t2316563989* L_18 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		float L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (float)L_21);
		SingleU5BU5D_t2316563989* L_22 = (SingleU5BU5D_t2316563989*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		float L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (float)L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void BetterList_1__ctor_m2301652735_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<TypewriterEffect/FadeEntry>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m3698161083_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t514942645 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t514942645 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t514942645 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t514942645 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t514942645 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t514942645 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t514942645 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<TypewriterEffect/FadeEntry>::get_Item(System.Int32)
extern "C"  FadeEntry_t2858472101  BetterList_1_get_Item_m1644696337_gshared (BetterList_1_t60472817 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		FadeEntry_t2858472101  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m3717958430_gshared (BetterList_1_t60472817 * __this, int32_t ___i0, FadeEntry_t2858472101  ___value1, const MethodInfo* method)
{
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		FadeEntry_t2858472101  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (FadeEntry_t2858472101 )L_2);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m1851445783_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m1851445783_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m1851445783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FadeEntryU5BU5D_t3343031592* V_0 = NULL;
	FadeEntryU5BU5D_t3343031592* G_B3_0 = NULL;
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_1 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((FadeEntryU5BU5D_t3343031592*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((FadeEntryU5BU5D_t3343031592*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (FadeEntryU5BU5D_t3343031592*)G_B3_0;
		FadeEntryU5BU5D_t3343031592* L_3 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_5 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		FadeEntryU5BU5D_t3343031592* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		FadeEntryU5BU5D_t3343031592* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Trim()
extern "C"  void BetterList_1_Trim_m2145506823_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	FadeEntryU5BU5D_t3343031592* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		FadeEntryU5BU5D_t3343031592* L_2 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (FadeEntryU5BU5D_t3343031592*)((FadeEntryU5BU5D_t3343031592*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		FadeEntryU5BU5D_t3343031592* L_4 = V_0;
		int32_t L_5 = V_1;
		FadeEntryU5BU5D_t3343031592* L_6 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FadeEntry_t2858472101  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (FadeEntry_t2858472101 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((FadeEntryU5BU5D_t3343031592*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Clear()
extern "C"  void BetterList_1_Clear_m4002753322_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Release()
extern "C"  void BetterList_1_Release_m4012097380_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((FadeEntryU5BU5D_t3343031592*)NULL);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Add(T)
extern "C"  void BetterList_1_Add_m1588409760_gshared (BetterList_1_t60472817 * __this, FadeEntry_t2858472101  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		FadeEntryU5BU5D_t3343031592* L_2 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t60472817 *)__this);
		((  void (*) (BetterList_1_t60472817 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t60472817 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		FadeEntryU5BU5D_t3343031592* L_3 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		FadeEntry_t2858472101  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (FadeEntry_t2858472101 )L_7);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m936062983_gshared (BetterList_1_t60472817 * __this, int32_t ___index0, FadeEntry_t2858472101  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		FadeEntryU5BU5D_t3343031592* L_2 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t60472817 *)__this);
		((  void (*) (BetterList_1_t60472817 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t60472817 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		FadeEntryU5BU5D_t3343031592* L_7 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		FadeEntryU5BU5D_t3343031592* L_9 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		FadeEntry_t2858472101  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (FadeEntry_t2858472101 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_16 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		FadeEntry_t2858472101  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (FadeEntry_t2858472101 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		FadeEntry_t2858472101  L_20 = ___item1;
		NullCheck((BetterList_1_t60472817 *)__this);
		((  void (*) (BetterList_1_t60472817 *, FadeEntry_t2858472101 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t60472817 *)__this, (FadeEntry_t2858472101 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<TypewriterEffect/FadeEntry>::Contains(T)
extern "C"  bool BetterList_1_Contains_m2163227878_gshared (BetterList_1_t60472817 * __this, FadeEntry_t2858472101  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		FadeEntryU5BU5D_t3343031592* L_1 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		FadeEntry_t2858472101  L_3 = ___item0;
		FadeEntry_t2858472101  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<TypewriterEffect/FadeEntry>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1957571590_gshared (BetterList_1_t60472817 * __this, FadeEntry_t2858472101  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		FadeEntryU5BU5D_t3343031592* L_1 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		FadeEntry_t2858472101  L_3 = ___item0;
		FadeEntry_t2858472101  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<TypewriterEffect/FadeEntry>::Remove(T)
extern Il2CppClass* FadeEntry_t2858472101_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m582680161_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m582680161_gshared (BetterList_1_t60472817 * __this, FadeEntry_t2858472101  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m582680161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t1966308982 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	FadeEntry_t2858472101  V_3;
	memset(&V_3, 0, sizeof(V_3));
	FadeEntry_t2858472101  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t1966308982 * L_1 = ((  EqualityComparer_1_t1966308982 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t1966308982 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t1966308982 * L_2 = V_0;
		FadeEntryU5BU5D_t3343031592* L_3 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		FadeEntry_t2858472101  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		FadeEntry_t2858472101  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t1966308982 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, FadeEntry_t2858472101 , FadeEntry_t2858472101  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<TypewriterEffect/FadeEntry>::Equals(!0,!0) */, (EqualityComparer_1_t1966308982 *)L_2, (FadeEntry_t2858472101 )L_6, (FadeEntry_t2858472101 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		FadeEntryU5BU5D_t3343031592* L_10 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (FadeEntry_t2858472101_il2cpp_TypeInfo_var, (&V_3));
		FadeEntry_t2858472101  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (FadeEntry_t2858472101 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		FadeEntryU5BU5D_t3343031592* L_14 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		FadeEntryU5BU5D_t3343031592* L_16 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		FadeEntry_t2858472101  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (FadeEntry_t2858472101 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_23 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (FadeEntry_t2858472101_il2cpp_TypeInfo_var, (&V_4));
		FadeEntry_t2858472101  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (FadeEntry_t2858472101 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::RemoveAt(System.Int32)
extern Il2CppClass* FadeEntry_t2858472101_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3104883149_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3104883149_gshared (BetterList_1_t60472817 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3104883149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	FadeEntry_t2858472101  V_1;
	memset(&V_1, 0, sizeof(V_1));
	FadeEntry_t2858472101  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		FadeEntryU5BU5D_t3343031592* L_5 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (FadeEntry_t2858472101_il2cpp_TypeInfo_var, (&V_1));
		FadeEntry_t2858472101  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (FadeEntry_t2858472101 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		FadeEntryU5BU5D_t3343031592* L_9 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		FadeEntryU5BU5D_t3343031592* L_11 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		FadeEntry_t2858472101  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (FadeEntry_t2858472101 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_18 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (FadeEntry_t2858472101_il2cpp_TypeInfo_var, (&V_2));
		FadeEntry_t2858472101  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (FadeEntry_t2858472101 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<TypewriterEffect/FadeEntry>::Pop()
extern Il2CppClass* FadeEntry_t2858472101_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m2212040655_MetadataUsageId;
extern "C"  FadeEntry_t2858472101  BetterList_1_Pop_m2212040655_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m2212040655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FadeEntry_t2858472101  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	FadeEntry_t2858472101  V_2;
	memset(&V_2, 0, sizeof(V_2));
	FadeEntry_t2858472101  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_2 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		FadeEntry_t2858472101  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (FadeEntry_t2858472101 )L_7;
		FadeEntryU5BU5D_t3343031592* L_8 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (FadeEntry_t2858472101_il2cpp_TypeInfo_var, (&V_2));
		FadeEntry_t2858472101  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (FadeEntry_t2858472101 )L_10);
		FadeEntry_t2858472101  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (FadeEntry_t2858472101_il2cpp_TypeInfo_var, (&V_3));
		FadeEntry_t2858472101  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<TypewriterEffect/FadeEntry>::ToArray()
extern "C"  FadeEntryU5BU5D_t3343031592* BetterList_1_ToArray_m3002788990_gshared (BetterList_1_t60472817 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t60472817 *)__this);
		((  void (*) (BetterList_1_t60472817 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t60472817 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		FadeEntryU5BU5D_t3343031592* L_0 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1043423842_gshared (BetterList_1_t60472817 * __this, CompareFunc_t3400190971 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	FadeEntry_t2858472101  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t3400190971 * L_2 = ___comparer0;
		FadeEntryU5BU5D_t3343031592* L_3 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		FadeEntry_t2858472101  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		FadeEntryU5BU5D_t3343031592* L_7 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		FadeEntry_t2858472101  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t3400190971 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t3400190971 *, FadeEntry_t2858472101 , FadeEntry_t2858472101 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t3400190971 *)L_2, (FadeEntry_t2858472101 )L_6, (FadeEntry_t2858472101 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		FadeEntryU5BU5D_t3343031592* L_12 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		FadeEntry_t2858472101  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (FadeEntry_t2858472101 )L_15;
		FadeEntryU5BU5D_t3343031592* L_16 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		FadeEntryU5BU5D_t3343031592* L_18 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		FadeEntry_t2858472101  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (FadeEntry_t2858472101 )L_21);
		FadeEntryU5BU5D_t3343031592* L_22 = (FadeEntryU5BU5D_t3343031592*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		FadeEntry_t2858472101  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (FadeEntry_t2858472101 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::.ctor()
extern "C"  void BetterList_1__ctor_m1815204181_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UICamera/DepthEntry>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m3336479177_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t3097052309 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t3097052309 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UICamera/DepthEntry>::get_Item(System.Int32)
extern "C"  DepthEntry_t1145614469  BetterList_1_get_Item_m1053881309_gshared (BetterList_1_t2642582481 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		DepthEntry_t1145614469  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m508321672_gshared (BetterList_1_t2642582481 * __this, int32_t ___i0, DepthEntry_t1145614469  ___value1, const MethodInfo* method)
{
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		DepthEntry_t1145614469  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (DepthEntry_t1145614469 )L_2);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m1896628993_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m1896628993_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m1896628993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DepthEntryU5BU5D_t1472539592* V_0 = NULL;
	DepthEntryU5BU5D_t1472539592* G_B3_0 = NULL;
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_1 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((DepthEntryU5BU5D_t1472539592*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((DepthEntryU5BU5D_t1472539592*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (DepthEntryU5BU5D_t1472539592*)G_B3_0;
		DepthEntryU5BU5D_t1472539592* L_3 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_5 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		DepthEntryU5BU5D_t1472539592* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		DepthEntryU5BU5D_t1472539592* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Trim()
extern "C"  void BetterList_1_Trim_m882888945_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	DepthEntryU5BU5D_t1472539592* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		DepthEntryU5BU5D_t1472539592* L_2 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (DepthEntryU5BU5D_t1472539592*)((DepthEntryU5BU5D_t1472539592*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		DepthEntryU5BU5D_t1472539592* L_4 = V_0;
		int32_t L_5 = V_1;
		DepthEntryU5BU5D_t1472539592* L_6 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		DepthEntry_t1145614469  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DepthEntry_t1145614469 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((DepthEntryU5BU5D_t1472539592*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Clear()
extern "C"  void BetterList_1_Clear_m3516304768_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Release()
extern "C"  void BetterList_1_Release_m391504954_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((DepthEntryU5BU5D_t1472539592*)NULL);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Add(T)
extern "C"  void BetterList_1_Add_m325791882_gshared (BetterList_1_t2642582481 * __this, DepthEntry_t1145614469  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		DepthEntryU5BU5D_t1472539592* L_2 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2642582481 *)__this);
		((  void (*) (BetterList_1_t2642582481 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t2642582481 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		DepthEntryU5BU5D_t1472539592* L_3 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		DepthEntry_t1145614469  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (DepthEntry_t1145614469 )L_7);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1889146609_gshared (BetterList_1_t2642582481 * __this, int32_t ___index0, DepthEntry_t1145614469  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		DepthEntryU5BU5D_t1472539592* L_2 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2642582481 *)__this);
		((  void (*) (BetterList_1_t2642582481 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t2642582481 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		DepthEntryU5BU5D_t1472539592* L_7 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		DepthEntryU5BU5D_t1472539592* L_9 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		DepthEntry_t1145614469  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (DepthEntry_t1145614469 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_16 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		DepthEntry_t1145614469  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (DepthEntry_t1145614469 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		DepthEntry_t1145614469  L_20 = ___item1;
		NullCheck((BetterList_1_t2642582481 *)__this);
		((  void (*) (BetterList_1_t2642582481 *, DepthEntry_t1145614469 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t2642582481 *)__this, (DepthEntry_t1145614469 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UICamera/DepthEntry>::Contains(T)
extern "C"  bool BetterList_1_Contains_m2157248036_gshared (BetterList_1_t2642582481 * __this, DepthEntry_t1145614469  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		DepthEntryU5BU5D_t1472539592* L_1 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		DepthEntry_t1145614469  L_3 = ___item0;
		DepthEntry_t1145614469  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UICamera/DepthEntry>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m2812979092_gshared (BetterList_1_t2642582481 * __this, DepthEntry_t1145614469  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		DepthEntryU5BU5D_t1472539592* L_1 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		DepthEntry_t1145614469  L_3 = ___item0;
		DepthEntry_t1145614469  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UICamera/DepthEntry>::Remove(T)
extern Il2CppClass* DepthEntry_t1145614469_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m310048543_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m310048543_gshared (BetterList_1_t2642582481 * __this, DepthEntry_t1145614469  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m310048543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t253451350 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	DepthEntry_t1145614469  V_3;
	memset(&V_3, 0, sizeof(V_3));
	DepthEntry_t1145614469  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t253451350 * L_1 = ((  EqualityComparer_1_t253451350 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t253451350 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t253451350 * L_2 = V_0;
		DepthEntryU5BU5D_t1472539592* L_3 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		DepthEntry_t1145614469  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		DepthEntry_t1145614469  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t253451350 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, DepthEntry_t1145614469 , DepthEntry_t1145614469  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UICamera/DepthEntry>::Equals(!0,!0) */, (EqualityComparer_1_t253451350 *)L_2, (DepthEntry_t1145614469 )L_6, (DepthEntry_t1145614469 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		DepthEntryU5BU5D_t1472539592* L_10 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (DepthEntry_t1145614469_il2cpp_TypeInfo_var, (&V_3));
		DepthEntry_t1145614469  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (DepthEntry_t1145614469 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		DepthEntryU5BU5D_t1472539592* L_14 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		DepthEntryU5BU5D_t1472539592* L_16 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		DepthEntry_t1145614469  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (DepthEntry_t1145614469 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_23 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (DepthEntry_t1145614469_il2cpp_TypeInfo_var, (&V_4));
		DepthEntry_t1145614469  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (DepthEntry_t1145614469 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::RemoveAt(System.Int32)
extern Il2CppClass* DepthEntry_t1145614469_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m4057966775_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m4057966775_gshared (BetterList_1_t2642582481 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m4057966775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DepthEntry_t1145614469  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DepthEntry_t1145614469  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		DepthEntryU5BU5D_t1472539592* L_5 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (DepthEntry_t1145614469_il2cpp_TypeInfo_var, (&V_1));
		DepthEntry_t1145614469  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (DepthEntry_t1145614469 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		DepthEntryU5BU5D_t1472539592* L_9 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		DepthEntryU5BU5D_t1472539592* L_11 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		DepthEntry_t1145614469  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (DepthEntry_t1145614469 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_18 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (DepthEntry_t1145614469_il2cpp_TypeInfo_var, (&V_2));
		DepthEntry_t1145614469  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (DepthEntry_t1145614469 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UICamera/DepthEntry>::Pop()
extern Il2CppClass* DepthEntry_t1145614469_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1257557379_MetadataUsageId;
extern "C"  DepthEntry_t1145614469  BetterList_1_Pop_m1257557379_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1257557379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DepthEntry_t1145614469  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	DepthEntry_t1145614469  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DepthEntry_t1145614469  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_2 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		DepthEntry_t1145614469  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (DepthEntry_t1145614469 )L_7;
		DepthEntryU5BU5D_t1472539592* L_8 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (DepthEntry_t1145614469_il2cpp_TypeInfo_var, (&V_2));
		DepthEntry_t1145614469  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (DepthEntry_t1145614469 )L_10);
		DepthEntry_t1145614469  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (DepthEntry_t1145614469_il2cpp_TypeInfo_var, (&V_3));
		DepthEntry_t1145614469  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<UICamera/DepthEntry>::ToArray()
extern "C"  DepthEntryU5BU5D_t1472539592* BetterList_1_ToArray_m2114361902_gshared (BetterList_1_t2642582481 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t2642582481 *)__this);
		((  void (*) (BetterList_1_t2642582481 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t2642582481 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		DepthEntryU5BU5D_t1472539592* L_0 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m3147294008_gshared (BetterList_1_t2642582481 * __this, CompareFunc_t1687333339 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	DepthEntry_t1145614469  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t1687333339 * L_2 = ___comparer0;
		DepthEntryU5BU5D_t1472539592* L_3 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		DepthEntry_t1145614469  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		DepthEntryU5BU5D_t1472539592* L_7 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		DepthEntry_t1145614469  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t1687333339 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t1687333339 *, DepthEntry_t1145614469 , DepthEntry_t1145614469 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t1687333339 *)L_2, (DepthEntry_t1145614469 )L_6, (DepthEntry_t1145614469 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		DepthEntryU5BU5D_t1472539592* L_12 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		DepthEntry_t1145614469  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (DepthEntry_t1145614469 )L_15;
		DepthEntryU5BU5D_t1472539592* L_16 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		DepthEntryU5BU5D_t1472539592* L_18 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		DepthEntry_t1145614469  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (DepthEntry_t1145614469 )L_21);
		DepthEntryU5BU5D_t1472539592* L_22 = (DepthEntryU5BU5D_t1472539592*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		DepthEntry_t1145614469  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (DepthEntry_t1145614469 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::.ctor()
extern "C"  void BetterList_1__ctor_m2692225884_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Color>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m1994410960_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1851017449 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t1851017449 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C"  Color_t4194546905  BetterList_1_get_Item_m872963894_gshared (BetterList_1_t1396547621 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Color_t4194546905  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m3391128673_gshared (BetterList_1_t1396547621 * __this, int32_t ___i0, Color_t4194546905  ___value1, const MethodInfo* method)
{
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Color_t4194546905  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Color_t4194546905 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m4029084442_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m4029084442_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m4029084442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorU5BU5D_t2441545636* V_0 = NULL;
	ColorU5BU5D_t2441545636* G_B3_0 = NULL;
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_1 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((ColorU5BU5D_t2441545636*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((ColorU5BU5D_t2441545636*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (ColorU5BU5D_t2441545636*)G_B3_0;
		ColorU5BU5D_t2441545636* L_3 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_5 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		ColorU5BU5D_t2441545636* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		ColorU5BU5D_t2441545636* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Trim()
extern "C"  void BetterList_1_Trim_m3405031946_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	ColorU5BU5D_t2441545636* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ColorU5BU5D_t2441545636* L_2 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (ColorU5BU5D_t2441545636*)((ColorU5BU5D_t2441545636*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		ColorU5BU5D_t2441545636* L_4 = V_0;
		int32_t L_5 = V_1;
		ColorU5BU5D_t2441545636* L_6 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Color_t4194546905  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Color_t4194546905 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((ColorU5BU5D_t2441545636*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Clear()
extern "C"  void BetterList_1_Clear_m98359175_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Release()
extern "C"  void BetterList_1_Release_m1395771521_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((ColorU5BU5D_t2441545636*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Add(T)
extern "C"  void BetterList_1_Add_m2847934883_gshared (BetterList_1_t1396547621 * __this, Color_t4194546905  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ColorU5BU5D_t2441545636* L_2 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1396547621 *)__this);
		((  void (*) (BetterList_1_t1396547621 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1396547621 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		ColorU5BU5D_t2441545636* L_3 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Color_t4194546905  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Color_t4194546905 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m3165888010_gshared (BetterList_1_t1396547621 * __this, int32_t ___index0, Color_t4194546905  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ColorU5BU5D_t2441545636* L_2 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1396547621 *)__this);
		((  void (*) (BetterList_1_t1396547621 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1396547621 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		ColorU5BU5D_t2441545636* L_7 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		ColorU5BU5D_t2441545636* L_9 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		Color_t4194546905  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Color_t4194546905 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_16 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		Color_t4194546905  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Color_t4194546905 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Color_t4194546905  L_20 = ___item1;
		NullCheck((BetterList_1_t1396547621 *)__this);
		((  void (*) (BetterList_1_t1396547621 *, Color_t4194546905 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t1396547621 *)__this, (Color_t4194546905 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Color>::Contains(T)
extern "C"  bool BetterList_1_Contains_m28461483_gshared (BetterList_1_t1396547621 * __this, Color_t4194546905  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ColorU5BU5D_t2441545636* L_1 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Color_t4194546905  L_3 = ___item0;
		Color_t4194546905  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Color_Equals_m3016668205((Color_t4194546905 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Color>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m2216612013_gshared (BetterList_1_t1396547621 * __this, Color_t4194546905  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ColorU5BU5D_t2441545636* L_1 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Color_t4194546905  L_3 = ___item0;
		Color_t4194546905  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Color_Equals_m3016668205((Color_t4194546905 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Color>::Remove(T)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m4289951846_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m4289951846_gshared (BetterList_1_t1396547621 * __this, Color_t4194546905  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m4289951846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t3302383786 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t3302383786 * L_1 = ((  EqualityComparer_1_t3302383786 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t3302383786 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t3302383786 * L_2 = V_0;
		ColorU5BU5D_t2441545636* L_3 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Color_t4194546905  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Color_t4194546905  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t3302383786 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, Color_t4194546905 , Color_t4194546905  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::Equals(!0,!0) */, (EqualityComparer_1_t3302383786 *)L_2, (Color_t4194546905 )L_6, (Color_t4194546905 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		ColorU5BU5D_t2441545636* L_10 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Color_t4194546905_il2cpp_TypeInfo_var, (&V_3));
		Color_t4194546905  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Color_t4194546905 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		ColorU5BU5D_t2441545636* L_14 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		ColorU5BU5D_t2441545636* L_16 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		Color_t4194546905  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Color_t4194546905 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_23 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Color_t4194546905_il2cpp_TypeInfo_var, (&V_4));
		Color_t4194546905  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Color_t4194546905 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::RemoveAt(System.Int32)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m1039740880_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m1039740880_gshared (BetterList_1_t1396547621 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m1039740880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		ColorU5BU5D_t2441545636* L_5 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Color_t4194546905_il2cpp_TypeInfo_var, (&V_1));
		Color_t4194546905  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Color_t4194546905 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		ColorU5BU5D_t2441545636* L_9 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		ColorU5BU5D_t2441545636* L_11 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		Color_t4194546905  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Color_t4194546905 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_18 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Color_t4194546905_il2cpp_TypeInfo_var, (&V_2));
		Color_t4194546905  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Color_t4194546905 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Color>::Pop()
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m3105358090_MetadataUsageId;
extern "C"  Color_t4194546905  BetterList_1_Pop_m3105358090_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m3105358090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_2 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		Color_t4194546905  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (Color_t4194546905 )L_7;
		ColorU5BU5D_t2441545636* L_8 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Color_t4194546905_il2cpp_TypeInfo_var, (&V_2));
		Color_t4194546905  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Color_t4194546905 )L_10);
		Color_t4194546905  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Color_t4194546905_il2cpp_TypeInfo_var, (&V_3));
		Color_t4194546905  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<UnityEngine.Color>::ToArray()
extern "C"  ColorU5BU5D_t2441545636* BetterList_1_ToArray_m1509532085_gshared (BetterList_1_t1396547621 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t1396547621 *)__this);
		((  void (*) (BetterList_1_t1396547621 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t1396547621 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		ColorU5BU5D_t2441545636* L_0 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m2221745279_gshared (BetterList_1_t1396547621 * __this, CompareFunc_t441298479 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t441298479 * L_2 = ___comparer0;
		ColorU5BU5D_t2441545636* L_3 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Color_t4194546905  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		ColorU5BU5D_t2441545636* L_7 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Color_t4194546905  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t441298479 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t441298479 *, Color_t4194546905 , Color_t4194546905 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t441298479 *)L_2, (Color_t4194546905 )L_6, (Color_t4194546905 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		ColorU5BU5D_t2441545636* L_12 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Color_t4194546905  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (Color_t4194546905 )L_15;
		ColorU5BU5D_t2441545636* L_16 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		ColorU5BU5D_t2441545636* L_18 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		Color_t4194546905  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Color_t4194546905 )L_21);
		ColorU5BU5D_t2441545636* L_22 = (ColorU5BU5D_t2441545636*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		Color_t4194546905  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (Color_t4194546905 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::.ctor()
extern "C"  void BetterList_1__ctor_m4247846203_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Color32>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m2633926575_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t2550291528 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t2550291528 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C"  Color32_t598853688  BetterList_1_get_Item_m3645095735_gshared (BetterList_1_t2095821700 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Color32_t598853688  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m3879320674_gshared (BetterList_1_t2095821700 * __this, int32_t ___i0, Color32_t598853688  ___value1, const MethodInfo* method)
{
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Color32_t598853688  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Color32_t598853688 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m3120891995_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m3120891995_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m3120891995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color32U5BU5D_t2960766953* V_0 = NULL;
	Color32U5BU5D_t2960766953* G_B3_0 = NULL;
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_1 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Color32U5BU5D_t2960766953*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Color32U5BU5D_t2960766953*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Color32U5BU5D_t2960766953*)G_B3_0;
		Color32U5BU5D_t2960766953* L_3 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_5 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		Color32U5BU5D_t2960766953* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Color32U5BU5D_t2960766953* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::Trim()
extern "C"  void BetterList_1_Trim_m3870855243_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	Color32U5BU5D_t2960766953* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Color32U5BU5D_t2960766953* L_2 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Color32U5BU5D_t2960766953*)((Color32U5BU5D_t2960766953*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Color32U5BU5D_t2960766953* L_4 = V_0;
		int32_t L_5 = V_1;
		Color32U5BU5D_t2960766953* L_6 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Color32_t598853688  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Color32_t598853688 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Color32U5BU5D_t2960766953*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::Clear()
extern "C"  void BetterList_1_Clear_m1653979494_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::Release()
extern "C"  void BetterList_1_Release_m1698279072_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Color32U5BU5D_t2960766953*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::Add(T)
extern "C"  void BetterList_1_Add_m3313758180_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Color32U5BU5D_t2960766953* L_2 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2095821700 *)__this);
		((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t2095821700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		Color32U5BU5D_t2960766953* L_3 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Color32_t598853688  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Color32_t598853688 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m185393739_gshared (BetterList_1_t2095821700 * __this, int32_t ___index0, Color32_t598853688  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Color32U5BU5D_t2960766953* L_2 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2095821700 *)__this);
		((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t2095821700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Color32U5BU5D_t2960766953* L_7 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Color32U5BU5D_t2960766953* L_9 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		Color32_t598853688  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Color32_t598853688 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_16 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		Color32_t598853688  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Color32_t598853688 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Color32_t598853688  L_20 = ___item1;
		NullCheck((BetterList_1_t2095821700 *)__this);
		((  void (*) (BetterList_1_t2095821700 *, Color32_t598853688 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t2095821700 *)__this, (Color32_t598853688 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Color32>::Contains(T)
extern "C"  bool BetterList_1_Contains_m3836725002_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Color32U5BU5D_t2960766953* L_1 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Color32_t598853688  L_3 = ___item0;
		Color32_t598853688  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Color32>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m373303790_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Color32U5BU5D_t2960766953* L_1 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Color32_t598853688  L_3 = ___item0;
		Color32_t598853688  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Color32>::Remove(T)
extern Il2CppClass* Color32_t598853688_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m1616822661_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m1616822661_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m1616822661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t4001657865 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Color32_t598853688  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color32_t598853688  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t4001657865 * L_1 = ((  EqualityComparer_1_t4001657865 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t4001657865 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t4001657865 * L_2 = V_0;
		Color32U5BU5D_t2960766953* L_3 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Color32_t598853688  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Color32_t598853688  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t4001657865 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, Color32_t598853688 , Color32_t598853688  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::Equals(!0,!0) */, (EqualityComparer_1_t4001657865 *)L_2, (Color32_t598853688 )L_6, (Color32_t598853688 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		Color32U5BU5D_t2960766953* L_10 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Color32_t598853688_il2cpp_TypeInfo_var, (&V_3));
		Color32_t598853688  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Color32_t598853688 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		Color32U5BU5D_t2960766953* L_14 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		Color32U5BU5D_t2960766953* L_16 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		Color32_t598853688  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Color32_t598853688 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_23 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Color32_t598853688_il2cpp_TypeInfo_var, (&V_4));
		Color32_t598853688  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Color32_t598853688 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern Il2CppClass* Color32_t598853688_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m2354213905_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m2354213905_gshared (BetterList_1_t2095821700 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m2354213905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Color32_t598853688  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color32_t598853688  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Color32U5BU5D_t2960766953* L_5 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Color32_t598853688_il2cpp_TypeInfo_var, (&V_1));
		Color32_t598853688  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Color32_t598853688 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Color32U5BU5D_t2960766953* L_9 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Color32U5BU5D_t2960766953* L_11 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		Color32_t598853688  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Color32_t598853688 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_18 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Color32_t598853688_il2cpp_TypeInfo_var, (&V_2));
		Color32_t598853688  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Color32_t598853688 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Color32>::Pop()
extern Il2CppClass* Color32_t598853688_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1769585385_MetadataUsageId;
extern "C"  Color32_t598853688  BetterList_1_Pop_m1769585385_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1769585385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color32_t598853688  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Color32_t598853688  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color32_t598853688  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_2 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		Color32_t598853688  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (Color32_t598853688 )L_7;
		Color32U5BU5D_t2960766953* L_8 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Color32_t598853688_il2cpp_TypeInfo_var, (&V_2));
		Color32_t598853688  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Color32_t598853688 )L_10);
		Color32_t598853688  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Color32_t598853688_il2cpp_TypeInfo_var, (&V_3));
		Color32_t598853688  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<UnityEngine.Color32>::ToArray()
extern "C"  Color32U5BU5D_t2960766953* BetterList_1_ToArray_m3421136020_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t2095821700 *)__this);
		((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t2095821700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Color32U5BU5D_t2960766953* L_0 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Color32>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m410803614_gshared (BetterList_1_t2095821700 * __this, CompareFunc_t1140572558 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Color32_t598853688  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t1140572558 * L_2 = ___comparer0;
		Color32U5BU5D_t2960766953* L_3 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Color32_t598853688  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Color32U5BU5D_t2960766953* L_7 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Color32_t598853688  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t1140572558 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t1140572558 *, Color32_t598853688 , Color32_t598853688 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t1140572558 *)L_2, (Color32_t598853688 )L_6, (Color32_t598853688 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Color32U5BU5D_t2960766953* L_12 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Color32_t598853688  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (Color32_t598853688 )L_15;
		Color32U5BU5D_t2960766953* L_16 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		Color32U5BU5D_t2960766953* L_18 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		Color32_t598853688  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Color32_t598853688 )L_21);
		Color32U5BU5D_t2960766953* L_22 = (Color32U5BU5D_t2960766953*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		Color32_t598853688  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (Color32_t598853688 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::.ctor()
extern "C"  void BetterList_1__ctor_m2212929352_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Vector2>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m1953221820_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1938537109 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t1938537109 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C"  Vector2_t4282066565  BetterList_1_get_Item_m884875658_gshared (BetterList_1_t1484067281 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Vector2_t4282066565  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m1302648309_gshared (BetterList_1_t1484067281 * __this, int32_t ___i0, Vector2_t4282066565  ___value1, const MethodInfo* method)
{
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Vector2_t4282066565  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Vector2_t4282066565 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m4068765102_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m4068765102_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m4068765102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2U5BU5D_t4024180168* V_0 = NULL;
	Vector2U5BU5D_t4024180168* G_B3_0 = NULL;
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_1 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Vector2U5BU5D_t4024180168*)G_B3_0;
		Vector2U5BU5D_t4024180168* L_3 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_5 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		Vector2U5BU5D_t4024180168* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Vector2U5BU5D_t4024180168* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Trim()
extern "C"  void BetterList_1_Trim_m1727002782_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	Vector2U5BU5D_t4024180168* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector2U5BU5D_t4024180168* L_2 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Vector2U5BU5D_t4024180168*)((Vector2U5BU5D_t4024180168*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Vector2U5BU5D_t4024180168* L_4 = V_0;
		int32_t L_5 = V_1;
		Vector2U5BU5D_t4024180168* L_6 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Vector2_t4282066565  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Vector2_t4282066565 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Vector2U5BU5D_t4024180168*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Clear()
extern "C"  void BetterList_1_Clear_m3914029939_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Release()
extern "C"  void BetterList_1_Release_m353304941_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Vector2U5BU5D_t4024180168*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Add(T)
extern "C"  void BetterList_1_Add_m1169905719_gshared (BetterList_1_t1484067281 * __this, Vector2_t4282066565  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector2U5BU5D_t4024180168* L_2 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1484067281 *)__this);
		((  void (*) (BetterList_1_t1484067281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1484067281 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		Vector2U5BU5D_t4024180168* L_3 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Vector2_t4282066565  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector2_t4282066565 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1720140958_gshared (BetterList_1_t1484067281 * __this, int32_t ___index0, Vector2_t4282066565  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector2U5BU5D_t4024180168* L_2 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1484067281 *)__this);
		((  void (*) (BetterList_1_t1484067281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1484067281 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Vector2U5BU5D_t4024180168* L_7 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Vector2U5BU5D_t4024180168* L_9 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		Vector2_t4282066565  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector2_t4282066565 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_16 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		Vector2_t4282066565  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector2_t4282066565 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Vector2_t4282066565  L_20 = ___item1;
		NullCheck((BetterList_1_t1484067281 *)__this);
		((  void (*) (BetterList_1_t1484067281 *, Vector2_t4282066565 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t1484067281 *)__this, (Vector2_t4282066565 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector2>::Contains(T)
extern "C"  bool BetterList_1_Contains_m4101741207_gshared (BetterList_1_t1484067281 * __this, Vector2_t4282066565  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector2U5BU5D_t4024180168* L_1 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector2_t4282066565  L_3 = ___item0;
		Vector2_t4282066565  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Vector2_Equals_m3404198849((Vector2_t4282066565 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Vector2>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1628778689_gshared (BetterList_1_t1484067281 * __this, Vector2_t4282066565  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector2U5BU5D_t4024180168* L_1 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector2_t4282066565  L_3 = ___item0;
		Vector2_t4282066565  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Vector2_Equals_m3404198849((Vector2_t4282066565 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector2>::Remove(T)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m271848530_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m271848530_gshared (BetterList_1_t1484067281 * __this, Vector2_t4282066565  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m271848530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t3389903446 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t3389903446 * L_1 = ((  EqualityComparer_1_t3389903446 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t3389903446 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t3389903446 * L_2 = V_0;
		Vector2U5BU5D_t4024180168* L_3 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector2_t4282066565  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Vector2_t4282066565  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t3389903446 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, Vector2_t4282066565 , Vector2_t4282066565  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::Equals(!0,!0) */, (EqualityComparer_1_t3389903446 *)L_2, (Vector2_t4282066565 )L_6, (Vector2_t4282066565 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		Vector2U5BU5D_t4024180168* L_10 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_3));
		Vector2_t4282066565  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Vector2_t4282066565 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		Vector2U5BU5D_t4024180168* L_14 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		Vector2U5BU5D_t4024180168* L_16 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		Vector2_t4282066565  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Vector2_t4282066565 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_23 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_4));
		Vector2_t4282066565  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Vector2_t4282066565 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3888961124_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3888961124_gshared (BetterList_1_t1484067281 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3888961124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Vector2U5BU5D_t4024180168* L_5 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t4282066565  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector2_t4282066565 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Vector2U5BU5D_t4024180168* L_9 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Vector2U5BU5D_t4024180168* L_11 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		Vector2_t4282066565  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector2_t4282066565 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_18 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_2));
		Vector2_t4282066565  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Vector2_t4282066565 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Vector2>::Pop()
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1700428854_MetadataUsageId;
extern "C"  Vector2_t4282066565  BetterList_1_Pop_m1700428854_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1700428854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_2 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		Vector2_t4282066565  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (Vector2_t4282066565 )L_7;
		Vector2U5BU5D_t4024180168* L_8 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_2));
		Vector2_t4282066565  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Vector2_t4282066565 )L_10);
		Vector2_t4282066565  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_3));
		Vector2_t4282066565  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<UnityEngine.Vector2>::ToArray()
extern "C"  Vector2U5BU5D_t4024180168* BetterList_1_ToArray_m2076161889_gshared (BetterList_1_t1484067281 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t1484067281 *)__this);
		((  void (*) (BetterList_1_t1484067281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t1484067281 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Vector2U5BU5D_t4024180168* L_0 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m749295979_gshared (BetterList_1_t1484067281 * __this, CompareFunc_t528818139 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t528818139 * L_2 = ___comparer0;
		Vector2U5BU5D_t4024180168* L_3 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector2_t4282066565  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Vector2U5BU5D_t4024180168* L_7 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Vector2_t4282066565  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t528818139 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t528818139 *, Vector2_t4282066565 , Vector2_t4282066565 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t528818139 *)L_2, (Vector2_t4282066565 )L_6, (Vector2_t4282066565 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Vector2U5BU5D_t4024180168* L_12 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Vector2_t4282066565  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (Vector2_t4282066565 )L_15;
		Vector2U5BU5D_t4024180168* L_16 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		Vector2U5BU5D_t4024180168* L_18 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		Vector2_t4282066565  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector2_t4282066565 )L_21);
		Vector2U5BU5D_t4024180168* L_22 = (Vector2U5BU5D_t4024180168*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		Vector2_t4282066565  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (Vector2_t4282066565 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::.ctor()
extern "C"  void BetterList_1__ctor_m415977993_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Vector3>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m1108749949_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1938537110 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t1938537110 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C"  Vector3_t4282066566  BetterList_1_get_Item_m915895465_gshared (BetterList_1_t1484067282 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Vector3_t4282066566  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m1047911764_gshared (BetterList_1_t1484067282 * __this, int32_t ___i0, Vector3_t4282066566  ___value1, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Vector3_t4282066566  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Vector3_t4282066566 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m3071692749_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m3071692749_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m3071692749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3U5BU5D_t215400611* V_0 = NULL;
	Vector3U5BU5D_t215400611* G_B3_0 = NULL;
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Vector3U5BU5D_t215400611*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Vector3U5BU5D_t215400611*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Vector3U5BU5D_t215400611*)G_B3_0;
		Vector3U5BU5D_t215400611* L_3 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_5 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		Vector3U5BU5D_t215400611* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Vector3U5BU5D_t215400611* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Trim()
extern "C"  void BetterList_1_Trim_m1530489277_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t215400611* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector3U5BU5D_t215400611* L_2 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Vector3U5BU5D_t215400611*)((Vector3U5BU5D_t215400611*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Vector3U5BU5D_t215400611* L_4 = V_0;
		int32_t L_5 = V_1;
		Vector3U5BU5D_t215400611* L_6 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Vector3_t4282066566  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Vector3_t4282066566 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Vector3U5BU5D_t215400611*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Clear()
extern "C"  void BetterList_1_Clear_m2117078580_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Release()
extern "C"  void BetterList_1_Release_m59901934_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Vector3U5BU5D_t215400611*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Add(T)
extern "C"  void BetterList_1_Add_m973392214_gshared (BetterList_1_t1484067282 * __this, Vector3_t4282066566  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector3U5BU5D_t215400611* L_2 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1484067282 *)__this);
		((  void (*) (BetterList_1_t1484067282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1484067282 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		Vector3U5BU5D_t215400611* L_3 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Vector3_t4282066566  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector3_t4282066566 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1751160765_gshared (BetterList_1_t1484067282 * __this, int32_t ___index0, Vector3_t4282066566  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector3U5BU5D_t215400611* L_2 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1484067282 *)__this);
		((  void (*) (BetterList_1_t1484067282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1484067282 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Vector3U5BU5D_t215400611* L_7 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Vector3U5BU5D_t215400611* L_9 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		Vector3_t4282066566  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector3_t4282066566 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_16 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		Vector3_t4282066566  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector3_t4282066566 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Vector3_t4282066566  L_20 = ___item1;
		NullCheck((BetterList_1_t1484067282 *)__this);
		((  void (*) (BetterList_1_t1484067282 *, Vector3_t4282066566 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t1484067282 *)__this, (Vector3_t4282066566 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector3>::Contains(T)
extern "C"  bool BetterList_1_Contains_m1314325720_gshared (BetterList_1_t1484067282 * __this, Vector3_t4282066566  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector3U5BU5D_t215400611* L_1 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector3_t4282066566  L_3 = ___item0;
		Vector3_t4282066566  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Vector3_Equals_m3337192096((Vector3_t4282066566 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Vector3>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1123220064_gshared (BetterList_1_t1484067282 * __this, Vector3_t4282066566  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector3U5BU5D_t215400611* L_1 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector3_t4282066566  L_3 = ___item0;
		Vector3_t4282066566  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Vector3_Equals_m3337192096((Vector3_t4282066566 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector3>::Remove(T)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m4273412819_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m4273412819_gshared (BetterList_1_t1484067282 * __this, Vector3_t4282066566  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m4273412819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t3389903447 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t3389903447 * L_1 = ((  EqualityComparer_1_t3389903447 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t3389903447 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t3389903447 * L_2 = V_0;
		Vector3U5BU5D_t215400611* L_3 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector3_t4282066566  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Vector3_t4282066566  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t3389903447 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, Vector3_t4282066566 , Vector3_t4282066566  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::Equals(!0,!0) */, (EqualityComparer_1_t3389903447 *)L_2, (Vector3_t4282066566 )L_6, (Vector3_t4282066566 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		Vector3U5BU5D_t215400611* L_10 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t4282066566  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Vector3_t4282066566 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		Vector3U5BU5D_t215400611* L_14 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		Vector3U5BU5D_t215400611* L_16 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		Vector3_t4282066566  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Vector3_t4282066566 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_23 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_4));
		Vector3_t4282066566  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Vector3_t4282066566 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3919980931_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3919980931_gshared (BetterList_1_t1484067282 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3919980931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Vector3U5BU5D_t215400611* L_5 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_1));
		Vector3_t4282066566  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector3_t4282066566 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Vector3U5BU5D_t215400611* L_9 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Vector3U5BU5D_t215400611* L_11 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		Vector3_t4282066566  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector3_t4282066566 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_18 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t4282066566  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Vector3_t4282066566 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Vector3>::Pop()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m4187941687_MetadataUsageId;
extern "C"  Vector3_t4282066566  BetterList_1_Pop_m4187941687_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m4187941687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_2 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		Vector3_t4282066566  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (Vector3_t4282066566 )L_7;
		Vector3U5BU5D_t215400611* L_8 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t4282066566  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Vector3_t4282066566 )L_10);
		Vector3_t4282066566  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t4282066566  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t215400611* BetterList_1_ToArray_m1782758882_gshared (BetterList_1_t1484067282 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t1484067282 *)__this);
		((  void (*) (BetterList_1_t1484067282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t1484067282 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Vector3U5BU5D_t215400611* L_0 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1079061740_gshared (BetterList_1_t1484067282 * __this, CompareFunc_t528818140 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t528818140 * L_2 = ___comparer0;
		Vector3U5BU5D_t215400611* L_3 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector3_t4282066566  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Vector3U5BU5D_t215400611* L_7 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Vector3_t4282066566  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t528818140 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t528818140 *, Vector3_t4282066566 , Vector3_t4282066566 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t528818140 *)L_2, (Vector3_t4282066566 )L_6, (Vector3_t4282066566 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_12 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Vector3_t4282066566  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (Vector3_t4282066566 )L_15;
		Vector3U5BU5D_t215400611* L_16 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		Vector3U5BU5D_t215400611* L_18 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		Vector3_t4282066566  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector3_t4282066566 )L_21);
		Vector3U5BU5D_t215400611* L_22 = (Vector3U5BU5D_t215400611*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		Vector3_t4282066566  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (Vector3_t4282066566 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::.ctor()
extern "C"  void BetterList_1__ctor_m2913993930_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Vector4>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m264278078_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * L_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__IteratorD_t1938537111 *)L_0;
		U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__IteratorD_t1938537111 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C"  Vector4_t4282066567  BetterList_1_get_Item_m946915272_gshared (BetterList_1_t1484067283 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Vector4_t4282066567  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m793175219_gshared (BetterList_1_t1484067283 * __this, int32_t ___i0, Vector4_t4282066567  ___value1, const MethodInfo* method)
{
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Vector4_t4282066567  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Vector4_t4282066567 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::AllocateMore()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m2074620396_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m2074620396_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m2074620396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4U5BU5D_t701588350* V_0 = NULL;
	Vector4U5BU5D_t701588350* G_B3_0 = NULL;
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_1 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Vector4U5BU5D_t701588350*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Vector4U5BU5D_t701588350*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Vector4U5BU5D_t701588350*)G_B3_0;
		Vector4U5BU5D_t701588350* L_3 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_5 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		Vector4U5BU5D_t701588350* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Vector4U5BU5D_t701588350* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Trim()
extern "C"  void BetterList_1_Trim_m1333975772_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	Vector4U5BU5D_t701588350* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector4U5BU5D_t701588350* L_2 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Vector4U5BU5D_t701588350*)((Vector4U5BU5D_t701588350*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Vector4U5BU5D_t701588350* L_4 = V_0;
		int32_t L_5 = V_1;
		Vector4U5BU5D_t701588350* L_6 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Vector4_t4282066567  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Vector4_t4282066567 )L_9);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_13 = V_0;
		__this->set_buffer_0(L_13);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Vector4U5BU5D_t701588350*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Clear()
extern "C"  void BetterList_1_Clear_m320127221_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Release()
extern "C"  void BetterList_1_Release_m4061466223_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Vector4U5BU5D_t701588350*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Add(T)
extern "C"  void BetterList_1_Add_m776878709_gshared (BetterList_1_t1484067283 * __this, Vector4_t4282066567  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector4U5BU5D_t701588350* L_2 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1484067283 *)__this);
		((  void (*) (BetterList_1_t1484067283 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1484067283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		Vector4U5BU5D_t701588350* L_3 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Vector4_t4282066567  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector4_t4282066567 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1782180572_gshared (BetterList_1_t1484067283 * __this, int32_t ___index0, Vector4_t4282066567  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector4U5BU5D_t701588350* L_2 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t1484067283 *)__this);
		((  void (*) (BetterList_1_t1484067283 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BetterList_1_t1484067283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Vector4U5BU5D_t701588350* L_7 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Vector4U5BU5D_t701588350* L_9 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		Vector4_t4282066567  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector4_t4282066567 )L_12);
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = ___index0;
		if ((((int32_t)L_14) > ((int32_t)L_15)))
		{
			goto IL_0043;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_16 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_17 = ___index0;
		Vector4_t4282066567  L_18 = ___item1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector4_t4282066567 )L_18);
		int32_t L_19 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_19+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Vector4_t4282066567  L_20 = ___item1;
		NullCheck((BetterList_1_t1484067283 *)__this);
		((  void (*) (BetterList_1_t1484067283 *, Vector4_t4282066567 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BetterList_1_t1484067283 *)__this, (Vector4_t4282066567 )L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector4>::Contains(T)
extern "C"  bool BetterList_1_Contains_m2821877529_gshared (BetterList_1_t1484067283 * __this, Vector4_t4282066567  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector4U5BU5D_t701588350* L_1 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector4_t4282066567  L_3 = ___item0;
		Vector4_t4282066567  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Vector4_Equals_m3270185343((Vector4_t4282066567 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Vector4>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m617661439_gshared (BetterList_1_t1484067283 * __this, Vector4_t4282066567  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector4U5BU5D_t701588350* L_1 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector4_t4282066567  L_3 = ___item0;
		Vector4_t4282066567  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		bool L_6 = Vector4_Equals_m3270185343((Vector4_t4282066567 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector4>::Remove(T)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m3980009812_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m3980009812_gshared (BetterList_1_t1484067283 * __this, Vector4_t4282066567  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m3980009812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t3389903448 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector4_t4282066567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t4282066567  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		EqualityComparer_1_t3389903448 * L_1 = ((  EqualityComparer_1_t3389903448 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t3389903448 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t3389903448 * L_2 = V_0;
		Vector4U5BU5D_t701588350* L_3 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector4_t4282066567  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Vector4_t4282066567  L_7 = ___item0;
		NullCheck((EqualityComparer_1_t3389903448 *)L_2);
		bool L_8 = VirtFuncInvoker2< bool, Vector4_t4282066567 , Vector4_t4282066567  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::Equals(!0,!0) */, (EqualityComparer_1_t3389903448 *)L_2, (Vector4_t4282066567 )L_6, (Vector4_t4282066567 )L_7);
		if (!L_8)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_9-(int32_t)1)));
		Vector4U5BU5D_t701588350* L_10 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_11 = V_1;
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_3));
		Vector4_t4282066567  L_12 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Vector4_t4282066567 )L_12);
		int32_t L_13 = V_1;
		V_2 = (int32_t)L_13;
		goto IL_0078;
	}

IL_005a:
	{
		Vector4U5BU5D_t701588350* L_14 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_15 = V_2;
		Vector4U5BU5D_t701588350* L_16 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		Vector4_t4282066567  L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Vector4_t4282066567 )L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		int32_t L_22 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_23 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_24 = (int32_t)__this->get_size_1();
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_4));
		Vector4_t4282066567  L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Vector4_t4282066567 )L_25);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3951000738_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3951000738_gshared (BetterList_1_t1484067283 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3951000738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Vector4U5BU5D_t701588350* L_5 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_1));
		Vector4_t4282066567  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector4_t4282066567 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Vector4U5BU5D_t701588350* L_9 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Vector4U5BU5D_t701588350* L_11 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		Vector4_t4282066567  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector4_t4282066567 )L_14);
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0048;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_18 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_19 = (int32_t)__this->get_size_1();
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_2));
		Vector4_t4282066567  L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Vector4_t4282066567 )L_20);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Vector4>::Pop()
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m2380487224_MetadataUsageId;
extern "C"  Vector4_t4282066567  BetterList_1_Pop_m2380487224_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m2380487224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4282066567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_2 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		Vector4_t4282066567  L_7 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = (Vector4_t4282066567 )L_7;
		Vector4U5BU5D_t701588350* L_8 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_9 = (int32_t)__this->get_size_1();
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_2));
		Vector4_t4282066567  L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Vector4_t4282066567 )L_10);
		Vector4_t4282066567  L_11 = V_0;
		return L_11;
	}

IL_004f:
	{
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_3));
		Vector4_t4282066567  L_12 = V_3;
		return L_12;
	}
}
// T[] BetterList`1<UnityEngine.Vector4>::ToArray()
extern "C"  Vector4U5BU5D_t701588350* BetterList_1_ToArray_m1489355875_gshared (BetterList_1_t1484067283 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t1484067283 *)__this);
		((  void (*) (BetterList_1_t1484067283 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BetterList_1_t1484067283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Vector4U5BU5D_t701588350* L_0 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1408827501_gshared (BetterList_1_t1484067283 * __this, CompareFunc_t528818141 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Vector4_t4282066567  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t528818141 * L_2 = ___comparer0;
		Vector4U5BU5D_t701588350* L_3 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector4_t4282066567  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Vector4U5BU5D_t701588350* L_7 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Vector4_t4282066567  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((CompareFunc_t528818141 *)L_2);
		int32_t L_11 = ((  int32_t (*) (CompareFunc_t528818141 *, Vector4_t4282066567 , Vector4_t4282066567 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CompareFunc_t528818141 *)L_2, (Vector4_t4282066567 )L_6, (Vector4_t4282066567 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Vector4U5BU5D_t701588350* L_12 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Vector4_t4282066567  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = (Vector4_t4282066567 )L_15;
		Vector4U5BU5D_t701588350* L_16 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_17 = V_3;
		Vector4U5BU5D_t701588350* L_18 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)1));
		Vector4_t4282066567  L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector4_t4282066567 )L_21);
		Vector4U5BU5D_t701588350* L_22 = (Vector4U5BU5D_t701588350*)__this->get_buffer_0();
		int32_t L_23 = V_3;
		Vector4_t4282066567  L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)1))), (Vector4_t4282066567 )L_24);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_25 = V_2;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_26 = V_3;
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_27 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_28 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m986248306_gshared (Action_1_t872614854 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3594021162_gshared (Action_1_t872614854 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3594021162((Action_1_t872614854 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m647183148_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m647183148_gshared (Action_1_t872614854 * __this, bool ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m647183148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1601629789_gshared (Action_1_t872614854 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m881151526_gshared (Action_1_t271665211 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m663971678_gshared (Action_1_t271665211 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m663971678((Action_1_t271665211 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m917692971_gshared (Action_1_t271665211 * __this, Il2CppObject * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3562128182_gshared (Action_1_t271665211 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m993889981_gshared (Action_1_t392767812 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Single>::Invoke(T)
extern "C"  void Action_1_Invoke_m2677867239_gshared (Action_1_t392767812 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2677867239((Action_1_t392767812 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1836875956_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1836875956_gshared (Action_1_t392767812 * __this, float ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1836875956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3782731597_gshared (Action_1_t392767812 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1178756541_gshared (Action_1_t3655021551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode>::Invoke(T)
extern "C"  void Action_1_Invoke_m2083277799_gshared (Action_1_t3655021551 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2083277799((Action_1_t3655021551 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* EErrorCode_t3259205415_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3101427380_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3101427380_gshared (Action_1_t3655021551 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3101427380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EErrorCode_t3259205415_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Utils.Core.AndroidOBBHandler/EErrorCode>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3867164237_gshared (Action_1_t3655021551 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1043807536_gshared (Action_1_t983143684 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m669325728_gshared (Action_1_t983143684 * __this, SmartTerrainInitializationInfo_t587327548  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m669325728((Action_1_t983143684 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SmartTerrainInitializationInfo_t587327548  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, SmartTerrainInitializationInfo_t587327548  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Vuforia.SmartTerrainInitializationInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* SmartTerrainInitializationInfo_t587327548_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2872323433_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2872323433_gshared (Action_1_t983143684 * __this, SmartTerrainInitializationInfo_t587327548  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2872323433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SmartTerrainInitializationInfo_t587327548_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m546883392_gshared (Action_1_t983143684 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m626199895_gshared (Action_1_t828034096 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::Invoke(T)
extern "C"  void Action_1_Invoke_m1545576094_gshared (Action_1_t828034096 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1545576094((Action_1_t828034096 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Vuforia.VuforiaUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitError_t432217960_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3881238443_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3881238443_gshared (Action_1_t828034096 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3881238443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitError_t432217960_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2374626366_gshared (Action_1_t828034096 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m4262077373_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2204526468_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4054268681_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4201941769_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1756209413 * L_2 = (ArrayReadOnlyList_1_t1756209413 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1756209413 * L_9 = (ArrayReadOnlyList_1_t1756209413 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t1108656482* L_10 = (ObjectU5BU5D_t1108656482*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2650805434_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2799549978_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t3059612989  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2027929923_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1186685398_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t3059612989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4084754896_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t645006031 * L_2 = (ArrayReadOnlyList_1_t645006031 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t645006031 * L_9 = (ArrayReadOnlyList_1_t645006031 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_10 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1616284631_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m643544331_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t3301293422  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4108562996_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2728697221_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t3301293422  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1461469503_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t886686464 * L_2 = (ArrayReadOnlyList_1_t886686464 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t886686464 * L_9 = (ArrayReadOnlyList_1_t886686464 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_10 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m4164061832_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m240689135_gshared (ArrayReadOnlyList_1_t1756209413 * __this, ObjectU5BU5D_t1108656482* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2499499206_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1756209413 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1756209413 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1756209413 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m535939909_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m2625374704_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m3813449101_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m1486920810_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m976758002_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2221418008_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m158553866_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1244492898_gshared (ArrayReadOnlyList_1_t1756209413 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1770413409_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1098739118_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m738278233_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m12996997_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2907098399_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m740547916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3527945938_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m932360725_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t645006031 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t645006031 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t645006031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  ArrayReadOnlyList_1_get_Item_m3165498630_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3031060371_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2401585746_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2438902353_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3652230485_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m3556686357_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2530929859_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1650178565_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2319686054_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m3408499785_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2906295740_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m609878398_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m780148610_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m467076531_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m904660545_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1042005508_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t886686464 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t886686464 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t886686464 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  ArrayReadOnlyList_1_get_Item_m957797877_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3144480962_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2684117187_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m3126393472_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3859776580_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1400680710_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2813461300_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1763599156_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2480410519_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m785214392_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m698594987_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m3157655599_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2867415153_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m627800996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2510835690_gshared (InternalEnumerator_1_t2155190829 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2510835690_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1__ctor_m2510835690(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		TableRange_t3372848153  L_0 = InternalEnumerator_1_get_Current_m2306301105((InternalEnumerator_1_t2155190829 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t3372848153  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2516768321(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1609192930(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId;
extern "C"  TableRange_t3372848153  InternalEnumerator_1_get_Current_m2306301105_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t3372848153  L_8 = ((  TableRange_t3372848153  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t3372848153  InternalEnumerator_1_get_Current_m2306301105_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2306301105(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4155127258_gshared (InternalEnumerator_1_t1949385224 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4155127258_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1__ctor_m4155127258(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3784081185((InternalEnumerator_1_t1949385224 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4263405617(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m569750258(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3784081185(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3894762810_gshared (InternalEnumerator_1_t798349321 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3894762810_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1__ctor_m3894762810(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		TagName_t2016006645  L_0 = InternalEnumerator_1_get_Current_m1405735267((InternalEnumerator_1_t798349321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TagName_t2016006645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1537296273(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3872827350(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId;
extern "C"  TagName_t2016006645  InternalEnumerator_1_get_Current_m1405735267_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TagName_t2016006645  L_8 = ((  TagName_t2016006645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TagName_t2016006645  InternalEnumerator_1_get_Current_m1405735267_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1405735267(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1191579514_gshared (InternalEnumerator_1_t970376284 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1191579514_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	InternalEnumerator_1__ctor_m1191579514(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2188033608  L_0 = InternalEnumerator_1_get_Current_m3176144321((InternalEnumerator_1_t970376284 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArraySegment_1_t2188033608  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2416953809_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2416953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2416953809(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m552394578_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m552394578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m552394578(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3176144321_MetadataUsageId;
extern "C"  ArraySegment_1_t2188033608  InternalEnumerator_1_get_Current_m3176144321_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3176144321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2188033608  L_8 = ((  ArraySegment_1_t2188033608  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArraySegment_1_t2188033608  InternalEnumerator_1_get_Current_m3176144321_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3176144321(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3904876858_gshared (InternalEnumerator_1_t3554108690 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3904876858_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	InternalEnumerator_1__ctor_m3904876858(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m94889217((InternalEnumerator_1_t3554108690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2505350033_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2505350033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2505350033(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2858864786_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2858864786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2858864786(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m94889217_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m94889217_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m94889217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m94889217_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m94889217(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2342462508_gshared (InternalEnumerator_1_t1644952336 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2342462508_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	InternalEnumerator_1__ctor_m2342462508(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4103229525((InternalEnumerator_1_t1644952336 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1621603587_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1621603587_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1621603587(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3669835172_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3669835172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3669835172(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4103229525_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4103229525_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4103229525(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3601500154_gshared (InternalEnumerator_1_t1644965214 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3601500154_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	InternalEnumerator_1__ctor_m3601500154(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m3048220323((InternalEnumerator_1_t1644965214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2748899921_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2748899921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2748899921(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4256283158_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4256283158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4256283158(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m3048220323_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m3048220323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3048220323(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m263261269_gshared (InternalEnumerator_1_t533949290 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m263261269_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	InternalEnumerator_1__ctor_m263261269(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t1751606614  L_0 = InternalEnumerator_1_get_Current_m2915343068((InternalEnumerator_1_t533949290 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1282215020_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1282215020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1282215020(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4030197783_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4030197783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4030197783(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  InternalEnumerator_1_get_Current_m2915343068_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t1751606614  L_8 = ((  DictionaryEntry_t1751606614  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t1751606614  InternalEnumerator_1_get_Current_m2915343068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2915343068(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2684410586_gshared (InternalEnumerator_1_t904941831 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2684410586_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	InternalEnumerator_1__ctor_m2684410586(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	{
		Link_t2122599155  L_0 = InternalEnumerator_1_get_Current_m13756385((InternalEnumerator_1_t904941831 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2122599155  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3733933361_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3733933361_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3733933361(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3124962674_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3124962674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3124962674(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m13756385_MetadataUsageId;
extern "C"  Link_t2122599155  InternalEnumerator_1_get_Current_m13756385_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m13756385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2122599155  L_8 = ((  Link_t2122599155  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2122599155  InternalEnumerator_1_get_Current_m13756385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m13756385(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2135273650_gshared (InternalEnumerator_1_t4127192417 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2135273650_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	InternalEnumerator_1__ctor_m2135273650(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1049882445  L_0 = InternalEnumerator_1_get_Current_m2170878265((InternalEnumerator_1_t4127192417 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1049882445  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1660227849_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1660227849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1660227849(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m47820698_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m47820698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m47820698(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2170878265_MetadataUsageId;
extern "C"  KeyValuePair_2_t1049882445  InternalEnumerator_1_get_Current_m2170878265_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2170878265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1049882445  L_8 = ((  KeyValuePair_2_t1049882445  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1049882445  InternalEnumerator_1_get_Current_m2170878265_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2170878265(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m664150035_gshared (InternalEnumerator_1_t2849202992 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m664150035_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	InternalEnumerator_1__ctor_m664150035(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4066860316  L_0 = InternalEnumerator_1_get_Current_m818645564((InternalEnumerator_1_t2849202992 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4066860316  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m233182634_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m233182634_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	InternalEnumerator_1_Dispose_m233182634(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m44493661_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m44493661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m44493661(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m818645564_MetadataUsageId;
extern "C"  KeyValuePair_2_t4066860316  InternalEnumerator_1_get_Current_m818645564_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m818645564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4066860316  L_8 = ((  KeyValuePair_2_t4066860316  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4066860316  InternalEnumerator_1_get_Current_m818645564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m818645564(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1007548845_gshared (InternalEnumerator_1_t3369230641 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1007548845_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3369230641 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3369230641 *>(__this + 1);
	InternalEnumerator_1__ctor_m1007548845(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1157724883_gshared (InternalEnumerator_1_t3369230641 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1157724883_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3369230641 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3369230641 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1157724883(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690877567_gshared (InternalEnumerator_1_t3369230641 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t291920669  L_0 = InternalEnumerator_1_get_Current_m2369010356((InternalEnumerator_1_t3369230641 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t291920669  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690877567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3369230641 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3369230641 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690877567(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1899644868_gshared (InternalEnumerator_1_t3369230641 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1899644868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3369230641 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3369230641 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1899644868(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3682099391_gshared (InternalEnumerator_1_t3369230641 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3682099391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3369230641 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3369230641 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3682099391(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2369010356_MetadataUsageId;
extern "C"  KeyValuePair_2_t291920669  InternalEnumerator_1_get_Current_m2369010356_gshared (InternalEnumerator_1_t3369230641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2369010356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t291920669  L_8 = ((  KeyValuePair_2_t291920669  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t291920669  InternalEnumerator_1_get_Current_m2369010356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3369230641 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3369230641 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2369010356(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3279540306_gshared (InternalEnumerator_1_t3432734252 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3279540306_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3432734252 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3432734252 *>(__this + 1);
	InternalEnumerator_1__ctor_m3279540306(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2352549902_gshared (InternalEnumerator_1_t3432734252 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2352549902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3432734252 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3432734252 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2352549902(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3425914426_gshared (InternalEnumerator_1_t3432734252 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t355424280  L_0 = InternalEnumerator_1_get_Current_m696489177((InternalEnumerator_1_t3432734252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t355424280  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3425914426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3432734252 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3432734252 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3425914426(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m138295465_gshared (InternalEnumerator_1_t3432734252 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m138295465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3432734252 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3432734252 *>(__this + 1);
	InternalEnumerator_1_Dispose_m138295465(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m494863354_gshared (InternalEnumerator_1_t3432734252 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m494863354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3432734252 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3432734252 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m494863354(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m696489177_MetadataUsageId;
extern "C"  KeyValuePair_2_t355424280  InternalEnumerator_1_get_Current_m696489177_gshared (InternalEnumerator_1_t3432734252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m696489177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t355424280  L_8 = ((  KeyValuePair_2_t355424280  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t355424280  InternalEnumerator_1_get_Current_m696489177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3432734252 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3432734252 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m696489177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2662086813_gshared (InternalEnumerator_1_t1327961296 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2662086813_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	InternalEnumerator_1__ctor_m2662086813(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2545618620  L_0 = InternalEnumerator_1_get_Current_m1242840582((InternalEnumerator_1_t1327961296 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2545618620  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1742566068_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1742566068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1742566068(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3398874899_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3398874899_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3398874899(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId;
extern "C"  KeyValuePair_2_t2545618620  InternalEnumerator_1_get_Current_m1242840582_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2545618620  L_8 = ((  KeyValuePair_2_t2545618620  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2545618620  InternalEnumerator_1_get_Current_m1242840582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1242840582(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2624907895_gshared (InternalEnumerator_1_t2005001078 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2624907895_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	InternalEnumerator_1__ctor_m2624907895(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3222658402  L_0 = InternalEnumerator_1_get_Current_m1033564064((InternalEnumerator_1_t2005001078 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3222658402  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1793576206_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1793576206_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1793576206(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1172054137_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1172054137_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1172054137(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId;
extern "C"  KeyValuePair_2_t3222658402  InternalEnumerator_1_get_Current_m1033564064_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3222658402  L_8 = ((  KeyValuePair_2_t3222658402  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3222658402  InternalEnumerator_1_get_Current_m1033564064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1033564064(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2957909742_gshared (InternalEnumerator_1_t727011653 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2957909742_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	InternalEnumerator_1__ctor_m2957909742(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1944668977  L_0 = InternalEnumerator_1_get_Current_m4216610997((InternalEnumerator_1_t727011653 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1944668977  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m72014405_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m72014405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	InternalEnumerator_1_Dispose_m72014405(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m535991902_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m535991902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m535991902(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId;
extern "C"  KeyValuePair_2_t1944668977  InternalEnumerator_1_get_Current_m4216610997_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1944668977  L_8 = ((  KeyValuePair_2_t1944668977  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1944668977  InternalEnumerator_1_get_Current_m4216610997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4216610997(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3050775493_gshared (InternalEnumerator_1_t848114254 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3050775493_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	InternalEnumerator_1__ctor_m3050775493(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2065771578  L_0 = InternalEnumerator_1_get_Current_m2312868556((InternalEnumerator_1_t848114254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2065771578  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2373234652_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2373234652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2373234652(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3154342823_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3154342823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3154342823(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2312868556_MetadataUsageId;
extern "C"  KeyValuePair_2_t2065771578  InternalEnumerator_1_get_Current_m2312868556_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2312868556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2065771578  L_8 = ((  KeyValuePair_2_t2065771578  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2065771578  InternalEnumerator_1_get_Current_m2312868556_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2312868556(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1257520974_gshared (InternalEnumerator_1_t875830501 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1257520974_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t875830501 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830501 *>(__this + 1);
	InternalEnumerator_1__ctor_m1257520974(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1542121362_gshared (InternalEnumerator_1_t875830501 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1542121362_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830501 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830501 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1542121362(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3457638398_gshared (InternalEnumerator_1_t875830501 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2093487825  L_0 = InternalEnumerator_1_get_Current_m2477934869((InternalEnumerator_1_t875830501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2093487825  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3457638398_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830501 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830501 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3457638398(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3719033509_gshared (InternalEnumerator_1_t875830501 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3719033509_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830501 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830501 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3719033509(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1924434430_gshared (InternalEnumerator_1_t875830501 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1924434430_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830501 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830501 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1924434430(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2477934869_MetadataUsageId;
extern "C"  KeyValuePair_2_t2093487825  InternalEnumerator_1_get_Current_m2477934869_gshared (InternalEnumerator_1_t875830501 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2477934869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2093487825  L_8 = ((  KeyValuePair_2_t2093487825  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2093487825  InternalEnumerator_1_get_Current_m2477934869_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830501 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830501 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2477934869(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m869685158_gshared (InternalEnumerator_1_t2812853368 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m869685158_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2812853368 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2812853368 *>(__this + 1);
	InternalEnumerator_1__ctor_m869685158(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2139324474_gshared (InternalEnumerator_1_t2812853368 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2139324474_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2812853368 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2812853368 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2139324474(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m60667686_gshared (InternalEnumerator_1_t2812853368 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4030510692  L_0 = InternalEnumerator_1_get_Current_m753937901((InternalEnumerator_1_t2812853368 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4030510692  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m60667686_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2812853368 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2812853368 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m60667686(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3764918525_gshared (InternalEnumerator_1_t2812853368 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3764918525_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2812853368 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2812853368 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3764918525(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1014342566_gshared (InternalEnumerator_1_t2812853368 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1014342566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2812853368 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2812853368 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1014342566(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m753937901_MetadataUsageId;
extern "C"  KeyValuePair_2_t4030510692  InternalEnumerator_1_get_Current_m753937901_gshared (InternalEnumerator_1_t2812853368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m753937901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4030510692  L_8 = ((  KeyValuePair_2_t4030510692  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4030510692  InternalEnumerator_1_get_Current_m753937901_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2812853368 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2812853368 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m753937901(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1975839218_gshared (InternalEnumerator_1_t500425236 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1975839218_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t500425236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t500425236 *>(__this + 1);
	InternalEnumerator_1__ctor_m1975839218(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4213396078_gshared (InternalEnumerator_1_t500425236 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4213396078_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t500425236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t500425236 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4213396078(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1549070308_gshared (InternalEnumerator_1_t500425236 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1718082560  L_0 = InternalEnumerator_1_get_Current_m589867419((InternalEnumerator_1_t500425236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1718082560  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1549070308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t500425236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t500425236 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1549070308(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3274627657_gshared (InternalEnumerator_1_t500425236 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3274627657_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t500425236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t500425236 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3274627657(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3836612382_gshared (InternalEnumerator_1_t500425236 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3836612382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t500425236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t500425236 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3836612382(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m589867419_MetadataUsageId;
extern "C"  KeyValuePair_2_t1718082560  InternalEnumerator_1_get_Current_m589867419_gshared (InternalEnumerator_1_t500425236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m589867419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1718082560  L_8 = ((  KeyValuePair_2_t1718082560  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1718082560  InternalEnumerator_1_get_Current_m589867419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t500425236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t500425236 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m589867419(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1865178318_gshared (InternalEnumerator_1_t846010146 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1865178318_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	InternalEnumerator_1__ctor_m1865178318(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	{
		Link_t2063667470  L_0 = InternalEnumerator_1_get_Current_m1839009783((InternalEnumerator_1_t846010146 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2063667470  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1986195493_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1986195493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1986195493(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m608833986_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m608833986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m608833986(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId;
extern "C"  Link_t2063667470  InternalEnumerator_1_get_Current_m1839009783_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2063667470  L_8 = ((  Link_t2063667470  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2063667470  InternalEnumerator_1_get_Current_m1839009783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1839009783(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1735201994_gshared (InternalEnumerator_1_t1042872857 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1735201994_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	InternalEnumerator_1__ctor_m1735201994(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	{
		Slot_t2260530181  L_0 = InternalEnumerator_1_get_Current_m3036426419((InternalEnumerator_1_t1042872857 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2260530181  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2838377249_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2838377249_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2838377249(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2675725766_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2675725766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2675725766(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId;
extern "C"  Slot_t2260530181  InternalEnumerator_1_get_Current_m3036426419_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2260530181  L_8 = ((  Slot_t2260530181  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2260530181  InternalEnumerator_1_get_Current_m3036426419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3036426419(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m831950091_gshared (InternalEnumerator_1_t854365966 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m831950091_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	InternalEnumerator_1__ctor_m831950091(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	{
		Slot_t2072023290  L_0 = InternalEnumerator_1_get_Current_m2267962386((InternalEnumerator_1_t854365966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2072023290  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m857987234_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m857987234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	InternalEnumerator_1_Dispose_m857987234(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3764038305_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3764038305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3764038305(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId;
extern "C"  Slot_t2072023290  InternalEnumerator_1_get_Current_m2267962386_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2072023290  L_8 = ((  Slot_t2072023290  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2072023290  InternalEnumerator_1_get_Current_m2267962386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2267962386(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3756518911_gshared (InternalEnumerator_1_t3066004003 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3756518911_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	InternalEnumerator_1__ctor_m3756518911(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = InternalEnumerator_1_get_Current_m4103732200((InternalEnumerator_1_t3066004003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t4283661327  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1192762006_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1192762006_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1192762006(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m556104049_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m556104049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m556104049(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId;
extern "C"  DateTime_t4283661327  InternalEnumerator_1_get_Current_m4103732200_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t4283661327  L_8 = ((  DateTime_t4283661327  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DateTime_t4283661327  InternalEnumerator_1_get_Current_m4103732200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4103732200(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2868618147_gshared (InternalEnumerator_1_t736693307 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2868618147_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	InternalEnumerator_1__ctor_m2868618147(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	{
		Decimal_t1954350631  L_0 = InternalEnumerator_1_get_Current_m3602913834((InternalEnumerator_1_t736693307 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t1954350631  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m136301882_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m136301882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	InternalEnumerator_1_Dispose_m136301882(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2432816137_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2432816137_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2432816137(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId;
extern "C"  Decimal_t1954350631  InternalEnumerator_1_get_Current_m3602913834_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t1954350631  L_8 = ((  Decimal_t1954350631  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Decimal_t1954350631  InternalEnumerator_1_get_Current_m3602913834_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3602913834(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m923076597_gshared (InternalEnumerator_1_t2650569241 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m923076597_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	InternalEnumerator_1__ctor_m923076597(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	{
		double L_0 = InternalEnumerator_1_get_Current_m403053214((InternalEnumerator_1_t2650569241 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3854110732_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3854110732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3854110732(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253414715_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3253414715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3253414715(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m403053214_MetadataUsageId;
extern "C"  double InternalEnumerator_1_get_Current_m403053214_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m403053214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  double InternalEnumerator_1_get_Current_m403053214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m403053214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1585494054_gshared (InternalEnumerator_1_t4231148414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1585494054_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	InternalEnumerator_1__ctor_m1585494054(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = InternalEnumerator_1_get_Current_m296300717((InternalEnumerator_1_t4231148414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2583239037_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2583239037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2583239037(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253274534_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3253274534_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3253274534(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m296300717_MetadataUsageId;
extern "C"  int16_t InternalEnumerator_1_get_Current_m296300717_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m296300717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int16_t InternalEnumerator_1_get_Current_m296300717_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m296300717(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m767389920_gshared (InternalEnumerator_1_t4231148472 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m767389920_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	InternalEnumerator_1__ctor_m767389920(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1478851815((InternalEnumerator_1_t4231148472 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2745733815_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2745733815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2745733815(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3995645356_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3995645356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3995645356(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1478851815_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1478851815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1478851815(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3055898623_gshared (InternalEnumerator_1_t4231148567 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3055898623_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	InternalEnumerator_1__ctor_m3055898623(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = InternalEnumerator_1_get_Current_m1194254150((InternalEnumerator_1_t4231148567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m642251926_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m642251926_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	InternalEnumerator_1_Dispose_m642251926(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3212216237_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3212216237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3212216237(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId;
extern "C"  int64_t InternalEnumerator_1_get_Current_m1194254150_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int64_t InternalEnumerator_1_get_Current_m1194254150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1194254150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m152804899_gshared (InternalEnumerator_1_t2792744647 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m152804899_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	InternalEnumerator_1__ctor_m152804899(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = InternalEnumerator_1_get_Current_m2342284108((InternalEnumerator_1_t2792744647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1686038458_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1686038458_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1686038458(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m467683661_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m467683661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m467683661(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId;
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m2342284108_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m2342284108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2342284108(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2616641763_gshared (InternalEnumerator_1_t2953159047 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2616641763_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	InternalEnumerator_1__ctor_m2616641763(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = InternalEnumerator_1_get_Current_m2178852364((InternalEnumerator_1_t2953159047 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2760671866_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2760671866_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2760671866(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3716548237_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3716548237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3716548237(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId;
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m2178852364_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m2178852364_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2178852364(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2025782336_gshared (InternalEnumerator_1_t1841955665 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2025782336_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	InternalEnumerator_1__ctor_m2025782336(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = InternalEnumerator_1_get_Current_m2477961351((InternalEnumerator_1_t1841955665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t3059612989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3114194455_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3114194455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3114194455(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1042509516_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1042509516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1042509516(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  InternalEnumerator_1_get_Current_m2477961351_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t3059612989  L_8 = ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeNamedArgument_t3059612989  InternalEnumerator_1_get_Current_m2477961351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2477961351(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m374673841_gshared (InternalEnumerator_1_t2083636098 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m374673841_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	InternalEnumerator_1__ctor_m374673841(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = InternalEnumerator_1_get_Current_m3407736504((InternalEnumerator_1_t2083636098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t3301293422  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1367004360_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1367004360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1367004360(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2714191419_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2714191419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2714191419(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  InternalEnumerator_1_get_Current_m3407736504_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t3301293422  L_8 = ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeTypedArgument_t3301293422  InternalEnumerator_1_get_Current_m3407736504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3407736504(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m238137273_gshared (InternalEnumerator_1_t1990166460 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m238137273_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	InternalEnumerator_1__ctor_m238137273(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	{
		LabelData_t3207823784  L_0 = InternalEnumerator_1_get_Current_m2308068992((InternalEnumerator_1_t1990166460 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t3207823784  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3252411600_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3252411600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3252411600(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4284495539_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4284495539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4284495539(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId;
extern "C"  LabelData_t3207823784  InternalEnumerator_1_get_Current_m2308068992_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t3207823784  L_8 = ((  LabelData_t3207823784  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelData_t3207823784  InternalEnumerator_1_get_Current_m2308068992_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2308068992(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4080636215_gshared (InternalEnumerator_1_t3737689414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4080636215_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	InternalEnumerator_1__ctor_m4080636215(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t660379442  L_0 = InternalEnumerator_1_get_Current_m3571284320((InternalEnumerator_1_t3737689414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t660379442  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2169103310_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2169103310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2169103310(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1336166329_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1336166329_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1336166329(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId;
extern "C"  LabelFixup_t660379442  InternalEnumerator_1_get_Current_m3571284320_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t660379442  L_8 = ((  LabelFixup_t660379442  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelFixup_t660379442  InternalEnumerator_1_get_Current_m3571284320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3571284320(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2480959198_gshared (InternalEnumerator_1_t136423630 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2480959198_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	InternalEnumerator_1__ctor_m2480959198(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t1354080954  L_0 = InternalEnumerator_1_get_Current_m3618560037((InternalEnumerator_1_t136423630 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t1354080954  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1771263541_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1771263541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1771263541(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2010832750_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2010832750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2010832750(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId;
extern "C"  ILTokenInfo_t1354080954  InternalEnumerator_1_get_Current_m3618560037_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t1354080954  L_8 = ((  ILTokenInfo_t1354080954  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ILTokenInfo_t1354080954  InternalEnumerator_1_get_Current_m3618560037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3618560037(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2078231777_gshared (InternalEnumerator_1_t3819239998 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2078231777_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	InternalEnumerator_1__ctor_m2078231777(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t741930026  L_0 = InternalEnumerator_1_get_Current_m147444170((InternalEnumerator_1_t3819239998 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t741930026  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2387364856_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2387364856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2387364856(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3792346959_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3792346959_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3792346959(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m147444170_MetadataUsageId;
extern "C"  ParameterModifier_t741930026  InternalEnumerator_1_get_Current_m147444170_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m147444170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t741930026  L_8 = ((  ParameterModifier_t741930026  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ParameterModifier_t741930026  InternalEnumerator_1_get_Current_m147444170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m147444170(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2772733110_gshared (InternalEnumerator_1_t896245509 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2772733110_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	InternalEnumerator_1__ctor_m2772733110(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t2113902833  L_0 = InternalEnumerator_1_get_Current_m4042729375((InternalEnumerator_1_t896245509 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t2113902833  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m420635149_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m420635149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	InternalEnumerator_1_Dispose_m420635149(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4160471130_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4160471130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4160471130(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId;
extern "C"  ResourceCacheItem_t2113902833  InternalEnumerator_1_get_Current_m4042729375_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t2113902833  L_8 = ((  ResourceCacheItem_t2113902833  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceCacheItem_t2113902833  InternalEnumerator_1_get_Current_m4042729375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4042729375(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2865872515_gshared (InternalEnumerator_1_t2795948550 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2865872515_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	InternalEnumerator_1__ctor_m2865872515(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t4013605874  L_0 = InternalEnumerator_1_get_Current_m3891250378((InternalEnumerator_1_t2795948550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t4013605874  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3639607322_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3639607322_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3639607322(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3402661033_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3402661033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3402661033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId;
extern "C"  ResourceInfo_t4013605874  InternalEnumerator_1_get_Current_m3891250378_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t4013605874  L_8 = ((  ResourceInfo_t4013605874  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceInfo_t4013605874  InternalEnumerator_1_get_Current_m3891250378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3891250378(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2400880886_gshared (InternalEnumerator_1_t1203046106 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2400880886_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	InternalEnumerator_1__ctor_m2400880886(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m790384317((InternalEnumerator_1_t1203046106 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m44740173_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m44740173_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	InternalEnumerator_1_Dispose_m44740173(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2285731670_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2285731670_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2285731670(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m790384317_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m790384317_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m790384317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m790384317_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m790384317(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1925586605_gshared (InternalEnumerator_1_t4239079749 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1925586605_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	InternalEnumerator_1__ctor_m1925586605(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = InternalEnumerator_1_get_Current_m92522612((InternalEnumerator_1_t4239079749 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2750196932_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2750196932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2750196932(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134001983_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4134001983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4134001983(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m92522612_MetadataUsageId;
extern "C"  int8_t InternalEnumerator_1_get_Current_m92522612_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m92522612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int8_t InternalEnumerator_1_get_Current_m92522612_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m92522612(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3415441081_gshared (InternalEnumerator_1_t3844211903 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3415441081_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	InternalEnumerator_1__ctor_m3415441081(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t766901931  L_0 = InternalEnumerator_1_get_Current_m2419281506((InternalEnumerator_1_t3844211903 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t766901931  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2305059792_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2305059792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2305059792(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1811839991_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1811839991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1811839991(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId;
extern "C"  X509ChainStatus_t766901931  InternalEnumerator_1_get_Current_m2419281506_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t766901931  L_8 = ((  X509ChainStatus_t766901931  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  X509ChainStatus_t766901931  InternalEnumerator_1_get_Current_m2419281506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2419281506(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2203995436_gshared (InternalEnumerator_1_t3074261648 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2203995436_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	InternalEnumerator_1__ctor_m2203995436(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m1563251989((InternalEnumerator_1_t3074261648 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m479600131_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m479600131_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	InternalEnumerator_1_Dispose_m479600131(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1722801188_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1722801188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1722801188(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m1563251989_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m1563251989_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1563251989(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m207626719_gshared (InternalEnumerator_1_t2593882473 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m207626719_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	InternalEnumerator_1__ctor_m207626719(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	{
		Mark_t3811539797  L_0 = InternalEnumerator_1_get_Current_m3956653384((InternalEnumerator_1_t2593882473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t3811539797  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1252370038_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1252370038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1252370038(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2967245969_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2967245969_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2967245969(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId;
extern "C"  Mark_t3811539797  InternalEnumerator_1_get_Current_m3956653384_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t3811539797  L_8 = ((  Mark_t3811539797  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Mark_t3811539797  InternalEnumerator_1_get_Current_m3956653384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3956653384(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m111087899_gshared (InternalEnumerator_1_t3490832959 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m111087899_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	InternalEnumerator_1__ctor_m111087899(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t413522987  L_0 = InternalEnumerator_1_get_Current_m2948637700((InternalEnumerator_1_t3490832959 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t413522987  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m774844594_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m774844594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	InternalEnumerator_1_Dispose_m774844594(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m485566165_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m485566165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m485566165(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId;
extern "C"  TimeSpan_t413522987  InternalEnumerator_1_get_Current_m2948637700_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t413522987  L_8 = ((  TimeSpan_t413522987  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TimeSpan_t413522987  InternalEnumerator_1_get_Current_m2948637700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2948637700(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1037769859_gshared (InternalEnumerator_1_t3101977895 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1037769859_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	InternalEnumerator_1__ctor_m1037769859(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m737292716((InternalEnumerator_1_t3101977895 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1215749658_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1215749658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1215749658(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3068600045_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3068600045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3068600045(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m737292716_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m737292716_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m737292716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m737292716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m737292716(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m219665725_gshared (InternalEnumerator_1_t3101977953 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m219665725_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	InternalEnumerator_1__ctor_m219665725(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m1919843814((InternalEnumerator_1_t3101977953 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1378244436_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1378244436_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1378244436(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3810970867_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3810970867_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3810970867(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1919843814_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1919843814_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1919843814(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2508174428_gshared (InternalEnumerator_1_t3101978048 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2508174428_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	InternalEnumerator_1__ctor_m2508174428(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m1635246149((InternalEnumerator_1_t3101978048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3569729843_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3569729843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3569729843(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3027541748_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3027541748_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3027541748(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1635246149_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1635246149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1635246149(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2879257728_gshared (InternalEnumerator_1_t73011651 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2879257728_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	InternalEnumerator_1__ctor_m2879257728(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	{
		UriScheme_t1290668975  L_0 = InternalEnumerator_1_get_Current_m3881062791((InternalEnumerator_1_t73011651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t1290668975  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1861559895_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1861559895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1861559895(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m91355148_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m91355148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m91355148(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId;
extern "C"  UriScheme_t1290668975  InternalEnumerator_1_get_Current_m3881062791_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t1290668975  L_8 = ((  UriScheme_t1290668975  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UriScheme_t1290668975  InternalEnumerator_1_get_Current_m3881062791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3881062791(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2145162288_gshared (InternalEnumerator_1_t2440554239 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2145162288_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	InternalEnumerator_1__ctor_m2145162288(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	{
		NsDecl_t3658211563  L_0 = InternalEnumerator_1_get_Current_m105405465((InternalEnumerator_1_t2440554239 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsDecl_t3658211563  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3530332679_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3530332679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3530332679(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1353284256_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1353284256_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1353284256(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m105405465_MetadataUsageId;
extern "C"  NsDecl_t3658211563  InternalEnumerator_1_get_Current_m105405465_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m105405465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsDecl_t3658211563  L_8 = ((  NsDecl_t3658211563  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NsDecl_t3658211563  InternalEnumerator_1_get_Current_m105405465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m105405465(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3201546884_gshared (InternalEnumerator_1_t531556423 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3201546884_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	InternalEnumerator_1__ctor_m3201546884(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	{
		NsScope_t1749213747  L_0 = InternalEnumerator_1_get_Current_m860004555((InternalEnumerator_1_t531556423 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsScope_t1749213747  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1025235291_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1025235291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1025235291(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3063542280_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3063542280_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3063542280(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m860004555_MetadataUsageId;
extern "C"  NsScope_t1749213747  InternalEnumerator_1_get_Current_m860004555_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m860004555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsScope_t1749213747  L_8 = ((  NsScope_t1749213747  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NsScope_t1749213747  InternalEnumerator_1_get_Current_m860004555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m860004555(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Tutorial>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1716220943_gshared (InternalEnumerator_1_t3335230866 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1716220943_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3335230866 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3335230866 *>(__this + 1);
	InternalEnumerator_1__ctor_m1716220943(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Tutorial>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3150041393_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3150041393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3335230866 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3335230866 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3150041393(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Tutorial>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m934371229_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method)
{
	{
		Tutorial_t257920894  L_0 = InternalEnumerator_1_get_Current_m4224607958((InternalEnumerator_1_t3335230866 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Tutorial_t257920894  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m934371229_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3335230866 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3335230866 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m934371229(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Tutorial>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m385414310_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m385414310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3335230866 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3335230866 *>(__this + 1);
	InternalEnumerator_1_Dispose_m385414310(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Tutorial>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2844926109_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2844926109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3335230866 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3335230866 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2844926109(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Tutorial>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4224607958_MetadataUsageId;
extern "C"  Tutorial_t257920894  InternalEnumerator_1_get_Current_m4224607958_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4224607958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Tutorial_t257920894  L_8 = ((  Tutorial_t257920894  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Tutorial_t257920894  InternalEnumerator_1_get_Current_m4224607958_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3335230866 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3335230866 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4224607958(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1560867446_gshared (InternalEnumerator_1_t1640814777 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1560867446_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1640814777 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1640814777 *>(__this + 1);
	InternalEnumerator_1__ctor_m1560867446(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m595907434_gshared (InternalEnumerator_1_t1640814777 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m595907434_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1640814777 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1640814777 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m595907434(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2160639574_gshared (InternalEnumerator_1_t1640814777 * __this, const MethodInfo* method)
{
	{
		FadeEntry_t2858472101  L_0 = InternalEnumerator_1_get_Current_m3911490749((InternalEnumerator_1_t1640814777 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FadeEntry_t2858472101  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2160639574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1640814777 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1640814777 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2160639574(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4198954957_gshared (InternalEnumerator_1_t1640814777 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4198954957_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1640814777 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1640814777 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4198954957(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m120295126_gshared (InternalEnumerator_1_t1640814777 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m120295126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1640814777 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1640814777 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m120295126(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3911490749_MetadataUsageId;
extern "C"  FadeEntry_t2858472101  InternalEnumerator_1_get_Current_m3911490749_gshared (InternalEnumerator_1_t1640814777 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3911490749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		FadeEntry_t2858472101  L_8 = ((  FadeEntry_t2858472101  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  FadeEntry_t2858472101  InternalEnumerator_1_get_Current_m3911490749_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1640814777 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1640814777 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3911490749(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UICamera/DepthEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1378027210_gshared (InternalEnumerator_1_t4222924441 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1378027210_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4222924441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4222924441 *>(__this + 1);
	InternalEnumerator_1__ctor_m1378027210(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UICamera/DepthEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2047121046_gshared (InternalEnumerator_1_t4222924441 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2047121046_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4222924441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4222924441 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2047121046(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791742988_gshared (InternalEnumerator_1_t4222924441 * __this, const MethodInfo* method)
{
	{
		DepthEntry_t1145614469  L_0 = InternalEnumerator_1_get_Current_m283693171((InternalEnumerator_1_t4222924441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DepthEntry_t1145614469  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791742988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4222924441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4222924441 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791742988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UICamera/DepthEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2940904737_gshared (InternalEnumerator_1_t4222924441 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2940904737_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4222924441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4222924441 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2940904737(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UICamera/DepthEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3900747590_gshared (InternalEnumerator_1_t4222924441 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3900747590_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4222924441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4222924441 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3900747590(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UICamera/DepthEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m283693171_MetadataUsageId;
extern "C"  DepthEntry_t1145614469  InternalEnumerator_1_get_Current_m283693171_gshared (InternalEnumerator_1_t4222924441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m283693171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DepthEntry_t1145614469  L_8 = ((  DepthEntry_t1145614469  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DepthEntry_t1145614469  InternalEnumerator_1_get_Current_m283693171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4222924441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4222924441 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m283693171(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Bounds>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4055748925_gshared (InternalEnumerator_1_t1493984525 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4055748925_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1493984525 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1493984525 *>(__this + 1);
	InternalEnumerator_1__ctor_m4055748925(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Bounds>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m822401347_gshared (InternalEnumerator_1_t1493984525 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m822401347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1493984525 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1493984525 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m822401347(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Bounds>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4284209263_gshared (InternalEnumerator_1_t1493984525 * __this, const MethodInfo* method)
{
	{
		Bounds_t2711641849  L_0 = InternalEnumerator_1_get_Current_m867797956((InternalEnumerator_1_t1493984525 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Bounds_t2711641849  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4284209263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1493984525 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1493984525 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4284209263(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Bounds>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m852972372_gshared (InternalEnumerator_1_t1493984525 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m852972372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1493984525 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1493984525 *>(__this + 1);
	InternalEnumerator_1_Dispose_m852972372(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Bounds>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1395966511_gshared (InternalEnumerator_1_t1493984525 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1395966511_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1493984525 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1493984525 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1395966511(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Bounds>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m867797956_MetadataUsageId;
extern "C"  Bounds_t2711641849  InternalEnumerator_1_get_Current_m867797956_gshared (InternalEnumerator_1_t1493984525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m867797956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Bounds_t2711641849  L_8 = ((  Bounds_t2711641849  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Bounds_t2711641849  InternalEnumerator_1_get_Current_m867797956_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1493984525 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1493984525 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m867797956(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1839379985_gshared (InternalEnumerator_1_t2976889581 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1839379985_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	InternalEnumerator_1__ctor_m1839379985(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = InternalEnumerator_1_get_Current_m3730699578((InternalEnumerator_1_t2976889581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color_t4194546905  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m643330344_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m643330344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	InternalEnumerator_1_Dispose_m643330344(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1891900575_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1891900575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1891900575(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId;
extern "C"  Color_t4194546905  InternalEnumerator_1_get_Current_m3730699578_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color_t4194546905  L_8 = ((  Color_t4194546905  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color_t4194546905  InternalEnumerator_1_get_Current_m3730699578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3730699578(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2826013104_gshared (InternalEnumerator_1_t3676163660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2826013104_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	InternalEnumerator_1__ctor_m2826013104(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	{
		Color32_t598853688  L_0 = InternalEnumerator_1_get_Current_m3648321497((InternalEnumerator_1_t3676163660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t598853688  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4247678855_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4247678855_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4247678855(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1461072288_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1461072288_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1461072288(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId;
extern "C"  Color32_t598853688  InternalEnumerator_1_get_Current_m3648321497_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t598853688  L_8 = ((  Color32_t598853688  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color32_t598853688  InternalEnumerator_1_get_Current_m3648321497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3648321497(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2847645656_gshared (InternalEnumerator_1_t3320393320 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2847645656_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	InternalEnumerator_1__ctor_m2847645656(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t243083348  L_0 = InternalEnumerator_1_get_Current_m3523444575((InternalEnumerator_1_t3320393320 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t243083348  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2261686191_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2261686191_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2261686191(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2398593716_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2398593716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2398593716(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId;
extern "C"  ContactPoint_t243083348  InternalEnumerator_1_get_Current_m3523444575_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t243083348  L_8 = ((  ContactPoint_t243083348  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint_t243083348  InternalEnumerator_1_get_Current_m3523444575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3523444575(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
