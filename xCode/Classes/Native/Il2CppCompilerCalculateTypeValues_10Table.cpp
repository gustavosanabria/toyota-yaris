﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe3559239239.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2435538904.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1676616792.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220377.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220410.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220447.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220505.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220348.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220352.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3988332409.h"
#include "System_U3CModuleU3E86524790.h"
#include "System_Locale2281372282.h"
#include "System_System_MonoTODOAttribute2091695241.h"
#include "System_System_Collections_Specialized_HybridDictio3032146074.h"
#include "System_System_Collections_Specialized_ListDictiona2682732540.h"
#include "System_System_Collections_Specialized_ListDictiona2044266038.h"
#include "System_System_Collections_Specialized_ListDictiona3271093914.h"
#include "System_System_Collections_Specialized_NameObjectCo1023199937.h"
#include "System_System_Collections_Specialized_NameObjectCo1625138937.h"
#include "System_System_Collections_Specialized_NameObjectCo4030006590.h"
#include "System_System_Collections_Specialized_NameObjectCo1246329035.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1665593056.h"
#include "System_System_ComponentModel_EditorBrowsableState1963658837.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "System_System_ComponentModel_TypeConverterAttribut1738187778.h"
#include "System_System_ComponentModel_Win32Exception819808416.h"
#include "System_System_IO_Compression_CompressionMode1453657991.h"
#include "System_System_IO_Compression_DeflateStream2030147241.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag2055733333.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet1873379884.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe3250749483.h"
#include "System_System_IO_Compression_GZipStream183418746.h"
#include "System_System_Net_Security_AuthenticatedStream3871465409.h"
#include "System_System_Net_Security_AuthenticationLevel4161053470.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "System_System_Net_Security_SslStream490807902.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe3681700014.h"
#include "System_System_Net_Sockets_AddressFamily3770679850.h"
#include "System_System_Net_Sockets_LingerOption3290254502.h"
#include "System_System_Net_Sockets_MulticastOption979356607.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "System_System_Net_Sockets_ProtocolType3327388960.h"
#include "System_System_Net_Sockets_SelectMode3812195181.h"
#include "System_System_Net_Sockets_Socket2157335841.h"
#include "System_System_Net_Sockets_Socket_SocketOperation4113109398.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult753587752.h"
#include "System_System_Net_Sockets_Socket_Worker3140563676.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall742231849.h"
#include "System_System_Net_Sockets_SocketError4204345479.h"
#include "System_System_Net_Sockets_SocketException3119490894.h"
#include "System_System_Net_Sockets_SocketFlags4205073670.h"
#include "System_System_Net_Sockets_SocketOptionLevel2476940110.h"
#include "System_System_Net_Sockets_SocketOptionName1841276065.h"
#include "System_System_Net_Sockets_SocketShutdown1229168279.h"
#include "System_System_Net_Sockets_SocketType1204660219.h"
#include "System_System_Net_AuthenticationManager3105338575.h"
#include "System_System_Net_Authorization3486603059.h"
#include "System_System_Net_BasicClient720594003.h"
#include "System_System_Net_ChunkStream1623008007.h"
#include "System_System_Net_ChunkStream_State3599614847.h"
#include "System_System_Net_ChunkStream_Chunk3584500059.h"
#include "System_System_Net_Cookie2033273982.h"
#include "System_System_Net_CookieCollection2536410684.h"
#include "System_System_Net_CookieCollection_CookieCollectio2125669164.h"
#include "System_System_Net_CookieContainer230274359.h"
#include "System_System_Net_CookieException2122856709.h"
#include "System_System_Net_DecompressionMethods3697240007.h"
#include "System_System_Net_DefaultCertificatePolicy3264712578.h"
#include "System_System_Net_DigestHeaderParser3747762954.h"
#include "System_System_Net_DigestSession1153412844.h"
#include "System_System_Net_DigestClient3690764489.h"
#include "System_System_Net_Dns3289571299.h"
#include "System_System_Net_EndPoint1026786191.h"
#include "System_System_Net_FileWebRequest1151586641.h"
#include "System_System_Net_FileWebRequest_FileWebStream902488976.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba1444420308.h"
#include "System_System_Net_FileWebRequestCreator1433256879.h"
#include "System_System_Net_FileWebResponse2971811667.h"
#include "System_System_Net_FtpAsyncResult1090897169.h"
#include "System_System_Net_FtpDataStream2383958406.h"
#include "System_System_Net_FtpDataStream_WriteDelegate1637408689.h"
#include "System_System_Net_FtpDataStream_ReadDelegate3891738222.h"
#include "System_System_Net_FtpRequestCreator3707472729.h"
#include "System_System_Net_FtpStatus1259919022.h"
#include "System_System_Net_FtpStatusCode1354479291.h"
#include "System_System_Net_FtpWebRequest3084461655.h"
#include "System_System_Net_FtpWebRequest_RequestState430963524.h"
#include "System_System_Net_FtpWebResponse2761394957.h"
#include "System_System_Net_GlobalProxySelection183150427.h"
#include "System_System_Net_HttpRequestCreator1200682847.h"
#include "System_System_Net_HttpStatusCode2219054529.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (PrivateKeySelectionCallback_t3559239239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238934), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1001[15] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (U24ArrayTypeU243132_t435538905)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t435538905_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (U24ArrayTypeU24256_t1676616793)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616793_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (U24ArrayTypeU2420_t3379220378)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220378_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (U24ArrayTypeU2432_t3379220411)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3379220411_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (U24ArrayTypeU2448_t3379220448)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t3379220448_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (U24ArrayTypeU2464_t3379220506)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t3379220506_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (U24ArrayTypeU2412_t3379220349)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220349_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (U24ArrayTypeU2416_t3379220353)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220353_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (U24ArrayTypeU244_t3988332409)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t3988332409_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (U3CModuleU3E_t86524792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { sizeof (Locale_t2281372284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (MonoTODOAttribute_t2091695242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1013[1] = 
{
	MonoTODOAttribute_t2091695242::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1014[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1015[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1016[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1017[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1018[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1019[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1020[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (HybridDictionary_t3032146074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1021[3] = 
{
	HybridDictionary_t3032146074::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t3032146074::get_offset_of_hashtable_1(),
	HybridDictionary_t3032146074::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (ListDictionary_t2682732540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1022[4] = 
{
	ListDictionary_t2682732540::get_offset_of_count_0(),
	ListDictionary_t2682732540::get_offset_of_version_1(),
	ListDictionary_t2682732540::get_offset_of_head_2(),
	ListDictionary_t2682732540::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (DictionaryNode_t2044266038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1023[3] = 
{
	DictionaryNode_t2044266038::get_offset_of_key_0(),
	DictionaryNode_t2044266038::get_offset_of_value_1(),
	DictionaryNode_t2044266038::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (DictionaryNodeEnumerator_t3271093914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1024[4] = 
{
	DictionaryNodeEnumerator_t3271093914::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (NameObjectCollectionBase_t1023199937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1025[10] = 
{
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t1023199937::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t1023199937::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t1023199937::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (_Item_t1625138937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1026[2] = 
{
	_Item_t1625138937::get_offset_of_key_0(),
	_Item_t1625138937::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (_KeysEnumerator_t4030006590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1027[2] = 
{
	_KeysEnumerator_t4030006590::get_offset_of_m_collection_0(),
	_KeysEnumerator_t4030006590::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (KeysCollection_t1246329035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1028[1] = 
{
	KeysCollection_t1246329035::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (NameValueCollection_t2791941106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1029[2] = 
{
	NameValueCollection_t2791941106::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t2791941106::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (EditorBrowsableAttribute_t1665593056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1030[1] = 
{
	EditorBrowsableAttribute_t1665593056::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (EditorBrowsableState_t1963658837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1031[4] = 
{
	EditorBrowsableState_t1963658837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (TypeConverter_t1753450284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { sizeof (TypeConverterAttribute_t1738187778), -1, sizeof(TypeConverterAttribute_t1738187778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1033[2] = 
{
	TypeConverterAttribute_t1738187778_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t1738187778::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { sizeof (Win32Exception_t819808416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1034[1] = 
{
	Win32Exception_t819808416::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { sizeof (CompressionMode_t1453657991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1035[3] = 
{
	CompressionMode_t1453657991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { sizeof (DeflateStream_t2030147241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1036[8] = 
{
	DeflateStream_t2030147241::get_offset_of_base_stream_1(),
	DeflateStream_t2030147241::get_offset_of_mode_2(),
	DeflateStream_t2030147241::get_offset_of_leaveOpen_3(),
	DeflateStream_t2030147241::get_offset_of_disposed_4(),
	DeflateStream_t2030147241::get_offset_of_feeder_5(),
	DeflateStream_t2030147241::get_offset_of_z_stream_6(),
	DeflateStream_t2030147241::get_offset_of_io_buffer_7(),
	DeflateStream_t2030147241::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (UnmanagedReadOrWrite_t2055733333), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (ReadMethod_t1873379884), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { sizeof (WriteMethod_t3250749483), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (GZipStream_t183418746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1040[1] = 
{
	GZipStream_t183418746::get_offset_of_deflateStream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (AuthenticatedStream_t3871465409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1041[2] = 
{
	AuthenticatedStream_t3871465409::get_offset_of_innerStream_1(),
	AuthenticatedStream_t3871465409::get_offset_of_leaveStreamOpen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { sizeof (AuthenticationLevel_t4161053470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1042[4] = 
{
	AuthenticationLevel_t4161053470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { sizeof (SslPolicyErrors_t3099591579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1043[5] = 
{
	SslPolicyErrors_t3099591579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (SslStream_t490807902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1044[3] = 
{
	SslStream_t490807902::get_offset_of_ssl_stream_3(),
	SslStream_t490807902::get_offset_of_validation_callback_4(),
	SslStream_t490807902::get_offset_of_selection_callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1045[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (AddressFamily_t3770679850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1046[32] = 
{
	AddressFamily_t3770679850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (LingerOption_t3290254502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1047[2] = 
{
	LingerOption_t3290254502::get_offset_of_enabled_0(),
	LingerOption_t3290254502::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (MulticastOption_t979356607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (NetworkStream_t3953762560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1049[6] = 
{
	NetworkStream_t3953762560::get_offset_of_access_1(),
	NetworkStream_t3953762560::get_offset_of_socket_2(),
	NetworkStream_t3953762560::get_offset_of_owns_socket_3(),
	NetworkStream_t3953762560::get_offset_of_readable_4(),
	NetworkStream_t3953762560::get_offset_of_writeable_5(),
	NetworkStream_t3953762560::get_offset_of_disposed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (ProtocolType_t3327388960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1050[26] = 
{
	ProtocolType_t3327388960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (SelectMode_t3812195181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1051[4] = 
{
	SelectMode_t3812195181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { sizeof (Socket_t2157335841), -1, sizeof(Socket_t2157335841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1052[22] = 
{
	Socket_t2157335841::get_offset_of_readQ_0(),
	Socket_t2157335841::get_offset_of_writeQ_1(),
	Socket_t2157335841::get_offset_of_islistening_2(),
	Socket_t2157335841::get_offset_of_MinListenPort_3(),
	Socket_t2157335841::get_offset_of_MaxListenPort_4(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t2157335841::get_offset_of_linger_timeout_7(),
	Socket_t2157335841::get_offset_of_socket_8(),
	Socket_t2157335841::get_offset_of_address_family_9(),
	Socket_t2157335841::get_offset_of_socket_type_10(),
	Socket_t2157335841::get_offset_of_protocol_type_11(),
	Socket_t2157335841::get_offset_of_blocking_12(),
	Socket_t2157335841::get_offset_of_blocking_thread_13(),
	Socket_t2157335841::get_offset_of_isbound_14(),
	Socket_t2157335841_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t2157335841::get_offset_of_max_bind_count_16(),
	Socket_t2157335841::get_offset_of_connected_17(),
	Socket_t2157335841::get_offset_of_closed_18(),
	Socket_t2157335841::get_offset_of_disposed_19(),
	Socket_t2157335841::get_offset_of_seed_endpoint_20(),
	Socket_t2157335841_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (SocketOperation_t4113109398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1053[15] = 
{
	SocketOperation_t4113109398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (SocketAsyncResult_t753587752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1054[25] = 
{
	SocketAsyncResult_t753587752::get_offset_of_Sock_0(),
	SocketAsyncResult_t753587752::get_offset_of_handle_1(),
	SocketAsyncResult_t753587752::get_offset_of_state_2(),
	SocketAsyncResult_t753587752::get_offset_of_callback_3(),
	SocketAsyncResult_t753587752::get_offset_of_waithandle_4(),
	SocketAsyncResult_t753587752::get_offset_of_delayedException_5(),
	SocketAsyncResult_t753587752::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t753587752::get_offset_of_Buffer_7(),
	SocketAsyncResult_t753587752::get_offset_of_Offset_8(),
	SocketAsyncResult_t753587752::get_offset_of_Size_9(),
	SocketAsyncResult_t753587752::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t753587752::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t753587752::get_offset_of_Addresses_12(),
	SocketAsyncResult_t753587752::get_offset_of_Port_13(),
	SocketAsyncResult_t753587752::get_offset_of_Buffers_14(),
	SocketAsyncResult_t753587752::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t753587752::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t753587752::get_offset_of_total_17(),
	SocketAsyncResult_t753587752::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t753587752::get_offset_of_completed_19(),
	SocketAsyncResult_t753587752::get_offset_of_blocking_20(),
	SocketAsyncResult_t753587752::get_offset_of_error_21(),
	SocketAsyncResult_t753587752::get_offset_of_operation_22(),
	SocketAsyncResult_t753587752::get_offset_of_ares_23(),
	SocketAsyncResult_t753587752::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (Worker_t3140563676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1055[3] = 
{
	Worker_t3140563676::get_offset_of_result_0(),
	Worker_t3140563676::get_offset_of_requireSocketSecurity_1(),
	Worker_t3140563676::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (SocketAsyncCall_t742231849), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (SocketError_t4204345479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1057[48] = 
{
	SocketError_t4204345479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (SocketException_t3119490894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (SocketFlags_t4205073670)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1059[11] = 
{
	SocketFlags_t4205073670::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (SocketOptionLevel_t2476940110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1060[6] = 
{
	SocketOptionLevel_t2476940110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (SocketOptionName_t1841276065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1061[44] = 
{
	SocketOptionName_t1841276065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (SocketShutdown_t1229168279)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1062[4] = 
{
	SocketShutdown_t1229168279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { sizeof (SocketType_t1204660219)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1063[7] = 
{
	SocketType_t1204660219::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { sizeof (AuthenticationManager_t3105338575), -1, sizeof(AuthenticationManager_t3105338575_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1064[3] = 
{
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { sizeof (Authorization_t3486603059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1065[4] = 
{
	Authorization_t3486603059::get_offset_of_token_0(),
	Authorization_t3486603059::get_offset_of_complete_1(),
	Authorization_t3486603059::get_offset_of_connectionGroupId_2(),
	Authorization_t3486603059::get_offset_of_module_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (BasicClient_t720594003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (ChunkStream_t1623008007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1067[9] = 
{
	ChunkStream_t1623008007::get_offset_of_headers_0(),
	ChunkStream_t1623008007::get_offset_of_chunkSize_1(),
	ChunkStream_t1623008007::get_offset_of_chunkRead_2(),
	ChunkStream_t1623008007::get_offset_of_state_3(),
	ChunkStream_t1623008007::get_offset_of_saved_4(),
	ChunkStream_t1623008007::get_offset_of_sawCR_5(),
	ChunkStream_t1623008007::get_offset_of_gotit_6(),
	ChunkStream_t1623008007::get_offset_of_trailerState_7(),
	ChunkStream_t1623008007::get_offset_of_chunks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (State_t3599614847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1068[5] = 
{
	State_t3599614847::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (Chunk_t3584500059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1069[2] = 
{
	Chunk_t3584500059::get_offset_of_Bytes_0(),
	Chunk_t3584500059::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (Cookie_t2033273982), -1, sizeof(Cookie_t2033273982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1070[18] = 
{
	Cookie_t2033273982::get_offset_of_comment_0(),
	Cookie_t2033273982::get_offset_of_commentUri_1(),
	Cookie_t2033273982::get_offset_of_discard_2(),
	Cookie_t2033273982::get_offset_of_domain_3(),
	Cookie_t2033273982::get_offset_of_expires_4(),
	Cookie_t2033273982::get_offset_of_httpOnly_5(),
	Cookie_t2033273982::get_offset_of_name_6(),
	Cookie_t2033273982::get_offset_of_path_7(),
	Cookie_t2033273982::get_offset_of_port_8(),
	Cookie_t2033273982::get_offset_of_ports_9(),
	Cookie_t2033273982::get_offset_of_secure_10(),
	Cookie_t2033273982::get_offset_of_timestamp_11(),
	Cookie_t2033273982::get_offset_of_val_12(),
	Cookie_t2033273982::get_offset_of_version_13(),
	Cookie_t2033273982_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t2033273982_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t2033273982_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t2033273982::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (CookieCollection_t2536410684), -1, sizeof(CookieCollection_t2536410684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1071[2] = 
{
	CookieCollection_t2536410684::get_offset_of_list_0(),
	CookieCollection_t2536410684_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (CookieCollectionComparer_t2125669164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (CookieContainer_t230274359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1073[4] = 
{
	CookieContainer_t230274359::get_offset_of_capacity_0(),
	CookieContainer_t230274359::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t230274359::get_offset_of_maxCookieSize_2(),
	CookieContainer_t230274359::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (CookieException_t2122856709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (DecompressionMethods_t3697240007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1075[4] = 
{
	DecompressionMethods_t3697240007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (DefaultCertificatePolicy_t3264712578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (DigestHeaderParser_t3747762954), -1, sizeof(DigestHeaderParser_t3747762954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1077[5] = 
{
	DigestHeaderParser_t3747762954::get_offset_of_header_0(),
	DigestHeaderParser_t3747762954::get_offset_of_length_1(),
	DigestHeaderParser_t3747762954::get_offset_of_pos_2(),
	DigestHeaderParser_t3747762954_StaticFields::get_offset_of_keywords_3(),
	DigestHeaderParser_t3747762954::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (DigestSession_t1153412844), -1, sizeof(DigestSession_t1153412844_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1078[6] = 
{
	DigestSession_t1153412844_StaticFields::get_offset_of_rng_0(),
	DigestSession_t1153412844::get_offset_of_lastUse_1(),
	DigestSession_t1153412844::get_offset_of__nc_2(),
	DigestSession_t1153412844::get_offset_of_hash_3(),
	DigestSession_t1153412844::get_offset_of_parser_4(),
	DigestSession_t1153412844::get_offset_of__cnonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (DigestClient_t3690764489), -1, sizeof(DigestClient_t3690764489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1079[1] = 
{
	DigestClient_t3690764489_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (Dns_t3289571299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (EndPoint_t1026786191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (FileWebRequest_t1151586641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1082[14] = 
{
	FileWebRequest_t1151586641::get_offset_of_uri_6(),
	FileWebRequest_t1151586641::get_offset_of_webHeaders_7(),
	FileWebRequest_t1151586641::get_offset_of_credentials_8(),
	FileWebRequest_t1151586641::get_offset_of_connectionGroup_9(),
	FileWebRequest_t1151586641::get_offset_of_contentLength_10(),
	FileWebRequest_t1151586641::get_offset_of_fileAccess_11(),
	FileWebRequest_t1151586641::get_offset_of_method_12(),
	FileWebRequest_t1151586641::get_offset_of_proxy_13(),
	FileWebRequest_t1151586641::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t1151586641::get_offset_of_timeout_15(),
	FileWebRequest_t1151586641::get_offset_of_webResponse_16(),
	FileWebRequest_t1151586641::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t1151586641::get_offset_of_requesting_18(),
	FileWebRequest_t1151586641::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (FileWebStream_t902488976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1083[1] = 
{
	FileWebStream_t902488976::get_offset_of_webRequest_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (GetResponseCallback_t1444420308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (FileWebRequestCreator_t1433256879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (FileWebResponse_t2971811667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1086[5] = 
{
	FileWebResponse_t2971811667::get_offset_of_responseUri_1(),
	FileWebResponse_t2971811667::get_offset_of_fileStream_2(),
	FileWebResponse_t2971811667::get_offset_of_contentLength_3(),
	FileWebResponse_t2971811667::get_offset_of_webHeaders_4(),
	FileWebResponse_t2971811667::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (FtpAsyncResult_t1090897169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1087[9] = 
{
	FtpAsyncResult_t1090897169::get_offset_of_response_0(),
	FtpAsyncResult_t1090897169::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t1090897169::get_offset_of_exception_2(),
	FtpAsyncResult_t1090897169::get_offset_of_callback_3(),
	FtpAsyncResult_t1090897169::get_offset_of_stream_4(),
	FtpAsyncResult_t1090897169::get_offset_of_state_5(),
	FtpAsyncResult_t1090897169::get_offset_of_completed_6(),
	FtpAsyncResult_t1090897169::get_offset_of_synch_7(),
	FtpAsyncResult_t1090897169::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (FtpDataStream_t2383958406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1088[5] = 
{
	FtpDataStream_t2383958406::get_offset_of_request_1(),
	FtpDataStream_t2383958406::get_offset_of_networkStream_2(),
	FtpDataStream_t2383958406::get_offset_of_disposed_3(),
	FtpDataStream_t2383958406::get_offset_of_isRead_4(),
	FtpDataStream_t2383958406::get_offset_of_totalRead_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (WriteDelegate_t1637408689), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (ReadDelegate_t3891738222), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { sizeof (FtpRequestCreator_t3707472729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (FtpStatus_t1259919022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1092[2] = 
{
	FtpStatus_t1259919022::get_offset_of_statusCode_0(),
	FtpStatus_t1259919022::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (FtpStatusCode_t1354479291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1093[38] = 
{
	FtpStatusCode_t1354479291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (FtpWebRequest_t3084461655), -1, sizeof(FtpWebRequest_t3084461655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1094[31] = 
{
	FtpWebRequest_t3084461655::get_offset_of_requestUri_6(),
	FtpWebRequest_t3084461655::get_offset_of_file_name_7(),
	FtpWebRequest_t3084461655::get_offset_of_servicePoint_8(),
	FtpWebRequest_t3084461655::get_offset_of_origDataStream_9(),
	FtpWebRequest_t3084461655::get_offset_of_dataStream_10(),
	FtpWebRequest_t3084461655::get_offset_of_controlStream_11(),
	FtpWebRequest_t3084461655::get_offset_of_controlReader_12(),
	FtpWebRequest_t3084461655::get_offset_of_credentials_13(),
	FtpWebRequest_t3084461655::get_offset_of_hostEntry_14(),
	FtpWebRequest_t3084461655::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t3084461655::get_offset_of_proxy_16(),
	FtpWebRequest_t3084461655::get_offset_of_timeout_17(),
	FtpWebRequest_t3084461655::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t3084461655::get_offset_of_offset_19(),
	FtpWebRequest_t3084461655::get_offset_of_binary_20(),
	FtpWebRequest_t3084461655::get_offset_of_enableSsl_21(),
	FtpWebRequest_t3084461655::get_offset_of_usePassive_22(),
	FtpWebRequest_t3084461655::get_offset_of_keepAlive_23(),
	FtpWebRequest_t3084461655::get_offset_of_method_24(),
	FtpWebRequest_t3084461655::get_offset_of_renameTo_25(),
	FtpWebRequest_t3084461655::get_offset_of_locker_26(),
	FtpWebRequest_t3084461655::get_offset_of_requestState_27(),
	FtpWebRequest_t3084461655::get_offset_of_asyncResult_28(),
	FtpWebRequest_t3084461655::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t3084461655::get_offset_of_requestStream_30(),
	FtpWebRequest_t3084461655::get_offset_of_initial_path_31(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t3084461655::get_offset_of_callback_33(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_35(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (RequestState_t430963524)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1095[10] = 
{
	RequestState_t430963524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (FtpWebResponse_t2761394957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1096[12] = 
{
	FtpWebResponse_t2761394957::get_offset_of_stream_1(),
	FtpWebResponse_t2761394957::get_offset_of_uri_2(),
	FtpWebResponse_t2761394957::get_offset_of_statusCode_3(),
	FtpWebResponse_t2761394957::get_offset_of_lastModified_4(),
	FtpWebResponse_t2761394957::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2761394957::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2761394957::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2761394957::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2761394957::get_offset_of_method_9(),
	FtpWebResponse_t2761394957::get_offset_of_disposed_10(),
	FtpWebResponse_t2761394957::get_offset_of_request_11(),
	FtpWebResponse_t2761394957::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (GlobalProxySelection_t183150427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (HttpRequestCreator_t1200682847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (HttpStatusCode_t2219054529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1099[47] = 
{
	HttpStatusCode_t2219054529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
