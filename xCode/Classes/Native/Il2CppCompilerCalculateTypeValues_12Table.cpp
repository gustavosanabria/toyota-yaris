﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_InterpreterF3485052664.h"
#include "System_System_Text_RegularExpressions_PatternCompi3989440925.h"
#include "System_System_Text_RegularExpressions_PatternCompi3356653547.h"
#include "System_System_Text_RegularExpressions_PatternCompil396179390.h"
#include "System_System_Text_RegularExpressions_LinkStack1760044604.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "System_System_Text_RegularExpressions_Interpreter4223808840.h"
#include "System_System_Text_RegularExpressions_Interpreter_3630763131.h"
#include "System_System_Text_RegularExpressions_Interpreter_1764265010.h"
#include "System_System_Text_RegularExpressions_Interpreter_1939935045.h"
#include "System_System_Text_RegularExpressions_Interval2482260685.h"
#include "System_System_Text_RegularExpressions_IntervalColl1888974603.h"
#include "System_System_Text_RegularExpressions_IntervalColl3676602947.h"
#include "System_System_Text_RegularExpressions_IntervalColl1292950321.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse3926544077.h"
#include "System_System_Text_RegularExpressions_QuickSearch2109051075.h"
#include "System_System_Text_RegularExpressions_ReplacementEv697641157.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre2279826820.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3183027782.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo2628834993.h"
#include "System_System_Text_RegularExpressions_Syntax_Group3733269553.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul1862766086.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu2867563114.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan1061768724.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBa3370744674.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet2377834527.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser3981028276.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3434259338.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3534504316.h"
#include "System_System_Text_RegularExpressions_Syntax_Alter3434519311.h"
#include "System_System_Text_RegularExpressions_Syntax_Liter2061497825.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit3788287627.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1741476861.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs2734841617.h"
#include "System_System_Text_RegularExpressions_Syntax_Chara2058232957.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho3681078449.h"
#include "System_System_DefaultUriParser3145002206.h"
#include "System_System_GenericUriParser444686856.h"
#include "System_System_Uri1116831938.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "System_System_UriFormatException308538560.h"
#include "System_System_UriHostNameType959572879.h"
#include "System_System_UriKind238866934.h"
#include "System_System_UriParser3685110593.h"
#include "System_System_UriPartial875461417.h"
#include "System_System_UriTypeConverter2523083502.h"
#include "System_System_Net_Security_LocalCertificateSelecti2431285719.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "System_System_Net_BindIPEndPoint3006124499.h"
#include "System_System_Net_HttpContinueDelegate1707598350.h"
#include "System_System_Text_RegularExpressions_MatchEvaluat1719977010.h"
#include "System_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220352.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1676615740.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220348.h"
#include "System_Core_U3CModuleU3E86524790.h"
#include "System_Core_System_Runtime_CompilerServices_Extens2299149759.h"
#include "System_Core_Locale2281372282.h"
#include "System_Core_System_MonoTODOAttribute2091695241.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder373726640.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTra131863657.h"
#include "System_Core_System_Linq_Check10677726.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "System_Core_System_Security_Cryptography_Aes2466798581.h"
#include "System_Core_System_Security_Cryptography_AesManaged23175804.h"
#include "System_Core_System_Security_Cryptography_AesTransf1787635017.h"
#include "System_Core_System_Action3771233898.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615769.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615732.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676616792.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A435478332.h"
#include "UnityEngine_U3CModuleU3E86524790.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1416890373.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest_Co561206607.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2154290273.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_PrimitiveType1035833655.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"
#include "UnityEngine_UnityEngine_Cursor2745727898.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (InterpreterFactory_t3485052664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1200[4] = 
{
	InterpreterFactory_t3485052664::get_offset_of_mapping_0(),
	InterpreterFactory_t3485052664::get_offset_of_pattern_1(),
	InterpreterFactory_t3485052664::get_offset_of_namesMapping_2(),
	InterpreterFactory_t3485052664::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (PatternCompiler_t3989440925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1201[1] = 
{
	PatternCompiler_t3989440925::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (PatternLinkStack_t3356653547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1202[1] = 
{
	PatternLinkStack_t3356653547::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (Link_t396179390)+ sizeof (Il2CppObject), sizeof(Link_t396179390_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1203[2] = 
{
	Link_t396179390::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t396179390::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (LinkStack_t1760044604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1204[1] = 
{
	LinkStack_t1760044604::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (Mark_t3811539797)+ sizeof (Il2CppObject), sizeof(Mark_t3811539797_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1205[3] = 
{
	Mark_t3811539797::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3811539797::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3811539797::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (Interpreter_t4223808840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1206[16] = 
{
	Interpreter_t4223808840::get_offset_of_program_1(),
	Interpreter_t4223808840::get_offset_of_program_start_2(),
	Interpreter_t4223808840::get_offset_of_text_3(),
	Interpreter_t4223808840::get_offset_of_text_end_4(),
	Interpreter_t4223808840::get_offset_of_group_count_5(),
	Interpreter_t4223808840::get_offset_of_match_min_6(),
	Interpreter_t4223808840::get_offset_of_qs_7(),
	Interpreter_t4223808840::get_offset_of_scan_ptr_8(),
	Interpreter_t4223808840::get_offset_of_repeat_9(),
	Interpreter_t4223808840::get_offset_of_fast_10(),
	Interpreter_t4223808840::get_offset_of_stack_11(),
	Interpreter_t4223808840::get_offset_of_deep_12(),
	Interpreter_t4223808840::get_offset_of_marks_13(),
	Interpreter_t4223808840::get_offset_of_mark_start_14(),
	Interpreter_t4223808840::get_offset_of_mark_end_15(),
	Interpreter_t4223808840::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (IntStack_t3630763131)+ sizeof (Il2CppObject), sizeof(IntStack_t3630763131_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1207[2] = 
{
	IntStack_t3630763131::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t3630763131::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (RepeatContext_t1764265010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1208[7] = 
{
	RepeatContext_t1764265010::get_offset_of_start_0(),
	RepeatContext_t1764265010::get_offset_of_min_1(),
	RepeatContext_t1764265010::get_offset_of_max_2(),
	RepeatContext_t1764265010::get_offset_of_lazy_3(),
	RepeatContext_t1764265010::get_offset_of_expr_pc_4(),
	RepeatContext_t1764265010::get_offset_of_previous_5(),
	RepeatContext_t1764265010::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (Mode_t1939935045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1209[4] = 
{
	Mode_t1939935045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (Interval_t2482260685)+ sizeof (Il2CppObject), sizeof(Interval_t2482260685_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1210[3] = 
{
	Interval_t2482260685::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2482260685::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2482260685::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (IntervalCollection_t1888974603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1211[1] = 
{
	IntervalCollection_t1888974603::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (Enumerator_t3676602947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1212[2] = 
{
	Enumerator_t3676602947::get_offset_of_list_0(),
	Enumerator_t3676602947::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (CostDelegate_t1292950321), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (Parser_t3926544077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1214[6] = 
{
	Parser_t3926544077::get_offset_of_pattern_0(),
	Parser_t3926544077::get_offset_of_ptr_1(),
	Parser_t3926544077::get_offset_of_caps_2(),
	Parser_t3926544077::get_offset_of_refs_3(),
	Parser_t3926544077::get_offset_of_num_groups_4(),
	Parser_t3926544077::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (QuickSearch_t2109051075), -1, sizeof(QuickSearch_t2109051075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1215[7] = 
{
	QuickSearch_t2109051075::get_offset_of_str_0(),
	QuickSearch_t2109051075::get_offset_of_len_1(),
	QuickSearch_t2109051075::get_offset_of_ignore_2(),
	QuickSearch_t2109051075::get_offset_of_reverse_3(),
	QuickSearch_t2109051075::get_offset_of_shift_4(),
	QuickSearch_t2109051075::get_offset_of_shiftExtended_5(),
	QuickSearch_t2109051075_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (ReplacementEvaluator_t697641157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1216[4] = 
{
	ReplacementEvaluator_t697641157::get_offset_of_regex_0(),
	ReplacementEvaluator_t697641157::get_offset_of_n_pieces_1(),
	ReplacementEvaluator_t697641157::get_offset_of_pieces_2(),
	ReplacementEvaluator_t697641157::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (ExpressionCollection_t2279826820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (Expression_t3183027782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (CompositeExpression_t2628834993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[1] = 
{
	CompositeExpression_t2628834993::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (Group_t3733269553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (RegularExpression_t1862766086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1221[1] = 
{
	RegularExpression_t1862766086::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (CapturingGroup_t2867563114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1222[2] = 
{
	CapturingGroup_t2867563114::get_offset_of_gid_1(),
	CapturingGroup_t2867563114::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (BalancingGroup_t1061768724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1223[1] = 
{
	BalancingGroup_t1061768724::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (NonBacktrackingGroup_t3370744674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (Repetition_t2377834527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1225[3] = 
{
	Repetition_t2377834527::get_offset_of_min_1(),
	Repetition_t2377834527::get_offset_of_max_2(),
	Repetition_t2377834527::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (Assertion_t3981028276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (CaptureAssertion_t3434259338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[3] = 
{
	CaptureAssertion_t3434259338::get_offset_of_alternate_1(),
	CaptureAssertion_t3434259338::get_offset_of_group_2(),
	CaptureAssertion_t3434259338::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (ExpressionAssertion_t3534504316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1228[2] = 
{
	ExpressionAssertion_t3534504316::get_offset_of_reverse_1(),
	ExpressionAssertion_t3534504316::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (Alternation_t3434519311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (Literal_t2061497825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1230[2] = 
{
	Literal_t2061497825::get_offset_of_str_0(),
	Literal_t2061497825::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (PositionAssertion_t3788287627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1231[1] = 
{
	PositionAssertion_t3788287627::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (Reference_t1741476861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1232[2] = 
{
	Reference_t1741476861::get_offset_of_group_0(),
	Reference_t1741476861::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (BackslashNumber_t2734841617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1233[2] = 
{
	BackslashNumber_t2734841617::get_offset_of_literal_2(),
	BackslashNumber_t2734841617::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (CharacterClass_t2058232957), -1, sizeof(CharacterClass_t2058232957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1234[6] = 
{
	CharacterClass_t2058232957_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t2058232957::get_offset_of_negate_1(),
	CharacterClass_t2058232957::get_offset_of_ignore_2(),
	CharacterClass_t2058232957::get_offset_of_pos_cats_3(),
	CharacterClass_t2058232957::get_offset_of_neg_cats_4(),
	CharacterClass_t2058232957::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (AnchorInfo_t3681078449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1235[6] = 
{
	AnchorInfo_t3681078449::get_offset_of_expr_0(),
	AnchorInfo_t3681078449::get_offset_of_pos_1(),
	AnchorInfo_t3681078449::get_offset_of_offset_2(),
	AnchorInfo_t3681078449::get_offset_of_str_3(),
	AnchorInfo_t3681078449::get_offset_of_width_4(),
	AnchorInfo_t3681078449::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (DefaultUriParser_t3145002206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (GenericUriParser_t444686856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (Uri_t1116831938), -1, sizeof(Uri_t1116831938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1238[38] = 
{
	0,
	Uri_t1116831938::get_offset_of_isUnixFilePath_1(),
	Uri_t1116831938::get_offset_of_source_2(),
	Uri_t1116831938::get_offset_of_scheme_3(),
	Uri_t1116831938::get_offset_of_host_4(),
	Uri_t1116831938::get_offset_of_port_5(),
	Uri_t1116831938::get_offset_of_path_6(),
	Uri_t1116831938::get_offset_of_query_7(),
	Uri_t1116831938::get_offset_of_fragment_8(),
	Uri_t1116831938::get_offset_of_userinfo_9(),
	Uri_t1116831938::get_offset_of_isUnc_10(),
	Uri_t1116831938::get_offset_of_isOpaquePart_11(),
	Uri_t1116831938::get_offset_of_isAbsoluteUri_12(),
	Uri_t1116831938::get_offset_of_segments_13(),
	Uri_t1116831938::get_offset_of_userEscaped_14(),
	Uri_t1116831938::get_offset_of_cachedAbsoluteUri_15(),
	Uri_t1116831938::get_offset_of_cachedToString_16(),
	Uri_t1116831938::get_offset_of_cachedLocalPath_17(),
	Uri_t1116831938::get_offset_of_cachedHashCode_18(),
	Uri_t1116831938_StaticFields::get_offset_of_hexUpperChars_19(),
	Uri_t1116831938_StaticFields::get_offset_of_SchemeDelimiter_20(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeFile_21(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeFtp_22(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeGopher_23(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeHttp_24(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeHttps_25(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeMailto_26(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNews_27(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNntp_28(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNetPipe_29(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNetTcp_30(),
	Uri_t1116831938_StaticFields::get_offset_of_schemes_31(),
	Uri_t1116831938::get_offset_of_parser_32(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map12_33(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_34(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_35(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_36(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (UriScheme_t1290668975)+ sizeof (Il2CppObject), sizeof(UriScheme_t1290668975_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1239[3] = 
{
	UriScheme_t1290668975::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1290668975::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1290668975::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (UriFormatException_t308538560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (UriHostNameType_t959572879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1241[6] = 
{
	UriHostNameType_t959572879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (UriKind_t238866934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1242[4] = 
{
	UriKind_t238866934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (UriParser_t3685110593), -1, sizeof(UriParser_t3685110593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1243[6] = 
{
	UriParser_t3685110593_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t3685110593_StaticFields::get_offset_of_table_1(),
	UriParser_t3685110593::get_offset_of_scheme_name_2(),
	UriParser_t3685110593::get_offset_of_default_port_3(),
	UriParser_t3685110593_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t3685110593_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (UriPartial_t875461417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1244[5] = 
{
	UriPartial_t875461417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (UriTypeConverter_t2523083502), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (LocalCertificateSelectionCallback_t2431285719), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (RemoteCertificateValidationCallback_t1894914657), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (BindIPEndPoint_t3006124499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (HttpContinueDelegate_t1707598350), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (MatchEvaluator_t1719977010), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238935), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1251[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (U24ArrayTypeU2416_t3379220354)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220354_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (U24ArrayTypeU24128_t1676615741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t1676615741_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (U24ArrayTypeU2412_t3379220350)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220350_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (U3CModuleU3E_t86524793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (ExtensionAttribute_t2299149759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (Locale_t2281372285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (MonoTODOAttribute_t2091695243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (KeyBuilder_t373726642), -1, sizeof(KeyBuilder_t373726642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1259[1] = 
{
	KeyBuilder_t373726642_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (SymmetricTransform_t131863658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1260[12] = 
{
	SymmetricTransform_t131863658::get_offset_of_algo_0(),
	SymmetricTransform_t131863658::get_offset_of_encrypt_1(),
	SymmetricTransform_t131863658::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t131863658::get_offset_of_temp_3(),
	SymmetricTransform_t131863658::get_offset_of_temp2_4(),
	SymmetricTransform_t131863658::get_offset_of_workBuff_5(),
	SymmetricTransform_t131863658::get_offset_of_workout_6(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t131863658::get_offset_of_m_disposed_9(),
	SymmetricTransform_t131863658::get_offset_of_lastBlock_10(),
	SymmetricTransform_t131863658::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1261[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1262[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1263[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1264[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (Check_t10677726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (Enumerable_t839044124), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1267[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1268[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1269[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (Aes_t2466798581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (AesManaged_t23175804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (AesTransform_t1787635017), -1, sizeof(AesTransform_t1787635017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1272[14] = 
{
	AesTransform_t1787635017::get_offset_of_expandedKey_12(),
	AesTransform_t1787635017::get_offset_of_Nk_13(),
	AesTransform_t1787635017::get_offset_of_Nr_14(),
	AesTransform_t1787635017_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t1787635017_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T0_18(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T1_19(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T2_20(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T3_21(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (Action_t3771233898), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238936), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1275[12] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (U24ArrayTypeU24136_t1676615770)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1676615770_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (U24ArrayTypeU24120_t1676615733)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1676615733_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (U24ArrayTypeU24256_t1676616794)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616794_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (U24ArrayTypeU241024_t435478333)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t435478333_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (U3CModuleU3E_t86524794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (AssetBundleCreateRequest_t1416890373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (CompatibilityCheck_t561206607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1282[6] = 
{
	CompatibilityCheck_t561206607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (AssetBundleRequest_t2154290273), sizeof(AssetBundleRequest_t2154290273_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (AssetBundle_t2070959688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (SendMessageOptions_t3856946179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1285[3] = 
{
	SendMessageOptions_t3856946179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (PrimitiveType_t1035833655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1286[7] = 
{
	PrimitiveType_t1035833655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (Space_t4209342076)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1287[3] = 
{
	Space_t4209342076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (RuntimePlatform_t3050318497)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1288[32] = 
{
	RuntimePlatform_t3050318497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (LogType_t4286006228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1289[6] = 
{
	LogType_t4286006228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (SystemInfo_t3820892225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (WaitForSeconds_t1615819279), sizeof(WaitForSeconds_t1615819279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1291[1] = 
{
	WaitForSeconds_t1615819279::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (WaitForFixedUpdate_t2130080621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (WaitForEndOfFrame_t2372756133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (CustomYieldInstruction_t2666549910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (Coroutine_t3621161934), sizeof(Coroutine_t3621161934_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1295[1] = 
{
	Coroutine_t3621161934::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (ScriptableObject_t2970544072), sizeof(ScriptableObject_t2970544072_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (UnhandledExceptionHandler_t1700300692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (CursorLockMode_t1155278888)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1298[4] = 
{
	CursorLockMode_t1155278888::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (Cursor_t2745727898), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
