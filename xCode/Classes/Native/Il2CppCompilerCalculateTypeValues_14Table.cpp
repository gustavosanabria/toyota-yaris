﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView2392257822.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3510840818.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3604098244.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai2244784594.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd3393793052.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inter3068494210.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inters507319425.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier514511185.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit3375281836.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1344855248.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1652317979.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2782999289.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1315979427.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Gener247595084.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1328382603.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri1378537957.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_Physics3358180733.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_Physics2D9846735.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_BoxCollider2D2212926951.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_AudioSettings3774206607.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu1377657005.h"
#include "UnityEngine_UnityEngine_AudioType794660134.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback83861602.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa4244274966.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902.h"
#include "UnityEngine_UnityEngine_AnimationEventSource2152433973.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator1374492422.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_HumanLimit3300934482.h"
#include "UnityEngine_UnityEngine_HumanBone194476679.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3906681469.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1390812184.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Custo503624423.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1923005356.h"
#include "UnityEngine_UnityEngine_TextGenerator538854556.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2918974229.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode1147493927.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "UnityEngine_UnityEngine_CharacterInfo2481726445.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal4168056797.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases4247149838.h"
#include "UnityEngine_UnityEngine_CanvasGroup3702418109.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3950887807.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_EventModifiers4195406918.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_GUI3134605553.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState2687015188.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction2749288659.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUILayout3864601915.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility87000299.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (Random_t3156561159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (YieldInstruction_t2048002629), sizeof(YieldInstruction_t2048002629_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (PlayerPrefsException_t3680716996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (PlayerPrefs_t1845493509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (ADBannerView_t2392257822), -1, sizeof(ADBannerView_t2392257822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1404[4] = 
{
	ADBannerView_t2392257822::get_offset_of__bannerView_0(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasClicked_1(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasLoaded_2(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerFailedToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (BannerWasClickedDelegate_t3510840818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (BannerWasLoadedDelegate_t3604098244), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (BannerFailedToLoadDelegate_t2244784594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (ADInterstitialAd_t3393793052), -1, sizeof(ADInterstitialAd_t3393793052_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1408[3] = 
{
	ADInterstitialAd_t3393793052::get_offset_of_interstitialView_0(),
	ADInterstitialAd_t3393793052_StaticFields::get_offset_of_onInterstitialWasLoaded_1(),
	ADInterstitialAd_t3393793052_StaticFields::get_offset_of_onInterstitialWasViewed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (InterstitialWasLoadedDelegate_t3068494210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (InterstitialWasViewedDelegate_t507319425), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (CalendarIdentifier_t514511185)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1411[12] = 
{
	CalendarIdentifier_t514511185::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (CalendarUnit_t3375281836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1412[12] = 
{
	CalendarUnit_t3375281836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (LocalNotification_t1344855248), -1, sizeof(LocalNotification_t1344855248_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1413[2] = 
{
	LocalNotification_t1344855248::get_offset_of_notificationWrapper_0(),
	LocalNotification_t1344855248_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (RemoteNotification_t1652317979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1414[1] = 
{
	RemoteNotification_t1652317979::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (DirectorPlayer_t2782999289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (PlayState_t1315979427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1416[3] = 
{
	PlayState_t1315979427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (Playable_t70832698)+ sizeof (Il2CppObject), sizeof(Playable_t70832698_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1417[2] = 
{
	Playable_t70832698::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Playable_t70832698::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (GenericMixerPlayable_t247595084)+ sizeof (Il2CppObject), sizeof(GenericMixerPlayable_t247595084_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1418[1] = 
{
	GenericMixerPlayable_t247595084::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (Playables_t1328382603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (ScriptPlayable_t1378537957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (LoadSceneMode_t3067001883)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1421[3] = 
{
	LoadSceneMode_t3067001883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (SceneManager_t2940962239), -1, sizeof(SceneManager_t2940962239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1422[3] = 
{
	SceneManager_t2940962239_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t2940962239_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t2940962239_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (Scene_t1080795294)+ sizeof (Il2CppObject), sizeof(Scene_t1080795294_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1423[1] = 
{
	Scene_t1080795294::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (ControllerColliderHit_t2416790841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1424[7] = 
{
	ControllerColliderHit_t2416790841::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Point_2(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t2416790841::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t2416790841::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (Collision_t2494107688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1425[5] = 
{
	Collision_t2494107688::get_offset_of_m_Impulse_0(),
	Collision_t2494107688::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2494107688::get_offset_of_m_Rigidbody_2(),
	Collision_t2494107688::get_offset_of_m_Collider_3(),
	Collision_t2494107688::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (QueryTriggerInteraction_t577895768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1426[4] = 
{
	QueryTriggerInteraction_t577895768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (Physics_t3358180733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (ContactPoint_t243083348)+ sizeof (Il2CppObject), sizeof(ContactPoint_t243083348_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1428[5] = 
{
	ContactPoint_t243083348::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (Rigidbody_t3346577219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (Collider_t2939674232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (BoxCollider_t2538127765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (MeshCollider_t2667653125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (RaycastHit_t4003175726)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1433[6] = 
{
	RaycastHit_t4003175726::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (CharacterController_t1618060635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (Physics2D_t9846735), -1, sizeof(Physics2D_t9846735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1435[1] = 
{
	Physics2D_t9846735_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (RaycastHit2D_t1374744384)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1436[6] = 
{
	RaycastHit2D_t1374744384::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (Rigidbody2D_t1743771669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (Collider2D_t1552025098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (BoxCollider2D_t2212926951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (ContactPoint2D_t4288432358)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1440[4] = 
{
	ContactPoint2D_t4288432358::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_Collider_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_OtherCollider_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (Collision2D_t2859305914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1441[5] = 
{
	Collision2D_t2859305914::get_offset_of_m_Rigidbody_0(),
	Collision2D_t2859305914::get_offset_of_m_Collider_1(),
	Collision2D_t2859305914::get_offset_of_m_Contacts_2(),
	Collision2D_t2859305914::get_offset_of_m_RelativeVelocity_3(),
	Collision2D_t2859305914::get_offset_of_m_Enabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (AudioSettings_t3774206607), -1, sizeof(AudioSettings_t3774206607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1442[1] = 
{
	AudioSettings_t3774206607_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (AudioConfigurationChangeHandler_t1377657005), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (AudioType_t794660134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1444[14] = 
{
	AudioType_t794660134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (AudioClip_t794140988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1445[2] = 
{
	AudioClip_t794140988::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t794140988::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (PCMReaderCallback_t83861602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (PCMSetPositionCallback_t4244274966), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (AudioListener_t3685735200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (AudioSource_t1740077639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (WebCamDevice_t3274004757)+ sizeof (Il2CppObject), sizeof(WebCamDevice_t3274004757_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1450[2] = 
{
	WebCamDevice_t3274004757::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WebCamDevice_t3274004757::get_offset_of_m_Flags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (WebCamTexture_t1290350902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (AnimationEventSource_t2152433973)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1452[4] = 
{
	AnimationEventSource_t2152433973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (AnimationEvent_t3669457594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1453[11] = 
{
	AnimationEvent_t3669457594::get_offset_of_m_Time_0(),
	AnimationEvent_t3669457594::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t3669457594::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t3669457594::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t3669457594::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t3669457594::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t3669457594::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t3669457594::get_offset_of_m_Source_7(),
	AnimationEvent_t3669457594::get_offset_of_m_StateSender_8(),
	AnimationEvent_t3669457594::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t3669457594::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (PlayMode_t1155122555)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1454[3] = 
{
	PlayMode_t1155122555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (Animation_t1724966010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (Enumerator_t1374492422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1456[2] = 
{
	Enumerator_t1374492422::get_offset_of_m_Outer_0(),
	Enumerator_t1374492422::get_offset_of_m_CurrentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (AnimationState_t3682323633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (AnimatorClipInfo_t2746035113)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t2746035113_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1458[2] = 
{
	AnimatorClipInfo_t2746035113::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t2746035113::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (AnimatorStateInfo_t323110318)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t323110318_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1459[9] = 
{
	AnimatorStateInfo_t323110318::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (AnimatorTransitionInfo_t2817229998)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2817229998_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1460[6] = 
{
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (Animator_t2776330603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (SkeletonBone_t421858229)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t421858229_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1462[5] = 
{
	SkeletonBone_t421858229::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_transformModified_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (HumanLimit_t3300934482)+ sizeof (Il2CppObject), sizeof(HumanLimit_t3300934482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1463[5] = 
{
	HumanLimit_t3300934482::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (HumanBone_t194476679)+ sizeof (Il2CppObject), sizeof(HumanBone_t194476679_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1464[3] = 
{
	HumanBone_t194476679::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t194476679::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t194476679::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (RuntimeAnimatorController_t274649809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (AnimatorControllerPlayable_t3906681469)+ sizeof (Il2CppObject), sizeof(AnimatorControllerPlayable_t3906681469_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1466[1] = 
{
	AnimatorControllerPlayable_t3906681469::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (AnimationPlayable_t1390812184)+ sizeof (Il2CppObject), sizeof(AnimationPlayable_t1390812184_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1467[1] = 
{
	AnimationPlayable_t1390812184::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (CustomAnimationPlayable_t503624423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1468[1] = 
{
	CustomAnimationPlayable_t503624423::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (TextGenerationSettings_t1923005356)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1469[18] = 
{
	TextGenerationSettings_t1923005356::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (TextGenerator_t538854556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1470[11] = 
{
	TextGenerator_t538854556::get_offset_of_m_Ptr_0(),
	TextGenerator_t538854556::get_offset_of_m_LastString_1(),
	TextGenerator_t538854556::get_offset_of_m_LastSettings_2(),
	TextGenerator_t538854556::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t538854556::get_offset_of_m_LastValid_4(),
	TextGenerator_t538854556::get_offset_of_m_Verts_5(),
	TextGenerator_t538854556::get_offset_of_m_Characters_6(),
	TextGenerator_t538854556::get_offset_of_m_Lines_7(),
	TextGenerator_t538854556::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t538854556::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t538854556::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (TextAnchor_t213922566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1471[10] = 
{
	TextAnchor_t213922566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (HorizontalWrapMode_t2918974229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1472[3] = 
{
	HorizontalWrapMode_t2918974229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (VerticalWrapMode_t1147493927)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1473[3] = 
{
	VerticalWrapMode_t1147493927::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (GUIText_t3371372606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (TextMesh_t2567681854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (CharacterInfo_t2481726445)+ sizeof (Il2CppObject), sizeof(CharacterInfo_t2481726445_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1476[7] = 
{
	CharacterInfo_t2481726445::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t2481726445::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t2481726445::get_offset_of_vert_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t2481726445::get_offset_of_width_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t2481726445::get_offset_of_size_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t2481726445::get_offset_of_style_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t2481726445::get_offset_of_flipped_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (Font_t4241557075), -1, sizeof(Font_t4241557075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1477[2] = 
{
	Font_t4241557075_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4241557075::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (FontTextureRebuildCallback_t4168056797), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (UICharInfo_t65807484)+ sizeof (Il2CppObject), sizeof(UICharInfo_t65807484_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1479[2] = 
{
	UICharInfo_t65807484::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t65807484::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (UILineInfo_t4113875482)+ sizeof (Il2CppObject), sizeof(UILineInfo_t4113875482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1480[3] = 
{
	UILineInfo_t4113875482::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4113875482::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4113875482::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (UIVertex_t4244065212)+ sizeof (Il2CppObject), sizeof(UIVertex_t4244065212_marshaled_pinvoke), sizeof(UIVertex_t4244065212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1481[9] = 
{
	UIVertex_t4244065212::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_tangent_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212_StaticFields::get_offset_of_s_DefaultColor_6(),
	UIVertex_t4244065212_StaticFields::get_offset_of_s_DefaultTangent_7(),
	UIVertex_t4244065212_StaticFields::get_offset_of_simpleVert_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (RenderMode_t77252893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1482[4] = 
{
	RenderMode_t77252893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (Canvas_t2727140764), -1, sizeof(Canvas_t2727140764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1483[1] = 
{
	Canvas_t2727140764_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (WillRenderCanvases_t4247149838), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (CanvasGroup_t3702418109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (CanvasRenderer_t3950887807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (RectTransformUtility_t3025555048), -1, sizeof(RectTransformUtility_t3025555048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1488[1] = 
{
	RectTransformUtility_t3025555048_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (Event_t4196595728), sizeof(Event_t4196595728_marshaled_pinvoke), sizeof(Event_t4196595728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1489[4] = 
{
	Event_t4196595728::get_offset_of_m_Ptr_0(),
	Event_t4196595728_StaticFields::get_offset_of_s_Current_1(),
	Event_t4196595728_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t4196595728_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (KeyCode_t3128317986)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1490[322] = 
{
	KeyCode_t3128317986::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (EventType_t637886954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1491[31] = 
{
	EventType_t637886954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (EventModifiers_t4195406918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1492[9] = 
{
	EventModifiers_t4195406918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (ScaleMode_t3023293187)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1493[4] = 
{
	ScaleMode_t3023293187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (GUI_t3134605553), -1, sizeof(GUI_t3134605553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1494[12] = 
{
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t3134605553_StaticFields::get_offset_of_s_HotTextField_1(),
	GUI_t3134605553_StaticFields::get_offset_of_s_BoxHash_2(),
	GUI_t3134605553_StaticFields::get_offset_of_s_RepeatButtonHash_3(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ToggleHash_4(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ButtonGridHash_5(),
	GUI_t3134605553_StaticFields::get_offset_of_s_SliderHash_6(),
	GUI_t3134605553_StaticFields::get_offset_of_s_BeginGroupHash_7(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollviewHash_8(),
	GUI_t3134605553_StaticFields::get_offset_of_s_Skin_9(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollViewStates_10(),
	GUI_t3134605553_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (ScrollViewState_t2687015188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (WindowFunction_t2749288659), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (GUIContent_t2094828418), -1, sizeof(GUIContent_t2094828418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1497[7] = 
{
	GUIContent_t2094828418::get_offset_of_m_Text_0(),
	GUIContent_t2094828418::get_offset_of_m_Image_1(),
	GUIContent_t2094828418::get_offset_of_m_Tooltip_2(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t2094828418_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (GUILayout_t3864601915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (GUILayoutUtility_t87000299), -1, sizeof(GUILayoutUtility_t87000299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1499[5] = 
{
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
