﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall1277370263.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3502354656.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall2972625667.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup4062675100.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList3597236437.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime3698499994.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4222409188.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram3749225885.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1017505774.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt1844602099.h"
#include "UnityEngine_UnityEngine_Logger2997509588.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3E86524790.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySe2197853779.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearCal846303360.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearCa2094571968.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Backgroun2608219151.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearUse945107814.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearUs3502530982.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamReco785660528.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullHideEx644331599.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StencilHi1063643244.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_LegacyHid3116252689.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MonoCamer3733412120.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StereoCam2173582563.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullCamer2143544308.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCamer984266456.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTr3340678586.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTarg637796117.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi877546845.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev1437122227.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev3311506418.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev2052521376.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudReco1213605353.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru2270629190.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcess465046465.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExces3226096284.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable3777854735.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTar1698426226.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlay3307683808.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityP531631577.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeU2903303273.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru3107149249.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstruc147246886.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr3365905913.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear3657261146.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E1319805025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E3010462567.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearIm3622106266.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerra587327548.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2826579942.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr1152863025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbs142368528.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderT1139674014.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (InvokableCall_t1277370263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[1] = 
{
	InvokableCall_t1277370263::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (UnityEventCallState_t3502354656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1606[4] = 
{
	UnityEventCallState_t3502354656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (PersistentCall_t2972625667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[5] = 
{
	PersistentCall_t2972625667::get_offset_of_m_Target_0(),
	PersistentCall_t2972625667::get_offset_of_m_MethodName_1(),
	PersistentCall_t2972625667::get_offset_of_m_Mode_2(),
	PersistentCall_t2972625667::get_offset_of_m_Arguments_3(),
	PersistentCall_t2972625667::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (PersistentCallGroup_t4062675100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[1] = 
{
	PersistentCallGroup_t4062675100::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (InvokableCallList_t3597236437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[4] = 
{
	InvokableCallList_t3597236437::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t3597236437::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t3597236437::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t3597236437::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (UnityEventBase_t1020378628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[4] = 
{
	UnityEventBase_t1020378628::get_offset_of_m_Calls_0(),
	UnityEventBase_t1020378628::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t1020378628::get_offset_of_m_TypeName_2(),
	UnityEventBase_t1020378628::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (UnityEvent_t1266085011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[1] = 
{
	UnityEvent_t1266085011::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (WaitForSecondsRealtime_t3698499994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[1] = 
{
	WaitForSecondsRealtime_t3698499994::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (AnimationPlayableUtilities_t4222409188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (FrameData_t3749225885)+ sizeof (Il2CppObject), sizeof(FrameData_t3749225885_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1618[4] = 
{
	FrameData_t3749225885::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (DefaultValueAttribute_t1017505774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[1] = 
{
	DefaultValueAttribute_t1017505774::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (ExcludeFromDocsAttribute_t1844602099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (Logger_t2997509588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1623[3] = 
{
	Logger_t2997509588::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t2997509588::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t2997509588::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (UsedByNativeCodeAttribute_t3197444790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (RequiredByNativeCodeAttribute_t3165457172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (FormerlySerializedAsAttribute_t2216353654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[1] = 
{
	FormerlySerializedAsAttribute_t2216353654::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (TypeInferenceRules_t2889237774)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1627[5] = 
{
	TypeInferenceRules_t2889237774::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (TypeInferenceRuleAttribute_t1657757719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	TypeInferenceRuleAttribute_t1657757719::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (GenericStack_t931085639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (NetFxCoreExtensions_t2541795172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (UnityAction_t594794173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (U3CModuleU3E_t86524795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (FactorySetter_t2197853779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (EyewearCalibrationProfileManager_t846303360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (EyewearCalibrationProfileManagerImpl_t2094571968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (BackgroundPlaneAbstractBehaviour_t2608219151), -1, sizeof(BackgroundPlaneAbstractBehaviour_t2608219151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1642[13] = 
{
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mVideoBgConfigChanged_5(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mQBehaviour_7(),
	BackgroundPlaneAbstractBehaviour_t2608219151_StaticFields::get_offset_of_maxDisplacement_8(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_defaultNumDivisions_9(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mMesh_10(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mStereoDepth_11(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mBackgroundOffset_12(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mVuforiaBehaviour_13(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (EyewearUserCalibrator_t945107814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (EyewearUserCalibratorImpl_t3502530982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (IOSCamRecoveringHelper_t785660528), -1, sizeof(IOSCamRecoveringHelper_t785660528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1649[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (NullHideExcessAreaClipping_t644331599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (StencilHideExcessAreaClipping_t1063643244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[13] = 
{
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mSceneIsScaledDown_4(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mCameraNearPlane_5(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t1063643244::get_offset_of_mBgPlaneLocalScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (LegacyHideExcessAreaClipping_t3116252689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[22] = 
{
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mSceneIsScaledDown_8(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mBgPlaneLocalPos_9(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mBgPlaneLocalScale_10(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mCameraNearPlane_11(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mCameraPixelRect_12(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mCameraFieldOFView_13(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mVuforiaBehaviour_14(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mHideBehaviours_15(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mDeactivatedHideBehaviours_16(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mPlanesActivated_17(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mLeftPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mRightPlaneCachedScale_19(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mBottomPlaneCachedScale_20(),
	LegacyHideExcessAreaClipping_t3116252689::get_offset_of_mTopPlaneCachedScale_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (MonoCameraConfiguration_t3733412120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[12] = 
{
	MonoCameraConfiguration_t3733412120::get_offset_of_mPrimaryCamera_0(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mCameraDeviceMode_1(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mLastVideoBackGroundMirroredFromSDK_2(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mOnVideoBackgroundConfigChanged_3(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mVideoBackgroundBehaviours_4(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mViewportRect_5(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mRenderVideoBackground_6(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mCameraWidthFactor_7(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mViewPortWidth_8(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mViewPortHeight_9(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mProjectionOrientation_10(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mInitialReflection_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (StereoCameraConfiguration_t2173582563), -1, sizeof(StereoCameraConfiguration_t2173582563_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1655[17] = 
{
	0,
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MIN_CENTER_13(),
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MAX_CENTER_14(),
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MAX_BOTTOM_15(),
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MAX_TOP_16(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mSecondaryCamera_17(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mSkewFrustum_18(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNeedToCheckStereo_19(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mLastAppliedNearClipPlane_20(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mLastAppliedFarClipPlane_21(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mLastAppliedVirtualFoV_22(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNewNearClipPlane_23(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNewFarClipPlane_24(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNewVirtualFoV_25(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mCameraOffset_26(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mIntegratedWithOtherSDK_27(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mEyewearUserCalibrationProfileId_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (NullCameraConfiguration_t2143544308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	NullCameraConfiguration_t2143544308::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (UnityCameraExtensions_t984266456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (TrackableBehaviour_t4179556250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[7] = 
{
	TrackableBehaviour_t4179556250::get_offset_of_mTrackableName_2(),
	TrackableBehaviour_t4179556250::get_offset_of_mPreviousScale_3(),
	TrackableBehaviour_t4179556250::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t4179556250::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t4179556250::get_offset_of_mStatus_6(),
	TrackableBehaviour_t4179556250::get_offset_of_mTrackable_7(),
	TrackableBehaviour_t4179556250::get_offset_of_mTrackableEventHandlers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (Status_t835151357)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1662[7] = 
{
	Status_t835151357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (DataSetTrackableBehaviour_t3340678586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[11] = 
{
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mDataSetPath_9(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mExtendedTracking_10(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mInitializeSmartTerrain_11(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mReconstructionToInitialize_12(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderBoundsMin_13(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderBoundsMax_14(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mIsSmartTerrainOccluderOffset_15(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderOffset_16(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderRotation_17(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderLockedInPlace_18(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mAutoSetOccluderFromTargetSize_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (ObjectTargetAbstractBehaviour_t637796117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[7] = 
{
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mPreviewImage_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (CameraDevice_t877546845), -1, sizeof(CameraDevice_t877546845_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	CameraDevice_t877546845_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (CameraDeviceMode_t134001414)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1667[4] = 
{
	CameraDeviceMode_t134001414::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (FocusMode_t1437122227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1668[6] = 
{
	FocusMode_t1437122227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (CameraDirection_t3311506418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[4] = 
{
	CameraDirection_t3311506418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (VideoModeData_t2052521376)+ sizeof (Il2CppObject), sizeof(VideoModeData_t2052521376_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1670[4] = 
{
	VideoModeData_t2052521376::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2052521376::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2052521376::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2052521376::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (CloudRecoAbstractBehaviour_t1213605353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[11] = 
{
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_SecretKey_10(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_ScanlineColor_11(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_FeaturePointColor_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (ReconstructionImpl_t2270629190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[5] = 
{
	ReconstructionImpl_t2270629190::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t2270629190::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t2270629190::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t2270629190::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t2270629190::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (HideExcessAreaAbstractBehaviour_t465046465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[5] = 
{
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_matteShader_2(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_clippingMode_3(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mClippingImpl_4(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mVuforiaBehaviour_5(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mPlaneOffset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (CLIPPING_MODE_t3226096284)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1675[4] = 
{
	CLIPPING_MODE_t3226096284::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (TrackableImpl_t3777854735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[2] = 
{
	TrackableImpl_t3777854735::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3777854735::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (ObjectTargetImpl_t1698426226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[2] = 
{
	ObjectTargetImpl_t1698426226::get_offset_of_mSize_2(),
	ObjectTargetImpl_t1698426226::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (UnityPlayer_t3307683808), -1, sizeof(UnityPlayer_t3307683808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1678[1] = 
{
	UnityPlayer_t3307683808_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (NullUnityPlayer_t531631577), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (PlayModeUnityPlayer_t2903303273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (ReconstructionFromTargetImpl_t3107149249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[6] = 
{
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (ReconstructionFromTargetAbstractBehaviour_t147246886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[2] = 
{
	ReconstructionFromTargetAbstractBehaviour_t147246886::get_offset_of_mReconstructionFromTarget_2(),
	ReconstructionFromTargetAbstractBehaviour_t147246886::get_offset_of_mReconstructionBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (SmartTerrainBuilder_t3365905913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (Eyewear_t3657261146), -1, sizeof(Eyewear_t3657261146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1687[3] = 
{
	Eyewear_t3657261146_StaticFields::get_offset_of_mInstance_0(),
	Eyewear_t3657261146::get_offset_of_mProfileManager_1(),
	Eyewear_t3657261146::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (EyeID_t1319805025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1688[4] = 
{
	EyeID_t1319805025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (EyewearCalibrationReading_t3010462567)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t3010462567_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1689[5] = 
{
	EyewearCalibrationReading_t3010462567::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (EyewearImpl_t3622106266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (SmartTerrainInitializationInfo_t587327548)+ sizeof (Il2CppObject), sizeof(SmartTerrainInitializationInfo_t587327548_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (SmartTerrainTrackableBehaviour_t2826579942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[4] = 
{
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mSmartTerrainTrackable_9(),
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mDisableAutomaticUpdates_10(),
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mMeshFilterToUpdate_11(),
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mMeshColliderToUpdate_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (SmartTerrainTrackerAbstractBehaviour_t1152863025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[7] = 
{
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mAutoInitTracker_2(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mAutoStartTracker_3(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mAutoInitBuilder_4(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mSceneUnitsToMillimeter_5(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mTrackerStarted_6(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mTrackerWasActiveBeforePause_7(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mTrackerWasActiveBeforeDisabling_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (SurfaceAbstractBehaviour_t142368528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[1] = 
{
	SurfaceAbstractBehaviour_t142368528::get_offset_of_mSurface_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (CylinderTargetAbstractBehaviour_t1139674014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[6] = 
{
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mCylinderTarget_20(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mTopDiameterRatio_21(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mBottomDiameterRatio_22(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mFrameIndex_23(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mUpdateFrameIndex_24(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mFutureScale_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (DataSet_t2095838082), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
