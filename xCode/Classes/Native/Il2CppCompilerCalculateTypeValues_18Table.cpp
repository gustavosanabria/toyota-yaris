﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNa3177738953.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNu1083833836.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNa4290029276.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaWr2202637939.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAlive2256617941.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru1860057025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstr1293468098.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateMana3262709086.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateMana2944561054.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin1331156121.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFind138063285.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3822354588.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin2468213607.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3609410114.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin1985818969.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3256513230.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin2230003487.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableS179597514.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable3166588170.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextureRe3929117224.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMa2355365335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMana55743383.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBut704206407.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1019632289.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1005202439.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImp1072092957.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro3136319864.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro1961690790.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro3794639002.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2347335889.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbst865486027.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAb3409682331.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTarg3565607731.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn1557074901.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn1795183225.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn2677974245.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb1091759131.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb2257996192.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMac337691955.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRu1069467038.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRu2616862271.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUti743372383.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAb137142681.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTar3516126639.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAb1299549867.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefin1531824314.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBack2475465524.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoText4076028962.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1191238304.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbst725705546.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstra545947899.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilter661135463.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImpleme3208964498.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImpleme3342667558.h"
#include "System_Xml_U3CModuleU3E86524790.h"
#include "System_Xml_System_MonoTODOAttribute2091695241.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet1710562669.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType730532811.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType1663577061.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic230829008.h"
#include "System_Xml_Mono_Xml_Schema_XsdString3497764096.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString3069561175.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2036792492.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage2730590407.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken280424909.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens613775752.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1434515194.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName3308285615.h"
#include "System_Xml_Mono_Xml_Schema_XsdID744268010.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef2025328715.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs3166186186.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity3091474642.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities2242065712.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation1627605361.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal696098180.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1111886705.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong1434469099.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt2108011298.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort2035664687.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1434180983.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger4252412075.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong1954151808.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2817511917.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort965959482.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte1953863692.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger346877240.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger4018722407.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger580566908.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat2023777551.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble3063791808.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary3891592351.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (VuforiaNativeIosWrapper_t3177738953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (VuforiaNullWrapper_t1083833836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (VuforiaNativeWrapper_t4290029276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (VuforiaWrapper_t2202637939), -1, sizeof(VuforiaWrapper_t2202637939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	VuforiaWrapper_t2202637939_StaticFields::get_offset_of_sWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (KeepAliveAbstractBehaviour_t2256617941), -1, sizeof(KeepAliveAbstractBehaviour_t2256617941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1808[8] = 
{
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepARCameraAlive_2(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepTrackableBehavioursAlive_3(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepTextRecoBehaviourAlive_4(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepUDTBuildingBehaviourAlive_5(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepCloudRecoBehaviourAlive_6(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepSmartTerrainAlive_7(),
	KeepAliveAbstractBehaviour_t2256617941_StaticFields::get_offset_of_sKeepAliveBehaviour_8(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (ReconstructionAbstractBehaviour_t1860057025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[22] = 
{
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (PropAbstractBehaviour_t1293468098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[2] = 
{
	PropAbstractBehaviour_t1293468098::get_offset_of_mProp_13(),
	PropAbstractBehaviour_t1293468098::get_offset_of_mBoxColliderToUpdate_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (StateManager_t3262709086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (StateManagerImpl_t2944561054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[6] = 
{
	StateManagerImpl_t2944561054::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t2944561054::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t2944561054::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t2944561054::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t2944561054::get_offset_of_mWordManager_4(),
	StateManagerImpl_t2944561054::get_offset_of_mCameraPositioningHelper_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (TargetFinder_t1331156121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (InitState_t138063285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[6] = 
{
	InitState_t138063285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (UpdateState_t3822354588)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[12] = 
{
	UpdateState_t3822354588::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (FilterMode_t2468213607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[3] = 
{
	FilterMode_t2468213607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (TargetSearchResult_t3609410114)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t3609410114_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[6] = 
{
	TargetSearchResult_t3609410114::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (TargetFinderImpl_t1985818969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	TargetFinderImpl_t1985818969::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t1985818969::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t1985818969::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t1985818969::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (TargetFinderState_t3256513230)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t3256513230_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	TargetFinderState_t3256513230::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3256513230::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3256513230::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3256513230::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (InternalTargetSearchResult_t2230003487)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t2230003487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[6] = 
{
	InternalTargetSearchResult_t2230003487::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (TrackableSource_t179597514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (TrackableSourceImpl_t3166588170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[1] = 
{
	TrackableSourceImpl_t3166588170::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (TextureRenderer_t3929117224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[3] = 
{
	TextureRenderer_t3929117224::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3929117224::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3929117224::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (TrackerManager_t2355365335), -1, sizeof(TrackerManager_t2355365335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[1] = 
{
	TrackerManager_t2355365335_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (TrackerManagerImpl_t55743383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[5] = 
{
	TrackerManagerImpl_t55743383::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t55743383::get_offset_of_mMarkerTracker_2(),
	TrackerManagerImpl_t55743383::get_offset_of_mTextTracker_3(),
	TrackerManagerImpl_t55743383::get_offset_of_mSmartTerrainTracker_4(),
	TrackerManagerImpl_t55743383::get_offset_of_mStateManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (VirtualButton_t704206407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (Sensitivity_t1019632289)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[4] = 
{
	Sensitivity_t1019632289::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (VirtualButtonImpl_t1005202439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[6] = 
{
	VirtualButtonImpl_t1005202439::get_offset_of_mName_1(),
	VirtualButtonImpl_t1005202439::get_offset_of_mID_2(),
	VirtualButtonImpl_t1005202439::get_offset_of_mArea_3(),
	VirtualButtonImpl_t1005202439::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t1005202439::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t1005202439::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (WebCamImpl_t1072092957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[15] = 
{
	WebCamImpl_t1072092957::get_offset_of_mARCameras_0(),
	WebCamImpl_t1072092957::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t1072092957::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t1072092957::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t1072092957::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t1072092957::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t1072092957::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t1072092957::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t1072092957::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t1072092957::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t1072092957::get_offset_of_mIsDirty_10(),
	WebCamImpl_t1072092957::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t1072092957::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t1072092957::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t1072092957::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (WebCamProfile_t3136319864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	WebCamProfile_t3136319864::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (ProfileData_t1961690790)+ sizeof (Il2CppObject), sizeof(ProfileData_t1961690790_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1831[3] = 
{
	ProfileData_t1961690790::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1961690790::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1961690790::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (ProfileCollection_t3794639002)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	ProfileCollection_t3794639002::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3794639002::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (ImageTargetAbstractBehaviour_t2347335889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mImageTarget_22(),
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mVirtualButtonBehaviours_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (MarkerAbstractBehaviour_t865486027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[2] = 
{
	MarkerAbstractBehaviour_t865486027::get_offset_of_mMarkerID_9(),
	MarkerAbstractBehaviour_t865486027::get_offset_of_mMarker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (MaskOutAbstractBehaviour_t3409682331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[1] = 
{
	MaskOutAbstractBehaviour_t3409682331::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (MultiTargetAbstractBehaviour_t3565607731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[1] = 
{
	MultiTargetAbstractBehaviour_t3565607731::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (VuforiaUnity_t1557074901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (InitError_t432217960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1841[12] = 
{
	InitError_t432217960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (VuforiaHint_t1795183225)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[4] = 
{
	VuforiaHint_t1795183225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (StorageType_t2677974245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1843[4] = 
{
	StorageType_t2677974245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (VuforiaAbstractBehaviour_t1091759131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[50] = 
{
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_VuforiaLicenseKey_2(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mCameraOffset_3(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSkewFrustum_4(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_CameraDeviceModeSetting_5(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_MaxSimultaneousImageTargets_6(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_MaxSimultaneousObjectTargets_7(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_UseDelayedLoadingObjectTargets_8(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_CameraDirection_9(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_MirrorVideoBackground_10(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mWorldCenterMode_11(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mWorldCenter_12(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mCentralAnchorPoint_13(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mParentAnchorPoint_14(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mPrimaryCamera_15(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mPrimaryCameraOriginalRect_16(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSecondaryCamera_17(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSecondaryCameraOriginalRect_18(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSecondaryCameraDisabledLocally_19(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mUsingHeadset_20(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mHeadsetID_21(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mEyewearUserCalibrationProfileId_22(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSynchronizePoseUpdates_23(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mTrackerEventHandlers_24(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mVideoBgEventHandlers_25(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnVuforiaInitError_26(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnVuforiaInitialized_27(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnVuforiaStarted_28(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnTrackablesUpdated_29(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mRenderOnUpdate_30(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnPause_31(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnBackgroundTextureChanged_32(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mStartHasBeenInvoked_33(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mHasStarted_34(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mFailedToInitialize_35(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mBackgroundTextureHasChanged_36(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mInitError_37(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mCameraConfiguration_38(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mClearMaterial_39(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mHasStartedOnce_40(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mWasEnabledBeforePause_41(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mObjectTrackerWasActiveBeforePause_42(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_43(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mMarkerTrackerWasActiveBeforePause_44(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mMarkerTrackerWasActiveBeforeDisabling_45(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mLastUpdatedFrame_46(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mTrackersRequestedToDeinit_47(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mMissedToApplyLeftProjectionMatrix_48(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mMissedToApplyRightProjectionMatrix_49(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mLeftProjectMatrixToApply_50(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mRightProjectMatrixToApply_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (WorldCenterMode_t2257996192)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	WorldCenterMode_t2257996192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (VuforiaMacros_t337691955)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t337691955_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (VuforiaRuntimeUtilities_t1069467038), -1, sizeof(VuforiaRuntimeUtilities_t1069467038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	VuforiaRuntimeUtilities_t1069467038_StaticFields::get_offset_of_sWebCamUsed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (WebCamUsed_t2616862271)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1848[4] = 
{
	WebCamUsed_t2616862271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (SurfaceUtilities_t743372383), -1, sizeof(SurfaceUtilities_t743372383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[1] = 
{
	SurfaceUtilities_t743372383_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (TextRecoAbstractBehaviour_t137142681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[12] = 
{
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (SimpleTargetData_t3516126639)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3516126639_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	SimpleTargetData_t3516126639::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3516126639::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (TurnOffAbstractBehaviour_t1299549867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t1531824314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (VideoBackgroundAbstractBehaviour_t2475465524), -1, sizeof(VideoBackgroundAbstractBehaviour_t2475465524_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1854[9] = 
{
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mVuforiaAbstractBehaviour_4(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t2475465524_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t2475465524_StaticFields::get_offset_of_mPostRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mDisabledMeshRenderers_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (VideoTextureRendererAbstractBehaviour_t4076028962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[3] = 
{
	VideoTextureRendererAbstractBehaviour_t4076028962::get_offset_of_mTexture_2(),
	VideoTextureRendererAbstractBehaviour_t4076028962::get_offset_of_mVideoBgConfigChanged_3(),
	VideoTextureRendererAbstractBehaviour_t4076028962::get_offset_of_mNativeTextureID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (VirtualButtonAbstractBehaviour_t1191238304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[14] = 
{
	0,
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPreviouslyEnabled_9(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPressed_10(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mHandlers_11(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mLeftTop_12(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mRightBottom_13(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mUnregisterOnDestroy_14(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mVirtualButton_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (WebCamAbstractBehaviour_t725705546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[7] = 
{
	WebCamAbstractBehaviour_t725705546::get_offset_of_RenderTextureLayer_2(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mPlayModeRenderVideo_3(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mDeviceNameSetInEditor_4(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mFlipHorizontally_5(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mTurnOffWebCam_6(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mWebCamImpl_7(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mBackgroundCameraInstance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (WordAbstractBehaviour_t545947899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[3] = 
{
	WordAbstractBehaviour_t545947899::get_offset_of_mMode_9(),
	WordAbstractBehaviour_t545947899::get_offset_of_mSpecificWord_10(),
	WordAbstractBehaviour_t545947899::get_offset_of_mWord_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (WordFilterMode_t661135463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[4] = 
{
	WordFilterMode_t661135463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (U3CPrivateImplementationDetailsU3EU7B3518E5ACU2DF4ACU2D49D1U2DB865U2DF4A6B711AD02U7D_t3208964498), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B3518E5ACU2DF4ACU2D49D1U2DB865U2DF4A6B711AD02U7D_t3208964498_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1860[1] = 
{
	U3CPrivateImplementationDetailsU3EU7B3518E5ACU2DF4ACU2D49D1U2DB865U2DF4A6B711AD02U7D_t3208964498_StaticFields::get_offset_of_U24U24method0x600091cU2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3342667558)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3342667558_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (MonoTODOAttribute_t2091695244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (XsdWhitespaceFacet_t1710562669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[4] = 
{
	XsdWhitespaceFacet_t1710562669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (XsdAnySimpleType_t730532811), -1, sizeof(XsdAnySimpleType_t730532811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1866[6] = 
{
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (XdtAnyAtomicType_t1663577061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (XdtUntypedAtomic_t230829008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (XsdString_t3497764096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (XsdNormalizedString_t3069561175), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (XsdToken_t2036792492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (XsdLanguage_t2730590407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (XsdNMToken_t280424909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (XsdNMTokens_t613775752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (XsdName_t1434515194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (XsdNCName_t3308285615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (XsdID_t744268010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (XsdIDRef_t2025328715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (XsdIDRefs_t3166186186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (XsdEntity_t3091474642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (XsdEntities_t2242065712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (XsdNotation_t1627605361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (XsdDecimal_t696098180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (XsdInteger_t1111886705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (XsdLong_t1434469099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (XsdInt_t2108011298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (XsdShort_t2035664687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (XsdByte_t1434180983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (XsdNonNegativeInteger_t4252412075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (XsdUnsignedLong_t1954151808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (XsdUnsignedInt_t2817511917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (XsdUnsignedShort_t965959482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (XsdUnsignedByte_t1953863692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (XsdPositiveInteger_t346877240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (XsdNonPositiveInteger_t4018722407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (XsdNegativeInteger_t580566908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (XsdFloat_t2023777551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (XsdDouble_t3063791808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (XsdBase64Binary_t3891592351), -1, sizeof(XsdBase64Binary_t3891592351_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1899[2] = 
{
	XsdBase64Binary_t3891592351_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t3891592351_StaticFields::get_offset_of_decodeTable_62(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
