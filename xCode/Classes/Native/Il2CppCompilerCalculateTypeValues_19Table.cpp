﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary4085742767.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName2033029455.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean3513513563.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2977086671.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration2352167683.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration621607249.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration36650571.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1841962250.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate1434217501.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime1434701628.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1947427211.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay297523254.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear2024125431.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth3118112584.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1434278436.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated4226823396.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute2904598248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS1577913490.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype196391954.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMet998439334.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement3664762632.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet2982631811.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3197530270.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo4089849692.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject3280570797.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3890186420.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet4125473774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType3060492794.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1889388569.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi1048666168.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe2016622060.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUn3206834991.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType4090188264.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil4090213040.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity1280069984.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAt1769351841.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2146545409.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut927884470.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2315070405.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerNa142497835.h"
#include "System_Xml_System_Xml_ConformanceLevel233510445.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory809616844.h"
#include "System_Xml_Mono_Xml_DTDObjectModel3593115196.h"
#include "System_Xml_Mono_Xml_DictionaryBase2849977869.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat2174423114.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase1997767749.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollection918003794.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollectio2994484303.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection3329514151.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollection87515240.h"
#include "System_Xml_Mono_Xml_DTDContentModel647030886.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection194646180.h"
#include "System_Xml_Mono_Xml_DTDNode2039770680.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration41916820.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3691706913.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration1204852177.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2319669258.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration3913042473.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration2992004394.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationCo541155938.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration149008292.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType4011965541.h"
#include "System_Xml_Mono_Xml_DTDOccurence81554249.h"
#include "System_Xml_System_Xml_DTDReader4151552713.h"
#include "System_Xml_System_Xml_EntityHandling2742017190.h"
#include "System_Xml_System_Xml_NameTable2699772693.h"
#include "System_Xml_System_Xml_NameTable_Entry2866414864.h"
#include "System_Xml_System_Xml_NamespaceHandling1516402802.h"
#include "System_Xml_System_Xml_NewLineHandling53740107.h"
#include "System_Xml_System_Xml_ReadState352099245.h"
#include "System_Xml_System_Xml_WhitespaceHandling2567612992.h"
#include "System_Xml_System_Xml_WriteState1937244592.h"
#include "System_Xml_System_Xml_XmlAttribute6647939.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3012627841.h"
#include "System_Xml_System_Xml_XmlCDataSection68050113.h"
#include "System_Xml_System_Xml_XmlChar856576415.h"
#include "System_Xml_System_Xml_XmlCharacterData915341530.h"
#include "System_Xml_System_Xml_XmlComment2893623302.h"
#include "System_Xml_System_Xml_XmlConvert2894815066.h"
#include "System_Xml_System_Xml_XmlDeclaration1240444833.h"
#include "System_Xml_System_Xml_XmlDocument730752740.h"
#include "System_Xml_System_Xml_XmlDocumentFragment1144967508.h"
#include "System_Xml_System_Xml_XmlDocumentType838630462.h"
#include "System_Xml_System_Xml_XmlElement280387747.h"
#include "System_Xml_System_Xml_XmlEntity3759900140.h"
#include "System_Xml_System_Xml_XmlEntityReference2288721231.h"
#include "System_Xml_System_Xml_XmlException1475188278.h"
#include "System_Xml_System_Xml_XmlImplementation3716119739.h"
#include "System_Xml_System_Xml_XmlStreamReader837919148.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader2673413431.h"
#include "System_Xml_System_Xml_XmlInputStream2962968081.h"
#include "System_Xml_System_Xml_XmlLinkedNode901819716.h"
#include "System_Xml_System_Xml_XmlNameEntry1203257998.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3943949348.h"
#include "System_Xml_System_Xml_XmlNameTable1216706026.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (XsdHexBinary_t4085742767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (XsdQName_t2033029455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (XsdBoolean_t3513513563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (XsdAnyURI_t2977086671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (XsdDuration_t2352167683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (XdtDayTimeDuration_t621607249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (XdtYearMonthDuration_t36650571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (XsdDateTime_t1841962250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (XsdDate_t1434217501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (XsdTime_t1434701628), -1, sizeof(XsdTime_t1434701628_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1909[1] = 
{
	XsdTime_t1434701628_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (XsdGYearMonth_t1947427211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (XsdGMonthDay_t297523254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (XsdGYear_t2024125431), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (XsdGMonth_t3118112584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (XsdGDay_t1434278436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (XmlSchemaAnnotated_t4226823396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (XmlSchemaAttribute_t2904598248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (XmlSchemaCompilationSettings_t1577913490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[1] = 
{
	XmlSchemaCompilationSettings_t1577913490::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (XmlSchemaDatatype_t196391954), -1, sizeof(XmlSchemaDatatype_t196391954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1919[55] = 
{
	XmlSchemaDatatype_t196391954::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t196391954::get_offset_of_sb_2(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (XmlSchemaDerivationMethod_t998439334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[9] = 
{
	XmlSchemaDerivationMethod_t998439334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (XmlSchemaElement_t3664762632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (XmlSchemaFacet_t2982631811), -1, sizeof(XmlSchemaFacet_t2982631811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1922[1] = 
{
	XmlSchemaFacet_t2982631811_StaticFields::get_offset_of_AllFacets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (Facet_t3197530270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[14] = 
{
	Facet_t3197530270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (XmlSchemaInfo_t4089849692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[7] = 
{
	XmlSchemaInfo_t4089849692::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t4089849692::get_offset_of_isNil_1(),
	XmlSchemaInfo_t4089849692::get_offset_of_memberType_2(),
	XmlSchemaInfo_t4089849692::get_offset_of_attr_3(),
	XmlSchemaInfo_t4089849692::get_offset_of_elem_4(),
	XmlSchemaInfo_t4089849692::get_offset_of_type_5(),
	XmlSchemaInfo_t4089849692::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (XmlSchemaObject_t3280570797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[3] = 
{
	XmlSchemaObject_t3280570797::get_offset_of_namespaces_0(),
	XmlSchemaObject_t3280570797::get_offset_of_unhandledAttributeList_1(),
	XmlSchemaObject_t3280570797::get_offset_of_CompilationId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (XmlSchemaParticle_t3890186420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (XmlSchemaSet_t4125473774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[5] = 
{
	XmlSchemaSet_t4125473774::get_offset_of_nameTable_0(),
	XmlSchemaSet_t4125473774::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t4125473774::get_offset_of_schemas_2(),
	XmlSchemaSet_t4125473774::get_offset_of_settings_3(),
	XmlSchemaSet_t4125473774::get_offset_of_CompilationId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (XmlSchemaSimpleType_t3060492794), -1, sizeof(XmlSchemaSimpleType_t3060492794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1928[53] = 
{
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_schemaLocationType_9(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_content_10(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_islocal_11(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_variety_12(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsAnySimpleType_13(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsString_14(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsBoolean_15(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDecimal_16(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsFloat_17(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDouble_18(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDuration_19(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDateTime_20(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsTime_21(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDate_22(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGYearMonth_23(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGYear_24(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGMonthDay_25(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGDay_26(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGMonth_27(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsHexBinary_28(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsBase64Binary_29(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsAnyUri_30(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsQName_31(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNotation_32(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNormalizedString_33(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsToken_34(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsLanguage_35(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNMToken_36(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNMTokens_37(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsName_38(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNCName_39(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsID_40(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsIDRef_41(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsIDRefs_42(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsEntity_43(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsEntities_44(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsInteger_45(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNonPositiveInteger_46(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNegativeInteger_47(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsLong_48(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsInt_49(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsShort_50(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsByte_51(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNonNegativeInteger_52(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedLong_53(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedInt_54(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedShort_55(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedByte_56(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsPositiveInteger_57(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtUntypedAtomic_58(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtAnyAtomicType_59(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtYearMonthDuration_60(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtDayTimeDuration_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (XmlSchemaSimpleTypeContent_t1889388569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (XmlSchemaSimpleTypeList_t1048666168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[2] = 
{
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_itemType_3(),
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (XmlSchemaSimpleTypeRestriction_t2016622060), -1, sizeof(XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1931[2] = 
{
	XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields::get_offset_of_lengthStyle_3(),
	XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields::get_offset_of_listFacets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (XmlSchemaSimpleTypeUnion_t3206834991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (XmlSchemaType_t4090188264), -1, sizeof(XmlSchemaType_t4090188264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1933[6] = 
{
	XmlSchemaType_t4090188264::get_offset_of_final_3(),
	XmlSchemaType_t4090188264::get_offset_of_BaseXmlSchemaTypeInternal_4(),
	XmlSchemaType_t4090188264::get_offset_of_DatatypeInternal_5(),
	XmlSchemaType_t4090188264::get_offset_of_QNameInternal_6(),
	XmlSchemaType_t4090188264_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_7(),
	XmlSchemaType_t4090188264_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (XmlSchemaUtil_t4090213040), -1, sizeof(XmlSchemaUtil_t4090213040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1934[4] = 
{
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_StrictMsCompliant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (XmlSchemaValidity_t1280069984)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1935[4] = 
{
	XmlSchemaValidity_t1280069984::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (XmlAttributeAttribute_t1769351841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[1] = 
{
	XmlAttributeAttribute_t1769351841::get_offset_of_attributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (XmlElementAttribute_t2146545409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[3] = 
{
	XmlElementAttribute_t2146545409::get_offset_of_elementName_0(),
	XmlElementAttribute_t2146545409::get_offset_of_type_1(),
	XmlElementAttribute_t2146545409::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (XmlEnumAttribute_t927884470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[1] = 
{
	XmlEnumAttribute_t927884470::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (XmlIgnoreAttribute_t2315070405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (XmlSerializerNamespaces_t142497835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[1] = 
{
	XmlSerializerNamespaces_t142497835::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (ConformanceLevel_t233510445)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1942[4] = 
{
	ConformanceLevel_t233510445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (DTDAutomataFactory_t809616844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[3] = 
{
	DTDAutomataFactory_t809616844::get_offset_of_root_0(),
	DTDAutomataFactory_t809616844::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t809616844::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (DTDObjectModel_t3593115196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[19] = 
{
	DTDObjectModel_t3593115196::get_offset_of_factory_0(),
	DTDObjectModel_t3593115196::get_offset_of_elementDecls_1(),
	DTDObjectModel_t3593115196::get_offset_of_attListDecls_2(),
	DTDObjectModel_t3593115196::get_offset_of_peDecls_3(),
	DTDObjectModel_t3593115196::get_offset_of_entityDecls_4(),
	DTDObjectModel_t3593115196::get_offset_of_notationDecls_5(),
	DTDObjectModel_t3593115196::get_offset_of_validationErrors_6(),
	DTDObjectModel_t3593115196::get_offset_of_resolver_7(),
	DTDObjectModel_t3593115196::get_offset_of_nameTable_8(),
	DTDObjectModel_t3593115196::get_offset_of_externalResources_9(),
	DTDObjectModel_t3593115196::get_offset_of_baseURI_10(),
	DTDObjectModel_t3593115196::get_offset_of_name_11(),
	DTDObjectModel_t3593115196::get_offset_of_publicId_12(),
	DTDObjectModel_t3593115196::get_offset_of_systemId_13(),
	DTDObjectModel_t3593115196::get_offset_of_intSubset_14(),
	DTDObjectModel_t3593115196::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t3593115196::get_offset_of_isStandalone_16(),
	DTDObjectModel_t3593115196::get_offset_of_lineNumber_17(),
	DTDObjectModel_t3593115196::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (DictionaryBase_t2849977869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (U3CU3Ec__Iterator3_t2174423114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[5] = 
{
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (DTDCollectionBase_t1997767749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[1] = 
{
	DTDCollectionBase_t1997767749::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (DTDElementDeclarationCollection_t918003794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (DTDAttListDeclarationCollection_t2994484303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (DTDEntityDeclarationCollection_t3329514151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (DTDNotationDeclarationCollection_t87515240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (DTDContentModel_t647030886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[6] = 
{
	DTDContentModel_t647030886::get_offset_of_root_5(),
	DTDContentModel_t647030886::get_offset_of_ownerElementName_6(),
	DTDContentModel_t647030886::get_offset_of_elementName_7(),
	DTDContentModel_t647030886::get_offset_of_orderType_8(),
	DTDContentModel_t647030886::get_offset_of_childModels_9(),
	DTDContentModel_t647030886::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (DTDContentModelCollection_t194646180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[1] = 
{
	DTDContentModelCollection_t194646180::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (DTDNode_t2039770680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[5] = 
{
	DTDNode_t2039770680::get_offset_of_root_0(),
	DTDNode_t2039770680::get_offset_of_isInternalSubset_1(),
	DTDNode_t2039770680::get_offset_of_baseURI_2(),
	DTDNode_t2039770680::get_offset_of_lineNumber_3(),
	DTDNode_t2039770680::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (DTDElementDeclaration_t41916820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[6] = 
{
	DTDElementDeclaration_t41916820::get_offset_of_root_5(),
	DTDElementDeclaration_t41916820::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t41916820::get_offset_of_name_7(),
	DTDElementDeclaration_t41916820::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t41916820::get_offset_of_isAny_9(),
	DTDElementDeclaration_t41916820::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (DTDAttributeDefinition_t3691706913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[4] = 
{
	DTDAttributeDefinition_t3691706913::get_offset_of_name_5(),
	DTDAttributeDefinition_t3691706913::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3691706913::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t3691706913::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (DTDAttListDeclaration_t1204852177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[3] = 
{
	DTDAttListDeclaration_t1204852177::get_offset_of_name_5(),
	DTDAttListDeclaration_t1204852177::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t1204852177::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (DTDEntityBase_t2319669258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[10] = 
{
	DTDEntityBase_t2319669258::get_offset_of_name_5(),
	DTDEntityBase_t2319669258::get_offset_of_publicId_6(),
	DTDEntityBase_t2319669258::get_offset_of_systemId_7(),
	DTDEntityBase_t2319669258::get_offset_of_literalValue_8(),
	DTDEntityBase_t2319669258::get_offset_of_replacementText_9(),
	DTDEntityBase_t2319669258::get_offset_of_uriString_10(),
	DTDEntityBase_t2319669258::get_offset_of_absUri_11(),
	DTDEntityBase_t2319669258::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2319669258::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2319669258::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (DTDEntityDeclaration_t3913042473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[6] = 
{
	DTDEntityDeclaration_t3913042473::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t3913042473::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t3913042473::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t3913042473::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t3913042473::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t3913042473::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (DTDNotationDeclaration_t2992004394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[5] = 
{
	DTDNotationDeclaration_t2992004394::get_offset_of_name_5(),
	DTDNotationDeclaration_t2992004394::get_offset_of_localName_6(),
	DTDNotationDeclaration_t2992004394::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t2992004394::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t2992004394::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (DTDParameterEntityDeclarationCollection_t541155938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[2] = 
{
	DTDParameterEntityDeclarationCollection_t541155938::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t541155938::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (DTDParameterEntityDeclaration_t149008292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (DTDContentOrderType_t4011965541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1963[4] = 
{
	DTDContentOrderType_t4011965541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (DTDOccurence_t81554249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[5] = 
{
	DTDOccurence_t81554249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (DTDReader_t4151552713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[14] = 
{
	DTDReader_t4151552713::get_offset_of_currentInput_0(),
	DTDReader_t4151552713::get_offset_of_parserInputStack_1(),
	DTDReader_t4151552713::get_offset_of_nameBuffer_2(),
	DTDReader_t4151552713::get_offset_of_nameLength_3(),
	DTDReader_t4151552713::get_offset_of_nameCapacity_4(),
	DTDReader_t4151552713::get_offset_of_valueBuffer_5(),
	DTDReader_t4151552713::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t4151552713::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t4151552713::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t4151552713::get_offset_of_normalization_9(),
	DTDReader_t4151552713::get_offset_of_processingInternalSubset_10(),
	DTDReader_t4151552713::get_offset_of_cachedPublicId_11(),
	DTDReader_t4151552713::get_offset_of_cachedSystemId_12(),
	DTDReader_t4151552713::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (EntityHandling_t2742017190)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1966[3] = 
{
	EntityHandling_t2742017190::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (NameTable_t2699772693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[3] = 
{
	NameTable_t2699772693::get_offset_of_count_0(),
	NameTable_t2699772693::get_offset_of_buckets_1(),
	NameTable_t2699772693::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (Entry_t2866414864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	Entry_t2866414864::get_offset_of_str_0(),
	Entry_t2866414864::get_offset_of_hash_1(),
	Entry_t2866414864::get_offset_of_len_2(),
	Entry_t2866414864::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (NamespaceHandling_t1516402802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[3] = 
{
	NamespaceHandling_t1516402802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (NewLineHandling_t53740107)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[4] = 
{
	NewLineHandling_t53740107::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (ReadState_t352099245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1974[6] = 
{
	ReadState_t352099245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (WhitespaceHandling_t2567612992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[4] = 
{
	WhitespaceHandling_t2567612992::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (WriteState_t1937244592)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[8] = 
{
	WriteState_t1937244592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (XmlAttribute_t6647939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[4] = 
{
	XmlAttribute_t6647939::get_offset_of_name_5(),
	XmlAttribute_t6647939::get_offset_of_isDefault_6(),
	XmlAttribute_t6647939::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t6647939::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (XmlAttributeCollection_t3012627841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[2] = 
{
	XmlAttributeCollection_t3012627841::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3012627841::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (XmlCDataSection_t68050113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (XmlChar_t856576415), -1, sizeof(XmlChar_t856576415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1980[5] = 
{
	XmlChar_t856576415_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t856576415_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t856576415_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t856576415_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t856576415_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (XmlCharacterData_t915341530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[1] = 
{
	XmlCharacterData_t915341530::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (XmlComment_t2893623302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (XmlConvert_t2894815066), -1, sizeof(XmlConvert_t2894815066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1983[7] = 
{
	XmlConvert_t2894815066_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t2894815066_StaticFields::get_offset_of__defaultStyle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (XmlDeclaration_t1240444833), -1, sizeof(XmlDeclaration_t1240444833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1984[4] = 
{
	XmlDeclaration_t1240444833::get_offset_of_encoding_6(),
	XmlDeclaration_t1240444833::get_offset_of_standalone_7(),
	XmlDeclaration_t1240444833::get_offset_of_version_8(),
	XmlDeclaration_t1240444833_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (XmlDocument_t730752740), -1, sizeof(XmlDocument_t730752740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1985[20] = 
{
	XmlDocument_t730752740_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t730752740::get_offset_of_optimal_create_element_6(),
	XmlDocument_t730752740::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t730752740::get_offset_of_nameTable_8(),
	XmlDocument_t730752740::get_offset_of_baseURI_9(),
	XmlDocument_t730752740::get_offset_of_implementation_10(),
	XmlDocument_t730752740::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t730752740::get_offset_of_resolver_12(),
	XmlDocument_t730752740::get_offset_of_idTable_13(),
	XmlDocument_t730752740::get_offset_of_nameCache_14(),
	XmlDocument_t730752740::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t730752740::get_offset_of_schemas_16(),
	XmlDocument_t730752740::get_offset_of_schemaInfo_17(),
	XmlDocument_t730752740::get_offset_of_loadMode_18(),
	XmlDocument_t730752740::get_offset_of_NodeChanged_19(),
	XmlDocument_t730752740::get_offset_of_NodeChanging_20(),
	XmlDocument_t730752740::get_offset_of_NodeInserted_21(),
	XmlDocument_t730752740::get_offset_of_NodeInserting_22(),
	XmlDocument_t730752740::get_offset_of_NodeRemoved_23(),
	XmlDocument_t730752740::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (XmlDocumentFragment_t1144967508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[1] = 
{
	XmlDocumentFragment_t1144967508::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (XmlDocumentType_t838630462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[3] = 
{
	XmlDocumentType_t838630462::get_offset_of_entities_6(),
	XmlDocumentType_t838630462::get_offset_of_notations_7(),
	XmlDocumentType_t838630462::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (XmlElement_t280387747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[5] = 
{
	XmlElement_t280387747::get_offset_of_attributes_6(),
	XmlElement_t280387747::get_offset_of_name_7(),
	XmlElement_t280387747::get_offset_of_lastLinkedChild_8(),
	XmlElement_t280387747::get_offset_of_isNotEmpty_9(),
	XmlElement_t280387747::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (XmlEntity_t3759900140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[7] = 
{
	XmlEntity_t3759900140::get_offset_of_name_5(),
	XmlEntity_t3759900140::get_offset_of_NDATA_6(),
	XmlEntity_t3759900140::get_offset_of_publicId_7(),
	XmlEntity_t3759900140::get_offset_of_systemId_8(),
	XmlEntity_t3759900140::get_offset_of_baseUri_9(),
	XmlEntity_t3759900140::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t3759900140::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (XmlEntityReference_t2288721231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[2] = 
{
	XmlEntityReference_t2288721231::get_offset_of_entityName_6(),
	XmlEntityReference_t2288721231::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (XmlException_t1475188278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[5] = 
{
	XmlException_t1475188278::get_offset_of_lineNumber_11(),
	XmlException_t1475188278::get_offset_of_linePosition_12(),
	XmlException_t1475188278::get_offset_of_sourceUri_13(),
	XmlException_t1475188278::get_offset_of_res_14(),
	XmlException_t1475188278::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (XmlImplementation_t3716119739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[1] = 
{
	XmlImplementation_t3716119739::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (XmlStreamReader_t837919148), -1, sizeof(XmlStreamReader_t837919148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[2] = 
{
	XmlStreamReader_t837919148::get_offset_of_input_12(),
	XmlStreamReader_t837919148_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (NonBlockingStreamReader_t2673413431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[11] = 
{
	NonBlockingStreamReader_t2673413431::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t2673413431::get_offset_of_pos_4(),
	NonBlockingStreamReader_t2673413431::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t2673413431::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t2673413431::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t2673413431::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t2673413431::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t2673413431::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (XmlInputStream_t2962968081), -1, sizeof(XmlInputStream_t2962968081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1995[7] = 
{
	XmlInputStream_t2962968081_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2962968081::get_offset_of_enc_2(),
	XmlInputStream_t2962968081::get_offset_of_stream_3(),
	XmlInputStream_t2962968081::get_offset_of_buffer_4(),
	XmlInputStream_t2962968081::get_offset_of_bufLength_5(),
	XmlInputStream_t2962968081::get_offset_of_bufPos_6(),
	XmlInputStream_t2962968081_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (XmlLinkedNode_t901819716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[1] = 
{
	XmlLinkedNode_t901819716::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (XmlNameEntry_t1203257998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[5] = 
{
	XmlNameEntry_t1203257998::get_offset_of_Prefix_0(),
	XmlNameEntry_t1203257998::get_offset_of_LocalName_1(),
	XmlNameEntry_t1203257998::get_offset_of_NS_2(),
	XmlNameEntry_t1203257998::get_offset_of_Hash_3(),
	XmlNameEntry_t1203257998::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (XmlNameEntryCache_t3943949348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[4] = 
{
	XmlNameEntryCache_t3943949348::get_offset_of_table_0(),
	XmlNameEntryCache_t3943949348::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3943949348::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3943949348::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (XmlNameTable_t1216706026), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
