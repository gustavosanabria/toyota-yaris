﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap977482698.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3658211563.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope1749213747.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1675009921.h"
#include "System_Xml_System_Xml_XmlNodeArrayList543782172.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction2010186255.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs1377283246.h"
#include "System_Xml_System_Xml_XmlNodeList991860617.h"
#include "System_Xml_System_Xml_XmlNodeListChildren3993110696.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerat3708952179.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "System_Xml_System_Xml_XmlNotation1447424459.h"
#include "System_Xml_System_Xml_XmlParserContext1291067127.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem3307232354.h"
#include "System_Xml_System_Xml_XmlParserInput3438354706.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInput173147476.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction2161892066.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502.h"
#include "System_Xml_System_Xml_XmlReader4123196108.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport870027186.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma3744455723.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG4120438118.h"
#include "System_Xml_System_Xml_XmlReaderSettings4229224207.h"
#include "System_Xml_System_Xml_XmlResolver3822670287.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace707263863.h"
#include "System_Xml_System_Xml_XmlSpace557686381.h"
#include "System_Xml_System_Xml_XmlText857080694.h"
#include "System_Xml_Mono_Xml2_XmlTextReader1609187425.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo597808448.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeToke982414386.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2016006645.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState2319200395.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt1440432317.h"
#include "System_Xml_System_Xml_XmlTextReader1367920089.h"
#include "System_Xml_System_Xml_XmlTextWriter1523325321.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo1808742809.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil614327009.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState205203294.h"
#include "System_Xml_System_Xml_XmlTokenizedType3753548874.h"
#include "System_Xml_System_Xml_XmlUrlResolver343097532.h"
#include "System_Xml_System_Xml_XmlWhitespace4130926598.h"
#include "System_Xml_System_Xml_XmlWriter4278601340.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler3074502249.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3379220348.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3988332413.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1676616792.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar435480436.h"
#include "UnityEngine_UI_U3CModuleU3E86524790.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandl1938870832.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge672607302.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger95600550.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge849715470.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3843052064.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2704060668.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect2840182460.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2104732159.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou2511441271.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1322796528.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEven384979233.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2820857440.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputMod15847059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3771003169.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2039702646.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3613479957.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp4082623702.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1096194655.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1573608962.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputM894675487.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2327671059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRa935388869.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRay1170055767.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT723277650.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (XmlNamedNodeMap_t977482698), -1, sizeof(XmlNamedNodeMap_t977482698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2000[4] = 
{
	XmlNamedNodeMap_t977482698_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t977482698::get_offset_of_parent_1(),
	XmlNamedNodeMap_t977482698::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t977482698::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (XmlNamespaceManager_t1467853467), -1, sizeof(XmlNamespaceManager_t1467853467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2001[9] = 
{
	XmlNamespaceManager_t1467853467::get_offset_of_decls_0(),
	XmlNamespaceManager_t1467853467::get_offset_of_declPos_1(),
	XmlNamespaceManager_t1467853467::get_offset_of_scopes_2(),
	XmlNamespaceManager_t1467853467::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t1467853467::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t1467853467::get_offset_of_count_5(),
	XmlNamespaceManager_t1467853467::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t1467853467::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t1467853467_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (NsDecl_t3658211563)+ sizeof (Il2CppObject), sizeof(NsDecl_t3658211563_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2002[2] = 
{
	NsDecl_t3658211563::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3658211563::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (NsScope_t1749213747)+ sizeof (Il2CppObject), sizeof(NsScope_t1749213747_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[2] = 
{
	NsScope_t1749213747::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t1749213747::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (XmlNode_t856910923), -1, sizeof(XmlNode_t856910923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2004[5] = 
{
	XmlNode_t856910923_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t856910923::get_offset_of_ownerDocument_1(),
	XmlNode_t856910923::get_offset_of_parentNode_2(),
	XmlNode_t856910923::get_offset_of_childNodes_3(),
	XmlNode_t856910923_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (EmptyNodeList_t1675009921), -1, sizeof(EmptyNodeList_t1675009921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2005[1] = 
{
	EmptyNodeList_t1675009921_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (XmlNodeArrayList_t543782172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[1] = 
{
	XmlNodeArrayList_t543782172::get_offset_of__rgNodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (XmlNodeChangedAction_t2010186255)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2007[4] = 
{
	XmlNodeChangedAction_t2010186255::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (XmlNodeChangedEventArgs_t1377283246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[6] = 
{
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (XmlNodeList_t991860617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (XmlNodeListChildren_t3993110696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	XmlNodeListChildren_t3993110696::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (Enumerator_t3708952179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[3] = 
{
	Enumerator_t3708952179::get_offset_of_parent_0(),
	Enumerator_t3708952179::get_offset_of_currentChild_1(),
	Enumerator_t3708952179::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (XmlNodeType_t992114213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[19] = 
{
	XmlNodeType_t992114213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (XmlNotation_t1447424459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[4] = 
{
	XmlNotation_t1447424459::get_offset_of_localName_5(),
	XmlNotation_t1447424459::get_offset_of_publicId_6(),
	XmlNotation_t1447424459::get_offset_of_systemId_7(),
	XmlNotation_t1447424459::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (XmlParserContext_t1291067127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[13] = 
{
	XmlParserContext_t1291067127::get_offset_of_baseURI_0(),
	XmlParserContext_t1291067127::get_offset_of_docTypeName_1(),
	XmlParserContext_t1291067127::get_offset_of_encoding_2(),
	XmlParserContext_t1291067127::get_offset_of_internalSubset_3(),
	XmlParserContext_t1291067127::get_offset_of_namespaceManager_4(),
	XmlParserContext_t1291067127::get_offset_of_nameTable_5(),
	XmlParserContext_t1291067127::get_offset_of_publicID_6(),
	XmlParserContext_t1291067127::get_offset_of_systemID_7(),
	XmlParserContext_t1291067127::get_offset_of_xmlLang_8(),
	XmlParserContext_t1291067127::get_offset_of_xmlSpace_9(),
	XmlParserContext_t1291067127::get_offset_of_contextItems_10(),
	XmlParserContext_t1291067127::get_offset_of_contextItemCount_11(),
	XmlParserContext_t1291067127::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (ContextItem_t3307232354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[3] = 
{
	ContextItem_t3307232354::get_offset_of_BaseURI_0(),
	ContextItem_t3307232354::get_offset_of_XmlLang_1(),
	ContextItem_t3307232354::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (XmlParserInput_t3438354706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[5] = 
{
	XmlParserInput_t3438354706::get_offset_of_sourceStack_0(),
	XmlParserInput_t3438354706::get_offset_of_source_1(),
	XmlParserInput_t3438354706::get_offset_of_has_peek_2(),
	XmlParserInput_t3438354706::get_offset_of_peek_char_3(),
	XmlParserInput_t3438354706::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (XmlParserInputSource_t173147476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[6] = 
{
	XmlParserInputSource_t173147476::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t173147476::get_offset_of_reader_1(),
	XmlParserInputSource_t173147476::get_offset_of_state_2(),
	XmlParserInputSource_t173147476::get_offset_of_isPE_3(),
	XmlParserInputSource_t173147476::get_offset_of_line_4(),
	XmlParserInputSource_t173147476::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (XmlProcessingInstruction_t2161892066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[2] = 
{
	XmlProcessingInstruction_t2161892066::get_offset_of_target_6(),
	XmlProcessingInstruction_t2161892066::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (XmlQualifiedName_t2133315502), -1, sizeof(XmlQualifiedName_t2133315502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2019[4] = 
{
	XmlQualifiedName_t2133315502_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t2133315502::get_offset_of_name_1(),
	XmlQualifiedName_t2133315502::get_offset_of_ns_2(),
	XmlQualifiedName_t2133315502::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (XmlReader_t4123196108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[2] = 
{
	XmlReader_t4123196108::get_offset_of_binary_0(),
	XmlReader_t4123196108::get_offset_of_settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (XmlReaderBinarySupport_t870027186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[5] = 
{
	XmlReaderBinarySupport_t870027186::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t870027186::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t870027186::get_offset_of_state_2(),
	XmlReaderBinarySupport_t870027186::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t870027186::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (CommandState_t3744455723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2022[6] = 
{
	CommandState_t3744455723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (CharGetter_t4120438118), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (XmlReaderSettings_t4229224207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[4] = 
{
	XmlReaderSettings_t4229224207::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t4229224207::get_offset_of_conformance_1(),
	XmlReaderSettings_t4229224207::get_offset_of_schemas_2(),
	XmlReaderSettings_t4229224207::get_offset_of_schemasNeedsInitialization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (XmlResolver_t3822670287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (XmlSignificantWhitespace_t707263863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (XmlSpace_t557686381)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2027[4] = 
{
	XmlSpace_t557686381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (XmlText_t857080694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (XmlTextReader_t1609187425), -1, sizeof(XmlTextReader_t1609187425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2029[54] = 
{
	XmlTextReader_t1609187425::get_offset_of_cursorToken_2(),
	XmlTextReader_t1609187425::get_offset_of_currentToken_3(),
	XmlTextReader_t1609187425::get_offset_of_currentAttributeToken_4(),
	XmlTextReader_t1609187425::get_offset_of_currentAttributeValueToken_5(),
	XmlTextReader_t1609187425::get_offset_of_attributeTokens_6(),
	XmlTextReader_t1609187425::get_offset_of_attributeValueTokens_7(),
	XmlTextReader_t1609187425::get_offset_of_currentAttribute_8(),
	XmlTextReader_t1609187425::get_offset_of_currentAttributeValue_9(),
	XmlTextReader_t1609187425::get_offset_of_attributeCount_10(),
	XmlTextReader_t1609187425::get_offset_of_parserContext_11(),
	XmlTextReader_t1609187425::get_offset_of_nameTable_12(),
	XmlTextReader_t1609187425::get_offset_of_nsmgr_13(),
	XmlTextReader_t1609187425::get_offset_of_readState_14(),
	XmlTextReader_t1609187425::get_offset_of_disallowReset_15(),
	XmlTextReader_t1609187425::get_offset_of_depth_16(),
	XmlTextReader_t1609187425::get_offset_of_elementDepth_17(),
	XmlTextReader_t1609187425::get_offset_of_depthUp_18(),
	XmlTextReader_t1609187425::get_offset_of_popScope_19(),
	XmlTextReader_t1609187425::get_offset_of_elementNames_20(),
	XmlTextReader_t1609187425::get_offset_of_elementNameStackPos_21(),
	XmlTextReader_t1609187425::get_offset_of_allowMultipleRoot_22(),
	XmlTextReader_t1609187425::get_offset_of_isStandalone_23(),
	XmlTextReader_t1609187425::get_offset_of_returnEntityReference_24(),
	XmlTextReader_t1609187425::get_offset_of_entityReferenceName_25(),
	XmlTextReader_t1609187425::get_offset_of_valueBuffer_26(),
	XmlTextReader_t1609187425::get_offset_of_reader_27(),
	XmlTextReader_t1609187425::get_offset_of_peekChars_28(),
	XmlTextReader_t1609187425::get_offset_of_peekCharsIndex_29(),
	XmlTextReader_t1609187425::get_offset_of_peekCharsLength_30(),
	XmlTextReader_t1609187425::get_offset_of_curNodePeekIndex_31(),
	XmlTextReader_t1609187425::get_offset_of_preserveCurrentTag_32(),
	XmlTextReader_t1609187425::get_offset_of_line_33(),
	XmlTextReader_t1609187425::get_offset_of_column_34(),
	XmlTextReader_t1609187425::get_offset_of_currentLinkedNodeLineNumber_35(),
	XmlTextReader_t1609187425::get_offset_of_currentLinkedNodeLinePosition_36(),
	XmlTextReader_t1609187425::get_offset_of_useProceedingLineInfo_37(),
	XmlTextReader_t1609187425::get_offset_of_startNodeType_38(),
	XmlTextReader_t1609187425::get_offset_of_currentState_39(),
	XmlTextReader_t1609187425::get_offset_of_nestLevel_40(),
	XmlTextReader_t1609187425::get_offset_of_readCharsInProgress_41(),
	XmlTextReader_t1609187425::get_offset_of_binaryCharGetter_42(),
	XmlTextReader_t1609187425::get_offset_of_namespaces_43(),
	XmlTextReader_t1609187425::get_offset_of_whitespaceHandling_44(),
	XmlTextReader_t1609187425::get_offset_of_resolver_45(),
	XmlTextReader_t1609187425::get_offset_of_normalization_46(),
	XmlTextReader_t1609187425::get_offset_of_checkCharacters_47(),
	XmlTextReader_t1609187425::get_offset_of_prohibitDtd_48(),
	XmlTextReader_t1609187425::get_offset_of_closeInput_49(),
	XmlTextReader_t1609187425::get_offset_of_entityHandling_50(),
	XmlTextReader_t1609187425::get_offset_of_whitespacePool_51(),
	XmlTextReader_t1609187425::get_offset_of_whitespaceCache_52(),
	XmlTextReader_t1609187425::get_offset_of_stateStack_53(),
	XmlTextReader_t1609187425_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_54(),
	XmlTextReader_t1609187425_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (XmlTokenInfo_t597808448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[13] = 
{
	XmlTokenInfo_t597808448::get_offset_of_valueCache_0(),
	XmlTokenInfo_t597808448::get_offset_of_Reader_1(),
	XmlTokenInfo_t597808448::get_offset_of_Name_2(),
	XmlTokenInfo_t597808448::get_offset_of_LocalName_3(),
	XmlTokenInfo_t597808448::get_offset_of_Prefix_4(),
	XmlTokenInfo_t597808448::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t597808448::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t597808448::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t597808448::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t597808448::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t597808448::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t597808448::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t597808448::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (XmlAttributeTokenInfo_t982414386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[4] = 
{
	XmlAttributeTokenInfo_t982414386::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t982414386::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t982414386::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t982414386::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (TagName_t2016006645)+ sizeof (Il2CppObject), sizeof(TagName_t2016006645_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2032[3] = 
{
	TagName_t2016006645::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2016006645::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2016006645::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (DtdInputState_t2319200395)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[10] = 
{
	DtdInputState_t2319200395::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (DtdInputStateStack_t1440432317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[1] = 
{
	DtdInputStateStack_t1440432317::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (XmlTextReader_t1367920089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[5] = 
{
	XmlTextReader_t1367920089::get_offset_of_entity_2(),
	XmlTextReader_t1367920089::get_offset_of_source_3(),
	XmlTextReader_t1367920089::get_offset_of_entityInsideAttribute_4(),
	XmlTextReader_t1367920089::get_offset_of_insideAttribute_5(),
	XmlTextReader_t1367920089::get_offset_of_entityNameStack_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (XmlTextWriter_t1523325321), -1, sizeof(XmlTextWriter_t1523325321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[35] = 
{
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t1523325321::get_offset_of_base_stream_3(),
	XmlTextWriter_t1523325321::get_offset_of_source_4(),
	XmlTextWriter_t1523325321::get_offset_of_writer_5(),
	XmlTextWriter_t1523325321::get_offset_of_preserver_6(),
	XmlTextWriter_t1523325321::get_offset_of_preserved_name_7(),
	XmlTextWriter_t1523325321::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t1523325321::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t1523325321::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t1523325321::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t1523325321::get_offset_of_namespaces_12(),
	XmlTextWriter_t1523325321::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t1523325321::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t1523325321::get_offset_of_newline_handling_15(),
	XmlTextWriter_t1523325321::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t1523325321::get_offset_of_state_17(),
	XmlTextWriter_t1523325321::get_offset_of_node_state_18(),
	XmlTextWriter_t1523325321::get_offset_of_nsmanager_19(),
	XmlTextWriter_t1523325321::get_offset_of_open_count_20(),
	XmlTextWriter_t1523325321::get_offset_of_elements_21(),
	XmlTextWriter_t1523325321::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t1523325321::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t1523325321::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t1523325321::get_offset_of_indent_25(),
	XmlTextWriter_t1523325321::get_offset_of_indent_count_26(),
	XmlTextWriter_t1523325321::get_offset_of_indent_char_27(),
	XmlTextWriter_t1523325321::get_offset_of_indent_string_28(),
	XmlTextWriter_t1523325321::get_offset_of_newline_29(),
	XmlTextWriter_t1523325321::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t1523325321::get_offset_of_quote_char_31(),
	XmlTextWriter_t1523325321::get_offset_of_v2_32(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_33(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (XmlNodeInfo_t1808742809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[7] = 
{
	XmlNodeInfo_t1808742809::get_offset_of_Prefix_0(),
	XmlNodeInfo_t1808742809::get_offset_of_LocalName_1(),
	XmlNodeInfo_t1808742809::get_offset_of_NS_2(),
	XmlNodeInfo_t1808742809::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t1808742809::get_offset_of_HasElements_4(),
	XmlNodeInfo_t1808742809::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t1808742809::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (StringUtil_t614327009), -1, sizeof(StringUtil_t614327009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2038[2] = 
{
	StringUtil_t614327009_StaticFields::get_offset_of_cul_0(),
	StringUtil_t614327009_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (XmlDeclState_t205203294)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[5] = 
{
	XmlDeclState_t205203294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (XmlTokenizedType_t3753548874)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[14] = 
{
	XmlTokenizedType_t3753548874::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (XmlUrlResolver_t343097532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	XmlUrlResolver_t343097532::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (XmlWhitespace_t4130926598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (XmlWriter_t4278601340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (XmlNodeChangedEventHandler_t3074502249), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2045[7] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D26_1(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D27_2(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D28_3(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D29_4(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D43_5(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D44_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U24ArrayTypeU248_t3988332414)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3988332414_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (U24ArrayTypeU24256_t1676616795)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616795_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (U24ArrayTypeU241280_t435480436)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t435480436_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (U3CModuleU3E_t86524797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (EventHandle_t1938870832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[3] = 
{
	EventHandle_t1938870832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (EventSystem_t2276120119), -1, sizeof(EventSystem_t2276120119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2070[10] = 
{
	EventSystem_t2276120119::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t2276120119::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t2276120119::get_offset_of_m_FirstSelected_4(),
	EventSystem_t2276120119::get_offset_of_m_sendNavigationEvents_5(),
	EventSystem_t2276120119::get_offset_of_m_DragThreshold_6(),
	EventSystem_t2276120119::get_offset_of_m_CurrentSelected_7(),
	EventSystem_t2276120119::get_offset_of_m_SelectionGuard_8(),
	EventSystem_t2276120119::get_offset_of_m_DummyData_9(),
	EventSystem_t2276120119_StaticFields::get_offset_of_s_RaycastComparer_10(),
	EventSystem_t2276120119_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (EventTrigger_t672607302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[2] = 
{
	EventTrigger_t672607302::get_offset_of_m_Delegates_2(),
	EventTrigger_t672607302::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (TriggerEvent_t95600550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (Entry_t849715470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[2] = 
{
	Entry_t849715470::get_offset_of_eventID_0(),
	Entry_t849715470::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (EventTriggerType_t3843052064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2074[18] = 
{
	EventTriggerType_t3843052064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (ExecuteEvents_t2704060668), -1, sizeof(ExecuteEvents_t2704060668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2075[20] = 
{
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (MoveDirection_t2840182460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2077[6] = 
{
	MoveDirection_t2840182460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (RaycasterManager_t2104732159), -1, sizeof(RaycasterManager_t2104732159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2078[1] = 
{
	RaycasterManager_t2104732159_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (RaycastResult_t3762661364)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[10] = 
{
	RaycastResult_t3762661364::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (UIBehaviour_t2511441271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (AxisEventData_t3355659985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[2] = 
{
	AxisEventData_t3355659985::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t3355659985::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (AbstractEventData_t1322796528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[1] = 
{
	AbstractEventData_t1322796528::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (BaseEventData_t2054899105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[1] = 
{
	BaseEventData_t2054899105::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (PointerEventData_t1848751023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[21] = 
{
	PointerEventData_t1848751023::get_offset_of_m_PointerPress_2(),
	PointerEventData_t1848751023::get_offset_of_hovered_3(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerEnterU3Ek__BackingField_4(),
	PointerEventData_t1848751023::get_offset_of_U3ClastPressU3Ek__BackingField_5(),
	PointerEventData_t1848751023::get_offset_of_U3CrawPointerPressU3Ek__BackingField_6(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerDragU3Ek__BackingField_7(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9(),
	PointerEventData_t1848751023::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1848751023::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1848751023::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1848751023::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1848751023::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1848751023::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1848751023::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1848751023::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1848751023::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1848751023::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1848751023::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1848751023::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (InputButton_t384979233)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[4] = 
{
	InputButton_t384979233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (FramePressState_t2820857440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2086[5] = 
{
	FramePressState_t2820857440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (BaseInputModule_t15847059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[4] = 
{
	BaseInputModule_t15847059::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t15847059::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t15847059::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t15847059::get_offset_of_m_BaseEventData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (PointerInputModule_t3771003169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3771003169::get_offset_of_m_PointerData_10(),
	PointerInputModule_t3771003169::get_offset_of_m_MouseState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (ButtonState_t2039702646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[2] = 
{
	ButtonState_t2039702646::get_offset_of_m_Button_0(),
	ButtonState_t2039702646::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (MouseState_t3613479957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[1] = 
{
	MouseState_t3613479957::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (MouseButtonEventData_t4082623702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[2] = 
{
	MouseButtonEventData_t4082623702::get_offset_of_buttonState_0(),
	MouseButtonEventData_t4082623702::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (StandaloneInputModule_t1096194655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[12] = 
{
	StandaloneInputModule_t1096194655::get_offset_of_m_PrevActionTime_12(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMoveVector_13(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ConsecutiveMoveCount_14(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMousePosition_15(),
	StandaloneInputModule_t1096194655::get_offset_of_m_MousePosition_16(),
	StandaloneInputModule_t1096194655::get_offset_of_m_HorizontalAxis_17(),
	StandaloneInputModule_t1096194655::get_offset_of_m_VerticalAxis_18(),
	StandaloneInputModule_t1096194655::get_offset_of_m_SubmitButton_19(),
	StandaloneInputModule_t1096194655::get_offset_of_m_CancelButton_20(),
	StandaloneInputModule_t1096194655::get_offset_of_m_InputActionsPerSecond_21(),
	StandaloneInputModule_t1096194655::get_offset_of_m_RepeatDelay_22(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ForceModuleActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (InputMode_t1573608962)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2093[3] = 
{
	InputMode_t1573608962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (TouchInputModule_t894675487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[3] = 
{
	TouchInputModule_t894675487::get_offset_of_m_LastMousePosition_12(),
	TouchInputModule_t894675487::get_offset_of_m_MousePosition_13(),
	TouchInputModule_t894675487::get_offset_of_m_ForceModuleActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (BaseRaycaster_t2327671059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (Physics2DRaycaster_t935388869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (PhysicsRaycaster_t1170055767), -1, sizeof(PhysicsRaycaster_t1170055767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2097[4] = 
{
	0,
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t1170055767_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (ColorTween_t723277650)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[6] = 
{
	ColorTween_t723277650::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
