﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1399125956.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons1640775616.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou1336501463.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical2052396382.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1596995480.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup352294875.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder1942933988.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility3144854024.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup423167365.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GooglePlayDownloader651251778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MoveSample395286779.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RotateSample1766843141.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SampleInfo2136294808.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween3087282050.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_LoopType1485160459.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_NamedValueCol1694108638.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_Defaults4166900319.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_CRSpline2211016973.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EasingFunctio1323017328.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_ApplyTween882368618.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenDelayU341536438.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenResta4135396267.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CStartU3Ec_2251047596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SkyboxHelper1246069112.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TextureHelper764444809.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_ARController1110212269.h"
#include "AssemblyU2DCSharp_ARHelpText1498983263.h"
#include "AssemblyU2DCSharp_AppLibrary3292178298.h"
#include "AssemblyU2DCSharp_Cleaner2521427094.h"
#include "AssemblyU2DCSharp_DescriptionsManager373055382.h"
#include "AssemblyU2DCSharp_DescriptionsManager_onVideoEndDe1248556915.h"
#include "AssemblyU2DCSharp_DescriptionsManager_U3CGetTextur1175778357.h"
#include "AssemblyU2DCSharp_DescriptionsStrings1951003243.h"
#include "AssemblyU2DCSharp_DeviceCameraManager3540801010.h"
#include "AssemblyU2DCSharp_FileManager106919537.h"
#include "AssemblyU2DCSharp_GlosarioController3609734652.h"
#include "AssemblyU2DCSharp_GlosarioUIController2753068944.h"
#include "AssemblyU2DCSharp_GlosarioUIController_U3CLoadWord3335664088.h"
#include "AssemblyU2DCSharp_GlosarioUIController_U3CLoadRowU3371403016.h"
#include "AssemblyU2DCSharp_GlossaryMgr3182292730.h"
#include "AssemblyU2DCSharp_GlossaryMgr_U3CShowDefinitionU3E3893704553.h"
#include "AssemblyU2DCSharp_GyroscopeController2265035361.h"
#include "AssemblyU2DCSharp_HomeScreenManager2427744994.h"
#include "AssemblyU2DCSharp_LightIconsManager444881737.h"
#include "AssemblyU2DCSharp_LoadingScript1258549511.h"
#include "AssemblyU2DCSharp_MagnifierScript903792201.h"
#include "AssemblyU2DCSharp_MarkerChildManager1830688875.h"
#include "AssemblyU2DCSharp_MySceneManager1709647213.h"
#include "AssemblyU2DCSharp_MyTrackeableEventHandler2447116080.h"
#include "AssemblyU2DCSharp_MyVuforiaScript3965407553.h"
#include "AssemblyU2DCSharp_OBBDownloader3918424420.h"
#include "AssemblyU2DCSharp_OBBDownloader_U3CStartU3Ec__Iter3160979344.h"
#include "AssemblyU2DCSharp_OBBDownloader_U3CLoadMainMenuU3E2591005235.h"
#include "AssemblyU2DCSharp_OldGyroscopeController3590691066.h"
#include "AssemblyU2DCSharp_SplashScript2592772658.h"
#include "AssemblyU2DCSharp_SplashScript23066541120.h"
#include "AssemblyU2DCSharp_TFTTutorialManager3352625645.h"
#include "AssemblyU2DCSharp_TTarget4033793797.h"
#include "AssemblyU2DCSharp_TargetManager3938169020.h"
#include "AssemblyU2DCSharp_TargetScript3903017436.h"
#include "AssemblyU2DCSharp_ToCenter2110661328.h"
#include "AssemblyU2DCSharp_TouchController2155287579.h"
#include "AssemblyU2DCSharp_TrackeabeManager2980078275.h"
#include "AssemblyU2DCSharp_TutoScreen2156301416.h"
#include "AssemblyU2DCSharp_TutoTargetScript3239500984.h"
#include "AssemblyU2DCSharp_TutorialManager900157007.h"
#include "AssemblyU2DCSharp_TutorialsHandler2864335061.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"
#include "AssemblyU2DCSharp_TutorialsIDS774177219.h"
#include "AssemblyU2DCSharp_VRCameraClamper2660686439.h"
#include "AssemblyU2DCSharp_VRModeManager3340808238.h"
#include "AssemblyU2DCSharp_CardboardOnGUI2062908102.h"
#include "AssemblyU2DCSharp_CardboardOnGUI_OnGUICallback3287452568.h"
#include "AssemblyU2DCSharp_CardboardOnGUIMouse2612501887.h"
#include "AssemblyU2DCSharp_CardboardOnGUIWindow3958675030.h"
#include "AssemblyU2DCSharp_SkyboxMesh1243902519.h"
#include "AssemblyU2DCSharp_StereoLensFlare786564152.h"
#include "AssemblyU2DCSharp_Cardboard1761541558.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (Axis_t1399125956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[3] = 
{
	Axis_t1399125956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (Constraint_t1640775616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2201[4] = 
{
	Constraint_t1640775616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (HorizontalLayoutGroup_t1336501463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (HorizontalOrVerticalLayoutGroup_t2052396382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (LayoutElement_t1596995480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[7] = 
{
	LayoutElement_t1596995480::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1596995480::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1596995480::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (LayoutGroup_t352294875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[8] = 
{
	LayoutGroup_t352294875::get_offset_of_m_Padding_2(),
	LayoutGroup_t352294875::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t352294875::get_offset_of_m_Rect_4(),
	LayoutGroup_t352294875::get_offset_of_m_Tracker_5(),
	LayoutGroup_t352294875::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t352294875::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t352294875::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t352294875::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (LayoutRebuilder_t1942933988), -1, sizeof(LayoutRebuilder_t1942933988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2211[9] = 
{
	LayoutRebuilder_t1942933988::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t1942933988::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (LayoutUtility_t3144854024), -1, sizeof(LayoutUtility_t3144854024_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2212[8] = 
{
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (VerticalLayoutGroup_t423167365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2218[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238938), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (U24ArrayTypeU2412_t3379220355)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220355_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CModuleU3E_t86524798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (GooglePlayDownloader_t651251778), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (MoveSample_t395286779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (RotateSample_t1766843141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (SampleInfo_t2136294808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (iTween_t3087282050), -1, sizeof(iTween_t3087282050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[51] = 
{
	iTween_t3087282050_StaticFields::get_offset_of_tweens_2(),
	iTween_t3087282050_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t3087282050::get_offset_of_id_4(),
	iTween_t3087282050::get_offset_of_type_5(),
	iTween_t3087282050::get_offset_of_method_6(),
	iTween_t3087282050::get_offset_of_easeType_7(),
	iTween_t3087282050::get_offset_of_time_8(),
	iTween_t3087282050::get_offset_of_delay_9(),
	iTween_t3087282050::get_offset_of_loopType_10(),
	iTween_t3087282050::get_offset_of_isRunning_11(),
	iTween_t3087282050::get_offset_of_isPaused_12(),
	iTween_t3087282050::get_offset_of__name_13(),
	iTween_t3087282050::get_offset_of_runningTime_14(),
	iTween_t3087282050::get_offset_of_percentage_15(),
	iTween_t3087282050::get_offset_of_delayStarted_16(),
	iTween_t3087282050::get_offset_of_kinematic_17(),
	iTween_t3087282050::get_offset_of_isLocal_18(),
	iTween_t3087282050::get_offset_of_loop_19(),
	iTween_t3087282050::get_offset_of_reverse_20(),
	iTween_t3087282050::get_offset_of_wasPaused_21(),
	iTween_t3087282050::get_offset_of_physics_22(),
	iTween_t3087282050::get_offset_of_tweenArguments_23(),
	iTween_t3087282050::get_offset_of_space_24(),
	iTween_t3087282050::get_offset_of_ease_25(),
	iTween_t3087282050::get_offset_of_apply_26(),
	iTween_t3087282050::get_offset_of_audioSource_27(),
	iTween_t3087282050::get_offset_of_vector3s_28(),
	iTween_t3087282050::get_offset_of_vector2s_29(),
	iTween_t3087282050::get_offset_of_colors_30(),
	iTween_t3087282050::get_offset_of_floats_31(),
	iTween_t3087282050::get_offset_of_rects_32(),
	iTween_t3087282050::get_offset_of_path_33(),
	iTween_t3087282050::get_offset_of_preUpdate_34(),
	iTween_t3087282050::get_offset_of_postUpdate_35(),
	iTween_t3087282050::get_offset_of_namedcolorvalue_36(),
	iTween_t3087282050::get_offset_of_lastRealTime_37(),
	iTween_t3087282050::get_offset_of_useRealTime_38(),
	iTween_t3087282050::get_offset_of_thisTransform_39(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_40(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_41(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_42(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_43(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_44(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_45(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_46(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_47(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_48(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_49(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_50(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_51(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (EaseType_t2734598229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2234[34] = 
{
	EaseType_t2734598229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (LoopType_t1485160459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	LoopType_t1485160459::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (NamedValueColor_t1694108638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2236[5] = 
{
	NamedValueColor_t1694108638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (Defaults_t4166900319), -1, sizeof(Defaults_t4166900319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2237[16] = 
{
	Defaults_t4166900319_StaticFields::get_offset_of_time_0(),
	Defaults_t4166900319_StaticFields::get_offset_of_delay_1(),
	Defaults_t4166900319_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4166900319_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4166900319_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4166900319_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4166900319_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4166900319_StaticFields::get_offset_of_space_7(),
	Defaults_t4166900319_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4166900319_StaticFields::get_offset_of_color_9(),
	Defaults_t4166900319_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4166900319_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4166900319_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4166900319_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4166900319_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4166900319_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (CRSpline_t2211016973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	CRSpline_t2211016973::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (EasingFunction_t1323017328), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (ApplyTween_t882368618), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t41536438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[3] = 
{
	U3CTweenDelayU3Ec__Iterator0_t41536438::get_offset_of_U24PC_0(),
	U3CTweenDelayU3Ec__Iterator0_t41536438::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t41536438::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t4135396267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[3] = 
{
	U3CTweenRestartU3Ec__Iterator1_t4135396267::get_offset_of_U24PC_0(),
	U3CTweenRestartU3Ec__Iterator1_t4135396267::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t4135396267::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (U3CStartU3Ec__Iterator2_t2251047596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	U3CStartU3Ec__Iterator2_t2251047596::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator2_t2251047596::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t2251047596::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (SkyboxHelper_t1246069112), -1, sizeof(SkyboxHelper_t1246069112_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2244[2] = 
{
	SkyboxHelper_t1246069112_StaticFields::get_offset_of__skybox_HD_0(),
	SkyboxHelper_t1246069112_StaticFields::get_offset_of__skybox_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (TextureHelper_t764444809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (U3CModuleU3E_t86524799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (ARController_t1110212269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[3] = 
{
	ARController_t1110212269::get_offset_of_mDataset_2(),
	ARController_t1110212269::get_offset_of_tracker_3(),
	ARController_t1110212269::get_offset_of__loaded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (ARHelpText_t1498983263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[4] = 
{
	ARHelpText_t1498983263::get_offset_of_twS_2(),
	ARHelpText_t1498983263::get_offset_of_helpTex_3(),
	ARHelpText_t1498983263::get_offset_of_helpRect_4(),
	ARHelpText_t1498983263::get_offset_of_slideHelp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (AppLibrary_t3292178298), -1, sizeof(AppLibrary_t3292178298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2249[36] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AppLibrary_t3292178298_StaticFields::get_offset_of_AppTYPE_26(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_sceneLoad_27(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_openTutAtomatically_28(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_openTFTTutAtomatically_29(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_TUT_TO_OPEN_30(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_LASTDEF_31(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_TFTSourceScene_32(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_CURRENTVIDEONAME_33(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_VIDEO_HELP_AR_34(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_VIDEO_VR_35(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_AR_HELPVIDEO_PLAYED_36(),
	AppLibrary_t3292178298_StaticFields::get_offset_of_APPKEY_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (Cleaner_t2521427094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (DescriptionsManager_t373055382), -1, sizeof(DescriptionsManager_t373055382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2251[30] = 
{
	DescriptionsManager_t373055382::get_offset_of_lightIconsDescriptionsTweenSpeed_2(),
	DescriptionsManager_t373055382::get_offset_of_descriptionsUICam_3(),
	DescriptionsManager_t373055382::get_offset_of_loadingTex_4(),
	DescriptionsManager_t373055382::get_offset_of_descriptionsBackground_5(),
	DescriptionsManager_t373055382::get_offset_of_descriptionTexture_6(),
	DescriptionsManager_t373055382::get_offset_of_extendedDescriptionContainer_7(),
	DescriptionsManager_t373055382::get_offset_of_extendedDescriptionScrollValue_8(),
	DescriptionsManager_t373055382::get_offset_of_extendedDescriptionScrollPanel_9(),
	DescriptionsManager_t373055382::get_offset_of_airbagDescrScrollPanel_10(),
	DescriptionsManager_t373055382::get_offset_of_acCenterScrollPanel_11(),
	DescriptionsManager_t373055382::get_offset_of_acLeftScrollPanel_12(),
	DescriptionsManager_t373055382::get_offset_of_acRightScrollPanel_13(),
	DescriptionsManager_t373055382::get_offset_of_extendedDescriptionOriginalPanelPos_14(),
	DescriptionsManager_t373055382::get_offset_of_airbagDescrOriginalPanelPos_15(),
	DescriptionsManager_t373055382::get_offset_of_acCenterDescrOriginalPanelPos_16(),
	DescriptionsManager_t373055382::get_offset_of_acLeftDescrOriginalPanelPos_17(),
	DescriptionsManager_t373055382::get_offset_of_acRightDescrOriginalPanelPos_18(),
	DescriptionsManager_t373055382::get_offset_of_extendedDescriptionOriginalClip_19(),
	DescriptionsManager_t373055382::get_offset_of_airbagDescrOriginalClip_20(),
	DescriptionsManager_t373055382::get_offset_of_acCenterDescrOriginalClip_21(),
	DescriptionsManager_t373055382::get_offset_of_acLeftDescrOriginalClip_22(),
	DescriptionsManager_t373055382::get_offset_of_acRightDescrOriginalClip_23(),
	DescriptionsManager_t373055382::get_offset_of_zAxis180Rotation_24(),
	DescriptionsManager_t373055382::get_offset_of_guiCameraUIScript_25(),
	DescriptionsManager_t373055382_StaticFields::get_offset_of_OffsetMin_26(),
	DescriptionsManager_t373055382::get_offset_of_texturesOffset_27(),
	DescriptionsManager_t373055382::get_offset_of_evs_28(),
	DescriptionsManager_t373055382::get_offset_of_U3CcurrDescriptionUITexU3Ek__BackingField_29(),
	DescriptionsManager_t373055382::get_offset_of_U3CcurrDescrOriginalWidthU3Ek__BackingField_30(),
	DescriptionsManager_t373055382::get_offset_of_U3CcurrDescrOriginalHeightU3Ek__BackingField_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (onVideoEndDel_t1248556915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (U3CGetTextureU3Ec__Iterator0_t1175778357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[9] = 
{
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U3CpathU3E__0_0(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_pathStr_1(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_name_2(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U3CresourceRequestU3E__1_3(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U24PC_4(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U24current_5(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U3CU24U3EpathStr_6(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U3CU24U3Ename_7(),
	U3CGetTextureU3Ec__Iterator0_t1175778357::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (DescriptionsStrings_t1951003243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[40] = 
{
	DescriptionsStrings_t1951003243::get_offset_of_versoPrecollision_2(),
	DescriptionsStrings_t1951003243::get_offset_of_versoLights_3(),
	DescriptionsStrings_t1951003243::get_offset_of_versoStabilityControl_4(),
	DescriptionsStrings_t1951003243::get_offset_of_versoParkingAssist_5(),
	DescriptionsStrings_t1951003243::get_offset_of_versoMirror_6(),
	DescriptionsStrings_t1951003243::get_offset_of_versoVoiceCommand_7(),
	DescriptionsStrings_t1951003243::get_offset_of_versoMode_8(),
	DescriptionsStrings_t1951003243::get_offset_of_versoPhone_9(),
	DescriptionsStrings_t1951003243::get_offset_of_versoDispTrip_10(),
	DescriptionsStrings_t1951003243::get_offset_of_versoSpeedLimit_11(),
	DescriptionsStrings_t1951003243::get_offset_of_versoSpeedRegulator_12(),
	DescriptionsStrings_t1951003243::get_offset_of_versoLeftTachometer_13(),
	DescriptionsStrings_t1951003243::get_offset_of_versoRightTachometer_14(),
	DescriptionsStrings_t1951003243::get_offset_of_versoCenterScreen_15(),
	DescriptionsStrings_t1951003243::get_offset_of_versoGPSLeftWheel_16(),
	DescriptionsStrings_t1951003243::get_offset_of_versoGPSRightWheel_17(),
	DescriptionsStrings_t1951003243::get_offset_of_versoGPSLeftMenu_18(),
	DescriptionsStrings_t1951003243::get_offset_of_versoGPSRightMenu_19(),
	DescriptionsStrings_t1951003243::get_offset_of_versoACLeftMenu_20(),
	DescriptionsStrings_t1951003243::get_offset_of_versoACCenterMenu_21(),
	DescriptionsStrings_t1951003243::get_offset_of_versoACRightMenu_22(),
	DescriptionsStrings_t1951003243::get_offset_of_versoAirbag_23(),
	DescriptionsStrings_t1951003243::get_offset_of_versoWindow_24(),
	DescriptionsStrings_t1951003243::get_offset_of_versoPower_25(),
	DescriptionsStrings_t1951003243::get_offset_of_versoA_OFF_26(),
	DescriptionsStrings_t1951003243::get_offset_of_doorDescr_27(),
	DescriptionsStrings_t1951003243::get_offset_of_mirrorDescr_28(),
	DescriptionsStrings_t1951003243::get_offset_of_tires_preCollissionDescr_29(),
	DescriptionsStrings_t1951003243::get_offset_of_volume_ModeDescr_30(),
	DescriptionsStrings_t1951003243::get_offset_of_phone_LineCrossDescr_31(),
	DescriptionsStrings_t1951003243::get_offset_of_speedRegulatorDescr_32(),
	DescriptionsStrings_t1951003243::get_offset_of_warning_airbag_seatbeltDescr_33(),
	DescriptionsStrings_t1951003243::get_offset_of_ac_DualDescr_34(),
	DescriptionsStrings_t1951003243::get_offset_of_auto_OffDescr_35(),
	DescriptionsStrings_t1951003243::get_offset_of_front_rearDescr_36(),
	DescriptionsStrings_t1951003243::get_offset_of_usb_12VoltDescr_37(),
	DescriptionsStrings_t1951003243::get_offset_of_mode_stabilityCtrlDescr_38(),
	DescriptionsStrings_t1951003243::get_offset_of_enginePowerDescr_39(),
	DescriptionsStrings_t1951003243::get_offset_of_fuelTankLidDescr_40(),
	DescriptionsStrings_t1951003243::get_offset_of_hoodOpenBtnDescr_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (DeviceCameraManager_t3540801010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[6] = 
{
	DeviceCameraManager_t3540801010::get_offset_of_autofocusEnabled_2(),
	DeviceCameraManager_t3540801010::get_offset_of_flashEnabled_3(),
	DeviceCameraManager_t3540801010::get_offset_of_flashBtnUITex_4(),
	DeviceCameraManager_t3540801010::get_offset_of_flashOnTex_5(),
	DeviceCameraManager_t3540801010::get_offset_of_flashOffTex_6(),
	DeviceCameraManager_t3540801010::get_offset_of_textMesh_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (FileManager_t106919537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (GlosarioController_t3609734652), -1, sizeof(GlosarioController_t3609734652_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[7] = 
{
	GlosarioController_t3609734652::get_offset_of__xml_0(),
	GlosarioController_t3609734652::get_offset_of__dictionary_1(),
	GlosarioController_t3609734652::get_offset_of__defTitleDic_2(),
	GlosarioController_t3609734652::get_offset_of__palabraKeywordDic_3(),
	GlosarioController_t3609734652::get_offset_of__vrTargetsDic_4(),
	GlosarioController_t3609734652::get_offset_of__tutCatDic_5(),
	GlosarioController_t3609734652_StaticFields::get_offset_of__instance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (GlosarioUIController_t2753068944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[9] = 
{
	GlosarioUIController_t2753068944::get_offset_of__panelIdioma_2(),
	GlosarioUIController_t2753068944::get_offset_of__panelGlosario_3(),
	GlosarioUIController_t2753068944::get_offset_of__body_4(),
	GlosarioUIController_t2753068944::get_offset_of__panelDefinicion_5(),
	GlosarioUIController_t2753068944::get_offset_of_txtSearch_6(),
	GlosarioUIController_t2753068944::get_offset_of_buttonsParent_7(),
	GlosarioUIController_t2753068944::get_offset_of_label_8(),
	GlosarioUIController_t2753068944::get_offset_of_scrollBar_9(),
	GlosarioUIController_t2753068944::get_offset_of_btnN_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (U3CLoadWordsU3Ec__Iterator1_t3335664088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[7] = 
{
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_words_0(),
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_U3CU24s_4U3E__0_1(),
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_U3CwordU3E__1_2(),
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_U24PC_3(),
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_U24current_4(),
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_U3CU24U3Ewords_5(),
	U3CLoadWordsU3Ec__Iterator1_t3335664088::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CLoadRowU3Ec__Iterator2_t3371403016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[8] = 
{
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U3CrowU3E__0_0(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_word_1(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U3CbtnU3E__1_2(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U3CdelU3E__2_3(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U24PC_4(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U24current_5(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U3CU24U3Eword_6(),
	U3CLoadRowU3Ec__Iterator2_t3371403016::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (GlossaryMgr_t3182292730), -1, sizeof(GlossaryMgr_t3182292730_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2261[15] = 
{
	GlossaryMgr_t3182292730::get_offset_of_definitionTexture_2(),
	GlossaryMgr_t3182292730::get_offset_of_wordsContainer_3(),
	GlossaryMgr_t3182292730::get_offset_of_path_4(),
	GlossaryMgr_t3182292730::get_offset_of_defTitle_5(),
	GlossaryMgr_t3182292730::get_offset_of_def_6(),
	GlossaryMgr_t3182292730::get_offset_of_wordsButtons_7(),
	GlossaryMgr_t3182292730::get_offset_of_btnCount_8(),
	GlossaryMgr_t3182292730::get_offset_of_backToMenuBtn_9(),
	GlossaryMgr_t3182292730_StaticFields::get_offset_of_vrTargetsToShow_10(),
	GlossaryMgr_t3182292730_StaticFields::get_offset_of_LASTDEFWORD_11(),
	GlossaryMgr_t3182292730::get_offset_of_showInVRBtn_12(),
	GlossaryMgr_t3182292730::get_offset_of_showInTUTBtn_13(),
	GlossaryMgr_t3182292730::get_offset_of_vrTarget_14(),
	GlossaryMgr_t3182292730::get_offset_of_lastDef_15(),
	GlossaryMgr_t3182292730::get_offset_of_fullPath_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (U3CShowDefinitionU3Ec__Iterator3_t3893704553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[7] = 
{
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_pDefTitle_0(),
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_pDef_1(),
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_U24PC_2(),
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_U24current_3(),
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_U3CU24U3EpDefTitle_4(),
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_U3CU24U3EpDef_5(),
	U3CShowDefinitionU3Ec__Iterator3_t3893704553::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (GyroscopeController_t2265035361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[24] = 
{
	GyroscopeController_t2265035361::get_offset_of_baseIdentity_2(),
	GyroscopeController_t2265035361::get_offset_of_referanceRotation_3(),
	GyroscopeController_t2265035361::get_offset_of_cameraBase_4(),
	GyroscopeController_t2265035361::get_offset_of_calibration_5(),
	GyroscopeController_t2265035361::get_offset_of_baseOrientation_6(),
	GyroscopeController_t2265035361::get_offset_of_baseOrientationRotationFix_7(),
	GyroscopeController_t2265035361::get_offset_of_gyroEnble_8(),
	GyroscopeController_t2265035361::get_offset_of_minSwipeDistY_9(),
	GyroscopeController_t2265035361::get_offset_of_minSwipeDistX_10(),
	GyroscopeController_t2265035361::get_offset_of_cam360_11(),
	GyroscopeController_t2265035361::get_offset_of_camButtons_12(),
	GyroscopeController_t2265035361::get_offset_of_startPos_13(),
	GyroscopeController_t2265035361::get_offset_of_swipeDistVertical_14(),
	GyroscopeController_t2265035361::get_offset_of_swipeTime_15(),
	GyroscopeController_t2265035361::get_offset_of_canCountUp_16(),
	GyroscopeController_t2265035361::get_offset_of_originalRotation_17(),
	GyroscopeController_t2265035361::get_offset_of_sensitivityX_18(),
	GyroscopeController_t2265035361::get_offset_of_sensitivityY_19(),
	GyroscopeController_t2265035361::get_offset_of_minimumX_20(),
	GyroscopeController_t2265035361::get_offset_of_maximumX_21(),
	GyroscopeController_t2265035361::get_offset_of_minimumY_22(),
	GyroscopeController_t2265035361::get_offset_of_maximumY_23(),
	GyroscopeController_t2265035361::get_offset_of_rotationY_24(),
	GyroscopeController_t2265035361::get_offset_of_rotationX_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (HomeScreenManager_t2427744994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[55] = 
{
	HomeScreenManager_t2427744994::get_offset_of_btnAssistanceUITex_2(),
	HomeScreenManager_t2427744994::get_offset_of_btnAssistanceGLOWTex_3(),
	HomeScreenManager_t2427744994::get_offset_of_btn360ViewUITex_4(),
	HomeScreenManager_t2427744994::get_offset_of_btn360ViewGLOWTex_5(),
	HomeScreenManager_t2427744994::get_offset_of_btnLexiqueUITex_6(),
	HomeScreenManager_t2427744994::get_offset_of_btnARUITex_7(),
	HomeScreenManager_t2427744994::get_offset_of_btnARGLOWTex_8(),
	HomeScreenManager_t2427744994::get_offset_of_btnLexiqueGLOWTex_9(),
	HomeScreenManager_t2427744994::get_offset_of_btnTUTUITex_10(),
	HomeScreenManager_t2427744994::get_offset_of_btnTUTGLOWTex_11(),
	HomeScreenManager_t2427744994::get_offset_of_btnLegalsUITex_12(),
	HomeScreenManager_t2427744994::get_offset_of_btnLegalsGLOWTex_13(),
	HomeScreenManager_t2427744994::get_offset_of_lexiqueFlashGO_14(),
	HomeScreenManager_t2427744994::get_offset_of_arFlashGO_15(),
	HomeScreenManager_t2427744994::get_offset_of_v360FlashGO_16(),
	HomeScreenManager_t2427744994::get_offset_of_tutFlashGO_17(),
	HomeScreenManager_t2427744994::get_offset_of_btnAssitanceNormalTex_18(),
	HomeScreenManager_t2427744994::get_offset_of_btnARNormalTex_19(),
	HomeScreenManager_t2427744994::get_offset_of_btn360NormalTex_20(),
	HomeScreenManager_t2427744994::get_offset_of_btnTUTNormalTex_21(),
	HomeScreenManager_t2427744994::get_offset_of_btnLegalsNormalTex_22(),
	HomeScreenManager_t2427744994::get_offset_of_tutoScreen_23(),
	HomeScreenManager_t2427744994::get_offset_of_arHelpFrameTex_24(),
	HomeScreenManager_t2427744994::get_offset_of_arHelpText_25(),
	HomeScreenManager_t2427744994::get_offset_of_goToARBtn_26(),
	HomeScreenManager_t2427744994::get_offset_of_backToMenuBtn_27(),
	HomeScreenManager_t2427744994::get_offset_of_backToMenuVRBtn_28(),
	HomeScreenManager_t2427744994::get_offset_of_goToVRBtn_29(),
	HomeScreenManager_t2427744994::get_offset_of_arHelpTex_30(),
	HomeScreenManager_t2427744994::get_offset_of_arTorchTex_31(),
	HomeScreenManager_t2427744994::get_offset_of_assistanceScreen_32(),
	HomeScreenManager_t2427744994::get_offset_of_arHelpScreen_33(),
	HomeScreenManager_t2427744994::get_offset_of_vrHelpScreen_34(),
	HomeScreenManager_t2427744994::get_offset_of_legalsScreen_35(),
	HomeScreenManager_t2427744994::get_offset_of_v360HelpScreen_36(),
	HomeScreenManager_t2427744994::get_offset_of_tutBtnsPanel_37(),
	HomeScreenManager_t2427744994::get_offset_of_originalTutButtonsPanelClipPos_38(),
	HomeScreenManager_t2427744994::get_offset_of_originalTutButtonsPanelPos_39(),
	HomeScreenManager_t2427744994::get_offset_of_backGroundAnim_40(),
	HomeScreenManager_t2427744994::get_offset_of_loadingBar_41(),
	HomeScreenManager_t2427744994::get_offset_of_loadingFrame_42(),
	HomeScreenManager_t2427744994::get_offset_of_vRLoadingBar_43(),
	HomeScreenManager_t2427744994::get_offset_of_vRLoadingFrame_44(),
	HomeScreenManager_t2427744994::get_offset_of_arLoadingLabel_45(),
	HomeScreenManager_t2427744994::get_offset_of_showARProgressBar_46(),
	HomeScreenManager_t2427744994::get_offset_of_showVRProgressBar_47(),
	HomeScreenManager_t2427744994::get_offset_of_barProgress_48(),
	HomeScreenManager_t2427744994::get_offset_of_flashs_49(),
	HomeScreenManager_t2427744994::get_offset_of_tutBtn_50(),
	HomeScreenManager_t2427744994::get_offset_of_tutWarningOKBtn_51(),
	HomeScreenManager_t2427744994::get_offset_of_tutCatPhoneBtn_52(),
	HomeScreenManager_t2427744994::get_offset_of_tutCatGPSBtn_53(),
	HomeScreenManager_t2427744994::get_offset_of_tutCatAppBtn_54(),
	HomeScreenManager_t2427744994::get_offset_of_tutCatRadioBtn_55(),
	HomeScreenManager_t2427744994::get_offset_of_uiCam_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (LightIconsManager_t444881737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[10] = 
{
	LightIconsManager_t444881737::get_offset_of_iconsContainer_2(),
	LightIconsManager_t444881737::get_offset_of_target28IconContainer_3(),
	LightIconsManager_t444881737::get_offset_of_targetSHIFTIconContainer_4(),
	LightIconsManager_t444881737::get_offset_of_target30IconContainer_5(),
	LightIconsManager_t444881737::get_offset_of_lightIconTweens_6(),
	LightIconsManager_t444881737::get_offset_of_currentIcon_7(),
	LightIconsManager_t444881737::get_offset_of_currentDescription_8(),
	LightIconsManager_t444881737::get_offset_of_iconsCount_9(),
	LightIconsManager_t444881737::get_offset_of_showingDescription_10(),
	LightIconsManager_t444881737::get_offset_of_currDescr_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (LoadingScript_t1258549511), -1, sizeof(LoadingScript_t1258549511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2266[6] = 
{
	LoadingScript_t1258549511::get_offset_of_levelAsync_2(),
	LoadingScript_t1258549511::get_offset_of_showARProgressBar_3(),
	LoadingScript_t1258549511::get_offset_of_loadingBar_4(),
	LoadingScript_t1258549511::get_offset_of_helpTex_5(),
	LoadingScript_t1258549511::get_offset_of_vrHelpTex_6(),
	LoadingScript_t1258549511_StaticFields::get_offset_of_sceneID_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (MagnifierScript_t903792201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[28] = 
{
	MagnifierScript_t903792201::get_offset_of_magnify_2(),
	MagnifierScript_t903792201::get_offset_of_guiCamera_3(),
	MagnifierScript_t903792201::get_offset_of_firstFingerPos_4(),
	MagnifierScript_t903792201::get_offset_of_currentFingerPos_5(),
	MagnifierScript_t903792201::get_offset_of_direction_6(),
	MagnifierScript_t903792201::get_offset_of_colPoint_7(),
	MagnifierScript_t903792201::get_offset_of_rcH_8(),
	MagnifierScript_t903792201::get_offset_of_magnifierCamera_9(),
	MagnifierScript_t903792201::get_offset_of_magnifierContainer_10(),
	MagnifierScript_t903792201::get_offset_of_magnifierFrameUiTex_11(),
	MagnifierScript_t903792201::get_offset_of_originalZ_12(),
	MagnifierScript_t903792201::get_offset_of_isVr_13(),
	MagnifierScript_t903792201::get_offset_of_gyroControl_14(),
	MagnifierScript_t903792201::get_offset_of_gyroControlFull_15(),
	MagnifierScript_t903792201::get_offset_of_magnifierFactor_16(),
	MagnifierScript_t903792201::get_offset_of_textureToMagnify_17(),
	MagnifierScript_t903792201::get_offset_of_descrMgr_18(),
	MagnifierScript_t903792201::get_offset_of_initSize_19(),
	MagnifierScript_t903792201::get_offset_of_magnifierEnabled_20(),
	MagnifierScript_t903792201::get_offset_of_t1_21(),
	MagnifierScript_t903792201::get_offset_of_t2_22(),
	MagnifierScript_t903792201::get_offset_of_zoomValue_23(),
	MagnifierScript_t903792201::get_offset_of_offsetX_24(),
	MagnifierScript_t903792201::get_offset_of_offsetYMin_25(),
	MagnifierScript_t903792201::get_offset_of_offsetYMax_26(),
	MagnifierScript_t903792201::get_offset_of_pos_27(),
	MagnifierScript_t903792201::get_offset_of_toPos_28(),
	MagnifierScript_t903792201::get_offset_of_lastT_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (MarkerChildManager_t1830688875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[3] = 
{
	MarkerChildManager_t1830688875::get_offset_of_childTweens_2(),
	MarkerChildManager_t1830688875::get_offset_of_childTweensCount_3(),
	MarkerChildManager_t1830688875::get_offset_of_childInitialSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (MySceneManager_t1709647213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (MyTrackeableEventHandler_t2447116080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[2] = 
{
	MyTrackeableEventHandler_t2447116080::get_offset_of_ARController_2(),
	MyTrackeableEventHandler_t2447116080::get_offset_of__trackableBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (MyVuforiaScript_t3965407553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (OBBDownloader_t3918424420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[2] = 
{
	OBBDownloader_t3918424420::get_offset_of_loadingText_2(),
	OBBDownloader_t3918424420::get_offset_of_originalText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (U3CStartU3Ec__Iterator4_t3160979344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[3] = 
{
	U3CStartU3Ec__Iterator4_t3160979344::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator4_t3160979344::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator4_t3160979344::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (U3CLoadMainMenuU3Ec__Iterator5_t2591005235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[2] = 
{
	U3CLoadMainMenuU3Ec__Iterator5_t2591005235::get_offset_of_U24PC_0(),
	U3CLoadMainMenuU3Ec__Iterator5_t2591005235::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (OldGyroscopeController_t3590691066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[33] = 
{
	OldGyroscopeController_t3590691066::get_offset_of_baseIdentity_2(),
	OldGyroscopeController_t3590691066::get_offset_of_referanceRotation_3(),
	OldGyroscopeController_t3590691066::get_offset_of_cameraBase_4(),
	OldGyroscopeController_t3590691066::get_offset_of_calibration_5(),
	OldGyroscopeController_t3590691066::get_offset_of_baseOrientation_6(),
	OldGyroscopeController_t3590691066::get_offset_of_baseOrientationRotationFix_7(),
	OldGyroscopeController_t3590691066::get_offset_of_gyroEnble_8(),
	OldGyroscopeController_t3590691066::get_offset_of_minSwipeDistY_9(),
	OldGyroscopeController_t3590691066::get_offset_of_minSwipeDistX_10(),
	OldGyroscopeController_t3590691066::get_offset_of_startPos_11(),
	OldGyroscopeController_t3590691066::get_offset_of_swipeDistVertical_12(),
	OldGyroscopeController_t3590691066::get_offset_of_swipeTime_13(),
	OldGyroscopeController_t3590691066::get_offset_of_canCountUp_14(),
	OldGyroscopeController_t3590691066::get_offset_of_originalRotation_15(),
	OldGyroscopeController_t3590691066::get_offset_of_sensitivityX_16(),
	OldGyroscopeController_t3590691066::get_offset_of_sensitivityY_17(),
	OldGyroscopeController_t3590691066::get_offset_of_minimumX_18(),
	OldGyroscopeController_t3590691066::get_offset_of_maximumX_19(),
	OldGyroscopeController_t3590691066::get_offset_of_minimumY_20(),
	OldGyroscopeController_t3590691066::get_offset_of_maximumY_21(),
	OldGyroscopeController_t3590691066::get_offset_of_rotationY_22(),
	OldGyroscopeController_t3590691066::get_offset_of_rotationX_23(),
	OldGyroscopeController_t3590691066::get_offset_of_maxUPRot_24(),
	OldGyroscopeController_t3590691066::get_offset_of_maxDownRot_25(),
	OldGyroscopeController_t3590691066::get_offset_of_headRot_26(),
	OldGyroscopeController_t3590691066::get_offset_of_headRot2_27(),
	OldGyroscopeController_t3590691066::get_offset_of_newVector_28(),
	OldGyroscopeController_t3590691066::get_offset_of_xRot_29(),
	OldGyroscopeController_t3590691066::get_offset_of_xRot2_30(),
	OldGyroscopeController_t3590691066::get_offset_of_xLimit_31(),
	OldGyroscopeController_t3590691066::get_offset_of_newX_32(),
	OldGyroscopeController_t3590691066::get_offset_of_upX_33(),
	OldGyroscopeController_t3590691066::get_offset_of_downX_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (SplashScript_t2592772658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (SplashScript2_t3066541120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (TFTTutorialManager_t3352625645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[16] = 
{
	TFTTutorialManager_t3352625645::get_offset_of_tftIconsContainer_2(),
	TFTTutorialManager_t3352625645::get_offset_of_mainMenuCam_3(),
	TFTTutorialManager_t3352625645::get_offset_of_tftTutContainer_4(),
	TFTTutorialManager_t3352625645::get_offset_of_currentTut_5(),
	TFTTutorialManager_t3352625645::get_offset_of_tutStepUITex_6(),
	TFTTutorialManager_t3352625645::get_offset_of_tutHandlerGO_7(),
	TFTTutorialManager_t3352625645::get_offset_of_currentTutStepIndex_8(),
	TFTTutorialManager_t3352625645::get_offset_of_currentTutStepCount_9(),
	TFTTutorialManager_t3352625645::get_offset_of_audioSrc_10(),
	TFTTutorialManager_t3352625645::get_offset_of_introClip_11(),
	TFTTutorialManager_t3352625645::get_offset_of_nextStepArrow_12(),
	TFTTutorialManager_t3352625645::get_offset_of_previousStepArrow_13(),
	TFTTutorialManager_t3352625645::get_offset_of_returnBtn_14(),
	TFTTutorialManager_t3352625645::get_offset_of_twS_15(),
	TFTTutorialManager_t3352625645::get_offset_of_toggleAudio_16(),
	TFTTutorialManager_t3352625645::get_offset_of_audioBtnSpr_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (TTarget_t4033793797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[1] = 
{
	TTarget_t4033793797::get_offset_of_buttonPos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (TargetManager_t3938169020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[10] = 
{
	TargetManager_t3938169020::get_offset_of_targets_2(),
	TargetManager_t3938169020::get_offset_of_targetsCount_3(),
	TargetManager_t3938169020::get_offset_of_targetsCameraUIScript_4(),
	TargetManager_t3938169020::get_offset_of_circleTargetHiLight_5(),
	TargetManager_t3938169020::get_offset_of_rectTargetHiLight_6(),
	TargetManager_t3938169020::get_offset_of_enableAnims_7(),
	TargetManager_t3938169020::get_offset_of_magnifierGO_8(),
	TargetManager_t3938169020::get_offset_of__show_9(),
	TargetManager_t3938169020::get_offset_of_Targets_10(),
	TargetManager_t3938169020::get_offset_of_vrCameraClamper_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (TargetScript_t3903017436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[1] = 
{
	TargetScript_t3903017436::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (ToCenter_t2110661328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[19] = 
{
	ToCenter_t2110661328::get_offset_of_currentHairPos_2(),
	ToCenter_t2110661328::get_offset_of_target_3(),
	ToCenter_t2110661328::get_offset_of_fwd_4(),
	ToCenter_t2110661328::get_offset_of_point_5(),
	ToCenter_t2110661328::get_offset_of_toPos_6(),
	ToCenter_t2110661328::get_offset_of_originalPos_7(),
	ToCenter_t2110661328::get_offset_of_targetScale_8(),
	ToCenter_t2110661328::get_offset_of_targetRot_9(),
	ToCenter_t2110661328::get_offset_of_originalScale_10(),
	ToCenter_t2110661328::get_offset_of_hit_11(),
	ToCenter_t2110661328::get_offset_of_camara_12(),
	ToCenter_t2110661328::get_offset_of_doTween_13(),
	ToCenter_t2110661328::get_offset_of_slide_14(),
	ToCenter_t2110661328::get_offset_of_matchSize_15(),
	ToCenter_t2110661328::get_offset_of_offset_16(),
	ToCenter_t2110661328::get_offset_of_slideSpeed_17(),
	ToCenter_t2110661328::get_offset_of_scaleFactor_18(),
	ToCenter_t2110661328::get_offset_of_originalZ_19(),
	ToCenter_t2110661328::get_offset_of_distance_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (TouchController_t2155287579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[12] = 
{
	TouchController_t2155287579::get_offset_of_referanceRotation_2(),
	TouchController_t2155287579::get_offset_of_originalRotation_3(),
	TouchController_t2155287579::get_offset_of_sensitivityX_4(),
	TouchController_t2155287579::get_offset_of_sensitivityY_5(),
	TouchController_t2155287579::get_offset_of_minimumX_6(),
	TouchController_t2155287579::get_offset_of_maximumX_7(),
	TouchController_t2155287579::get_offset_of_minimumY_8(),
	TouchController_t2155287579::get_offset_of_maximumY_9(),
	TouchController_t2155287579::get_offset_of_rotationY_10(),
	TouchController_t2155287579::get_offset_of_rotationX_11(),
	TouchController_t2155287579::get_offset_of_rot_12(),
	TouchController_t2155287579::get_offset_of_smoothRot_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (TrackeabeManager_t2980078275), -1, sizeof(TrackeabeManager_t2980078275_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2284[9] = 
{
	TrackeabeManager_t2980078275::get_offset_of_steeringWheelmarkersGO_2(),
	TrackeabeManager_t2980078275::get_offset_of_steeringWheelmarkersCount_3(),
	TrackeabeManager_t2980078275::get_offset_of_rav4SWMarkersGO_4(),
	TrackeabeManager_t2980078275::get_offset_of_rav4SWMarkersCount_5(),
	TrackeabeManager_t2980078275::get_offset_of_rav4ACMarkersGO_6(),
	TrackeabeManager_t2980078275::get_offset_of_rav4ACMarkersCount_7(),
	TrackeabeManager_t2980078275::get_offset_of_rav4TachometerGO_8(),
	TrackeabeManager_t2980078275::get_offset_of_rav4TachometerMarkersCount_9(),
	TrackeabeManager_t2980078275_StaticFields::get_offset_of_U3CinstanceU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (TutoScreen_t2156301416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[3] = 
{
	TutoScreen_t2156301416::get_offset_of_flashTextureAnim_2(),
	TutoScreen_t2156301416::get_offset_of_buttonsScroll_3(),
	TutoScreen_t2156301416::get_offset_of_scrollAmmount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (TutoTargetScript_t3239500984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[4] = 
{
	TutoTargetScript_t3239500984::get_offset_of_targetHL_2(),
	TutoTargetScript_t3239500984::get_offset_of_an_3(),
	TutoTargetScript_t3239500984::get_offset_of_tS_4(),
	TutoTargetScript_t3239500984::get_offset_of_initSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (TutorialManager_t900157007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[62] = 
{
	TutorialManager_t900157007::get_offset_of_mainMenuCam_2(),
	TutorialManager_t900157007::get_offset_of_tutorialBackground_3(),
	TutorialManager_t900157007::get_offset_of_tutoWarning_4(),
	TutorialManager_t900157007::get_offset_of_phoneBtnGO_5(),
	TutorialManager_t900157007::get_offset_of_phoneBtnHL_6(),
	TutorialManager_t900157007::get_offset_of_phoneBtn_7(),
	TutorialManager_t900157007::get_offset_of_gpsBtnGO_8(),
	TutorialManager_t900157007::get_offset_of_gpsBtnHL_9(),
	TutorialManager_t900157007::get_offset_of_gpsBtn_10(),
	TutorialManager_t900157007::get_offset_of_applicationBtnGO_11(),
	TutorialManager_t900157007::get_offset_of_applicationBtnHL_12(),
	TutorialManager_t900157007::get_offset_of_applicationBtn_13(),
	TutorialManager_t900157007::get_offset_of_radioBtnGO_14(),
	TutorialManager_t900157007::get_offset_of_radioBtnHL_15(),
	TutorialManager_t900157007::get_offset_of_radioBtn_16(),
	TutorialManager_t900157007::get_offset_of_warningContainer_17(),
	TutorialManager_t900157007::get_offset_of_categoriesContainer_18(),
	TutorialManager_t900157007::get_offset_of_phoneBtnsContainer_19(),
	TutorialManager_t900157007::get_offset_of_tutStepScreen_20(),
	TutorialManager_t900157007::get_offset_of_currentTutTargetPos_21(),
	TutorialManager_t900157007::get_offset_of_currentTutStepsCount_22(),
	TutorialManager_t900157007::get_offset_of_currentTutAutomaticStepIndex_23(),
	TutorialManager_t900157007::get_offset_of_currentTutStepIndex_24(),
	TutorialManager_t900157007::get_offset_of_tutStepDir_25(),
	TutorialManager_t900157007::get_offset_of_previousScreen_26(),
	TutorialManager_t900157007::get_offset_of_screensList_27(),
	TutorialManager_t900157007::get_offset_of_screensCount_28(),
	TutorialManager_t900157007::get_offset_of_tutorialContainer_29(),
	TutorialManager_t900157007::get_offset_of_currentTut_30(),
	TutorialManager_t900157007::get_offset_of_targetSpeed_31(),
	TutorialManager_t900157007::get_offset_of_targetTrans_32(),
	TutorialManager_t900157007::get_offset_of_targetHLTex_33(),
	TutorialManager_t900157007::get_offset_of_prevStepBtnCol_34(),
	TutorialManager_t900157007::get_offset_of_audioBtnCol_35(),
	TutorialManager_t900157007::get_offset_of_menuBtn_36(),
	TutorialManager_t900157007::get_offset_of_tutoHandler_37(),
	TutorialManager_t900157007::get_offset_of_timeLineScroll_38(),
	TutorialManager_t900157007::get_offset_of_slidingIndex_39(),
	TutorialManager_t900157007::get_offset_of_slideSteps_40(),
	TutorialManager_t900157007::get_offset_of_nextScreen_41(),
	TutorialManager_t900157007::get_offset_of_slidedForward_42(),
	TutorialManager_t900157007::get_offset_of_slidedBackwards_43(),
	TutorialManager_t900157007::get_offset_of_stepsCols_44(),
	TutorialManager_t900157007::get_offset_of_stepsColsCount_45(),
	TutorialManager_t900157007::get_offset_of_audioSrc_46(),
	TutorialManager_t900157007::get_offset_of_originalTimeLineToPos_47(),
	TutorialManager_t900157007::get_offset_of_originalTimeLineMaskTOPos_48(),
	TutorialManager_t900157007::get_offset_of_deployTutMenu_49(),
	TutorialManager_t900157007::get_offset_of_tutMenuBtnsTween_50(),
	TutorialManager_t900157007::get_offset_of_automaticStepDelay_51(),
	TutorialManager_t900157007::get_offset_of_scrollValue_52(),
	TutorialManager_t900157007::get_offset_of_timeLineMaskSlideValue_53(),
	TutorialManager_t900157007::get_offset_of_slideSpeed_54(),
	TutorialManager_t900157007::get_offset_of_timeLineTrans_55(),
	TutorialManager_t900157007::get_offset_of_timeLineMaskTrans_56(),
	TutorialManager_t900157007::get_offset_of_timeLineMaskTOPos_57(),
	TutorialManager_t900157007::get_offset_of_updateTimeLine_58(),
	TutorialManager_t900157007::get_offset_of_slideTimeLine_59(),
	TutorialManager_t900157007::get_offset_of_toggleAudio_60(),
	TutorialManager_t900157007::get_offset_of_audioBtnSpr_61(),
	TutorialManager_t900157007::get_offset_of_timeLineToPos_62(),
	TutorialManager_t900157007::get_offset_of_twS_63(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (TutorialsHandler_t2864335061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[2] = 
{
	TutorialsHandler_t2864335061::get_offset_of_tutorials_2(),
	TutorialsHandler_t2864335061::get_offset_of_tutorialsCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (Tutorial_t257920894)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[11] = 
{
	Tutorial_t257920894::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_targetPos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_steps_names_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_clips_prefix_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_clips_startIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_clips_endIndex_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_automaticIndex_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_slidingIndex_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_slideSteps_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_searchLabels_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tutorial_t257920894::get_offset_of_audio_paths_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (TutorialsIDS_t774177219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[30] = 
{
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP1_2(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP2_3(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP3_4(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP4_5(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP5_6(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP6_7(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP7_8(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP8_9(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP9_10(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP10_11(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP11_12(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP12_13(),
	TutorialsIDS_t774177219::get_offset_of_TUT_STEP13_14(),
	TutorialsIDS_t774177219::get_offset_of_TUT_PHONE_BLUETOOTH_15(),
	TutorialsIDS_t774177219::get_offset_of_TUT_PHONE_INTERNET_16(),
	TutorialsIDS_t774177219::get_offset_of_TUT_PHONE_TUT_3_17(),
	TutorialsIDS_t774177219::get_offset_of_TUT_PHONE_TUT_4_18(),
	TutorialsIDS_t774177219::get_offset_of_TUT_GPS_TUT_1_19(),
	TutorialsIDS_t774177219::get_offset_of_TUT_GPS_TUT_2_20(),
	TutorialsIDS_t774177219::get_offset_of_TUT_GPS_TUT_3_21(),
	TutorialsIDS_t774177219::get_offset_of_TUT_GPS_TUT_4_22(),
	TutorialsIDS_t774177219::get_offset_of_TUT_GPS_TUT_5_23(),
	TutorialsIDS_t774177219::get_offset_of_TUT_APP_TUT_1_24(),
	TutorialsIDS_t774177219::get_offset_of_TUT_RADIO_TUT_1_25(),
	TutorialsIDS_t774177219::get_offset_of_TUT_TFT_TUT_1_26(),
	TutorialsIDS_t774177219::get_offset_of_TUT_TFT_TUT_2_27(),
	TutorialsIDS_t774177219::get_offset_of_TUT_TFT_TUT_3_28(),
	TutorialsIDS_t774177219::get_offset_of_TUT_TFT_TUT_4_29(),
	TutorialsIDS_t774177219::get_offset_of_TUT_TFT_TUT_5_30(),
	TutorialsIDS_t774177219::get_offset_of_TUT_TFT_TUT_6_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (VRCameraClamper_t2660686439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[34] = 
{
	VRCameraClamper_t2660686439::get_offset_of_cBHead_2(),
	VRCameraClamper_t2660686439::get_offset_of_headTrans_3(),
	VRCameraClamper_t2660686439::get_offset_of_cBHeadRotTM_4(),
	VRCameraClamper_t2660686439::get_offset_of_maxUPRot_5(),
	VRCameraClamper_t2660686439::get_offset_of_maxDownRot_6(),
	VRCameraClamper_t2660686439::get_offset_of_debugRotation_7(),
	VRCameraClamper_t2660686439::get_offset_of_updateRotation_8(),
	VRCameraClamper_t2660686439::get_offset_of_SmoothRotation_9(),
	VRCameraClamper_t2660686439::get_offset_of_clampRotation_10(),
	VRCameraClamper_t2660686439::get_offset_of_doubleTapZoom_11(),
	VRCameraClamper_t2660686439::get_offset_of_newX_12(),
	VRCameraClamper_t2660686439::get_offset_of_xRot_13(),
	VRCameraClamper_t2660686439::get_offset_of_xRot2_14(),
	VRCameraClamper_t2660686439::get_offset_of_xLimit_15(),
	VRCameraClamper_t2660686439::get_offset_of_originalMaxUpRot_16(),
	VRCameraClamper_t2660686439::get_offset_of_originalMaxDownRot_17(),
	VRCameraClamper_t2660686439::get_offset_of_newVector_18(),
	VRCameraClamper_t2660686439::get_offset_of_headRot_19(),
	VRCameraClamper_t2660686439::get_offset_of_headRot2_20(),
	VRCameraClamper_t2660686439::get_offset_of_gyroBtnTex_21(),
	VRCameraClamper_t2660686439::get_offset_of_swipeBtnTex_22(),
	VRCameraClamper_t2660686439::get_offset_of_vrControlModeBtn_23(),
	VRCameraClamper_t2660686439::get_offset_of_enableGyro_24(),
	VRCameraClamper_t2660686439::get_offset_of_tapCount_25(),
	VRCameraClamper_t2660686439::get_offset_of_t_26(),
	VRCameraClamper_t2660686439::get_offset_of_t1_27(),
	VRCameraClamper_t2660686439::get_offset_of_t2_28(),
	VRCameraClamper_t2660686439::get_offset_of_zoomIn_29(),
	VRCameraClamper_t2660686439::get_offset_of_tolerance_30(),
	VRCameraClamper_t2660686439::get_offset_of_prevDistance_31(),
	VRCameraClamper_t2660686439::get_offset_of_distance_32(),
	VRCameraClamper_t2660686439::get_offset_of_t1Pos_33(),
	VRCameraClamper_t2660686439::get_offset_of_t2Pos_34(),
	VRCameraClamper_t2660686439::get_offset_of_posibleRotation_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (VRModeManager_t3340808238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[4] = 
{
	VRModeManager_t3340808238::get_offset_of_blackQuad_2(),
	VRModeManager_t3340808238::get_offset_of_vrContainer_3(),
	VRModeManager_t3340808238::get_offset_of_loadSkybox_4(),
	VRModeManager_t3340808238::get_offset_of_returnToGlossaryBtn_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (CardboardOnGUI_t2062908102), -1, sizeof(CardboardOnGUI_t2062908102_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2293[5] = 
{
	CardboardOnGUI_t2062908102_StaticFields::get_offset_of_okToDraw_2(),
	CardboardOnGUI_t2062908102_StaticFields::get_offset_of_isGUIVisible_3(),
	CardboardOnGUI_t2062908102::get_offset_of_background_4(),
	CardboardOnGUI_t2062908102::get_offset_of_guiScreen_5(),
	CardboardOnGUI_t2062908102_StaticFields::get_offset_of_onGUICallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (OnGUICallback_t3287452568), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (CardboardOnGUIMouse_t2612501887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[6] = 
{
	CardboardOnGUIMouse_t2612501887::get_offset_of_pointerImage_2(),
	CardboardOnGUIMouse_t2612501887::get_offset_of_pointerSize_3(),
	CardboardOnGUIMouse_t2612501887::get_offset_of_pointerSpot_4(),
	CardboardOnGUIMouse_t2612501887::get_offset_of_pointerVisible_5(),
	CardboardOnGUIMouse_t2612501887::get_offset_of_pointerX_6(),
	CardboardOnGUIMouse_t2612501887::get_offset_of_pointerY_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (CardboardOnGUIWindow_t3958675030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[2] = 
{
	CardboardOnGUIWindow_t3958675030::get_offset_of_meshRenderer_2(),
	CardboardOnGUIWindow_t3958675030::get_offset_of_rect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (SkyboxMesh_t1243902519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (StereoLensFlare_t786564152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (Cardboard_t1761541558), -1, sizeof(Cardboard_t1761541558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2299[30] = 
{
	Cardboard_t1761541558_StaticFields::get_offset_of_sdk_2(),
	Cardboard_t1761541558_StaticFields::get_offset_of_currentMainCamera_3(),
	Cardboard_t1761541558_StaticFields::get_offset_of_currentController_4(),
	Cardboard_t1761541558::get_offset_of_vrModeEnabled_5(),
	Cardboard_t1761541558::get_offset_of_distortionCorrection_6(),
	Cardboard_t1761541558::get_offset_of_enableAlignmentMarker_7(),
	Cardboard_t1761541558::get_offset_of_enableSettingsButton_8(),
	Cardboard_t1761541558::get_offset_of_backButtonMode_9(),
	Cardboard_t1761541558::get_offset_of_tapIsTrigger_10(),
	Cardboard_t1761541558::get_offset_of_neckModelScale_11(),
	Cardboard_t1761541558::get_offset_of_autoDriftCorrection_12(),
	Cardboard_t1761541558::get_offset_of_electronicDisplayStabilization_13(),
	Cardboard_t1761541558::get_offset_of_syncWithCardboardApp_14(),
	Cardboard_t1761541558_StaticFields::get_offset_of_device_15(),
	Cardboard_t1761541558::get_offset_of_stereoScreenScale_16(),
	Cardboard_t1761541558_StaticFields::get_offset_of_stereoScreen_17(),
	Cardboard_t1761541558::get_offset_of_defaultComfortableViewingRange_18(),
	Cardboard_t1761541558::get_offset_of_DefaultDeviceProfile_19(),
	Cardboard_t1761541558::get_offset_of_updated_20(),
	Cardboard_t1761541558::get_offset_of_OnStereoScreenChanged_21(),
	Cardboard_t1761541558::get_offset_of_OnTrigger_22(),
	Cardboard_t1761541558::get_offset_of_OnTilt_23(),
	Cardboard_t1761541558::get_offset_of_OnProfileChange_24(),
	Cardboard_t1761541558::get_offset_of_OnBackButton_25(),
	Cardboard_t1761541558::get_offset_of_U3CNativeDistortionCorrectionSupportedU3Ek__BackingField_26(),
	Cardboard_t1761541558::get_offset_of_U3CNativeUILayerSupportedU3Ek__BackingField_27(),
	Cardboard_t1761541558::get_offset_of_U3CTriggeredU3Ek__BackingField_28(),
	Cardboard_t1761541558::get_offset_of_U3CTiltedU3Ek__BackingField_29(),
	Cardboard_t1761541558::get_offset_of_U3CProfileChangedU3Ek__BackingField_30(),
	Cardboard_t1761541558::get_offset_of_U3CBackButtonPressedU3Ek__BackingField_31(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
