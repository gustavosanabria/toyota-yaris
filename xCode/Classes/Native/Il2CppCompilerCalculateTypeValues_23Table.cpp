﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMet475334837.h"
#include "AssemblyU2DCSharp_Cardboard_BackButtonModes2097296702.h"
#include "AssemblyU2DCSharp_Cardboard_Eye2975048248.h"
#include "AssemblyU2DCSharp_Cardboard_Distortion619549366.h"
#include "AssemblyU2DCSharp_Cardboard_StereoScreenChangeDeleg862778642.h"
#include "AssemblyU2DCSharp_Cardboard_U3CEndOfFrameU3Ec__Ite3419169349.h"
#include "AssemblyU2DCSharp_CardboardEye2174202011.h"
#include "AssemblyU2DCSharp_CardboardHead2975823030.h"
#include "AssemblyU2DCSharp_CardboardPostRender2005367980.h"
#include "AssemblyU2DCSharp_CardboardPreRender502417667.h"
#include "AssemblyU2DCSharp_CardboardProfile3514264339.h"
#include "AssemblyU2DCSharp_CardboardProfile_Screen128980744.h"
#include "AssemblyU2DCSharp_CardboardProfile_Lenses4225285320.h"
#include "AssemblyU2DCSharp_CardboardProfile_MaxFOV4250474341.h"
#include "AssemblyU2DCSharp_CardboardProfile_Distortion2959612217.h"
#include "AssemblyU2DCSharp_CardboardProfile_Device3996480754.h"
#include "AssemblyU2DCSharp_CardboardProfile_ScreenSizes2553298538.h"
#include "AssemblyU2DCSharp_CardboardProfile_DeviceTypes1054882471.h"
#include "AssemblyU2DCSharp_GazeInputModule2064533489.h"
#include "AssemblyU2DCSharp_Pose3D2396367586.h"
#include "AssemblyU2DCSharp_MutablePose3D1273683304.h"
#include "AssemblyU2DCSharp_RadialUndistortionEffect3929131462.h"
#include "AssemblyU2DCSharp_StereoController1637909972.h"
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU33320551400.h"
#include "AssemblyU2DCSharp_StereoRenderEffect4012593919.h"
#include "AssemblyU2DCSharp_BaseCardboardDevice2358938779.h"
#include "AssemblyU2DCSharp_BaseCardboardDevice_VREventCallb2063132207.h"
#include "AssemblyU2DCSharp_BaseVRDevice4087747107.h"
#include "AssemblyU2DCSharp_BaseVRDevice_DisplayMetrics2172228397.h"
#include "AssemblyU2DCSharp_CardboardiOSDevice2496114637.h"
#include "AssemblyU2DCSharp_DownloadObbExample1827715139.h"
#include "AssemblyU2DCSharp_MedaiPlayerSampleGUI1334456284.h"
#include "AssemblyU2DCSharp_MedaiPlayerSampleSphereGUI3177899023.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl3572035536.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_ERRO1475309647.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT1488282328.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIA_SCALE4148698416.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoEnd3177907679.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoReady259270055.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoError247668236.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoFirstFrameR2520860170.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CDownloadStrea2578073446.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CCopyStreamingAs96045604.h"
#include "AssemblyU2DCSharp_MediaPlayerFullScreenCtrl326609931.h"
#include "AssemblyU2DCSharp_SphereMirror167396876.h"
#include "AssemblyU2DCSharp_VideoCopyTexture908209291.h"
#include "AssemblyU2DCSharp_LanguageSelection764444916.h"
#include "AssemblyU2DCSharp_TypewriterEffect2948707390.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry2858472101.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "AssemblyU2DCSharp_UIButtonActivate4170568185.h"
#include "AssemblyU2DCSharp_UIButtonColor1546108957.h"
#include "AssemblyU2DCSharp_UIButtonColor_State1650987007.h"
#include "AssemblyU2DCSharp_UIButtonKeys2405408250.h"
#include "AssemblyU2DCSharp_UIButtonMessage4052372033.h"
#include "AssemblyU2DCSharp_UIButtonMessage_Trigger1801692106.h"
#include "AssemblyU2DCSharp_UIButtonOffset1019800345.h"
#include "AssemblyU2DCSharp_UIButtonRotation1491274884.h"
#include "AssemblyU2DCSharp_UIButtonScale1560517124.h"
#include "AssemblyU2DCSharp_UICenterOnChild854454836.h"
#include "AssemblyU2DCSharp_UICenterOnChild_OnCenterCallback2672694804.h"
#include "AssemblyU2DCSharp_UICenterOnClick854573728.h"
#include "AssemblyU2DCSharp_UIDragCamera167830317.h"
#include "AssemblyU2DCSharp_UIDragDropContainer3116818762.h"
#include "AssemblyU2DCSharp_UIDragDropItem2087865514.h"
#include "AssemblyU2DCSharp_UIDragDropItem_Restriction442348999.h"
#include "AssemblyU2DCSharp_UIDragDropItem_U3CEnableDragScro1710221838.h"
#include "AssemblyU2DCSharp_UIDragDropRoot2088129145.h"
#include "AssemblyU2DCSharp_UIDragObject512213831.h"
#include "AssemblyU2DCSharp_UIDragObject_DragEffect718078925.h"
#include "AssemblyU2DCSharp_UIDragResize601144508.h"
#include "AssemblyU2DCSharp_UIDragScrollView123487002.h"
#include "AssemblyU2DCSharp_UIDraggableCamera1776763358.h"
#include "AssemblyU2DCSharp_UIEventTrigger46291570.h"
#include "AssemblyU2DCSharp_UIForwardEvents1048473098.h"
#include "AssemblyU2DCSharp_UIGrid2503122938.h"
#include "AssemblyU2DCSharp_UIGrid_Arrangement2060582997.h"
#include "AssemblyU2DCSharp_UIGrid_Sorting2346397775.h"
#include "AssemblyU2DCSharp_UIGrid_OnReposition1310478256.h"
#include "AssemblyU2DCSharp_UIImageButton726205753.h"
#include "AssemblyU2DCSharp_UIKeyBinding2313833690.h"
#include "AssemblyU2DCSharp_UIKeyBinding_Action3569464619.h"
#include "AssemblyU2DCSharp_UIKeyBinding_Modifier3912257676.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1837256607.h"
#include "AssemblyU2DCSharp_UIKeyNavigation_Constraint3356609133.h"
#include "AssemblyU2DCSharp_UIPlayAnimation2708996220.h"
#include "AssemblyU2DCSharp_UIPlaySound532605447.h"
#include "AssemblyU2DCSharp_UIPlaySound_Trigger1400315280.h"
#include "AssemblyU2DCSharp_UIPlayTween533751651.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942.h"
#include "AssemblyU2DCSharp_UIPopupList_Position2071232514.h"
#include "AssemblyU2DCSharp_UIPopupList_OpenOn1064537826.h"
#include "AssemblyU2DCSharp_UIPopupList_LegacyEvent524649176.h"
#include "AssemblyU2DCSharp_UIPopupList_U3CUpdateTweenPositio989198279.h"
#include "AssemblyU2DCSharp_UIPopupList_U3CCloseIfUnselectedU777622634.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"
#include "AssemblyU2DCSharp_UIProgressBar_FillDirection1321036095.h"
#include "AssemblyU2DCSharp_UIProgressBar_OnDragFinished4207031714.h"
#include "AssemblyU2DCSharp_UISavedOption3282279080.h"
#include "AssemblyU2DCSharp_UIScrollBar2839103954.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (DistortionCorrectionMethod_t475334837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	DistortionCorrectionMethod_t475334837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (BackButtonModes_t2097296702)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	BackButtonModes_t2097296702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (Eye_t2975048248)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[4] = 
{
	Eye_t2975048248::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (Distortion_t619549366)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2303[3] = 
{
	Distortion_t619549366::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (StereoScreenChangeDelegate_t862778642), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CEndOfFrameU3Ec__Iterator6_t3419169349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[3] = 
{
	U3CEndOfFrameU3Ec__Iterator6_t3419169349::get_offset_of_U24PC_0(),
	U3CEndOfFrameU3Ec__Iterator6_t3419169349::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator6_t3419169349::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (CardboardEye_t2174202011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[10] = 
{
	CardboardEye_t2174202011::get_offset_of_eye_2(),
	CardboardEye_t2174202011::get_offset_of_toggleCullingMask_3(),
	CardboardEye_t2174202011::get_offset_of_controller_4(),
	CardboardEye_t2174202011::get_offset_of_stereoEffect_5(),
	CardboardEye_t2174202011::get_offset_of_monoCamera_6(),
	CardboardEye_t2174202011::get_offset_of_realProj_7(),
	CardboardEye_t2174202011::get_offset_of_projvec_8(),
	CardboardEye_t2174202011::get_offset_of_unprojvec_9(),
	CardboardEye_t2174202011::get_offset_of_interpPosition_10(),
	CardboardEye_t2174202011::get_offset_of_U3CcameraU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (CardboardHead_t2975823030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[5] = 
{
	CardboardHead_t2975823030::get_offset_of_trackRotation_2(),
	CardboardHead_t2975823030::get_offset_of_trackPosition_3(),
	CardboardHead_t2975823030::get_offset_of_target_4(),
	CardboardHead_t2975823030::get_offset_of_updateEarly_5(),
	CardboardHead_t2975823030::get_offset_of_updated_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (CardboardPostRender_t2005367980), -1, sizeof(CardboardPostRender_t2005367980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	CardboardPostRender_t2005367980::get_offset_of_distortionMesh_14(),
	CardboardPostRender_t2005367980::get_offset_of_meshMaterial_15(),
	CardboardPostRender_t2005367980::get_offset_of_uiMaterial_16(),
	CardboardPostRender_t2005367980::get_offset_of_centerWidthPx_17(),
	CardboardPostRender_t2005367980::get_offset_of_buttonWidthPx_18(),
	CardboardPostRender_t2005367980::get_offset_of_xScale_19(),
	CardboardPostRender_t2005367980::get_offset_of_yScale_20(),
	CardboardPostRender_t2005367980::get_offset_of_xfm_21(),
	CardboardPostRender_t2005367980_StaticFields::get_offset_of_Angles_22(),
	CardboardPostRender_t2005367980::get_offset_of_U3CcameraU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (CardboardPreRender_t502417667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[1] = 
{
	CardboardPreRender_t502417667::get_offset_of_U3CcameraU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (CardboardProfile_t3514264339), -1, sizeof(CardboardProfile_t3514264339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2310[15] = 
{
	CardboardProfile_t3514264339::get_offset_of_screen_0(),
	CardboardProfile_t3514264339::get_offset_of_device_1(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_Nexus5_2(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_Nexus6_3(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_GalaxyS6_4(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_GalaxyNote4_5(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_LGG3_6(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_iPhone4_7(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_iPhone5_8(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_iPhone6_9(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_iPhone6p_10(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_CardboardJun2014_11(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_CardboardMay2015_12(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_GoggleTechC1Glass_13(),
	CardboardProfile_t3514264339_StaticFields::get_offset_of_Default_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (Screen_t128980744)+ sizeof (Il2CppObject), sizeof(Screen_t128980744_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2311[3] = 
{
	Screen_t128980744::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t128980744::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t128980744::get_offset_of_border_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (Lenses_t4225285320)+ sizeof (Il2CppObject), sizeof(Lenses_t4225285320_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2312[7] = 
{
	0,
	0,
	0,
	Lenses_t4225285320::get_offset_of_separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t4225285320::get_offset_of_offset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t4225285320::get_offset_of_screenDistance_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t4225285320::get_offset_of_alignment_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (MaxFOV_t4250474341)+ sizeof (Il2CppObject), sizeof(MaxFOV_t4250474341_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[4] = 
{
	MaxFOV_t4250474341::get_offset_of_outer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t4250474341::get_offset_of_inner_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t4250474341::get_offset_of_upper_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t4250474341::get_offset_of_lower_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (Distortion_t2959612217)+ sizeof (Il2CppObject), sizeof(Distortion_t2959612217_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2314[2] = 
{
	Distortion_t2959612217::get_offset_of_k1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Distortion_t2959612217::get_offset_of_k2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (Device_t3996480754)+ sizeof (Il2CppObject), sizeof(Device_t3996480754_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[4] = 
{
	Device_t3996480754::get_offset_of_lenses_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Device_t3996480754::get_offset_of_maxFOV_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Device_t3996480754::get_offset_of_distortion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Device_t3996480754::get_offset_of_inverse_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ScreenSizes_t2553298538)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2316[10] = 
{
	ScreenSizes_t2553298538::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (DeviceTypes_t1054882471)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	DeviceTypes_t1054882471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (GazeInputModule_t2064533489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[8] = 
{
	GazeInputModule_t2064533489::get_offset_of_vrModeOnly_6(),
	GazeInputModule_t2064533489::get_offset_of_cursor_7(),
	GazeInputModule_t2064533489::get_offset_of_showCursor_8(),
	GazeInputModule_t2064533489::get_offset_of_scaleCursorSize_9(),
	GazeInputModule_t2064533489::get_offset_of_clickTime_10(),
	GazeInputModule_t2064533489::get_offset_of_hotspot_11(),
	GazeInputModule_t2064533489::get_offset_of_pointerData_12(),
	GazeInputModule_t2064533489::get_offset_of_lastHeadPose_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (Pose3D_t2396367586), -1, sizeof(Pose3D_t2396367586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2319[4] = 
{
	Pose3D_t2396367586_StaticFields::get_offset_of_flipZ_0(),
	Pose3D_t2396367586::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t2396367586::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t2396367586::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (MutablePose3D_t1273683304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (RadialUndistortionEffect_t3929131462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[1] = 
{
	RadialUndistortionEffect_t3929131462::get_offset_of_material_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (StereoController_t1637909972), -1, sizeof(StereoController_t1637909972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2322[17] = 
{
	StereoController_t1637909972::get_offset_of_directRender_2(),
	StereoController_t1637909972::get_offset_of_keepStereoUpdated_3(),
	StereoController_t1637909972::get_offset_of_stereoMultiplier_4(),
	StereoController_t1637909972::get_offset_of_matchMonoFOV_5(),
	StereoController_t1637909972::get_offset_of_matchByZoom_6(),
	StereoController_t1637909972::get_offset_of_centerOfInterest_7(),
	StereoController_t1637909972::get_offset_of_radiusOfInterest_8(),
	StereoController_t1637909972::get_offset_of_checkStereoComfort_9(),
	StereoController_t1637909972::get_offset_of_stereoAdjustSmoothing_10(),
	StereoController_t1637909972::get_offset_of_screenParallax_11(),
	StereoController_t1637909972::get_offset_of_stereoPaddingX_12(),
	StereoController_t1637909972::get_offset_of_stereoPaddingY_13(),
	StereoController_t1637909972::get_offset_of_renderedStereo_14(),
	StereoController_t1637909972::get_offset_of_eyes_15(),
	StereoController_t1637909972::get_offset_of_head_16(),
	StereoController_t1637909972::get_offset_of_U3CcameraU3Ek__BackingField_17(),
	StereoController_t1637909972_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (U3CEndOfFrameU3Ec__Iterator7_t3320551400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[3] = 
{
	U3CEndOfFrameU3Ec__Iterator7_t3320551400::get_offset_of_U24PC_0(),
	U3CEndOfFrameU3Ec__Iterator7_t3320551400::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator7_t3320551400::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (StereoRenderEffect_t4012593919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[2] = 
{
	StereoRenderEffect_t4012593919::get_offset_of_material_2(),
	StereoRenderEffect_t4012593919::get_offset_of_camera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (BaseCardboardDevice_t2358938779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	BaseCardboardDevice_t2358938779::get_offset_of_headData_24(),
	BaseCardboardDevice_t2358938779::get_offset_of_viewData_25(),
	BaseCardboardDevice_t2358938779::get_offset_of_profileData_26(),
	BaseCardboardDevice_t2358938779::get_offset_of_headView_27(),
	BaseCardboardDevice_t2358938779::get_offset_of_leftEyeView_28(),
	BaseCardboardDevice_t2358938779::get_offset_of_rightEyeView_29(),
	BaseCardboardDevice_t2358938779::get_offset_of_eventQueue_30(),
	BaseCardboardDevice_t2358938779::get_offset_of_debugDisableNativeProjections_31(),
	BaseCardboardDevice_t2358938779::get_offset_of_debugDisableNativeDistortion_32(),
	BaseCardboardDevice_t2358938779::get_offset_of_debugDisableNativeUILayer_33(),
	BaseCardboardDevice_t2358938779::get_offset_of_events_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (VREventCallback_t2063132207), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (BaseVRDevice_t4087747107), -1, sizeof(BaseVRDevice_t4087747107_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2327[18] = 
{
	BaseVRDevice_t4087747107_StaticFields::get_offset_of_device_0(),
	BaseVRDevice_t4087747107::get_offset_of_headPose_1(),
	BaseVRDevice_t4087747107::get_offset_of_leftEyePose_2(),
	BaseVRDevice_t4087747107::get_offset_of_rightEyePose_3(),
	BaseVRDevice_t4087747107::get_offset_of_leftEyeDistortedProjection_4(),
	BaseVRDevice_t4087747107::get_offset_of_rightEyeDistortedProjection_5(),
	BaseVRDevice_t4087747107::get_offset_of_leftEyeUndistortedProjection_6(),
	BaseVRDevice_t4087747107::get_offset_of_rightEyeUndistortedProjection_7(),
	BaseVRDevice_t4087747107::get_offset_of_leftEyeDistortedViewport_8(),
	BaseVRDevice_t4087747107::get_offset_of_rightEyeDistortedViewport_9(),
	BaseVRDevice_t4087747107::get_offset_of_leftEyeUndistortedViewport_10(),
	BaseVRDevice_t4087747107::get_offset_of_rightEyeUndistortedViewport_11(),
	BaseVRDevice_t4087747107::get_offset_of_recommendedTextureSize_12(),
	BaseVRDevice_t4087747107::get_offset_of_triggered_13(),
	BaseVRDevice_t4087747107::get_offset_of_tilted_14(),
	BaseVRDevice_t4087747107::get_offset_of_profileChanged_15(),
	BaseVRDevice_t4087747107::get_offset_of_backButtonPressed_16(),
	BaseVRDevice_t4087747107::get_offset_of_U3CProfileU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (DisplayMetrics_t2172228397)+ sizeof (Il2CppObject), sizeof(DisplayMetrics_t2172228397_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2328[4] = 
{
	DisplayMetrics_t2172228397::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DisplayMetrics_t2172228397::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DisplayMetrics_t2172228397::get_offset_of_xdpi_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DisplayMetrics_t2172228397::get_offset_of_ydpi_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (CardboardiOSDevice_t2496114637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[2] = 
{
	CardboardiOSDevice_t2496114637::get_offset_of_isOpenGL_35(),
	CardboardiOSDevice_t2496114637::get_offset_of_debugOnboarding_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (DownloadObbExample_t1827715139), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (MedaiPlayerSampleGUI_t1334456284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[3] = 
{
	MedaiPlayerSampleGUI_t1334456284::get_offset_of_loadingGO_2(),
	MedaiPlayerSampleGUI_t1334456284::get_offset_of_scrMedia_3(),
	MedaiPlayerSampleGUI_t1334456284::get_offset_of_m_bFinish_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (MedaiPlayerSampleSphereGUI_t3177899023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[1] = 
{
	MedaiPlayerSampleSphereGUI_t3177899023::get_offset_of_scrMedia_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (MediaPlayerCtrl_t3572035536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[30] = 
{
	MediaPlayerCtrl_t3572035536::get_offset_of_m_strFileName_2(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_TargetMaterial_3(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_VideoTexture_4(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_VideoTextureDummy_5(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_CurrentState_6(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iCurrentSeekPosition_7(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_fVolume_8(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bFullScreen_9(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bSupportRockchip_10(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnReady_11(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnEnd_12(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnVideoError_13(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnVideoFirstFrameReady_14(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iPauseFrame_15(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iAndroidMgrID_16(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bIsFirstFrameReady_17(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bFirst_18(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_ScaleValue_19(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_objResize_20(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bLoop_21(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bAutoPlay_22(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bStop_23(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bInit_24(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bCheckFBO_25(),
	MediaPlayerCtrl_t3572035536::get_offset_of_isHelpVideo_26(),
	MediaPlayerCtrl_t3572035536::get_offset_of_terminateOnClick_27(),
	MediaPlayerCtrl_t3572035536::get_offset_of_hSM_28(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bPause_29(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iID_30(),
	MediaPlayerCtrl_t3572035536::get_offset_of__videoTexture_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (MEDIAPLAYER_ERROR_t1475309647)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[8] = 
{
	MEDIAPLAYER_ERROR_t1475309647::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (MEDIAPLAYER_STATE_t1488282328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2335[8] = 
{
	MEDIAPLAYER_STATE_t1488282328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (MEDIA_SCALE_t4148698416)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2336[8] = 
{
	MEDIA_SCALE_t4148698416::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (VideoEnd_t3177907679), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (VideoReady_t259270055), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (VideoError_t247668236), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (VideoFirstFrameReady_t2520860170), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[7] = 
{
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_strURL_0(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_U3CwwwU3E__0_1(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_U3Cwrite_pathU3E__1_2(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_U24PC_3(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_U24current_4(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_U3CU24U3EstrURL_5(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator8_t2578073446::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[7] = 
{
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_strURL_0(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_U3Cwrite_pathU3E__0_1(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_U3CwwwU3E__1_2(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_U24PC_3(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_U24current_4(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_U3CU24U3EstrURL_5(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator9_t96045604::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (MediaPlayerFullScreenCtrl_t326609931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[3] = 
{
	MediaPlayerFullScreenCtrl_t326609931::get_offset_of_m_objVideo_2(),
	MediaPlayerFullScreenCtrl_t326609931::get_offset_of_m_iOrgWidth_3(),
	MediaPlayerFullScreenCtrl_t326609931::get_offset_of_m_iOrgHeight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (SphereMirror_t167396876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (VideoCopyTexture_t908209291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[1] = 
{
	VideoCopyTexture_t908209291::get_offset_of_m_srcVideo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (LanguageSelection_t764444916), -1, sizeof(LanguageSelection_t764444916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	LanguageSelection_t764444916::get_offset_of_mList_2(),
	LanguageSelection_t764444916_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (TypewriterEffect_t2948707390), -1, sizeof(TypewriterEffect_t2948707390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2347[15] = 
{
	TypewriterEffect_t2948707390_StaticFields::get_offset_of_current_2(),
	TypewriterEffect_t2948707390::get_offset_of_charsPerSecond_3(),
	TypewriterEffect_t2948707390::get_offset_of_fadeInTime_4(),
	TypewriterEffect_t2948707390::get_offset_of_delayOnPeriod_5(),
	TypewriterEffect_t2948707390::get_offset_of_delayOnNewLine_6(),
	TypewriterEffect_t2948707390::get_offset_of_scrollView_7(),
	TypewriterEffect_t2948707390::get_offset_of_keepFullDimensions_8(),
	TypewriterEffect_t2948707390::get_offset_of_onFinished_9(),
	TypewriterEffect_t2948707390::get_offset_of_mLabel_10(),
	TypewriterEffect_t2948707390::get_offset_of_mFullText_11(),
	TypewriterEffect_t2948707390::get_offset_of_mCurrentOffset_12(),
	TypewriterEffect_t2948707390::get_offset_of_mNextChar_13(),
	TypewriterEffect_t2948707390::get_offset_of_mReset_14(),
	TypewriterEffect_t2948707390::get_offset_of_mActive_15(),
	TypewriterEffect_t2948707390::get_offset_of_mFade_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (FadeEntry_t2858472101)+ sizeof (Il2CppObject), sizeof(FadeEntry_t2858472101_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2348[3] = 
{
	FadeEntry_t2858472101::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FadeEntry_t2858472101::get_offset_of_text_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FadeEntry_t2858472101::get_offset_of_alpha_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (UIButton_t179429094), -1, sizeof(UIButton_t179429094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2349[14] = 
{
	UIButton_t179429094_StaticFields::get_offset_of_current_12(),
	UIButton_t179429094::get_offset_of_dragHighlight_13(),
	UIButton_t179429094::get_offset_of_hoverSprite_14(),
	UIButton_t179429094::get_offset_of_pressedSprite_15(),
	UIButton_t179429094::get_offset_of_disabledSprite_16(),
	UIButton_t179429094::get_offset_of_hoverSprite2D_17(),
	UIButton_t179429094::get_offset_of_pressedSprite2D_18(),
	UIButton_t179429094::get_offset_of_disabledSprite2D_19(),
	UIButton_t179429094::get_offset_of_pixelSnap_20(),
	UIButton_t179429094::get_offset_of_onClick_21(),
	UIButton_t179429094::get_offset_of_mSprite_22(),
	UIButton_t179429094::get_offset_of_mSprite2D_23(),
	UIButton_t179429094::get_offset_of_mNormalSprite_24(),
	UIButton_t179429094::get_offset_of_mNormalSprite2D_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (UIButtonActivate_t4170568185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[2] = 
{
	UIButtonActivate_t4170568185::get_offset_of_target_2(),
	UIButtonActivate_t4170568185::get_offset_of_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (UIButtonColor_t1546108957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[10] = 
{
	UIButtonColor_t1546108957::get_offset_of_tweenTarget_2(),
	UIButtonColor_t1546108957::get_offset_of_hover_3(),
	UIButtonColor_t1546108957::get_offset_of_pressed_4(),
	UIButtonColor_t1546108957::get_offset_of_disabledColor_5(),
	UIButtonColor_t1546108957::get_offset_of_duration_6(),
	UIButtonColor_t1546108957::get_offset_of_mStartingColor_7(),
	UIButtonColor_t1546108957::get_offset_of_mDefaultColor_8(),
	UIButtonColor_t1546108957::get_offset_of_mInitDone_9(),
	UIButtonColor_t1546108957::get_offset_of_mWidget_10(),
	UIButtonColor_t1546108957::get_offset_of_mState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (State_t1650987007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[5] = 
{
	State_t1650987007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (UIButtonKeys_t2405408250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[5] = 
{
	UIButtonKeys_t2405408250::get_offset_of_selectOnClick_12(),
	UIButtonKeys_t2405408250::get_offset_of_selectOnUp_13(),
	UIButtonKeys_t2405408250::get_offset_of_selectOnDown_14(),
	UIButtonKeys_t2405408250::get_offset_of_selectOnLeft_15(),
	UIButtonKeys_t2405408250::get_offset_of_selectOnRight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (UIButtonMessage_t4052372033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	UIButtonMessage_t4052372033::get_offset_of_target_2(),
	UIButtonMessage_t4052372033::get_offset_of_functionName_3(),
	UIButtonMessage_t4052372033::get_offset_of_trigger_4(),
	UIButtonMessage_t4052372033::get_offset_of_includeChildren_5(),
	UIButtonMessage_t4052372033::get_offset_of_mStarted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (Trigger_t1801692106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2355[7] = 
{
	Trigger_t1801692106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (UIButtonOffset_t1019800345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[7] = 
{
	UIButtonOffset_t1019800345::get_offset_of_tweenTarget_2(),
	UIButtonOffset_t1019800345::get_offset_of_hover_3(),
	UIButtonOffset_t1019800345::get_offset_of_pressed_4(),
	UIButtonOffset_t1019800345::get_offset_of_duration_5(),
	UIButtonOffset_t1019800345::get_offset_of_mPos_6(),
	UIButtonOffset_t1019800345::get_offset_of_mStarted_7(),
	UIButtonOffset_t1019800345::get_offset_of_mPressed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (UIButtonRotation_t1491274884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[6] = 
{
	UIButtonRotation_t1491274884::get_offset_of_tweenTarget_2(),
	UIButtonRotation_t1491274884::get_offset_of_hover_3(),
	UIButtonRotation_t1491274884::get_offset_of_pressed_4(),
	UIButtonRotation_t1491274884::get_offset_of_duration_5(),
	UIButtonRotation_t1491274884::get_offset_of_mRot_6(),
	UIButtonRotation_t1491274884::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (UIButtonScale_t1560517124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[6] = 
{
	UIButtonScale_t1560517124::get_offset_of_tweenTarget_2(),
	UIButtonScale_t1560517124::get_offset_of_hover_3(),
	UIButtonScale_t1560517124::get_offset_of_pressed_4(),
	UIButtonScale_t1560517124::get_offset_of_duration_5(),
	UIButtonScale_t1560517124::get_offset_of_mScale_6(),
	UIButtonScale_t1560517124::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (UICenterOnChild_t854454836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[6] = 
{
	UICenterOnChild_t854454836::get_offset_of_springStrength_2(),
	UICenterOnChild_t854454836::get_offset_of_nextPageThreshold_3(),
	UICenterOnChild_t854454836::get_offset_of_onFinished_4(),
	UICenterOnChild_t854454836::get_offset_of_onCenter_5(),
	UICenterOnChild_t854454836::get_offset_of_mScrollView_6(),
	UICenterOnChild_t854454836::get_offset_of_mCenteredObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (OnCenterCallback_t2672694804), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (UICenterOnClick_t854573728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (UIDragCamera_t167830317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[1] = 
{
	UIDragCamera_t167830317::get_offset_of_draggableCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (UIDragDropContainer_t3116818762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[1] = 
{
	UIDragDropContainer_t3116818762::get_offset_of_reparentTarget_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (UIDragDropItem_t2087865514), -1, sizeof(UIDragDropItem_t2087865514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2364[18] = 
{
	UIDragDropItem_t2087865514::get_offset_of_restriction_2(),
	UIDragDropItem_t2087865514::get_offset_of_cloneOnDrag_3(),
	UIDragDropItem_t2087865514::get_offset_of_pressAndHoldDelay_4(),
	UIDragDropItem_t2087865514::get_offset_of_interactable_5(),
	UIDragDropItem_t2087865514::get_offset_of_mTrans_6(),
	UIDragDropItem_t2087865514::get_offset_of_mParent_7(),
	UIDragDropItem_t2087865514::get_offset_of_mCollider_8(),
	UIDragDropItem_t2087865514::get_offset_of_mCollider2D_9(),
	UIDragDropItem_t2087865514::get_offset_of_mButton_10(),
	UIDragDropItem_t2087865514::get_offset_of_mRoot_11(),
	UIDragDropItem_t2087865514::get_offset_of_mGrid_12(),
	UIDragDropItem_t2087865514::get_offset_of_mTable_13(),
	UIDragDropItem_t2087865514::get_offset_of_mDragStartTime_14(),
	UIDragDropItem_t2087865514::get_offset_of_mDragScrollView_15(),
	UIDragDropItem_t2087865514::get_offset_of_mPressed_16(),
	UIDragDropItem_t2087865514::get_offset_of_mDragging_17(),
	UIDragDropItem_t2087865514::get_offset_of_mTouch_18(),
	UIDragDropItem_t2087865514_StaticFields::get_offset_of_draggedItems_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (Restriction_t442348999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2365[5] = 
{
	Restriction_t442348999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (U3CEnableDragScrollViewU3Ec__IteratorA_t1710221838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[3] = 
{
	U3CEnableDragScrollViewU3Ec__IteratorA_t1710221838::get_offset_of_U24PC_0(),
	U3CEnableDragScrollViewU3Ec__IteratorA_t1710221838::get_offset_of_U24current_1(),
	U3CEnableDragScrollViewU3Ec__IteratorA_t1710221838::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (UIDragDropRoot_t2088129145), -1, sizeof(UIDragDropRoot_t2088129145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[1] = 
{
	UIDragDropRoot_t2088129145_StaticFields::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (UIDragObject_t512213831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[18] = 
{
	UIDragObject_t512213831::get_offset_of_target_2(),
	UIDragObject_t512213831::get_offset_of_panelRegion_3(),
	UIDragObject_t512213831::get_offset_of_scrollMomentum_4(),
	UIDragObject_t512213831::get_offset_of_restrictWithinPanel_5(),
	UIDragObject_t512213831::get_offset_of_contentRect_6(),
	UIDragObject_t512213831::get_offset_of_dragEffect_7(),
	UIDragObject_t512213831::get_offset_of_momentumAmount_8(),
	UIDragObject_t512213831::get_offset_of_scale_9(),
	UIDragObject_t512213831::get_offset_of_scrollWheelFactor_10(),
	UIDragObject_t512213831::get_offset_of_mPlane_11(),
	UIDragObject_t512213831::get_offset_of_mTargetPos_12(),
	UIDragObject_t512213831::get_offset_of_mLastPos_13(),
	UIDragObject_t512213831::get_offset_of_mMomentum_14(),
	UIDragObject_t512213831::get_offset_of_mScroll_15(),
	UIDragObject_t512213831::get_offset_of_mBounds_16(),
	UIDragObject_t512213831::get_offset_of_mTouchID_17(),
	UIDragObject_t512213831::get_offset_of_mStarted_18(),
	UIDragObject_t512213831::get_offset_of_mPressed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (DragEffect_t718078925)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	DragEffect_t718078925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (UIDragResize_t601144508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[13] = 
{
	UIDragResize_t601144508::get_offset_of_target_2(),
	UIDragResize_t601144508::get_offset_of_pivot_3(),
	UIDragResize_t601144508::get_offset_of_minWidth_4(),
	UIDragResize_t601144508::get_offset_of_minHeight_5(),
	UIDragResize_t601144508::get_offset_of_maxWidth_6(),
	UIDragResize_t601144508::get_offset_of_maxHeight_7(),
	UIDragResize_t601144508::get_offset_of_updateAnchors_8(),
	UIDragResize_t601144508::get_offset_of_mPlane_9(),
	UIDragResize_t601144508::get_offset_of_mRayPos_10(),
	UIDragResize_t601144508::get_offset_of_mLocalPos_11(),
	UIDragResize_t601144508::get_offset_of_mWidth_12(),
	UIDragResize_t601144508::get_offset_of_mHeight_13(),
	UIDragResize_t601144508::get_offset_of_mDragging_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (UIDragScrollView_t123487002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[6] = 
{
	UIDragScrollView_t123487002::get_offset_of_scrollView_2(),
	UIDragScrollView_t123487002::get_offset_of_draggablePanel_3(),
	UIDragScrollView_t123487002::get_offset_of_mTrans_4(),
	UIDragScrollView_t123487002::get_offset_of_mScroll_5(),
	UIDragScrollView_t123487002::get_offset_of_mAutoFind_6(),
	UIDragScrollView_t123487002::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (UIDraggableCamera_t1776763358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[14] = 
{
	UIDraggableCamera_t1776763358::get_offset_of_rootForBounds_2(),
	UIDraggableCamera_t1776763358::get_offset_of_scale_3(),
	UIDraggableCamera_t1776763358::get_offset_of_scrollWheelFactor_4(),
	UIDraggableCamera_t1776763358::get_offset_of_dragEffect_5(),
	UIDraggableCamera_t1776763358::get_offset_of_smoothDragStart_6(),
	UIDraggableCamera_t1776763358::get_offset_of_momentumAmount_7(),
	UIDraggableCamera_t1776763358::get_offset_of_mCam_8(),
	UIDraggableCamera_t1776763358::get_offset_of_mTrans_9(),
	UIDraggableCamera_t1776763358::get_offset_of_mPressed_10(),
	UIDraggableCamera_t1776763358::get_offset_of_mMomentum_11(),
	UIDraggableCamera_t1776763358::get_offset_of_mBounds_12(),
	UIDraggableCamera_t1776763358::get_offset_of_mScroll_13(),
	UIDraggableCamera_t1776763358::get_offset_of_mRoot_14(),
	UIDraggableCamera_t1776763358::get_offset_of_mDragStarted_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (UIEventTrigger_t46291570), -1, sizeof(UIEventTrigger_t46291570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[14] = 
{
	UIEventTrigger_t46291570_StaticFields::get_offset_of_current_2(),
	UIEventTrigger_t46291570::get_offset_of_onHoverOver_3(),
	UIEventTrigger_t46291570::get_offset_of_onHoverOut_4(),
	UIEventTrigger_t46291570::get_offset_of_onPress_5(),
	UIEventTrigger_t46291570::get_offset_of_onRelease_6(),
	UIEventTrigger_t46291570::get_offset_of_onSelect_7(),
	UIEventTrigger_t46291570::get_offset_of_onDeselect_8(),
	UIEventTrigger_t46291570::get_offset_of_onClick_9(),
	UIEventTrigger_t46291570::get_offset_of_onDoubleClick_10(),
	UIEventTrigger_t46291570::get_offset_of_onDragStart_11(),
	UIEventTrigger_t46291570::get_offset_of_onDragEnd_12(),
	UIEventTrigger_t46291570::get_offset_of_onDragOver_13(),
	UIEventTrigger_t46291570::get_offset_of_onDragOut_14(),
	UIEventTrigger_t46291570::get_offset_of_onDrag_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (UIForwardEvents_t1048473098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[10] = 
{
	UIForwardEvents_t1048473098::get_offset_of_target_2(),
	UIForwardEvents_t1048473098::get_offset_of_onHover_3(),
	UIForwardEvents_t1048473098::get_offset_of_onPress_4(),
	UIForwardEvents_t1048473098::get_offset_of_onClick_5(),
	UIForwardEvents_t1048473098::get_offset_of_onDoubleClick_6(),
	UIForwardEvents_t1048473098::get_offset_of_onSelect_7(),
	UIForwardEvents_t1048473098::get_offset_of_onDrag_8(),
	UIForwardEvents_t1048473098::get_offset_of_onDrop_9(),
	UIForwardEvents_t1048473098::get_offset_of_onSubmit_10(),
	UIForwardEvents_t1048473098::get_offset_of_onScroll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (UIGrid_t2503122938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[15] = 
{
	UIGrid_t2503122938::get_offset_of_arrangement_2(),
	UIGrid_t2503122938::get_offset_of_sorting_3(),
	UIGrid_t2503122938::get_offset_of_pivot_4(),
	UIGrid_t2503122938::get_offset_of_maxPerLine_5(),
	UIGrid_t2503122938::get_offset_of_cellWidth_6(),
	UIGrid_t2503122938::get_offset_of_cellHeight_7(),
	UIGrid_t2503122938::get_offset_of_animateSmoothly_8(),
	UIGrid_t2503122938::get_offset_of_hideInactive_9(),
	UIGrid_t2503122938::get_offset_of_keepWithinPanel_10(),
	UIGrid_t2503122938::get_offset_of_onReposition_11(),
	UIGrid_t2503122938::get_offset_of_onCustomSort_12(),
	UIGrid_t2503122938::get_offset_of_sorted_13(),
	UIGrid_t2503122938::get_offset_of_mReposition_14(),
	UIGrid_t2503122938::get_offset_of_mPanel_15(),
	UIGrid_t2503122938::get_offset_of_mInitDone_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (Arrangement_t2060582997)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	Arrangement_t2060582997::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (Sorting_t2346397775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2377[6] = 
{
	Sorting_t2346397775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (OnReposition_t1310478256), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (UIImageButton_t726205753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[6] = 
{
	UIImageButton_t726205753::get_offset_of_target_2(),
	UIImageButton_t726205753::get_offset_of_normalSprite_3(),
	UIImageButton_t726205753::get_offset_of_hoverSprite_4(),
	UIImageButton_t726205753::get_offset_of_pressedSprite_5(),
	UIImageButton_t726205753::get_offset_of_disabledSprite_6(),
	UIImageButton_t726205753::get_offset_of_pixelSnap_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (UIKeyBinding_t2313833690), -1, sizeof(UIKeyBinding_t2313833690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2380[7] = 
{
	UIKeyBinding_t2313833690_StaticFields::get_offset_of_mList_2(),
	UIKeyBinding_t2313833690::get_offset_of_keyCode_3(),
	UIKeyBinding_t2313833690::get_offset_of_modifier_4(),
	UIKeyBinding_t2313833690::get_offset_of_action_5(),
	UIKeyBinding_t2313833690::get_offset_of_mIgnoreUp_6(),
	UIKeyBinding_t2313833690::get_offset_of_mIsInput_7(),
	UIKeyBinding_t2313833690::get_offset_of_mPress_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (Action_t3569464619)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[4] = 
{
	Action_t3569464619::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (Modifier_t3912257676)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2382[6] = 
{
	Modifier_t3912257676::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (UIKeyNavigation_t1837256607), -1, sizeof(UIKeyNavigation_t1837256607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2383[10] = 
{
	UIKeyNavigation_t1837256607_StaticFields::get_offset_of_list_2(),
	UIKeyNavigation_t1837256607::get_offset_of_constraint_3(),
	UIKeyNavigation_t1837256607::get_offset_of_onUp_4(),
	UIKeyNavigation_t1837256607::get_offset_of_onDown_5(),
	UIKeyNavigation_t1837256607::get_offset_of_onLeft_6(),
	UIKeyNavigation_t1837256607::get_offset_of_onRight_7(),
	UIKeyNavigation_t1837256607::get_offset_of_onClick_8(),
	UIKeyNavigation_t1837256607::get_offset_of_onTab_9(),
	UIKeyNavigation_t1837256607::get_offset_of_startsSelected_10(),
	UIKeyNavigation_t1837256607::get_offset_of_mStarted_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (Constraint_t3356609133)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2384[5] = 
{
	Constraint_t3356609133::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (UIPlayAnimation_t2708996220), -1, sizeof(UIPlayAnimation_t2708996220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2385[16] = 
{
	UIPlayAnimation_t2708996220_StaticFields::get_offset_of_current_2(),
	UIPlayAnimation_t2708996220::get_offset_of_target_3(),
	UIPlayAnimation_t2708996220::get_offset_of_animator_4(),
	UIPlayAnimation_t2708996220::get_offset_of_clipName_5(),
	UIPlayAnimation_t2708996220::get_offset_of_trigger_6(),
	UIPlayAnimation_t2708996220::get_offset_of_playDirection_7(),
	UIPlayAnimation_t2708996220::get_offset_of_resetOnPlay_8(),
	UIPlayAnimation_t2708996220::get_offset_of_clearSelection_9(),
	UIPlayAnimation_t2708996220::get_offset_of_ifDisabledOnPlay_10(),
	UIPlayAnimation_t2708996220::get_offset_of_disableWhenFinished_11(),
	UIPlayAnimation_t2708996220::get_offset_of_onFinished_12(),
	UIPlayAnimation_t2708996220::get_offset_of_eventReceiver_13(),
	UIPlayAnimation_t2708996220::get_offset_of_callWhenFinished_14(),
	UIPlayAnimation_t2708996220::get_offset_of_mStarted_15(),
	UIPlayAnimation_t2708996220::get_offset_of_mActivated_16(),
	UIPlayAnimation_t2708996220::get_offset_of_dragHighlight_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (UIPlaySound_t532605447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[5] = 
{
	UIPlaySound_t532605447::get_offset_of_audioClip_2(),
	UIPlaySound_t532605447::get_offset_of_trigger_3(),
	UIPlaySound_t532605447::get_offset_of_volume_4(),
	UIPlaySound_t532605447::get_offset_of_pitch_5(),
	UIPlaySound_t532605447::get_offset_of_mIsOver_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (Trigger_t1400315280)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2387[9] = 
{
	Trigger_t1400315280::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (UIPlayTween_t533751651), -1, sizeof(UIPlayTween_t533751651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2388[17] = 
{
	UIPlayTween_t533751651_StaticFields::get_offset_of_current_2(),
	UIPlayTween_t533751651::get_offset_of_tweenTarget_3(),
	UIPlayTween_t533751651::get_offset_of_tweenGroup_4(),
	UIPlayTween_t533751651::get_offset_of_trigger_5(),
	UIPlayTween_t533751651::get_offset_of_playDirection_6(),
	UIPlayTween_t533751651::get_offset_of_resetOnPlay_7(),
	UIPlayTween_t533751651::get_offset_of_resetIfDisabled_8(),
	UIPlayTween_t533751651::get_offset_of_ifDisabledOnPlay_9(),
	UIPlayTween_t533751651::get_offset_of_disableWhenFinished_10(),
	UIPlayTween_t533751651::get_offset_of_includeChildren_11(),
	UIPlayTween_t533751651::get_offset_of_onFinished_12(),
	UIPlayTween_t533751651::get_offset_of_eventReceiver_13(),
	UIPlayTween_t533751651::get_offset_of_callWhenFinished_14(),
	UIPlayTween_t533751651::get_offset_of_mTweens_15(),
	UIPlayTween_t533751651::get_offset_of_mStarted_16(),
	UIPlayTween_t533751651::get_offset_of_mActive_17(),
	UIPlayTween_t533751651::get_offset_of_mActivated_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (UIPopupList_t1804933942), -1, sizeof(UIPopupList_t1804933942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2389[43] = 
{
	0,
	UIPopupList_t1804933942_StaticFields::get_offset_of_current_3(),
	UIPopupList_t1804933942_StaticFields::get_offset_of_mChild_4(),
	UIPopupList_t1804933942_StaticFields::get_offset_of_mFadeOutComplete_5(),
	UIPopupList_t1804933942::get_offset_of_atlas_6(),
	UIPopupList_t1804933942::get_offset_of_bitmapFont_7(),
	UIPopupList_t1804933942::get_offset_of_trueTypeFont_8(),
	UIPopupList_t1804933942::get_offset_of_fontSize_9(),
	UIPopupList_t1804933942::get_offset_of_fontStyle_10(),
	UIPopupList_t1804933942::get_offset_of_backgroundSprite_11(),
	UIPopupList_t1804933942::get_offset_of_highlightSprite_12(),
	UIPopupList_t1804933942::get_offset_of_position_13(),
	UIPopupList_t1804933942::get_offset_of_alignment_14(),
	UIPopupList_t1804933942::get_offset_of_items_15(),
	UIPopupList_t1804933942::get_offset_of_itemData_16(),
	UIPopupList_t1804933942::get_offset_of_padding_17(),
	UIPopupList_t1804933942::get_offset_of_textColor_18(),
	UIPopupList_t1804933942::get_offset_of_backgroundColor_19(),
	UIPopupList_t1804933942::get_offset_of_highlightColor_20(),
	UIPopupList_t1804933942::get_offset_of_isAnimated_21(),
	UIPopupList_t1804933942::get_offset_of_isLocalized_22(),
	UIPopupList_t1804933942::get_offset_of_separatePanel_23(),
	UIPopupList_t1804933942::get_offset_of_openOn_24(),
	UIPopupList_t1804933942::get_offset_of_onChange_25(),
	UIPopupList_t1804933942::get_offset_of_mSelectedItem_26(),
	UIPopupList_t1804933942::get_offset_of_mPanel_27(),
	UIPopupList_t1804933942::get_offset_of_mBackground_28(),
	UIPopupList_t1804933942::get_offset_of_mHighlight_29(),
	UIPopupList_t1804933942::get_offset_of_mHighlightedLabel_30(),
	UIPopupList_t1804933942::get_offset_of_mLabelList_31(),
	UIPopupList_t1804933942::get_offset_of_mBgBorder_32(),
	UIPopupList_t1804933942::get_offset_of_mSelection_33(),
	UIPopupList_t1804933942::get_offset_of_mOpenFrame_34(),
	UIPopupList_t1804933942::get_offset_of_eventReceiver_35(),
	UIPopupList_t1804933942::get_offset_of_functionName_36(),
	UIPopupList_t1804933942::get_offset_of_textScale_37(),
	UIPopupList_t1804933942::get_offset_of_font_38(),
	UIPopupList_t1804933942::get_offset_of_textLabel_39(),
	UIPopupList_t1804933942::get_offset_of_mLegacyEvent_40(),
	UIPopupList_t1804933942::get_offset_of_mExecuting_41(),
	UIPopupList_t1804933942::get_offset_of_mUseDynamicFont_42(),
	UIPopupList_t1804933942::get_offset_of_mTweening_43(),
	UIPopupList_t1804933942::get_offset_of_source_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (Position_t2071232514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	Position_t2071232514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (OpenOn_t1064537826)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2391[5] = 
{
	OpenOn_t1064537826::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (LegacyEvent_t524649176), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (U3CUpdateTweenPositionU3Ec__IteratorB_t989198279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[4] = 
{
	U3CUpdateTweenPositionU3Ec__IteratorB_t989198279::get_offset_of_U3CtpU3E__0_0(),
	U3CUpdateTweenPositionU3Ec__IteratorB_t989198279::get_offset_of_U24PC_1(),
	U3CUpdateTweenPositionU3Ec__IteratorB_t989198279::get_offset_of_U24current_2(),
	U3CUpdateTweenPositionU3Ec__IteratorB_t989198279::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (U3CCloseIfUnselectedU3Ec__IteratorC_t777622634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[3] = 
{
	U3CCloseIfUnselectedU3Ec__IteratorC_t777622634::get_offset_of_U24PC_0(),
	U3CCloseIfUnselectedU3Ec__IteratorC_t777622634::get_offset_of_U24current_1(),
	U3CCloseIfUnselectedU3Ec__IteratorC_t777622634::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (UIProgressBar_t168062834), -1, sizeof(UIProgressBar_t168062834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2395[13] = 
{
	UIProgressBar_t168062834_StaticFields::get_offset_of_current_2(),
	UIProgressBar_t168062834::get_offset_of_onDragFinished_3(),
	UIProgressBar_t168062834::get_offset_of_thumb_4(),
	UIProgressBar_t168062834::get_offset_of_mBG_5(),
	UIProgressBar_t168062834::get_offset_of_mFG_6(),
	UIProgressBar_t168062834::get_offset_of_mValue_7(),
	UIProgressBar_t168062834::get_offset_of_mFill_8(),
	UIProgressBar_t168062834::get_offset_of_mTrans_9(),
	UIProgressBar_t168062834::get_offset_of_mIsDirty_10(),
	UIProgressBar_t168062834::get_offset_of_mCam_11(),
	UIProgressBar_t168062834::get_offset_of_mOffset_12(),
	UIProgressBar_t168062834::get_offset_of_numberOfSteps_13(),
	UIProgressBar_t168062834::get_offset_of_onChange_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (FillDirection_t1321036095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[5] = 
{
	FillDirection_t1321036095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (OnDragFinished_t4207031714), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (UISavedOption_t3282279080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[4] = 
{
	UISavedOption_t3282279080::get_offset_of_keyName_2(),
	UISavedOption_t3282279080::get_offset_of_mList_3(),
	UISavedOption_t3282279080::get_offset_of_mCheck_4(),
	UISavedOption_t3282279080::get_offset_of_mSlider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (UIScrollBar_t2839103954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[3] = 
{
	UIScrollBar_t2839103954::get_offset_of_mSize_19(),
	UIScrollBar_t2839103954::get_offset_of_mScroll_20(),
	UIScrollBar_t2839103954::get_offset_of_mDir_21(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
