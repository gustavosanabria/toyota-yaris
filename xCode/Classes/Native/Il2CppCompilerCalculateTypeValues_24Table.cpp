﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIScrollBar_Direction2820314978.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement2769919992.h"
#include "AssemblyU2DCSharp_UIScrollView_DragEffect2340818158.h"
#include "AssemblyU2DCSharp_UIScrollView_ShowCondition3198502133.h"
#include "AssemblyU2DCSharp_UIScrollView_OnDragNotification2323474503.h"
#include "AssemblyU2DCSharp_UIShowControlScheme2031965489.h"
#include "AssemblyU2DCSharp_UISlider657469589.h"
#include "AssemblyU2DCSharp_UISlider_Direction2371757669.h"
#include "AssemblyU2DCSharp_UISoundVolume1043221973.h"
#include "AssemblyU2DCSharp_UITable298892698.h"
#include "AssemblyU2DCSharp_UITable_Direction2397696298.h"
#include "AssemblyU2DCSharp_UITable_Sorting4124159471.h"
#include "AssemblyU2DCSharp_UITable_OnReposition213079568.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIToggle_Validate2927776861.h"
#include "AssemblyU2DCSharp_UIToggledComponents3531934354.h"
#include "AssemblyU2DCSharp_UIToggledObjects793825048.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2759048342.h"
#include "AssemblyU2DCSharp_ActiveAnimation557316862.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Trigger2113303578.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction1859285089.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition1579250490.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableConditio1452383689.h"
#include "AssemblyU2DCSharp_BMFont1962830650.h"
#include "AssemblyU2DCSharp_BMGlyph719052705.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339.h"
#include "AssemblyU2DCSharp_ByteReader2446302219.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter2566940569.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_Localization3647281849.h"
#include "AssemblyU2DCSharp_Localization_LoadFunction2234103444.h"
#include "AssemblyU2DCSharp_Localization_OnLocalizeNotificat2879153737.h"
#include "AssemblyU2DCSharp_NGUIDebug222190662.h"
#include "AssemblyU2DCSharp_NGUIMath3886757557.h"
#include "AssemblyU2DCSharp_NGUIText3886970074.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3426431694.h"
#include "AssemblyU2DCSharp_NGUIText_SymbolStyle99318052.h"
#include "AssemblyU2DCSharp_NGUIText_GlyphInfo2737853669.h"
#include "AssemblyU2DCSharp_NGUITools237277134.h"
#include "AssemblyU2DCSharp_PropertyBinding2650920656.h"
#include "AssemblyU2DCSharp_PropertyBinding_UpdateCondition1215877363.h"
#include "AssemblyU2DCSharp_PropertyBinding_Direction260931296.h"
#include "AssemblyU2DCSharp_PropertyReference614957270.h"
#include "AssemblyU2DCSharp_RealTime3499460011.h"
#include "AssemblyU2DCSharp_SpringPanel929169367.h"
#include "AssemblyU2DCSharp_SpringPanel_OnFinished3316389065.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type741864970.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection3514300524.h"
#include "AssemblyU2DCSharp_UIBasicSprite_AdvancedType4228915532.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip741435197.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "AssemblyU2DCSharp_UIDrawCall_Clipping3937989211.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2651898963.h"
#include "AssemblyU2DCSharp_UIEventListener1278105402.h"
#include "AssemblyU2DCSharp_UIEventListener_VoidDelegate986119182.h"
#include "AssemblyU2DCSharp_UIEventListener_BoolDelegate4240584548.h"
#include "AssemblyU2DCSharp_UIEventListener_FloatDelegate3249382860.h"
#include "AssemblyU2DCSharp_UIEventListener_VectorDelegate3512109949.h"
#include "AssemblyU2DCSharp_UIEventListener_ObjectDelegate172593529.h"
#include "AssemblyU2DCSharp_UIEventListener_KeyCodeDelegate2040735132.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint3581172420.h"
#include "AssemblyU2DCSharp_UIRect_AnchorUpdate3786070549.h"
#include "AssemblyU2DCSharp_UISnapshotPoint3024846840.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource788096023.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3695058769.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback3030491454.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3889696652.h"
#include "AssemblyU2DCSharp_AnimatedAlpha3219346779.h"
#include "AssemblyU2DCSharp_AnimatedColor3221279584.h"
#include "AssemblyU2DCSharp_AnimatedWidget1642214887.h"
#include "AssemblyU2DCSharp_SpringPosition3802689142.h"
#include "AssemblyU2DCSharp_SpringPosition_OnFinished2399837386.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587.h"
#include "AssemblyU2DCSharp_TweenColor2922258392.h"
#include "AssemblyU2DCSharp_TweenFOV1594102402.h"
#include "AssemblyU2DCSharp_TweenHeight529510226.h"
#include "AssemblyU2DCSharp_TweenOrthoSize1845504462.h"
#include "AssemblyU2DCSharp_TweenPosition3684358292.h"
#include "AssemblyU2DCSharp_TweenRotation2896252649.h"
#include "AssemblyU2DCSharp_TweenScale2936666559.h"
#include "AssemblyU2DCSharp_TweenTransform1891506529.h"
#include "AssemblyU2DCSharp_TweenVolume939656517.h"
#include "AssemblyU2DCSharp_TweenWidth2940542523.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UITweener_Style3365907174.h"
#include "AssemblyU2DCSharp_UI2DSprite1326097995.h"
#include "AssemblyU2DCSharp_UI2DSpriteAnimation3878779257.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (Direction_t2820314978)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[4] = 
{
	Direction_t2820314978::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (UIScrollView_t2113479878), -1, sizeof(UIScrollView_t2113479878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2401[37] = 
{
	UIScrollView_t2113479878_StaticFields::get_offset_of_list_2(),
	UIScrollView_t2113479878::get_offset_of_movement_3(),
	UIScrollView_t2113479878::get_offset_of_dragEffect_4(),
	UIScrollView_t2113479878::get_offset_of_restrictWithinPanel_5(),
	UIScrollView_t2113479878::get_offset_of_disableDragIfFits_6(),
	UIScrollView_t2113479878::get_offset_of_smoothDragStart_7(),
	UIScrollView_t2113479878::get_offset_of_iOSDragEmulation_8(),
	UIScrollView_t2113479878::get_offset_of_scrollWheelFactor_9(),
	UIScrollView_t2113479878::get_offset_of_momentumAmount_10(),
	UIScrollView_t2113479878::get_offset_of_dampenStrength_11(),
	UIScrollView_t2113479878::get_offset_of_horizontalScrollBar_12(),
	UIScrollView_t2113479878::get_offset_of_verticalScrollBar_13(),
	UIScrollView_t2113479878::get_offset_of_showScrollBars_14(),
	UIScrollView_t2113479878::get_offset_of_customMovement_15(),
	UIScrollView_t2113479878::get_offset_of_contentPivot_16(),
	UIScrollView_t2113479878::get_offset_of_onDragStarted_17(),
	UIScrollView_t2113479878::get_offset_of_onDragFinished_18(),
	UIScrollView_t2113479878::get_offset_of_onMomentumMove_19(),
	UIScrollView_t2113479878::get_offset_of_onStoppedMoving_20(),
	UIScrollView_t2113479878::get_offset_of_scale_21(),
	UIScrollView_t2113479878::get_offset_of_relativePositionOnReset_22(),
	UIScrollView_t2113479878::get_offset_of_mTrans_23(),
	UIScrollView_t2113479878::get_offset_of_mPanel_24(),
	UIScrollView_t2113479878::get_offset_of_mPlane_25(),
	UIScrollView_t2113479878::get_offset_of_mLastPos_26(),
	UIScrollView_t2113479878::get_offset_of_mPressed_27(),
	UIScrollView_t2113479878::get_offset_of_mMomentum_28(),
	UIScrollView_t2113479878::get_offset_of_mScroll_29(),
	UIScrollView_t2113479878::get_offset_of_mBounds_30(),
	UIScrollView_t2113479878::get_offset_of_mCalculatedBounds_31(),
	UIScrollView_t2113479878::get_offset_of_mShouldMove_32(),
	UIScrollView_t2113479878::get_offset_of_mIgnoreCallbacks_33(),
	UIScrollView_t2113479878::get_offset_of_mDragID_34(),
	UIScrollView_t2113479878::get_offset_of_mDragStartOffset_35(),
	UIScrollView_t2113479878::get_offset_of_mDragStarted_36(),
	UIScrollView_t2113479878::get_offset_of_mStarted_37(),
	UIScrollView_t2113479878::get_offset_of_centerOnChild_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (Movement_t2769919992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2402[5] = 
{
	Movement_t2769919992::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (DragEffect_t2340818158)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2403[4] = 
{
	DragEffect_t2340818158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (ShowCondition_t3198502133)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2404[4] = 
{
	ShowCondition_t3198502133::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (OnDragNotification_t2323474503), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (UIShowControlScheme_t2031965489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[4] = 
{
	UIShowControlScheme_t2031965489::get_offset_of_target_2(),
	UIShowControlScheme_t2031965489::get_offset_of_mouse_3(),
	UIShowControlScheme_t2031965489::get_offset_of_touch_4(),
	UIShowControlScheme_t2031965489::get_offset_of_controller_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (UISlider_t657469589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[4] = 
{
	UISlider_t657469589::get_offset_of_foreground_15(),
	UISlider_t657469589::get_offset_of_rawValue_16(),
	UISlider_t657469589::get_offset_of_direction_17(),
	UISlider_t657469589::get_offset_of_mInverted_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (Direction_t2371757669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[4] = 
{
	Direction_t2371757669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (UISoundVolume_t1043221973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (UITable_t298892698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[13] = 
{
	UITable_t298892698::get_offset_of_columns_2(),
	UITable_t298892698::get_offset_of_direction_3(),
	UITable_t298892698::get_offset_of_sorting_4(),
	UITable_t298892698::get_offset_of_pivot_5(),
	UITable_t298892698::get_offset_of_cellAlignment_6(),
	UITable_t298892698::get_offset_of_hideInactive_7(),
	UITable_t298892698::get_offset_of_keepWithinPanel_8(),
	UITable_t298892698::get_offset_of_padding_9(),
	UITable_t298892698::get_offset_of_onReposition_10(),
	UITable_t298892698::get_offset_of_onCustomSort_11(),
	UITable_t298892698::get_offset_of_mPanel_12(),
	UITable_t298892698::get_offset_of_mInitDone_13(),
	UITable_t298892698::get_offset_of_mReposition_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (Direction_t2397696298)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2411[3] = 
{
	Direction_t2397696298::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (Sorting_t4124159471)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2412[6] = 
{
	Sorting_t4124159471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (OnReposition_t213079568), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (UIToggle_t688812808), -1, sizeof(UIToggle_t688812808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2414[18] = 
{
	UIToggle_t688812808_StaticFields::get_offset_of_list_2(),
	UIToggle_t688812808_StaticFields::get_offset_of_current_3(),
	UIToggle_t688812808::get_offset_of_group_4(),
	UIToggle_t688812808::get_offset_of_activeSprite_5(),
	UIToggle_t688812808::get_offset_of_activeAnimation_6(),
	UIToggle_t688812808::get_offset_of_animator_7(),
	UIToggle_t688812808::get_offset_of_startsActive_8(),
	UIToggle_t688812808::get_offset_of_instantTween_9(),
	UIToggle_t688812808::get_offset_of_optionCanBeNone_10(),
	UIToggle_t688812808::get_offset_of_onChange_11(),
	UIToggle_t688812808::get_offset_of_validator_12(),
	UIToggle_t688812808::get_offset_of_checkSprite_13(),
	UIToggle_t688812808::get_offset_of_checkAnimation_14(),
	UIToggle_t688812808::get_offset_of_eventReceiver_15(),
	UIToggle_t688812808::get_offset_of_functionName_16(),
	UIToggle_t688812808::get_offset_of_startsChecked_17(),
	UIToggle_t688812808::get_offset_of_mIsActive_18(),
	UIToggle_t688812808::get_offset_of_mStarted_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (Validate_t2927776861), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (UIToggledComponents_t3531934354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[4] = 
{
	UIToggledComponents_t3531934354::get_offset_of_activate_2(),
	UIToggledComponents_t3531934354::get_offset_of_deactivate_3(),
	UIToggledComponents_t3531934354::get_offset_of_target_4(),
	UIToggledComponents_t3531934354::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (UIToggledObjects_t793825048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[4] = 
{
	UIToggledObjects_t793825048::get_offset_of_activate_2(),
	UIToggledObjects_t793825048::get_offset_of_deactivate_3(),
	UIToggledObjects_t793825048::get_offset_of_target_4(),
	UIToggledObjects_t793825048::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (UIWidgetContainer_t1520767337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (UIWrapContent_t33025435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[11] = 
{
	UIWrapContent_t33025435::get_offset_of_itemSize_2(),
	UIWrapContent_t33025435::get_offset_of_cullContent_3(),
	UIWrapContent_t33025435::get_offset_of_minIndex_4(),
	UIWrapContent_t33025435::get_offset_of_maxIndex_5(),
	UIWrapContent_t33025435::get_offset_of_onInitializeItem_6(),
	UIWrapContent_t33025435::get_offset_of_mTrans_7(),
	UIWrapContent_t33025435::get_offset_of_mPanel_8(),
	UIWrapContent_t33025435::get_offset_of_mScroll_9(),
	UIWrapContent_t33025435::get_offset_of_mHorizontal_10(),
	UIWrapContent_t33025435::get_offset_of_mFirstTime_11(),
	UIWrapContent_t33025435::get_offset_of_mChildren_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (OnInitializeItem_t2759048342), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (ActiveAnimation_t557316862), -1, sizeof(ActiveAnimation_t557316862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2421[10] = 
{
	ActiveAnimation_t557316862_StaticFields::get_offset_of_current_2(),
	ActiveAnimation_t557316862::get_offset_of_onFinished_3(),
	ActiveAnimation_t557316862::get_offset_of_eventReceiver_4(),
	ActiveAnimation_t557316862::get_offset_of_callWhenFinished_5(),
	ActiveAnimation_t557316862::get_offset_of_mAnim_6(),
	ActiveAnimation_t557316862::get_offset_of_mLastDirection_7(),
	ActiveAnimation_t557316862::get_offset_of_mDisableDirection_8(),
	ActiveAnimation_t557316862::get_offset_of_mNotify_9(),
	ActiveAnimation_t557316862::get_offset_of_mAnimator_10(),
	ActiveAnimation_t557316862::get_offset_of_mClip_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (Trigger_t2113303578)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2422[15] = 
{
	Trigger_t2113303578::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (Direction_t1859285089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2423[4] = 
{
	Direction_t1859285089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (EnableCondition_t1579250490)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2424[4] = 
{
	EnableCondition_t1579250490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (DisableCondition_t1452383689)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2425[4] = 
{
	DisableCondition_t1452383689::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (BMFont_t1962830650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[7] = 
{
	BMFont_t1962830650::get_offset_of_mSize_0(),
	BMFont_t1962830650::get_offset_of_mBase_1(),
	BMFont_t1962830650::get_offset_of_mWidth_2(),
	BMFont_t1962830650::get_offset_of_mHeight_3(),
	BMFont_t1962830650::get_offset_of_mSpriteName_4(),
	BMFont_t1962830650::get_offset_of_mSaved_5(),
	BMFont_t1962830650::get_offset_of_mDict_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (BMGlyph_t719052705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[10] = 
{
	BMGlyph_t719052705::get_offset_of_index_0(),
	BMGlyph_t719052705::get_offset_of_x_1(),
	BMGlyph_t719052705::get_offset_of_y_2(),
	BMGlyph_t719052705::get_offset_of_width_3(),
	BMGlyph_t719052705::get_offset_of_height_4(),
	BMGlyph_t719052705::get_offset_of_offsetX_5(),
	BMGlyph_t719052705::get_offset_of_offsetY_6(),
	BMGlyph_t719052705::get_offset_of_advance_7(),
	BMGlyph_t719052705::get_offset_of_channel_8(),
	BMGlyph_t719052705::get_offset_of_kerning_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (BMSymbol_t1170982339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[11] = 
{
	BMSymbol_t1170982339::get_offset_of_sequence_0(),
	BMSymbol_t1170982339::get_offset_of_spriteName_1(),
	BMSymbol_t1170982339::get_offset_of_mSprite_2(),
	BMSymbol_t1170982339::get_offset_of_mIsValid_3(),
	BMSymbol_t1170982339::get_offset_of_mLength_4(),
	BMSymbol_t1170982339::get_offset_of_mOffsetX_5(),
	BMSymbol_t1170982339::get_offset_of_mOffsetY_6(),
	BMSymbol_t1170982339::get_offset_of_mWidth_7(),
	BMSymbol_t1170982339::get_offset_of_mHeight_8(),
	BMSymbol_t1170982339::get_offset_of_mAdvance_9(),
	BMSymbol_t1170982339::get_offset_of_mUV_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (ByteReader_t2446302219), -1, sizeof(ByteReader_t2446302219_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[3] = 
{
	ByteReader_t2446302219::get_offset_of_mBuffer_0(),
	ByteReader_t2446302219::get_offset_of_mOffset_1(),
	ByteReader_t2446302219_StaticFields::get_offset_of_mTemp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (EventDelegate_t4004424223), -1, sizeof(EventDelegate_t4004424223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2433[11] = 
{
	EventDelegate_t4004424223::get_offset_of_mTarget_0(),
	EventDelegate_t4004424223::get_offset_of_mMethodName_1(),
	EventDelegate_t4004424223::get_offset_of_mParameters_2(),
	EventDelegate_t4004424223::get_offset_of_oneShot_3(),
	EventDelegate_t4004424223::get_offset_of_mCachedCallback_4(),
	EventDelegate_t4004424223::get_offset_of_mRawDelegate_5(),
	EventDelegate_t4004424223::get_offset_of_mCached_6(),
	EventDelegate_t4004424223::get_offset_of_mMethod_7(),
	EventDelegate_t4004424223::get_offset_of_mParameterInfos_8(),
	EventDelegate_t4004424223::get_offset_of_mArgs_9(),
	EventDelegate_t4004424223_StaticFields::get_offset_of_s_Hash_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (Parameter_t2566940569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[7] = 
{
	Parameter_t2566940569::get_offset_of_obj_0(),
	Parameter_t2566940569::get_offset_of_field_1(),
	Parameter_t2566940569::get_offset_of_mValue_2(),
	Parameter_t2566940569::get_offset_of_expectedType_3(),
	Parameter_t2566940569::get_offset_of_cached_4(),
	Parameter_t2566940569::get_offset_of_propInfo_5(),
	Parameter_t2566940569::get_offset_of_fieldInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (Callback_t1094463061), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (Localization_t3647281849), -1, sizeof(Localization_t3647281849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2436[10] = 
{
	Localization_t3647281849_StaticFields::get_offset_of_loadFunction_0(),
	Localization_t3647281849_StaticFields::get_offset_of_onLocalize_1(),
	Localization_t3647281849_StaticFields::get_offset_of_localizationHasBeenSet_2(),
	Localization_t3647281849_StaticFields::get_offset_of_mLanguages_3(),
	Localization_t3647281849_StaticFields::get_offset_of_mOldDictionary_4(),
	Localization_t3647281849_StaticFields::get_offset_of_mDictionary_5(),
	Localization_t3647281849_StaticFields::get_offset_of_mReplacement_6(),
	Localization_t3647281849_StaticFields::get_offset_of_mLanguageIndex_7(),
	Localization_t3647281849_StaticFields::get_offset_of_mLanguage_8(),
	Localization_t3647281849_StaticFields::get_offset_of_mMerging_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (LoadFunction_t2234103444), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (OnLocalizeNotification_t2879153737), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (NGUIDebug_t222190662), -1, sizeof(NGUIDebug_t222190662_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2439[3] = 
{
	NGUIDebug_t222190662_StaticFields::get_offset_of_mRayDebug_2(),
	NGUIDebug_t222190662_StaticFields::get_offset_of_mLines_3(),
	NGUIDebug_t222190662_StaticFields::get_offset_of_mInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (NGUIMath_t3886757557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (NGUIText_t3886970074), -1, sizeof(NGUIText_t3886970074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2441[39] = 
{
	NGUIText_t3886970074_StaticFields::get_offset_of_bitmapFont_0(),
	NGUIText_t3886970074_StaticFields::get_offset_of_dynamicFont_1(),
	NGUIText_t3886970074_StaticFields::get_offset_of_glyph_2(),
	NGUIText_t3886970074_StaticFields::get_offset_of_fontSize_3(),
	NGUIText_t3886970074_StaticFields::get_offset_of_fontScale_4(),
	NGUIText_t3886970074_StaticFields::get_offset_of_pixelDensity_5(),
	NGUIText_t3886970074_StaticFields::get_offset_of_fontStyle_6(),
	NGUIText_t3886970074_StaticFields::get_offset_of_alignment_7(),
	NGUIText_t3886970074_StaticFields::get_offset_of_tint_8(),
	NGUIText_t3886970074_StaticFields::get_offset_of_rectWidth_9(),
	NGUIText_t3886970074_StaticFields::get_offset_of_rectHeight_10(),
	NGUIText_t3886970074_StaticFields::get_offset_of_regionWidth_11(),
	NGUIText_t3886970074_StaticFields::get_offset_of_regionHeight_12(),
	NGUIText_t3886970074_StaticFields::get_offset_of_maxLines_13(),
	NGUIText_t3886970074_StaticFields::get_offset_of_gradient_14(),
	NGUIText_t3886970074_StaticFields::get_offset_of_gradientBottom_15(),
	NGUIText_t3886970074_StaticFields::get_offset_of_gradientTop_16(),
	NGUIText_t3886970074_StaticFields::get_offset_of_encoding_17(),
	NGUIText_t3886970074_StaticFields::get_offset_of_spacingX_18(),
	NGUIText_t3886970074_StaticFields::get_offset_of_spacingY_19(),
	NGUIText_t3886970074_StaticFields::get_offset_of_premultiply_20(),
	NGUIText_t3886970074_StaticFields::get_offset_of_symbolStyle_21(),
	NGUIText_t3886970074_StaticFields::get_offset_of_finalSize_22(),
	NGUIText_t3886970074_StaticFields::get_offset_of_finalSpacingX_23(),
	NGUIText_t3886970074_StaticFields::get_offset_of_finalLineHeight_24(),
	NGUIText_t3886970074_StaticFields::get_offset_of_baseline_25(),
	NGUIText_t3886970074_StaticFields::get_offset_of_useSymbols_26(),
	NGUIText_t3886970074_StaticFields::get_offset_of_mInvisible_27(),
	NGUIText_t3886970074_StaticFields::get_offset_of_mColors_28(),
	NGUIText_t3886970074_StaticFields::get_offset_of_mAlpha_29(),
	NGUIText_t3886970074_StaticFields::get_offset_of_mTempChar_30(),
	NGUIText_t3886970074_StaticFields::get_offset_of_mSizes_31(),
	NGUIText_t3886970074_StaticFields::get_offset_of_s_c0_32(),
	NGUIText_t3886970074_StaticFields::get_offset_of_s_c1_33(),
	NGUIText_t3886970074_StaticFields::get_offset_of_mBoldOffset_34(),
	NGUIText_t3886970074_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_35(),
	NGUIText_t3886970074_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_36(),
	NGUIText_t3886970074_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_37(),
	NGUIText_t3886970074_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (Alignment_t3426431694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2442[6] = 
{
	Alignment_t3426431694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (SymbolStyle_t99318052)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[4] = 
{
	SymbolStyle_t99318052::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (GlyphInfo_t2737853669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[8] = 
{
	GlyphInfo_t2737853669::get_offset_of_v0_0(),
	GlyphInfo_t2737853669::get_offset_of_v1_1(),
	GlyphInfo_t2737853669::get_offset_of_u0_2(),
	GlyphInfo_t2737853669::get_offset_of_u1_3(),
	GlyphInfo_t2737853669::get_offset_of_u2_4(),
	GlyphInfo_t2737853669::get_offset_of_u3_5(),
	GlyphInfo_t2737853669::get_offset_of_advance_6(),
	GlyphInfo_t2737853669::get_offset_of_channel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (NGUITools_t237277134), -1, sizeof(NGUITools_t237277134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[7] = 
{
	NGUITools_t237277134_StaticFields::get_offset_of_mListener_0(),
	NGUITools_t237277134_StaticFields::get_offset_of_mLoaded_1(),
	NGUITools_t237277134_StaticFields::get_offset_of_mGlobalVolume_2(),
	NGUITools_t237277134_StaticFields::get_offset_of_mLastTimestamp_3(),
	NGUITools_t237277134_StaticFields::get_offset_of_mLastClip_4(),
	NGUITools_t237277134_StaticFields::get_offset_of_mSides_5(),
	NGUITools_t237277134_StaticFields::get_offset_of_keys_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (PropertyBinding_t2650920656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[6] = 
{
	PropertyBinding_t2650920656::get_offset_of_source_2(),
	PropertyBinding_t2650920656::get_offset_of_target_3(),
	PropertyBinding_t2650920656::get_offset_of_direction_4(),
	PropertyBinding_t2650920656::get_offset_of_update_5(),
	PropertyBinding_t2650920656::get_offset_of_editMode_6(),
	PropertyBinding_t2650920656::get_offset_of_mLastValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (UpdateCondition_t1215877363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[5] = 
{
	UpdateCondition_t1215877363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (Direction_t260931296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2448[4] = 
{
	Direction_t260931296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (PropertyReference_t614957270), -1, sizeof(PropertyReference_t614957270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2449[5] = 
{
	PropertyReference_t614957270::get_offset_of_mTarget_0(),
	PropertyReference_t614957270::get_offset_of_mName_1(),
	PropertyReference_t614957270::get_offset_of_mField_2(),
	PropertyReference_t614957270::get_offset_of_mProperty_3(),
	PropertyReference_t614957270_StaticFields::get_offset_of_s_Hash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (RealTime_t3499460011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (SpringPanel_t929169367), -1, sizeof(SpringPanel_t929169367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2451[7] = 
{
	SpringPanel_t929169367_StaticFields::get_offset_of_current_2(),
	SpringPanel_t929169367::get_offset_of_target_3(),
	SpringPanel_t929169367::get_offset_of_strength_4(),
	SpringPanel_t929169367::get_offset_of_onFinished_5(),
	SpringPanel_t929169367::get_offset_of_mPanel_6(),
	SpringPanel_t929169367::get_offset_of_mTrans_7(),
	SpringPanel_t929169367::get_offset_of_mDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (OnFinished_t3316389065), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (UIBasicSprite_t2501337439), -1, sizeof(UIBasicSprite_t2501337439_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2453[14] = 
{
	UIBasicSprite_t2501337439::get_offset_of_mType_52(),
	UIBasicSprite_t2501337439::get_offset_of_mFillDirection_53(),
	UIBasicSprite_t2501337439::get_offset_of_mFillAmount_54(),
	UIBasicSprite_t2501337439::get_offset_of_mInvert_55(),
	UIBasicSprite_t2501337439::get_offset_of_mFlip_56(),
	UIBasicSprite_t2501337439::get_offset_of_mInnerUV_57(),
	UIBasicSprite_t2501337439::get_offset_of_mOuterUV_58(),
	UIBasicSprite_t2501337439::get_offset_of_centerType_59(),
	UIBasicSprite_t2501337439::get_offset_of_leftType_60(),
	UIBasicSprite_t2501337439::get_offset_of_rightType_61(),
	UIBasicSprite_t2501337439::get_offset_of_bottomType_62(),
	UIBasicSprite_t2501337439::get_offset_of_topType_63(),
	UIBasicSprite_t2501337439_StaticFields::get_offset_of_mTempPos_64(),
	UIBasicSprite_t2501337439_StaticFields::get_offset_of_mTempUVs_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (Type_t741864970)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2454[6] = 
{
	Type_t741864970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (FillDirection_t3514300524)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2455[6] = 
{
	FillDirection_t3514300524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (AdvancedType_t4228915532)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2456[4] = 
{
	AdvancedType_t4228915532::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (Flip_t741435197)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2457[5] = 
{
	Flip_t741435197::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (UIDrawCall_t913273974), -1, sizeof(UIDrawCall_t913273974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2458[35] = 
{
	0,
	UIDrawCall_t913273974_StaticFields::get_offset_of_mActiveList_3(),
	UIDrawCall_t913273974_StaticFields::get_offset_of_mInactiveList_4(),
	UIDrawCall_t913273974::get_offset_of_widgetCount_5(),
	UIDrawCall_t913273974::get_offset_of_depthStart_6(),
	UIDrawCall_t913273974::get_offset_of_depthEnd_7(),
	UIDrawCall_t913273974::get_offset_of_manager_8(),
	UIDrawCall_t913273974::get_offset_of_panel_9(),
	UIDrawCall_t913273974::get_offset_of_clipTexture_10(),
	UIDrawCall_t913273974::get_offset_of_alwaysOnScreen_11(),
	UIDrawCall_t913273974::get_offset_of_verts_12(),
	UIDrawCall_t913273974::get_offset_of_norms_13(),
	UIDrawCall_t913273974::get_offset_of_tans_14(),
	UIDrawCall_t913273974::get_offset_of_uvs_15(),
	UIDrawCall_t913273974::get_offset_of_cols_16(),
	UIDrawCall_t913273974::get_offset_of_mMaterial_17(),
	UIDrawCall_t913273974::get_offset_of_mTexture_18(),
	UIDrawCall_t913273974::get_offset_of_mShader_19(),
	UIDrawCall_t913273974::get_offset_of_mClipCount_20(),
	UIDrawCall_t913273974::get_offset_of_mTrans_21(),
	UIDrawCall_t913273974::get_offset_of_mMesh_22(),
	UIDrawCall_t913273974::get_offset_of_mFilter_23(),
	UIDrawCall_t913273974::get_offset_of_mRenderer_24(),
	UIDrawCall_t913273974::get_offset_of_mDynamicMat_25(),
	UIDrawCall_t913273974::get_offset_of_mIndices_26(),
	UIDrawCall_t913273974::get_offset_of_mRebuildMat_27(),
	UIDrawCall_t913273974::get_offset_of_mLegacyShader_28(),
	UIDrawCall_t913273974::get_offset_of_mRenderQueue_29(),
	UIDrawCall_t913273974::get_offset_of_mTriangles_30(),
	UIDrawCall_t913273974::get_offset_of_isDirty_31(),
	UIDrawCall_t913273974::get_offset_of_mTextureClip_32(),
	UIDrawCall_t913273974::get_offset_of_onRender_33(),
	UIDrawCall_t913273974_StaticFields::get_offset_of_mCache_34(),
	UIDrawCall_t913273974_StaticFields::get_offset_of_ClipRange_35(),
	UIDrawCall_t913273974_StaticFields::get_offset_of_ClipArgs_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (Clipping_t3937989211)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2459[5] = 
{
	Clipping_t3937989211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (OnRenderCallback_t2651898963), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (UIEventListener_t1278105402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[16] = 
{
	UIEventListener_t1278105402::get_offset_of_parameter_2(),
	UIEventListener_t1278105402::get_offset_of_onSubmit_3(),
	UIEventListener_t1278105402::get_offset_of_onClick_4(),
	UIEventListener_t1278105402::get_offset_of_onDoubleClick_5(),
	UIEventListener_t1278105402::get_offset_of_onHover_6(),
	UIEventListener_t1278105402::get_offset_of_onPress_7(),
	UIEventListener_t1278105402::get_offset_of_onSelect_8(),
	UIEventListener_t1278105402::get_offset_of_onScroll_9(),
	UIEventListener_t1278105402::get_offset_of_onDragStart_10(),
	UIEventListener_t1278105402::get_offset_of_onDrag_11(),
	UIEventListener_t1278105402::get_offset_of_onDragOver_12(),
	UIEventListener_t1278105402::get_offset_of_onDragOut_13(),
	UIEventListener_t1278105402::get_offset_of_onDragEnd_14(),
	UIEventListener_t1278105402::get_offset_of_onDrop_15(),
	UIEventListener_t1278105402::get_offset_of_onKey_16(),
	UIEventListener_t1278105402::get_offset_of_onTooltip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (VoidDelegate_t986119182), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (BoolDelegate_t4240584548), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (FloatDelegate_t3249382860), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (VectorDelegate_t3512109949), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (ObjectDelegate_t172593529), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (KeyCodeDelegate_t2040735132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (UIGeometry_t3586695974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[6] = 
{
	UIGeometry_t3586695974::get_offset_of_verts_0(),
	UIGeometry_t3586695974::get_offset_of_uvs_1(),
	UIGeometry_t3586695974::get_offset_of_cols_2(),
	UIGeometry_t3586695974::get_offset_of_mRtpVerts_3(),
	UIGeometry_t3586695974::get_offset_of_mRtpNormal_4(),
	UIGeometry_t3586695974::get_offset_of_mRtpTan_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (UIRect_t2503437976), -1, sizeof(UIRect_t2503437976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2469[20] = 
{
	UIRect_t2503437976::get_offset_of_leftAnchor_2(),
	UIRect_t2503437976::get_offset_of_rightAnchor_3(),
	UIRect_t2503437976::get_offset_of_bottomAnchor_4(),
	UIRect_t2503437976::get_offset_of_topAnchor_5(),
	UIRect_t2503437976::get_offset_of_updateAnchors_6(),
	UIRect_t2503437976::get_offset_of_mGo_7(),
	UIRect_t2503437976::get_offset_of_mTrans_8(),
	UIRect_t2503437976::get_offset_of_mChildren_9(),
	UIRect_t2503437976::get_offset_of_mChanged_10(),
	UIRect_t2503437976::get_offset_of_mStarted_11(),
	UIRect_t2503437976::get_offset_of_mParentFound_12(),
	UIRect_t2503437976::get_offset_of_mUpdateAnchors_13(),
	UIRect_t2503437976::get_offset_of_mUpdateFrame_14(),
	UIRect_t2503437976::get_offset_of_mAnchorsCached_15(),
	UIRect_t2503437976::get_offset_of_mRoot_16(),
	UIRect_t2503437976::get_offset_of_mParent_17(),
	UIRect_t2503437976::get_offset_of_mRootSet_18(),
	UIRect_t2503437976::get_offset_of_mCam_19(),
	UIRect_t2503437976::get_offset_of_finalAlpha_20(),
	UIRect_t2503437976_StaticFields::get_offset_of_mSides_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (AnchorPoint_t3581172420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[5] = 
{
	AnchorPoint_t3581172420::get_offset_of_target_0(),
	AnchorPoint_t3581172420::get_offset_of_relative_1(),
	AnchorPoint_t3581172420::get_offset_of_absolute_2(),
	AnchorPoint_t3581172420::get_offset_of_rect_3(),
	AnchorPoint_t3581172420::get_offset_of_targetCam_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (AnchorUpdate_t3786070549)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[4] = 
{
	AnchorUpdate_t3786070549::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (UISnapshotPoint_t3024846840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[6] = 
{
	UISnapshotPoint_t3024846840::get_offset_of_isOrthographic_2(),
	UISnapshotPoint_t3024846840::get_offset_of_nearClip_3(),
	UISnapshotPoint_t3024846840::get_offset_of_farClip_4(),
	UISnapshotPoint_t3024846840::get_offset_of_fieldOfView_5(),
	UISnapshotPoint_t3024846840::get_offset_of_orthoSize_6(),
	UISnapshotPoint_t3024846840::get_offset_of_thumbnail_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (UIWidget_t769069560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[30] = 
{
	UIWidget_t769069560::get_offset_of_mColor_22(),
	UIWidget_t769069560::get_offset_of_mPivot_23(),
	UIWidget_t769069560::get_offset_of_mWidth_24(),
	UIWidget_t769069560::get_offset_of_mHeight_25(),
	UIWidget_t769069560::get_offset_of_mDepth_26(),
	UIWidget_t769069560::get_offset_of_onChange_27(),
	UIWidget_t769069560::get_offset_of_onPostFill_28(),
	UIWidget_t769069560::get_offset_of_mOnRender_29(),
	UIWidget_t769069560::get_offset_of_autoResizeBoxCollider_30(),
	UIWidget_t769069560::get_offset_of_hideIfOffScreen_31(),
	UIWidget_t769069560::get_offset_of_keepAspectRatio_32(),
	UIWidget_t769069560::get_offset_of_aspectRatio_33(),
	UIWidget_t769069560::get_offset_of_hitCheck_34(),
	UIWidget_t769069560::get_offset_of_panel_35(),
	UIWidget_t769069560::get_offset_of_geometry_36(),
	UIWidget_t769069560::get_offset_of_fillGeometry_37(),
	UIWidget_t769069560::get_offset_of_mPlayMode_38(),
	UIWidget_t769069560::get_offset_of_mDrawRegion_39(),
	UIWidget_t769069560::get_offset_of_mLocalToPanel_40(),
	UIWidget_t769069560::get_offset_of_mIsVisibleByAlpha_41(),
	UIWidget_t769069560::get_offset_of_mIsVisibleByPanel_42(),
	UIWidget_t769069560::get_offset_of_mIsInFront_43(),
	UIWidget_t769069560::get_offset_of_mLastAlpha_44(),
	UIWidget_t769069560::get_offset_of_mMoved_45(),
	UIWidget_t769069560::get_offset_of_drawCall_46(),
	UIWidget_t769069560::get_offset_of_mCorners_47(),
	UIWidget_t769069560::get_offset_of_mAlphaFrameID_48(),
	UIWidget_t769069560::get_offset_of_mMatrixFrame_49(),
	UIWidget_t769069560::get_offset_of_mOldV0_50(),
	UIWidget_t769069560::get_offset_of_mOldV1_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (Pivot_t240933195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2474[10] = 
{
	Pivot_t240933195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (AspectRatioSource_t788096023)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2475[4] = 
{
	AspectRatioSource_t788096023::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (OnDimensionsChanged_t3695058769), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (OnPostFillCallback_t3030491454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (HitCheck_t3889696652), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (AnimatedAlpha_t3219346779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[3] = 
{
	AnimatedAlpha_t3219346779::get_offset_of_alpha_2(),
	AnimatedAlpha_t3219346779::get_offset_of_mWidget_3(),
	AnimatedAlpha_t3219346779::get_offset_of_mPanel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (AnimatedColor_t3221279584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[2] = 
{
	AnimatedColor_t3221279584::get_offset_of_color_2(),
	AnimatedColor_t3221279584::get_offset_of_mWidget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (AnimatedWidget_t1642214887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[3] = 
{
	AnimatedWidget_t1642214887::get_offset_of_width_2(),
	AnimatedWidget_t1642214887::get_offset_of_height_3(),
	AnimatedWidget_t1642214887::get_offset_of_mWidget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (SpringPosition_t3802689142), -1, sizeof(SpringPosition_t3802689142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2482[12] = 
{
	SpringPosition_t3802689142_StaticFields::get_offset_of_current_2(),
	SpringPosition_t3802689142::get_offset_of_target_3(),
	SpringPosition_t3802689142::get_offset_of_strength_4(),
	SpringPosition_t3802689142::get_offset_of_worldSpace_5(),
	SpringPosition_t3802689142::get_offset_of_ignoreTimeScale_6(),
	SpringPosition_t3802689142::get_offset_of_updateScrollView_7(),
	SpringPosition_t3802689142::get_offset_of_onFinished_8(),
	SpringPosition_t3802689142::get_offset_of_eventReceiver_9(),
	SpringPosition_t3802689142::get_offset_of_callWhenFinished_10(),
	SpringPosition_t3802689142::get_offset_of_mTrans_11(),
	SpringPosition_t3802689142::get_offset_of_mThreshold_12(),
	SpringPosition_t3802689142::get_offset_of_mSv_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (OnFinished_t2399837386), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (TweenAlpha_t2920325587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[6] = 
{
	TweenAlpha_t2920325587::get_offset_of_from_20(),
	TweenAlpha_t2920325587::get_offset_of_to_21(),
	TweenAlpha_t2920325587::get_offset_of_mCached_22(),
	TweenAlpha_t2920325587::get_offset_of_mRect_23(),
	TweenAlpha_t2920325587::get_offset_of_mMat_24(),
	TweenAlpha_t2920325587::get_offset_of_mSr_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (TweenColor_t2922258392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[7] = 
{
	TweenColor_t2922258392::get_offset_of_from_20(),
	TweenColor_t2922258392::get_offset_of_to_21(),
	TweenColor_t2922258392::get_offset_of_mCached_22(),
	TweenColor_t2922258392::get_offset_of_mWidget_23(),
	TweenColor_t2922258392::get_offset_of_mMat_24(),
	TweenColor_t2922258392::get_offset_of_mLight_25(),
	TweenColor_t2922258392::get_offset_of_mSr_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (TweenFOV_t1594102402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[3] = 
{
	TweenFOV_t1594102402::get_offset_of_from_20(),
	TweenFOV_t1594102402::get_offset_of_to_21(),
	TweenFOV_t1594102402::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (TweenHeight_t529510226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[5] = 
{
	TweenHeight_t529510226::get_offset_of_from_20(),
	TweenHeight_t529510226::get_offset_of_to_21(),
	TweenHeight_t529510226::get_offset_of_updateTable_22(),
	TweenHeight_t529510226::get_offset_of_mWidget_23(),
	TweenHeight_t529510226::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (TweenOrthoSize_t1845504462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[3] = 
{
	TweenOrthoSize_t1845504462::get_offset_of_from_20(),
	TweenOrthoSize_t1845504462::get_offset_of_to_21(),
	TweenOrthoSize_t1845504462::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (TweenPosition_t3684358292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[5] = 
{
	TweenPosition_t3684358292::get_offset_of_from_20(),
	TweenPosition_t3684358292::get_offset_of_to_21(),
	TweenPosition_t3684358292::get_offset_of_worldSpace_22(),
	TweenPosition_t3684358292::get_offset_of_mTrans_23(),
	TweenPosition_t3684358292::get_offset_of_mRect_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (TweenRotation_t2896252649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[4] = 
{
	TweenRotation_t2896252649::get_offset_of_from_20(),
	TweenRotation_t2896252649::get_offset_of_to_21(),
	TweenRotation_t2896252649::get_offset_of_quaternionLerp_22(),
	TweenRotation_t2896252649::get_offset_of_mTrans_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (TweenScale_t2936666559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[5] = 
{
	TweenScale_t2936666559::get_offset_of_from_20(),
	TweenScale_t2936666559::get_offset_of_to_21(),
	TweenScale_t2936666559::get_offset_of_updateTable_22(),
	TweenScale_t2936666559::get_offset_of_mTrans_23(),
	TweenScale_t2936666559::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (TweenTransform_t1891506529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[7] = 
{
	TweenTransform_t1891506529::get_offset_of_from_20(),
	TweenTransform_t1891506529::get_offset_of_to_21(),
	TweenTransform_t1891506529::get_offset_of_parentWhenFinished_22(),
	TweenTransform_t1891506529::get_offset_of_mTrans_23(),
	TweenTransform_t1891506529::get_offset_of_mPos_24(),
	TweenTransform_t1891506529::get_offset_of_mRot_25(),
	TweenTransform_t1891506529::get_offset_of_mScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (TweenVolume_t939656517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[3] = 
{
	TweenVolume_t939656517::get_offset_of_from_20(),
	TweenVolume_t939656517::get_offset_of_to_21(),
	TweenVolume_t939656517::get_offset_of_mSource_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (TweenWidth_t2940542523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[5] = 
{
	TweenWidth_t2940542523::get_offset_of_from_20(),
	TweenWidth_t2940542523::get_offset_of_to_21(),
	TweenWidth_t2940542523::get_offset_of_updateTable_22(),
	TweenWidth_t2940542523::get_offset_of_mWidget_23(),
	TweenWidth_t2940542523::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (UITweener_t105489188), -1, sizeof(UITweener_t105489188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2495[18] = 
{
	UITweener_t105489188_StaticFields::get_offset_of_current_2(),
	UITweener_t105489188::get_offset_of_method_3(),
	UITweener_t105489188::get_offset_of_style_4(),
	UITweener_t105489188::get_offset_of_animationCurve_5(),
	UITweener_t105489188::get_offset_of_ignoreTimeScale_6(),
	UITweener_t105489188::get_offset_of_delay_7(),
	UITweener_t105489188::get_offset_of_duration_8(),
	UITweener_t105489188::get_offset_of_steeperCurves_9(),
	UITweener_t105489188::get_offset_of_tweenGroup_10(),
	UITweener_t105489188::get_offset_of_onFinished_11(),
	UITweener_t105489188::get_offset_of_eventReceiver_12(),
	UITweener_t105489188::get_offset_of_callWhenFinished_13(),
	UITweener_t105489188::get_offset_of_mStarted_14(),
	UITweener_t105489188::get_offset_of_mStartTime_15(),
	UITweener_t105489188::get_offset_of_mDuration_16(),
	UITweener_t105489188::get_offset_of_mAmountPerDelta_17(),
	UITweener_t105489188::get_offset_of_mFactor_18(),
	UITweener_t105489188::get_offset_of_mTemp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (Method_t1078127180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2496[7] = 
{
	Method_t1078127180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (Style_t3365907174)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2497[4] = 
{
	Style_t3365907174::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (UI2DSprite_t1326097995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[8] = 
{
	UI2DSprite_t1326097995::get_offset_of_mSprite_66(),
	UI2DSprite_t1326097995::get_offset_of_mMat_67(),
	UI2DSprite_t1326097995::get_offset_of_mShader_68(),
	UI2DSprite_t1326097995::get_offset_of_mBorder_69(),
	UI2DSprite_t1326097995::get_offset_of_mFixedAspect_70(),
	UI2DSprite_t1326097995::get_offset_of_mPixelSize_71(),
	UI2DSprite_t1326097995::get_offset_of_nextSprite_72(),
	UI2DSprite_t1326097995::get_offset_of_mPMA_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (UI2DSpriteAnimation_t3878779257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[8] = 
{
	UI2DSpriteAnimation_t3878779257::get_offset_of_framerate_2(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_ignoreTimeScale_3(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_loop_4(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_frames_5(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_mUnitySprite_6(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_mNguiSprite_7(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_mIndex_8(),
	UI2DSpriteAnimation_t3878779257::get_offset_of_mUpdate_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
