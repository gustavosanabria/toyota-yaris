﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIAnchor143817321.h"
#include "AssemblyU2DCSharp_UIAnchor_Side1741963613.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "AssemblyU2DCSharp_UIAtlas_Sprite2668923293.h"
#include "AssemblyU2DCSharp_UIAtlas_Coordinates3883285027.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme2923531404.h"
#include "AssemblyU2DCSharp_UICamera_ClickNotification564146365.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch2057376333.h"
#include "AssemblyU2DCSharp_UICamera_EventType3387030814.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "AssemblyU2DCSharp_UICamera_Touch2499143785.h"
#include "AssemblyU2DCSharp_UICamera_GetKeyStateFunc2745734774.h"
#include "AssemblyU2DCSharp_UICamera_GetAxisFunc783615269.h"
#include "AssemblyU2DCSharp_UICamera_GetAnyKeyFunc4194451799.h"
#include "AssemblyU2DCSharp_UICamera_OnScreenResize3828578773.h"
#include "AssemblyU2DCSharp_UICamera_OnCustomInput736305828.h"
#include "AssemblyU2DCSharp_UICamera_OnSchemeChange2731530058.h"
#include "AssemblyU2DCSharp_UICamera_MoveDelegate1906135052.h"
#include "AssemblyU2DCSharp_UICamera_VoidDelegate3135042255.h"
#include "AssemblyU2DCSharp_UICamera_BoolDelegate2094540325.h"
#include "AssemblyU2DCSharp_UICamera_FloatDelegate1146521387.h"
#include "AssemblyU2DCSharp_UICamera_VectorDelegate2747913726.h"
#include "AssemblyU2DCSharp_UICamera_ObjectDelegate3703364602.h"
#include "AssemblyU2DCSharp_UICamera_KeyCodeDelegate4120455995.h"
#include "AssemblyU2DCSharp_UICamera_GetTouchCountCallback3435560085.h"
#include "AssemblyU2DCSharp_UICamera_GetTouchCallback1934354820.h"
#include "AssemblyU2DCSharp_UIColorPicker4217226909.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_UIInput289134998.h"
#include "AssemblyU2DCSharp_UIInput_InputType3551371467.h"
#include "AssemblyU2DCSharp_UIInput_Validation1298096818.h"
#include "AssemblyU2DCSharp_UIInput_KeyboardType1053727674.h"
#include "AssemblyU2DCSharp_UIInput_OnReturnKey1979253975.h"
#include "AssemblyU2DCSharp_UIInput_OnValidate2690340430.h"
#include "AssemblyU2DCSharp_UIInputOnGUI3418819302.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UILabel_Effect3176279520.h"
#include "AssemblyU2DCSharp_UILabel_Overflow229724305.h"
#include "AssemblyU2DCSharp_UILabel_Crispness3952758239.h"
#include "AssemblyU2DCSharp_UILocalize3641481405.h"
#include "AssemblyU2DCSharp_UIOrthoCamera3902783945.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UIPanel_RenderQueue3352612156.h"
#include "AssemblyU2DCSharp_UIPanel_OnGeometryUpdated4285773675.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved1586118419.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "AssemblyU2DCSharp_UIRoot_Scaling174157806.h"
#include "AssemblyU2DCSharp_UIRoot_Constraint2067133462.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UISpriteAnimation4279777547.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "AssemblyU2DCSharp_UIStretch3439076817.h"
#include "AssemblyU2DCSharp_UIStretch_Style3950950355.h"
#include "AssemblyU2DCSharp_UITextList736798239.h"
#include "AssemblyU2DCSharp_UITextList_Style464733601.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph3894573406.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_UITooltip4180872911.h"
#include "AssemblyU2DCSharp_UIViewport2937361242.h"
#include "AssemblyU2DCSharp_GyroscopeControllerFullFreedom685599910.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler3535974956.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_EEr3259205415.h"
#include "AssemblyU2DCSharp_Utils_Core_AndroidOBBHandler_U3C3158326693.h"
#include "AssemblyU2DCSharp_Utils_Core_DataBuilder3810402835.h"
#include "AssemblyU2DCSharp_Utils_Core_FileDescription856434530.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper1028318256.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_EFolder738843808.h"
#include "AssemblyU2DCSharp_Utils_Core_FileHelper_U3CCopyToP3141760867.h"
#include "AssemblyU2DCSharp_Utils_Vuforia_DataSetHandler896450754.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour1927425041.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3993405419.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour1850077856.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour2989373670.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErr3746290221.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH543367463.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan4108278998.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler4275497481.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour439862723.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour1735871187.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer1535493961.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB2489055709.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer2749231339.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponent900168586.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1171410903.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour3170674893.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour1204933533.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour2222860085.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour2508606743.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour3213561028.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour3695833539.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetB582925864.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehavi442856755.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour3457144850.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour892056475.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour1278464685.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour3289283011.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin3016919996.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper1694779206.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (UIAnchor_t143817321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[12] = 
{
	UIAnchor_t143817321::get_offset_of_uiCamera_2(),
	UIAnchor_t143817321::get_offset_of_container_3(),
	UIAnchor_t143817321::get_offset_of_side_4(),
	UIAnchor_t143817321::get_offset_of_runOnlyOnce_5(),
	UIAnchor_t143817321::get_offset_of_relativeOffset_6(),
	UIAnchor_t143817321::get_offset_of_pixelOffset_7(),
	UIAnchor_t143817321::get_offset_of_widgetContainer_8(),
	UIAnchor_t143817321::get_offset_of_mTrans_9(),
	UIAnchor_t143817321::get_offset_of_mAnim_10(),
	UIAnchor_t143817321::get_offset_of_mRect_11(),
	UIAnchor_t143817321::get_offset_of_mRoot_12(),
	UIAnchor_t143817321::get_offset_of_mStarted_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Side_t1741963613)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2501[10] = 
{
	Side_t1741963613::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (UIAtlas_t281921111), -1, sizeof(UIAtlas_t281921111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2502[9] = 
{
	UIAtlas_t281921111::get_offset_of_material_2(),
	UIAtlas_t281921111::get_offset_of_mSprites_3(),
	UIAtlas_t281921111::get_offset_of_mPixelSize_4(),
	UIAtlas_t281921111::get_offset_of_mReplacement_5(),
	UIAtlas_t281921111::get_offset_of_mCoordinates_6(),
	UIAtlas_t281921111::get_offset_of_sprites_7(),
	UIAtlas_t281921111::get_offset_of_mPMA_8(),
	UIAtlas_t281921111::get_offset_of_mSpriteIndices_9(),
	UIAtlas_t281921111_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Sprite_t2668923293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[8] = 
{
	Sprite_t2668923293::get_offset_of_name_0(),
	Sprite_t2668923293::get_offset_of_outer_1(),
	Sprite_t2668923293::get_offset_of_inner_2(),
	Sprite_t2668923293::get_offset_of_rotated_3(),
	Sprite_t2668923293::get_offset_of_paddingLeft_4(),
	Sprite_t2668923293::get_offset_of_paddingRight_5(),
	Sprite_t2668923293::get_offset_of_paddingTop_6(),
	Sprite_t2668923293::get_offset_of_paddingBottom_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (Coordinates_t3883285027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[3] = 
{
	Coordinates_t3883285027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (UICamera_t189364953), -1, sizeof(UICamera_t189364953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2505[91] = 
{
	UICamera_t189364953_StaticFields::get_offset_of_list_2(),
	UICamera_t189364953_StaticFields::get_offset_of_GetKeyDown_3(),
	UICamera_t189364953_StaticFields::get_offset_of_GetKeyUp_4(),
	UICamera_t189364953_StaticFields::get_offset_of_GetKey_5(),
	UICamera_t189364953_StaticFields::get_offset_of_GetAxis_6(),
	UICamera_t189364953_StaticFields::get_offset_of_GetAnyKeyDown_7(),
	UICamera_t189364953_StaticFields::get_offset_of_onScreenResize_8(),
	UICamera_t189364953::get_offset_of_eventType_9(),
	UICamera_t189364953::get_offset_of_eventsGoToColliders_10(),
	UICamera_t189364953::get_offset_of_eventReceiverMask_11(),
	UICamera_t189364953::get_offset_of_debug_12(),
	UICamera_t189364953::get_offset_of_useMouse_13(),
	UICamera_t189364953::get_offset_of_useTouch_14(),
	UICamera_t189364953::get_offset_of_allowMultiTouch_15(),
	UICamera_t189364953::get_offset_of_useKeyboard_16(),
	UICamera_t189364953::get_offset_of_useController_17(),
	UICamera_t189364953::get_offset_of_stickyTooltip_18(),
	UICamera_t189364953::get_offset_of_tooltipDelay_19(),
	UICamera_t189364953::get_offset_of_longPressTooltip_20(),
	UICamera_t189364953::get_offset_of_mouseDragThreshold_21(),
	UICamera_t189364953::get_offset_of_mouseClickThreshold_22(),
	UICamera_t189364953::get_offset_of_touchDragThreshold_23(),
	UICamera_t189364953::get_offset_of_touchClickThreshold_24(),
	UICamera_t189364953::get_offset_of_rangeDistance_25(),
	UICamera_t189364953::get_offset_of_horizontalAxisName_26(),
	UICamera_t189364953::get_offset_of_verticalAxisName_27(),
	UICamera_t189364953::get_offset_of_horizontalPanAxisName_28(),
	UICamera_t189364953::get_offset_of_verticalPanAxisName_29(),
	UICamera_t189364953::get_offset_of_scrollAxisName_30(),
	UICamera_t189364953::get_offset_of_commandClick_31(),
	UICamera_t189364953::get_offset_of_submitKey0_32(),
	UICamera_t189364953::get_offset_of_submitKey1_33(),
	UICamera_t189364953::get_offset_of_cancelKey0_34(),
	UICamera_t189364953::get_offset_of_cancelKey1_35(),
	UICamera_t189364953::get_offset_of_autoHideCursor_36(),
	UICamera_t189364953_StaticFields::get_offset_of_onCustomInput_37(),
	UICamera_t189364953_StaticFields::get_offset_of_showTooltips_38(),
	UICamera_t189364953_StaticFields::get_offset_of_mDisableController_39(),
	UICamera_t189364953_StaticFields::get_offset_of_mLastPos_40(),
	UICamera_t189364953_StaticFields::get_offset_of_lastWorldPosition_41(),
	UICamera_t189364953_StaticFields::get_offset_of_lastHit_42(),
	UICamera_t189364953_StaticFields::get_offset_of_current_43(),
	UICamera_t189364953_StaticFields::get_offset_of_currentCamera_44(),
	UICamera_t189364953_StaticFields::get_offset_of_onSchemeChange_45(),
	UICamera_t189364953_StaticFields::get_offset_of_currentTouchID_46(),
	UICamera_t189364953_StaticFields::get_offset_of_mCurrentKey_47(),
	UICamera_t189364953_StaticFields::get_offset_of_currentTouch_48(),
	UICamera_t189364953_StaticFields::get_offset_of_mInputFocus_49(),
	UICamera_t189364953_StaticFields::get_offset_of_mGenericHandler_50(),
	UICamera_t189364953_StaticFields::get_offset_of_fallThrough_51(),
	UICamera_t189364953_StaticFields::get_offset_of_onClick_52(),
	UICamera_t189364953_StaticFields::get_offset_of_onDoubleClick_53(),
	UICamera_t189364953_StaticFields::get_offset_of_onHover_54(),
	UICamera_t189364953_StaticFields::get_offset_of_onPress_55(),
	UICamera_t189364953_StaticFields::get_offset_of_onSelect_56(),
	UICamera_t189364953_StaticFields::get_offset_of_onScroll_57(),
	UICamera_t189364953_StaticFields::get_offset_of_onDrag_58(),
	UICamera_t189364953_StaticFields::get_offset_of_onDragStart_59(),
	UICamera_t189364953_StaticFields::get_offset_of_onDragOver_60(),
	UICamera_t189364953_StaticFields::get_offset_of_onDragOut_61(),
	UICamera_t189364953_StaticFields::get_offset_of_onDragEnd_62(),
	UICamera_t189364953_StaticFields::get_offset_of_onDrop_63(),
	UICamera_t189364953_StaticFields::get_offset_of_onKey_64(),
	UICamera_t189364953_StaticFields::get_offset_of_onNavigate_65(),
	UICamera_t189364953_StaticFields::get_offset_of_onPan_66(),
	UICamera_t189364953_StaticFields::get_offset_of_onTooltip_67(),
	UICamera_t189364953_StaticFields::get_offset_of_onMouseMove_68(),
	UICamera_t189364953_StaticFields::get_offset_of_mMouse_69(),
	UICamera_t189364953_StaticFields::get_offset_of_controller_70(),
	UICamera_t189364953_StaticFields::get_offset_of_activeTouches_71(),
	UICamera_t189364953_StaticFields::get_offset_of_mTouchIDs_72(),
	UICamera_t189364953_StaticFields::get_offset_of_mWidth_73(),
	UICamera_t189364953_StaticFields::get_offset_of_mHeight_74(),
	UICamera_t189364953_StaticFields::get_offset_of_mTooltip_75(),
	UICamera_t189364953::get_offset_of_mCam_76(),
	UICamera_t189364953_StaticFields::get_offset_of_mTooltipTime_77(),
	UICamera_t189364953::get_offset_of_mNextRaycast_78(),
	UICamera_t189364953_StaticFields::get_offset_of_isDragging_79(),
	UICamera_t189364953_StaticFields::get_offset_of_mRayHitObject_80(),
	UICamera_t189364953_StaticFields::get_offset_of_mHover_81(),
	UICamera_t189364953_StaticFields::get_offset_of_mSelected_82(),
	UICamera_t189364953_StaticFields::get_offset_of_mHit_83(),
	UICamera_t189364953_StaticFields::get_offset_of_mHits_84(),
	UICamera_t189364953_StaticFields::get_offset_of_m2DPlane_85(),
	UICamera_t189364953_StaticFields::get_offset_of_mNextEvent_86(),
	UICamera_t189364953_StaticFields::get_offset_of_mNotifying_87(),
	UICamera_t189364953_StaticFields::get_offset_of_mUsingTouchEvents_88(),
	UICamera_t189364953_StaticFields::get_offset_of_GetInputTouchCount_89(),
	UICamera_t189364953_StaticFields::get_offset_of_GetInputTouch_90(),
	UICamera_t189364953_StaticFields::get_offset_of_U3CU3Ef__amU24cache59_91(),
	UICamera_t189364953_StaticFields::get_offset_of_U3CU3Ef__amU24cache5A_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (ControlScheme_t2923531404)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2506[4] = 
{
	ControlScheme_t2923531404::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (ClickNotification_t564146365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[4] = 
{
	ClickNotification_t564146365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (MouseOrTouch_t2057376333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[17] = 
{
	MouseOrTouch_t2057376333::get_offset_of_key_0(),
	MouseOrTouch_t2057376333::get_offset_of_pos_1(),
	MouseOrTouch_t2057376333::get_offset_of_lastPos_2(),
	MouseOrTouch_t2057376333::get_offset_of_delta_3(),
	MouseOrTouch_t2057376333::get_offset_of_totalDelta_4(),
	MouseOrTouch_t2057376333::get_offset_of_pressedCam_5(),
	MouseOrTouch_t2057376333::get_offset_of_last_6(),
	MouseOrTouch_t2057376333::get_offset_of_current_7(),
	MouseOrTouch_t2057376333::get_offset_of_pressed_8(),
	MouseOrTouch_t2057376333::get_offset_of_dragged_9(),
	MouseOrTouch_t2057376333::get_offset_of_pressTime_10(),
	MouseOrTouch_t2057376333::get_offset_of_clickTime_11(),
	MouseOrTouch_t2057376333::get_offset_of_clickNotification_12(),
	MouseOrTouch_t2057376333::get_offset_of_touchBegan_13(),
	MouseOrTouch_t2057376333::get_offset_of_pressStarted_14(),
	MouseOrTouch_t2057376333::get_offset_of_dragStarted_15(),
	MouseOrTouch_t2057376333::get_offset_of_ignoreDelta_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (EventType_t3387030814)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[5] = 
{
	EventType_t3387030814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (DepthEntry_t1145614469)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[4] = 
{
	DepthEntry_t1145614469::get_offset_of_depth_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t1145614469::get_offset_of_hit_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t1145614469::get_offset_of_point_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t1145614469::get_offset_of_go_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (Touch_t2499143785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[4] = 
{
	Touch_t2499143785::get_offset_of_fingerId_0(),
	Touch_t2499143785::get_offset_of_phase_1(),
	Touch_t2499143785::get_offset_of_position_2(),
	Touch_t2499143785::get_offset_of_tapCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (GetKeyStateFunc_t2745734774), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (GetAxisFunc_t783615269), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (GetAnyKeyFunc_t4194451799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (OnScreenResize_t3828578773), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (OnCustomInput_t736305828), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (OnSchemeChange_t2731530058), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (MoveDelegate_t1906135052), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (VoidDelegate_t3135042255), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (BoolDelegate_t2094540325), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (FloatDelegate_t1146521387), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (VectorDelegate_t2747913726), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (ObjectDelegate_t3703364602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (KeyCodeDelegate_t4120455995), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (GetTouchCountCallback_t3435560085), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (GetTouchCallback_t1934354820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (UIColorPicker_t4217226909), -1, sizeof(UIColorPicker_t4217226909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[14] = 
{
	UIColorPicker_t4217226909_StaticFields::get_offset_of_current_2(),
	UIColorPicker_t4217226909::get_offset_of_value_3(),
	UIColorPicker_t4217226909::get_offset_of_selectionWidget_4(),
	UIColorPicker_t4217226909::get_offset_of_onChange_5(),
	UIColorPicker_t4217226909::get_offset_of_mTrans_6(),
	UIColorPicker_t4217226909::get_offset_of_mUITex_7(),
	UIColorPicker_t4217226909::get_offset_of_mTex_8(),
	UIColorPicker_t4217226909::get_offset_of_mCam_9(),
	UIColorPicker_t4217226909::get_offset_of_mPos_10(),
	UIColorPicker_t4217226909::get_offset_of_mWidth_11(),
	UIColorPicker_t4217226909::get_offset_of_mHeight_12(),
	UIColorPicker_t4217226909_StaticFields::get_offset_of_mRed_13(),
	UIColorPicker_t4217226909_StaticFields::get_offset_of_mGreen_14(),
	UIColorPicker_t4217226909_StaticFields::get_offset_of_mBlue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (UIFont_t2503090435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[12] = 
{
	UIFont_t2503090435::get_offset_of_mMat_2(),
	UIFont_t2503090435::get_offset_of_mUVRect_3(),
	UIFont_t2503090435::get_offset_of_mFont_4(),
	UIFont_t2503090435::get_offset_of_mAtlas_5(),
	UIFont_t2503090435::get_offset_of_mReplacement_6(),
	UIFont_t2503090435::get_offset_of_mSymbols_7(),
	UIFont_t2503090435::get_offset_of_mDynamicFont_8(),
	UIFont_t2503090435::get_offset_of_mDynamicFontSize_9(),
	UIFont_t2503090435::get_offset_of_mDynamicFontStyle_10(),
	UIFont_t2503090435::get_offset_of_mSprite_11(),
	UIFont_t2503090435::get_offset_of_mPMA_12(),
	UIFont_t2503090435::get_offset_of_mPacked_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (UIInput_t289134998), -1, sizeof(UIInput_t289134998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2529[42] = 
{
	UIInput_t289134998_StaticFields::get_offset_of_current_2(),
	UIInput_t289134998_StaticFields::get_offset_of_selection_3(),
	UIInput_t289134998::get_offset_of_label_4(),
	UIInput_t289134998::get_offset_of_inputType_5(),
	UIInput_t289134998::get_offset_of_onReturnKey_6(),
	UIInput_t289134998::get_offset_of_keyboardType_7(),
	UIInput_t289134998::get_offset_of_hideInput_8(),
	UIInput_t289134998::get_offset_of_selectAllTextOnFocus_9(),
	UIInput_t289134998::get_offset_of_validation_10(),
	UIInput_t289134998::get_offset_of_characterLimit_11(),
	UIInput_t289134998::get_offset_of_savedAs_12(),
	UIInput_t289134998::get_offset_of_selectOnTab_13(),
	UIInput_t289134998::get_offset_of_activeTextColor_14(),
	UIInput_t289134998::get_offset_of_caretColor_15(),
	UIInput_t289134998::get_offset_of_selectionColor_16(),
	UIInput_t289134998::get_offset_of_onSubmit_17(),
	UIInput_t289134998::get_offset_of_onChange_18(),
	UIInput_t289134998::get_offset_of_onValidate_19(),
	UIInput_t289134998::get_offset_of_mValue_20(),
	UIInput_t289134998::get_offset_of_mDefaultText_21(),
	UIInput_t289134998::get_offset_of_mDefaultColor_22(),
	UIInput_t289134998::get_offset_of_mPosition_23(),
	UIInput_t289134998::get_offset_of_mDoInit_24(),
	UIInput_t289134998::get_offset_of_mPivot_25(),
	UIInput_t289134998::get_offset_of_mLoadSavedValue_26(),
	UIInput_t289134998_StaticFields::get_offset_of_mDrawStart_27(),
	UIInput_t289134998_StaticFields::get_offset_of_mLastIME_28(),
	UIInput_t289134998_StaticFields::get_offset_of_mKeyboard_29(),
	UIInput_t289134998_StaticFields::get_offset_of_mWaitForKeyboard_30(),
	UIInput_t289134998::get_offset_of_mSelectionStart_31(),
	UIInput_t289134998::get_offset_of_mSelectionEnd_32(),
	UIInput_t289134998::get_offset_of_mHighlight_33(),
	UIInput_t289134998::get_offset_of_mCaret_34(),
	UIInput_t289134998::get_offset_of_mBlankTex_35(),
	UIInput_t289134998::get_offset_of_mNextBlink_36(),
	UIInput_t289134998::get_offset_of_mLastAlpha_37(),
	UIInput_t289134998::get_offset_of_mCached_38(),
	UIInput_t289134998::get_offset_of_mSelectMe_39(),
	UIInput_t289134998::get_offset_of_mSelectTime_40(),
	UIInput_t289134998::get_offset_of_mCam_41(),
	UIInput_t289134998::get_offset_of_mEllipsis_42(),
	UIInput_t289134998_StaticFields::get_offset_of_mIgnoreKey_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (InputType_t3551371467)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2530[4] = 
{
	InputType_t3551371467::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (Validation_t1298096818)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[8] = 
{
	Validation_t1298096818::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (KeyboardType_t1053727674)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2532[9] = 
{
	KeyboardType_t1053727674::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (OnReturnKey_t1979253975)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2533[4] = 
{
	OnReturnKey_t1979253975::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (OnValidate_t2690340430), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (UIInputOnGUI_t3418819302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (UILabel_t291504320), -1, sizeof(UILabel_t291504320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2536[45] = 
{
	UILabel_t291504320::get_offset_of_keepCrispWhenShrunk_52(),
	UILabel_t291504320::get_offset_of_mTrueTypeFont_53(),
	UILabel_t291504320::get_offset_of_mFont_54(),
	UILabel_t291504320::get_offset_of_mText_55(),
	UILabel_t291504320::get_offset_of_mFontSize_56(),
	UILabel_t291504320::get_offset_of_mFontStyle_57(),
	UILabel_t291504320::get_offset_of_mAlignment_58(),
	UILabel_t291504320::get_offset_of_mEncoding_59(),
	UILabel_t291504320::get_offset_of_mMaxLineCount_60(),
	UILabel_t291504320::get_offset_of_mEffectStyle_61(),
	UILabel_t291504320::get_offset_of_mEffectColor_62(),
	UILabel_t291504320::get_offset_of_mSymbols_63(),
	UILabel_t291504320::get_offset_of_mEffectDistance_64(),
	UILabel_t291504320::get_offset_of_mOverflow_65(),
	UILabel_t291504320::get_offset_of_mMaterial_66(),
	UILabel_t291504320::get_offset_of_mApplyGradient_67(),
	UILabel_t291504320::get_offset_of_mGradientTop_68(),
	UILabel_t291504320::get_offset_of_mGradientBottom_69(),
	UILabel_t291504320::get_offset_of_mSpacingX_70(),
	UILabel_t291504320::get_offset_of_mSpacingY_71(),
	UILabel_t291504320::get_offset_of_mUseFloatSpacing_72(),
	UILabel_t291504320::get_offset_of_mFloatSpacingX_73(),
	UILabel_t291504320::get_offset_of_mFloatSpacingY_74(),
	UILabel_t291504320::get_offset_of_mOverflowEllipsis_75(),
	UILabel_t291504320::get_offset_of_mShrinkToFit_76(),
	UILabel_t291504320::get_offset_of_mMaxLineWidth_77(),
	UILabel_t291504320::get_offset_of_mMaxLineHeight_78(),
	UILabel_t291504320::get_offset_of_mLineWidth_79(),
	UILabel_t291504320::get_offset_of_mMultiline_80(),
	UILabel_t291504320::get_offset_of_mActiveTTF_81(),
	UILabel_t291504320::get_offset_of_mDensity_82(),
	UILabel_t291504320::get_offset_of_mShouldBeProcessed_83(),
	UILabel_t291504320::get_offset_of_mProcessedText_84(),
	UILabel_t291504320::get_offset_of_mPremultiply_85(),
	UILabel_t291504320::get_offset_of_mCalculatedSize_86(),
	UILabel_t291504320::get_offset_of_mScale_87(),
	UILabel_t291504320::get_offset_of_mFinalFontSize_88(),
	UILabel_t291504320::get_offset_of_mLastWidth_89(),
	UILabel_t291504320::get_offset_of_mLastHeight_90(),
	UILabel_t291504320_StaticFields::get_offset_of_mList_91(),
	UILabel_t291504320_StaticFields::get_offset_of_mFontUsage_92(),
	UILabel_t291504320_StaticFields::get_offset_of_mTempPanelList_93(),
	UILabel_t291504320_StaticFields::get_offset_of_mTexRebuildAdded_94(),
	UILabel_t291504320_StaticFields::get_offset_of_mTempVerts_95(),
	UILabel_t291504320_StaticFields::get_offset_of_mTempIndices_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (Effect_t3176279520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2537[5] = 
{
	Effect_t3176279520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (Overflow_t229724305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[5] = 
{
	Overflow_t229724305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (Crispness_t3952758239)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	Crispness_t3952758239::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (UILocalize_t3641481405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	UILocalize_t3641481405::get_offset_of_key_2(),
	UILocalize_t3641481405::get_offset_of_mStarted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (UIOrthoCamera_t3902783945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[2] = 
{
	UIOrthoCamera_t3902783945::get_offset_of_mCam_2(),
	UIOrthoCamera_t3902783945::get_offset_of_mTrans_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (UIPanel_t295209936), -1, sizeof(UIPanel_t295209936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2542[40] = 
{
	UIPanel_t295209936_StaticFields::get_offset_of_list_22(),
	UIPanel_t295209936::get_offset_of_onGeometryUpdated_23(),
	UIPanel_t295209936::get_offset_of_showInPanelTool_24(),
	UIPanel_t295209936::get_offset_of_generateNormals_25(),
	UIPanel_t295209936::get_offset_of_widgetsAreStatic_26(),
	UIPanel_t295209936::get_offset_of_cullWhileDragging_27(),
	UIPanel_t295209936::get_offset_of_alwaysOnScreen_28(),
	UIPanel_t295209936::get_offset_of_anchorOffset_29(),
	UIPanel_t295209936::get_offset_of_softBorderPadding_30(),
	UIPanel_t295209936::get_offset_of_renderQueue_31(),
	UIPanel_t295209936::get_offset_of_startingRenderQueue_32(),
	UIPanel_t295209936::get_offset_of_widgets_33(),
	UIPanel_t295209936::get_offset_of_drawCalls_34(),
	UIPanel_t295209936::get_offset_of_worldToLocal_35(),
	UIPanel_t295209936::get_offset_of_drawCallClipRange_36(),
	UIPanel_t295209936::get_offset_of_onClipMove_37(),
	UIPanel_t295209936::get_offset_of_mClipTexture_38(),
	UIPanel_t295209936::get_offset_of_mAlpha_39(),
	UIPanel_t295209936::get_offset_of_mClipping_40(),
	UIPanel_t295209936::get_offset_of_mClipRange_41(),
	UIPanel_t295209936::get_offset_of_mClipSoftness_42(),
	UIPanel_t295209936::get_offset_of_mDepth_43(),
	UIPanel_t295209936::get_offset_of_mSortingOrder_44(),
	UIPanel_t295209936::get_offset_of_mRebuild_45(),
	UIPanel_t295209936::get_offset_of_mResized_46(),
	UIPanel_t295209936::get_offset_of_mClipOffset_47(),
	UIPanel_t295209936::get_offset_of_mMatrixFrame_48(),
	UIPanel_t295209936::get_offset_of_mAlphaFrameID_49(),
	UIPanel_t295209936::get_offset_of_mLayer_50(),
	UIPanel_t295209936_StaticFields::get_offset_of_mTemp_51(),
	UIPanel_t295209936::get_offset_of_mMin_52(),
	UIPanel_t295209936::get_offset_of_mMax_53(),
	UIPanel_t295209936::get_offset_of_mHalfPixelOffset_54(),
	UIPanel_t295209936::get_offset_of_mSortWidgets_55(),
	UIPanel_t295209936::get_offset_of_mUpdateScroll_56(),
	UIPanel_t295209936::get_offset_of_mParentPanel_57(),
	UIPanel_t295209936_StaticFields::get_offset_of_mCorners_58(),
	UIPanel_t295209936_StaticFields::get_offset_of_mUpdateFrame_59(),
	UIPanel_t295209936::get_offset_of_mOnRender_60(),
	UIPanel_t295209936::get_offset_of_mForced_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (RenderQueue_t3352612156)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2543[4] = 
{
	RenderQueue_t3352612156::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (OnGeometryUpdated_t4285773675), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (OnClippingMoved_t1586118419), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (UIRoot_t2503447958), -1, sizeof(UIRoot_t2503447958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2546[11] = 
{
	UIRoot_t2503447958_StaticFields::get_offset_of_list_2(),
	UIRoot_t2503447958::get_offset_of_scalingStyle_3(),
	UIRoot_t2503447958::get_offset_of_manualWidth_4(),
	UIRoot_t2503447958::get_offset_of_manualHeight_5(),
	UIRoot_t2503447958::get_offset_of_minimumHeight_6(),
	UIRoot_t2503447958::get_offset_of_maximumHeight_7(),
	UIRoot_t2503447958::get_offset_of_fitWidth_8(),
	UIRoot_t2503447958::get_offset_of_fitHeight_9(),
	UIRoot_t2503447958::get_offset_of_adjustByDPI_10(),
	UIRoot_t2503447958::get_offset_of_shrinkPortraitUI_11(),
	UIRoot_t2503447958::get_offset_of_mTrans_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (Scaling_t174157806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2547[4] = 
{
	Scaling_t174157806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (Constraint_t2067133462)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2548[5] = 
{
	Constraint_t2067133462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (UISprite_t661437049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[5] = 
{
	UISprite_t661437049::get_offset_of_mAtlas_66(),
	UISprite_t661437049::get_offset_of_mSpriteName_67(),
	UISprite_t661437049::get_offset_of_mFillCenter_68(),
	UISprite_t661437049::get_offset_of_mSprite_69(),
	UISprite_t661437049::get_offset_of_mSpriteSet_70(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (UISpriteAnimation_t4279777547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[9] = 
{
	UISpriteAnimation_t4279777547::get_offset_of_mFPS_2(),
	UISpriteAnimation_t4279777547::get_offset_of_mPrefix_3(),
	UISpriteAnimation_t4279777547::get_offset_of_mLoop_4(),
	UISpriteAnimation_t4279777547::get_offset_of_mSnap_5(),
	UISpriteAnimation_t4279777547::get_offset_of_mSprite_6(),
	UISpriteAnimation_t4279777547::get_offset_of_mDelta_7(),
	UISpriteAnimation_t4279777547::get_offset_of_mIndex_8(),
	UISpriteAnimation_t4279777547::get_offset_of_mActive_9(),
	UISpriteAnimation_t4279777547::get_offset_of_mSpriteNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (UISpriteData_t3578345923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[13] = 
{
	UISpriteData_t3578345923::get_offset_of_name_0(),
	UISpriteData_t3578345923::get_offset_of_x_1(),
	UISpriteData_t3578345923::get_offset_of_y_2(),
	UISpriteData_t3578345923::get_offset_of_width_3(),
	UISpriteData_t3578345923::get_offset_of_height_4(),
	UISpriteData_t3578345923::get_offset_of_borderLeft_5(),
	UISpriteData_t3578345923::get_offset_of_borderRight_6(),
	UISpriteData_t3578345923::get_offset_of_borderTop_7(),
	UISpriteData_t3578345923::get_offset_of_borderBottom_8(),
	UISpriteData_t3578345923::get_offset_of_paddingLeft_9(),
	UISpriteData_t3578345923::get_offset_of_paddingRight_10(),
	UISpriteData_t3578345923::get_offset_of_paddingTop_11(),
	UISpriteData_t3578345923::get_offset_of_paddingBottom_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (UIStretch_t3439076817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[16] = 
{
	UIStretch_t3439076817::get_offset_of_uiCamera_2(),
	UIStretch_t3439076817::get_offset_of_container_3(),
	UIStretch_t3439076817::get_offset_of_style_4(),
	UIStretch_t3439076817::get_offset_of_runOnlyOnce_5(),
	UIStretch_t3439076817::get_offset_of_relativeSize_6(),
	UIStretch_t3439076817::get_offset_of_initialSize_7(),
	UIStretch_t3439076817::get_offset_of_borderPadding_8(),
	UIStretch_t3439076817::get_offset_of_widgetContainer_9(),
	UIStretch_t3439076817::get_offset_of_mTrans_10(),
	UIStretch_t3439076817::get_offset_of_mWidget_11(),
	UIStretch_t3439076817::get_offset_of_mSprite_12(),
	UIStretch_t3439076817::get_offset_of_mPanel_13(),
	UIStretch_t3439076817::get_offset_of_mRoot_14(),
	UIStretch_t3439076817::get_offset_of_mAnim_15(),
	UIStretch_t3439076817::get_offset_of_mRect_16(),
	UIStretch_t3439076817::get_offset_of_mStarted_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Style_t3950950355)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[8] = 
{
	Style_t3950950355::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (UITextList_t736798239), -1, sizeof(UITextList_t736798239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2554[11] = 
{
	UITextList_t736798239::get_offset_of_textLabel_2(),
	UITextList_t736798239::get_offset_of_scrollBar_3(),
	UITextList_t736798239::get_offset_of_style_4(),
	UITextList_t736798239::get_offset_of_paragraphHistory_5(),
	UITextList_t736798239::get_offset_of_mSeparator_6(),
	UITextList_t736798239::get_offset_of_mScroll_7(),
	UITextList_t736798239::get_offset_of_mTotalLines_8(),
	UITextList_t736798239::get_offset_of_mLastWidth_9(),
	UITextList_t736798239::get_offset_of_mLastHeight_10(),
	UITextList_t736798239::get_offset_of_mParagraphs_11(),
	UITextList_t736798239_StaticFields::get_offset_of_mHistory_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (Style_t464733601)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2555[3] = 
{
	Style_t464733601::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (Paragraph_t3894573406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[2] = 
{
	Paragraph_t3894573406::get_offset_of_text_0(),
	Paragraph_t3894573406::get_offset_of_lines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (UITexture_t3903132647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[7] = 
{
	UITexture_t3903132647::get_offset_of_mRect_66(),
	UITexture_t3903132647::get_offset_of_mTexture_67(),
	UITexture_t3903132647::get_offset_of_mMat_68(),
	UITexture_t3903132647::get_offset_of_mShader_69(),
	UITexture_t3903132647::get_offset_of_mBorder_70(),
	UITexture_t3903132647::get_offset_of_mFixedAspect_71(),
	UITexture_t3903132647::get_offset_of_mPMA_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (UITooltip_t4180872911), -1, sizeof(UITooltip_t4180872911_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[14] = 
{
	UITooltip_t4180872911_StaticFields::get_offset_of_mInstance_2(),
	UITooltip_t4180872911::get_offset_of_uiCamera_3(),
	UITooltip_t4180872911::get_offset_of_text_4(),
	UITooltip_t4180872911::get_offset_of_tooltipRoot_5(),
	UITooltip_t4180872911::get_offset_of_background_6(),
	UITooltip_t4180872911::get_offset_of_appearSpeed_7(),
	UITooltip_t4180872911::get_offset_of_scalingTransitions_8(),
	UITooltip_t4180872911::get_offset_of_mTooltip_9(),
	UITooltip_t4180872911::get_offset_of_mTrans_10(),
	UITooltip_t4180872911::get_offset_of_mTarget_11(),
	UITooltip_t4180872911::get_offset_of_mCurrent_12(),
	UITooltip_t4180872911::get_offset_of_mPos_13(),
	UITooltip_t4180872911::get_offset_of_mSize_14(),
	UITooltip_t4180872911::get_offset_of_mWidgets_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (UIViewport_t2937361242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[5] = 
{
	UIViewport_t2937361242::get_offset_of_sourceCamera_2(),
	UIViewport_t2937361242::get_offset_of_topLeft_3(),
	UIViewport_t2937361242::get_offset_of_bottomRight_4(),
	UIViewport_t2937361242::get_offset_of_fullSize_5(),
	UIViewport_t2937361242::get_offset_of_mCam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (GyroscopeControllerFullFreedom_t685599910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[17] = 
{
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_baseIdentity_2(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_referenceRotation_3(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_cameraBase_4(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_calibration_5(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_baseOrientation_6(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_m_gyroEnable_7(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_originalRotation_8(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_sensitivityX_9(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_sensitivityY_10(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_minimumX_11(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_maximumX_12(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_minimumY_13(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_maximumY_14(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_rotationY_15(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_rotationX_16(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_m_fovMin_17(),
	GyroscopeControllerFullFreedom_t685599910::get_offset_of_m_fovMax_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (AndroidOBBHandler_t3535974956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[6] = 
{
	AndroidOBBHandler_t3535974956::get_offset_of_OnError_3(),
	AndroidOBBHandler_t3535974956::get_offset_of_OnCompleted_4(),
	AndroidOBBHandler_t3535974956::get_offset_of_OnProgress_5(),
	AndroidOBBHandler_t3535974956::get_offset_of__expPath_6(),
	AndroidOBBHandler_t3535974956::get_offset_of__downloadStarted_7(),
	AndroidOBBHandler_t3535974956::get_offset_of_U3Cbase64AppKeyU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (EErrorCode_t3259205415)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[7] = 
{
	EErrorCode_t3259205415::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (U3CdownloadU3Ec__IteratorE_t3158326693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[2] = 
{
	U3CdownloadU3Ec__IteratorE_t3158326693::get_offset_of_U24PC_0(),
	U3CdownloadU3Ec__IteratorE_t3158326693::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (DataBuilder_t3810402835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[2] = 
{
	DataBuilder_t3810402835::get_offset_of__file_0(),
	DataBuilder_t3810402835::get_offset_of__writer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (FileDescription_t856434530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[3] = 
{
	FileDescription_t856434530::get_offset_of_U3CpathU3Ek__BackingField_0(),
	FileDescription_t856434530::get_offset_of_U3CfilenameU3Ek__BackingField_1(),
	FileDescription_t856434530::get_offset_of_U3CextensionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (FileHelper_t1028318256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (EFolder_t738843808)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[4] = 
{
	EFolder_t738843808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (U3CCopyToPersistentU3Ec__IteratorF_t3141760867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[10] = 
{
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_file_0(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U3C_pathU3E__0_1(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_overwrite_2(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U3C_sourceU3E__1_3(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_callback_4(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U24PC_5(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U24current_6(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U3CU24U3Efile_7(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U3CU24U3Eoverwrite_8(),
	U3CCopyToPersistentU3Ec__IteratorF_t3141760867::get_offset_of_U3CU24U3Ecallback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (DataSetHandler_t896450754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[4] = 
{
	DataSetHandler_t896450754::get_offset_of_mDataset_2(),
	DataSetHandler_t896450754::get_offset_of_tracker_3(),
	DataSetHandler_t896450754::get_offset_of__dataSet_4(),
	DataSetHandler_t896450754::get_offset_of__projectID_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (BackgroundPlaneBehaviour_t1927425041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (CloudRecoBehaviour_t3993405419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (CylinderTargetBehaviour_t1850077856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (DatabaseLoadBehaviour_t2989373670), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (DefaultInitializationErrorHandler_t3746290221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[3] = 
{
	0,
	DefaultInitializationErrorHandler_t3746290221::get_offset_of_mErrorText_3(),
	DefaultInitializationErrorHandler_t3746290221::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (DefaultSmartTerrainEventHandler_t543367463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[3] = 
{
	DefaultSmartTerrainEventHandler_t543367463::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t543367463::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t543367463::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (DefaultTrackableEventHandler_t4108278998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[1] = 
{
	DefaultTrackableEventHandler_t4108278998::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (GLErrorHandler_t4275497481), -1, sizeof(GLErrorHandler_t4275497481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2578[3] = 
{
	0,
	GLErrorHandler_t4275497481_StaticFields::get_offset_of_mErrorText_3(),
	GLErrorHandler_t4275497481_StaticFields::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (HideExcessAreaBehaviour_t439862723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (ImageTargetBehaviour_t1735871187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (AndroidUnityPlayer_t1535493961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t1535493961::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t1535493961::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t1535493961::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t1535493961::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (ComponentFactoryStarterBehaviour_t2489055709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (IOSUnityPlayer_t2749231339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	IOSUnityPlayer_t2749231339::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (VuforiaBehaviourComponentFactory_t900168586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (KeepAliveBehaviour_t1171410903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (MarkerBehaviour_t3170674893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (MaskOutBehaviour_t1204933533), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (MultiTargetBehaviour_t2222860085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (ObjectTargetBehaviour_t2508606743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (PropBehaviour_t3213561028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (ReconstructionBehaviour_t3695833539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (ReconstructionFromTargetBehaviour_t582925864), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (SmartTerrainTrackerBehaviour_t442856755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (SurfaceBehaviour_t3457144850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (TextRecoBehaviour_t892056475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (TurnOffBehaviour_t1278464685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (TurnOffWordBehaviour_t3289283011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (UserDefinedTargetBuildingBehaviour_t3016919996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (VRIntegrationHelper_t1694779206), -1, sizeof(VRIntegrationHelper_t1694779206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2599[12] = 
{
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t1694779206_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t1694779206::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t1694779206::get_offset_of_TrackableParent_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
