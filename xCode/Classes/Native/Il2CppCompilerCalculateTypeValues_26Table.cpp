﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3391125558.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer1585639557.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2253448098.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour1845338141.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour2450634316.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour433318935.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH3463640687.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour2034767101.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220377.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220410.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3988332413.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (VideoBackgroundBehaviour_t3391125558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (VideoTextureRenderer_t1585639557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (VirtualButtonBehaviour_t2253448098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (VuforiaBehaviour_t1845338141), -1, sizeof(VuforiaBehaviour_t1845338141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[1] = 
{
	VuforiaBehaviour_t1845338141_StaticFields::get_offset_of_mVuforiaBehaviour_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (WebCamBehaviour_t2450634316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (WireframeBehaviour_t433318935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[3] = 
{
	WireframeBehaviour_t433318935::get_offset_of_mLineMaterial_2(),
	WireframeBehaviour_t433318935::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t433318935::get_offset_of_LineColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (WireframeTrackableEventHandler_t3463640687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[1] = 
{
	WireframeTrackableEventHandler_t3463640687::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (WordBehaviour_t2034767101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238939), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2608[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (U24ArrayTypeU2420_t3379220379)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220379_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (U24ArrayTypeU2432_t3379220412)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3379220412_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (U24ArrayTypeU248_t3988332415)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3988332415_marshaled_pinvoke), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
