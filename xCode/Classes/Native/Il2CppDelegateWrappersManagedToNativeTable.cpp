﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t691583313 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t4166107989 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1428404869 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2583486074 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t651537830 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t1474775431 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t124146534 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t1873379884 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t3250749483 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3891738222 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1637408689 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t742231849 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1292950321 ();
extern "C" void DelegatePInvokeWrapper_Action_t3771233898 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t2984951347 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t83861602 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4247149838 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2578300556 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t594794173 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2749288659 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878 ();
extern "C" void DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594 ();
extern "C" void DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818 ();
extern "C" void DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t4120438118 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t3952708057 ();
extern "C" void DelegatePInvokeWrapper_ApplyTween_t882368618 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t1323017328 ();
extern "C" void DelegatePInvokeWrapper_VREventCallback_t2063132207 ();
extern "C" void DelegatePInvokeWrapper_OnGUICallback_t3287452568 ();
extern "C" void DelegatePInvokeWrapper_onVideoEndDel_t1248556915 ();
extern "C" void DelegatePInvokeWrapper_Callback_t1094463061 ();
extern "C" void DelegatePInvokeWrapper_LoadFunction_t2234103444 ();
extern "C" void DelegatePInvokeWrapper_OnLocalizeNotification_t2879153737 ();
extern "C" void DelegatePInvokeWrapper_VideoEnd_t3177907679 ();
extern "C" void DelegatePInvokeWrapper_VideoError_t247668236 ();
extern "C" void DelegatePInvokeWrapper_VideoFirstFrameReady_t2520860170 ();
extern "C" void DelegatePInvokeWrapper_VideoReady_t259270055 ();
extern "C" void DelegatePInvokeWrapper_OnFinished_t3316389065 ();
extern "C" void DelegatePInvokeWrapper_OnFinished_t2399837386 ();
extern "C" void DelegatePInvokeWrapper_GetAnyKeyFunc_t4194451799 ();
extern "C" void DelegatePInvokeWrapper_GetAxisFunc_t783615269 ();
extern "C" void DelegatePInvokeWrapper_GetKeyStateFunc_t2745734774 ();
extern "C" void DelegatePInvokeWrapper_GetTouchCountCallback_t3435560085 ();
extern "C" void DelegatePInvokeWrapper_MoveDelegate_t1906135052 ();
extern "C" void DelegatePInvokeWrapper_OnCustomInput_t736305828 ();
extern "C" void DelegatePInvokeWrapper_OnSchemeChange_t2731530058 ();
extern "C" void DelegatePInvokeWrapper_OnScreenResize_t3828578773 ();
extern "C" void DelegatePInvokeWrapper_OnReposition_t1310478256 ();
extern "C" void DelegatePInvokeWrapper_OnValidate_t2690340430 ();
extern "C" void DelegatePInvokeWrapper_OnGeometryUpdated_t4285773675 ();
extern "C" void DelegatePInvokeWrapper_LegacyEvent_t524649176 ();
extern "C" void DelegatePInvokeWrapper_OnDragFinished_t4207031714 ();
extern "C" void DelegatePInvokeWrapper_OnDragNotification_t2323474503 ();
extern "C" void DelegatePInvokeWrapper_OnReposition_t213079568 ();
extern "C" void DelegatePInvokeWrapper_Validate_t2927776861 ();
extern "C" void DelegatePInvokeWrapper_HitCheck_t3889696652 ();
extern "C" void DelegatePInvokeWrapper_OnDimensionsChanged_t3695058769 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[65] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t691583313,
	DelegatePInvokeWrapper_Swapper_t4166107989,
	DelegatePInvokeWrapper_ReadDelegate_t1428404869,
	DelegatePInvokeWrapper_WriteDelegate_t2583486074,
	DelegatePInvokeWrapper_CrossContextDelegate_t651537830,
	DelegatePInvokeWrapper_CallbackHandler_t1474775431,
	DelegatePInvokeWrapper_ThreadStart_t124146534,
	DelegatePInvokeWrapper_ReadMethod_t1873379884,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333,
	DelegatePInvokeWrapper_WriteMethod_t3250749483,
	DelegatePInvokeWrapper_ReadDelegate_t3891738222,
	DelegatePInvokeWrapper_WriteDelegate_t1637408689,
	DelegatePInvokeWrapper_SocketAsyncCall_t742231849,
	DelegatePInvokeWrapper_CostDelegate_t1292950321,
	DelegatePInvokeWrapper_Action_t3771233898,
	DelegatePInvokeWrapper_LogCallback_t2984951347,
	DelegatePInvokeWrapper_PCMReaderCallback_t83861602,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005,
	DelegatePInvokeWrapper_WillRenderCanvases_t4247149838,
	DelegatePInvokeWrapper_StateChanged_t2578300556,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515,
	DelegatePInvokeWrapper_UnityAction_t594794173,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797,
	DelegatePInvokeWrapper_WindowFunction_t2749288659,
	DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878,
	DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594,
	DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818,
	DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244,
	DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210,
	DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425,
	DelegatePInvokeWrapper_CharGetter_t4120438118,
	DelegatePInvokeWrapper_OnValidateInput_t3952708057,
	DelegatePInvokeWrapper_ApplyTween_t882368618,
	DelegatePInvokeWrapper_EasingFunction_t1323017328,
	DelegatePInvokeWrapper_VREventCallback_t2063132207,
	DelegatePInvokeWrapper_OnGUICallback_t3287452568,
	DelegatePInvokeWrapper_onVideoEndDel_t1248556915,
	DelegatePInvokeWrapper_Callback_t1094463061,
	DelegatePInvokeWrapper_LoadFunction_t2234103444,
	DelegatePInvokeWrapper_OnLocalizeNotification_t2879153737,
	DelegatePInvokeWrapper_VideoEnd_t3177907679,
	DelegatePInvokeWrapper_VideoError_t247668236,
	DelegatePInvokeWrapper_VideoFirstFrameReady_t2520860170,
	DelegatePInvokeWrapper_VideoReady_t259270055,
	DelegatePInvokeWrapper_OnFinished_t3316389065,
	DelegatePInvokeWrapper_OnFinished_t2399837386,
	DelegatePInvokeWrapper_GetAnyKeyFunc_t4194451799,
	DelegatePInvokeWrapper_GetAxisFunc_t783615269,
	DelegatePInvokeWrapper_GetKeyStateFunc_t2745734774,
	DelegatePInvokeWrapper_GetTouchCountCallback_t3435560085,
	DelegatePInvokeWrapper_MoveDelegate_t1906135052,
	DelegatePInvokeWrapper_OnCustomInput_t736305828,
	DelegatePInvokeWrapper_OnSchemeChange_t2731530058,
	DelegatePInvokeWrapper_OnScreenResize_t3828578773,
	DelegatePInvokeWrapper_OnReposition_t1310478256,
	DelegatePInvokeWrapper_OnValidate_t2690340430,
	DelegatePInvokeWrapper_OnGeometryUpdated_t4285773675,
	DelegatePInvokeWrapper_LegacyEvent_t524649176,
	DelegatePInvokeWrapper_OnDragFinished_t4207031714,
	DelegatePInvokeWrapper_OnDragNotification_t2323474503,
	DelegatePInvokeWrapper_OnReposition_t213079568,
	DelegatePInvokeWrapper_Validate_t2927776861,
	DelegatePInvokeWrapper_HitCheck_t3889696652,
	DelegatePInvokeWrapper_OnDimensionsChanged_t3695058769,
};
