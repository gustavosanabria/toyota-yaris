﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImpleme3342667558.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{3518E5AC-F4AC-49D1-B865-F4A6B711AD02}
struct  U3CPrivateImplementationDetailsU3EU7B3518E5ACU2DF4ACU2D49D1U2DB865U2DF4A6B711AD02U7D_t3208964498  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B3518E5ACU2DF4ACU2D49D1U2DB865U2DF4A6B711AD02U7D_t3208964498_StaticFields
{
public:
	// <PrivateImplementationDetails>{3518E5AC-F4AC-49D1-B865-F4A6B711AD02}/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>{3518E5AC-F4AC-49D1-B865-F4A6B711AD02}::$$method0x600091c-1
	__StaticArrayInitTypeSizeU3D24_t3342667558  ___U24U24method0x600091cU2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x600091cU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B3518E5ACU2DF4ACU2D49D1U2DB865U2DF4A6B711AD02U7D_t3208964498_StaticFields, ___U24U24method0x600091cU2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t3342667558  get_U24U24method0x600091cU2D1_0() const { return ___U24U24method0x600091cU2D1_0; }
	inline __StaticArrayInitTypeSizeU3D24_t3342667558 * get_address_of_U24U24method0x600091cU2D1_0() { return &___U24U24method0x600091cU2D1_0; }
	inline void set_U24U24method0x600091cU2D1_0(__StaticArrayInitTypeSizeU3D24_t3342667558  value)
	{
		___U24U24method0x600091cU2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
