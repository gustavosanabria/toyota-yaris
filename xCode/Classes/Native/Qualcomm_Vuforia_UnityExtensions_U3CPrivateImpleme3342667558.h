﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{3518E5AC-F4AC-49D1-B865-F4A6B711AD02}/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t3342667558 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3342667558__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>{3518E5AC-F4AC-49D1-B865-F4A6B711AD02}/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t3342667558_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3342667558__padding[24];
	};
};
// Native definition for marshalling of: <PrivateImplementationDetails>{3518E5AC-F4AC-49D1-B865-F4A6B711AD02}/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t3342667558_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3342667558__padding[24];
	};
};
