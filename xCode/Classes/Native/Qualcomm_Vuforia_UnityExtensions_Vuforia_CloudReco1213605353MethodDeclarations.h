﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t1213605353;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t796991165;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
extern "C"  bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m4285334914 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
extern "C"  void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m3218173377 (CloudRecoAbstractBehaviour_t1213605353 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
extern "C"  bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m458606677 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
extern "C"  void CloudRecoAbstractBehaviour_Initialize_m793373040 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
extern "C"  void CloudRecoAbstractBehaviour_Deinitialize_m1172471985 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
extern "C"  void CloudRecoAbstractBehaviour_CheckInitialization_m2733980874 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
extern "C"  void CloudRecoAbstractBehaviour_StartCloudReco_m2777012562 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
extern "C"  void CloudRecoAbstractBehaviour_StopCloudReco_m2820144916 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C"  void CloudRecoAbstractBehaviour_RegisterEventHandler_m2426205612 (CloudRecoAbstractBehaviour_t1213605353 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C"  bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2460258609 (CloudRecoAbstractBehaviour_t1213605353 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
extern "C"  void CloudRecoAbstractBehaviour_OnEnable_m3944112482 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
extern "C"  void CloudRecoAbstractBehaviour_OnDisable_m2449339915 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
extern "C"  void CloudRecoAbstractBehaviour_Start_m1894755492 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
extern "C"  void CloudRecoAbstractBehaviour_Update_m2908697577 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
extern "C"  void CloudRecoAbstractBehaviour_OnDestroy_m331904669 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnVuforiaStarted()
extern "C"  void CloudRecoAbstractBehaviour_OnVuforiaStarted_m984884414 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern "C"  void CloudRecoAbstractBehaviour__ctor_m2947617700 (CloudRecoAbstractBehaviour_t1213605353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
