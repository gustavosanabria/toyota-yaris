﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t2095838082;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn2677974245.h"

// System.Boolean Vuforia.DataSet::Exists(System.String)
extern "C"  bool DataSet_Exists_m2370804369 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSet::Exists(System.String,Vuforia.VuforiaUnity/StorageType)
extern "C"  bool DataSet_Exists_m1595867088 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSet::.ctor()
extern "C"  void DataSet__ctor_m471356803 (DataSet_t2095838082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
