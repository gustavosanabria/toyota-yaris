﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Eyewear
struct Eyewear_t3657261146;
// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t846303360;
// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t945107814;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Eyewear
struct  Eyewear_t3657261146  : public Il2CppObject
{
public:
	// Vuforia.EyewearCalibrationProfileManager Vuforia.Eyewear::mProfileManager
	EyewearCalibrationProfileManager_t846303360 * ___mProfileManager_1;
	// Vuforia.EyewearUserCalibrator Vuforia.Eyewear::mCalibrator
	EyewearUserCalibrator_t945107814 * ___mCalibrator_2;

public:
	inline static int32_t get_offset_of_mProfileManager_1() { return static_cast<int32_t>(offsetof(Eyewear_t3657261146, ___mProfileManager_1)); }
	inline EyewearCalibrationProfileManager_t846303360 * get_mProfileManager_1() const { return ___mProfileManager_1; }
	inline EyewearCalibrationProfileManager_t846303360 ** get_address_of_mProfileManager_1() { return &___mProfileManager_1; }
	inline void set_mProfileManager_1(EyewearCalibrationProfileManager_t846303360 * value)
	{
		___mProfileManager_1 = value;
		Il2CppCodeGenWriteBarrier(&___mProfileManager_1, value);
	}

	inline static int32_t get_offset_of_mCalibrator_2() { return static_cast<int32_t>(offsetof(Eyewear_t3657261146, ___mCalibrator_2)); }
	inline EyewearUserCalibrator_t945107814 * get_mCalibrator_2() const { return ___mCalibrator_2; }
	inline EyewearUserCalibrator_t945107814 ** get_address_of_mCalibrator_2() { return &___mCalibrator_2; }
	inline void set_mCalibrator_2(EyewearUserCalibrator_t945107814 * value)
	{
		___mCalibrator_2 = value;
		Il2CppCodeGenWriteBarrier(&___mCalibrator_2, value);
	}
};

struct Eyewear_t3657261146_StaticFields
{
public:
	// Vuforia.Eyewear Vuforia.Eyewear::mInstance
	Eyewear_t3657261146 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(Eyewear_t3657261146_StaticFields, ___mInstance_0)); }
	inline Eyewear_t3657261146 * get_mInstance_0() const { return ___mInstance_0; }
	inline Eyewear_t3657261146 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(Eyewear_t3657261146 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___mInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
