﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.Eyewear
struct Eyewear_t3657261146;
// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t846303360;
// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t945107814;

#include "codegen/il2cpp-codegen.h"

// Vuforia.Eyewear Vuforia.Eyewear::get_Instance()
extern "C"  Eyewear_t3657261146 * Eyewear_get_Instance_m1517927656 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.EyewearCalibrationProfileManager Vuforia.Eyewear::getProfileManager()
extern "C"  EyewearCalibrationProfileManager_t846303360 * Eyewear_getProfileManager_m997292718 (Eyewear_t3657261146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.EyewearUserCalibrator Vuforia.Eyewear::getCalibrator()
extern "C"  EyewearUserCalibrator_t945107814 * Eyewear_getCalibrator_m867489891 (Eyewear_t3657261146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Eyewear::.ctor()
extern "C"  void Eyewear__ctor_m4225227691 (Eyewear_t3657261146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Eyewear::.cctor()
extern "C"  void Eyewear__cctor_m1650943330 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
