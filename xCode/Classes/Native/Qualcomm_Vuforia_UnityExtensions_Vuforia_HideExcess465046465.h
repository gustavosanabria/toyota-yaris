﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t3191267369;
// Vuforia.IExcessAreaClipping
struct IExcessAreaClipping_t250426359;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t1091759131;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExces3226096284.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour
struct  HideExcessAreaAbstractBehaviour_t465046465  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Shader Vuforia.HideExcessAreaAbstractBehaviour::matteShader
	Shader_t3191267369 * ___matteShader_2;
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.HideExcessAreaAbstractBehaviour::clippingMode
	int32_t ___clippingMode_3;
	// Vuforia.IExcessAreaClipping Vuforia.HideExcessAreaAbstractBehaviour::mClippingImpl
	Il2CppObject * ___mClippingImpl_4;
	// Vuforia.VuforiaAbstractBehaviour Vuforia.HideExcessAreaAbstractBehaviour::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t1091759131 * ___mVuforiaBehaviour_5;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mPlaneOffset
	Vector3_t4282066566  ___mPlaneOffset_6;

public:
	inline static int32_t get_offset_of_matteShader_2() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___matteShader_2)); }
	inline Shader_t3191267369 * get_matteShader_2() const { return ___matteShader_2; }
	inline Shader_t3191267369 ** get_address_of_matteShader_2() { return &___matteShader_2; }
	inline void set_matteShader_2(Shader_t3191267369 * value)
	{
		___matteShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___matteShader_2, value);
	}

	inline static int32_t get_offset_of_clippingMode_3() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___clippingMode_3)); }
	inline int32_t get_clippingMode_3() const { return ___clippingMode_3; }
	inline int32_t* get_address_of_clippingMode_3() { return &___clippingMode_3; }
	inline void set_clippingMode_3(int32_t value)
	{
		___clippingMode_3 = value;
	}

	inline static int32_t get_offset_of_mClippingImpl_4() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mClippingImpl_4)); }
	inline Il2CppObject * get_mClippingImpl_4() const { return ___mClippingImpl_4; }
	inline Il2CppObject ** get_address_of_mClippingImpl_4() { return &___mClippingImpl_4; }
	inline void set_mClippingImpl_4(Il2CppObject * value)
	{
		___mClippingImpl_4 = value;
		Il2CppCodeGenWriteBarrier(&___mClippingImpl_4, value);
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_5() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mVuforiaBehaviour_5)); }
	inline VuforiaAbstractBehaviour_t1091759131 * get_mVuforiaBehaviour_5() const { return ___mVuforiaBehaviour_5; }
	inline VuforiaAbstractBehaviour_t1091759131 ** get_address_of_mVuforiaBehaviour_5() { return &___mVuforiaBehaviour_5; }
	inline void set_mVuforiaBehaviour_5(VuforiaAbstractBehaviour_t1091759131 * value)
	{
		___mVuforiaBehaviour_5 = value;
		Il2CppCodeGenWriteBarrier(&___mVuforiaBehaviour_5, value);
	}

	inline static int32_t get_offset_of_mPlaneOffset_6() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mPlaneOffset_6)); }
	inline Vector3_t4282066566  get_mPlaneOffset_6() const { return ___mPlaneOffset_6; }
	inline Vector3_t4282066566 * get_address_of_mPlaneOffset_6() { return &___mPlaneOffset_6; }
	inline void set_mPlaneOffset_6(Vector3_t4282066566  value)
	{
		___mPlaneOffset_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
