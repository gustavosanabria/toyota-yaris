﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t465046465;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnTrackablesUpdated()
extern "C"  void HideExcessAreaAbstractBehaviour_OnTrackablesUpdated_m53597392 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::get_PlaneOffset()
extern "C"  Vector3_t4282066566  HideExcessAreaAbstractBehaviour_get_PlaneOffset_m4229123618 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::set_PlaneOffset(UnityEngine.Vector3)
extern "C"  void HideExcessAreaAbstractBehaviour_set_PlaneOffset_m3511314981 (HideExcessAreaAbstractBehaviour_t465046465 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesRenderingActive(System.Boolean)
extern "C"  void HideExcessAreaAbstractBehaviour_SetPlanesRenderingActive_m2412753264 (HideExcessAreaAbstractBehaviour_t465046465 * __this, bool ___activeflag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::IsPlanesRenderingActive()
extern "C"  bool HideExcessAreaAbstractBehaviour_IsPlanesRenderingActive_m1472281855 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPreCull()
extern "C"  void HideExcessAreaAbstractBehaviour_OnPreCull_m3379802904 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPostRender()
extern "C"  void HideExcessAreaAbstractBehaviour_OnPostRender_m2386892277 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern "C"  void HideExcessAreaAbstractBehaviour_Start_m3503597860 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDisable()
extern "C"  void HideExcessAreaAbstractBehaviour_OnDisable_m1175499403 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnEnable()
extern "C"  void HideExcessAreaAbstractBehaviour_OnEnable_m1132074210 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern "C"  void HideExcessAreaAbstractBehaviour_OnDestroy_m3353031453 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C"  void HideExcessAreaAbstractBehaviour__ctor_m261492772 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
