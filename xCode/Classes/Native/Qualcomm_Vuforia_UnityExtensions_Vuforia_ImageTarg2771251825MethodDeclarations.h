﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ImageTargetBuilderImpl
struct ImageTargetBuilderImpl_t2771251825;
// System.String
struct String_t;
// Vuforia.TrackableSource
struct TrackableSource_t179597514;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarge614449126.h"

// System.Boolean Vuforia.ImageTargetBuilderImpl::Build(System.String,System.Single)
extern "C"  bool ImageTargetBuilderImpl_Build_m3481619051 (ImageTargetBuilderImpl_t2771251825 * __this, String_t* ___targetName0, float ___sceenSizeWidth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::StartScan()
extern "C"  void ImageTargetBuilderImpl_StartScan_m1461478649 (ImageTargetBuilderImpl_t2771251825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::StopScan()
extern "C"  void ImageTargetBuilderImpl_StopScan_m1097894407 (ImageTargetBuilderImpl_t2771251825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilderImpl::GetFrameQuality()
extern "C"  int32_t ImageTargetBuilderImpl_GetFrameQuality_m177712885 (ImageTargetBuilderImpl_t2771251825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::GetTrackableSource()
extern "C"  TrackableSource_t179597514 * ImageTargetBuilderImpl_GetTrackableSource_m1727402673 (ImageTargetBuilderImpl_t2771251825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::.ctor()
extern "C"  void ImageTargetBuilderImpl__ctor_m4265429532 (ImageTargetBuilderImpl_t2771251825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
