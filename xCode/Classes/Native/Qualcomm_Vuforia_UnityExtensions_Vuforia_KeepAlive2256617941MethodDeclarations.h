﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t2256617941;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t678501447;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m1587546131 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m3241339650 (KeepAliveAbstractBehaviour_t2256617941 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m1519567832 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m1674916247 (KeepAliveAbstractBehaviour_t2256617941 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m1091656002 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m356841345 (KeepAliveAbstractBehaviour_t2256617941 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m1303782213 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m3211145908 (KeepAliveAbstractBehaviour_t2256617941 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m3301087037 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m4271541420 (KeepAliveAbstractBehaviour_t2256617941 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m3526907936 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m1257216591 (KeepAliveAbstractBehaviour_t2256617941 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
extern "C"  KeepAliveAbstractBehaviour_t2256617941 * KeepAliveAbstractBehaviour_get_Instance_m3733725568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C"  void KeepAliveAbstractBehaviour_RegisterEventHandler_m3293846606 (KeepAliveAbstractBehaviour_t2256617941 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C"  bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m3125456083 (KeepAliveAbstractBehaviour_t2256617941 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
extern "C"  void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m1097788213 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
extern "C"  void KeepAliveAbstractBehaviour__ctor_m612933944 (KeepAliveAbstractBehaviour_t2256617941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
