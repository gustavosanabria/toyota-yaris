﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Marker
struct Marker_t616694972;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MarkerAbstractBehaviour
struct  MarkerAbstractBehaviour_t865486027  : public TrackableBehaviour_t4179556250
{
public:
	// System.Int32 Vuforia.MarkerAbstractBehaviour::mMarkerID
	int32_t ___mMarkerID_9;
	// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::mMarker
	Il2CppObject * ___mMarker_10;

public:
	inline static int32_t get_offset_of_mMarkerID_9() { return static_cast<int32_t>(offsetof(MarkerAbstractBehaviour_t865486027, ___mMarkerID_9)); }
	inline int32_t get_mMarkerID_9() const { return ___mMarkerID_9; }
	inline int32_t* get_address_of_mMarkerID_9() { return &___mMarkerID_9; }
	inline void set_mMarkerID_9(int32_t value)
	{
		___mMarkerID_9 = value;
	}

	inline static int32_t get_offset_of_mMarker_10() { return static_cast<int32_t>(offsetof(MarkerAbstractBehaviour_t865486027, ___mMarker_10)); }
	inline Il2CppObject * get_mMarker_10() const { return ___mMarker_10; }
	inline Il2CppObject ** get_address_of_mMarker_10() { return &___mMarker_10; }
	inline void set_mMarker_10(Il2CppObject * value)
	{
		___mMarker_10 = value;
		Il2CppCodeGenWriteBarrier(&___mMarker_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
