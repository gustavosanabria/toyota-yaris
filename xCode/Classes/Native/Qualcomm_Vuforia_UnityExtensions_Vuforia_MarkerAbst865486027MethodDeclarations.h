﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t865486027;
// Vuforia.Marker
struct Marker_t616694972;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
extern "C"  Il2CppObject * MarkerAbstractBehaviour_get_Marker_m3363422270 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
extern "C"  void MarkerAbstractBehaviour__ctor_m856755802 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void MarkerAbstractBehaviour_InternalUnregisterTrackable_m1185139588 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
extern "C"  bool MarkerAbstractBehaviour_CorrectScaleImpl_m1604695486 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.get_MarkerID()
extern "C"  int32_t MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m745928241 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.SetMarkerID(System.Int32)
extern "C"  bool MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m2311746737 (MarkerAbstractBehaviour_t865486027 * __this, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.InitializeMarker(Vuforia.Marker)
extern "C"  void MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m1040674321 (MarkerAbstractBehaviour_t865486027 * __this, Il2CppObject * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m955732784 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2340651143 (MarkerAbstractBehaviour_t865486027 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t1659122786 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m3483097549 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t3674682005 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m580934403 (MarkerAbstractBehaviour_t865486027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
