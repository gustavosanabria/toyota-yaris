﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct Dictionary_2_t1958451778;

#include "mscorlib_System_Object4170816371.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2868837278.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MonoCameraConfiguration
struct  MonoCameraConfiguration_t3733412120  : public Il2CppObject
{
public:
	// UnityEngine.Camera Vuforia.MonoCameraConfiguration::mPrimaryCamera
	Camera_t2727095145 * ___mPrimaryCamera_0;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.MonoCameraConfiguration::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_1;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.MonoCameraConfiguration::mLastVideoBackGroundMirroredFromSDK
	int32_t ___mLastVideoBackGroundMirroredFromSDK_2;
	// System.Action Vuforia.MonoCameraConfiguration::mOnVideoBackgroundConfigChanged
	Action_t3771233898 * ___mOnVideoBackgroundConfigChanged_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour> Vuforia.MonoCameraConfiguration::mVideoBackgroundBehaviours
	Dictionary_2_t1958451778 * ___mVideoBackgroundBehaviours_4;
	// UnityEngine.Rect Vuforia.MonoCameraConfiguration::mViewportRect
	Rect_t4241904616  ___mViewportRect_5;
	// System.Boolean Vuforia.MonoCameraConfiguration::mRenderVideoBackground
	bool ___mRenderVideoBackground_6;
	// System.Int32 Vuforia.MonoCameraConfiguration::mCameraWidthFactor
	int32_t ___mCameraWidthFactor_7;
	// System.Int32 Vuforia.MonoCameraConfiguration::mViewPortWidth
	int32_t ___mViewPortWidth_8;
	// System.Int32 Vuforia.MonoCameraConfiguration::mViewPortHeight
	int32_t ___mViewPortHeight_9;
	// UnityEngine.ScreenOrientation Vuforia.MonoCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_10;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.MonoCameraConfiguration::mInitialReflection
	int32_t ___mInitialReflection_11;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_0() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mPrimaryCamera_0)); }
	inline Camera_t2727095145 * get_mPrimaryCamera_0() const { return ___mPrimaryCamera_0; }
	inline Camera_t2727095145 ** get_address_of_mPrimaryCamera_0() { return &___mPrimaryCamera_0; }
	inline void set_mPrimaryCamera_0(Camera_t2727095145 * value)
	{
		___mPrimaryCamera_0 = value;
		Il2CppCodeGenWriteBarrier(&___mPrimaryCamera_0, value);
	}

	inline static int32_t get_offset_of_mCameraDeviceMode_1() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mCameraDeviceMode_1)); }
	inline int32_t get_mCameraDeviceMode_1() const { return ___mCameraDeviceMode_1; }
	inline int32_t* get_address_of_mCameraDeviceMode_1() { return &___mCameraDeviceMode_1; }
	inline void set_mCameraDeviceMode_1(int32_t value)
	{
		___mCameraDeviceMode_1 = value;
	}

	inline static int32_t get_offset_of_mLastVideoBackGroundMirroredFromSDK_2() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mLastVideoBackGroundMirroredFromSDK_2)); }
	inline int32_t get_mLastVideoBackGroundMirroredFromSDK_2() const { return ___mLastVideoBackGroundMirroredFromSDK_2; }
	inline int32_t* get_address_of_mLastVideoBackGroundMirroredFromSDK_2() { return &___mLastVideoBackGroundMirroredFromSDK_2; }
	inline void set_mLastVideoBackGroundMirroredFromSDK_2(int32_t value)
	{
		___mLastVideoBackGroundMirroredFromSDK_2 = value;
	}

	inline static int32_t get_offset_of_mOnVideoBackgroundConfigChanged_3() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mOnVideoBackgroundConfigChanged_3)); }
	inline Action_t3771233898 * get_mOnVideoBackgroundConfigChanged_3() const { return ___mOnVideoBackgroundConfigChanged_3; }
	inline Action_t3771233898 ** get_address_of_mOnVideoBackgroundConfigChanged_3() { return &___mOnVideoBackgroundConfigChanged_3; }
	inline void set_mOnVideoBackgroundConfigChanged_3(Action_t3771233898 * value)
	{
		___mOnVideoBackgroundConfigChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___mOnVideoBackgroundConfigChanged_3, value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundBehaviours_4() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mVideoBackgroundBehaviours_4)); }
	inline Dictionary_2_t1958451778 * get_mVideoBackgroundBehaviours_4() const { return ___mVideoBackgroundBehaviours_4; }
	inline Dictionary_2_t1958451778 ** get_address_of_mVideoBackgroundBehaviours_4() { return &___mVideoBackgroundBehaviours_4; }
	inline void set_mVideoBackgroundBehaviours_4(Dictionary_2_t1958451778 * value)
	{
		___mVideoBackgroundBehaviours_4 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoBackgroundBehaviours_4, value);
	}

	inline static int32_t get_offset_of_mViewportRect_5() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mViewportRect_5)); }
	inline Rect_t4241904616  get_mViewportRect_5() const { return ___mViewportRect_5; }
	inline Rect_t4241904616 * get_address_of_mViewportRect_5() { return &___mViewportRect_5; }
	inline void set_mViewportRect_5(Rect_t4241904616  value)
	{
		___mViewportRect_5 = value;
	}

	inline static int32_t get_offset_of_mRenderVideoBackground_6() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mRenderVideoBackground_6)); }
	inline bool get_mRenderVideoBackground_6() const { return ___mRenderVideoBackground_6; }
	inline bool* get_address_of_mRenderVideoBackground_6() { return &___mRenderVideoBackground_6; }
	inline void set_mRenderVideoBackground_6(bool value)
	{
		___mRenderVideoBackground_6 = value;
	}

	inline static int32_t get_offset_of_mCameraWidthFactor_7() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mCameraWidthFactor_7)); }
	inline int32_t get_mCameraWidthFactor_7() const { return ___mCameraWidthFactor_7; }
	inline int32_t* get_address_of_mCameraWidthFactor_7() { return &___mCameraWidthFactor_7; }
	inline void set_mCameraWidthFactor_7(int32_t value)
	{
		___mCameraWidthFactor_7 = value;
	}

	inline static int32_t get_offset_of_mViewPortWidth_8() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mViewPortWidth_8)); }
	inline int32_t get_mViewPortWidth_8() const { return ___mViewPortWidth_8; }
	inline int32_t* get_address_of_mViewPortWidth_8() { return &___mViewPortWidth_8; }
	inline void set_mViewPortWidth_8(int32_t value)
	{
		___mViewPortWidth_8 = value;
	}

	inline static int32_t get_offset_of_mViewPortHeight_9() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mViewPortHeight_9)); }
	inline int32_t get_mViewPortHeight_9() const { return ___mViewPortHeight_9; }
	inline int32_t* get_address_of_mViewPortHeight_9() { return &___mViewPortHeight_9; }
	inline void set_mViewPortHeight_9(int32_t value)
	{
		___mViewPortHeight_9 = value;
	}

	inline static int32_t get_offset_of_mProjectionOrientation_10() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mProjectionOrientation_10)); }
	inline int32_t get_mProjectionOrientation_10() const { return ___mProjectionOrientation_10; }
	inline int32_t* get_address_of_mProjectionOrientation_10() { return &___mProjectionOrientation_10; }
	inline void set_mProjectionOrientation_10(int32_t value)
	{
		___mProjectionOrientation_10 = value;
	}

	inline static int32_t get_offset_of_mInitialReflection_11() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t3733412120, ___mInitialReflection_11)); }
	inline int32_t get_mInitialReflection_11() const { return ___mInitialReflection_11; }
	inline int32_t* get_address_of_mInitialReflection_11() { return &___mInitialReflection_11; }
	inline void set_mInitialReflection_11(int32_t value)
	{
		___mInitialReflection_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
