﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.MonoCameraConfiguration
struct MonoCameraConfiguration_t3733412120;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Action
struct Action_t3771233898;
// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t725705546;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2868837278.h"
#include "System_Core_System_Action3771233898.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbst725705546.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void Vuforia.MonoCameraConfiguration::.ctor(UnityEngine.Camera,Vuforia.CameraDevice/CameraDeviceMode,Vuforia.VuforiaRenderer/VideoBackgroundReflection,System.Action)
extern "C"  void MonoCameraConfiguration__ctor_m1342741180 (MonoCameraConfiguration_t3733412120 * __this, Camera_t2727095145 * ___leftCamera0, int32_t ___cameraDeviceMode1, int32_t ___mirrorVideoBackground2, Action_t3771233898 * ___onVideoBackgroundConfigChanged3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::Init()
extern "C"  void MonoCameraConfiguration_Init_m1682392071 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::ConfigureVideoBackground()
extern "C"  void MonoCameraConfiguration_ConfigureVideoBackground_m881318778 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::UpdatePlayModeParameters(Vuforia.WebCamAbstractBehaviour,System.Single)
extern "C"  void MonoCameraConfiguration_UpdatePlayModeParameters_m3761181538 (MonoCameraConfiguration_t3733412120 * __this, WebCamAbstractBehaviour_t725705546 * ___webCamBehaviour0, float ___cameraOffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::UpdateProjection(UnityEngine.ScreenOrientation)
extern "C"  void MonoCameraConfiguration_UpdateProjection_m2029470294 (MonoCameraConfiguration_t3733412120 * __this, int32_t ___orientation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.MonoCameraConfiguration::CheckForSurfaceChanges()
extern "C"  int32_t MonoCameraConfiguration_CheckForSurfaceChanges_m534606536 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::UpdateStereoDepth(UnityEngine.Transform)
extern "C"  void MonoCameraConfiguration_UpdateStereoDepth_m728144368 (MonoCameraConfiguration_t3733412120 * __this, Transform_t1659122786 * ___trackingReference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MonoCameraConfiguration::IsStereo()
extern "C"  bool MonoCameraConfiguration_IsStereo_m112231309 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::ResetBackgroundPlane(System.Boolean)
extern "C"  void MonoCameraConfiguration_ResetBackgroundPlane_m2144151213 (MonoCameraConfiguration_t3733412120 * __this, bool ___disable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.MonoCameraConfiguration::get_VideoBackgroundMirrored()
extern "C"  int32_t MonoCameraConfiguration_get_VideoBackgroundMirrored_m3877179998 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::ApplyCorrectedProjectionMatrix(UnityEngine.Matrix4x4,System.Boolean)
extern "C"  void MonoCameraConfiguration_ApplyCorrectedProjectionMatrix_m2916369221 (MonoCameraConfiguration_t3733412120 * __this, Matrix4x4_t1651859333  ___projectionMatrix0, bool ___primaryCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::SetSkewFrustum(System.Boolean)
extern "C"  void MonoCameraConfiguration_SetSkewFrustum_m1538489316 (MonoCameraConfiguration_t3733412120 * __this, bool ___setSkewing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.MonoCameraConfiguration::get_ViewportRect()
extern "C"  Rect_t4241904616  MonoCameraConfiguration_get_ViewportRect_m3482070256 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::EnableObjectRenderer(UnityEngine.GameObject,System.Boolean)
extern "C"  void MonoCameraConfiguration_EnableObjectRenderer_m2134083401 (MonoCameraConfiguration_t3733412120 * __this, GameObject_t3674682005 * ___go0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::ApplyMatrix(UnityEngine.Camera,UnityEngine.Matrix4x4)
extern "C"  void MonoCameraConfiguration_ApplyMatrix_m1775297202 (MonoCameraConfiguration_t3733412120 * __this, Camera_t2727095145 * ___cam0, Matrix4x4_t1651859333  ___inputMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::ResolveVideoBackgroundBehaviours()
extern "C"  void MonoCameraConfiguration_ResolveVideoBackgroundBehaviours_m854147306 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MonoCameraConfiguration::CameraParameterChanged()
extern "C"  bool MonoCameraConfiguration_CameraParameterChanged_m2844670171 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MonoCameraConfiguration::get_EyewearUserCalibrationProfileId()
extern "C"  int32_t MonoCameraConfiguration_get_EyewearUserCalibrationProfileId_m990024701 (MonoCameraConfiguration_t3733412120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MonoCameraConfiguration::set_EyewearUserCalibrationProfileId(System.Int32)
extern "C"  void MonoCameraConfiguration_set_EyewearUserCalibrationProfileId_m871456468 (MonoCameraConfiguration_t3733412120 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
