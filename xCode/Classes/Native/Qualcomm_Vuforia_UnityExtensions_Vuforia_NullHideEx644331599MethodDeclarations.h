﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.NullHideExcessAreaClipping
struct NullHideExcessAreaClipping_t644331599;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Vuforia.NullHideExcessAreaClipping::SetPlanesRenderingActive(System.Boolean)
extern "C"  void NullHideExcessAreaClipping_SetPlanesRenderingActive_m4031795414 (NullHideExcessAreaClipping_t644331599 * __this, bool ___activeflag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.NullHideExcessAreaClipping::IsPlanesRenderingActive()
extern "C"  bool NullHideExcessAreaClipping_IsPlanesRenderingActive_m1828013185 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::OnPreCull()
extern "C"  void NullHideExcessAreaClipping_OnPreCull_m3595237874 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::OnPostRender()
extern "C"  void NullHideExcessAreaClipping_OnPostRender_m3728943323 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::Start()
extern "C"  void NullHideExcessAreaClipping_Start_m231042302 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::OnDisable()
extern "C"  void NullHideExcessAreaClipping_OnDisable_m1390934373 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::OnEnable()
extern "C"  void NullHideExcessAreaClipping_OnEnable_m4187065032 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::OnDestroy()
extern "C"  void NullHideExcessAreaClipping_OnDestroy_m3568466423 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::Update(UnityEngine.Vector3)
extern "C"  void NullHideExcessAreaClipping_Update_m2815772842 (NullHideExcessAreaClipping_t644331599 * __this, Vector3_t4282066566  ___planeOffset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullHideExcessAreaClipping::.ctor()
extern "C"  void NullHideExcessAreaClipping__ctor_m1283904510 (NullHideExcessAreaClipping_t644331599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
