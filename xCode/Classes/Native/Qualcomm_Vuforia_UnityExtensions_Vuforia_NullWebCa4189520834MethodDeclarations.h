﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.NullWebCamTexAdaptor
struct NullWebCamTexAdaptor_t4189520834;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRen172958837.h"

// System.Boolean Vuforia.NullWebCamTexAdaptor::get_DidUpdateThisFrame()
extern "C"  bool NullWebCamTexAdaptor_get_DidUpdateThisFrame_m4257731029 (NullWebCamTexAdaptor_t4189520834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.NullWebCamTexAdaptor::get_IsPlaying()
extern "C"  bool NullWebCamTexAdaptor_get_IsPlaying_m1919518360 (NullWebCamTexAdaptor_t4189520834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.NullWebCamTexAdaptor::get_Texture()
extern "C"  Texture_t2526458961 * NullWebCamTexAdaptor_get_Texture_m1834788940 (NullWebCamTexAdaptor_t4189520834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::.ctor(System.Int32,Vuforia.VuforiaRenderer/Vec2I)
extern "C"  void NullWebCamTexAdaptor__ctor_m3693606867 (NullWebCamTexAdaptor_t4189520834 * __this, int32_t ___requestedFPS0, Vec2I_t172958837  ___requestedTextureSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Play()
extern "C"  void NullWebCamTexAdaptor_Play_m258783533 (NullWebCamTexAdaptor_t4189520834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Stop()
extern "C"  void NullWebCamTexAdaptor_Stop_m352467579 (NullWebCamTexAdaptor_t4189520834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
