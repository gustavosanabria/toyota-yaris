﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t2154819873;
// Vuforia.PlayModeEditorUtility
struct PlayModeEditorUtility_t1981907378;

#include "codegen/il2cpp-codegen.h"

// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
extern "C"  Il2CppObject * PlayModeEditorUtility_get_Instance_m3066837733 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
extern "C"  void PlayModeEditorUtility_set_Instance_m1878572472 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
extern "C"  void PlayModeEditorUtility__ctor_m2380394579 (PlayModeEditorUtility_t1981907378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
