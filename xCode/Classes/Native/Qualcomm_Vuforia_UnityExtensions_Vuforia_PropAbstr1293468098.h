﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Prop
struct Prop_t2165309157;
// UnityEngine.BoxCollider
struct BoxCollider_t2538127765;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2826579942.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t1293468098  : public SmartTerrainTrackableBehaviour_t2826579942
{
public:
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Il2CppObject * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t2538127765 * ___mBoxColliderToUpdate_14;

public:
	inline static int32_t get_offset_of_mProp_13() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t1293468098, ___mProp_13)); }
	inline Il2CppObject * get_mProp_13() const { return ___mProp_13; }
	inline Il2CppObject ** get_address_of_mProp_13() { return &___mProp_13; }
	inline void set_mProp_13(Il2CppObject * value)
	{
		___mProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___mProp_13, value);
	}

	inline static int32_t get_offset_of_mBoxColliderToUpdate_14() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t1293468098, ___mBoxColliderToUpdate_14)); }
	inline BoxCollider_t2538127765 * get_mBoxColliderToUpdate_14() const { return ___mBoxColliderToUpdate_14; }
	inline BoxCollider_t2538127765 ** get_address_of_mBoxColliderToUpdate_14() { return &___mBoxColliderToUpdate_14; }
	inline void set_mBoxColliderToUpdate_14(BoxCollider_t2538127765 * value)
	{
		___mBoxColliderToUpdate_14 = value;
		Il2CppCodeGenWriteBarrier(&___mBoxColliderToUpdate_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
