﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t1293468098;
// Vuforia.Prop
struct Prop_t2165309157;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.MeshCollider
struct MeshCollider_t2667653125;
// UnityEngine.BoxCollider
struct BoxCollider_t2538127765;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765.h"

// Vuforia.Prop Vuforia.PropAbstractBehaviour::get_Prop()
extern "C"  Il2CppObject * PropAbstractBehaviour_get_Prop_m2021750855 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::UpdateMeshAndColliders()
extern "C"  void PropAbstractBehaviour_UpdateMeshAndColliders_m1417820031 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Start()
extern "C"  void PropAbstractBehaviour_Start_m4276640579 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void PropAbstractBehaviour_InternalUnregisterTrackable_m76069549 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.InitializeProp(Vuforia.Prop)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m2901525315 (PropAbstractBehaviour_t1293468098 * __this, Il2CppObject * ___prop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m547418802 (PropAbstractBehaviour_t1293468098 * __this, MeshFilter_t3839065225 * ___meshFilterToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshFilterToUpdate()
extern "C"  MeshFilter_t3839065225 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m4005427626 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m158076594 (PropAbstractBehaviour_t1293468098 * __this, MeshCollider_t2667653125 * ___meshColliderToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshColliderToUpdate()
extern "C"  MeshCollider_t2667653125 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m1579696298 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetBoxColliderToUpdate(UnityEngine.BoxCollider)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m4267451050 (PropAbstractBehaviour_t1293468098 * __this, BoxCollider_t2538127765 * ___boxColliderToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_BoxColliderToUpdate()
extern "C"  BoxCollider_t2538127765 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m369215410 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::.ctor()
extern "C"  void PropAbstractBehaviour__ctor_m1034535491 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m898155801 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m1232702384 (PropAbstractBehaviour_t1293468098 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t1659122786 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m4106942966 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t3674682005 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m307052602 (PropAbstractBehaviour_t1293468098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
