﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t1152863025;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_Awake_m2014234559 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnEnable_m2754257834 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnDisable_m4218551491 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m2101116245 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2244357540 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, Action_t3771233898 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m4169824619 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, Action_t3771233898 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m3755567122 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m866121156 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2124475766 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaInitialized()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnVuforiaInitialized_m2328836761 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaStarted()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnVuforiaStarted_m4154826502 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnPause_m1181636232 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m142530391 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, bool ___autoStart0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
extern "C"  bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m3534579399 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m3816916185 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, float ___scaleToMM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
extern "C"  float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m25924599 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C"  void SmartTerrainTrackerAbstractBehaviour__ctor_m1776629340 (SmartTerrainTrackerAbstractBehaviour_t1152863025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
