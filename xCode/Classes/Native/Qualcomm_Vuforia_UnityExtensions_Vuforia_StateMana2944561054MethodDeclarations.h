﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.StateManagerImpl
struct StateManagerImpl_t2944561054;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t3185501911;
// Vuforia.WordManager
struct WordManager_t2702921325;
// Vuforia.Trackable
struct Trackable_t3781061455;
// Vuforia.DataSet
struct DataSet_t2095838082;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t2347335889;
// Vuforia.ImageTarget
struct ImageTarget_t3520455670;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t865486027;
// Vuforia.Marker
struct Marker_t616694972;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t3323440965;
// UnityEngine.Transform
struct Transform_t1659122786;
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t1273933373;
// Vuforia.VuforiaManagerImpl/WordData[]
struct WordDataU5BU5D_t2668962181;
// Vuforia.VuforiaManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t2868255668;
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t1213482721;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t3565607731;
// Vuforia.MultiTarget
struct MultiTarget_t1085092180;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t1139674014;
// Vuforia.CylinderTarget
struct CylinderTarget_t1959645577;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t637796117;
// Vuforia.ObjectTarget
struct ObjectTarget_t983729458;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbst865486027.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa2263627731.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::GetActiveTrackableBehaviours()
extern "C"  Il2CppObject* StateManagerImpl_GetActiveTrackableBehaviours_m3131489464 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::GetTrackableBehaviours()
extern "C"  Il2CppObject* StateManagerImpl_GetTrackableBehaviours_m3999626654 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordManager Vuforia.StateManagerImpl::GetWordManager()
extern "C"  WordManager_t2702921325 * StateManagerImpl_GetWordManager_m1343476670 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
extern "C"  void StateManagerImpl_DestroyTrackableBehavioursForTrackable_m294103510 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, bool ___destroyGameObjects1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateMarkerBehaviours()
extern "C"  void StateManagerImpl_AssociateMarkerBehaviours_m3929835675 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateTrackableBehavioursForDataSet(Vuforia.DataSet)
extern "C"  void StateManagerImpl_AssociateTrackableBehavioursForDataSet_m1522447877 (StateManagerImpl_t2944561054 * __this, DataSet_t2095838082 * ___dataSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RegisterExternallyManagedTrackableBehaviour(Vuforia.TrackableBehaviour)
extern "C"  void StateManagerImpl_RegisterExternallyManagedTrackableBehaviour_m2142786405 (StateManagerImpl_t2944561054 * __this, TrackableBehaviour_t4179556250 * ___trackableBehaviour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UnregisterExternallyManagedTrackableBehaviour(System.Int32)
extern "C"  void StateManagerImpl_UnregisterExternallyManagedTrackableBehaviour_m560158145 (StateManagerImpl_t2944561054 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RemoveDestroyedTrackables()
extern "C"  void StateManagerImpl_RemoveDestroyedTrackables_m342080688 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::ClearTrackableBehaviours()
extern "C"  void StateManagerImpl_ClearTrackableBehaviours_m4226140707 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::FindOrCreateImageTargetBehaviourForTrackable(Vuforia.ImageTarget,UnityEngine.GameObject)
extern "C"  ImageTargetAbstractBehaviour_t2347335889 * StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m2937945888 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, GameObject_t3674682005 * ___gameObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::FindOrCreateImageTargetBehaviourForTrackable(Vuforia.ImageTarget,UnityEngine.GameObject,Vuforia.DataSet)
extern "C"  ImageTargetAbstractBehaviour_t2347335889 * StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m2222231648 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, GameObject_t3674682005 * ___gameObject1, DataSet_t2095838082 * ___dataSet2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.StateManagerImpl::CreateNewMarkerBehaviourForMarker(Vuforia.Marker,System.String)
extern "C"  MarkerAbstractBehaviour_t865486027 * StateManagerImpl_CreateNewMarkerBehaviourForMarker_m501860521 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, String_t* ___gameObjectName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.StateManagerImpl::CreateNewMarkerBehaviourForMarker(Vuforia.Marker,UnityEngine.GameObject)
extern "C"  MarkerAbstractBehaviour_t865486027 * StateManagerImpl_CreateNewMarkerBehaviourForMarker_m1988440497 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, GameObject_t3674682005 * ___gameObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::SetTrackableBehavioursForTrackableToNotFound(Vuforia.Trackable)
extern "C"  void StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m2434712069 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::EnableTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
extern "C"  void StateManagerImpl_EnableTrackableBehavioursForTrackable_m127100229 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___trackable0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RemoveDisabledTrackablesFromQueue(System.Collections.Generic.LinkedList`1<System.Int32>&)
extern "C"  void StateManagerImpl_RemoveDisabledTrackablesFromQueue_m1963257034 (StateManagerImpl_t2944561054 * __this, LinkedList_1_t3323440965 ** ___trackableIDs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateCameraPose(UnityEngine.Transform,UnityEngine.Transform,Vuforia.VuforiaManagerImpl/TrackableResultData[],System.Int32)
extern "C"  void StateManagerImpl_UpdateCameraPose_m3559944391 (StateManagerImpl_t2944561054 * __this, Transform_t1659122786 * ___cameraTransform0, Transform_t1659122786 * ___parentTransformToUpdate1, TrackableResultDataU5BU5D_t1273933373* ___trackableResultDataArray2, int32_t ___originTrackableID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateTrackablePoses(UnityEngine.Transform,Vuforia.VuforiaManagerImpl/TrackableResultData[],System.Int32,System.Int32)
extern "C"  void StateManagerImpl_UpdateTrackablePoses_m1614076878 (StateManagerImpl_t2944561054 * __this, Transform_t1659122786 * ___arCameraTransform0, TrackableResultDataU5BU5D_t1273933373* ___trackableResultDataArray1, int32_t ___originTrackableID2, int32_t ___frameIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateVirtualButtons(System.Int32,System.IntPtr)
extern "C"  void StateManagerImpl_UpdateVirtualButtons_m1195621459 (StateManagerImpl_t2944561054 * __this, int32_t ___numVirtualButtons0, IntPtr_t ___virtualButtonPtr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateWords(UnityEngine.Transform,Vuforia.VuforiaManagerImpl/WordData[],Vuforia.VuforiaManagerImpl/WordResultData[])
extern "C"  void StateManagerImpl_UpdateWords_m3878418863 (StateManagerImpl_t2944561054 * __this, Transform_t1659122786 * ___arCameraTransform0, WordDataU5BU5D_t2668962181* ___wordData1, WordResultDataU5BU5D_t2868255668* ___wordResultData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateVirtualButtonBehaviours(Vuforia.VirtualButtonAbstractBehaviour[],Vuforia.DataSet)
extern "C"  void StateManagerImpl_AssociateVirtualButtonBehaviours_m3042380094 (StateManagerImpl_t2944561054 * __this, VirtualButtonAbstractBehaviourU5BU5D_t1213482721* ___vbBehaviours0, DataSet_t2095838082 * ___dataSet1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::CreateMissingDataSetTrackableBehaviours(Vuforia.DataSet)
extern "C"  void StateManagerImpl_CreateMissingDataSetTrackableBehaviours_m766241420 (StateManagerImpl_t2944561054 * __this, DataSet_t2095838082 * ___dataSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateImageTargetBehaviour(Vuforia.ImageTarget)
extern "C"  ImageTargetAbstractBehaviour_t2347335889 * StateManagerImpl_CreateImageTargetBehaviour_m2144695222 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___imageTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateMultiTargetBehaviour(Vuforia.MultiTarget)
extern "C"  MultiTargetAbstractBehaviour_t3565607731 * StateManagerImpl_CreateMultiTargetBehaviour_m1044717272 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___multiTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateCylinderTargetBehaviour(Vuforia.CylinderTarget)
extern "C"  CylinderTargetAbstractBehaviour_t1139674014 * StateManagerImpl_CreateCylinderTargetBehaviour_m2539480713 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___cylinderTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateObjectTargetBehaviour(Vuforia.ObjectTarget)
extern "C"  ObjectTargetAbstractBehaviour_t637796117 * StateManagerImpl_CreateObjectTargetBehaviour_m258530670 (StateManagerImpl_t2944561054 * __this, Il2CppObject * ___objectTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::InitializeMarkerBehaviour(Vuforia.MarkerAbstractBehaviour,Vuforia.Marker)
extern "C"  void StateManagerImpl_InitializeMarkerBehaviour_m2843589059 (StateManagerImpl_t2944561054 * __this, MarkerAbstractBehaviour_t865486027 * ___markerBehaviour0, Il2CppObject * ___marker1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::PositionCamera(Vuforia.TrackableBehaviour,UnityEngine.Transform,UnityEngine.Transform,Vuforia.VuforiaManagerImpl/PoseData)
extern "C"  void StateManagerImpl_PositionCamera_m1094944102 (StateManagerImpl_t2944561054 * __this, TrackableBehaviour_t4179556250 * ___trackableBehaviour0, Transform_t1659122786 * ___cameraTransform1, Transform_t1659122786 * ___parentTransformToUpdate2, PoseData_t2263627731  ___camToTargetPose3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.StateManagerImpl::ExtractTranslationFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Vector3_t4282066566  StateManagerImpl_ExtractTranslationFromMatrix_m1188993424 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.StateManagerImpl::ExtractRotationFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t1553702882  StateManagerImpl_ExtractRotationFromMatrix_m467398563 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::PositionTrackable(Vuforia.TrackableBehaviour,UnityEngine.Transform,Vuforia.VuforiaManagerImpl/PoseData)
extern "C"  void StateManagerImpl_PositionTrackable_m3865365141 (StateManagerImpl_t2944561054 * __this, TrackableBehaviour_t4179556250 * ___trackableBehaviour0, Transform_t1659122786 * ___arCameraTransform1, PoseData_t2263627731  ___camToTargetPose2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::.ctor()
extern "C"  void StateManagerImpl__ctor_m741297807 (StateManagerImpl_t2944561054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
