﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t142368528;
// Vuforia.Surface
struct Surface_t3094389719;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.MeshCollider
struct MeshCollider_t2667653125;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125.h"

// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::get_Surface()
extern "C"  Il2CppObject * SurfaceAbstractBehaviour_get_Surface_m3105593613 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void SurfaceAbstractBehaviour_InternalUnregisterTrackable_m4052980807 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.InitializeSurface(Vuforia.Surface)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m776930061 (SurfaceAbstractBehaviour_t142368528 * __this, Il2CppObject * ___surface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m600711254 (SurfaceAbstractBehaviour_t142368528 * __this, MeshFilter_t3839065225 * ___meshFilterToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshFilterToUpdate()
extern "C"  MeshFilter_t3839065225 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m1361067718 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m826395222 (SurfaceAbstractBehaviour_t142368528 * __this, MeshCollider_t2667653125 * ___meshColliderToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshColliderToUpdate()
extern "C"  MeshCollider_t2667653125 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m3868330950 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
extern "C"  void SurfaceAbstractBehaviour__ctor_m3989293917 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2013946459 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m547847498 (SurfaceAbstractBehaviour_t142368528 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t1659122786 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2382540564 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t3674682005 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m1552705306 (SurfaceAbstractBehaviour_t142368528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
