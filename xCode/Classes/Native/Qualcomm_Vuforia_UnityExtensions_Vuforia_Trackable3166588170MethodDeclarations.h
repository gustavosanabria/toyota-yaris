﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TrackableSourceImpl
struct TrackableSourceImpl_t3166588170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.IntPtr Vuforia.TrackableSourceImpl::get_TrackableSourcePtr()
extern "C"  IntPtr_t TrackableSourceImpl_get_TrackableSourcePtr_m1338931339 (TrackableSourceImpl_t3166588170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::set_TrackableSourcePtr(System.IntPtr)
extern "C"  void TrackableSourceImpl_set_TrackableSourcePtr_m3210375808 (TrackableSourceImpl_t3166588170 * __this, IntPtr_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::.ctor(System.IntPtr)
extern "C"  void TrackableSourceImpl__ctor_m1154113849 (TrackableSourceImpl_t3166588170 * __this, IntPtr_t ___trackableSourcePtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
