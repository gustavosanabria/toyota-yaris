﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.Tracker
struct Tracker_t3880226402;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.Tracker::get_IsActive()
extern "C"  bool Tracker_get_IsActive_m4164706286 (Tracker_t3880226402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
extern "C"  void Tracker_set_IsActive_m3846885381 (Tracker_t3880226402 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::.ctor()
extern "C"  void Tracker__ctor_m376239779 (Tracker_t3880226402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
