﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct UserDefinedTargetBuildingAbstractBehaviour_t1531824314;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t2660219000;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarge614449126.h"

// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m3218697314 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern "C"  bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m528702119 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m2477537156 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m275901235 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, String_t* ___targetName0, float ___sceenSizeWidth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m653871364 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m97208647 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, int32_t ___frameQuality0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_Start_m3685481715 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_Update_m2586635642 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m3680238259 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m2859173594 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m741738348 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnVuforiaStarted()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_OnVuforiaStarted_m3858226447 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m1462662687 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour__ctor_m443376627 (UserDefinedTargetBuildingAbstractBehaviour_t1531824314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
