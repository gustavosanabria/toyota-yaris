﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoTextureRendererAbstractBehaviour
struct  VideoTextureRendererAbstractBehaviour_t4076028962  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D Vuforia.VideoTextureRendererAbstractBehaviour::mTexture
	Texture2D_t3884108195 * ___mTexture_2;
	// System.Boolean Vuforia.VideoTextureRendererAbstractBehaviour::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_3;
	// System.Int32 Vuforia.VideoTextureRendererAbstractBehaviour::mNativeTextureID
	int32_t ___mNativeTextureID_4;

public:
	inline static int32_t get_offset_of_mTexture_2() { return static_cast<int32_t>(offsetof(VideoTextureRendererAbstractBehaviour_t4076028962, ___mTexture_2)); }
	inline Texture2D_t3884108195 * get_mTexture_2() const { return ___mTexture_2; }
	inline Texture2D_t3884108195 ** get_address_of_mTexture_2() { return &___mTexture_2; }
	inline void set_mTexture_2(Texture2D_t3884108195 * value)
	{
		___mTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___mTexture_2, value);
	}

	inline static int32_t get_offset_of_mVideoBgConfigChanged_3() { return static_cast<int32_t>(offsetof(VideoTextureRendererAbstractBehaviour_t4076028962, ___mVideoBgConfigChanged_3)); }
	inline bool get_mVideoBgConfigChanged_3() const { return ___mVideoBgConfigChanged_3; }
	inline bool* get_address_of_mVideoBgConfigChanged_3() { return &___mVideoBgConfigChanged_3; }
	inline void set_mVideoBgConfigChanged_3(bool value)
	{
		___mVideoBgConfigChanged_3 = value;
	}

	inline static int32_t get_offset_of_mNativeTextureID_4() { return static_cast<int32_t>(offsetof(VideoTextureRendererAbstractBehaviour_t4076028962, ___mNativeTextureID_4)); }
	inline int32_t get_mNativeTextureID_4() const { return ___mNativeTextureID_4; }
	inline int32_t* get_address_of_mNativeTextureID_4() { return &___mNativeTextureID_4; }
	inline void set_mNativeTextureID_4(int32_t value)
	{
		___mNativeTextureID_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
