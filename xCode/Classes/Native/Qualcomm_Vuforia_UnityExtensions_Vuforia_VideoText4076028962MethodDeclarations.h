﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t4076028962;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern "C"  void VideoTextureRendererAbstractBehaviour_Awake_m1846945094 (VideoTextureRendererAbstractBehaviour_t4076028962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern "C"  void VideoTextureRendererAbstractBehaviour_Start_m556477667 (VideoTextureRendererAbstractBehaviour_t4076028962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern "C"  void VideoTextureRendererAbstractBehaviour_Update_m76790666 (VideoTextureRendererAbstractBehaviour_t4076028962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern "C"  void VideoTextureRendererAbstractBehaviour_OnDestroy_m1035714396 (VideoTextureRendererAbstractBehaviour_t4076028962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C"  void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m2649023497 (VideoTextureRendererAbstractBehaviour_t4076028962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern "C"  void VideoTextureRendererAbstractBehaviour__ctor_m1609339875 (VideoTextureRendererAbstractBehaviour_t4076028962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
