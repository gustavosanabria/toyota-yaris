﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa2263627731.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1781697161.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/WordResultData
struct  WordResultData_t1826866697 
{
public:
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/WordResultData::pose
	PoseData_t2263627731  ___pose_0;
	// Vuforia.TrackableBehaviour/Status Vuforia.VuforiaManagerImpl/WordResultData::status
	int32_t ___status_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordResultData::id
	int32_t ___id_2;
	// Vuforia.VuforiaManagerImpl/Obb2D Vuforia.VuforiaManagerImpl/WordResultData::orientedBoundingBox
	Obb2D_t1781697161  ___orientedBoundingBox_3;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(WordResultData_t1826866697, ___pose_0)); }
	inline PoseData_t2263627731  get_pose_0() const { return ___pose_0; }
	inline PoseData_t2263627731 * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_t2263627731  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(WordResultData_t1826866697, ___status_1)); }
	inline int32_t get_status_1() const { return ___status_1; }
	inline int32_t* get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(int32_t value)
	{
		___status_1 = value;
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(WordResultData_t1826866697, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_orientedBoundingBox_3() { return static_cast<int32_t>(offsetof(WordResultData_t1826866697, ___orientedBoundingBox_3)); }
	inline Obb2D_t1781697161  get_orientedBoundingBox_3() const { return ___orientedBoundingBox_3; }
	inline Obb2D_t1781697161 * get_address_of_orientedBoundingBox_3() { return &___orientedBoundingBox_3; }
	inline void set_orientedBoundingBox_3(Obb2D_t1781697161  value)
	{
		___orientedBoundingBox_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Vuforia.VuforiaManagerImpl/WordResultData
#pragma pack(push, tp, 1)
struct WordResultData_t1826866697_marshaled_pinvoke
{
	PoseData_t2263627731_marshaled_pinvoke ___pose_0;
	int32_t ___status_1;
	int32_t ___id_2;
	Obb2D_t1781697161_marshaled_pinvoke ___orientedBoundingBox_3;
};
#pragma pack(pop, tp)
// Native definition for marshalling of: Vuforia.VuforiaManagerImpl/WordResultData
#pragma pack(push, tp, 1)
struct WordResultData_t1826866697_marshaled_com
{
	PoseData_t2263627731_marshaled_com ___pose_0;
	int32_t ___status_1;
	int32_t ___id_2;
	Obb2D_t1781697161_marshaled_com ___orientedBoundingBox_3;
};
#pragma pack(pop, tp)
