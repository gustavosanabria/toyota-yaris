﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaManagerImpl/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t3728673716;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"

// System.Void Vuforia.VuforiaManagerImpl/<>c__DisplayClass3::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3__ctor_m1212591163 (U3CU3Ec__DisplayClass3_t3728673716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaManagerImpl/<>c__DisplayClass3::<UpdateTrackers>b__1(Vuforia.VuforiaManagerImpl/TrackableResultData)
extern "C"  bool U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m334552406 (U3CU3Ec__DisplayClass3_t3728673716 * __this, TrackableResultData_t395876724  ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
