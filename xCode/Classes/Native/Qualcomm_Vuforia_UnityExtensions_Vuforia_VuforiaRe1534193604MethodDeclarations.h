﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaRenderer/VideoBGCfgData
struct VideoBGCfgData_t1534193604;
struct VideoBGCfgData_t1534193604_marshaled_pinvoke;
struct VideoBGCfgData_t1534193604_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct VideoBGCfgData_t1534193604;
struct VideoBGCfgData_t1534193604_marshaled_pinvoke;

extern "C" void VideoBGCfgData_t1534193604_marshal_pinvoke(const VideoBGCfgData_t1534193604& unmarshaled, VideoBGCfgData_t1534193604_marshaled_pinvoke& marshaled);
extern "C" void VideoBGCfgData_t1534193604_marshal_pinvoke_back(const VideoBGCfgData_t1534193604_marshaled_pinvoke& marshaled, VideoBGCfgData_t1534193604& unmarshaled);
extern "C" void VideoBGCfgData_t1534193604_marshal_pinvoke_cleanup(VideoBGCfgData_t1534193604_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VideoBGCfgData_t1534193604;
struct VideoBGCfgData_t1534193604_marshaled_com;

extern "C" void VideoBGCfgData_t1534193604_marshal_com(const VideoBGCfgData_t1534193604& unmarshaled, VideoBGCfgData_t1534193604_marshaled_com& marshaled);
extern "C" void VideoBGCfgData_t1534193604_marshal_com_back(const VideoBGCfgData_t1534193604_marshaled_com& marshaled, VideoBGCfgData_t1534193604& unmarshaled);
extern "C" void VideoBGCfgData_t1534193604_marshal_com_cleanup(VideoBGCfgData_t1534193604_marshaled_com& marshaled);
