﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaRendererImpl
struct VuforiaRendererImpl_t2217546767;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe1534193604.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe4078668580.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2019914778.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe1888443502.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2868837278.h"

// UnityEngine.Texture2D Vuforia.VuforiaRendererImpl::get_VideoBackgroundTexture()
extern "C"  Texture2D_t3884108195 * VuforiaRendererImpl_get_VideoBackgroundTexture_m3509418668 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRenderer/VideoBGCfgData Vuforia.VuforiaRendererImpl::GetVideoBackgroundConfig()
extern "C"  VideoBGCfgData_t1534193604  VuforiaRendererImpl_GetVideoBackgroundConfig_m1124814034 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRendererImpl::ClearVideoBackgroundConfig()
extern "C"  void VuforiaRendererImpl_ClearVideoBackgroundConfig_m103631372 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRendererImpl::SetVideoBackgroundConfig(Vuforia.VuforiaRenderer/VideoBGCfgData)
extern "C"  void VuforiaRendererImpl_SetVideoBackgroundConfig_m2177526227 (VuforiaRendererImpl_t2217546767 * __this, VideoBGCfgData_t1534193604  ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRendererImpl::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
extern "C"  bool VuforiaRendererImpl_SetVideoBackgroundTexture_m1246416751 (VuforiaRendererImpl_t2217546767 * __this, Texture2D_t3884108195 * ___texture0, int32_t ___nativeTextureID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRendererImpl::IsVideoBackgroundInfoAvailable()
extern "C"  bool VuforiaRendererImpl_IsVideoBackgroundInfoAvailable_m1477187454 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRenderer/VideoTextureInfo Vuforia.VuforiaRendererImpl::GetVideoTextureInfo()
extern "C"  VideoTextureInfo_t4078668580  VuforiaRendererImpl_GetVideoTextureInfo_m1434496297 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRendererImpl::Pause(System.Boolean)
extern "C"  void VuforiaRendererImpl_Pause_m780323617 (VuforiaRendererImpl_t2217546767 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaRendererImpl::GetRecommendedFps(Vuforia.VuforiaRenderer/FpsHint)
extern "C"  int32_t VuforiaRendererImpl_GetRecommendedFps_m181172634 (VuforiaRendererImpl_t2217546767 * __this, int32_t ___flags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRendererImpl::UnityRenderEvent(Vuforia.VuforiaRendererImpl/RenderEvent)
extern "C"  void VuforiaRendererImpl_UnityRenderEvent_m3747708295 (VuforiaRendererImpl_t2217546767 * __this, int32_t ___renderEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRendererImpl::HasBackgroundTextureChanged()
extern "C"  bool VuforiaRendererImpl_HasBackgroundTextureChanged_m218271745 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRendererImpl::SetVideoBackgroundConfigInternal(Vuforia.VuforiaRenderer/VideoBGCfgData)
extern "C"  void VuforiaRendererImpl_SetVideoBackgroundConfigInternal_m725302704 (VuforiaRendererImpl_t2217546767 * __this, VideoBGCfgData_t1534193604  ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaRendererImpl::GetLastSetReflection()
extern "C"  int32_t VuforiaRendererImpl_GetLastSetReflection_m3612314 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRendererImpl::.ctor()
extern "C"  void VuforiaRendererImpl__ctor_m117637526 (VuforiaRendererImpl_t2217546767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
