﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.IVuforiaWrapper
struct IVuforiaWrapper_t2793608676;

#include "codegen/il2cpp-codegen.h"

// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::get_Instance()
extern "C"  Il2CppObject * VuforiaWrapper_get_Instance_m2545616589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaWrapper::Create()
extern "C"  void VuforiaWrapper_Create_m3936659814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaWrapper::SetImplementation(Vuforia.IVuforiaWrapper)
extern "C"  void VuforiaWrapper_SetImplementation_m898691896 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___implementation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaWrapper::.cctor()
extern "C"  void VuforiaWrapper__cctor_m1229879955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
