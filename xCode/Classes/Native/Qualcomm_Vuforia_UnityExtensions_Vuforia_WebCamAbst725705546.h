﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t1072092957;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamAbstractBehaviour
struct  WebCamAbstractBehaviour_t725705546  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 Vuforia.WebCamAbstractBehaviour::RenderTextureLayer
	int32_t ___RenderTextureLayer_2;
	// System.Boolean Vuforia.WebCamAbstractBehaviour::mPlayModeRenderVideo
	bool ___mPlayModeRenderVideo_3;
	// System.String Vuforia.WebCamAbstractBehaviour::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_4;
	// System.Boolean Vuforia.WebCamAbstractBehaviour::mFlipHorizontally
	bool ___mFlipHorizontally_5;
	// System.Boolean Vuforia.WebCamAbstractBehaviour::mTurnOffWebCam
	bool ___mTurnOffWebCam_6;
	// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::mWebCamImpl
	WebCamImpl_t1072092957 * ___mWebCamImpl_7;
	// UnityEngine.Camera Vuforia.WebCamAbstractBehaviour::mBackgroundCameraInstance
	Camera_t2727095145 * ___mBackgroundCameraInstance_8;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_2() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___RenderTextureLayer_2)); }
	inline int32_t get_RenderTextureLayer_2() const { return ___RenderTextureLayer_2; }
	inline int32_t* get_address_of_RenderTextureLayer_2() { return &___RenderTextureLayer_2; }
	inline void set_RenderTextureLayer_2(int32_t value)
	{
		___RenderTextureLayer_2 = value;
	}

	inline static int32_t get_offset_of_mPlayModeRenderVideo_3() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___mPlayModeRenderVideo_3)); }
	inline bool get_mPlayModeRenderVideo_3() const { return ___mPlayModeRenderVideo_3; }
	inline bool* get_address_of_mPlayModeRenderVideo_3() { return &___mPlayModeRenderVideo_3; }
	inline void set_mPlayModeRenderVideo_3(bool value)
	{
		___mPlayModeRenderVideo_3 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_4() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___mDeviceNameSetInEditor_4)); }
	inline String_t* get_mDeviceNameSetInEditor_4() const { return ___mDeviceNameSetInEditor_4; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_4() { return &___mDeviceNameSetInEditor_4; }
	inline void set_mDeviceNameSetInEditor_4(String_t* value)
	{
		___mDeviceNameSetInEditor_4 = value;
		Il2CppCodeGenWriteBarrier(&___mDeviceNameSetInEditor_4, value);
	}

	inline static int32_t get_offset_of_mFlipHorizontally_5() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___mFlipHorizontally_5)); }
	inline bool get_mFlipHorizontally_5() const { return ___mFlipHorizontally_5; }
	inline bool* get_address_of_mFlipHorizontally_5() { return &___mFlipHorizontally_5; }
	inline void set_mFlipHorizontally_5(bool value)
	{
		___mFlipHorizontally_5 = value;
	}

	inline static int32_t get_offset_of_mTurnOffWebCam_6() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___mTurnOffWebCam_6)); }
	inline bool get_mTurnOffWebCam_6() const { return ___mTurnOffWebCam_6; }
	inline bool* get_address_of_mTurnOffWebCam_6() { return &___mTurnOffWebCam_6; }
	inline void set_mTurnOffWebCam_6(bool value)
	{
		___mTurnOffWebCam_6 = value;
	}

	inline static int32_t get_offset_of_mWebCamImpl_7() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___mWebCamImpl_7)); }
	inline WebCamImpl_t1072092957 * get_mWebCamImpl_7() const { return ___mWebCamImpl_7; }
	inline WebCamImpl_t1072092957 ** get_address_of_mWebCamImpl_7() { return &___mWebCamImpl_7; }
	inline void set_mWebCamImpl_7(WebCamImpl_t1072092957 * value)
	{
		___mWebCamImpl_7 = value;
		Il2CppCodeGenWriteBarrier(&___mWebCamImpl_7, value);
	}

	inline static int32_t get_offset_of_mBackgroundCameraInstance_8() { return static_cast<int32_t>(offsetof(WebCamAbstractBehaviour_t725705546, ___mBackgroundCameraInstance_8)); }
	inline Camera_t2727095145 * get_mBackgroundCameraInstance_8() const { return ___mBackgroundCameraInstance_8; }
	inline Camera_t2727095145 ** get_address_of_mBackgroundCameraInstance_8() { return &___mBackgroundCameraInstance_8; }
	inline void set_mBackgroundCameraInstance_8(Camera_t2727095145 * value)
	{
		___mBackgroundCameraInstance_8 = value;
		Il2CppCodeGenWriteBarrier(&___mBackgroundCameraInstance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
