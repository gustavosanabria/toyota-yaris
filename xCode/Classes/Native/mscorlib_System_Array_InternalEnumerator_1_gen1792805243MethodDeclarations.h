﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1792805243.h"
#include "mscorlib_System_Array1146569071.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E3010462567.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1623315628_gshared (InternalEnumerator_1_t1792805243 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1623315628(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1792805243 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1623315628_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m271575412_gshared (InternalEnumerator_1_t1792805243 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m271575412(__this, method) ((  void (*) (InternalEnumerator_1_t1792805243 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m271575412_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402617002_gshared (InternalEnumerator_1_t1792805243 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402617002(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1792805243 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402617002_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2681537923_gshared (InternalEnumerator_1_t1792805243 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2681537923(__this, method) ((  void (*) (InternalEnumerator_1_t1792805243 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2681537923_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m100511140_gshared (InternalEnumerator_1_t1792805243 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m100511140(__this, method) ((  bool (*) (InternalEnumerator_1_t1792805243 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m100511140_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::get_Current()
extern "C"  EyewearCalibrationReading_t3010462567  InternalEnumerator_1_get_Current_m1950257429_gshared (InternalEnumerator_1_t1792805243 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1950257429(__this, method) ((  EyewearCalibrationReading_t3010462567  (*) (InternalEnumerator_1_t1792805243 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1950257429_gshared)(__this, method)
