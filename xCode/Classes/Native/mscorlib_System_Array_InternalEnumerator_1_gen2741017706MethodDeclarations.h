﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<CardboardOnGUIWindow>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3306946535(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2741017706 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<CardboardOnGUIWindow>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m451167833(__this, method) ((  void (*) (InternalEnumerator_1_t2741017706 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<CardboardOnGUIWindow>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1978723909(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2741017706 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CardboardOnGUIWindow>::Dispose()
#define InternalEnumerator_1_Dispose_m3992317566(__this, method) ((  void (*) (InternalEnumerator_1_t2741017706 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CardboardOnGUIWindow>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1720333509(__this, method) ((  bool (*) (InternalEnumerator_1_t2741017706 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CardboardOnGUIWindow>::get_Current()
#define InternalEnumerator_1_get_Current_m3939937838(__this, method) ((  CardboardOnGUIWindow_t3958675030 * (*) (InternalEnumerator_1_t2741017706 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
