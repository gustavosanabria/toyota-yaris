﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3335230866.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Tutorial257920894.h"

// System.Void System.Array/InternalEnumerator`1<Tutorial>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1716220943_gshared (InternalEnumerator_1_t3335230866 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1716220943(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3335230866 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1716220943_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Tutorial>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3150041393_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3150041393(__this, method) ((  void (*) (InternalEnumerator_1_t3335230866 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3150041393_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Tutorial>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m934371229_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m934371229(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3335230866 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m934371229_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Tutorial>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m385414310_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m385414310(__this, method) ((  void (*) (InternalEnumerator_1_t3335230866 *, const MethodInfo*))InternalEnumerator_1_Dispose_m385414310_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Tutorial>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2844926109_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2844926109(__this, method) ((  bool (*) (InternalEnumerator_1_t3335230866 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2844926109_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Tutorial>::get_Current()
extern "C"  Tutorial_t257920894  InternalEnumerator_1_get_Current_m4224607958_gshared (InternalEnumerator_1_t3335230866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4224607958(__this, method) ((  Tutorial_t257920894  (*) (InternalEnumerator_1_t3335230866 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4224607958_gshared)(__this, method)
