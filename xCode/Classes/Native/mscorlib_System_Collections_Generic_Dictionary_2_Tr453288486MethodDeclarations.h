﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Transform_1_t453288486;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m558890822_gshared (Transform_1_t453288486 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m558890822(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t453288486 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m558890822_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(TKey,TValue)
extern "C"  TrackableResultData_t395876724  Transform_1_Invoke_m2064834290_gshared (Transform_1_t453288486 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2064834290(__this, ___key0, ___value1, method) ((  TrackableResultData_t395876724  (*) (Transform_1_t453288486 *, int32_t, TrackableResultData_t395876724 , const MethodInfo*))Transform_1_Invoke_m2064834290_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2739079325_gshared (Transform_1_t453288486 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2739079325(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t453288486 *, int32_t, TrackableResultData_t395876724 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2739079325_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C"  TrackableResultData_t395876724  Transform_1_EndInvoke_m2746177112_gshared (Transform_1_t453288486 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2746177112(__this, ___result0, method) ((  TrackableResultData_t395876724  (*) (Transform_1_t453288486 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2746177112_gshared)(__this, ___result0, method)
