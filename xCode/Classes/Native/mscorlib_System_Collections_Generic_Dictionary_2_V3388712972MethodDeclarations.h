﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ValueCollection_t3388712972;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t393139963;
// System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct IEnumerator_1_t2307741773;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t1273933373;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2619940667.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1020020923_gshared (ValueCollection_t3388712972 * __this, Dictionary_2_t393139963 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1020020923(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3388712972 *, Dictionary_2_t393139963 *, const MethodInfo*))ValueCollection__ctor_m1020020923_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m595095575_gshared (ValueCollection_t3388712972 * __this, TrackableResultData_t395876724  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m595095575(__this, ___item0, method) ((  void (*) (ValueCollection_t3388712972 *, TrackableResultData_t395876724 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m595095575_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4041212000_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4041212000(__this, method) ((  void (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4041212000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3968080495_gshared (ValueCollection_t3388712972 * __this, TrackableResultData_t395876724  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3968080495(__this, ___item0, method) ((  bool (*) (ValueCollection_t3388712972 *, TrackableResultData_t395876724 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3968080495_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3039757012_gshared (ValueCollection_t3388712972 * __this, TrackableResultData_t395876724  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3039757012(__this, ___item0, method) ((  bool (*) (ValueCollection_t3388712972 *, TrackableResultData_t395876724 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3039757012_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2768839022_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2768839022(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2768839022_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2061085028_gshared (ValueCollection_t3388712972 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2061085028(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3388712972 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2061085028_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1109681247_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1109681247(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1109681247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2243256354_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2243256354(__this, method) ((  bool (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2243256354_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2554415362_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2554415362(__this, method) ((  bool (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2554415362_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3951845870_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3951845870(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3951845870_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2384098882_gshared (ValueCollection_t3388712972 * __this, TrackableResultDataU5BU5D_t1273933373* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2384098882(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3388712972 *, TrackableResultDataU5BU5D_t1273933373*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2384098882_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetEnumerator()
extern "C"  Enumerator_t2619940667  ValueCollection_GetEnumerator_m1969412773_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1969412773(__this, method) ((  Enumerator_t2619940667  (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_GetEnumerator_m1969412773_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m339087176_gshared (ValueCollection_t3388712972 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m339087176(__this, method) ((  int32_t (*) (ValueCollection_t3388712972 *, const MethodInfo*))ValueCollection_get_Count_m339087176_gshared)(__this, method)
