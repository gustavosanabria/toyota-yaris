﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct DefaultComparer_t3138620687;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C"  void DefaultComparer__ctor_m3691377184_gshared (DefaultComparer_t3138620687 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3691377184(__this, method) ((  void (*) (DefaultComparer_t3138620687 *, const MethodInfo*))DefaultComparer__ctor_m3691377184_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3805551059_gshared (DefaultComparer_t3138620687 * __this, TrackableResultData_t395876724  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3805551059(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3138620687 *, TrackableResultData_t395876724 , const MethodInfo*))DefaultComparer_GetHashCode_m3805551059_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1840848565_gshared (DefaultComparer_t3138620687 * __this, TrackableResultData_t395876724  ___x0, TrackableResultData_t395876724  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1840848565(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3138620687 *, TrackableResultData_t395876724 , TrackableResultData_t395876724 , const MethodInfo*))DefaultComparer_Equals_m1840848565_gshared)(__this, ___x0, ___y1, method)
